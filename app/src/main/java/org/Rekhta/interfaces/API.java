package org.Rekhta.interfaces;

import java.util.HashMap;

import org.Rekhta.model.ContentTypePojo;
import org.Rekhta.model.historyModels.HistoryMainPojo;
import org.Rekhta.model.homeScreenModel.HomeMainPojo;
import org.Rekhta.model.myFavorite.OnlineFavModelMain;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Query;
import rx.Observable;


public interface API {

    /*
    @POST("/deviceInfo")
    void registerDevice(@Body HashMap obj, Callback<deviceRegistrationResponse> callback);


    @GET("/appInfo")
    void getLanguageObj(@Header("Content-Type") String contentType, @Header("fac") String lang, @Header("version") String version, Callback<HashMap> callback);

    @POST("/deviceInfo")
    void setLanguageObj(@Body HashMap obj, Callback<HashMap> callback);
*/
    @POST("/V4_ApiAccount/V4Login")
    void login(@Header("Content-Type") String contentType, @Header("TempToken") String token, @Query("reToken") String retoken, @Body HashMap obj, Callback<HashMap> callback);

    @POST("/V4_ApiAccount/V4Register")
    void register(@Header("Content-Type") String contentType, @Header("TempToken") String token, @Body HashMap obj, Callback<HashMap> callback);

    @POST("/V4_ApiAccount/V4ForgotPassword")
    void forgotPassword(@Header("Content-Type") String contentType, @Header("TempToken") String token, @Query("reToken") String retoken, @Body HashMap obj, Callback<HashMap> callback);

    @POST("/V4_ApiAccount/V4LoginExternal")
    void externalLoginFbGplus(@Header("Content-Type") String contentType, @Header("TempToken") String token, @Query("reToken") String retoken, @Body HashMap obj, Callback<HashMap> callback);

    @POST("/v4/shayari/GetContentListWithPaging")
    void getContentListWithPaging(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("poetId") String poet, @Query("contentTypeId") String contentTypeId, @Query("targetId") String targetId, @Query("keyword") String keyword, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

    @GET("/v4/shayari/GetContentById")
    void getContentById(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("lang") int langcode, @Query("contentId") String conntentCode, Callback<HashMap> callback);

    @GET("/v4/shayari/GetBottomContentById")
    void getBottomContentById(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("lang") int langcode, @Query("contentId") String contentCode, Callback<HashMap> callback);

    @POST("/v4/shayari/GetWordMeaning")
    void getWordMeaning(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("word") String word, Callback<HashMap> callback);

    @POST("/v4/shayari/Critique")
    void submitCritique(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Body HashMap hashMap, Callback<HashMap> callback);

    @POST("/v4/shayari/GetTagsList")
    void getSherTagData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, Callback<HashMap> callback);

    @POST("/v4/shayari/GetT20")
    void getTop20Sher(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, Callback<HashMap> callback);

    @POST("/v4/shayari/GetCoupletListWithPaging")
    void getSlectedSherCollectionData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("poetId") String poet, @Query("contentTypeId") String contentTypeId, @Query("targetId") String targetId, @Query("keyword") String keyword, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

    @POST("/v4/shayari/GetPoetsListWithPaging")
    void getPoetsData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("lastFetchDate") String lastFetchDate, @Query("targetId") String targetId, @Query("keyword") String keyword, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

    @POST("/v4/shayari/GetRekhtaDictionaryMeanings")
    void getRekhtaDictionaryData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("keyword") String keyword, @Header("Authorization") String auth, Callback<HashMap> callback);

    @POST("/v4/shayari/GetPlattsDictionaryMeanings")
    void getPlattsDictionaryData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("keyword") String keyword, @Header("Authorization") String auth, Callback<HashMap> callback);

    @POST("/v4/shayari/GetImageDictionaryMeanings")
    void getImageDictionaryData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("keyword") String keyword, @Header("Authorization") String auth, Callback<HashMap> callback);

    @GET("/v4/shayari/WordOfTheDay")
    void getWordOfTheDay(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("displayDate") String displayDate, Callback<HashMap> callback);

    @POST("/v4/shayari/GetShayariImages")
    void getImageShayriData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

    @POST("/v4/shayari/GetPoetProfile")
    void getPoetDetails(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Query("poetId") String poetId, Callback<HashMap> callback);

    @POST("/v4/shayari/GetUserSettings")
    void getSettingDetails(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, Callback<HashMap> callback);

    @POST("/v4/shayari/SetUserSetting")
    void updateSettingDetails(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("setting") int settingNo, @Query("value") boolean value, Callback<HashMap> callback);

    @POST("/v4/shayari/MarkFavorite")
    void markAsFavourite(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("contentId") String contentId, @Query("language") int langcode, Callback<HashMap> callback);

    @POST("/v4/shayari/RemoveFavorite")
    void removeFromFavourite(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("contentId") String contentId, Callback<HashMap> callback);

    //For Search Screen
    @POST("/v4/shayari/SearchAll")
    void getAllSearchedData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("keyword") String keyword, @Query("lang") int langCode, Callback<HashMap> callback);

    @POST("/v4/shayari/SearchAllLoadOnDemand")
    void getSearchAllLoadOnDemandData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("keyword") String keyword, @Query("lang") int langCode, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

    @POST("/v4/shayari/SearchContentByTypePageWise")
    void getSearchContentByTypePageWiseData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("keyword") String keyword, @Query("lang") int langCode, @Query("pageIndex") int pageIndex, @Query("type") int type, Callback<HashMap> callback);

    //Audio
    @POST("/v4/shayari/GetAudioListByPoetIdWithPaging")
    void getAudioListByPoetIdWithPaging(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("poetId") String poetId, @Query("keyword") String keyword, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

    //video
    @POST("/v4/shayari/GetVideoListByPoetIdWithPaging")
    void getVideoListByPoetIdWithPaging(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("poetId") String poetId, @Query("keyword") String keyword, @Query("pageIndex") int pageIndex, Callback<HashMap> callback);

   // @POST("/v4/shayari/GetHomePageCollection/")
  //  void getHomeData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken, @Query("lang") String lang, @Query("lastFetchDate") String date, @Body HashMap obj, Callback<HomeMainPojo> callback);

    @POST("/v4/shayari/GetFavoriteListWithPaging/?keyword=")
    void getFavData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken, @Header("Authorization")
            String accesstoken, @Query("pageIndex") String lang, Callback<OnlineFavModelMain> callback);

    @POST("/v4/shayari/GetContentTypeList?lastFetchDate=")
    void getContentTypeIds(@Header("Content-Type") String contentType,
                           @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken,
                           @Query("lastFetchDate") String data, Callback<ContentTypePojo> callback);

    @POST("/v4/shayari/GetUserHistory")
    void getHistoryData(@Header("Content-Type") String contentType,
                        @Header("tempToken") String tempToken, @Header("Authorization") String accesstoken,
                        Callback<HistoryMainPojo> callback);

    @POST("/v4/shayari/GetHomePageCollection/")
    Observable<HomeMainPojo> getHomeData(@Header("Content-Type") String contentType, @Header("tempToken") String tempToken,
                                      @Header("Authorization") String accesstoken, @Query("lang") String lang,
                                      @Query("lastFetchDate") String date, @Body HashMap obj);
}

