package org.Rekhta.adapters.poetDetail;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.poetDetailledFragments.PoetSherFragment;
import org.Rekhta.fragments.sherFragments.CollectionSherView;
import org.Rekhta.model.LocalModels.AddSher;
import org.Rekhta.model.poetDetail.PoetGhazalData;
import org.Rekhta.model.poetDetail.PoetSherData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MyApplicationClass;
import org.json.JSONArray;
import org.json.JSONException;

import static android.content.Context.CLIPBOARD_SERVICE;


public class PoetSherAdapter extends RecyclerView.Adapter<PoetSherAdapter.RecyclerViewHolder> {

    private ArrayList<PoetSherData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfMItalic;
    Typeface engTfMBold;
    Typeface hinditf;
    Typeface urdutf;

    AppDatabase appDatabase;

    public PoetSherAdapter(ArrayList<PoetSherData> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        engTfMItalic = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_light_italic.ttf");
        engTfMBold = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_bold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

        appDatabase = MyApplicationClass.getDb();
    }

    @Override
    public PoetSherAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_detailed_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collectionheader_recyl_item, parent, false);
        }
        return new PoetSherAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (position == 0) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.default_profile);
            requestOptions.error(R.drawable.default_profile);
            Glide.with(context).setDefaultRequestOptions(requestOptions).load("https://rekhtacdn.azureedge.net/Images/Shayar/" + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.headerPoetImg);

            holder.headerPoetTenure.setText(((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfBirth() + " - " + (((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfDeath() + " |"));
            holder.headerTypeOfScreen.setText(" " + LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode).toUpperCase() + " ");
            holder.headerTypeOfScreenItemCount.setText(arrayList.size() + "");
            if (CommonUtil.languageCode == 1) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInEng());
                String desText = ((PoetDetailedActivity) context).poetShortDescInEng;
                if (desText.equalsIgnoreCase("null") || desText.equalsIgnoreCase("") || desText == null) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInEng);
                }
            } else if (CommonUtil.languageCode == 2) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInHin());
                if (((PoetDetailedActivity) context).poetShortDescInHin.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInHin);
                }
            } else {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInUrdu());
                if (((PoetDetailedActivity) context).poetShortDescInUrdu.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInUrdu);
                }
            }


        } else {
            final PoetSherData poetSherData = arrayList.get(position - 1);


            if (position % 2 == 0) {
//                holder.mainViewTemplate.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mainViewTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
                }
            } else {
//                holder.mainViewTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mainViewTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }


            if (CommonUtil.languageCode == 1) {
                String fdfdf = poetSherData.getSherTitleInEng();
                holder.ghazalTitle.setText(poetSherData.getSherTitleInEng());
                holder.poetName.setText(poetSherData.getSherPoetNameInEng());

                CommonUtil.setEnglishMerriwetherFont(context, holder.ghazalTitle);
                CommonUtil.setEnglishMerriweatherBoldFont(context, holder.poetName);

                // holder.ghazalTitle.setTypeface(engTfMItalic);
                // holder.poetName.setTypeface(engTfMBold);

            } else if (CommonUtil.languageCode == 2) {
                String fdfdf = poetSherData.getSherTitleInHin();

                holder.ghazalTitle.setText(poetSherData.getSherTitleInHin());
                holder.poetName.setText(poetSherData.getSherPoetNameInHin());
                CommonUtil.setHindiLailaRegular(context, holder.ghazalTitle);
                CommonUtil.setHindiFont(context, holder.poetName);
            } else if (CommonUtil.languageCode == 3) {
                String fdfdf = poetSherData.getSherTitleInHin();

                holder.ghazalTitle.setText(poetSherData.getSherTitleInUrdu());
                holder.poetName.setText(poetSherData.getSherPoetNameInUrdu());
                CommonUtil.setUrduNotoNataliq(context, holder.ghazalTitle);
                CommonUtil.setUrduNotoNataliq(context, holder.poetName);
            } else {
                holder.ghazalTitle.setText(poetSherData.getSherTitleInEng());
                holder.poetName.setText(poetSherData.getSherPoetNameInEng());
                // holder.ghazalTitle.setTypeface(engTfMItalic);
                //holder.ghazalTitle.setTypeface(engTfMBold);

            }

            if (!CommonUtil.isSkipped) {
                if (CommonUtil.isContentUsersFav(poetSherData.getSherId())) {
                    holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorited);
                    holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                } else {
                    holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorite);
                    holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                }
            } else {
                holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorite);
                holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
            }


            holder.ghazalFavIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommonUtil.isContentUsersFav(poetSherData.getSherId())) {
                        CommonUtil.removeFromFavTroughApi(context, poetSherData.getSherId());
                        holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorite);
                        holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                    } else {

                        String sherContent = null, poetName = null;

                        if (CommonUtil.languageCode == 1) {

                            sherContent = poetSherData.getSherTitleInEng();
                            poetName = poetSherData.getSherPoetNameInEng();
                        } else if (CommonUtil.languageCode == 2) {

                            sherContent = poetSherData.getSherTitleInHin();
                            poetName = poetSherData.getSherPoetNameInHin();
                        } else if (CommonUtil.languageCode == 3) {

                            sherContent = poetSherData.getSherTitleInUrdu();
                            poetName = poetSherData.getSherPoetNameInUrdu();
                        }

                        final AddSher addSher = new AddSher();
                        addSher.setSher_content(sherContent);
                        addSher.setSher_auther(poetName);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                appDatabase.sherDao().insert(addSher);
                            }
                        }).start();

                        CommonUtil.addToFavTroughApi(context, poetSherData.getSherId());
                        holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorited);
                        holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                    }
                }
            });
/*        // for EditorChoice Icon Logic
        if (poetSherData.isEditorChoice()) {
            holder.ghazaleditorChoiceIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ghazaleditorChoiceIcon.setVisibility(View.GONE);
        }

        //popular Choice
        if (poetSherData.isPopularChoice()) {
            holder.ghazalPopularChoiceIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ghazalPopularChoiceIcon.setVisibility(View.GONE);
        }*/


 /*       //for Audio Icon
        if (poetSherData.isAudioAvailable()) {
            holder.ghazalAudioIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ghazalAudioIcon.setVisibility(View.GONE);
        }

        //For Video Icon
        if (poetSherData.isVideoAvailable()) {
            holder.ghazalVideoIcon.setVisibility(View.VISIBLE);
        } else {
            holder.ghazalVideoIcon.setVisibility(View.GONE);
        }
        */
            holder.tagContainer.setVisibility(View.GONE);
            holder.ghazalShareIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(context);
                    } else {
                        if (CommonUtil.languageCode == 1) {
                            shareTheUrl(poetSherData.getSherShareLinkInEng());
                        }
                        if (CommonUtil.languageCode == 2) {
                            shareTheUrl(poetSherData.getSherShareLinkInHin());
                        }
                        if (CommonUtil.languageCode == 3) {
                            shareTheUrl(poetSherData.getSherShareLinkInUrdu());
                        }
                    }
                }
            });
            holder.sher_clipbordIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommonUtil.languageCode == 1) {
                        CopyToClipBoard(poetSherData.getSherTitleInEng());
                    }
                    if (CommonUtil.languageCode == 2) {
                        CopyToClipBoard(poetSherData.getSherTitleInEng());
                    }
                    if (CommonUtil.languageCode == 3) {
                        CopyToClipBoard(poetSherData.getSherTitleInEng());
                    }

                }
            });

            int totalTags = poetSherData.getTagArrayLength();
            String firstTagName = getTagName(poetSherData.getSherTagArray(), CommonUtil.languageCode, 0);
            final JSONArray tagArray = poetSherData.getSherTagArray();

            if (totalTags == 0) {
                holder.sherFirstTagView.setVisibility(View.GONE);
                holder.sherFirstTagView.setVisibility(View.GONE);
            } else {


                holder.sherFirstTagView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            ((PoetDetailedActivity) context).lastRenderSherName.add(CommonUtil.cSherTypeNameEnglish);
                            showDetailSherPage(getTagName(poetSherData.getSherTagArray(), CommonUtil.languageCode, 0), tagArray.getJSONObject(0).getString("I"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }



            holder.sher_translateIcon.setVisibility(View.GONE);
        }
    }

    public String getTagName(JSONArray array, int langcode, int position) {
        String str = "";
        if (array.length() != 0) {
            try {
                if (langcode == 1) {
                    str = array.getJSONObject(position).getString("NE");
                }
                if (langcode == 2) {
                    str = array.getJSONObject(position).getString("NH");
                }
                if (langcode == 3) {
                    str = array.getJSONObject(position).getString("NU");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;

    }

    private void showDetailSherPage(String TagName, String id) {


        Bundle bundle = new Bundle();
        bundle.putString("I", id);
        ((SherActivity) context).idsRendered.add(((SherActivity) context).currentSherID);
        ((SherActivity) context).currentSherID = id;
        CommonUtil.cSherTypeNameEnglish = TagName;
        CommonUtil.hasCameFromTag = true;
        CollectionSherView collectionSherView = new CollectionSherView();
        collectionSherView.setArguments(bundle);
        ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                .replace(R.id.drawer_layout, collectionSherView, "collectionFrag")
                .addToBackStack(null)
                .commit();

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    public void CopyToClipBoard(String sherContetText) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Sher", sherContetText);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    public void shareTheUrl(String urlLink) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "https://Rekhta.org");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, urlLink);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView ghazalFavIcon;
        TextView ghazalTitle;
        TextView poetName;
        // ImageView ghazaleditorChoiceIcon;
        ImageView sher_clipbordIcon;
        ImageView sher_translateIcon;
        ImageView sherTranslateIcon;
        ImageView ghazalShareIcon;
        LinearLayout mainViewTemplate;
        LinearLayout tagContainer;

        //header
        ImageView headerPoetImg;
        TextView headerPoetName;
        TextView headerPoetTenure;
        TextView headerPoetBirthPlace;
        TextView headerPoetDescription;
        TextView headerTypeOfScreen;
        TextView headerTypeOfScreenItemCount;
        TextView sherFirstTagView;


        public RecyclerViewHolder(View view) {
            super(view);
            ghazalTitle = (TextView) view.findViewById(R.id.textsher);
            poetName = (TextView) view.findViewById(R.id.sher_poet_name);

            ghazalFavIcon = (ImageView) view.findViewById(R.id.sher_heartIcon);
            // ghazaleditorChoiceIcon = (ImageView) view.findViewById(R.id.poetTemplateEditorChoiceIcon);
            sher_translateIcon = (ImageView) view.findViewById(R.id.sher_translateIcon);
            sher_clipbordIcon = (ImageView) view.findViewById(R.id.sher_clipbordIcon);
            sherTranslateIcon = (ImageView) view.findViewById(R.id.sher_translateIcon);
            ghazalShareIcon = (ImageView) view.findViewById(R.id.sher_shareIcon);
            mainViewTemplate = (LinearLayout) view.findViewById(R.id.sherTemplatemainview);
            tagContainer = (LinearLayout) view.findViewById(R.id.tagContainer);

            //header
            headerPoetImg = (ImageView) view.findViewById(R.id.poetProfileImageTemplate);
            headerPoetName = (TextView) view.findViewById(R.id.poetNameTemplateTV);
            headerPoetTenure = (TextView) view.findViewById(R.id.poetTenureTemplateTV);
            headerPoetBirthPlace = (TextView) view.findViewById(R.id.poetBirthPlaceTemplate);
            headerPoetDescription = (TextView) view.findViewById(R.id.poetDescriptionTemplate);
            headerTypeOfScreen = (TextView) view.findViewById(R.id.poetTypeOfScreenTemplate);
            headerTypeOfScreenItemCount = (TextView) view.findViewById(R.id.poetCountTemplate);
            sherFirstTagView = (TextView) view.findViewById(R.id.sherFirstTagView);


        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
