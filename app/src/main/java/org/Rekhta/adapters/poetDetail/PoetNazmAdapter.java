package org.Rekhta.adapters.poetDetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.Services.NazamContentService;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.poetDetailledFragments.GhazalAndNazmContent;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.model.poetDetail.PoetNazmData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MyApplicationClass;


public class PoetNazmAdapter extends RecyclerView.Adapter<PoetNazmAdapter.RecyclerViewHolder> {

    private ArrayList<PoetNazmData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;
    AppDatabase db;
    int totalNumberOfContent;


    public PoetNazmAdapter(ArrayList<PoetNazmData> arrayList, Context context, int totalNumberOfContent) {
        this.arrayList = arrayList;
        this.context = context;

        this.totalNumberOfContent = totalNumberOfContent;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

        db = MyApplicationClass.getDb();
    }

    @Override
    public PoetNazmAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_detailed_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_poet_nazm, parent, false);
        }
        return new PoetNazmAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (position == 0) {
            //   holder.headerPoetImg.setImageResource(R.drawable.bg);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.default_profile);
            requestOptions.error(R.drawable.default_profile);
            Glide.with(context).setDefaultRequestOptions(requestOptions).load("https://rekhtacdn.azureedge.net/Images/Shayar/" + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.headerPoetImg);

            holder.headerPoetTenure.setText(((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfBirth() + " - " + (((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfDeath() + " |"));
            holder.headerTypeOfScreen.setText(" " + LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode).toUpperCase() + " ");
            holder.headerTypeOfScreenItemCount.setText(totalNumberOfContent + "");
            if (CommonUtil.languageCode == 1) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInEng());
                String desText = ((PoetDetailedActivity) context).poetShortDescInEng;
                if (desText.equalsIgnoreCase("null") || desText.equalsIgnoreCase("") || desText == null) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInEng);
                }
            } else if (CommonUtil.languageCode == 2) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInHin());
                if (((PoetDetailedActivity) context).poetShortDescInHin.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInHin);
                }
            } else {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInUrdu());
                if (((PoetDetailedActivity) context).poetShortDescInUrdu.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInUrdu);
                }
            }

        } else {
            final PoetNazmData poetNazmData = arrayList.get(position - 1);

            holder.MainViewTemplate.setBackgroundColor(Color.WHITE);
            if (position % 2 == 0) {
//                holder.MainViewTemplate.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.MainViewTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
                }
            } else {
//                holder.MainViewTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.MainViewTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }


            if (CommonUtil.languageCode == 1) {
                holder.poetNazmTitle.setText(poetNazmData.getNazmTitleInEng());
                holder.poetNazmSeries.setText(poetNazmData.getNazmSeriesInEng());

                CommonUtil.setEnglishLatoXBold(context, holder.poetNazmTitle);
                CommonUtil.setEnglishLatoXRegualr(context, holder.poetNazmSeries);

            } else if (CommonUtil.languageCode == 2) {
                holder.poetNazmTitle.setText(poetNazmData.getNazmTitleInHin());
                holder.poetNazmSeries.setText(poetNazmData.getNazmSeriesInHin());

                CommonUtil.setHindiLailaRegular(context, holder.poetNazmTitle);

                CommonUtil.setHindiLailaRegular(context, holder.poetNazmSeries);
            } else if (CommonUtil.languageCode == 3) {
                holder.poetNazmTitle.setText(poetNazmData.getNazmTitleInUrdu());
                holder.poetNazmSeries.setText(poetNazmData.getNazmSeriesInUrdu());


                CommonUtil.setUrduNotoNataliq(context, holder.poetNazmTitle);

                CommonUtil.setUrduNotoNataliq(context, holder.poetNazmSeries);
            } else {
                holder.poetNazmTitle.setText(poetNazmData.getNazmTitleInEng());
                holder.poetNazmSeries.setText(poetNazmData.getNazmSeriesInEng());

                CommonUtil.setEnglishLatoXBold(context, holder.poetNazmTitle);

                CommonUtil.setEnglishLatoXRegualr(context, holder.poetNazmSeries);

            }

            if (!CommonUtil.isSkipped) {
                if (CommonUtil.isContentUsersFav(poetNazmData.getNazmId())) {
                    holder.poetNazmFavIcon.setImageResource(R.drawable.ic_favorited);
                    holder.poetNazmFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                } else {
                    holder.poetNazmFavIcon.setImageResource(R.drawable.ic_favorite);
                    holder.poetNazmFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                }
            } else {
                holder.poetNazmFavIcon.setImageResource(R.drawable.ic_favorite);
                holder.poetNazmFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

            }

            holder.poetNazmFavIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(context);
                    } else {
                        if (CommonUtil.isContentUsersFav(poetNazmData.getNazmId())) {
                            CommonUtil.removeFromFavTroughApi(context, poetNazmData.getNazmId());
                            holder.poetNazmFavIcon.setImageResource(R.drawable.ic_favorite);
                            holder.poetNazmFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                        } else {

                            CommonUtil.addToFavTroughApi(context, poetNazmData.getNazmId());

                            final AddNazam addNazam = new AddNazam();

                            holder.poetNazmFavIcon.setImageResource(R.drawable.ic_favorited);
                            holder.poetNazmFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);


                            String titile, seriesName, autherName;
                            if (CommonUtil.languageCode == 1) {
                                titile = poetNazmData.getNazmTitleInEng();
                                seriesName = poetNazmData.getNazmSeriesInEng();
                                autherName = ((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng();

                            } else if (CommonUtil.languageCode == 2) {
                                titile = poetNazmData.getNazmTitleInHin();
                                seriesName = poetNazmData.getNazmSeriesInHin();
                                autherName = ((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin();
                            } else if (CommonUtil.languageCode == 3) {
                                titile = poetNazmData.getNazmTitleInUrdu();
                                seriesName = poetNazmData.getNazmSeriesInUrdu();
                                autherName = ((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu();
                            } else {
                                titile = poetNazmData.getNazmTitleInEng();
                                seriesName = poetNazmData.getNazmSeriesInEng();
                                autherName = ((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng();

                            }

                            Intent intent = new Intent(context, NazamContentService.class);
                            intent.putExtra("title", titile);
                            intent.putExtra("content", seriesName);
                            intent.putExtra("autherName", autherName);
                            intent.putExtra("Ivalue", poetNazmData.getNazmId());
                            context.startService(intent);

                        }
                    }


                }
            });
            // for EditorChoice Icon Logic
            if (poetNazmData.isEditorChoice())

            {
                holder.poetNazmeditorChoiceIcon.setVisibility(View.VISIBLE);
            } else

            {
                holder.poetNazmeditorChoiceIcon.setVisibility(View.GONE);
            }

            //popular Choice
            if (poetNazmData.isPopularChoice())

            {
                holder.poetNazmPopularChoiceIcon.setVisibility(View.VISIBLE);
            } else

            {
                holder.poetNazmPopularChoiceIcon.setVisibility(View.GONE);
            }


            //for Audio Icon
            if (poetNazmData.isAudioAvailable())

            {
                holder.poetNazmAudioIcon.setVisibility(View.VISIBLE);
            } else

            {
                holder.poetNazmAudioIcon.setVisibility(View.GONE);
            }

            //For Video Icon
            if (poetNazmData.isVideoAvailable())

            {
                holder.poetNazmVideoIcon.setVisibility(View.VISIBLE);
            } else

            {
                holder.poetNazmVideoIcon.setVisibility(View.GONE);
            }

            holder.MainViewTemplate.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick(View view) {
                    GhazalAndNazmContent ghazalAndNazmContent = new GhazalAndNazmContent();
                    ((PoetDetailedActivity) context).idGhazalToRender = poetNazmData.getNazmId();

                    CommonUtil.gType = "nazam";
                    ((PoetDetailedActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.drawer_layout, ghazalAndNazmContent, "ghazalAndNazmContent")
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView poetNazmFavIcon;
        TextView poetNazmTitle;
        TextView poetNazmSeries;
        ImageView poetNazmeditorChoiceIcon;
        ImageView poetNazmPopularChoiceIcon;
        ImageView poetNazmAudioIcon;
        ImageView poetNazmVideoIcon;
        RelativeLayout MainViewTemplate;


        //header
        ImageView headerPoetImg;
        TextView headerPoetName;
        TextView headerPoetTenure;
        TextView headerPoetBirthPlace;
        TextView headerPoetDescription;
        TextView headerTypeOfScreen;
        TextView headerTypeOfScreenItemCount;


        public RecyclerViewHolder(View view) {
            super(view);
            poetNazmTitle = (TextView) view.findViewById(R.id.poetNazmTitle);
            poetNazmSeries = (TextView) view.findViewById(R.id.poetNazmSeriesName);

            poetNazmFavIcon = (ImageView) view.findViewById(R.id.poetNazmaddToFav);
            poetNazmeditorChoiceIcon = (ImageView) view.findViewById(R.id.poetNazmeditorchoice);
            poetNazmPopularChoiceIcon = (ImageView) view.findViewById(R.id.poetNazmpopularchoice);
            poetNazmAudioIcon = (ImageView) view.findViewById(R.id.poetNazmaudioLink);
            poetNazmVideoIcon = (ImageView) view.findViewById(R.id.poetNazmyoutubeLink);
            MainViewTemplate = (RelativeLayout) view.findViewById(R.id.top20sher_maintemp_layout);

            //header
            headerPoetImg = (ImageView) view.findViewById(R.id.poetProfileImageTemplate);
            headerPoetName = (TextView) view.findViewById(R.id.poetNameTemplateTV);
            headerPoetTenure = (TextView) view.findViewById(R.id.poetTenureTemplateTV);
            headerPoetBirthPlace = (TextView) view.findViewById(R.id.poetBirthPlaceTemplate);
            headerPoetDescription = (TextView) view.findViewById(R.id.poetDescriptionTemplate);
            headerTypeOfScreen = (TextView) view.findViewById(R.id.poetTypeOfScreenTemplate);
            headerTypeOfScreenItemCount = (TextView) view.findViewById(R.id.poetCountTemplate);

        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
