package org.Rekhta.adapters.poetDetail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeApiServiceUtil;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.fragments.poetDetailledFragments.PoetVideoFragment;
import org.Rekhta.model.poetDetail.PoetVideoData;
import org.Rekhta.utils.CommonUtil;


public class PoetVideoAdapter extends RecyclerView.Adapter<PoetVideoAdapter.RecyclerViewHolder> {

    private ArrayList<PoetVideoData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;
    private boolean readyForLoadingYoutubeThumbnail = true;
    YouTubePlayerSupportFragment youTubePlayerFragment;

    int totalNumberOfContent;
    PoetVideoFragment poetVideoFragment;

    public PoetVideoAdapter(ArrayList<PoetVideoData> arrayList, Context context, int totalNumberOfContent, PoetVideoFragment poetVideoFragment) {
        this.arrayList = arrayList;
        this.context = context;
        this.poetVideoFragment = poetVideoFragment;
        this.totalNumberOfContent = totalNumberOfContent;
        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

    }

    @Override
    public PoetVideoAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_detailed_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_video_template, parent, false);
        }
        return new PoetVideoAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PoetVideoAdapter.RecyclerViewHolder holder, final int position) {


        if (position == 0) {

            Glide.with(context).load("https://rekhtacdn.azureedge.net/Images/Shayar/" + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.headerPoetImg);

            holder.headerPoetTenure.setText(((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfBirth() + " - " + (((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfDeath() + " |"));
            holder.headerTypeOfScreen.setText("VIDEO ");
            holder.headerTypeOfScreenItemCount.setText(totalNumberOfContent + "");
            if (CommonUtil.languageCode == 1) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInEng());
                String desText = ((PoetDetailedActivity) context).poetShortDescInEng;
                if (desText.equalsIgnoreCase("null") || desText.equalsIgnoreCase("") || desText == null) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInEng);
                }
            } else if (CommonUtil.languageCode == 2) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInHin());
                if (((PoetDetailedActivity) context).poetShortDescInHin.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInHin);
                }
            } else {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInUrdu());
                if (((PoetDetailedActivity) context).poetShortDescInUrdu.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInUrdu);
                }
            }

        } else {
            final PoetVideoData poetVideoData = arrayList.get(position - 1);


            if (position % 2 == 0) {
                holder.mainRelative.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mainRelative.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
                }
            } else {
                holder.mainRelative.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mainRelative.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }

            holder.total_viewer.setText(String.valueOf(poetVideoData.getTotalCount()));

            if (CommonUtil.languageCode == 1) {
                String vedioTitle = poetVideoData.getVideoTitleInEng().trim();
                String singerName = poetVideoData.getVideoAE().trim();
                holder.videosTitleTextView.setText(vedioTitle + " - " + singerName);
                // holder.videosTitleTextView.setTypeface(engTfLatoX);
            }
            if (CommonUtil.languageCode == 2) {

                String vedioTitle = poetVideoData.getVideoTitleInHin().trim();
                String singerName = poetVideoData.getVideoAH().trim();
                holder.videosTitleTextView.setText(vedioTitle + " - " + singerName);
                //holder.videosTitleTextView.setTypeface(hinditf);
            }
            if (CommonUtil.languageCode == 3) {
                String vedioTitle = poetVideoData.getVideoTitleInUrdu().trim();
                String singerName = poetVideoData.getVideoAU().trim();
                holder.videosTitleTextView.setText(vedioTitle + " - " + singerName);
            }

            holder.relativeLayoutOverYouTubeThumbnailView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(context).equals(YouTubeInitializationResult.SUCCESS)) {
                        //This means that your device has the Youtube API Service (the app) and you are safe to launch it.
                        Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context, "AIzaSyAucCMhVmsY5JZRRl2JPsvRdnWeThfOHx8", poetVideoData.getVideoYoutubeId(), 500, true, false);

                        poetVideoFragment.checkOrientaiotn(intent);


                    } else {
                        // Log the outcome, take necessary measure, like playing the video in webview :)
                        Toast.makeText(context, "Please install Youtube player in your device to play this video", Toast.LENGTH_LONG).show();
                    }

                }
            });

            final YouTubeThumbnailLoader.OnThumbnailLoadedListener onThumbnailLoadedListener = new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                @Override
                public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                    Log.e("YouTube Error", errorReason.toString());
                }

                @Override
                public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                    youTubeThumbnailView.setVisibility(View.VISIBLE);
                    holder.relativeLayoutOverYouTubeThumbnailView.setVisibility(View.VISIBLE);
                }
            };

            try {
                if (readyForLoadingYoutubeThumbnail) {

                    holder.youTubeThumbnailView.initialize("AIzaSyAucCMhVmsY5JZRRl2JPsvRdnWeThfOHx8", new YouTubeThumbnailView.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                            youTubeThumbnailLoader.setVideo(poetVideoData.getVideoYoutubeId());
                            youTubeThumbnailLoader.setOnThumbnailLoadedListener(onThumbnailLoadedListener);
                            readyForLoadingYoutubeThumbnail = false;

                        }

                        @Override
                        public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                            //write something for failure
                        }
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.mainRelative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (YouTubeApiServiceUtil.isYouTubeApiServiceAvailable(context).equals(YouTubeInitializationResult.SUCCESS)) {
                        //This means that your device has the Youtube API Service (the app) and you are safe to launch it.
                        Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context, "AIzaSyAucCMhVmsY5JZRRl2JPsvRdnWeThfOHx8", poetVideoData.getVideoYoutubeId(), 500, true, false);

                        poetVideoFragment.checkOrientaiotn(intent);


                    } else {
                        // Log the outcome, take necessary measure, like playing the video in webview :)
                        Toast.makeText(context, "Please install Youtube player in your device to play this video", Toast.LENGTH_LONG).show();
                    }


                }
            });
        }
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        //new demo
        protected RelativeLayout relativeLayoutOverYouTubeThumbnailView;
        YouTubeThumbnailView youTubeThumbnailView;
        protected ImageView playButton;
        protected TextView videosTitleTextView;
        private LinearLayout mainRelative;
        protected TextView total_viewer;


        //header
        ImageView headerPoetImg;
        TextView headerPoetName;
        TextView headerPoetTenure;
        TextView headerPoetBirthPlace;
        TextView headerPoetDescription;
        TextView headerTypeOfScreen;
        TextView headerTypeOfScreenItemCount;
        YouTubePlayer player;

        public RecyclerViewHolder(View view) {
            super(view);

            //new
            playButton = (ImageView) view.findViewById(R.id.btnYoutube_playerlk);
            videosTitleTextView = (TextView) view.findViewById(R.id.tv_discrpition);
            total_viewer = (TextView) view.findViewById(R.id.total_viewer);

            relativeLayoutOverYouTubeThumbnailView = (RelativeLayout) view.findViewById(R.id.relativeLayout_over_youtube_thumbnail);
            youTubeThumbnailView = (YouTubeThumbnailView) view.findViewById(R.id.youtube_thumbnail);
            mainRelative = (LinearLayout) view.findViewById(R.id.parent_relativeLayout);

            //header
            headerPoetImg = (ImageView) view.findViewById(R.id.poetProfileImageTemplate);
            headerPoetName = (TextView) view.findViewById(R.id.poetNameTemplateTV);
            headerPoetTenure = (TextView) view.findViewById(R.id.poetTenureTemplateTV);
            headerPoetBirthPlace = (TextView) view.findViewById(R.id.poetBirthPlaceTemplate);
            headerPoetDescription = (TextView) view.findViewById(R.id.poetDescriptionTemplate);
            headerTypeOfScreen = (TextView) view.findViewById(R.id.poetTypeOfScreenTemplate);
            headerTypeOfScreenItemCount = (TextView) view.findViewById(R.id.poetCountTemplate);
        }


    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }

}
