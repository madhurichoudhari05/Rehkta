package org.Rekhta.adapters.poetDetail;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import org.json.JSONObject;

import java.util.ArrayList;

import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.fragments.poetDetailledFragments.PoetAudioFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetGhazalFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetNazmFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetProfileFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetSherFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetVideoFragment;

public class PoetDetailedViewPagerAdapter extends FragmentStatePagerAdapter {

    Context context;
    private ArrayList<Fragment> data;
    private ArrayList<String> tabName;


    public PoetDetailedViewPagerAdapter(FragmentManager fm, Context context, ArrayList<Fragment> data, ArrayList<String> tabName) {
        super(fm);
        this.data = data;
        this.tabName = tabName;
    }

    @Override

    public Fragment getItem(int position) {

        Fragment f = null;

       /* if (position == 0) {
            f = new PoetProfileFragment();
        } else if (position == 1) {
            f = new PoetGhazalFragment();
        } else if (position == 2) {
            f = new PoetNazmFragment();
        } else if (position == 3) {
            f = new PoetSherFragment();
        } else if (position == 4) {
            f = new PoetAudioFragment();
        } else if (position == 5) {
            f = new PoetVideoFragment();
        }*/
        return data.get(position);
        //return f;
    }


    @Override
    public int getCount() {

        return data.size();

    }

    @Override
    public CharSequence getPageTitle(int position) {
    /*    if(position == 0) {
            return  "PROFILE";
        }else
        if(position == 1) {
            return  "GHAZAL";
        }else
        if(position == 2) {
            return  "NAZM";
        }else
        if(position == 3) {
           return "SHER";
        }else
        if(position ==4) {
            return  "AUDIO";
        }else
        if(position == 5) {
           return "VIDEO";
        }
        return super.getPageTitle(position);*/
        return tabName.get(position);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
}
