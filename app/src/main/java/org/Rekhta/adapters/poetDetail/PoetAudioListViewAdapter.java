package org.Rekhta.adapters.poetDetail;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.model.poetDetail.PoetAudioData;
import org.Rekhta.utils.CommonUtil;


public class PoetAudioListViewAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PoetAudioData> arrayList;



    public PoetAudioListViewAdapter(Context c, ArrayList<PoetAudioData> list) {
        mContext = c;
        arrayList = list;

    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        ArrayList<PoetAudioData> obj = arrayList;

        return obj;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        View view = null;
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            PoetAudioData poetAudioData = arrayList.get(position);
            final ViewHolder mHolder = new ViewHolder();
            if (view == null) {

                view = inflater.inflate(R.layout.poet_audiolist_musicplayer, null);
                view.setTag(mHolder);
            } else {
                view = (View) view.getTag();
            }
       /*     if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.parseColor("#eeeeee"));
            }*/
            mHolder.audioTitle = (TextView) view.findViewById(R.id.audioTitltMp);
            mHolder.audioAuthor = (TextView) view.findViewById(R.id.audioSingerMP);

            if (CommonUtil.languageCode == 1) {
                mHolder.audioTitle.setText(poetAudioData.getAudioTitleInEng());
                mHolder.audioAuthor.setText(poetAudioData.getAudioPoetNameinEng());


            } else if (CommonUtil.languageCode == 2) {
                mHolder.audioTitle.setText(poetAudioData.getAudioTitleInHin());
                mHolder.audioAuthor.setText(poetAudioData.getAudioPoetNameInHin());

            } else {
                mHolder.audioTitle.setText(poetAudioData.getAudioTitleInUrdu());
                mHolder.audioAuthor.setText(poetAudioData.getAudioPoetNameInHin());
            }

            mHolder.playIcon = (ImageView) view.findViewById(R.id.playIconMp);







        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;

    }

    private class ViewHolder {
        private TextView audioTitle;
        private TextView audioAuthor;
        private ImageView playIcon;

    }


}


