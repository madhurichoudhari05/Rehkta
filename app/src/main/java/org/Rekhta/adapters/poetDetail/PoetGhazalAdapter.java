package org.Rekhta.adapters.poetDetail;

import android.content.Context;
import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.Services.GhazalContentService;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.poetDetailledFragments.GhazalAndNazmContent;
import org.Rekhta.model.poetDetail.PoetGhazalData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MyApplicationClass;


public class PoetGhazalAdapter extends RecyclerView.Adapter<PoetGhazalAdapter.RecyclerViewHolder> {

    private ArrayList<PoetGhazalData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();


    AppDatabase appDatabase;
    int totalNumberOfContent;

    public PoetGhazalAdapter(ArrayList<PoetGhazalData> arrayList, Context context, int totalNumberOfContent) {
        this.arrayList = arrayList;
        this.context = context;

        appDatabase = MyApplicationClass.getDb();
        this.totalNumberOfContent = totalNumberOfContent;

    }

    @Override
    public PoetGhazalAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_detailed_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_listview_template, parent, false);
        }
        return new PoetGhazalAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        if (position == 0) {

            Glide.with(context).load("https://rekhtacdn.azureedge.net/Images/Shayar/" + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.headerPoetImg);
            holder.headerPoetTenure.setText(((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfBirth() + " - " + (((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfDeath() + " |"));
            holder.headerTypeOfScreen.setText(" " + LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode).toUpperCase() + " ");
            holder.headerTypeOfScreenItemCount.setText(totalNumberOfContent + "");


            if (CommonUtil.languageCode == 1) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInEng());
                String desText = ((PoetDetailedActivity) context).poetShortDescInEng;
                Log.v("DesText", desText);
                if (desText.equalsIgnoreCase("null") || desText.equalsIgnoreCase("") || desText == null) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInEng);
                }

            } else if (CommonUtil.languageCode == 2) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInHin());
                if (((PoetDetailedActivity) context).poetShortDescInHin.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInHin);
                }

            } else {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInUrdu());
                if (((PoetDetailedActivity) context).poetShortDescInUrdu.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInUrdu);
                }
            }


        } else {
            final PoetGhazalData poetGhazalData = arrayList.get(position - 1);

            holder.ghazalMainView.setBackgroundColor(Color.WHITE);
            if (position % 2 == 0) {
//                holder.ghazalMainView.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.ghazalMainView.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
                }
            } else {
//                holder.ghazalMainView.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.ghazalMainView.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }

            if (CommonUtil.languageCode == 1) {
                String ghghg = poetGhazalData.getPoetsGhazalTiltleInEng().trim();
                if (ghghg.length() >= 30) {
                    String dd2 = ghghg.substring(0, ghghg.length() - 2).trim();
                    String dd3 = ghghg.substring(ghghg.length() - 2, ghghg.length()).trim();
                    holder.ghazalTitle2.setVisibility(View.VISIBLE);
                    holder.ghazalTitle.setText(dd2);
                    holder.ghazalTitle2.setText(dd3);

                    CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalTitle);
                    CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalTitle2);


                    makeTextBlur(holder.ghazalTitle2);

                } else {
                    holder.ghazalTitle.setText(ghghg);
                    holder.ghazalTitle2.setVisibility(View.GONE);
                    CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalTitle);
                    CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalTitle2);


                }

                // holder.ghazalTitle.setTypeface(engTfLatoBold);

            } else if (CommonUtil.languageCode == 2) {

                String ghghg = poetGhazalData.getPoetsGhazalTiltleInHin().trim();
                if (ghghg.length() >= 20) {
                    String dd2 = ghghg.substring(0, ghghg.length() - 2);
                    String dd3 = ghghg.substring(ghghg.length() - 2, ghghg.length());
                    holder.ghazalTitle2.setVisibility(View.VISIBLE);
                    holder.ghazalTitle.setText(dd2.trim());
                    holder.ghazalTitle2.setText(dd3.trim());

                    makeTextBlur(holder.ghazalTitle2);

                    CommonUtil.setHindiLailaRegular(context, holder.ghazalTitle);
                    CommonUtil.setHindiLailaRegular(context, holder.ghazalTitle2);


                } else {
                    holder.ghazalTitle.setText(ghghg);
                    holder.ghazalTitle2.setVisibility(View.GONE);

                    CommonUtil.setHindiLailaRegular(context, holder.ghazalTitle);
                }
                // holder.ghazalTitle.setTypeface(hinditf);
            } else if (CommonUtil.languageCode == 3) {

                String ghghg = poetGhazalData.getPoetsGhazalTiltleInUrdu().trim();
                if (ghghg.length() >= 20) {
                    String dd2 = ghghg.substring(0, ghghg.length() - 2);
                    String dd3 = ghghg.substring(ghghg.length() - 2, ghghg.length());
                    holder.ghazalTitle2.setVisibility(View.VISIBLE);
                    holder.ghazalTitle.setText(dd2);
                    holder.ghazalTitle2.setText(dd3);

                    makeTextBlur(holder.ghazalTitle2);

                    CommonUtil.setUrduNotoNataliq(context, holder.ghazalTitle);
                    CommonUtil.setUrduNotoNataliq(context, holder.ghazalTitle2);


                } else {
                    holder.ghazalTitle.setText(ghghg);
                    holder.ghazalTitle2.setVisibility(View.GONE);

                    CommonUtil.setUrduNotoNataliq(context, holder.ghazalTitle);

                }

                // holder.ghazalTitle.setText(poetGhazalData.getPoetsGhazalTiltleInUrdu());
                //holder.ghazalTitle.setTypeface(urdutf);
            } else {
                holder.ghazalTitle.setText(poetGhazalData.getPoetsGhazalTiltleInEng());

                CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalTitle);
                CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalTitle2);
                CommonUtil.setEnglishLatoXRegualr(context, holder.ghazalAuthorName);

            }

            //holder.ghazalAuthorName.setVisibility(View.INVISIBLE);

            if (!CommonUtil.isSkipped) {
                if (CommonUtil.isContentUsersFav(poetGhazalData.getPoetsGhazalId())) {

                    holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorited);
                    holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                } else {

                    holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorite);
                    holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                }
            } else {
                holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorite);
                holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
            }

            holder.ghazalFavIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(context);
                    } else {
                        if (CommonUtil.isContentUsersFav(poetGhazalData.getPoetsGhazalId())) {
                            CommonUtil.removeFromFavTroughApi(context, poetGhazalData.getPoetsGhazalId());
                            holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorite);
                            holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                        } else {

                            //add to local.............................
                            String ghazalTitile;
                            if (CommonUtil.languageCode == 1) {
                                ghazalTitile = poetGhazalData.getPoetsGhazalTiltleInEng();

                            } else if (CommonUtil.languageCode == 2) {
                                ghazalTitile = poetGhazalData.getPoetsGhazalTiltleInHin();
                            } else if (CommonUtil.languageCode == 3) {
                                ghazalTitile = poetGhazalData.getPoetsGhazalTiltleInUrdu();
                            } else {
                                ghazalTitile = poetGhazalData.getPoetsGhazalTiltleInEng();

                            }

                            String ghazalId = poetGhazalData.getPoetsGhazalId();
                            Intent intent = new Intent(context, GhazalContentService.class);
                            intent.putExtra("ghazalTitle", ghazalTitile);
                            intent.putExtra("ghazalAuther", ((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());
                            intent.putExtra("jsonObject", ghazalId);

                            context.startService(intent);


                            CommonUtil.addToFavTroughApi(context, poetGhazalData.getPoetsGhazalId());
                            holder.ghazalFavIcon.setImageResource(R.drawable.ic_favorited);
                            holder.ghazalFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                        }
                    }


                }
            });
            // for EditorChoice Icon Logic
            if (poetGhazalData.isEditorChoice()) {
                holder.ghazaleditorChoiceIcon.setVisibility(View.VISIBLE);
            } else {
                holder.ghazaleditorChoiceIcon.setVisibility(View.GONE);
            }

            //popular Choice
            if (poetGhazalData.isPopularChoice()) {
                holder.ghazalPopularChoiceIcon.setVisibility(View.VISIBLE);
            } else {
                holder.ghazalPopularChoiceIcon.setVisibility(View.GONE);
            }


            //for Audio Icon
            if (poetGhazalData.isAudioAvailable()) {
                holder.ghazalAudioIcon.setVisibility(View.VISIBLE);
            } else {
                holder.ghazalAudioIcon.setVisibility(View.GONE);
            }

            //For Video Icon
            if (poetGhazalData.isVideoAvailable()) {
                holder.ghazalVideoIcon.setVisibility(View.VISIBLE);
            } else {
                holder.ghazalVideoIcon.setVisibility(View.GONE);
            }

            holder.ghazalMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GhazalAndNazmContent ghazalAndNazmContent = new GhazalAndNazmContent();
                    ((PoetDetailedActivity) context).idGhazalToRender = poetGhazalData.getPoetsGhazalId();
                    // ((PoetDetailedActivity) context).currentGhazalPoetName = poetGhazalData.get();

                    CommonUtil.gType = "ghazal";
                    ((PoetDetailedActivity) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.drawer_layout, ghazalAndNazmContent, "findThisFragment")
                            .addToBackStack(null)
                            .commit();
                }
            });
        }
    }

    private void makeTextBlur(TextView ghazalTitle) {

        if (Build.VERSION.SDK_INT >= 11) {
            ghazalTitle.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        float radius = ghazalTitle.getTextSize() / 3;
        BlurMaskFilter filter = new BlurMaskFilter(radius, BlurMaskFilter.Blur.INNER);
        ghazalTitle.getPaint().setMaskFilter(filter);
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView ghazalFavIcon;
        TextView ghazalTitle, ghazalTitle2;
        TextView ghazalAuthorName;
        ImageView ghazaleditorChoiceIcon;
        ImageView ghazalPopularChoiceIcon;
        ImageView ghazalAudioIcon;
        ImageView ghazalVideoIcon;
        //        ImageView ghazalShareIcon;
        RelativeLayout ghazalMainView;


        //header
        ImageView headerPoetImg;
        TextView headerPoetName;
        TextView headerPoetTenure;
        TextView headerPoetBirthPlace;
        TextView headerPoetDescription;
        TextView headerTypeOfScreen;
        TextView headerTypeOfScreenItemCount;

        public RecyclerViewHolder(View view) {
            super(view);

            ghazalTitle = (TextView) view.findViewById(R.id.ghazalTitle);
            ghazalTitle2 = (TextView) view.findViewById(R.id.ghazalTitle2);
            ghazalAuthorName = (TextView) view.findViewById(R.id.ghazalAuthorName);
            ghazalFavIcon = (ImageView) view.findViewById(R.id.offlineFavIcon);
            ghazaleditorChoiceIcon = (ImageView) view.findViewById(R.id.poetEditorChoiceIcon);
            ghazalPopularChoiceIcon = (ImageView) view.findViewById(R.id.poetPopularChoiceIcon);
            ghazalAudioIcon = (ImageView) view.findViewById(R.id.poetaudioLink);
            ghazalVideoIcon = (ImageView) view.findViewById(R.id.poetyoutubeLink);
            ghazalMainView = (RelativeLayout) view.findViewById(R.id.poetList_layout);


            //header
            headerPoetImg = (ImageView) view.findViewById(R.id.poetProfileImageTemplate);
            headerPoetName = (TextView) view.findViewById(R.id.poetNameTemplateTV);
            headerPoetTenure = (TextView) view.findViewById(R.id.poetTenureTemplateTV);
            headerPoetBirthPlace = (TextView) view.findViewById(R.id.poetBirthPlaceTemplate);
            headerPoetDescription = (TextView) view.findViewById(R.id.poetDescriptionTemplate);
            headerTypeOfScreen = (TextView) view.findViewById(R.id.poetTypeOfScreenTemplate);
            headerTypeOfScreenItemCount = (TextView) view.findViewById(R.id.poetCountTemplate);

        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
