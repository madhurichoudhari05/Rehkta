package org.Rekhta.adapters.poetDetail;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.madhu.MediaPlayerHandler;
import org.Rekhta.model.poetDetail.PoetAudioData;
import org.Rekhta.utils.CommonUtil;


public class PoetAudioAdapter2 extends RecyclerView.Adapter<PoetAudioAdapter2.RecyclerViewHolder>{

    private ArrayList<PoetAudioData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;
    private MediaPlayer mediaPlayer;
    private Boolean isMusicRunning = false;

    private int currentPlayingPosition;
    private SeekBarUpdater seekBarUpdater;
    public RecyclerViewHolder playingHolder;
    private SeekBar currentSeekBar;
    private boolean seekbarsttus;
    private Handler mHandler = new Handler();
    private MediaPlayerHandler handler;
    private boolean prgstatus=true;
    private String audioUrlPath="";

    public PoetAudioAdapter2(ArrayList<PoetAudioData> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

        this.currentPlayingPosition = -1;
        seekBarUpdater = new SeekBarUpdater();

        mediaPlayer = new MediaPlayer();
        init();


        //mediaPlayer.setOnCompletionListener(this);

    }

    private void init() {





    }

    @Override
    public PoetAudioAdapter2.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_detailed_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_poetdetailed_ghazallist, parent, false);
        }
        return new PoetAudioAdapter2.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PoetAudioAdapter2.RecyclerViewHolder holder, final int position) {

        if (position == 0) {

            String audiourl = "https://rekhtacdn.azureedge.net/images/shayar/round/";

            Glide.with(context).load(audiourl + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.headerPoetImg);

            holder.headerPoetTenure.setText(((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfBirth() + " - " + (((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfDeath() + " |"));
            holder.headerTypeOfScreen.setText("AUDIO ");
            holder.headerTypeOfScreenItemCount.setText(arrayList.size() + "");
            if (CommonUtil.languageCode == 1) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInEng());
                if (((PoetDetailedActivity) context).poetShortDescInEng.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInEng);
                }
            } else if (CommonUtil.languageCode == 2) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInHin());
                if (((PoetDetailedActivity) context).poetShortDescInHin.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInHin);
                }
            } else {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInUrdu());
                if (((PoetDetailedActivity) context).poetShortDescInUrdu.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInUrdu);
                }
            }


        }
        else {
            final PoetAudioData poetAudioData = arrayList.get(position - 1);


            if (position % 2 == 0) {
                holder.mainView.setBackgroundColor(Color.parseColor("#eeeeee"));
            } else {
                holder.mainView.setBackgroundColor(Color.WHITE);
            }

            holder.audioTitle.setText(poetAudioData.getAudioTitleInEng());

            holder.audioFavIcon.setVisibility(View.VISIBLE);
            if (CommonUtil.isContentUsersFav(poetAudioData.getAudioID())) {
                holder.audioFavIcon.setImageResource(R.drawable.ic_favorited);
                holder.audioFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

            }


            holder.audioFavIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isContentUsersFav(poetAudioData.getAudioID())) {
                        CommonUtil.removeFromFavTroughApi(context, poetAudioData.getAudioID());
                        holder.audioFavIcon.setImageResource(R.drawable.ic_favorite);
                        holder.audioFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                    } else {

                        CommonUtil.addToFavTroughApi(context, poetAudioData.getAudioID());
                        holder.audioFavIcon.setImageResource(R.drawable.ic_favorited);
                        holder.audioFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                    }

                }
            });

            Glide.with(context).load("https://rekhtacdn.azureedge.net/images/shayar/" + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.poetImg);

        /*    if (position == currentPlayingPosition) {
                playingHolder = holder;
                updatePlayingView();
            } else {
                updateNonPlayingView(holder);
            }*/
            // Audio Player



          audioUrlPath=poetAudioData.getAudioID();


            String audioUrl ="https://rekhta.org/Images/SiteImages/Audio/" +audioUrlPath + ".mp3";

            Log.e("audiourl","audiSayari"+audioUrl);

            //     handler = MediaPlayerHandler.getInstance(this,audioUrl);
//            handler = MediaPlayerHandler.getInstance(this,audioUrl);



            holder.audioPlayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    audioUrlPath=poetAudioData.getAudioID();

                    if (prgstatus) {
                        prgstatus=false;
                        holder.audioSeekBar.setVisibility(View.VISIBLE);
                        holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
//                        handler.startPlaying1();
                        notifyDataSetChanged();
                    }
                    else {
                        holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
                        prgstatus=true;
                        handler.stopPlaying();

                    }
                }
            });




            holder.audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    handler.setUsingSeekBar(progress, fromUser);
                }
            });


          /*  holder.audioeditorChoiceIcon.setVisibility(View.GONE);  // this is commented because design is cahnged
            holder.audioPopularChoiceIcon.setVisibility(View.GONE);
            holder.audioAudioIcon.setVisibility(View.GONE);
            holder.audioVideoIcon.setVisibility(View.GONE);
           holder.audioShareIcon.setVisibility(View.GONE); */

   /*         holder.mainView.setOnClickListener(new View.OnClickListener() { // this is commented because design is cahnged
                @Override
                public void onClick(View view) {
                    BottomSheetDialogFragment bottomSheetDialogFragment = new PoetFragmentMusicPlayer();
                    Bundle bundle = new Bundle();
                    bundle.putInt("positionClicked",position-1);
                    bottomSheetDialogFragment.setArguments(bundle);
                    bottomSheetDialogFragment.show(((PoetDetailedActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

                }
            });*/
        /*    holder.audioPlayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == currentPlayingPosition) {
                        playingHolder = holder;
                        if (mediaPlayer != null) {
                            if (mediaPlayer.isPlaying()) {
                                mediaPlayer.pause();
                            } else {
                                mediaPlayer.start();
                            }
                        }
                    }else {
                        currentPlayingPosition = position;
                        if (mediaPlayer != null) {
                            playingHolder = holder;
                            if (null != playingHolder) {
                                updateNonPlayingView(playingHolder);
                            }
                            mediaPlayer.release();
                        }
                        //playingHolder = view;
                        startMediaPlayer(poetAudioData.getAudioID());
                    }
                    updatePlayingView();
                }
            });*/

         /*   holder.audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if (b) {
                        mediaPlayer.seekTo(i);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });*/

        }
    }



 /*   @Override
    public void onViewRecycled(RecyclerViewHolder holder) {
        super.onViewRecycled(holder);
        if (currentPlayingPosition == holder.getAdapterPosition()) {
            if( holder.audioSeekBar!=null) {

                Toast.makeText(context, "play", Toast.LENGTH_SHORT).show();

                holder.audioSeekBar.removeCallbacks(seekBarUpdater);
                holder.audioSeekBar.setEnabled(false);
                holder.audioSeekBar.setProgress(0);
                holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
            }

          //  updateNonPlayingView(playingHolder);
            playingHolder = null;
        }
    }*/

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size() + 1;
    }


/*
    public void getDuration(){
        Uri uri = Uri.parse(pathStr);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(AppContext.getAppContext(),uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int millSecond = Integer.parseInt(durationStr);
    }*/

    private void updateNonPlayingView(RecyclerViewHolder holder) {
        holder.audioSeekBar.removeCallbacks(seekBarUpdater);
        holder.audioSeekBar.setEnabled(false);
        holder.audioSeekBar.setProgress(0);
        holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
    }

    private void updatePlayingView() {
        if (playingHolder!= null) {

            if (mediaPlayer != null) {

                playingHolder.audioSeekBar.setMax(mediaPlayer.getDuration());
                playingHolder.audioSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                playingHolder.audioSeekBar.setEnabled(true);
                if (mediaPlayer.isPlaying()) {
                    playingHolder.audioSeekBar.postDelayed(seekBarUpdater, 100);
                    playingHolder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                } else {
                    playingHolder.audioSeekBar.removeCallbacks(seekBarUpdater);
                    playingHolder.audioPlayBtn.setImageResource(R.drawable.ic_play);
                }
            }
        }
    }


    void stopPlayer() {
        if (null != mediaPlayer) {
            releaseMediaPlayer();
        }
    }

   /* @Override
    public void setSeekBarMaxLength(int setLength) {
        playingHolder.audioSeekBar.setMax(setLength);
    }

    @Override
    public void setSeekBarCurrentProgress(final int length) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playingHolder.audioSeekBar.setProgress(length);
                new Handler().postDelayed(this, 1000);
            }
        });

    }*/


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView audioFavIcon;
        TextView audioTitle;
        ImageView poetImg;
        ImageView audioPlayBtn;
        SeekBar audioSeekBar;
        /*      ImageView audioeditorChoiceIcon;
                ImageView audioPopularChoiceIcon;
                ImageView audioAudioIcon;
                ImageView audioVideoIcon;
                ImageView audioShareIcon;*/
        LinearLayout mainView;

        //header
        ImageView headerPoetImg;
        TextView headerPoetName;
        TextView headerPoetTenure;
        TextView headerPoetBirthPlace;
        TextView headerPoetDescription;
        TextView headerTypeOfScreen;
        TextView headerTypeOfScreenItemCount;

        public RecyclerViewHolder(final View view) {
            super(view);
            audioTitle = (TextView) view.findViewById(R.id.tv_header);

            audioFavIcon = (ImageView) view.findViewById(R.id.poetTemplateFavIcon);
            poetImg = (ImageView) view.findViewById(R.id.iv_logo);
            audioPlayBtn = (ImageView) view.findViewById(R.id.audioPlayButton);
            audioSeekBar = (SeekBar) view.findViewById(R.id.audioSeekbar);
         //   audioPlayBtn.setOnClickListener(this);
            //  audioSeekBar.setOnSeekBarChangeListener(this);
          /*audioeditorChoiceIcon = (ImageView) view.findViewById(R.id.poetTemplateEditorChoiceIcon);
            audioPopularChoiceIcon = (ImageView) view.findViewById(R.id.poetTemplatePopularChoiceIcon);
            audioAudioIcon = (ImageView) view.findViewById(R.id.poetTemplateAudioIcon);
            audioVideoIcon = (ImageView) view.findViewById(R.id.poetTemplateVideoIcon);
            audioShareIcon = (ImageView) view.findViewById(R.id.poetTemplateShareIcon); */
            mainView = (LinearLayout) view.findViewById(R.id.mainTemplateofPoetDetailedview);

            //header
            headerPoetImg = (ImageView) view.findViewById(R.id.poetProfileImageTemplate);
            headerPoetName = (TextView) view.findViewById(R.id.poetNameTemplateTV);
            headerPoetTenure = (TextView) view.findViewById(R.id.poetTenureTemplateTV);
            headerPoetBirthPlace = (TextView) view.findViewById(R.id.poetBirthPlaceTemplate);
            headerPoetDescription = (TextView) view.findViewById(R.id.poetDescriptionTemplate);
            headerTypeOfScreen = (TextView) view.findViewById(R.id.poetTypeOfScreenTemplate);
            headerTypeOfScreenItemCount = (TextView) view.findViewById(R.id.poetCountTemplate);

        }

    }

    private void startMediaPlayer(String audioResId) {
        try {
            //mediaPlayer = MediaPlayer.create(getApplicationContext(), audioResId);
            mediaPlayer.setDataSource("https://rekhta.org/Images/SiteImages/Audio/" + audioResId + ".mp3");
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    releaseMediaPlayer();
                }
            });
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void releaseMediaPlayer() {
        if (null != playingHolder) {
            updateNonPlayingView(playingHolder);
        }
        mediaPlayer.release();
        mediaPlayer = null;
        currentPlayingPosition = -1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }


    private class SeekBarUpdater implements Runnable {
        @Override
        public void run() {
            if (null != playingHolder) {
                playingHolder.audioSeekBar.setProgress(mediaPlayer.getCurrentPosition());
                playingHolder.audioSeekBar.postDelayed(this, 100);
            }
        }
    }

    public boolean checkImageisOfPlayBtn(Context ctx, ImageView imageView, int imageResource) {
        boolean result = false;

        if (ctx != null && imageView != null && imageView.getDrawable() != null) {
            Drawable.ConstantState constantState;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                constantState = ctx.getResources()
                        .getDrawable(imageResource, ctx.getTheme())
                        .getConstantState();
            } else {
                constantState = ctx.getResources().getDrawable(imageResource)
                        .getConstantState();
            }

            if (imageView.getDrawable().getConstantState() == constantState) {
                result = true;
            }
        }

        return result;
    }

    public void stopAudio() {

        if (mediaPlayer != null) {
            //mediaPlayer.reset();
            mediaPlayer.stop();
            isMusicRunning = false;
            currentSeekBar.setProgress(0);

        }
    }

    public void playAudio(String audioID) {

        try {
            if (mediaPlayer.isPlaying()) {

                Toast.makeText(context, "play", Toast.LENGTH_SHORT).show();
                stopAudio();
            } else {

                if(mediaPlayer == null){
                    mediaPlayer = new MediaPlayer();
                }
                if(audioID!=null) {
                    mediaPlayer.setDataSource("https://rekhta.org/Images/SiteImages/Audio/" + audioID + ".mp3");
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    currentSeekBar.setMax(mediaPlayer.getDuration());
                }
//                new Thread((Runnable) this).start();

            }

            } catch(IOException e){
                e.printStackTrace();
            }
        }


}
