package org.Rekhta.adapters.poetDetail;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.fragments.poetDetailledFragments.PoetAudioFragment;
import org.Rekhta.madhu.MediaPlayerHandler;
import org.Rekhta.model.poetDetail.PoetAudioData;
import org.Rekhta.utils.CommonUtil;


public class PoetAudioAdapter extends RecyclerView.Adapter<PoetAudioAdapter.RecyclerViewHolder> {

    private ArrayList<PoetAudioData> arrayList = new ArrayList<>();
    Context context;
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;
    private MediaPlayerHandler handler;
    private String audioUrlPath = "";
    private int currentStatus = 0;
    private int count = 0;
    private PoetAudioFragment poetAudioFragment;
    int totalNumberOfContent;

    public PoetAudioAdapter(ArrayList<PoetAudioData> arrayList, Context context, PoetAudioFragment poetAudioFragment, int totalNumberOfContent) {
        this.arrayList = arrayList;
        this.context = context;
        handler = MediaPlayerHandler.getInstance();
        this.poetAudioFragment = poetAudioFragment;

        this.totalNumberOfContent = totalNumberOfContent;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

    }


    @Override
    public PoetAudioAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_detailed_header, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_poetdetailed_ghazallist, parent, false);
        }
        return new PoetAudioAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PoetAudioAdapter.RecyclerViewHolder holder, final int position) {

        if (position == 0) {

            String audiourl = "https://rekhtacdn.azureedge.net/images/shayar/round/";

            Glide.with(context).load(audiourl + ((PoetDetailedActivity) context).currentPoetId + ".png").into(holder.headerPoetImg);


            holder.headerPoetTenure.setText(((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfBirth() + " - " + (((PoetDetailedActivity) context).poetDetailData.getPoetsDateOfDeath() + " |"));
            holder.headerTypeOfScreen.setText("AUDIO ");
            holder.headerTypeOfScreenItemCount.setText(totalNumberOfContent+"");
            if (CommonUtil.languageCode == 1) {
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInEng());

                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInEng());
                String desText = ((PoetDetailedActivity) context).poetShortDescInEng;
                if (desText.equalsIgnoreCase("null") || desText.equalsIgnoreCase("") || desText == null) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInEng);
                }
            } else if (CommonUtil.languageCode == 2) {
                String kk = ((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin();
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInHin());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInHin());
                if (((PoetDetailedActivity) context).poetShortDescInHin.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInHin);
                }
            } else if (CommonUtil.languageCode == 3) {
                String kk = ((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu();
                holder.headerPoetName.setText(((PoetDetailedActivity) context).poetDetailData.getPoetNameInUrdu());
                holder.headerPoetBirthPlace.setText(((PoetDetailedActivity) context).poetDetailData.getPoetBirthPlaceInUrdu());
                if (((PoetDetailedActivity) context).poetShortDescInUrdu.equalsIgnoreCase("null")) {
                    holder.headerPoetDescription.setVisibility(View.GONE);
                } else {
                    holder.headerPoetDescription.setText(((PoetDetailedActivity) context).poetShortDescInUrdu);
                }
            }

        } else {
            final PoetAudioData poetAudioData = arrayList.get(position - 1);

            if (CommonUtil.languageCode == 1) {
                holder.audioTitle.setText(poetAudioData.getAudioTitleInEng());
                holder.singer_name.setText(poetAudioData.getAudioHeaderInEng());
            } else if (CommonUtil.languageCode == 2) {
                holder.audioTitle.setText(poetAudioData.getAudioTitleInHin().trim());
                holder.singer_name.setText(poetAudioData.getAudioHeaderInHIn().trim());
            } else if (CommonUtil.languageCode == 3) {
                holder.audioTitle.setText(poetAudioData.getAudioTitleInUrdu().trim());
                holder.singer_name.setText(poetAudioData.getAudioHeaderInUrdu().trim());
            }

            if (position % 2 == 0) {
                holder.mainView.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mainView.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
                }
            } else {
                holder.mainView.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mainView.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }


            holder.audioFavIcon.setVisibility(View.VISIBLE);
            if (CommonUtil.isContentUsersFav(poetAudioData.getAudioID())) {
                holder.audioFavIcon.setImageResource(R.drawable.ic_favorited);
                holder.audioFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

            }


            holder.audioFavIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isContentUsersFav(poetAudioData.getAudioID())) {
                        CommonUtil.removeFromFavTroughApi(context, poetAudioData.getAudioID());
                        holder.audioFavIcon.setImageResource(R.drawable.ic_favorite);
                        holder.audioFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                    } else {

                        CommonUtil.addToFavTroughApi(context, poetAudioData.getAudioID());
                        holder.audioFavIcon.setImageResource(R.drawable.ic_favorited);
                        holder.audioFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                    }

                }
            });

            holder.main_item_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    poetAudioFragment.playClickedAudio(position - 1, context);
                }
            });

            String poetId = ((PoetDetailedActivity) context).currentPoetId;

            Glide.with(context).load("https://rekhtacdn.azureedge.net/images/shayar/" + poetAudioData.getAudioAS() + ".png").into(holder.poetImg);

        /*    if (position == currentPlayingPosition) {
                playingHolder = holder;
                updatePlayingView();
            } else {
                updateNonPlayingView(holder);
            }*/
            // Audio Player


            audioUrlPath = poetAudioData.getAudioID();

            final String audioUrl = "https://rekhta.org/Images/SiteImages/Audio/" + poetAudioData.getId() + ".mp3";

            Log.e("audiourl", "audiSayari" + audioUrl);

            //     handler = MediaPlayerHandler.getInstance(this,audioUrl);
            final String url = "https://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3";

            if (poetAudioData.isRunning()) {
                holder.audioSeekBar.setEnabled(true);
                holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
            } else {
                holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
                holder.audioSeekBar.setProgress(0);
                holder.audioSeekBar.setEnabled(false);
            }

            //getAudioLength(audioUrl, holder.totalTime);
            holder.totalTime.setText(poetAudioData.getLength());

            holder.audioPlayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    holder.audioSeekBar.setVisibility(View.VISIBLE);
                    for (int i = 0; i < arrayList.size(); i++) {
                        arrayList.get(i).setRunning(false);
                    }
                    poetAudioData.setRunning(true);
                    handler.setSeekLisner(audioUrl, position, new MediaPlayerHandler.SeekListner() {
                        @Override
                        public void setSeekBarMaxLength(int setLength) {
                            holder.audioSeekBar.setMax(setLength);
                        }

                        @Override
                        public void setSeekBarCurrentProgress(int length) {
                            holder.audioSeekBar.setProgress(length);

                            updatePlayer(length, holder.remiainTime);
                            if (currentStatus != 0) {
                                if (count == 10) {
                                    playStatus(currentStatus);
                                    count = 0;
                                } else {
                                    count++;
                                }
                            }
                        }

                        @Override
                        public void invalidatePrevious() {
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
                            holder.audioSeekBar.setEnabled(false);
                            holder.audioSeekBar.setProgress(0);
                            holder.remiainTime.setText("00:00");
                            holder.totalTime.setText("00:00");
//                                poetAudioData.setRunning(false);
                        }

                        @Override
                        public void setTotalLength(int duration) {

                            updatePlayer(duration, holder.totalTime);
                        }

                        @Override
                        public void playStatus(int isPlaying) {
                            currentStatus = isPlaying;
                            switch (isPlaying) {
                                case MediaPlayerHandler.PLAYING:
                                    holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                                    holder.audioSeekBar.setEnabled(true);
                                    break;
                                case MediaPlayerHandler.PAUSE:
                                    holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
                                    break;
                                case MediaPlayerHandler.STOP:
                                    holder.audioPlayBtn.setImageResource(R.drawable.ic_stop2);
                                    poetAudioData.setRunning(false);
                                    break;
                                case MediaPlayerHandler.LOADING:
                                    holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                                    break;
                                case MediaPlayerHandler.LOADED:
                                    holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                                    break;
                                case MediaPlayerHandler.LOAD_FAIL:
                                    holder.audioPlayBtn.setImageResource(R.drawable.ic_error);
                                    poetAudioData.setRunning(false);
                                    break;

                            }
                        }


                    });
//                    }

                    audioUrlPath = poetAudioData.getAudioID();
                    handler.startPlaying();

                }
            });


            holder.audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    handler.setUsingSeekBar(progress, fromUser);
                }
            });

        }
    }

    private int getAudioLength(String audioUrl, final TextView totalTime) {


        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(audioUrl, new HashMap<String, String>());
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        int currentDuration = Integer.parseInt(time);
        totalTime.setText("" + milliSecondsToTimer((long) currentDuration));

        return Integer.parseInt(time);
    }

    private void updatePlayer(int currentDuration, TextView remiainTime) {
        remiainTime.setText("" + milliSecondsToTimer((long) currentDuration));
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView audioFavIcon;
        TextView audioTitle, singer_name;
        ImageView poetImg;
        ImageView audioPlayBtn;
        SeekBar audioSeekBar;
        /*      ImageView audioeditorChoiceIcon;
                ImageView audioPopularChoiceIcon;
                ImageView audioAudioIcon;
                ImageView audioVideoIcon;
                ImageView audioShareIcon;*/
        LinearLayout mainView;

        //header
        ImageView headerPoetImg;
        TextView headerPoetName;
        TextView headerPoetTenure;
        TextView headerPoetBirthPlace;
        TextView headerPoetDescription;
        TextView headerTypeOfScreen;
        TextView headerTypeOfScreenItemCount;
        TextView remiainTime;
        TextView totalTime;
        RelativeLayout main_item_layout;

        public RecyclerViewHolder(final View view) {
            super(view);
            audioTitle = (TextView) view.findViewById(R.id.tv_header);
            singer_name = (TextView) view.findViewById(R.id.singer_name);

            audioFavIcon = (ImageView) view.findViewById(R.id.poetTemplateFavIcon);
            poetImg = (ImageView) view.findViewById(R.id.iv_logo);
            audioPlayBtn = (ImageView) view.findViewById(R.id.audioPlayButton);
            audioSeekBar = (SeekBar) view.findViewById(R.id.audioSeekbar);
            main_item_layout = (RelativeLayout) view.findViewById(R.id.main_item_layout);

            mainView = (LinearLayout) view.findViewById(R.id.mainTemplateofPoetDetailedview);

            //header
            headerPoetImg = (ImageView) view.findViewById(R.id.poetProfileImageTemplate);
            headerPoetName = (TextView) view.findViewById(R.id.poetNameTemplateTV);
            headerPoetTenure = (TextView) view.findViewById(R.id.poetTenureTemplateTV);
            headerPoetBirthPlace = (TextView) view.findViewById(R.id.poetBirthPlaceTemplate);
            headerPoetDescription = (TextView) view.findViewById(R.id.poetDescriptionTemplate);
            headerTypeOfScreen = (TextView) view.findViewById(R.id.poetTypeOfScreenTemplate);
            headerTypeOfScreenItemCount = (TextView) view.findViewById(R.id.poetCountTemplate);
            remiainTime = (TextView) view.findViewById(R.id.remiainTime);
            totalTime = (TextView) view.findViewById(R.id.totalTime);

        }

    }


}


/*
*

if(holder.audioPlayBtn.getTag()!=null){
                    int i=0;
                    try {
                        i=Integer.parseInt((String) holder.audioPlayBtn.getTag());
                    }catch (Exception e){}
                    switch (i) {
                        case MediaPlayerHandler.PLAYING:
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                            break;
                        case MediaPlayerHandler.PAUSE:
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_play);
                            break;
                        case MediaPlayerHandler.STOP:
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_stop2);
                            break;
                        case MediaPlayerHandler.LOADING:
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                            break;
                        case MediaPlayerHandler.LOADED:
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_pause);
                            break;
                        case MediaPlayerHandler.LOAD_FAIL:
                            holder.audioPlayBtn.setImageResource(R.drawable.ic_error);
                            break;

                    }
                }

* */