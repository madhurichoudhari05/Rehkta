package org.Rekhta.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import org.Rekhta.R;;


public class SavedImagePagerAdapter extends PagerAdapter {

    private Activity _activity;
    private Context context;
    private ArrayList<Bitmap> imagesList;
    private LayoutInflater inflater;

    public SavedImagePagerAdapter(Context context, ArrayList<Bitmap> imagesList) {
        //this._activity = activity;
        this.context = context;
        this.imagesList = imagesList;
    }


    @Override
    public int getCount() {
        return imagesList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        final ImageView imgDisplay;
        Button btnClose, btnSaveTogallery;

        inflater = LayoutInflater.from(context);
        View viewLayout = inflater.inflate(R.layout.template_zoomed_image, container, false);

        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        btnSaveTogallery = (Button) viewLayout.findViewById(R.id.btn_SaveToGallery);

        btnClose.setVisibility(View.GONE);
        btnSaveTogallery.setVisibility(View.GONE);


        imgDisplay.setImageBitmap(imagesList.get(position));
        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
    }
}
