package org.Rekhta.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.model.homeScreenModel.HomeCards;
import org.Rekhta.utils.CommonUtil;


public class ExploreRecyclerAdpater extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    List<HomeCards> cardsModel;

    public ExploreRecyclerAdpater(Context context, List<HomeCards> cardsModel) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.cardsModel = cardsModel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.expplore_category_items, null, false);

        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);

        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
        categoryViewHolder.heading.setText(cardsModel.get(position).getTextHeading());
        categoryViewHolder.content.setText(cardsModel.get(position).getTextContent());

        String baseUrl = "https://rekhtacdn.azureedge.net/Images/Cms/Cards/" + cardsModel.get(position).getCardId() + ".jpg";
        Glide.with(context).load(baseUrl).into(categoryViewHolder.imageView);

        if (CommonUtil.languageCode == 1) {
            CommonUtil.setOswaldMedium(context, categoryViewHolder.heading);
        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setHindiLailaRegular(context, categoryViewHolder.heading);
        } else if (CommonUtil.languageCode == 3) {
            CommonUtil.setUrduFont(context, categoryViewHolder.heading);
        }

    }

    @Override
    public int getItemCount() {
        return cardsModel.size();
    }

    private class CategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        TextView heading, content;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.explorePoetImage);
            heading = (TextView) itemView.findViewById(R.id.exploreHeatingText);
            content = (TextView) itemView.findViewById(R.id.exploreContentText);
        }
    }

}
