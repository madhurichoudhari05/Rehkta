package org.Rekhta.adapters.viewProfile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.model.viewProfile.ProfileModel;


/**
 * Created by Admin on 11/7/2017.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {
    ArrayList<ProfileModel> profileList = new ArrayList<>();
    Context context;

    public ProfileAdapter(ArrayList<ProfileModel> profileList, Context context) {
        this.profileList = profileList;
        this.context = context;
    }

    @Override
    public ProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_item,parent,false);
        ViewHolder recyclerViewHolder = new ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.ViewHolder holder, int position) {
        ProfileModel profileModel = profileList.get(position);
        holder.poetName.setText(profileModel.getPoet());
        holder.poet.setText(profileModel.getPoetName());

    }

    @Override
    public int getItemCount() {
        return profileList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView poet,poetName;
        public ViewHolder(View itemView) {
            super(itemView);
            poet=(TextView)itemView.findViewById(R.id.poetry);
            poetName=(TextView)itemView.findViewById(R.id.poet_name);
        }
    }
}
