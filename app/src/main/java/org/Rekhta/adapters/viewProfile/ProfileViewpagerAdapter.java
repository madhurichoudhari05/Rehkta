package org.Rekhta.adapters.viewProfile;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.Rekhta.fragments.viewProfile.ProfileFragment;

/**
 * Created by Admin on 11/7/2017.
 */

public class ProfileViewpagerAdapter extends FragmentPagerAdapter {
    public ProfileViewpagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return new ProfileFragment();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

            case 0:
                return "Ghazals      ";

            case 1:
                return "Nazms";
            case 2:
                return "       Sher";

            default:
                return "";
        }
    }
}
