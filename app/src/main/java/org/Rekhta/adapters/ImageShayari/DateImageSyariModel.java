package org.Rekhta.adapters.ImageShayari;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 12/8/2017.
 */

public class DateImageSyariModel {

    public String getImageDate() {
        return imageDate;
    }

    public void setImageDate(String imageDate) {
        this.imageDate = imageDate;
    }

    public List<ImageListModel> getList() {
        return list;
    }

    public void setList(List<ImageListModel> list) {
        this.list = list;
    }

    private String imageDate;
    private List<ImageListModel> list;
}
