package org.Rekhta.adapters.ImageShayari;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.activites.ZoomedImageActivity;
import org.Rekhta.fragments.imageShayari.ZoomedImageFragment;
import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionData;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionDataGallery;
import org.Rekhta.utils.CommonUtil;


public class ImageShayriRecyclerViewAdapterList extends RecyclerView.Adapter<ImageShayriRecyclerViewAdapterList.RecyclerViewHolder> {

    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;
    List<ShayariDetailDto> getShayari1;
    private List<ShayariDetailDto> arrayList = new ArrayList<>();

    public ImageShayriRecyclerViewAdapterList(List<ShayariDetailDto> arrayList, Context context, List<ShayariDetailDto> getShayari1) {
        this.arrayList = arrayList;
        this.context = context;

        this.getShayari1 = getShayari1;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public ImageShayriRecyclerViewAdapterList.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_image_shayeri, parent, false);
        return new ImageShayriRecyclerViewAdapterList.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageShayriRecyclerViewAdapterList.RecyclerViewHolder holder, final int position) {

        final ShayariDetailDto imageShayriCollectionData = arrayList.get(position);
        Log.e("--------------", imageShayriCollectionData.getImageId());
        if (CommonUtil.languageCode == 1) {


        } else if (CommonUtil.languageCode == 2) {


        } else if (CommonUtil.languageCode == 3) {

        } else {

        }
        //  int  resourceId = context.getResources().getIdentifier(imageShayriCollectionData.getImageUrl(), "drawable", "org.Rekhta");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_launcher);


        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)

                .load("https://rekhta.org/Images/ShayariImages/" + imageShayriCollectionData.getImageId() + ".png")
                .into(holder.shayriImg);

        holder.shayriImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.shayriImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ZoomedImageFragment zoomedImageFragment = new ZoomedImageFragment();
                Bundle bundle = new Bundle();
                bundle.putString("key", imageShayriCollectionData.getImageId());
                zoomedImageFragment.setArguments(bundle);
                ((ImageShyariActivity) context).currentImgClicked = position;
                ((ImageShyariActivity) context).currentImgClickedS = imageShayriCollectionData.getImageId();

                ((ImageShyariActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.drawer_layout, zoomedImageFragment, "imageFrag")
                        .addToBackStack(null)
                        .commit();

                /*Intent intent = new Intent(context, ZoomedImageActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("list", (Parcelable) getShayari1);
                intent.putExtra("imageId", imageShayriCollectionData.getImageId());
                intent.putExtra("key", imageShayriCollectionData.getImageId());
                context.startActivity(intent);*/
            }
        });


    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView shayriImg;

        public RecyclerViewHolder(View view) {
            super(view);
            shayriImg = (ImageView) view.findViewById(R.id.imgShayeriIMAGE);
        }

    }


}
