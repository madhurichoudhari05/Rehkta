package org.Rekhta.adapters.ImageShayari;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

import org.Rekhta.R;;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.fragments.imageShayari.ZoomedImageFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

public class FullScreenImageAdapter extends PagerAdapter {

    private Activity _activity;
    private Context context;
    private ArrayList<String> _imagePaths;
    private LayoutInflater inflater;
    ZoomedImageFragment zoomedImageFragment;
    public Button btnClose, btnSaveTogallery;

    // constructor
    public FullScreenImageAdapter(Context context, ArrayList<String> imagePaths, ZoomedImageFragment zoomedImageFragment) {
        //  this._activity = activity;
        this.context = context;
        this._imagePaths = imagePaths;
        this.zoomedImageFragment = zoomedImageFragment;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final ImageView imgDisplay;
        Button btnClose, btnSaveTogallery;


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.template_zoomed_image, container, false);

        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        btnSaveTogallery = (Button) viewLayout.findViewById(R.id.btn_SaveToGallery);

        btnSaveTogallery.setText(LanguageSelection.getSpecificLanguageWordById("SAVE TO GALLERY", CommonUtil.languageCode));
        btnClose.setText(LanguageSelection.getSpecificLanguageWordById("SHARE", CommonUtil.languageCode));

        // New logic
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_close);

        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load("https://rekhta.org/Images/ShayariImages/" + _imagePaths.get(position) + ".png")
                .into(imgDisplay);

        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BitmapDrawable draw = (BitmapDrawable) imgDisplay.getDrawable();
                final Bitmap bitmap = draw.getBitmap();
                shareImge(bitmap);
                // onShareItem(imgDisplay);
            }
        });


        //save To gallery


        btnSaveTogallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    BitmapDrawable draw = (BitmapDrawable) imgDisplay.getDrawable();
                    final Bitmap bitmap = draw.getBitmap();
                    if (Build.VERSION.SDK_INT >= 23) {
                        if (((ImageShyariActivity) _activity).checkPermission()) {
//                            SaveImageLocally(anImage);

                            saveImageToExternal("rekhta" + position, bitmap);
                        } else {
                            ((ImageShyariActivity) _activity).requestPermission(); // Code for permission
                        }
                    } else {
//                        SaveImageLocally(anImage);
                        saveImageToExternal("rekhta" + position, bitmap);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }


    public void saveImageToExternal(String imgName, Bitmap bm) throws IOException {
//Create Path to save Image
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "Rekhta"); //Creates app specific folder
        path.mkdirs();
        File imageFile = new File(path, imgName + ".png"); // Imagename.png
        FileOutputStream out = new FileOutputStream(imageFile);
        try {
            bm.compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
            out.flush();
            out.close();


            Toast.makeText(_activity, "Image Saved Successfully", Toast.LENGTH_LONG).show();


            // LocalBroadcastManager.getInstance(.this).sendBroadcast(locationReceiver);

            LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intent = new Intent("FullImage");
            localBroadcastManager.sendBroadcast(intent);


            MediaScannerConnection.scanFile(_activity, new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });
        } catch (Exception e) {
            throw new IOException();
        }
    }

    public void shareImge(Bitmap bitmap) {

        try {
            File file = new File(_activity.getExternalCacheDir(), "rekhtaImageShayri.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            file.setReadable(true, false);
            final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            intent.putExtra(Intent.EXTRA_SUBJECT, "www.rekhta.org");

            intent.setType("image/png");
            _activity.startActivity(Intent.createChooser(intent, "Share image via"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void onShareItem(ImageView ivImage) {
        // src = https://github.com/codepath/android_guides/wiki/Sharing-Content-with-Intents
        Uri bmpUri = getLocalBitmapUri(ivImage);
        if (bmpUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            _activity.startActivity(Intent.createChooser(shareIntent, "Share Image"));
        } else {
            Toast.makeText(_activity, "Unable to Share", Toast.LENGTH_SHORT).show();
        }
    }

    public Uri getLocalBitmapUri(ImageView imageView) {

        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        Uri bmpUri = null;
        try {

            File file = new File(_activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
}