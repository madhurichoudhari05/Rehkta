package org.Rekhta.adapters.ImageShayari;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.madhu.dto.MasterShayriImageDto;
import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionDataGallery;
import org.Rekhta.utils.CommonUtil;


public class ImageShayriAdapterGallery extends RecyclerView.Adapter<ImageShayriAdapterGallery.RecyclerViewHolder> {

    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;
    SimpleDateFormat df;
    String formattedDate;
    long elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds;
    private List<MasterShayriImageDto> arrayList = new ArrayList<>();
    List<ShayariDetailDto> getShayari1;


    public ImageShayriAdapterGallery(List<MasterShayriImageDto> arrayList, Context context, List<ShayariDetailDto> getShayari1) {
        this.arrayList = arrayList;
        this.context = context;
        this.getShayari1 = getShayari1;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
        }

        return outputDate;

    }

    @Override
    public ImageShayriAdapterGallery.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_image_shayerlist, parent, false);
        return new ImageShayriAdapterGallery.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageShayriAdapterGallery.RecyclerViewHolder holder, final int position) {


    /*  holder. mainScrollView.fullScroll(ScrollView.FOCUS_UP);
        holder.  mainScrollView.setFocusableInTouchMode(true);
        holder.  mainScrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);*/


        final MasterShayriImageDto imageShayriCollectionDataGallery = arrayList.get(position);
        Log.e("--------------", imageShayriCollectionDataGallery.getDate());
        if (CommonUtil.languageCode == 1) {


        } else if (CommonUtil.languageCode == 2) {


        } else if (CommonUtil.languageCode == 3) {

        } else {

        }
        //  int  resourceId = context.getResources().getIdentifier(ImageShayriCollectionDataGallery.getImageUrl(), "drawable", "org.Rekhta");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_launcher);

        Calendar calendar = Calendar.getInstance();
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(calendar.getTime());

        String datelist = imageShayriCollectionDataGallery.getDate().split("T")[0].trim();

        String date_after = formateDateFromstring("yyyy-MM-dd", "dd MMM yy", datelist);

        diffTime(datelist, formattedDate);


        if (elapsedDays > 5) {
            //Toast.makeText(context, "555555"+date_after, Toast.LENGTH_SHORT).show();
            holder.tvDate.setText(date_after);
        } else if (elapsedDays <= 5 && elapsedDays >= 1) {
            holder.tvDate.setText(String.valueOf(elapsedDays) + "d");
        } else if (elapsedHours >= 1 && elapsedDays == 0) {
            holder.tvDate.setText(String.valueOf(elapsedDays) + "h");
        } else if (elapsedMinutes >= 1 && elapsedDays == 0 && elapsedHours == 0) {

            holder.tvDate.setText(String.valueOf(elapsedDays) + "m");
        } else if (elapsedSeconds >= 1 && elapsedMinutes == 0 && elapsedHours == 0 && elapsedDays == 0) {
            holder.tvDate.setText(String.valueOf(elapsedDays) + "s");
        }

        holder.tvDate.setText(date_after);


        ImageShayriRecyclerViewAdapterList imageShayriRecyclerViewAdapter = new ImageShayriRecyclerViewAdapterList(imageShayriCollectionDataGallery.getShyri(), context,getShayari1);
        RecyclerView.LayoutManager manager = new GridLayoutManager(context, 4);
        holder.imageShayeriRecylerView.setLayoutManager(manager);
        holder.imageShayeriRecylerView.setAdapter(imageShayriRecyclerViewAdapter);
    }

    void diffTime(String dtstart, String end) {
        //DateTimeUtils obj = new DateTimeUtils();
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

            Date date1 = simpleDateFormat.parse(dtstart);
            Date date2 = simpleDateFormat.parse(end);

            printDifference(date1, date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void printDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        elapsedSeconds = different / secondsInMilli;

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        RecyclerView imageShayeriRecylerView;
        TextView tvDate;
        ScrollView mainScrollView;

        public RecyclerViewHolder(View view) {
            super(view);
            imageShayeriRecylerView = (RecyclerView) view.findViewById(R.id.imageShayeriTabrecyclerview);
            tvDate = (TextView) view.findViewById(R.id.tvDate);

        }

    }


}
