package org.Rekhta.adapters.ImageShayari;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.activites.ZoomedImageActivity;
import org.Rekhta.fragments.imageShayari.ZoomedImageFragment;
import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionData;
import org.Rekhta.utils.CommonUtil;


public class ImageShayriRecyclerViewAdapter extends RecyclerView.Adapter<ImageShayriRecyclerViewAdapter.RecyclerViewHolder> {

    private ArrayList<ImageShayriCollectionData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;

    public ImageShayriRecyclerViewAdapter(ArrayList<ImageShayriCollectionData> arrayList, Context context, List<ShayariDetailDto> getShayari) {
        this.arrayList = arrayList;
        this.context = context;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public ImageShayriRecyclerViewAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_image_shayeri, parent, false);
        return new ImageShayriRecyclerViewAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageShayriRecyclerViewAdapter.RecyclerViewHolder holder, final int position) {

        final ImageShayriCollectionData imageShayriCollectionData = arrayList.get(position);
        Log.e("--------------", imageShayriCollectionData.getImageId());
        if (CommonUtil.languageCode == 1) {


        } else if (CommonUtil.languageCode == 2) {


        } else if (CommonUtil.languageCode == 3) {

        } else {

        }
        //  int  resourceId = context.getResources().getIdentifier(imageShayriCollectionData.getImageUrl(), "drawable", "org.Rekhta");

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_launcher);


        Glide.with(context)
                .setDefaultRequestOptions(requestOptions)

                .load("https://rekhta.org/Images/ShayariImages/" + imageShayriCollectionData.getImageId() + ".png")
                .into(holder.shayriImg);

        holder.shayriImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.shayriImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ZoomedImageFragment zoomedImageFragment = new ZoomedImageFragment();
                //  ((ImageShyariActivity)context).currentImgClicked = position;
                ((ImageShyariActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.drawer_layout, zoomedImageFragment, "imageFrag")
                        .addToBackStack(null)
                        .commit();

               /* Intent intent = new Intent(context, ZoomedImageActivity.class);
                intent.putExtra("position", position);

                context.startActivity(intent);*/
            }
        });


    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        ImageView shayriImg;

        public RecyclerViewHolder(View view) {
            super(view);
            shayriImg = (ImageView) view.findViewById(R.id.imgShayeriIMAGE);
        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
