package org.Rekhta.adapters.ImageShayari;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import org.json.JSONObject;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.activites.SavedImagesGallery;
import org.Rekhta.fragments.imageShayari.ZoomedImageFragment;


public class SavedImageShayriGridAdapter extends BaseAdapter {

    ArrayList<Bitmap> bmpArray;
    ArrayList<String> fileName;
    Context context;

    public SavedImageShayriGridAdapter(ArrayList<Bitmap> bmpArray, ArrayList<String> fileName, Context context) {
        this.bmpArray = bmpArray;
        this.fileName = fileName;
        this.context = context;
    }

    @Override
    public int getCount() {
        return bmpArray.size();
    }

    @Override
    public Object getItem(int i) {
        return bmpArray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View Convertview, ViewGroup viewGroup) {
        View view = null;
        JSONObject obj;
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final SavedImageShayriGridAdapter.ViewHolder mHolder = new SavedImageShayriGridAdapter.ViewHolder();
            if (view == null) {

                view = inflater.inflate(R.layout.template_image_shayeri, null);
                view.setTag(mHolder);
            } else {
                view = (View) view.getTag();
            }

            mHolder.imageview = (ImageView) view.findViewById(R.id.imgShayeriIMAGE);


            //Drawable d = new BitmapDrawable(context.getResources(),  bmpArray.get(i));
            mHolder.imageview.setImageBitmap(bmpArray.get(position));


        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private class ViewHolder {
        private ImageView imageview;
    }
}
