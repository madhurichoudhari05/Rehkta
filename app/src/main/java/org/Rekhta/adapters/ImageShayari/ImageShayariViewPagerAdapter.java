package org.Rekhta.adapters.ImageShayari;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.Rekhta.fragments.imageShayari.DailyFeedsTabFragment;
import org.Rekhta.fragments.imageShayari.ImageShayriTabFragmentGallery;
import org.Rekhta.fragments.imageShayari.SavedImageTabFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;


public class ImageShayariViewPagerAdapter extends FragmentPagerAdapter {
    public ImageShayariViewPagerAdapter(FragmentManager fm) {

        super(fm);

    }

    @Override

    public Fragment getItem(int position) {
        Fragment f = null;
        if (position == 0) {
            // f=new ImageShayriTabFragment();
            f = new ImageShayriTabFragmentGallery();
        } else if (position == 1) {
            f = new SavedImageTabFragment();
        } else if (position == 2) {
            f = new DailyFeedsTabFragment();
        }
        return f;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return LanguageSelection.getSpecificLanguageWordById("IMAGE SHAYERI", CommonUtil.languageCode);
        } else if (position == 1) {
            return LanguageSelection.getSpecificLanguageWordById("SAVED IMAGES", CommonUtil.languageCode);
        } else if (position == 2) {
            return LanguageSelection.getSpecificLanguageWordById("DAILY FEEDS", CommonUtil.languageCode);
        }
        return super.getPageTitle(position);
    }
}
