package org.Rekhta.adapters.nazm;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.Rekhta.fragments.nazmFragments.NazmBeginnersFragment;
import org.Rekhta.fragments.nazmFragments.NazmEditorsChoiceFragment;
import org.Rekhta.fragments.nazmFragments.NazmHumourFragment;
import org.Rekhta.fragments.nazmFragments.NazmTop50Fragment;


public class NazmViewPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    public NazmViewPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                NazmTop50Fragment nazmTop50Fragment = new NazmTop50Fragment();
                return nazmTop50Fragment;
            case 1:
                NazmBeginnersFragment nazmBeginnersFragment = new NazmBeginnersFragment();
                return nazmBeginnersFragment;
            case 2:
                NazmEditorsChoiceFragment nazmEditorsChoiceFragment = new NazmEditorsChoiceFragment();
                return nazmEditorsChoiceFragment;
            case 3:
                NazmHumourFragment nazmHumourFragment = new NazmHumourFragment();
                return nazmHumourFragment;
            default:
                NazmTop50Fragment nazm50Fragment = new NazmTop50Fragment();
                return nazm50Fragment;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
