package org.Rekhta.adapters.nazm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.Rekhta.R;;
import org.Rekhta.Services.NazamContentService;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MyApplicationClass;


public class NazmCommonListAdapter extends BaseAdapter {

    AppDatabase db;
    private Context mContext;
    private JSONArray arrayList;


    public NazmCommonListAdapter(Context c, JSONArray list) {
        mContext = c;
        arrayList = list;
        db = MyApplicationClass.getDb();
    }

    @Override
    public int getCount() {
        return arrayList.length();
    }

    public void updateData(JSONArray array) {

        arrayList = new JSONArray();
        arrayList = array;
        this.notifyDataSetChanged();
    }

    @Override
    public Object getItem(int i) {
        JSONObject obj = null;
        try {
            obj = arrayList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View view = null;
        try {
            final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final JSONObject obj = (JSONObject) getItem(position);
            final ViewHolder mHolder = new ViewHolder();
            if (view == null) {

                view = inflater.inflate(R.layout.listview_template_nazm, null);
                view.setTag(mHolder);
            } else {
                view = (View) view.getTag();
            }
            view.setBackgroundColor(Color.WHITE);
            if (position % 2 == 0) {
//                view.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(mContext.getResources().getDrawable(R.drawable.poet_selector));
                }
            } else {
//                view.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(mContext.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }
            mHolder.nazmTitle = (TextView) view.findViewById(R.id.ghazalTitle);
            mHolder.nazmAuthorName = (TextView) view.findViewById(R.id.ghazalAuthorName);
            mHolder.nazmfirstLine = (TextView) view.findViewById(R.id.ghazalContentLine);

            if (CommonUtil.languageCode == 1) {

                mHolder.nazmTitle.setText(obj.getString("TE").trim());
                mHolder.nazmAuthorName.setText(obj.getString("PE").trim());
                mHolder.nazmfirstLine.setText(obj.getString("SE").trim());
                CommonUtil.setEnglishLatoXBold(mContext, mHolder.nazmTitle);
                CommonUtil.setEnglishLatoXBold(mContext, mHolder.nazmAuthorName);
                CommonUtil.setEnglishLatoXRegualr(mContext, mHolder.nazmfirstLine);

            } else if (CommonUtil.languageCode == 2) {
                mHolder.nazmTitle.setText(obj.getString("TH").trim());
                mHolder.nazmAuthorName.setText(obj.getString("PH").trim());
                mHolder.nazmfirstLine.setText(obj.getString("SH").trim());
                CommonUtil.setHindiLailaRegular(mContext, mHolder.nazmTitle);
                CommonUtil.setHindiLailaRegular(mContext, mHolder.nazmAuthorName);
                CommonUtil.setHindiLailaRegular(mContext, mHolder.nazmfirstLine);
            } else if (CommonUtil.languageCode == 3) {
                mHolder.nazmTitle.setText(obj.getString("TU").trim());
                CommonUtil.setUrduNotoNataliq(mContext, mHolder.nazmTitle);
                CommonUtil.setUrduNotoNataliq(mContext, mHolder.nazmAuthorName);
                CommonUtil.setUrduNotoNataliq(mContext, mHolder.nazmfirstLine);
                mHolder.nazmAuthorName.setText(obj.getString("PU").trim());
                mHolder.nazmfirstLine.setText(obj.getString("SU").trim());
            } else {
                mHolder.nazmTitle.setText(obj.getString("TE").trim());
                mHolder.nazmAuthorName.setText(obj.getString("PE").trim());
                mHolder.nazmfirstLine.setText(obj.getString("SE").trim());
                CommonUtil.setEnglishLatoXBold(mContext, mHolder.nazmTitle);
                CommonUtil.setEnglishLatoXBold(mContext, mHolder.nazmAuthorName);
                CommonUtil.setEnglishLatoXRegualr(mContext, mHolder.nazmfirstLine);

            }

            mHolder.addtofav = (ImageView) view.findViewById(R.id.offlineFavIcon);
            mHolder.audioIcon = (ImageView) view.findViewById(R.id.audioLink);
            mHolder.youtubeIcon = (ImageView) view.findViewById(R.id.youtubeLink);

            if (obj.getInt("AC") > 0) {
                mHolder.audioIcon.setVisibility(View.VISIBLE);
            } else {
                mHolder.audioIcon.setVisibility(View.GONE);
            }

            if (obj.getInt("VC") > 0) {
                mHolder.youtubeIcon.setVisibility(View.VISIBLE);
            } else {
                mHolder.youtubeIcon.setVisibility(View.GONE);
            }

            if (!CommonUtil.isSkipped) {
                if (CommonUtil.isContentUsersFav(obj.getString("I"))) {
                    mHolder.addtofav.setImageResource(R.drawable.ic_favorited);
                    mHolder.addtofav.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                }
            }

            mHolder.addtofav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(mContext);
                    } else {
                        try {

                            if (CommonUtil.isContentUsersFav(obj.getString("I"))) {
                                CommonUtil.removeFromFavTroughApi(mContext, obj.getString("I"));
                                mHolder.addtofav.setImageResource(R.drawable.ic_favorite);
                                mHolder.addtofav.setColorFilter(ContextCompat.getColor(mContext, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                            } else {

                                CommonUtil.addToFavTroughApi(mContext, obj.getString("I"));
                                String title;
                                String content;
                                String autherName;

                                if (CommonUtil.languageCode == 1) {
                                    title = obj.getString("TE").trim();
                                    content = obj.getString("SE").trim();
                                    autherName = obj.getString("PE").trim();
                                } else if (CommonUtil.languageCode == 2) {

                                    title = obj.getString("TH").trim();
                                    content = obj.getString("PH").trim();
                                    autherName = obj.getString("SH").trim();

                                } else if (CommonUtil.languageCode == 3) {
                                    title = obj.getString("TE").trim();
                                    content = obj.getString("SE").trim();
                                    autherName = obj.getString("PE").trim();

                                } else {
                                    title = obj.getString("TU").trim();
                                    content = obj.getString("SU").trim();
                                    autherName = obj.getString("PU").trim();
                                }


                                JSONObject jsonObject = new JSONObject(arrayList.getJSONObject(position).toString());

                                Intent intent = new Intent(mContext, NazamContentService.class);
                                intent.putExtra("Ivalue", jsonObject.toString());
                                intent.putExtra("title", title);
                                intent.putExtra("content", content);
                                intent.putExtra("autherName", autherName);
                                mContext.startService(intent);

                                mHolder.addtofav.setImageResource(R.drawable.ic_favorited);
                                mHolder.addtofav.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;

    }

    private class ViewHolder {
        private TextView nazmTitle;
        private TextView nazmfirstLine;
        private TextView nazmAuthorName;
        private ImageView addtofav;
        private ImageView audioIcon;
        private ImageView youtubeIcon;


    }


}


