package org.Rekhta.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.fragments.sherFragments.CollectionSherView;
import org.Rekhta.model.CollectionData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;


public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.RecyclerViewHolder> {

    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoRegular;
    Typeface hinditf;
    Typeface urdutf;
    private ArrayList<CollectionData> arrayList = new ArrayList<>();


    public CollectionAdapter(ArrayList<CollectionData> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        engTfLatoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public CollectionAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sher_top20_header, parent, false);

        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collection_recyclerview_item, parent, false);
        }

        return new CollectionAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CollectionAdapter.RecyclerViewHolder holder, int position) {


        if (position == 0) {

            holder.textView3.setText(LanguageSelection.getSpecificLanguageWordById("Top 20 collections", CommonUtil.languageCode));
            holder.textView1.setText(LanguageSelection.getSpecificLanguageWordById("Top 20 Des", CommonUtil.languageCode));

        } else {
            final CollectionData collectionData = arrayList.get(position - 1);

            if (position % 2 == 0) {
                holder.templateContainer.setBackgroundColor(Color.WHITE);
            } else {
                holder.templateContainer.setBackgroundColor(Color.parseColor("#eeeeee"));
            }

            if (CommonUtil.languageCode == 1) {
                holder.textView.setText(collectionData.getcTitleEng().trim());
                holder.textView.setTypeface(engTfLatoRegular);
                holder.textViewOne.setText(collectionData.getTop20TextEng().trim());
                holder.textViewOne.setTypeface(engTfLatoX);
            } else if (CommonUtil.languageCode == 2) {
                holder.textView.setText(collectionData.getcTitleHin().trim());
                holder.textView.setTypeface(hinditf);
                holder.textViewOne.setText(collectionData.getTop20TextHindi().trim());
                holder.textViewOne.setTypeface(hinditf);
            } else if (CommonUtil.languageCode == 3) {
                holder.textView.setText(collectionData.getcTitleUrdu().trim());
                holder.textView.setTypeface(urdutf);
                holder.textViewOne.setText(collectionData.getTop20TextUrdu().trim());
                holder.textViewOne.setTypeface(urdutf);
            } else {
                holder.textView.setText(collectionData.getcTitleEng());
                holder.textViewOne.setText(collectionData.getTop20TextEng());
                holder.textViewOne.setTypeface(engTfLatoX);
            }

            Glide.with(context).load("https://rekhta.org/Images/Cms/T20/" + collectionData.getImageUrlTxt() + "_Small.png").into(holder.image);
            holder.templateContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDetailSherPage(holder.textView, collectionData.getSherId(), collectionData);
                }
            });
        }
    }

    private void showDetailSherPage(TextView textView, String id, CollectionData collectionData) {

        if (textView != null) {
            bundle.putString("I", id);
            bundle.putString("fromHome", null);
//            ((SherActivity) context).lastSherIDRendered = ((SherActivity) context).currentSherID;
            ((SherActivity) context).idsRendered.add(((SherActivity) context).currentSherID);
            ((SherActivity) context).currentSherID = id;
            CommonUtil.cSherTypeNameEnglish = collectionData.getcTitleEng().trim();
            CommonUtil.cSherTypeNameHindi = collectionData.getcTitleHin().trim();
            CommonUtil.cSherTypeNameUrdu = collectionData.getcTitleUrdu().trim();
            CommonUtil.hasCameFromTag = false;
            CollectionSherView collectionSherView = new CollectionSherView();
            collectionSherView.setArguments(bundle);
            ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.drawer_layout, collectionSherView, "collectionFrag")
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        TextView textViewOne;
        ImageView image;
        LinearLayout templateContainer;
        private TextView textView1, textView3;


        public RecyclerViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);

            textView3 = (TextView) view.findViewById(R.id.top20sherText);
            textView1 = (TextView) view.findViewById(R.id.top20DetailText);
            textView = (TextView) view.findViewById(R.id.sher);
            textViewOne = (TextView) view.findViewById(R.id.no_of_sher);
            image = (ImageView) view.findViewById(R.id.coll_image);
            templateContainer = (LinearLayout) view.findViewById(R.id.top20sher_maintemp_layout);
        }

        @Override
        public void onClick(View v) {

         /*   FragmentManager fragmentManager = ((SherActivity) context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.frame, new CollectionSherView());
            fragmentTransaction.commit();
*/

        }

    }
}
