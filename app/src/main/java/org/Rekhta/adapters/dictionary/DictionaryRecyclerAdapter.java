package org.Rekhta.adapters.dictionary;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.model.dictionary.RekhtaDictionaryData;
import org.Rekhta.utils.CommonUtil;


public class DictionaryRecyclerAdapter extends RecyclerView.Adapter<DictionaryRecyclerAdapter.RecyclerViewHolder> {

    private ArrayList<RekhtaDictionaryData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;

    public DictionaryRecyclerAdapter(ArrayList<RekhtaDictionaryData> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public DictionaryRecyclerAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
     /*   View view = null;
        if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dictionary_header_template, parent, false);
        }else{*/
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_dictionary, parent, false);
//        }

        return new DictionaryRecyclerAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DictionaryRecyclerAdapter.RecyclerViewHolder holder, final int position) {

     /*   if (position == 0) {
            String text = "<font color=#000000>Showing results for </font> <font color=#00bfff>"+CommonUtil.serachedText +"</font>";
            holder.dic_headerTextView.setText(Html.fromHtml(text));
        } else {*/

        final RekhtaDictionaryData rekhtaDictionaryData = arrayList.get(position);

        if (position % 2 == 0) {
            holder.mainrekhtaemplate.setBackgroundColor(Color.WHITE);
        } else {
            holder.mainrekhtaemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }
        if (CommonUtil.languageCode == 1) {

        } else if (CommonUtil.languageCode == 2) {


        } else if (CommonUtil.languageCode == 3) {

        } else {

        }

        holder.dic_mainTextEng.setText(rekhtaDictionaryData.getWordInEng());

        // for hindi Text
        if (rekhtaDictionaryData.getWordInHin().toString().equalsIgnoreCase("null")) {
            holder.dotSymbol.setVisibility(View.GONE);
            holder.dic_mainTextHin.setVisibility(View.GONE);
        } else {
            holder.dic_mainTextHin.setText(rekhtaDictionaryData.getWordInHin());
            holder.dic_mainTextHin.setTypeface(hinditf);
        }

        // for Urdu text
        if (rekhtaDictionaryData.getWordInUrdu().toString().equalsIgnoreCase("null")) {
            holder.dotSymbol.setVisibility(View.GONE);
            holder.dic_mainTextUrdu.setVisibility(View.GONE);
        } else {
            holder.dic_mainTextUrdu.setText(rekhtaDictionaryData.getWordInUrdu());
            holder.dic_mainTextUrdu.setTypeface(urdutf);
        }


        holder.dic_MeaningTxt1.setText(rekhtaDictionaryData.getMeaning1inEng());

          /*  if(dictionaryData.getisAudioAvaialble()){
                holder.dic_audioImgView.setVisibility(View.VISIBLE);
                holder.dic_videoImgView.setVisibility(View.VISIBLE);
            }else{
                holder.dic_audioImgView.setVisibility(View.GONE);
                holder.dic_videoImgView.setVisibility(View.GONE);
            }*/


        if (!rekhtaDictionaryData.getMeaning2inEng().toString().equalsIgnoreCase("null") && !rekhtaDictionaryData.getMeaning2inEng().toString().equalsIgnoreCase("") &&
                !rekhtaDictionaryData.getMeaning2inEng().toString().equalsIgnoreCase(" ")) {
            holder.dic_MeaningTxt2.setVisibility(View.VISIBLE);
            holder.dic_MeaningTxt2.setText(rekhtaDictionaryData.getMeaning2inEng());
        } else {
            holder.dic_MeaningTxt2.setVisibility(View.GONE);
        }
        if (!rekhtaDictionaryData.getMeaning3inEng().toString().equalsIgnoreCase("null") && !rekhtaDictionaryData.getMeaning3inEng().toString().equalsIgnoreCase("") &&
                !rekhtaDictionaryData.getMeaning3inEng().toString().equalsIgnoreCase(" ")) {
            holder.dic_MeaningTxt3.setVisibility(View.VISIBLE);
            holder.dic_MeaningTxt3.setText(rekhtaDictionaryData.getMeaning3inEng());
        } else {
            holder.dic_MeaningTxt3.setVisibility(View.GONE);
        }

        // }

    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        //  TextView dic_headerTextView ;

        TextView dic_mainTextEng;
        TextView dic_mainTextHin;
        TextView dic_mainTextUrdu;
        TextView dic_secondText;
        TextView dic_MeaningTxt1;
        TextView dic_MeaningTxt2;
        TextView dic_MeaningTxt3;

        TextView dotSymbol;

        LinearLayout mainrekhtaemplate;

        /*  ImageView dic_audioImgView;
          ImageView dic_videoImgView;
  */
        public RecyclerViewHolder(View view) {
            super(view);
            // dic_headerTextView = (TextView) view.findViewById(R.id.dictonarYShowingText);
            dic_mainTextEng = (TextView) view.findViewById(R.id.rekhtaDictionaryMainText);
            dic_mainTextHin = (TextView) view.findViewById(R.id.rekhtaDictionaryHindiTxt);
            dic_mainTextUrdu = (TextView) view.findViewById(R.id.rekhtaDictionaryUrduTxt);
            dic_secondText = (TextView) view.findViewById(R.id.rekhtaDictionarySecondText);
            dic_MeaningTxt1 = (TextView) view.findViewById(R.id.rekhtaDictionaryMeaning1);
            dic_MeaningTxt2 = (TextView) view.findViewById(R.id.rekhtaDictionaryMeaning2);
            dic_MeaningTxt3 = (TextView) view.findViewById(R.id.rekhtaDictionaryMeaning3);

            dotSymbol = (TextView) view.findViewById(R.id.dotSymbol);

            mainrekhtaemplate = (LinearLayout) view.findViewById(R.id.mainrekhtaemplate);
      /*      dic_audioImgView = (ImageView) view.findViewById(R.id.rekhtaDictionaryAudioIcon);
            dic_audioImgView = (ImageView) view.findViewById(R.id.rekhtaDictionaryVideoIcon);
       */
        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
