package org.Rekhta.adapters.dictionary;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.model.dictionary.PlattsDictionaryData;
import org.Rekhta.model.dictionary.RekhtaDictionaryData;
import org.Rekhta.utils.CommonUtil;


public class DictionaryPlattsRecyclerAdapter extends RecyclerView.Adapter<DictionaryPlattsRecyclerAdapter.RecyclerViewHolder> {

    private ArrayList<PlattsDictionaryData> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;

    public DictionaryPlattsRecyclerAdapter(ArrayList<PlattsDictionaryData> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        engTfLatoX  = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Black.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public DictionaryPlattsRecyclerAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       /*  View view = null;
       if (viewType == 0) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dictionary_header_template, parent, false);
        }else{*/
          View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.template_platts_dictionary, parent, false);
//        }

        return new DictionaryPlattsRecyclerAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DictionaryPlattsRecyclerAdapter.RecyclerViewHolder holder, final int position) {

        /*if (position == 0) {
            String text = "<font color=#000000>Showing results for </font> <font color=#00bfff>"+CommonUtil.serachedText +"</font>";
            holder.dic_headerTextView.setText(Html.fromHtml(text));
        } else {*/

            final PlattsDictionaryData plattsDictionaryData = arrayList.get(position );


        if (position % 2 == 0) {
            holder.mainplattsDictionarytemplate.setBackgroundColor(Color.WHITE);
        } else {
            holder.mainplattsDictionarytemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }
            if (CommonUtil.languageCode == 1) {
            } else if (CommonUtil.languageCode == 2) {
            } else if (CommonUtil.languageCode == 3) {
            } else {
            }

            holder.dic_mainTextEng.setText(plattsDictionaryData.getWordInEnglish());

            holder.dic_mainTextHin.setText(plattsDictionaryData.getWordInHindi());

            holder.dic_mainTextHin.setTypeface(hinditf);

            holder.dic_mainTextUrdu.setText(plattsDictionaryData.getWordInUrdu());
            holder.dic_mainTextUrdu.setTypeface(urdutf);

          //  holder.dic_MeaningTxt1.setText(plattsDictionaryData.getMeaningHtml());
            holder.dic_MeaningTxt1.setText(Html.fromHtml(plattsDictionaryData.getMeaningHtml()));

    //    }

    }
    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        TextView dic_mainTextEng;
        TextView dic_mainTextHin;
        TextView dic_mainTextUrdu;
        TextView dic_secondText;
        TextView dic_MeaningTxt1;

        LinearLayout mainplattsDictionarytemplate;



    public RecyclerViewHolder(View view) {
            super(view);

            dic_mainTextEng     = (TextView) view.findViewById(R.id.rekhtaDictionaryMainText);
            dic_mainTextHin     = (TextView) view.findViewById(R.id.rekhtaDictionaryHindiTxt);
            dic_mainTextUrdu    = (TextView) view.findViewById(R.id.rekhtaDictionaryUrduTxt);
            dic_secondText      = (TextView) view.findViewById(R.id.rekhtaDictionarySecondText);
            dic_MeaningTxt1     = (TextView) view.findViewById(R.id.rekhtaDictionaryMeaning1);

        mainplattsDictionarytemplate =(LinearLayout) view.findViewById(R.id.mainplattsDictionarytemplate);

      }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
