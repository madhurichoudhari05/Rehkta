package org.Rekhta.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.model.homeScreenModel.ImageTags;


public class MoreDialogItemAdapter extends BaseAdapter {

    Context context;
    List<ImageTags> arrayList;
    LayoutInflater layoutInflater;

    public MoreDialogItemAdapter(Context context, List<ImageTags> arrayList) {

        this.context = context;
        this.arrayList = arrayList;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        view = layoutInflater.inflate(R.layout.more_dialog_list_items, null, false);
        TextView textView = (TextView) view.findViewById(R.id.moredialogtext);
        textView.setText(arrayList.get(position).getTagName());
        return view;
    }
}
