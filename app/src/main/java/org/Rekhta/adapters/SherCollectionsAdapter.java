package org.Rekhta.adapters;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.activites.Dashboard;
import org.Rekhta.activites.PoetDetailedActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.activites.View_ProfileActivity;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.sherFragments.CollectionSherView;
import org.Rekhta.fragments.sherFragments.SherGhalContentFragment;
import org.Rekhta.model.LocalModels.AddSher;
import org.Rekhta.model.SherCollection;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.RestClient;
import org.Rekhta.views.MeaningFragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.CLIPBOARD_SERVICE;


public class SherCollectionsAdapter extends RecyclerView.Adapter<SherCollectionsAdapter.RecyclerViewHolder> {


    public boolean hasTranslateOption = false;
    Context context;
    AppDatabase appDatabase;


    boolean value = false;
    boolean anothervalue = false;

    TextView[] textview, newTextview;
    LinearLayout linearlayout, newLinearLayout;
    Typeface engtf, hinditf, urdutf;
    private LinearLayout linear;
    CollectionSherView collectionSherView;


    ViewTreeObserver viewTreeObserver;

    private ArrayList<SherCollection> arrayList = new ArrayList<>();
    private float[] lastTouchDownXY = new float[2];

    public SherCollectionsAdapter(ArrayList<SherCollection> arrayList, Context context, CollectionSherView collectionSherView) {
        this.arrayList = arrayList;
        this.context = context;
        this.collectionSherView = collectionSherView;
        appDatabase = MyApplicationClass.getDb();


    }

    @Override
    public SherCollectionsAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == 0) {
            if (CommonUtil.hasCameFromTag) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tag_header, parent, false);
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sher_header_template, parent, false);
            }
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collectionheader_recyl_item, parent, false);
        }
        return new SherCollectionsAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (position == 0) {
            if (!CommonUtil.cSherTypeNameEnglish.toLowerCase().contains("shayari")) {
                // holder.sherTypeId.setText(CommonUtil.cSherTypeNameEnglish + " shayari");
                // holder.sherTypeId.setText(CommonUtil.cSherTypeNameEnglish + " " + LanguageSelection.getSpecificLanguageWordById("shayari", CommonUtil.languageCode));
                setHeaderText(holder.sherTypeId);
                //holder.sherTypeId.setText(CommonUtil.cSherTypeNameEnglish.trim());
            } else {
                setHeaderText(holder.sherTypeId);
                //holder.sherTypeId.setText(CommonUtil.cSherTypeNameEnglish.trim());
            }

            holder.totalSher.setText(LanguageSelection.getSpecificLanguageWordById("top 20", CommonUtil.languageCode) + " " + CommonUtil.cSherTotalCount);

        } else {

            final SherCollection sherCollection = arrayList.get(position - 1);


            if (CommonUtil.languageCode == 1) {

                setSherData(sherCollection.getShertextInENG(), holder);
                holder.poetName.setText(sherCollection.getPoetNameInEng());
                CommonUtil.setEnglishMerriweatherBoldFont(context, holder.poetName);
                //  wordClicklisterner(holder.sherText, sherCollection.getShertextInENG());
                int widthSpec = View.MeasureSpec.makeMeasureSpec(holder.pagerLinearSher.getWidth(), View.MeasureSpec.EXACTLY);
                int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                holder.pagerLinearSher.measure(widthSpec, heightSpec);
                Log.e("height", holder.pagerLinearSher.getMeasuredHeight() + "--");
                if (DoSherHasTranlation(sherCollection.getShertextInENG())) {
                    holder.sherTranslateIcon.setVisibility(View.VISIBLE);
                    holder.sherTranslateIcon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (holder.sherTranslatedTextView.getVisibility() == View.GONE) {
                                holder.sherTranslatedTextView.setText(formatTranslatedText(sherCollection.getShertextInENG()));
                                holder.sherTranslatedTextView.setVisibility(View.VISIBLE);

                            } else {
                                holder.sherTranslatedTextView.setVisibility(View.GONE);
                            }
                        }
                    });
                } else {
                    holder.sherTranslateIcon.setVisibility(View.GONE);
                }

            } else if (CommonUtil.languageCode == 2) {

                setSherData(sherCollection.getShertextInHIN(), holder);
                holder.poetName.setText(sherCollection.getPoetNameInHIN());
                CommonUtil.setHindiFont(context, holder.poetName);
                // wordClicklisterner(holder.sherText, sherCollection.getShertextInHIN());
                holder.sherTranslateIcon.setVisibility(View.GONE);

            } else if (CommonUtil.languageCode == 3) {

                setSherData(sherCollection.getShertextInURDU(), holder);
                holder.poetName.setText(sherCollection.getPoetNameInURDU());
                CommonUtil.setUrduNotoNataliq(context, holder.poetName);

                //wordClicklisterner(holder.sherText, sherCollection.getShertextInURDU());
                holder.sherTranslateIcon.setVisibility(View.GONE);
            } else {

                holder.poetName.setText(sherCollection.getPoetNameInEng());
                CommonUtil.setEnglishMerriweatherBoldFont(context, holder.poetName);
                // wordClicklisterner(holder.sherText, sherCollection.getShertextInENG());
            }

            //open poet Info Activity
            holder.poetName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, PoetDetailedActivity.class);

                    intent.putExtra("poetId", sherCollection.getPoetId());
                    intent.putExtra("ghazalCount", "" + "");
                    intent.putExtra("nazmCount", "" + "");
                    intent.putExtra("sherCount", "" + "");
                    intent.putExtra("shortDescInEng", "");
                    intent.putExtra("shortDescInHin", "");
                    intent.putExtra("shortDescInUrdu", "");
                    context.startActivity(intent);

                }
            });

            // for fav icon
            if (CommonUtil.isSkipped) {
                holder.sherFavIcon.setImageResource(R.drawable.ic_favorite);
                holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
            } else {
                if (CommonUtil.isContentUsersFav(sherCollection.getShercontentId())) {
                    holder.sherFavIcon.setImageResource(R.drawable.ic_favorited);
                    holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                } else {
                    holder.sherFavIcon.setImageResource(R.drawable.ic_favorite);
                    holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                }
            }


            holder.sherFavIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(context);
                    } else {
                        if (CommonUtil.isContentUsersFav(sherCollection.getShercontentId())) {
                            CommonUtil.removeFromFavTroughApi(context, sherCollection.getShercontentId());
                            holder.sherFavIcon.setImageResource(R.drawable.ic_favorite);
                            holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                        } else {
                            //add to local.......................................................
                            String sherContent = null, poetName = null;

                            if (CommonUtil.languageCode == 1) {

                                sherContent = sherCollection.getShertextInENG();
                                poetName = sherCollection.getPoetNameInEng();

                            } else if (CommonUtil.languageCode == 2) {
                                sherContent = sherCollection.getShertextInHIN();
                                poetName = sherCollection.getPoetNameInHIN();

                            } else if (CommonUtil.languageCode == 3) {
                                sherContent = sherCollection.getShertextInURDU();
                                poetName = sherCollection.getPoetNameInURDU();
                            }

                            CommonUtil.addToFavTroughApi(context, sherCollection.getShercontentId());

                            final AddSher addSher = new AddSher();
                            addSher.setSher_content(sherContent);
                            addSher.setSher_auther(poetName);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    appDatabase.sherDao().insert(addSher);
                                }
                            }).start();

                            holder.sherFavIcon.setImageResource(R.drawable.ic_favorited);
                            holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                        }
                    }
                }
            });

            //for share Icon
            holder.sherShareIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(context);
                    } else {
                        if (CommonUtil.languageCode == 1) {
                            shareTheSherUrl(sherCollection.getShareUrlEng());
                        } else if (CommonUtil.languageCode == 2) {
                            shareTheSherUrl(sherCollection.getShareUrlHIN());
                        } else if (CommonUtil.languageCode == 3) {
                            shareTheSherUrl(sherCollection.getShareUrlURDU());
                        } else {
                            shareTheSherUrl(sherCollection.getShareUrlEng());
                        }
                    }

                }
            });

            // for copy to CipBoard Icon
            holder.sherClipboard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.languageCode == 1) {
                        CopyToClipBoard(formatText(sherCollection.getShertextInENG()));
                    } else if (CommonUtil.languageCode == 2) {
                        CopyToClipBoard(formatText(sherCollection.getShertextInHIN()));
                    } else if (CommonUtil.languageCode == 3) {
                        CopyToClipBoard(formatText(sherCollection.getShertextInURDU()));
                    } else {
                        CopyToClipBoard(formatText(sherCollection.getShertextInENG()));
                    }
                }
            });

          /*  //for Search Icon
            holder.sherSearchIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // write the code
                }
            });

*/
            int totalTags = sherCollection.getTagArrayLength();
            String firstTagName = getTagName(sherCollection.getSherTagArray(), CommonUtil.languageCode, 0);
            final JSONArray tagArray = sherCollection.getSherTagArray();

            if (totalTags == 0) {
                holder.sherFirstTag.setVisibility(View.GONE);
                holder.sherTagIcon.setVisibility(View.GONE);
            } else {
                holder.sherFirstTag.setText(firstTagName);
                holder.sherFirstTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            ((SherActivity) context).lastRenderSherName.add(CommonUtil.cSherTypeNameEnglish);
                            showDetailSherPage(getTagName(sherCollection.getSherTagArray(), CommonUtil.languageCode, 0), tagArray.getJSONObject(0).getString("I"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            if (totalTags == 0 || (totalTags - 1) == 0) {
                holder.shermoreTag.setVisibility(View.GONE);
                holder.shersandText.setVisibility(View.GONE);
            } else if ((totalTags - 1) == 1) {
                holder.shersandText.setText(",");
                holder.shermoreTag.setText(getTagName(sherCollection.getSherTagArray(), CommonUtil.languageCode, 1));
                holder.shermoreTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            ((SherActivity) context).lastRenderSherName.add(CommonUtil.cSherTypeNameEnglish);
                            showDetailSherPage(getTagName(sherCollection.getSherTagArray(), CommonUtil.languageCode, 1), tagArray.getJSONObject(1).getString("I"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                holder.shersandText.setVisibility(View.VISIBLE);
                holder.shermoreTag.setText((totalTags - 1) + " more");
                holder.shermoreTag.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PopupMenu popup = new PopupMenu(context, holder.sherFirstTag);
                        for (int i = 1; i < tagArray.length(); i++) {
                            try {
                                if (CommonUtil.languageCode == 1) {
                                    popup.getMenu().add(i, i, i, tagArray.getJSONObject(i).getString("NE"));
                                }
                                if (CommonUtil.languageCode == 2) {
                                    popup.getMenu().add(i, i, i, tagArray.getJSONObject(i).getString("NH"));
                                }
                                if (CommonUtil.languageCode == 3) {
                                    popup.getMenu().add(i, i, i, tagArray.getJSONObject(i).getString("NU"));
                                }
//                              popup.getMenu().add(tagArray.getJSONObject(i).getString("NE"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        popup.getMenuInflater().inflate(R.menu.sher_tag_menu, popup.getMenu());
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                try {
                                    ((SherActivity) context).lastRenderSherName.add(CommonUtil.cSherTypeNameEnglish);
                                    showDetailSherPage(item.getTitle().toString(), tagArray.getJSONObject(item.getGroupId()).getString("I"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }
                        });

                        popup.show();
                    }
                });
            }

            if (sherCollection.getGhazalId() != null) {

                holder.sherGhazalIcon.setVisibility(View.VISIBLE);
                holder.sherGhazalIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SherGhalContentFragment sherGhalContentFragment = new SherGhalContentFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("sentFrom", "CollectionSher");
                        sherGhalContentFragment.setArguments(bundle);
//                        ghazalDetails.setArguments(bundle);
//                        Bundle bundle = new Bundle();
//                        bundle.putString("ghazalId",sherCollection.getGhazalId());
//                        sherGhalContentFragment.setArguments(bundle);
                        if (context instanceof Dashboard) {
                            ((Dashboard) context).idGhazalToRender = sherCollection.getGhazalId();
                            ((Dashboard) context).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.drawer_layout, sherGhalContentFragment, "sherActivityFrag                                                            ")
                                    .addToBackStack(null)
                                    .commit();
                        } else if (context instanceof SherActivity) {
                            ((SherActivity) context).idGhazalToRender = sherCollection.getGhazalId();
                            ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.drawer_layout, sherGhalContentFragment, "sherActivityFrag                                                            ")
                                    .addToBackStack(null)
                                    .commit();
                        }
                    }
                });
            }


//            Log.e(position + "getlay", "" + holder.sherText.getLayout().getLineCount());
            /*if (CommonUtil.languageCode == 1) {
                if (holder.sherText.getLayout().getLineCount() > 101) {
//                Log.e(position + "if condn", "" + holder.sherText.getLineCount());
                    holder.sherText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12.5f);
                }
            }*/
        }
    }

    private void setHeaderText(TextView sherTypeId) {

        if (CommonUtil.languageCode == 1) {
            sherTypeId.setText(CommonUtil.cSherTypeNameEnglish.trim());
        } else if (CommonUtil.languageCode == 2) {
            sherTypeId.setText(CommonUtil.cSherTypeNameHindi.trim());
        } else if (CommonUtil.languageCode == 3) {
            sherTypeId.setText(CommonUtil.cSherTypeNameUrdu.trim());
        }
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size() + 1;
    }

    public String formatText(String data) {
        String str = "";
        try {
            Log.v("DataData", data);
            JSONObject obj = new JSONObject(data);
            JSONArray Parray = obj.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {
                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {

                  /* if (j == 1) {
                        str = str + "\n";
                    }*/
                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    if (CommonUtil.languageCode != 3) {

                        for (int k = 0; k < Warray.length(); k++) {
                            str = str + " " + Warray.getJSONObject(k).getString("W");
                        }

                    } else {

                        for (int k = 0; k < Warray.length(); k++) {
                            str = str + " " + Warray.getJSONObject(k).getString("W");
                            //str = str + " " + Warray.getJSONObject(k).getString("W");
                        }
                        //str = str + "\n";
                        Log.v("STR1", str);
                    }
                    str = str + "\n";
                    //Log.v("STR2", str);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    public String formatTranslatedText(String data) {
        String str = "";
        try {
            JSONObject obj = new JSONObject(data);
            JSONArray Parray = obj.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {
                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("T");
                for (int j = 0; j < Larray.length(); j++) {
                    if (j == 1) {
                        str = str + "\n";
                    }
                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    if (CommonUtil.languageCode != 3) {

                        for (int k = 0; k < Warray.length(); k++) {
                            str = str + " " + Warray.getJSONObject(k).getString("W");
                        }

                    } else {
                        for (int k = Warray.length() - 1; k >= 0; k--) {
                            str = str + " " + Warray.getJSONObject(k).getString("W");
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Spanned justifiedString = Html.fromHtml("<body style='text-align: justify; -moz-text-align-last: center;text-align-last: justify;'>"+str+"</body>");
//        return justifiedString.toString() ;
        return str;
    }

    public void shareTheSherUrl(String shareUrl) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "https://Rekhta.org");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void CopyToClipBoard(String sherContetText) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Sher", sherContetText);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }

    public void wordClicklisterner(final TextView textView, final String data) {
        float x = 0, y = 0;
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int mOffset = textView.getOffsetForPosition(lastTouchDownXY[0], lastTouchDownXY[1]);
                String word = findWordForRightHanded(textView.getText().toString(), mOffset);
                String wordCode = getWordCodeForMeaning(data, word);
            /* Toast.makeText(context, ""+word+
                     "--"+wordCode, Toast.LENGTH_SHORT).show();*/
                showMeaningDialog(word, wordCode);
            }
        });

        textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                shareParaGraphText(textView.getText().toString());
                return false;
            }
        });

        textView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    lastTouchDownXY[0] = event.getX();
                    lastTouchDownXY[1] = event.getY();
                }
                return false;
            }
        });

    }

    public void shareParaGraphText(String paratoShare) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = paratoShare;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "https://Rekhta.org");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }

    private String findWordForRightHanded(String str, int offset) { // when you touch ' ', this method returns left word.
        if (str.length() == offset) {
            offset--; // without this code, you will get exception when touching end of the text
        }

        if (str.charAt(offset) == ' ') {
            offset--;
        }
        int startIndex = offset;
        int endIndex = offset;

        try {
            while (str.charAt(startIndex) != ' ' && str.charAt(startIndex) != '\n') {
                startIndex--;
            }
        } catch (StringIndexOutOfBoundsException e) {
            startIndex = 0;
        }

        try {
            while (str.charAt(endIndex) != ' ' && str.charAt(endIndex) != '\n') {
                endIndex++;
            }
        } catch (StringIndexOutOfBoundsException e) {
            endIndex = str.length();
        }

        // without this code, you will get 'here!' instead of 'here'
        // if you use only english, just check whether this is alphabet,
        // but 'I' use korean, so i use below algorithm to get clean word.
        char last = str.charAt(endIndex - 1);
        if (last == ',' || last == '.' ||
                last == '!' || last == '?' ||
                last == ':' || last == ';') {
            endIndex--;
        }

        return str.substring(startIndex, endIndex);
    }

    public void showMeaningDialog(String mainText, String mcode) {
  /*  MeaningFragment meaningFragment = (MeaningFragment) ((SherActivity)context).getSupportFragmentManager().findFragmentByTag("meaningFrag");
    if (meaningFragment != null && meaningFragment.isVisible()) {
        ((SherActivity)context).onBackPressed();
       getMeaningOfTheWord(mainText, mcode);

    } else {
*/
        getMeaningOfTheWord(mainText, mcode);
//    }
    }

    public String getWordCodeForMeaning(String data, String wordClicked) {
        String str = "null";
        try {
            JSONObject obj = new JSONObject(data);
            JSONArray Parray = obj.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {
                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {
                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    if (CommonUtil.languageCode != 3) {
                        for (int k = 0; k < Warray.length(); k++) {
                            if (Warray.getJSONObject(k).getString("W").equals(wordClicked.trim())) {
                                str = "" + Warray.getJSONObject(k).getString("M");
                                return str;
                            }
                        }

                    } else {
                        for (int k = Warray.length() - 1; k >= 0; k--) {
                            if (wordClicked.equals(Warray.getJSONObject(k).getString("W"))) {
                                str = Warray.getJSONObject(k).getString("M");
                                return str;
                            }
                        }

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    public void getMeaningOfTheWord(final String mainWord, String word) {
        //getMeaningOfWord
        final ProgressDialog loading = ProgressDialog.show(context, null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        MeaningFragment meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.sherListmainview, meaningFragment, "meaningFrag")
                                .addToBackStack(null)
                                .commit();

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });
    }

    public boolean DoSherHasTranlation(String data) {
        boolean hasTranlation = false;
        try {
            JSONObject obj = new JSONObject(data);
            JSONArray Parray = obj.getJSONArray("P");
            if (Parray.getJSONObject(0).get("T").toString() == "null") {
                hasTranlation = false;
            } else {
                hasTranlation = true;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasTranlation;
    }

    public String getTagName(JSONArray array, int langcode, int position) {
        String str = "";
        if (array.length() != 0) {
            try {
                if (langcode == 1) {
                    str = array.getJSONObject(position).getString("NE");
                }
                if (langcode == 2) {
                    str = array.getJSONObject(position).getString("NH");
                }
                if (langcode == 3) {
                    str = array.getJSONObject(position).getString("NU");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return str;

    }

    private void showDetailSherPage(String TagName, String id) {


        Bundle bundle = new Bundle();
        bundle.putString("I", id);
        ((SherActivity) context).idsRendered.add(((SherActivity) context).currentSherID);
        ((SherActivity) context).currentSherID = id;
        CommonUtil.cSherTypeNameEnglish = TagName;
        CommonUtil.hasCameFromTag = true;
        CollectionSherView collectionSherView = new CollectionSherView();
        collectionSherView.setArguments(bundle);
        ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                .replace(R.id.drawer_layout, collectionSherView, "collectionFrag")
                .addToBackStack(null)
                .commit();

    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        LinearLayout pagerLinearSher, sherTemplatemainview;
        TextView poetName, sherTypeId, totalSher, sherTranslatedTextView, sherFirstTag,
                shersandText, shermoreTag;

        ImageView sherFavIcon, sherShareIcon, sherClipboard, sherTagIcon,
                sherGhazalIcon, sherTranslateIcon;
        //ImageView sherSearchIcon;


        public RecyclerViewHolder(View view) {
            super(view);
            pagerLinearSher = (LinearLayout) view.findViewById(R.id.textsher);

            sherTemplatemainview = (LinearLayout) view.findViewById(R.id.sherTemplatemainview);
            sherTranslatedTextView = (TextView) view.findViewById(R.id.translatedtextsher);
            poetName = (TextView) view.findViewById(R.id.sher_poet_name);
            sherFavIcon = (ImageView) view.findViewById(R.id.sher_heartIcon);
            sherShareIcon = (ImageView) view.findViewById(R.id.sher_shareIcon);
            sherClipboard = (ImageView) view.findViewById(R.id.sher_clipbordIcon);
            sherTagIcon = (ImageView) view.findViewById(R.id.sherTagIcon);
            sherGhazalIcon = (ImageView) view.findViewById(R.id.sher_ghazalIcon);
            // sherSearchIcon = (ImageView) view.findViewById(R.id.sher_searchIcon);
            sherTranslateIcon = (ImageView) view.findViewById(R.id.sher_translateIcon);
            sherTypeId = (TextView) view.findViewById(R.id.sherTypeId);
            totalSher = (TextView) view.findViewById(R.id.textTop20);
            sherFirstTag = (TextView) view.findViewById(R.id.sherFirstTagView);
            shersandText = (TextView) view.findViewById(R.id.shersandText);
            shermoreTag = (TextView) view.findViewById(R.id.shermoreTagView);
        }
    }


    private void setSherData(String title, RecyclerViewHolder holder) {

        holder.pagerLinearSher.removeAllViews();
        String titileSher = title;
        String renderString = "";
        anothervalue = true;
        viewTreeObserver = holder.pagerLinearSher.getViewTreeObserver();
        int minGap = 10;
        newLinearLayout = new LinearLayout(context);
        newLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        newLinearLayout.setOrientation(LinearLayout.VERTICAL);
        newLinearLayout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        try {

            JSONObject jsonObject = new JSONObject(titileSher);
            JSONArray Parray = jsonObject.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {

                    LinearLayout linear = new LinearLayout(context);
                    linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linear.setOrientation(LinearLayout.HORIZONTAL);
                    linear.setPadding(8, 12, 8, 12);


                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    int noOfWords = Warray.length();
                    linear.setWeightSum(noOfWords);

                    newTextview = new TextView[Warray.length()];

                    for (int k = 0; k < Warray.length(); k++) {
                        newTextview[j] = new TextView(context);
                        newTextview[j].setLayoutParams(dim);
                        newTextview[j].setSingleLine(true);
                        newTextview[j].setIncludeFontPadding(false);

                        newTextview[j].setText(" " + Warray.getJSONObject(k).getString("W"));
                        setWeight(newTextview[j]);
                        if (k == 0) {
                            newTextview[j].setGravity(Gravity.START);
                        } else if (k == Warray.length() - 1) {
                            newTextview[j].setGravity(Gravity.END);
                        } else {
                            newTextview[j].setGravity(Gravity.CENTER_HORIZONTAL);
                        }


                        if (CommonUtil.languageCode == 1) {
                            CommonUtil.setEnglishMerriwetherItalicFont(context, newTextview[j]);
                            // newTextview[j].setTextSize(14);
                        } else if (CommonUtil.languageCode == 2) {
                            CommonUtil.setHindiLailaRegular(context, newTextview[j]);
                           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                newTextview[j].setLetterSpacing((float) 0.7);
                            }*/
                            // newTextview[j].setTextSize(16);
                        } else if (CommonUtil.languageCode == 3) {
                            CommonUtil.setUrduFont(context, newTextview[j]);
                            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                newTextview[j].setLetterSpacing((float) 0.7);
                                //newTextview[j].setLetterSpacing(1);

                            }*/
                            //newTextview[j].setTextSize(12);
                        }

                        newTextview[j].setTextColor(Color.parseColor("#000000"));

                        //  newTextview[j].setGravity(Gravity.START);
                        linear.addView(newTextview[j]);
                        //linear.setGravity(Gravity.FILL_HORIZONTAL);

                        final TextView textView = newTextview[j];
                        final JSONObject obj = Warray.getJSONObject(k);
                        newTextview[j].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showMeaningFragment(textView, obj);
                            }
                        });

//                            str = str + " " + Warray.getJSONObject(k).getString("W");
                    }
                    renderString = renderString + "\n";
                    newLinearLayout.addView(linear);
                }
            }

        } catch (Exception e) {

        }


        holder.pagerLinearSher.addView(newLinearLayout);

        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

                /*if (anothervalue) {
                    widhtOfAnotherView();
                    anothervalue = false;
                }*/
                return true;
            }
        });


    }

    private void setWeight(TextView textView) {

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        param.gravity = Gravity.CENTER;
        textView.setLayoutParams(param);

    }

    private void widhtOfAnotherView() {

        int childCount = newLinearLayout.getChildCount();
        int x = newLinearLayout.getWidth();
        for (int j = 0; j < childCount; j++) {
            LinearLayout linearLayout = (LinearLayout) newLinearLayout.getChildAt(j);
            int y = linearLayout.getWidth();
            int z = x - y;
            int count = linearLayout.getChildCount();

            int g = z / count;

            for (int k = 0; k < count; k++) {
                View textView = linearLayout.getChildAt(k);
                int extraPadding = g;
                textView.setPadding(0, 0, extraPadding, 0);
            }
        }


        /*for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = newLinearLayout.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                int curWidthDiff = newLinearLayout.getWidth() - lineView.getWidth();
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
//                    textView.setBackgroundColor(Color.parseColor("#FFE800"));
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        textView.setPadding(0, 0, extraPadding, 0);
                    }
                }

                //TextView
            }
        }*/
    }

    private void showMeaningFragment(TextView textView, JSONObject obj) {

        collectionSherView.showmeaningFragment(textView, obj);

    }


}

