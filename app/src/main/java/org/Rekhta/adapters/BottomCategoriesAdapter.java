package org.Rekhta.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.activites.Dashboard;
import org.Rekhta.activites.Home_Bottom_Explore_Display;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.fragments.sherFragments.CollectionSherView;
import org.Rekhta.fragments.sherFragments.SherGhalContentFragment;
import org.Rekhta.model.homeScreenModel.T20Series;
import org.Rekhta.utils.CommonUtil;


public class BottomCategoriesAdapter extends RecyclerView.Adapter {

    Context context;
    LayoutInflater layoutInflater;
    String lang;
    List<T20Series> t20Series;
    Bundle bundle = new Bundle();
    String colors[] = {"#347ebf", "#690893", "#ec17d3", "#d9c924", "#f17619", "#1cc1c4"};

    public BottomCategoriesAdapter(Context context, List<T20Series> t20Series, String lang) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.lang = lang;
        this.t20Series = t20Series;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.bottom_cat_items, null, false);

        BottomCatHolder categoryViewHolder = new BottomCatHolder(view);

        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final BottomCatHolder bottomCatHolder = (BottomCatHolder) holder;

        if (position < 6) {
            bottomCatHolder.cardView.setCardBackgroundColor(Color.parseColor(colors[position]));
        }

        if (lang.equals("1")) {
            bottomCatHolder.textView.setText(t20Series.get(position).getName_En().trim());
            CommonUtil.setEnglishLatoRegularFont(context, bottomCatHolder.textView);
        } else if (lang.equals("2")) {
            bottomCatHolder.textView.setText(t20Series.get(position).getName_Hi().trim());
            CommonUtil.setHindiFont(context, bottomCatHolder.textView);
        } else if (lang.equals("3")) {
            bottomCatHolder.textView.setText(t20Series.get(position).getName_Ur().trim());
            CommonUtil.setUrduNotoNataliq(context, bottomCatHolder.textView);
        }
        bottomCatHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonUtil.cSherTypeNameEnglish = t20Series.get(position).getName_En();

                bundle.putString("I", t20Series.get(position).getId());
                bundle.putString("fromHome", "");
//            ((SherActivity) context).lastSherIDRendered = ((SherActivity) context).currentSherID;
                    ((Dashboard) context).idsRendered.add(((Dashboard) context).currentSherID);
                    ((Dashboard) context).currentSherID = t20Series.get(position).getId();
                    CommonUtil.cSherTypeNameEnglish = t20Series.get(position).getName_En();
                    CommonUtil.cSherTypeNameHindi = t20Series.get(position).getName_Hi();
                    CommonUtil.cSherTypeNameUrdu = t20Series.get(position).getName_Ur();
                    CommonUtil.hasCameFromTag = false;
                    CollectionSherView collectionSherView = new CollectionSherView();
                    collectionSherView.setArguments(bundle);
                    ((Dashboard) context).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.drawer_layout, collectionSherView, "collectionFrag")
                            .addToBackStack(null)
                            .commit();

                //context.startActivity(new Intent(context, Home_Bottom_Explore_Display.class).putExtra("sher_Id", t20Series.get(position).getId()));

            }
        });
    }

    @Override
    public int getItemCount() {
        return t20Series.size();
    }

    private class BottomCatHolder extends RecyclerView.ViewHolder {

        TextView textView;
        CardView cardView;

        public BottomCatHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.categoty_txt_bottom);
            cardView = (CardView) itemView.findViewById(R.id.bottomCardView);
        }
    }


}
