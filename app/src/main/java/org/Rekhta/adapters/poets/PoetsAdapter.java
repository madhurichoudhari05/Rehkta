package org.Rekhta.adapters.poets;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.Rekhta.fragments.poetsFragments.PoetsAllFragment;
import org.Rekhta.fragments.poetsFragments.PoetsTopReadFragment;
import org.Rekhta.fragments.poetsFragments.PoetsWomenFragment;
import org.Rekhta.fragments.poetsFragments.PoetsYoungFragment;
import org.Rekhta.fragments.poetsFragments.PoetsClassicalFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;


public class PoetsAdapter extends FragmentPagerAdapter {
    public PoetsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override

    public Fragment getItem(int position) {
        Fragment f = null;
        if (position == 0) {
            f = new PoetsAllFragment();
        } else if (position == 1) {
            f = new PoetsTopReadFragment();
        } else if (position == 2) {
            f = new PoetsWomenFragment();
        } else if (position == 3) {
            f = new PoetsYoungFragment();
        } else if (position == 4) {
            f = new PoetsClassicalFragment();
        }
        return f;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return LanguageSelection.getSpecificLanguageWordById("all", CommonUtil.languageCode);
        } else if (position == 1) {
            return LanguageSelection.getSpecificLanguageWordById("top", CommonUtil.languageCode);

        } else if (position == 2) {
            return LanguageSelection.getSpecificLanguageWordById("womans_poets", CommonUtil.languageCode);

        } else if (position == 3) {
            return LanguageSelection.getSpecificLanguageWordById("Young Poets", CommonUtil.languageCode);

        } else if (position == 4) {
            return LanguageSelection.getSpecificLanguageWordById("Classical Poets", CommonUtil.languageCode);

        }
        return super.getPageTitle(position);
    }
}
