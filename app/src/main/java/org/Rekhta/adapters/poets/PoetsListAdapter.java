package org.Rekhta.adapters.poets;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.model.poets.PoetsDataModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RoundedImageView;


public class PoetsListAdapter extends RecyclerView.Adapter<PoetsListAdapter.RecyclerViewHolder> {

    private ArrayList<PoetsDataModel> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();
    Typeface engTfLatoX;
    Typeface engTfLatoBold;
    Typeface hinditf;
    Typeface urdutf;

    public PoetsListAdapter(ArrayList<PoetsDataModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;

        engTfLatoX = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
        engTfLatoBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public PoetsListAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poet_list_template, parent, false);
        return new PoetsListAdapter.RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {

        final PoetsDataModel poetsDataModel = arrayList.get(position);
        int gc = poetsDataModel.getTotalGhazalCount();
        int nc = poetsDataModel.getTotalNazmCount();
        int sc = poetsDataModel.getTotalSherCount();
        String ghazalText = LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode);
        String nazmText = LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode);
        String sherText = LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode);

        if (position % 2 == 0) {
//            holder.fullTemplate.setBackgroundColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.fullTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
            }
        } else {
//            holder.fullTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.fullTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
            }

        }

        if (CommonUtil.languageCode == 1) {
            holder.poetName.setText(poetsDataModel.getPoetNameInEng());
            holder.poetName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            holder.ghazalCountTextview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
            holder.nazmCountTextview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
            holder.sherCountTextview.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
            holder.poetName.setTypeface(engTfLatoX);
            holder.poetDescriptionTV.setTypeface(engTfLatoBold);
            if (poetsDataModel.getPoetDescInEng().equalsIgnoreCase("") || poetsDataModel.getPoetDescInEng().toString() == "null") {
                holder.poetDescriptionTV.setVisibility(View.GONE);
            } else {
                holder.poetDescriptionTV.setVisibility(View.VISIBLE);
                holder.poetDescriptionTV.setText(poetsDataModel.getPoetDescInEng());
            }

        } else if (CommonUtil.languageCode == 2) {
            holder.poetName.setText(poetsDataModel.getPoetNameInHin());
            holder.poetName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            holder.poetName.setTypeface(hinditf);
            holder.poetDescriptionTV.setTypeface(hinditf);
            if (poetsDataModel.getPoetDescInHin().equalsIgnoreCase("") || poetsDataModel.getPoetDescInHin().toString() == "null") {
                holder.poetDescriptionTV.setVisibility(View.GONE);
            } else {
                holder.poetDescriptionTV.setVisibility(View.VISIBLE);
                holder.poetDescriptionTV.setText(poetsDataModel.getPoetDescInHin());
            }
        } else {
            holder.poetName.setTypeface(urdutf);
            holder.poetName.setText(poetsDataModel.getPoetNameInUrdu());
            holder.poetName.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            holder.poetDescriptionTV.setTypeface(urdutf);
            if (poetsDataModel.getPoetDescInUrdu().equalsIgnoreCase("") || poetsDataModel.getPoetDescInUrdu().toString().equalsIgnoreCase("null")) {
                holder.poetDescriptionTV.setVisibility(View.GONE);
            } else {
                holder.poetDescriptionTV.setVisibility(View.VISIBLE);
                holder.poetDescriptionTV.setText(poetsDataModel.getPoetDescInUrdu());
            }

        }


        // for setting Total Count
        if (gc == 0) {
            holder.ghazalCountTextview.setVisibility(View.GONE);
        } else {
            holder.ghazalCountTextview.setVisibility(View.VISIBLE);
            holder.ghazalCountTextview.setText(gc + " " + ghazalText);
        }
        if (nc == 0) {
            holder.nazmCountTextview.setVisibility(View.GONE);
        } else {
            holder.nazmCountTextview.setVisibility(View.VISIBLE);
            if (gc == 0) {
                holder.nazmCountTextview.setText(nc + " " + nazmText);
            } else {
                holder.nazmCountTextview.setText("\u2022" + " " + nc + " " + nazmText);
            }
        }
        if (sc == 0) {
            holder.sherCountTextview.setVisibility(View.GONE);
        } else {
            holder.sherCountTextview.setVisibility(View.VISIBLE);
            if (nc == 0 && gc == 0) {
                holder.sherCountTextview.setText(sc + " " + sherText);
            } else {
                holder.sherCountTextview.setText("\u2022" + " " + sc + " " + sherText);
            }

        }

        //  Glide.with(context).load(R.drawable.bg).into(holder.poetImg);
        String imageUrl = poetsDataModel.getPoetImgUrl();

        if (imageUrl != null && !imageUrl.equals("")) {

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.default_profile);
            requestOptions.error(R.drawable.default_profile);

                Glide.with(context)
                        .setDefaultRequestOptions(requestOptions).
                        load("https://rekhtacdn.azureedge.net/Images/Shayar/" + imageUrl + ".png").into(holder.poetImg);

        }

        if (poetsDataModel.isPoetNew()) {
            holder.isNewTextview.setVisibility(View.VISIBLE);
        } else {
            holder.isNewTextview.setVisibility(View.GONE);
        }

        holder.fullTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPoetDetailedActivity(poetsDataModel.getPoetID(), poetsDataModel.getTotalGhazalCount(),
                        poetsDataModel.getTotalNazmCount(), poetsDataModel.getTotalSherCount(),
                        poetsDataModel.getPoetDescInEng().toString(), poetsDataModel.getPoetDescInHin().toString(),
                        poetsDataModel.getPoetDescInUrdu().toString());
            }
        });

    }

/*    *//* private void showDetailSherPage(TextView textView,String id) {

         if(textView != null) {
             bundle.putString("I",id);
 //            ((SherActivity) context).lastSherIDRendered = ((SherActivity) context).currentSherID;
             ((SherActivity) context).idsRendered.add(((SherActivity) context).currentSherID);
             ((SherActivity) context).currentSherID =id;
             CommonUtil.cSherTypeNameEnglish = textView.getText().toString();
             CommonUtil.hasCameFromTag = false;
             CollectionSherView collectionSherView = new CollectionSherView();
             collectionSherView.setArguments(bundle);
             ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                     .replace(R.id.drawer_layout, collectionSherView, "collectionFrag")
                     .addToBackStack(null)
                     .commit();
         }
     }
 */

    public void startPoetDetailedActivity(String poetId, int ghazalCount, int nazmCount, int sherCount, String descInEng, String descInHin, String descInUrdu) {


        Intent intent = new Intent(context, PoetDetailedActivity.class);
        intent.putExtra("poetId", poetId);
        intent.putExtra("ghazalCount", ghazalCount + "");
        intent.putExtra("nazmCount", nazmCount + "");
        intent.putExtra("sherCount", sherCount + "");
        intent.putExtra("shortDescInEng", descInEng);
        intent.putExtra("shortDescInHin", descInHin);
        intent.putExtra("shortDescInUrdu", descInUrdu);

        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView poetName;
        TextView isNewTextview;
        TextView ghazalCountTextview;
        TextView nazmCountTextview;
        TextView sherCountTextview;
        TextView poetDescriptionTV;

        ImageView poetImg;
        RelativeLayout fullTemplate;

        public RecyclerViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);

            poetName = (TextView) view.findViewById(R.id.poetListpoetName);
            isNewTextview = (TextView) view.findViewById(R.id.poetListIsnewText);
            poetDescriptionTV = (TextView) view.findViewById(R.id.poetDescriptionTv);
            ghazalCountTextview = (TextView) view.findViewById(R.id.poetListghazalText);
            nazmCountTextview = (TextView) view.findViewById(R.id.poetListnazmText);
            sherCountTextview = (TextView) view.findViewById(R.id.poetListSherText);
            poetImg = (ImageView) view.findViewById(R.id.poetListPoetImg);
            fullTemplate = (RelativeLayout) view.findViewById(R.id.poetTemplate);
        }

        @Override
        public void onClick(View v) {

         /*   FragmentManager fragmentManager = ((SherActivity) context).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.frame, new CollectionSherView());
            fragmentTransaction.commit();
*/

        }

    }

    public void updateList(ArrayList<PoetsDataModel> searchedArrayList) {
        this.arrayList = searchedArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        }
        return 1;
    }
}
