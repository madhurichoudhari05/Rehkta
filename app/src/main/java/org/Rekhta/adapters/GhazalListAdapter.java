package org.Rekhta.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.Rekhta.R;;
import org.Rekhta.Services.GhazalContentService;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;


public class GhazalListAdapter extends BaseAdapter {

    TextView ghazalTitle, ghazalAuthorName;
    ImageView youtubeIcon, addtofav, audioIcon;
    AppDatabase appDatabase;
    private Context mContext;
    private JSONArray arrayList;


    public GhazalListAdapter(Context c, JSONArray list) {
        mContext = c;
        arrayList = list;

        appDatabase = MyApplicationClass.getDb();
    }

    @Override
    public int getCount() {
        return arrayList.length();
    }

    public void updateData(JSONArray array) {
        arrayList = new JSONArray();
        arrayList = array;
        this.notifyDataSetChanged();
    }

    @Override
    public Object getItem(int i) {
        JSONObject obj = null;
        try {
            obj = arrayList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View view = null;
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final JSONObject obj = (JSONObject) getItem(position);
            final ViewHolder mHolder = new ViewHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.listview_template, null);
                view.setTag(mHolder);
            } else {
                view = (View) view.getTag();
            }
            view.setBackgroundColor(Color.WHITE);
            if (position % 2 == 0) {
//                view.setBackgroundColor(Color.WHITE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(mContext.getResources().getDrawable(R.drawable.poet_selector));
                }

            } else {
//                view.setBackgroundColor(Color.parseColor("#eeeeee"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    view.setBackground(mContext.getResources().getDrawable(R.drawable.poet_selector_light_black));
                }
            }
            mHolder.ghazalTitle = (TextView) view.findViewById(R.id.ghazalTitle);
            mHolder.ghazalAuthorName = (TextView) view.findViewById(R.id.ghazalAuthorName);

            if (CommonUtil.languageCode == 1) {

                mHolder.ghazalTitle.setText(obj.getString("TE").trim());
                mHolder.ghazalAuthorName.setText(obj.getString("PE").trim());
                CommonUtil.setEnglishLatoXRegualr(mContext, mHolder.ghazalTitle);
                CommonUtil.setEnglishLatoXRegualr(mContext, mHolder.ghazalAuthorName);
              //  mHolder.ghazalAuthorName.setTextSize(R.dimen._10ssp);

            } else if (CommonUtil.languageCode == 2) {

                mHolder.ghazalTitle.setText(obj.getString("TH").trim());
                mHolder.ghazalAuthorName.setText(obj.getString("PH").trim());
               // mHolder.ghazalAuthorName.setTextSize(R.dimen._14ssp);
                CommonUtil.setHindiLailaRegular(mContext, mHolder.ghazalTitle);
                CommonUtil.setHindiLailaRegular(mContext, mHolder.ghazalAuthorName);

            } else if (CommonUtil.languageCode == 3) {

                String cc = obj.getString("TU").trim();
                mHolder.ghazalTitle.setText(obj.getString("TU").trim().trim());
                CommonUtil.setUrduNotoNataliq(mContext, mHolder.ghazalTitle);
                CommonUtil.setUrduNotoNataliq(mContext, mHolder.ghazalAuthorName);
                mHolder.ghazalAuthorName.setText(obj.getString("PU").trim().trim());
              //  mHolder.ghazalAuthorName.setTextSize(R.dimen._14ssp);

            } else {

                mHolder.ghazalTitle.setText(obj.getString("TE").trim());
                mHolder.ghazalAuthorName.setText(obj.getString("PE").trim());
                CommonUtil.setEnglishLatoXRegualr(mContext, mHolder.ghazalTitle);
                CommonUtil.setEnglishLatoXRegualr(mContext, mHolder.ghazalAuthorName);
            }

            mHolder.addtofav = (ImageView) view.findViewById(R.id.offlineFavIcon);
            mHolder.audioIcon = (ImageView) view.findViewById(R.id.audioLink);
            mHolder.youtubeIcon = (ImageView) view.findViewById(R.id.youtubeLink);

            if (obj.getInt("AC") > 0) {
                mHolder.audioIcon.setVisibility(View.VISIBLE);
            } else {
                mHolder.audioIcon.setVisibility(View.GONE);
            }

            if (obj.getInt("VC") > 0) {
                mHolder.youtubeIcon.setVisibility(View.VISIBLE);
            } else {
                mHolder.youtubeIcon.setVisibility(View.GONE);
            }

            if (!CommonUtil.isSkipped) {
                if (CommonUtil.isContentUsersFav(obj.getString("I"))) {

                    mHolder.addtofav.setImageResource(R.drawable.ic_favorited);
                    mHolder.addtofav.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                }
            }


            mHolder.addtofav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(mContext);
                    } else {
                        try {
                            if (CommonUtil.isContentUsersFav(obj.getString("I"))) {
                                CommonUtil.removeFromFavTroughApi(mContext, obj.getString("I"));
                                mHolder.addtofav.setImageResource(R.drawable.ic_favorite);
                                mHolder.addtofav.setColorFilter(ContextCompat.getColor(mContext, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                            } else {
                                CommonUtil.addToFavTroughApi(mContext, obj.getString("I"));
                                mHolder.addtofav.setImageResource(R.drawable.ic_favorited);
                                mHolder.addtofav.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);


                                String title;
                                String auther;

                                if (CommonUtil.languageCode == 1) {
                                    title = obj.getString("TE");
                                    auther = obj.getString("PE");

                                } else if (CommonUtil.languageCode == 2) {
                                    title = obj.getString("TH");
                                    auther = obj.getString("PH");

                                } else if (CommonUtil.languageCode == 3) {
                                    title = obj.getString("TU");
                                    auther = obj.getString("PU");

                                } else {
                                    title = obj.getString("TE");
                                    auther = obj.getString("PE");
                                }

                                JSONObject jsonObject = new JSONObject(arrayList.getJSONObject(position).toString());
                                Intent intent = new Intent(mContext, GhazalContentService.class);

                                intent.putExtra("ghazalTitle", title);
                                intent.putExtra("ghazalAuther", auther);
                                intent.putExtra("jsonObject", jsonObject.toString());

                                mContext.startService(intent);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;

    }

    private class ViewHolder {

        private TextView ghazalTitle;
        private TextView ghazalAuthorName;
        private ImageView addtofav;
        private ImageView audioIcon;
        private ImageView youtubeIcon;


    }


}
