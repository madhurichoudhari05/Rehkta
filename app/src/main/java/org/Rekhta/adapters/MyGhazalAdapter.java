package org.Rekhta.adapters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;


import org.Rekhta.fragments.myFavorite.MyFavoriteGhzals;
import org.Rekhta.fragments.myFavorite.MyNazamFagment;

public class MyGhazalAdapter extends FragmentStatePagerAdapter {


    public MyGhazalAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = MyFavoriteGhzals.newInstance();
        } else if (position == 1) {
            fragment = MyNazamFagment.newInstance();
        }else if (position==2)
        {
            fragment = MyNazamFagment.newInstance();
        }

        return fragment;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Ghazal";
        } else if (position == 1) {
            title = "Nazam";
        } else if (position == 2) {
            title = "Saved Images";
        }
        return title;
    }
}
