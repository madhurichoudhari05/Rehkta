package org.Rekhta.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.fragments.sherFragments.CollectionSherView;
import org.Rekhta.model.Tags;
import org.Rekhta.utils.CommonUtil;

/**
 * Created by Admin on 9/27/2017.
 */

public class TagRecyclerViewAdapter extends RecyclerView.Adapter<TagRecyclerViewAdapter.RecyclerViewHolder> {

    private ArrayList<Tags> arrayList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();

    public TagRecyclerViewAdapter(ArrayList<Tags> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }
    @Override
    public TagRecyclerViewAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tags_recyclerview_item, parent, false);
        return new RecyclerViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final TagRecyclerViewAdapter.RecyclerViewHolder holder, int position) {
        final Tags tags = arrayList.get(position);

        if(CommonUtil.languageCode == 1){
            holder.gazalText.setText(tags.getGazaltextInEnglish().trim());
            CommonUtil.setEnglishLatoBoldFont(context,holder.gazalText);
        }else if(CommonUtil.languageCode == 2){
            holder.gazalText.setText(tags.getGazaltextInHindi().trim());
            CommonUtil.setHindiFont(context, holder.gazalText);
        }else if(CommonUtil.languageCode == 3){
            holder.gazalText.setText(tags.getGazaltextInUrdu().trim());
            CommonUtil.setUrduFont(context, holder.gazalText);
        }else{
            holder.gazalText.setText(tags.getGazaltextInEnglish());
            CommonUtil.setEnglishLatoBoldFont(context,holder.gazalText);
        }
        CommonUtil.setEnglishLatoRegularFont(context,holder.gazalCount);
        holder.gazalCount.setText(tags.getCount());

        holder.tagListtemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDetailSherPage(holder.gazalText,tags.getTagId());
            }
        });


    }

    public void updateList(ArrayList<Tags> searchedArrayList){
        this.arrayList = searchedArrayList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }
    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView gazalText;
        TextView gazalCount;
        LinearLayout tagListtemplate;
        TextView noRestultTag;
        public RecyclerViewHolder(View view) {
            super(view);
            gazalText = (TextView) view.findViewById(R.id.gazal_text);
            gazalCount = (TextView) view.findViewById(R.id.gazal_count);
            noRestultTag = (TextView) view.findViewById(R.id.noRestultTag);
            tagListtemplate = (LinearLayout) view.findViewById(R.id.tagListtemplate);
        }
    }

    private void showDetailSherPage(TextView textView,String id) {

        if(textView != null) {
            //bundle.putString("I",id);
            bundle.putString("fromHome", null);
            CommonUtil.cSherTypeNameEnglish = textView.getText().toString();
            CommonUtil.hasCameFromTag = true;
            ((SherActivity) context).idsRendered.add(((SherActivity) context).currentSherID);
            ((SherActivity) context).currentSherID =id;
            CollectionSherView collectionSherView = new CollectionSherView();
            collectionSherView.setArguments(bundle);
            ((SherActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.drawer_layout, collectionSherView, "collectionFrag")
                    .addToBackStack(null)
                    .commit();
        }
    }
}
