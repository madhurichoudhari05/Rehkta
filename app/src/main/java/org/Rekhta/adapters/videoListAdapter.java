package org.Rekhta.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.Rekhta.R;;
import org.Rekhta.utils.CommonUtil;


public class videoListAdapter extends BaseAdapter {
    private Context mContext;
    private JSONArray arrayList;


    public videoListAdapter(Context c, JSONArray list) {
        mContext = c;
        arrayList = list;

    }

    @Override
    public int getCount() {
        return arrayList.length();
    }


    @Override
    public Object getItem(int i) {
        JSONObject obj = null;
        try {
            obj = arrayList.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        View view = null;
        try {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final JSONObject obj = (JSONObject) getItem(position);
            final videoListAdapter.ViewHolder mHolder = new videoListAdapter.ViewHolder();
            if (view == null) {

                view = inflater.inflate(R.layout.video_list_template, null);
                view.setTag(mHolder);
            } else {
                view = (View) view.getTag();
            }

            mHolder.videoAuthorName = (TextView) view.findViewById(R.id.videAuthor);
            mHolder.authorIamge = (ImageView) view.findViewById(R.id.ImageOfAuthor);

            mHolder.videoAuthorName.setText(obj.getString("AN").toUpperCase());
            Glide.with(mContext)
                    .load("https://rekhta.org" + obj.getString("IU"))
                    .into(mHolder.authorIamge);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;

    }

    private class ViewHolder {
        private TextView videoTitle;
        private TextView videoAuthorName;
        private ImageView authorIamge;

    }


}
