package org.Rekhta.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.activites.Dashboard;
import org.Rekhta.model.homeScreenModel.CarouselsModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;


public class HomepageAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    List<CarouselsModel> carouselsModels;
    String lang;
    String baseUrl;
    Dashboard dashboard;


    public HomepageAdapter(Context context, List<CarouselsModel> carouselsModels, String lang, Dashboard dashboard) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.carouselsModels = carouselsModels;
        this.lang = lang;
        this.dashboard = dashboard;
        baseUrl = "https://world.rekhta.org/Images/MobileApp/Android/HDPI/";

    }

    @Override
    public int getCount() {
        return carouselsModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, final int position) {

        RelativeLayout itemView = (RelativeLayout) layoutInflater.inflate(R.layout.home_header_pager_items, null, false);

        TextView title1 = (TextView) itemView.findViewById(R.id.headerTitle);
        TextView title2 = (TextView) itemView.findViewById(R.id.headrsubTitle);
        TextView header_pager_text = (TextView) itemView.findViewById(R.id.header_pager_text);
        header_pager_text.setText(LanguageSelection.getSpecificLanguageWordById("Featured", CommonUtil.languageCode));
        ImageView caroselbackImage = (ImageView) itemView.findViewById(R.id.caroselbackImage);
        ImageView caroselShare = (ImageView) itemView.findViewById(R.id.caroselShare);



        caroselShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dashboard.shareURL(carouselsModels.get(position).getTU());
            }
        });

        //String poetName = carouselsModels.get(position).getBLE();
        if (lang.equals("1")) {
            title2.setText(carouselsModels.get(position).getBLE());
            title1.setText(carouselsModels.get(position).getBE());

            CommonUtil.setEnglishLatoRegularFont(context, title1);
            CommonUtil.setEnglishLatoRegularFont(context, title2);
            CommonUtil.setEnglishLatoBoldFont(context, header_pager_text);

        } else if (lang.equals("2")) {
            title2.setText(carouselsModels.get(position).getBLH());
            title1.setText(carouselsModels.get(position).getBH());


            CommonUtil.setHindiFont(context, title1);
            CommonUtil.setHindiFont(context, title2);
            CommonUtil.setHindiFont(context, header_pager_text);

        } else if (lang.equals("3")) {
            title2.setText(carouselsModels.get(position).getBLU());
            title1.setText(carouselsModels.get(position).getBU());


            CommonUtil.setUrduFont(context, title1);
            CommonUtil.setUrduFont(context, title2);
            CommonUtil.setUrduFont(context, header_pager_text);
        }
        String url = baseUrl + carouselsModels.get(position).getBI() + ".jpg";

        if (url != null) {
            Glide.with(context).load(url).into(caroselbackImage);
        }

        collection.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //super.destroyItem(container, position, object);
    }
}
