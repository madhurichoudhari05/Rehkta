package org.Rekhta.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.Rekhta.activites.Dashboard;
import org.Rekhta.views.MeaningFragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.model.homeScreenModel.TodaysTop;
import org.Rekhta.utils.CommonUtil;


public class FeaturedPagerAdapter extends PagerAdapter {


    Context context;
    List<TodaysTop> todaysTopsModell;
    LayoutInflater layoutInflater;
    Dashboard dashboard;
    LinearLayout pagerLinearSher;
    LinearLayout linearlayout, newLinearLayout;
    boolean value = false;
    boolean anothervalue = false;

    TextView[] textview, newTextview;
    Typeface engtf, hinditf, urdutf;
    private TextView home_sher_txt;

    public FeaturedPagerAdapter(Context context, List<TodaysTop> todaysTopsModel, Dashboard dashboard) {

        this.context = context;
        this.todaysTopsModell = todaysTopsModel;
        layoutInflater = LayoutInflater.from(context);
        this.dashboard = dashboard;

        engtf = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_light_italic.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    @Override
    public int getCount() {
        return todaysTopsModell.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, final int position) {

        LinearLayout itemView = null;


        itemView = (LinearLayout) layoutInflater.inflate(R.layout.featured_sher_items, null, false);

        //TextView sherContent = (TextView) itemView.findViewById(R.id.featured_sher_content);

        pagerLinearSher = (LinearLayout) itemView.findViewById(R.id.pagerLinearSher);


        Typeface engtf = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_light_italic.ttf");

        setSherData(todaysTopsModell.get(position).getTitle());

        /*newLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (anothervalue)

                    widhtOfAnotherView();
                // anothervalue = false;
            }
        });*/

        newLinearLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

               /* if (anothervalue)

                    widhtOfAnotherView();*/

                return true;
            }
        });

        dashboard.changePoetName(position, todaysTopsModell);
        dashboard.changeFavIcon(position, todaysTopsModell);

        collection.addView(itemView);
        return itemView;
    }

    private void setSherNewData(String title, TextView home_sher_txt) {

        String renderString = "";

        try {

            JSONObject jsonObject = new JSONObject(title);
            JSONArray Parray = jsonObject.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {


                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");

                    for (int k = 0; k < Warray.length(); k++) {

                        renderString = renderString + " " + Warray.getJSONObject(k).getString("W");

                    }
                    renderString = renderString + "\n";

                }
                home_sher_txt.setText(renderString);
                if (CommonUtil.languageCode == 1) {
                    CommonUtil.setEnglishMerriwetherItalicFont(context, home_sher_txt);
                } else if (CommonUtil.languageCode == 2) {
                    CommonUtil.setHindiFont(context, home_sher_txt);
                } else if (CommonUtil.languageCode == 3) {
                    CommonUtil.setUrduFont(context, home_sher_txt);
                }
            }
        } catch (Exception e) {

        }

    }


    private void setSherData(String title) {

        pagerLinearSher.removeAllViews();
        String titileSher = title;
        String renderString = "";
        anothervalue = true;
        int minGap = 10;
        newLinearLayout = new LinearLayout(context);
        newLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        newLinearLayout.setOrientation(LinearLayout.VERTICAL);
        newLinearLayout.setGravity(Gravity.CENTER_HORIZONTAL);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        try {

            JSONObject jsonObject = new JSONObject(titileSher);
            JSONArray Parray = jsonObject.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {

                    LinearLayout linear = new LinearLayout(context);
                    linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linear.setOrientation(LinearLayout.HORIZONTAL);
                    linear.setPadding(8, 12, 8, 12);


                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    int noOfWords = Warray.length();
                    linear.setWeightSum(noOfWords);

                    newTextview = new TextView[Warray.length()];

                    for (int k = 0; k < Warray.length(); k++) {
                        newTextview[j] = new TextView(context);
                        newTextview[j].setLayoutParams(dim);
                        newTextview[j].setSingleLine(true);
                        newTextview[j].setIncludeFontPadding(false);

                        newTextview[j].setText(" " + Warray.getJSONObject(k).getString("W"));
                        setWeight(newTextview[j]);
                        if (k == 0) {
                            newTextview[j].setGravity(Gravity.START);
                        } else if (k == Warray.length() - 1) {
                            newTextview[j].setGravity(Gravity.END);
                        } else {
                            newTextview[j].setGravity(Gravity.CENTER_HORIZONTAL);
                        }


                        if (CommonUtil.languageCode == 1) {
                            CommonUtil.setEnglishMerriwetherItalicFont(context, newTextview[j]);
                            // newTextview[j].setTextSize(14);
                        } else if (CommonUtil.languageCode == 2) {
                            CommonUtil.setHindiLailaRegular(context, newTextview[j]);
                           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                newTextview[j].setLetterSpacing((float) 0.7);
                            }*/
                            // newTextview[j].setTextSize(16);
                        } else if (CommonUtil.languageCode == 3) {
                            CommonUtil.setUrduFont(context, newTextview[j]);
                            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                newTextview[j].setLetterSpacing((float) 0.7);
                                //newTextview[j].setLetterSpacing(1);

                            }*/
                            //newTextview[j].setTextSize(12);
                        }

                        newTextview[j].setTextColor(Color.parseColor("#000000"));

                        //  newTextview[j].setGravity(Gravity.START);
                        linear.addView(newTextview[j]);
                        //linear.setGravity(Gravity.FILL_HORIZONTAL);

                        final TextView textView = newTextview[j];
                        final JSONObject obj = Warray.getJSONObject(k);
                        newTextview[j].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showMeaningFragment(textView, obj);
                            }
                        });

//                            str = str + " " + Warray.getJSONObject(k).getString("W");
                    }
                    renderString = renderString + "\n";
                    newLinearLayout.addView(linear);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pagerLinearSher.addView(newLinearLayout);

    }

    private void setWeight(TextView textView) {

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        param.gravity = Gravity.CENTER;
        textView.setLayoutParams(param);

    }


    private void showMeaningFragment(TextView textView, JSONObject obj) {

        MeaningFragment meaningFragment = (MeaningFragment) dashboard.getSupportFragmentManager().findFragmentByTag("meaningFrag");
        if (meaningFragment != null && meaningFragment.isVisible()) {

            // showMeaning(mainText.getText().toString(), obj);

        } else {

            showMeaning(textView.getText().toString(), obj);
        }

    }

    private void showMeaning(String text, JSONObject obj) {

        try {
            dashboard.getMeaningOfTheWord(text, obj.getString("M"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void widhtOfAnotherView() {

        int childCount = newLinearLayout.getChildCount();
        int x = pagerLinearSher.getWidth();
        if (x != 0) {
            anothervalue = false;
        } else {
            anothervalue = true;
        }
        for (int j = 0; j < childCount; j++) {
            LinearLayout linearLayout = (LinearLayout) newLinearLayout.getChildAt(j);
            int y = linearLayout.getWidth();
            int z = x - y;
            int count = linearLayout.getChildCount() - 1;

            int g = z / count;

            for (int k = 0; k < count; k++) {
                View textView = linearLayout.getChildAt(k);
                int extraPadding = g;
                textView.setPadding(0, 0, g, 0);
            }

            float scaleF = (float) newLinearLayout.getWidth() / (float) linearLayout.getWidth();

            // newLinearLayout.setScaleX((float) ((float) (scaleF) + 2.0));
            //newLinearLayout.setScaleY((float) (scaleF));
        }


       /* int childCount = newLinearLayout.getChildCount();
        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = newLinearLayout.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                int curWidthDiff = newLinearLayout.getWidth() - lineView.getWidth();
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
//                    textView.setBackgroundColor(Color.parseColor("#FFE800"));
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        textView.setPadding(0, 0, extraPadding, 0);

                    }
                }

                //TextView
            }
        }*/
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        View view = (View) object;
        ((ViewPager) container).removeView(view);
        view = null;
    }

}
