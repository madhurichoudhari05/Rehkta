package org.Rekhta.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import org.Rekhta.fragments.ghazalFragments.BeginnersNewTab;
import org.Rekhta.fragments.ghazalFragments.ghazal_BeginnersTab;
import org.Rekhta.fragments.ghazalFragments.ghazal_EditorChoiceTab;
import org.Rekhta.fragments.ghazalFragments.ghazal_Top100Tab;
import org.Rekhta.fragments.myFavorite.MyFavoriteGhzals;
import org.Rekhta.fragments.myFavorite.MyNazamFagment;
import org.Rekhta.fragments.myFavorite.MySherFragment;

/**
 * Created by Admin on 1/25/2018.
 */

public class FavPagerAdapter extends FragmentStatePagerAdapter {

    int tabCount;

    public FavPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                MyFavoriteGhzals tab1 = new MyFavoriteGhzals();
                return tab1;
            case 1:
                MyNazamFagment beginnersNewTab = new MyNazamFagment();
                return beginnersNewTab;
            case 2:
                MySherFragment tab3 = new MySherFragment();
                return tab3;

            default:
                return null;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // super.destroyItem(container, position, object);
        getItem(position).onDetach();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Ghazal";
            case 1:
                return "Nazm";
            case 2:
                return "Sher";
        }
        return null;
    }

}
