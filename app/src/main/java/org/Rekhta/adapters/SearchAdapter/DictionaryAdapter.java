package org.Rekhta.adapters.SearchAdapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.model.SearchModel.DictionaryModel;
import org.Rekhta.utils.CommonUtil;



public class DictionaryAdapter extends RecyclerView.Adapter<DictionaryAdapter.ViewHolder> {
    ArrayList<DictionaryModel> dictionaryModelArrayList = new ArrayList<>();
    Context context;

    public DictionaryAdapter(ArrayList<DictionaryModel> dictionaryModelArrayList, Context context) {
        this.dictionaryModelArrayList = dictionaryModelArrayList;
        this.context = context;
    }

    @Override
    public DictionaryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_dictionary_item, parent, false);
        return new DictionaryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DictionaryAdapter.ViewHolder holder, int position) {

        DictionaryModel dictionaryModel = dictionaryModelArrayList.get(position);

        holder.word.setText(dictionaryModel.getDictionaryWordInEnglish().trim());
        holder.hindi.setText(dictionaryModel.getDictionaryWordInHindi().trim());
        holder.urdu.setText(dictionaryModel.getDictionaryWordInUrdu().trim());

        if (!dictionaryModel.getDictionaryMeaning1InEng().equalsIgnoreCase("")) {

            holder.meaning1.setVisibility(View.VISIBLE);

            if (CommonUtil.languageCode == 1) {
                holder.meaning1.setText(dictionaryModel.getDictionaryMeaning1InEng().trim());
            } else if (CommonUtil.languageCode == 2) {
                holder.meaning1.setText(dictionaryModel.getDictionaryMeaning1InHin().trim());
            } else if (CommonUtil.languageCode == 3) {
                holder.meaning1.setText(dictionaryModel.getDictionaryMeaning1InUrdu().trim());
            }
        } else {
            holder.meaning1.setVisibility(View.GONE);
        }
        if (!dictionaryModel.getDictionaryMeaning2InEng().equalsIgnoreCase("") && !dictionaryModel.getDictionaryMeaning2InEng().toString().equalsIgnoreCase("null")) {
            holder.meaning2.setVisibility(View.VISIBLE);
            String meaning = dictionaryModel.getDictionaryMeaning2InEng().trim();
            if (meaning != null) {

                if (CommonUtil.languageCode == 1) {
                    holder.meaning2.setText(meaning);
                } else if (CommonUtil.languageCode == 2) {
                    holder.meaning2.setText(meaning);
                } else if (CommonUtil.languageCode == 3) {
                    holder.meaning2.setText(meaning);
                }
            } else {
                holder.meaning2.setText("Not Available");
            }

        } else {
            holder.meaning2.setVisibility(View.GONE);
        }
        if (!dictionaryModel.getDictionaryMeaning3InEng().equalsIgnoreCase("") && !dictionaryModel.getDictionaryMeaning3InEng().toString().equalsIgnoreCase("null")) {
            holder.meaning3.setVisibility(View.VISIBLE);

            String meaning2 = dictionaryModel.getDictionaryMeaning3InEng().trim();
            if (meaning2 != null) {
                if (CommonUtil.languageCode == 1) {
                    holder.meaning3.setText(meaning2);
                } else if (CommonUtil.languageCode == 2) {
                    holder.meaning3.setText(meaning2);
                } else if (CommonUtil.languageCode == 3) {
                    holder.meaning3.setText(meaning2);
                }
            } else {
                holder.meaning3.setText("Not Available");
            }
        } else {
            holder.meaning3.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return dictionaryModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView word, hindi, urdu, meaning1, meaning2, meaning3;

        public ViewHolder(View itemView) {
            super(itemView);
            word = (TextView) itemView.findViewById(R.id.word);
            hindi = (TextView) itemView.findViewById(R.id.hindi);
            urdu = (TextView) itemView.findViewById(R.id.urdu);
            meaning1 = (TextView) itemView.findViewById(R.id.english1_meaning);
            meaning2 = (TextView) itemView.findViewById(R.id.english2_meaning);
            meaning3 = (TextView) itemView.findViewById(R.id.english3_meaning);
        }
    }
}
