package org.Rekhta.adapters.SearchAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.model.SearchModel.PoetsModel;
import org.Rekhta.utils.RoundedImageView;

/**
 * Created by Admin on 11/14/2017.
 */

public class PoetsSearchAdapter extends RecyclerView.Adapter<PoetsSearchAdapter.ViewHolder> {
    ArrayList<PoetsModel>poetsModelArrayList = new ArrayList<>();
    Context context;

    public PoetsSearchAdapter(ArrayList<PoetsModel> poetsModelArrayList, Context context) {
        this.poetsModelArrayList = poetsModelArrayList;
        this.context = context;
    }

    @Override
    public PoetsSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poets_item, parent, false);
        return new PoetsSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PoetsSearchAdapter.ViewHolder holder, int position) {
        final PoetsModel poetsModel = poetsModelArrayList.get(position);

        if (position % 2 == 0) {
            holder.mainTemplate.setBackgroundColor(Color.WHITE);
        } else {
            holder.mainTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        holder.name.setText(poetsModel.getPoetName());
        holder.secondName.setText(poetsModel.getPoetSEO_Slug());
//        String countText ="1 Ghazal  \u2022  2 Sher";
          String countText ="";

        if(poetsModel.getPoetGazalCount()>0){
            countText = poetsModel.getPoetGazalCount() +" Ghazals ";
        }
        if(poetsModel.getPoetNazmCount()>0){
            if(!countText.equalsIgnoreCase("")){
                countText =countText +"\u2022 "+ poetsModel.getPoetNazmCount() +" Nazms ";
            }else{
                countText =countText + poetsModel.getPoetNazmCount() +" Nazms ";
            }

        }
        holder.noOfGhazals.setText(countText);
        //Picasso.with(context).load(poetsModel.getImage()).into(holder.circleImageView);
        Glide.with(context).load(poetsModel.getPoetImageUrl()).into(holder.circleImageView);

        holder.mainTemplate.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context , PoetDetailedActivity.class);
                intent.putExtra("poetId",poetsModel.getPoetEntityId());
                intent.putExtra("ghazalCount",poetsModel.getPoetGazalCount()+"");
                intent.putExtra("nazmCount",poetsModel.getPoetNazmCount()+"");
                intent.putExtra("sherCount",0+"");
                intent.putExtra("shortDescInEng", " ");
                intent.putExtra("shortDescInHin", " ");
                intent.putExtra("shortDescInUrdu", " ");
                intent.putExtra("shouldLandonprofile",true);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return poetsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name,secondName,noOfGhazals;
        RoundedImageView circleImageView;
        LinearLayout mainTemplate;
        public ViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            secondName=(TextView)itemView.findViewById(R.id.second_name);
            noOfGhazals=(TextView)itemView.findViewById(R.id.no_of_ghazal);
            circleImageView=(RoundedImageView) itemView.findViewById(R.id.image);
            mainTemplate=(LinearLayout) itemView.findViewById(R.id.searchPoetMainView);
        }
    }
}
