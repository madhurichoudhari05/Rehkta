package org.Rekhta.adapters.SearchAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.Rekhta.fragments.SearchFragment.All;
import org.Rekhta.fragments.SearchFragment.GazalSearch;
import org.Rekhta.fragments.SearchFragment.NazmSearch;
import org.Rekhta.fragments.SearchFragment.PoetsSearch;
import org.Rekhta.fragments.SearchFragment.SherSearch;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

/**
 * Created by Admin on 11/14/2017.
 */

public class SearchViewPagerAdapter extends FragmentPagerAdapter {


    public SearchViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {

            return new All();
        } else if (position == 1) {
            return new PoetsSearch();

        } else if (position == 2) {
            return new GazalSearch();

        } else if (position == 3) {
            return new NazmSearch();

        } else if (position == 4) {
            return new SherSearch();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

            case 0:
                return LanguageSelection.getSpecificLanguageWordById("all", CommonUtil.languageCode).toUpperCase();
            case 1:
                return LanguageSelection.getSpecificLanguageWordById("poets", CommonUtil.languageCode).toUpperCase();

            case 2:
                return LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode).toUpperCase();

            case 3:
                return LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode).toUpperCase();

            case 4:
                return LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode).toUpperCase();

            default:
                return "";
        }
    }

    @Override
    public int getItemPosition(Object object) {

        return POSITION_NONE;
    }
}