package org.Rekhta.adapters.SearchAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.Rekhta.adapters.TotalContentAdapter;
import org.Rekhta.fragments.SearchFragment.SearchGhazalContentRendering;
import org.Rekhta.model.SearchModel.ContentModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.Services.NazamContentService;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.fragments.SearchFragment.SearchNazmContentRendering;
import org.Rekhta.model.SearchModel.NazmSearchModel;
import org.Rekhta.utils.CommonUtil;

/**
 * Created by Admin on 11/14/2017.
 */

public class NazmSearchAdapter extends RecyclerView.Adapter<NazmSearchAdapter.ViewHolder> {
    ArrayList<NazmSearchModel> contentList = new ArrayList<>();
    Context context;

    public NazmSearchAdapter(ArrayList<NazmSearchModel> contentList, Context context) {
        this.contentList = contentList;
        this.context = context;
    }

    @Override
    public NazmSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_content_templatae, parent, false);
        return new NazmSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NazmSearchAdapter.ViewHolder holder, final int position) {

        final NazmSearchModel nazmSearchModel = contentList.get(position);

        if (position % 2 == 0) {
            holder.mainTemplate.setBackgroundColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.mainTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.mainTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
            }
            holder.mainTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        holder.mainTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchGhazalContentRendering searchGhazalContentRendering = new SearchGhazalContentRendering();
                ((SearchActivity) context).idGhazalToRender = nazmSearchModel.getNazmId();
                ((SearchActivity) context).currentGhazalPoetName = nazmSearchModel.getNazmPoetName();
                ((SearchActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.searchActivity_mainView, searchGhazalContentRendering, "searchGhazalContentRendering")
                        .addToBackStack(null)
                        .commit();
            }
        });

        //holder.type.setText(contentModel.getContentPoetSlug());
        holder.poetName.setText(nazmSearchModel.getNazmPoetName());
        String bodyData = nazmSearchModel.getNazmBody().replace("<br/>", "\n");
        holder.content.setText(bodyData);
        CommonUtil.setEnglishLatoBoldFont(context, holder.content);

        String typeId = nazmSearchModel.getNazmTypeId();

        holder.ghazalType.setVisibility(View.GONE);
        String title = nazmSearchModel.getNazmTitle();
        holder.ghazalSubject.setText(title.trim());

        setFont(context, holder);

        if (!CommonUtil.isSkipped) {
            if (CommonUtil.isContentUsersFav(nazmSearchModel.getNazmId())) {
                holder.favIcon.setImageResource(R.drawable.ic_favorited);
                holder.favIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

            }
        }


        holder.favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(context);
                } else {
                    if (!CommonUtil.isContentUsersFav(nazmSearchModel.getNazmId())) {
                        holder.favIcon.setImageResource(R.drawable.ic_favorited);
                        holder.favIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                        CommonUtil.addToFavTroughApi(context, nazmSearchModel.getNazmId());
                        //add to local here.................................

                        String title;
                        String content = null;
                        String autherName;

                        if (CommonUtil.languageCode == 1) {
                            title = nazmSearchModel.getNazmTitle().trim();
                            autherName = nazmSearchModel.getNazmPoetName().trim();
                            content = nazmSearchModel.getNazmBody();

                        } else if (CommonUtil.languageCode == 2) {

                            title = nazmSearchModel.getNazmTitle().trim();
                            autherName = nazmSearchModel.getNazmPoetName().trim();
                            content = nazmSearchModel.getNazmBody();

                        } else if (CommonUtil.languageCode == 3) {
                            title = nazmSearchModel.getNazmTitle().trim();
                            autherName = nazmSearchModel.getNazmPoetName().trim();
                            content = nazmSearchModel.getNazmBody();

                        } else {
                            title = nazmSearchModel.getNazmTitle().trim();
                            autherName = nazmSearchModel.getNazmPoetName().trim();
                            content = nazmSearchModel.getNazmBody();
                        }


                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(nazmSearchModel.getNazmBody());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(context, NazamContentService.class);
                        intent.putExtra("Ivalue", nazmSearchModel.getNazmId());
                        intent.putExtra("title", title);
                        intent.putExtra("content", content);
                        intent.putExtra("autherName", autherName);
                        context.startService(intent);


                        //CommonUtil.removeFromFavPref(contentModel.getContentId()); // remove this Line when Api starts working
                    } else {
                        holder.favIcon.setImageResource(R.drawable.ic_favorite);
                        holder.favIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                        CommonUtil.removeFromFavTroughApi(context, nazmSearchModel.getNazmId());
                    }
                }


            }
        });

        holder.shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "https://Rekhta.org");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, nazmSearchModel.getNazmContentUrl());
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        if (nazmSearchModel.isNazmEditorChoice()) {
            holder.editorChoiceIcon.setVisibility(View.VISIBLE);
        } else {
            holder.editorChoiceIcon.setVisibility(View.GONE);
        }

        if (nazmSearchModel.isNazmPopularChoice()) {
            holder.popularchoiceIcn.setVisibility(View.VISIBLE);
        } else {
            holder.popularchoiceIcn.setVisibility(View.GONE);
        }

        if (nazmSearchModel.getNazmAudioCount() > 0) {
            holder.audioIcon.setVisibility(View.VISIBLE);
        } else {
            holder.audioIcon.setVisibility(View.GONE);
        }
        if (nazmSearchModel.getNazmVideoCount() > 0) {
            holder.videoIcon.setVisibility(View.VISIBLE);
        } else {
            holder.videoIcon.setVisibility(View.GONE);
        }

    }

    private void setFont(Context context, ViewHolder holder) {

        if (CommonUtil.languageCode == 1) {
            CommonUtil.setEnglishLatoRegularFont(context, holder.ghazalSubject);
            CommonUtil.setEnglishLatoRegularFont(context, holder.ghazalType);
            CommonUtil.setEnglishLatoRegularFont(context, holder.poetName);
            CommonUtil.setEnglishLatoRegularFont(context, holder.content);

            holder.ghazalSubject.setTextSize(14);
            holder.ghazalType.setTextSize(11);
            holder.content.setTextSize(12);
            holder.poetName.setTextSize(11);

        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setHindiLailaRegular(context, holder.ghazalSubject);
            CommonUtil.setHindiLailaRegular(context, holder.ghazalType);
            CommonUtil.setHindiLailaRegular(context, holder.poetName);
            CommonUtil.setHindiLailaRegular(context, holder.content);
            holder.ghazalSubject.setTextSize(17);
            holder.ghazalType.setTextSize(14);
            holder.content.setTextSize(15);
            holder.poetName.setTextSize(14);

        } else if (CommonUtil.languageCode == 3) {
            CommonUtil.setUrduNotoNataliq(context, holder.ghazalSubject);
            CommonUtil.setUrduNotoNataliq(context, holder.ghazalType);
            CommonUtil.setUrduNotoNataliq(context, holder.poetName);
            CommonUtil.setUrduNotoNataliq(context, holder.content);

            holder.ghazalSubject.setTextSize(17);
            holder.ghazalType.setTextSize(14);
            holder.content.setTextSize(15);
            holder.poetName.setTextSize(14);
        }
    }


    @Override
    public int getItemCount() {
        return contentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView content, poetName, ghazalType, ghazalSubject;
        ImageView favIcon, shareIcon, editorChoiceIcon, popularchoiceIcn, audioIcon, videoIcon;
        RelativeLayout mainTemplate;

        public ViewHolder(View itemView) {
            super(itemView);

            content = (TextView) itemView.findViewById(R.id.ghazalTitle);
            poetName = (TextView) itemView.findViewById(R.id.ghazalAuthorName);
            ghazalType = (TextView) itemView.findViewById(R.id.ghazaltype);
            ghazalSubject = (TextView) itemView.findViewById(R.id.subject);

            favIcon = (ImageView) itemView.findViewById(R.id.offlineFavIcon);
            shareIcon = (ImageView) itemView.findViewById(R.id.shareIcon);
            editorChoiceIcon = (ImageView) itemView.findViewById(R.id.searchEditorChoiceIcon);
            popularchoiceIcn = (ImageView) itemView.findViewById(R.id.searchPopularChoiceIcon);
            audioIcon = (ImageView) itemView.findViewById(R.id.searchaudioLink);
            videoIcon = (ImageView) itemView.findViewById(R.id.searchyoutubeLink);


            mainTemplate = (RelativeLayout) itemView.findViewById(R.id.poetList_layout);

        }
    }
}
