package org.Rekhta.adapters.SearchAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.fragments.SearchFragment.SearchGhazalContentRendering;
import org.Rekhta.model.SearchModel.ContentModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

/**
 * Created by Admin on 11/14/2017.
 */

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ViewHolder> {
    ArrayList<ContentModel> contentList = new ArrayList<>();
    Context context;

    public ContentAdapter(ArrayList<ContentModel> contentList, Context context) {
        this.contentList = contentList;
        this.context = context;
    }

    @Override
    public ContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_content_templatae, parent, false);
        return new ContentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContentAdapter.ViewHolder holder, int position) {
        final ContentModel contentModel = contentList.get(position);

        if (position % 2 == 0) {
            holder.mainTemplate.setBackgroundColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.mainTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.mainTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
            }
            holder.mainTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        holder.mainTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchGhazalContentRendering searchGhazalContentRendering = new SearchGhazalContentRendering();
                ((SearchActivity) context).idGhazalToRender = contentModel.getContentId();
                ((SearchActivity) context).currentGhazalPoetName = contentModel.getContentPoetName();
                ((SearchActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.searchActivity_mainView, searchGhazalContentRendering, "searchGhazalContentRendering")
                        .addToBackStack(null)
                        .commit();
            }
        });

        //holder.type.setText(contentModel.getContentPoetSlug());
        holder.poetName.setText(contentModel.getContentPoetName());
        String bodyData = contentModel.getContentBody().replace("<br/>", "\n");
        holder.content.setText(bodyData);
        CommonUtil.setEnglishLatoBoldFont(context, holder.content);

        String typeId = contentModel.getContentTypeId();

        setGhazalType(typeId.toUpperCase(), holder.ghazalType, holder.ghazalSubject);
        String title = contentModel.getContentTitle().trim();
        holder.ghazalSubject.setText(title);

        setFont(context, holder);


        if (CommonUtil.isContentUsersFav(contentModel.getContentId())) {
            holder.favIcon.setImageResource(R.drawable.ic_favorited);
            holder.favIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        }
        holder.favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!CommonUtil.isContentUsersFav(contentModel.getContentId())) {
                    holder.favIcon.setImageResource(R.drawable.ic_favorited);
                    holder.favIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    CommonUtil.addToFavTroughApi(context, contentModel.getContentId());
                    //CommonUtil.removeFromFavPref(contentModel.getContentId()); // remove this Line when Api starts working
                } else {
                    holder.favIcon.setImageResource(R.drawable.ic_favorite);
                    holder.favIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                    CommonUtil.removeFromFavTroughApi(context, contentModel.getContentId());
                }
            }
        });

        holder.shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "https://Rekhta.org");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, contentModel.getContentContentUrl());
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        if (contentModel.isContentEditorChoice()) {
            holder.editorChoiceIcon.setVisibility(View.VISIBLE);
        } else {
            holder.editorChoiceIcon.setVisibility(View.GONE);
        }

        if (contentModel.isContentPopularChoice()) {
            holder.popularchoiceIcn.setVisibility(View.VISIBLE);
        } else {
            holder.popularchoiceIcn.setVisibility(View.GONE);
        }

        if (contentModel.getContentAudioCount() > 0) {
            holder.audioIcon.setVisibility(View.VISIBLE);
        } else {
            holder.audioIcon.setVisibility(View.GONE);
        }
        if (contentModel.getContentVideoCount() > 0) {
            holder.videoIcon.setVisibility(View.VISIBLE);
        } else {
            holder.videoIcon.setVisibility(View.GONE);
        }


    }

    private void setFont(Context context, ViewHolder holder) {

        if (CommonUtil.languageCode == 1) {
            CommonUtil.setEnglishLatoRegularFont(context, holder.ghazalSubject);
            CommonUtil.setEnglishLatoRegularFont(context, holder.ghazalType);
            CommonUtil.setEnglishLatoRegularFont(context, holder.poetName);
            CommonUtil.setEnglishLatoRegularFont(context, holder.content);

            holder.ghazalSubject.setTextSize(14);
            holder.ghazalType.setTextSize(11);
            holder.content.setTextSize(12);
            holder.poetName.setTextSize(11);

        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setHindiLailaRegular(context, holder.ghazalSubject);
            CommonUtil.setHindiLailaRegular(context, holder.ghazalType);
            CommonUtil.setHindiLailaRegular(context, holder.poetName);
            CommonUtil.setHindiLailaRegular(context, holder.content);
            holder.ghazalSubject.setTextSize(17);
            holder.ghazalType.setTextSize(14);
            holder.content.setTextSize(15);
            holder.poetName.setTextSize(14);

        } else if (CommonUtil.languageCode == 3) {
            CommonUtil.setUrduNotoNataliq(context, holder.ghazalSubject);
            CommonUtil.setUrduNotoNataliq(context, holder.ghazalType);
            CommonUtil.setUrduNotoNataliq(context, holder.poetName);
            CommonUtil.setUrduNotoNataliq(context, holder.content);

            holder.ghazalSubject.setTextSize(17);
            holder.ghazalType.setTextSize(14);
            holder.content.setTextSize(15);
            holder.poetName.setTextSize(14);
        }
    }

    private void setGhazalType(String typeId, TextView ghazalType, TextView ghazalSubject) {

        switch (typeId) {
            case CommonUtil.ghazalTypeId:
                ghazalSubject.setVisibility(View.GONE);
                ghazalType.setText(LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode));
                break;

            case CommonUtil.NazmtypeId:
                ghazalSubject.setVisibility(View.VISIBLE);
                ghazalType.setText(LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode));
                break;
            case CommonUtil.sherTypeId:
                ghazalSubject.setVisibility(View.GONE);
                ghazalType.setText(LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode));
                break;

            default:
                ghazalType.setText("Ghazal");
        }
    }

    @Override
    public int getItemCount() {
        if (contentList.size() >= 5) {
            return 5;
        } else {
            return contentList.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // TextView type;
        TextView content, poetName, ghazalType, ghazalSubject;
        ImageView favIcon, shareIcon, editorChoiceIcon, popularchoiceIcn, audioIcon, videoIcon;
        RelativeLayout mainTemplate;

        public ViewHolder(View itemView) {
            super(itemView);
            //type = (TextView) itemView.findViewById(R.id.type_sher);
            content = (TextView) itemView.findViewById(R.id.ghazalTitle);
            poetName = (TextView) itemView.findViewById(R.id.ghazalAuthorName);
            ghazalType = (TextView) itemView.findViewById(R.id.ghazaltype);
            ghazalSubject = (TextView) itemView.findViewById(R.id.subject);

            favIcon = (ImageView) itemView.findViewById(R.id.offlineFavIcon);
            shareIcon = (ImageView) itemView.findViewById(R.id.shareIcon);
            editorChoiceIcon = (ImageView) itemView.findViewById(R.id.searchEditorChoiceIcon);
            popularchoiceIcn = (ImageView) itemView.findViewById(R.id.searchPopularChoiceIcon);
            audioIcon = (ImageView) itemView.findViewById(R.id.searchaudioLink);
            videoIcon = (ImageView) itemView.findViewById(R.id.searchyoutubeLink);


            mainTemplate = (RelativeLayout) itemView.findViewById(R.id.poetList_layout);
        }
    }
}
