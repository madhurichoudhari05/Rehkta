package org.Rekhta.adapters.SearchAdapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fonts.OswaldBoldTextView;
import org.Rekhta.model.LocalModels.AddSher;
import org.Rekhta.model.SearchModel.NazmSearchModel;
import org.Rekhta.model.SearchModel.SherSearchModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;

import static android.content.Context.CLIPBOARD_SERVICE;


public class SherSearchAdapter extends RecyclerView.Adapter<SherSearchAdapter.ViewHolder> {
    ArrayList<SherSearchModel> contentList = new ArrayList<>();
    Context context;
    private AppDatabase appDatabase;

    public SherSearchAdapter(ArrayList<SherSearchModel> contentList, Context context) {
        this.contentList = contentList;
        this.context = context;
        appDatabase = MyApplicationClass.getDb();
    }

    @Override
    public SherSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_sher_render_layout, parent, false);
        return new SherSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SherSearchModel sherSearchModel = contentList.get(position);

        if (position % 2 == 0) {
            holder.mainTemplate.setBackgroundColor(Color.WHITE);
        } else {
            holder.mainTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        String sherContent = sherSearchModel.getSherBody();
        //  holder.sherText.setText(Html.fromHtml(sherSearchModel.getSherBody().replace("\\r\\n", "\n")));
        holder.sherText.setText(sherSearchModel.getSherBody());

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (CommonUtil.languageCode == 1) {
                holder.sherText.setLetterSpacing(0.02f);
                CommonUtil.setEnglishMeriWeatherItalic(context, holder.sherText);
            } else if (CommonUtil.languageCode == 2) {
                holder.sherText.setLetterSpacing(0.8f);
                CommonUtil.setHindiLailaRegular(context, holder.sherText);
            } else if (CommonUtil.languageCode == 3) {
                holder.sherText.setLetterSpacing(0.8f);
                CommonUtil.setUrduFont(context, holder.sherText);
            }
        }*/


        holder.poetName.setText(sherSearchModel.getSherPoetName());

        holder.poetName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PoetDetailedActivity.class);

                intent.putExtra("poetId", sherSearchModel.getSherPoetId());
                intent.putExtra("ghazalCount", "" + "");
                intent.putExtra("nazmCount", "" + "");
                intent.putExtra("sherCount", "" + "");
                intent.putExtra("shortDescInEng", "");
                intent.putExtra("shortDescInHin", "");
                intent.putExtra("shortDescInUrdu", "");
                context.startActivity(intent);
            }
        });

        holder.sherShareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "https://Rekhta.org");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, sherSearchModel.getSherContentUrl());
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }

        });

        holder.sherTagIcon.setVisibility(View.GONE);
        holder.sherGhazalIcon.setVisibility(View.GONE);
        holder.sherTranslateIcon.setVisibility(View.GONE);
        holder.sherTranslatedTextView.setVisibility(View.GONE);
        holder.sherFirstTag.setVisibility(View.INVISIBLE);
        holder.shersandText.setVisibility(View.INVISIBLE);
        holder.sherFirstTag.setVisibility(View.INVISIBLE);
        holder.shersandText.setVisibility(View.INVISIBLE);
        holder.shermoreTag.setVisibility(View.INVISIBLE);


        holder.sherClipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Sher", sherSearchModel.getSherBody());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, "Copied to clipboard", Toast.LENGTH_SHORT).show();
            }
        });

        // for fav icon

        if (!CommonUtil.isSkipped) {
            if (CommonUtil.isContentUsersFav(sherSearchModel.getSherId())) {
                holder.sherFavIcon.setImageResource(R.drawable.ic_favorited);
                holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            } else {
                holder.sherFavIcon.setImageResource(R.drawable.ic_favorite);
                holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
            }
        }

        holder.sherFavIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(context);
                } else {
                    if (CommonUtil.isContentUsersFav(sherSearchModel.getSherId())) {
                        CommonUtil.removeFromFavTroughApi(context, sherSearchModel.getSherId());
                        holder.sherFavIcon.setImageResource(R.drawable.ic_favorite);
                        holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                    } else {

                        String sherContent = null, poetName = null;

                        if (CommonUtil.languageCode == 1) {

                            sherContent = sherSearchModel.getSherBody();
                            poetName = sherSearchModel.getSherPoetName();

                        } else if (CommonUtil.languageCode == 2) {
                            sherContent = sherSearchModel.getSherBody();
                            poetName = sherSearchModel.getSherPoetName();

                        } else if (CommonUtil.languageCode == 3) {
                            sherContent = sherSearchModel.getSherBody();
                            poetName = sherSearchModel.getSherPoetName();
                        }


                        final AddSher addSher = new AddSher();
                        addSher.setSher_content(sherContent);
                        addSher.setSher_auther(poetName);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                appDatabase.sherDao().insert(addSher);
                            }
                        }).start();


                        CommonUtil.addToFavTroughApi(context, sherSearchModel.getSherId());
                        holder.sherFavIcon.setImageResource(R.drawable.ic_favorited);
                        holder.sherFavIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sherText;
        TextView poetName;
        TextView sherTypeId;

        ImageView sherFavIcon;
        ImageView sherShareIcon;
        ImageView sherClipboard;
        ImageView sherTagIcon;
        ImageView sherGhazalIcon;
        //ImageView sherSearchIcon;
        ImageView sherTranslateIcon;
        TextView sherTranslatedTextView;
        TextView sherFirstTag;
        TextView shersandText;
        TextView shermoreTag;

        LinearLayout mainTemplate;

        public ViewHolder(View view) {
            super(view);
            // type=(TextView)itemView.findViewById(R.id.type_sher);
            sherText = (TextView) view.findViewById(R.id.textsher);
            sherTranslatedTextView = (TextView) view.findViewById(R.id.translatedtextsher);
            poetName = (TextView) view.findViewById(R.id.sher_poet_name);
            sherFavIcon = (ImageView) view.findViewById(R.id.sher_heartIcon);
            sherShareIcon = (ImageView) view.findViewById(R.id.sher_shareIcon);
            sherClipboard = (ImageView) view.findViewById(R.id.sher_clipbordIcon);
            sherTagIcon = (ImageView) view.findViewById(R.id.sherTagIcon);
            sherGhazalIcon = (ImageView) view.findViewById(R.id.sher_ghazalIcon);
            // sherSearchIcon = (ImageView) view.findViewById(R.id.sher_searchIcon);
            sherTranslateIcon = (ImageView) view.findViewById(R.id.sher_translateIcon);
            sherFirstTag = (TextView) view.findViewById(R.id.sherFirstTagView);
            shersandText = (TextView) view.findViewById(R.id.shersandText);
            shermoreTag = (TextView) view.findViewById(R.id.shermoreTagView);
            mainTemplate = (LinearLayout) itemView.findViewById(R.id.sherTemplatemainview);
        }
    }
}
