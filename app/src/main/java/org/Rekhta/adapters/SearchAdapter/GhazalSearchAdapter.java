package org.Rekhta.adapters.SearchAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.Services.GhazalContentService;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.fragments.SearchFragment.SearchGhazalContentRendering;
import org.Rekhta.model.SearchModel.GhazalSearchModel;
import org.Rekhta.utils.CommonUtil;

/**
 * Created by Abdul on 11/14/2017.
 */

public class GhazalSearchAdapter extends RecyclerView.Adapter<GhazalSearchAdapter.ViewHolder> {
    ArrayList<GhazalSearchModel> contentList = new ArrayList<>();
    Context context;

    public GhazalSearchAdapter(ArrayList<GhazalSearchModel> contentList, Context context) {
        this.contentList = contentList;
        this.context = context;
    }

    @Override
    public GhazalSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poetlistview_template, parent, false);
        return new GhazalSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GhazalSearchAdapter.ViewHolder holder, int position) {

        final GhazalSearchModel ghazalSearchModel = contentList.get(position);
//        holder.type.setText(ghazalSearchModel.getGhazalContentSlug());
        holder.ghazalTitle.setText(ghazalSearchModel.getGhazalBody());
        holder.ghazalAuthorName.setText(ghazalSearchModel.getGhazalPoetName());

        if (position % 2 == 0) {
            holder.mainTemplate.setBackgroundColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.mainTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                holder.mainTemplate.setBackground(context.getResources().getDrawable(R.drawable.poet_selector_light_black));
            }
            holder.mainTemplate.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        holder.mainTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchGhazalContentRendering searchGhazalContentRendering = new SearchGhazalContentRendering();
                ((SearchActivity) context).idGhazalToRender = ghazalSearchModel.getGhazalId();
                ((SearchActivity) context).currentGhazalPoetName = ghazalSearchModel.getGhazalPoetName();
                ((SearchActivity) context).getSupportFragmentManager().beginTransaction()
                        .replace(R.id.searchActivity_mainView, searchGhazalContentRendering, "searchGhazalContentRendering")
                        .addToBackStack(null)
                        .commit();
            }
        });

        if (ghazalSearchModel.getGhazalAudioCount() > 0) {
            holder.audioIcon.setVisibility(View.VISIBLE);
        } else {
            holder.audioIcon.setVisibility(View.GONE);
        }

        if (ghazalSearchModel.getGhazalVideoCount() > 0) {
            holder.youtubeIcon.setVisibility(View.VISIBLE);
        } else {
            holder.youtubeIcon.setVisibility(View.GONE);
        }

        if (ghazalSearchModel.isGhazalEditorChoice()) {
            holder.editorChioce.setVisibility(View.VISIBLE);
        } else {
            holder.editorChioce.setVisibility(View.GONE);
        }

        if (ghazalSearchModel.isGhazalPopularChoice()) {
            holder.popularChoice.setVisibility(View.VISIBLE);
        } else {
            holder.popularChoice.setVisibility(View.GONE);
        }


        if (!CommonUtil.isSkipped) {
            if (CommonUtil.isContentUsersFav(ghazalSearchModel.getGhazalId())) {
                holder.addtofav.setImageResource(R.drawable.ic_favorited);
                holder.addtofav.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

            }
        }


        holder.addtofav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(context);
                } else {
                    if (!CommonUtil.isContentUsersFav(ghazalSearchModel.getGhazalId())) {
                        CommonUtil.addToFavTroughApi(context, ghazalSearchModel.getGhazalId());
                        holder.addtofav.setImageResource(R.drawable.ic_favorited);
                        holder.addtofav.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                        //.............add to Local.....................................................

                        String title;
                        String auther;

                        if (CommonUtil.languageCode == 1) {
                            title = ghazalSearchModel.getGhazalTitle();
                            auther = ghazalSearchModel.getGhazalPoetName();

                        } else if (CommonUtil.languageCode == 2) {
                            title = ghazalSearchModel.getGhazalTitle();
                            auther = ghazalSearchModel.getGhazalPoetName();

                        } else if (CommonUtil.languageCode == 3) {
                            title = ghazalSearchModel.getGhazalTitle();
                            auther = ghazalSearchModel.getGhazalPoetName();

                        } else {
                            title = ghazalSearchModel.getGhazalTitle();
                            auther = ghazalSearchModel.getGhazalPoetName();
                        }

                        String ghazalBody = ghazalSearchModel.getGhazalId();
                        JSONObject jsonObject = null;

                        Intent intent = new Intent(context, GhazalContentService.class);

                        intent.putExtra("ghazalTitle", title);
                        intent.putExtra("ghazalAuther", auther);
                        intent.putExtra("jsonObject", ghazalSearchModel.getGhazalId());

                        context.startService(intent);


                    } else {
                        CommonUtil.removeFromFavTroughApi(context, ghazalSearchModel.getGhazalId());
                        holder.addtofav.setImageResource(R.drawable.ic_favorite);
                        holder.addtofav.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);

                    }
                }


            }
        });

        setFonts(context, holder);

        holder.shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "https://Rekhta.org");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, ghazalSearchModel.getGhazalContentUrl());
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
    }

    private void setFonts(Context context, ViewHolder holder) {

        if (CommonUtil.languageCode == 1) {

            CommonUtil.setEnglishLatoRegularFont(context, holder.ghazalTitle);
            CommonUtil.setEnglishLatoRegularFont(context, holder.ghazalAuthorName);


        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setHindiLailaRegular(context, holder.ghazalTitle);
            CommonUtil.setHindiLailaRegular(context, holder.ghazalAuthorName);


        } else if (CommonUtil.languageCode == 3) {
            CommonUtil.setUrduNotoNataliq(context, holder.ghazalTitle);
            CommonUtil.setUrduNotoNataliq(context, holder.ghazalAuthorName);

        }
    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout mainTemplate;
        //        TextView type;
        private TextView ghazalTitle;
        private TextView ghazalAuthorName;
        private ImageView addtofav;
        private ImageView audioIcon, editorChioce, popularChoice;
        private ImageView youtubeIcon;
        private ImageView shareIcon;

        public ViewHolder(View itemView) {
            super(itemView);
//            type=(TextView)itemView.findViewById(R.id.type_sher);
            ghazalTitle = (TextView) itemView.findViewById(R.id.ghazalTitle);
            ghazalAuthorName = (TextView) itemView.findViewById(R.id.ghazalAuthorName);

            addtofav = (ImageView) itemView.findViewById(R.id.offlineFavIcon);
            audioIcon = (ImageView) itemView.findViewById(R.id.audioLink);
            youtubeIcon = (ImageView) itemView.findViewById(R.id.youtubeLink);
            shareIcon = (ImageView) itemView.findViewById(R.id.shareTheContent);

            editorChioce = (ImageView) itemView.findViewById(R.id.searchGhazalEditorChoice);
            popularChoice = (ImageView) itemView.findViewById(R.id.searchGhazalPopularChoice);

            mainTemplate = (RelativeLayout) itemView.findViewById(R.id.searchmaintemp_layout);

        }
    }
}
