package org.Rekhta.adapters.SearchAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.activites.PoetsActivity;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.model.SearchModel.MorePoet;

/**
 * Created by Admin on 11/13/2017.
 */

public class AllAdapter extends RecyclerView.Adapter<AllAdapter.ViewHolder> {
    ArrayList<MorePoet> poetList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();

    public AllAdapter(ArrayList<MorePoet> poetList, Context context) {
        this.poetList = poetList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_poet_item, parent, false);
        return new AllAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MorePoet morePoet = poetList.get(position);
        //Picasso.with(context).load(morePoet.getPoetImage()).into(holder.image);
       Glide.with(context).load(morePoet.getPoetImageUrl()).into(holder.image);
       holder.name.setText(morePoet.getPoetName());

        holder.poetMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context , PoetDetailedActivity.class);
                intent.putExtra("poetId",morePoet.getPoetEntityId());
                intent.putExtra("ghazalCount",morePoet.getPoetGazalCount()+"");
                intent.putExtra("nazmCount",morePoet.getPoetNazmCount()+"");
                intent.putExtra("sherCount",0+"");
                intent.putExtra("shortDescInEng", " ");
                intent.putExtra("shortDescInHin", " ");
                intent.putExtra("shortDescInUrdu", " ");
                intent.putExtra("shouldLandonprofile",false);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return poetList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;
        LinearLayout poetMainView;
        public ViewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            image=(ImageView)itemView.findViewById(R.id.image);
            poetMainView = (LinearLayout) itemView.findViewById(R.id.poetMainView);
        }
    }
}
