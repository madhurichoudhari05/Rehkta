package org.Rekhta.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.Rekhta.fragments.myFavorite.FragmentHistry;
import org.Rekhta.fragments.myFavorite.FragmentMyConnections;
import org.Rekhta.fragments.myFavorite.FragmentSavedImages;


public class MyFavoriteAdapter extends FragmentPagerAdapter {

    public MyFavoriteAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new FragmentMyConnections();

        } else if (position == 1) {
            fragment = new FragmentHistry();
        } else if (position == 2) {
            fragment = new FragmentSavedImages();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "My Collections";
        } else if (position == 1) {
            title = "History";
        } else if (position == 2) {
            title = "Images";
        }
        return title;
    }

}
