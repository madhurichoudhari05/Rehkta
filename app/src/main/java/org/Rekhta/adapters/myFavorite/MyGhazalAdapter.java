package org.Rekhta.adapters.myFavorite;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.activites.OfflineContent;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.myFavorite.MyFavoriteGhzals;
import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;


public class MyGhazalAdapter extends RecyclerView.Adapter {


    Context context;
    LayoutInflater layoutInflater;
    List<AddGhazal> addGhazals;
    AppDatabase appDatabase;
    ArrayList<FavFCModel> favGhazalsList;

    MyFavoriteGhzals myFavoriteGhzals;
    String title;
    String auther;

    public MyGhazalAdapter(Context context, List<AddGhazal> addGhazals, ArrayList<FavFCModel> favGhazalsList, MyFavoriteGhzals myFavoriteGhzals) {

        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.addGhazals = addGhazals;
        appDatabase = MyApplicationClass.getDb();
        this.myFavoriteGhzals = myFavoriteGhzals;
        this.favGhazalsList = favGhazalsList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.single_ghazal_items, null, false);
        MyGhazalViewHolder myGhazalViewHolder = new MyGhazalViewHolder(view);
        return myGhazalViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        MyGhazalViewHolder myGhazalViewHolder = (MyGhazalViewHolder) holder;

        if (position % 2 == 0) {
            myGhazalViewHolder.favGhazalMain.setBackgroundColor(Color.WHITE);
        } else {
            myGhazalViewHolder.favGhazalMain.setBackgroundColor(Color.parseColor("#eeeeee"));
        }

        final String jsonObj;
        if (addGhazals == null) {
            if (CommonUtil.languageCode == 1) {
                title = favGhazalsList.get(position).getTE();
                auther = favGhazalsList.get(position).getPE();
            } else if (CommonUtil.languageCode == 2) {
                title = favGhazalsList.get(position).getTH();
                auther = favGhazalsList.get(position).getPH();
            } else if (CommonUtil.languageCode == 3) {
                title = favGhazalsList.get(position).getTU();
                auther = favGhazalsList.get(position).getPU();
            }

            jsonObj = null;
        } else {
            title = addGhazals.get(position).getTitle();
            auther = addGhazals.get(position).getAuther();
            jsonObj = addGhazals.get(position).getContent_data();
        }

        myGhazalViewHolder.ghzalTitle.setText(title);
        myGhazalViewHolder.ghazalAuther.setText(auther);


        myGhazalViewHolder.addfavGhazal.setImageResource(R.drawable.ic_favorited);
        myGhazalViewHolder.addfavGhazal.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        myGhazalViewHolder.addfavGhazal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myFavoriteGhzals.removeItem(addGhazals, position, appDatabase);

            }
        });

        myGhazalViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (addGhazals == null) {

                    Intent intent = new Intent(context, OfflineContent.class);
                    intent.putExtra("DataArray", jsonObj);
                    intent.putExtra("positionClicked", position);
                    intent.putExtra("listType", "ghazal");
                    intent.putExtra("poetName", auther);
                    intent.putExtra("contentId", favGhazalsList.get(position).getI());
                    intent.putExtra("addGhazalList", (Serializable) favGhazalsList);
                    context.startActivity(intent);

                } else {
                    Intent intent = new Intent(context, OfflineContent.class);
                    intent.putExtra("DataArray", jsonObj);
                    intent.putExtra("positionClicked", position);
                    intent.putExtra("contentId", "");
                    intent.putExtra("listType", "ghazal");
                    intent.putExtra("poetName", addGhazals.get(position).getAuther());
                    intent.putExtra("addGhazalList", (Serializable) addGhazals);
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        if (addGhazals == null) {
            return favGhazalsList.size();
        } else {
            return addGhazals.size();
        }

    }

    private class MyGhazalViewHolder extends RecyclerView.ViewHolder {

        TextView ghzalTitle, ghazalAuther;
        ImageView addfavGhazal;
        RelativeLayout favGhazalMain;

        public MyGhazalViewHolder(View itemView) {
            super(itemView);
            ghzalTitle = (TextView) itemView.findViewById(R.id.offlineGhazalTitle);
            ghazalAuther = (TextView) itemView.findViewById(R.id.offlienGhazalAuthor);
            addfavGhazal = (ImageView) itemView.findViewById(R.id.offlineGhazalFavIcon);
            favGhazalMain = (RelativeLayout) itemView.findViewById(R.id.favGhazalMain);
        }
    }

}
