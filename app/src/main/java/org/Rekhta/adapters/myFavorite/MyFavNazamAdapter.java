package org.Rekhta.adapters.myFavorite;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.activites.OfflineContent;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.myFavorite.MyNazamFagment;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;


public class MyFavNazamAdapter extends RecyclerView.Adapter {

    LayoutInflater layoutInflater;
    Context context;
    List<AddNazam> nazamList;
    AppDatabase db;
    MyNazamFagment myNazamFagment;
    ArrayList<FavFCModel> updatedNazamList;

    String title;
    String content;
    String auther;
    String jsonObj;


    public MyFavNazamAdapter(Context context, List<AddNazam> nazamList, ArrayList<FavFCModel> updatedNazamList, MyNazamFagment myNazamFagment) {
        this.context = context;
        this.nazamList = nazamList;
        this.updatedNazamList = updatedNazamList;
        layoutInflater = LayoutInflater.from(context);
        db = MyApplicationClass.getDb();
        this.myNazamFagment = myNazamFagment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.my_fav_nazam_list_items, null, false);

        MyFavNazamHolder myFavNazamHolder = new MyFavNazamHolder(view);
        return myFavNazamHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        MyFavNazamHolder myFavNazamHolder = (MyFavNazamHolder) holder;


        if (position % 2 == 0) {
            myFavNazamHolder.favNazmItemsMain.setBackgroundColor(Color.WHITE);
        } else {
            myFavNazamHolder.favNazmItemsMain.setBackgroundColor(Color.parseColor("#eeeeee"));
        }
        if (nazamList == null) {
            if (CommonUtil.languageCode == 1) {
                title = updatedNazamList.get(position).getTE();
                auther = updatedNazamList.get(position).getPE();
            } else if (CommonUtil.languageCode == 2) {
                title = updatedNazamList.get(position).getTH();
                auther = updatedNazamList.get(position).getPH();
            } else if (CommonUtil.languageCode == 3) {
                title = updatedNazamList.get(position).getTU();
                auther = updatedNazamList.get(position).getPU();
            }

            content = "";
            jsonObj = "";
        } else {
            title = nazamList.get(position).getTitle();
            content = nazamList.get(position).getContent();
            auther = nazamList.get(position).getAuther();
           // jsonObj = nazamList.get(position).getDataArray();
        }


        myFavNazamHolder.nazamTitle.setText(title);
        myFavNazamHolder.nazamContent.setText(content);


        if (auther != null) {
            myFavNazamHolder.nazamAuther.setText(auther);
        } else {
            myFavNazamHolder.nazamAuther.setText("");
        }

        myFavNazamHolder.removefav.setImageResource(R.drawable.ic_favorited);
        myFavNazamHolder.removefav.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        myFavNazamHolder.removefav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                myNazamFagment.rempveItem(nazamList, position, db);

            }
        });

        myFavNazamHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (nazamList == null) {
                    Intent intent = new Intent(context, OfflineContent.class);
                    intent.putExtra("DataArray", jsonObj);
                    intent.putExtra("listType", "nazam");
                    intent.putExtra("positionClicked", position);
                    intent.putExtra("poetName", auther);
                    intent.putExtra("contentId", updatedNazamList.get(position).getI());
                    intent.putExtra("addGhazalList", (Serializable) updatedNazamList);
                    context.startActivity(intent);
                } else {
                    jsonObj = nazamList.get(position).getDataArray();
                    Intent intent = new Intent(context, OfflineContent.class);
                    intent.putExtra("DataArray", jsonObj);
                    intent.putExtra("listType", "nazam");
                    intent.putExtra("positionClicked", position);
                    intent.putExtra("contentId", "");
                    intent.putExtra("poetName", nazamList.get(position).getAuther());
                    intent.putExtra("addGhazalList", (Serializable) nazamList);
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {

        if (nazamList == null) {
            return updatedNazamList.size();
        } else {
            return nazamList.size();
        }

    }

    private class MyFavNazamHolder extends RecyclerView.ViewHolder {


        TextView nazamTitle, nazamContent, nazamAuther;
        ImageView removefav;
        RelativeLayout favNazmItemsMain;

        public MyFavNazamHolder(View itemView) {
            super(itemView);

            nazamTitle = (TextView) itemView.findViewById(R.id.offlineghazalTitle);
            nazamContent = (TextView) itemView.findViewById(R.id.offlineGhazalContent);
            nazamAuther = (TextView) itemView.findViewById(R.id.offlienAuthorName);
            removefav = (ImageView) itemView.findViewById(R.id.offlineFavIcon);
            favNazmItemsMain = (RelativeLayout) itemView.findViewById(R.id.favNazmItemsMain);
        }
    }

}
