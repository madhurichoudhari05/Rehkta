package org.Rekhta.adapters.myFavorite;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.Rekhta.utils.CommonUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.myFavorite.MySherFragment;
import org.Rekhta.model.LocalModels.AddSher;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.utils.MyApplicationClass;


public class MyFavSherAdapter extends RecyclerView.Adapter {


    private final Typeface engtf, hinditf, urdutf;
    Context context;
    List<AddSher> addShersList;
    LayoutInflater layoutInflater;
    LinearLayout newLinearLayout;

    AppDatabase appDatabase;
    MySherFragment mySherFragment;
    boolean anothervalue = false;
    TextView[] newTextview;
    Typeface getEngtf;
    ArrayList<FavFCModel> updatedNazamList;

    public MyFavSherAdapter(Context context, List<AddSher> addShersList, ArrayList<FavFCModel> updatedNazamList, MySherFragment mySherFragment) {

        this.context = context;
        this.addShersList = addShersList;
        this.updatedNazamList = updatedNazamList;
        layoutInflater = LayoutInflater.from(context);
        appDatabase = MyApplicationClass.getDb();
        this.mySherFragment = mySherFragment;

        engtf = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        hinditf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.myfavsher_items, parent, false);

        MySherViewHolder mySherViewHolder = new MySherViewHolder(view);

        return mySherViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        MySherViewHolder mySherViewHolder = (MySherViewHolder) holder;
        String sherContent = null;
        String poetName = null;


        if (position % 2 == 0) {
            mySherViewHolder.favSherItemsMain.setBackgroundColor(Color.WHITE);
        } else {
            mySherViewHolder.favSherItemsMain.setBackgroundColor(Color.parseColor("#eeeeee"));
        }


        if (addShersList == null) {

            switch (CommonUtil.languageCode) {
                case 1:
                    sherContent = updatedNazamList.get(position).getTE();
                    poetName = updatedNazamList.get(position).getPE();
                    break;
                case 2:
                    sherContent = updatedNazamList.get(position).getTH();
                    poetName = updatedNazamList.get(position).getPH();
                    break;
                case 3:
                    sherContent = updatedNazamList.get(position).getTU();
                    poetName = updatedNazamList.get(position).getPU();
                    break;
            }


        } else {
            sherContent = addShersList.get(position).getSher_content();
            poetName = addShersList.get(position).getSher_auther();
        }

        if (sherContent.contains("{")) {
            setSherData(sherContent, mySherViewHolder);
            // mySherViewHolder.sherContent.setText(formatText(sherContent));
        } else {
            mySherViewHolder.sherContent.removeAllViews();
            TextView textView = new TextView(context);

            textView.setTextColor(Color.BLACK);
            textView.setGravity(Gravity.CENTER);
            textView.setText(sherContent);
            mySherViewHolder.sherContent.addView(textView);
            // mySherViewHolder.sherContent.setText((addShersList.get(position).getSher_content()));
        }

        mySherViewHolder.poetName.setText(poetName);

        mySherViewHolder.sherFaveIcon.setImageResource(R.drawable.ic_favorited);
        mySherViewHolder.sherFaveIcon.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        mySherViewHolder.sherFaveIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mySherFragment.removeItem(addShersList, position, appDatabase);

            }
        });
    }

    private void setSherData(String sherContent, MySherViewHolder mySherViewHolder) {


        ((LinearLayout) mySherViewHolder.sherContent).removeAllViews();
        String titileSher = sherContent;
        anothervalue = true;
        int minGap = 10;
        newLinearLayout = new LinearLayout(context);
        newLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        newLinearLayout.setOrientation(LinearLayout.VERTICAL);

        newLinearLayout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        try {

            JSONObject jsonObject = new JSONObject(titileSher);
            JSONArray Parray = jsonObject.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {

                    LinearLayout linear = new LinearLayout(context);

                    linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linear.setOrientation(LinearLayout.HORIZONTAL);
                    linear.setPadding(0, 17, 0, 17);


                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    newTextview = new TextView[Warray.length()];


                    for (int k = 0; k < Warray.length(); k++) {
                        newTextview[j] = new TextView(context);
                        newTextview[j].setLayoutParams(dim);
                        newTextview[j].setSingleLine(true);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            newTextview[j].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        }
                        /*if (k < Warray.length() - 1) {
                            newTextview[j].setPadding(0, 0, minGap, 0);
                        }*/
                        newTextview[j].setText(" " + Warray.getJSONObject(k).getString("W"));
                        newTextview[j].setGravity(Gravity.CENTER);
                        newTextview[j].setTypeface(engtf);
                       // newTextview[j].setTextSize(8);
                        linear.addView(newTextview[j]);

//                         str = str + " " + Warray.getJSONObject(k).getString("W");
                    }
                    newLinearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                    newLinearLayout.addView(linear);
                }
            }
        } catch (Exception e) {

        }

        mySherViewHolder.sherContent.addView(newLinearLayout);
        mySherViewHolder.sherContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (anothervalue)
                    widhtOfAnotherView();
                anothervalue = false;
            }
        });
    }

    private void widhtOfAnotherView() {

        int childCount = newLinearLayout.getChildCount();
        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = newLinearLayout.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                int curWidthDiff = newLinearLayout.getWidth() - lineView.getWidth();
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
//                    textView.setBackgroundColor(Color.parseColor("#FFE800"));
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        textView.setPadding(0, 0, extraPadding, 0);
                    }
                }

                //TextView
            }
        }
    }


    @Override
    public int getItemCount() {

        if (addShersList == null) {
            return updatedNazamList.size();
        } else {
            return addShersList.size();
        }

    }

    private class MySherViewHolder extends RecyclerView.ViewHolder {
        TextView poetName;
        LinearLayout sherContent;
        ImageView sherFaveIcon;

        LinearLayout favSherItemsMain;

        public MySherViewHolder(View itemView) {
            super(itemView);

            sherContent = (LinearLayout) itemView.findViewById(R.id.myFavSherContent);
            poetName = (TextView) itemView.findViewById(R.id.myFavSherPoet);
            sherFaveIcon = (ImageView) itemView.findViewById(R.id.myfavSherIcon);
            favSherItemsMain = (LinearLayout) itemView.findViewById(R.id.favSherItemsMain);

        }
    }

}
