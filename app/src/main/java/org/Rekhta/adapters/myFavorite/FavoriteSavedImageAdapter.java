package org.Rekhta.adapters.myFavorite;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;

/**
 * Created by RAJESH on 11/29/2017.
 */

public class FavoriteSavedImageAdapter extends RecyclerView.Adapter<FavoriteSavedImageAdapter.ViewHolder> {
    ArrayList<Bitmap> bmpArray;
    List<String> myDataset;
    Context context;
    private List<String> values;

    public FavoriteSavedImageAdapter(ArrayList<Bitmap> bmpArray, List<String> myDataset, FragmentActivity activity) {

        values = myDataset;
        this.bmpArray = bmpArray;
        this.myDataset = myDataset;
        this.context = context;
    }

    public void add(int position, String item) {
        // values.add(position, item);
        // notifyItemInserted(position);
    }

    public void remove(int position) {
        // values.remove(position);
        //notifyItemRemoved(position);
    }

    @Override
    public FavoriteSavedImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.imageView.setImageBitmap(bmpArray.get(position));


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return bmpArray.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHeader;
        public TextView txtFooter;
        public View layout;
        ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            imageView = (ImageView) v.findViewById(R.id.imageIcon);
        }
    }

}