package org.Rekhta.adapters.myFavorite;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.model.historyModels.HistoryRPojo;
import org.Rekhta.model.myFavorite.SongAndAuther;
import org.Rekhta.utils.CommonUtil;


public class HistryAdapter extends RecyclerView.Adapter<HistryAdapter.MyViewHolder> {

    List<HistoryRPojo> arrayList;
    TextView songName, autherName;
    ImageView youtubeIcon, addtofav, audioIcon;
    private Context mContext;

    public HistryAdapter(Context mContext, List<HistoryRPojo> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_histry_row_item_data, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //#3F51B5

        if (CommonUtil.languageCode == 1) {
            holder.tvSongName.setText(arrayList.get(position).getTE());
            holder.tvAutherName.setText(arrayList.get(position).getNE());
        } else if (CommonUtil.languageCode == 2) {
            holder.tvSongName.setText(arrayList.get(position).getTH());
            holder.tvAutherName.setText(arrayList.get(position).getNH());
        } else if (CommonUtil.languageCode == 3) {
            holder.tvSongName.setText(arrayList.get(position).getTU());
            holder.tvAutherName.setText(arrayList.get(position).getNU());

        }


    }

    @Override
    public int getItemCount() {
        if (arrayList != null)
            return arrayList.size();
        else
            return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSongName, tvAutherName;

        public MyViewHolder(View view) {
            super(view);

            tvSongName = (TextView) view.findViewById(R.id.ghazalTitle);
            tvAutherName = (TextView) view.findViewById(R.id.ghazalAuthorName);
            youtubeIcon = (ImageView) view.findViewById(R.id.youtubeLink);
            audioIcon = (ImageView) view.findViewById(R.id.audioLink);
            addtofav = (ImageView) view.findViewById(R.id.offlineFavIcon);

        }
    }
}

