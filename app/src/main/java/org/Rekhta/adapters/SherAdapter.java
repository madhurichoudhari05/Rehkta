package org.Rekhta.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.Rekhta.fragments.sherFragments.CollectonFragment;
import org.Rekhta.fragments.sherFragments.TagsFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

/**
 * Created by Admin on 9/27/2017.
 */


public class SherAdapter extends FragmentPagerAdapter {
    public SherAdapter(FragmentManager fm) {

        super(fm);

    }

    @Override

    public Fragment getItem(int position) {
        Fragment f = null;
        if (position == 0) {
            f = new CollectonFragment();
        } else if (position == 1) {
            f = new TagsFragment();
        }
        return f;
    }

    @Override
    public int getCount() {

        return 2;

    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {

            return LanguageSelection.getSpecificLanguageWordById("Collections", CommonUtil.languageCode);

        } else if (position == 1) {
            return LanguageSelection.getSpecificLanguageWordById("Tags", CommonUtil.languageCode);
        }
        return super.getPageTitle(position);
    }
}
