package org.Rekhta.TabsCollection;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.Rekhta.R;;

/**
 * Created by Admin on 9/27/2017.
 */

public  class CollectionFragment extends Fragment {
    RecyclerView CollectionRecyler;

    @Nullable

    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.collectionfragment, null);
        CollectionRecyler = (RecyclerView)vv.findViewById(R.id.recyclercollection);
        return  vv;

    }
}
