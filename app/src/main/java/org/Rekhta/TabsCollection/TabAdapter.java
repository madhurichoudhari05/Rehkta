package org.Rekhta.TabsCollection;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Admin on 9/27/2017.
 */

public class TabAdapter  extends FragmentPagerAdapter {
    int count = 1;
    public TabAdapter(FragmentManager fm, int count) {
        super(fm);
        this.count = count;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TagFragment();
            case 1:
                return new CollectionFragment();
            default:
                return new Fragment();
        }
    }
    @Override
    public int getCount()
    {
        return count;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "";
            case 1:
                return "";
            default:
                return "";
        }
    }
}