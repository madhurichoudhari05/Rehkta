package org.Rekhta.views;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.activites.Dashboard;
import org.Rekhta.activites.PoetDetailedActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.Rekhta.R;
import org.Rekhta.utils.CommonUtil;


public class MeaningFragment extends BottomSheetDialogFragment {

    JSONObject responseObj;
    TextView mainText, mainword2, mainword3, detailedText;
    ImageView closeImg;
    RelativeLayout mainView;
    Typeface engtf;
    View v;
    boolean isFromPoetSher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            String word = getArguments().getString("data");
            final String fromHome = getArguments().getString("home");
            isFromPoetSher = getArguments().getBoolean("fromPoetSher");
            responseObj = new JSONObject(getArguments().getString("response"));

            v = inflater.inflate(R.layout.meaning_template, container, false);
            mainView = (RelativeLayout) v.findViewById(R.id.mainMeaningview);
            mainText = (TextView) v.findViewById(R.id.mainText);
            detailedText = (TextView) v.findViewById(R.id.abcd);
            mainword2 = (TextView) v.findViewById(R.id.mainword2);
            mainword3 = (TextView) v.findViewById(R.id.mainword3);
            closeImg = (ImageView) v.findViewById(R.id.closeImg);
            mainText.setText(word);
            engtf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/merriweather_light_italic.ttf");

            mainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (fromHome != null && fromHome.equals("home")) {
                        ((Dashboard) getActivity()).removeFragment();
                    } else if (isFromPoetSher) {
                        ((PoetDetailedActivity) getActivity()).removeMeaningFragment();
                    } else {
                        getActivity().onBackPressed();
                    }


                }
            });
            mainText.setTypeface(engtf);
            setTextAccordingLang();
            closeImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (fromHome != null && fromHome.equals("home")) {
                        ((Dashboard) getActivity()).removeFragment();
                    } else if (isFromPoetSher) {
                        ((PoetDetailedActivity) getActivity()).removeMeaningFragment();

                    } else {
                        CommonUtil.fromMeaning = true;
                        getActivity().onBackPressed();
                    }


                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return v;

    }

    public void setTextAccordingLang() {
        try {


            if (CommonUtil.languageCode == 1) {
                mainword2.setText(responseObj.getString("H"));
                mainword3.setText(responseObj.getString("U"));
                CommonUtil.setHindiFont(getActivity(), mainword2);
                CommonUtil.setUrduFont(getActivity(), mainword3);

            } else if (CommonUtil.languageCode == 2) {
                mainword2.setText(responseObj.getString("E"));
                mainword3.setText(responseObj.getString("U"));
                mainword2.setTypeface(engtf);
                CommonUtil.setUrduFont(getActivity(), mainword3);

            } else {
                mainword2.setText(responseObj.getString("E"));
                mainword3.setText(responseObj.getString("H"));
                mainword2.setTypeface(engtf);
                CommonUtil.setHindiFont(getActivity(), mainword3);
            }
            String meaning = "";
            if (!responseObj.isNull("M1E") || !responseObj.getString("M1E").equalsIgnoreCase("")) {
                meaning = responseObj.getString("M1E");
            } else if (!responseObj.isNull("M1H") || !responseObj.getString("M1H").equalsIgnoreCase("")) {
                meaning = responseObj.getString("M1H");
            } else if (!responseObj.isNull("M1U") || responseObj.getString("M1U").equalsIgnoreCase("")) {
                meaning = responseObj.getString("M1H");
            } else {
                meaning = "";
            }
            detailedText.setText("1. " + meaning);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
