package org.Rekhta.views;

import android.media.MediaPlayer;

import java.util.ArrayList;

/**
 * Created by SANCHIT on 9/7/2016.
 */
public class MediaPlayerService {

    static private MediaPlayer mediaPlayer;
    static private Boolean isMusicRunning = false;
    static public ArrayList<String> playNextQueue = new ArrayList<>();

    static public void playAudio(String audioUrl){
        try{
            String Url = "";

                System.out.println("----------------------------------------------------------------------------------- URL : " + Url);
                if(isMusicRunning){
                    //MediaPlayerService.stopAudio();
                    System.out.println("-----------------------------------------------------------------------------------");
                    playNextQueue.add(audioUrl);
                    //MediaPlayerService.playAudio(audioUrl);
                }else{
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource(Url);
                    //mediaPlayer.prepare();
                    mediaPlayer.prepareAsync();
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){

                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                    //mediaPlayer.start();
                    isMusicRunning = true;

                    if(playNextQueue.size() >= 1){
                        playNextQueue.remove(0);
                        System.out.println(playNextQueue);
                    }


                    // REALEASING INSTANCE WHEN ON COMPLETE IS INVOKED
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            MediaPlayerService.stopAudio();
                        }
                    });

                }



        }catch(Exception e){
            //e.printStackTrace();
            System.out.println("*******************************"+e.getMessage());
            //mediaPlayer.release();
            mediaPlayer =  null;
        }

    };


    static public void stopAudio(){

        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer =  null;
            isMusicRunning = false;
            if(playNextQueue.size() >= 1){
                playAudio(playNextQueue.get(0));
            }
        }
    };

    static public void clearAudioQueue(){

            if(mediaPlayer != null){
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer =  null;
                isMusicRunning = false;
            }
            playNextQueue.clear();

    }

}
