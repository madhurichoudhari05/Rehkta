package org.Rekhta.db.tables;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;




//@Entity
@Entity(tableName = "user_table")
public class UserDetail {

    @PrimaryKey
    @ColumnInfo(name = "userid")
    @NonNull
    private String userid;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "age")
    private String age;
    @ColumnInfo(name = "gender")
    private String gender;


    public UserDetail(@NonNull String userid, String name, String age, String gender) {

        this.userid = userid;
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    @NonNull
    public String getUserid() {
        return userid;
    }

    public void setUserid(@NonNull String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}