package org.Rekhta.db.database;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import org.Rekhta.db.controlloer.GhazalDao;
import org.Rekhta.db.controlloer.NazamDao;
import org.Rekhta.db.controlloer.SherDao;
import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.model.LocalModels.AddSher;


@Database(entities = {AddNazam.class, AddGhazal.class, AddSher.class}, version = 1, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    public abstract NazamDao userDao();

    public abstract GhazalDao ghazalDao();

    public abstract SherDao sherDao();

}
