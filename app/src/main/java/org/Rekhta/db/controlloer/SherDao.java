package org.Rekhta.db.controlloer;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.LocalModels.AddSher;

@Dao
public interface SherDao {

    @Query("SELECT * FROM sher_table")
    List<AddSher> getAll();

    @Query("SELECT * FROM sher_table WHERE userid IN (:userIds)")
    List<AddSher> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM sher_table WHERE userid IN (:userIds)")
    List<AddSher> loadAllByIds222(int userIds);

    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AddSher> users);

    @Insert
    void insert(AddSher addSher);

    @Delete
    void delete(AddSher addSher);

    @Query("DELETE FROM user_table")

    public void nukeTable();
}
