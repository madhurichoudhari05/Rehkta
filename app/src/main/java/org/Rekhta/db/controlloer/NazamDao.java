package org.Rekhta.db.controlloer;



import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;



import java.util.List;

import org.Rekhta.db.tables.UserDetail;
import org.Rekhta.model.LocalModels.AddNazam;


@Dao
public interface NazamDao {

    @Query("SELECT * FROM user_table")
    List<AddNazam> getAll();

    @Query("SELECT * FROM user_table WHERE userid IN (:userIds)")
    List<AddNazam> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user_table WHERE userid IN (:userIds)")
    List<AddNazam> loadAllByIds222(int userIds);

    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AddNazam> users);

    @Insert
    void insert(AddNazam addNazam);

    @Delete
    void delete(AddNazam user);

    @Query("DELETE FROM user_table")

    public void nukeTable();
}

