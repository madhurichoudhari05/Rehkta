package org.Rekhta.db.controlloer;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.LocalModels.AddNazam;

@Dao
public interface GhazalDao {

    @Query("SELECT * FROM ghazal_table")
    List<AddGhazal> getAll();

    @Query("SELECT * FROM ghazal_table WHERE userid IN (:userIds)")
    List<AddGhazal> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM ghazal_table WHERE userid IN (:userIds)")
    List<AddGhazal> loadAllByIds222(int userIds);

    /*@Query("SELECT * FROM user WHERE first_name LIKE :first AND "
            + "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AddGhazal> users);

    @Insert
    void insert(AddGhazal addGhazal);


    @Delete
    void delete(AddGhazal addGhazal);

    @Query("DELETE FROM user_table")

    public void nukeTable();
}
