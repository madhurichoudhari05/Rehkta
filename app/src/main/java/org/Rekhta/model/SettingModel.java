package org.Rekhta.model;

/**
 * Created by SANCHIT on 11/13/2017.
 */

public class SettingModel {

    /* {
                                IsContentSpecificNotificationEn = 1,
                                IsContentSpecificNotificationHi = 2,
                                IsContentSpecificNotificationUr = 3,

                                IsEventNotificationSubscriber = 4,
                                IsSaveFavoriteOffline = 5,
                                IsSherOfTheDaySubscriber = 6,
                                IsWordOfTheDaySubscriber = 7,
                                IsGenericNotificationSubscriber = 8,
                                IsRequestedNewsLetter = 0
                }*/

    private boolean IsContentSpecificNotificationEn;
    private boolean IsContentSpecificNotificationHi;
    private boolean IsContentSpecificNotificationUr;
    private boolean IsEventNotificationSubscriber;
    private boolean IsSaveFavoriteOffline;
    private boolean IsSherOfTheDaySubscriber;
    private boolean IsWordOfTheDaySubscriber;
    private boolean IsGenericNotificationSubscriber;
    private boolean IsRequestedNewsLetter;

    public SettingModel(boolean isContentSpecificNotificationEn, boolean isContentSpecificNotificationHi, boolean isContentSpecificNotificationUr,
                        boolean isEventNotificationSubscriber, boolean isSaveFavoriteOffline, boolean isSherOfTheDaySubscriber,
                        boolean isWordOfTheDaySubscriber, boolean isGenericNotificationSubscriber, boolean isRequestedNewsLetter) {

        IsContentSpecificNotificationEn = isContentSpecificNotificationEn;
        IsContentSpecificNotificationHi = isContentSpecificNotificationHi;
        IsContentSpecificNotificationUr = isContentSpecificNotificationUr;
        IsEventNotificationSubscriber   = isEventNotificationSubscriber;
        IsSaveFavoriteOffline           = isSaveFavoriteOffline;
        IsSherOfTheDaySubscriber        = isSherOfTheDaySubscriber;
        IsWordOfTheDaySubscriber        = isWordOfTheDaySubscriber;
        IsGenericNotificationSubscriber = isGenericNotificationSubscriber;
        IsRequestedNewsLetter           = isRequestedNewsLetter;

    }

    public boolean isContentSpecificNotificationEn() {
        return IsContentSpecificNotificationEn;
    }

    public void setContentSpecificNotificationEn(boolean contentSpecificNotificationEn) {
        IsContentSpecificNotificationEn = contentSpecificNotificationEn;
    }

    public boolean isContentSpecificNotificationHi() {
        return IsContentSpecificNotificationHi;
    }

    public void setContentSpecificNotificationHi(boolean contentSpecificNotificationHi) {
        IsContentSpecificNotificationHi = contentSpecificNotificationHi;
    }

    public boolean isContentSpecificNotificationUr() {
        return IsContentSpecificNotificationUr;
    }

    public void setContentSpecificNotificationUr(boolean contentSpecificNotificationUr) {
        IsContentSpecificNotificationUr = contentSpecificNotificationUr;
    }

    public boolean isEventNotificationSubscriber() {
        return IsEventNotificationSubscriber;
    }

    public void setEventNotificationSubscriber(boolean eventNotificationSubscriber) {
        IsEventNotificationSubscriber = eventNotificationSubscriber;
    }

    public boolean isSaveFavoriteOffline() {
        return IsSaveFavoriteOffline;
    }

    public void setSaveFavoriteOffline(boolean saveFavoriteOffline) {
        IsSaveFavoriteOffline = saveFavoriteOffline;
    }

    public boolean isSherOfTheDaySubscriber() {
        return IsSherOfTheDaySubscriber;
    }

    public void setSherOfTheDaySubscriber(boolean sherOfTheDaySubscriber) {
        IsSherOfTheDaySubscriber = sherOfTheDaySubscriber;
    }

    public boolean isWordOfTheDaySubscriber() {
        return IsWordOfTheDaySubscriber;
    }

    public void setWordOfTheDaySubscriber(boolean wordOfTheDaySubscriber) {
        IsWordOfTheDaySubscriber = wordOfTheDaySubscriber;
    }

    public boolean isGenericNotificationSubscriber() {
        return IsGenericNotificationSubscriber;
    }

    public void setGenericNotificationSubscriber(boolean genericNotificationSubscriber) {
        IsGenericNotificationSubscriber = genericNotificationSubscriber;
    }

    public boolean isRequestedNewsLetter() {
        return IsRequestedNewsLetter;
    }

    public void setRequestedNewsLetter(boolean requestedNewsLetter) {
        IsRequestedNewsLetter = requestedNewsLetter;
    }


}
