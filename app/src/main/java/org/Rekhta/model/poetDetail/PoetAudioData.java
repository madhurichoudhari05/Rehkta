package org.Rekhta.model.poetDetail;

/**
 * Created by SANCHIT on 11/10/2017.
 */

public class PoetAudioData {

    private String id;
    private String audioPoetid;
    private String audioPoetNameinEng;
    private String audioPoetNameInHin;
    private String audioPoetNameInUrdu;
    private String audioPoestScatch;
    private String audioContendId;
    private String audioTitleInEng;
    private String audioTitleInHin;
    private String audioTitleInUrdu;
    private String audioContentScript;
    private String audioID;
    private String audioHeaderInEng;
    private String audioHeaderInHIn;
    private String audioHeaderInUrdu;
    private String audioAS;
    private String audioTI;
    private String audioPSN;
    private String audioASN;
    private boolean audioHA;
    private boolean audioHP;
    private boolean isRunning;

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    private String length;


    public PoetAudioData(String id, String audioPoetid, String audioPoetNameinEng, String audioPoetNameInHin, String audioPoetNameInUrdu,
                         String audioPoestScatch, String audioContendId, String audioTitleInEng, String audioTitleInHin,
                         String audioTitleInUrdu, String audioContentScript, String audioID, String audioHeaderInEng,
                         String audioHeaderInHIn, String audioHeaderInUrdu, String audioAS, String audioTI,
                         String audioPSN, String audioASN, boolean audioHA, boolean audioHP, String length) {
        this.id = id;
        this.audioPoetid = audioPoetid;
        this.audioPoetNameinEng = audioPoetNameinEng;
        this.audioPoetNameInHin = audioPoetNameInHin;
        this.audioPoetNameInUrdu = audioPoetNameInUrdu;
        this.audioPoestScatch = audioPoestScatch;
        this.audioContendId = audioContendId;
        this.audioTitleInEng = audioTitleInEng;
        this.audioTitleInHin = audioTitleInHin;
        this.audioTitleInUrdu = audioTitleInUrdu;
        this.audioContentScript = audioContentScript;
        this.audioID = audioID;
        this.audioHeaderInEng = audioHeaderInEng;
        this.audioHeaderInHIn = audioHeaderInHIn;
        this.audioHeaderInUrdu = audioHeaderInUrdu;
        this.audioAS = audioAS;
        this.audioTI = audioTI;
        this.audioPSN = audioPSN;
        this.audioASN = audioASN;
        this.audioHA = audioHA;
        this.audioHP = audioHP;
        this.length = length;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAudioPoetid() {
        return audioPoetid;
    }

    public void setAudioPoetid(String audioPoetid) {
        this.audioPoetid = audioPoetid;
    }

    public String getAudioPoetNameinEng() {
        return audioPoetNameinEng;
    }

    public void setAudioPoetNameinEng(String audioPoetNameinEng) {
        this.audioPoetNameinEng = audioPoetNameinEng;
    }

    public String getAudioPoetNameInHin() {
        return audioPoetNameInHin;
    }

    public void setAudioPoetNameInHin(String audioPoetNameInHin) {
        this.audioPoetNameInHin = audioPoetNameInHin;
    }

    public String getAudioPoetNameInUrdu() {
        return audioPoetNameInUrdu;
    }

    public void setAudioPoetNameInUrdu(String audioPoetNameInUrdu) {
        this.audioPoetNameInUrdu = audioPoetNameInUrdu;
    }

    public String getAudioPoestScatch() {
        return audioPoestScatch;
    }

    public void setAudioPoestScatch(String audioPoestScatch) {
        this.audioPoestScatch = audioPoestScatch;
    }

    public String getAudioContendId() {
        return audioContendId;
    }

    public void setAudioContendId(String audioContendId) {
        this.audioContendId = audioContendId;
    }

    public String getAudioTitleInEng() {
        return audioTitleInEng;
    }

    public void setAudioTitleInEng(String audioTitleInEng) {
        this.audioTitleInEng = audioTitleInEng;
    }

    public String getAudioTitleInHin() {
        return audioTitleInHin;
    }

    public void setAudioTitleInHin(String audioTitleInHin) {
        this.audioTitleInHin = audioTitleInHin;
    }

    public String getAudioTitleInUrdu() {
        return audioTitleInUrdu;
    }

    public void setAudioTitleInUrdu(String audioTitleInUrdu) {
        this.audioTitleInUrdu = audioTitleInUrdu;
    }

    public String getAudioContentScript() {
        return audioContentScript;
    }

    public void setAudioContentScript(String audioContentScript) {
        this.audioContentScript = audioContentScript;
    }

    public String getAudioID() {
        return audioID;
    }

    public void setAudioID(String audioID) {
        this.audioID = audioID;
    }

    public String getAudioHeaderInEng() {
        return audioHeaderInEng;
    }

    public void setAudioHeaderInEng(String audioHeaderInEng) {
        this.audioHeaderInEng = audioHeaderInEng;
    }

    public String getAudioHeaderInHIn() {
        return audioHeaderInHIn;
    }

    public void setAudioHeaderInHIn(String audioHeaderInHIn) {
        this.audioHeaderInHIn = audioHeaderInHIn;
    }

    public String getAudioHeaderInUrdu() {
        return audioHeaderInUrdu;
    }

    public void setAudioHeaderInUrdu(String audioHeaderInUrdu) {
        this.audioHeaderInUrdu = audioHeaderInUrdu;
    }

    public String getAudioAS() {
        return audioAS;
    }

    public void setAudioAS(String audioAS) {
        this.audioAS = audioAS;
    }

    public String getAudioTI() {
        return audioTI;
    }

    public void setAudioTI(String audioTI) {
        this.audioTI = audioTI;
    }

    public String getAudioPSN() {
        return audioPSN;
    }

    public void setAudioPSN(String audioPSN) {
        this.audioPSN = audioPSN;
    }

    public String getAudioASN() {
        return audioASN;
    }

    public void setAudioASN(String audioASN) {
        this.audioASN = audioASN;
    }

    public boolean isAudioHA() {
        return audioHA;
    }

    public void setAudioHA(boolean audioHA) {
        this.audioHA = audioHA;
    }

    public boolean isAudioHP() {
        return audioHP;
    }

    public void setAudioHP(boolean audioHP) {
        this.audioHP = audioHP;
    }
}
