package org.Rekhta.model.poetDetail;


import android.text.Html;

import org.json.JSONArray;

public class PoetDetailData {


    private String poetId;
    private String poetNameInEng;
    private String poetNameInHin;
    private String poetNameInUrdu;
    private String poetDescInEng;
    private String poetDescInHin;
    private String poetDescInUrdu;
    private String poetBirthPlaceInEng;
    private String poetBirthPlaceInHin;
    private String poetBirthPlaceInUrdu;
    private String poetDeathPlaceInEng;
    private String poetDeathPlaceInHin;
    private String poetDeathPlaceInUrdu;
    private String poetsRealNameInEng;
    private String poetsRealNameInHin;
    private String poetsRealNameInUrdu;
    private String poetsDateOfBirth;
    private String poetsDateOfDeath;
    private int totalAudioByPoet;
    private int totalVideoByPoet;
    private int totalEditorChoiceByPoet;
    private JSONArray totalSGNData;

    public PoetDetailData(String poetId,
                          String poetNameInEng, String poetNameInHin, String poetNameInUrdu,
                          String poetDescInEng, String poetDescInHin,String poetDescInUrdu,
                          String poetBirthPlaceInEng,String poetBirthPlaceInHin, String poetBirthPlaceInUrdu,
                          String poetDeathPlaceInEng, String poetDeathPlaceInHin, String poetDeathPlaceInUrdu,
                          String poetsRealNameInEng, String poetsRealNameInHin, String poetsRealNameInUrdu,
                          String poetsDateOfBirth, String poetsDateOfDeath,
                          int totalAudioByPoet, int totalVideoByPoet, int totalEditorChoiceByPoet, JSONArray totalSGNData) {
        this.poetId = poetId;
        this.poetNameInEng = poetNameInEng;
        this.poetNameInHin = poetNameInHin;
        this.poetNameInUrdu = poetNameInUrdu;
        this.poetDescInEng = poetDescInEng;
        this.poetDescInHin = poetDescInHin;
        this.poetDescInUrdu = poetDescInUrdu;
        this.poetBirthPlaceInEng = poetBirthPlaceInEng;
        this.poetBirthPlaceInHin = poetBirthPlaceInHin;
        this.poetBirthPlaceInUrdu = poetBirthPlaceInUrdu;
        this.poetDeathPlaceInEng = poetDeathPlaceInEng;
        this.poetDeathPlaceInHin = poetDeathPlaceInHin;
        this.poetDeathPlaceInUrdu = poetDeathPlaceInUrdu;
        this.poetsRealNameInEng = poetsRealNameInEng;
        this.poetsRealNameInHin = poetsRealNameInHin;
        this.poetsRealNameInUrdu = poetsRealNameInUrdu;
        this.poetsDateOfBirth = poetsDateOfBirth;
        this.poetsDateOfDeath = poetsDateOfDeath;
        this.totalAudioByPoet = totalAudioByPoet;
        this.totalVideoByPoet = totalVideoByPoet;
        this.totalEditorChoiceByPoet = totalEditorChoiceByPoet;
        this.totalSGNData = totalSGNData;
    }

    public String getPoetId() {
        return poetId;
    }

    public void setPoetId(String poetId) {
        this.poetId = poetId;
    }

    public String getPoetNameInEng() {
        return poetNameInEng;
    }

    public void setPoetNameInEng(String poetNameInEng) {
        this.poetNameInEng = poetNameInEng;
    }

    public String getPoetNameInHin() {
        return poetNameInHin;
    }

    public void setPoetNameInHin(String poetNameInHin) {
        this.poetNameInHin = poetNameInHin;
    }

    public String getPoetNameInUrdu() {
        return poetNameInUrdu;
    }

    public void setPoetNameInUrdu(String poetNameInUrdu) {
        this.poetNameInUrdu = poetNameInUrdu;
    }

    public String getPoetDescInEng() {
        String htmlToNormal = Html.fromHtml(poetDescInEng)+"";
        return htmlToNormal;
    }

    public void setPoetDescInEng(String poetDescInEng) {
        this.poetDescInEng = poetDescInEng;
    }

    public String getPoetDescInHin() {
        return poetDescInHin;
    }

    public void setPoetDescInHin(String poetDescInHin) {
        this.poetDescInHin = poetDescInHin;
    }

    public String getPoetDescInUrdu() {
        return poetDescInUrdu;
    }

    public void setPoetDescInUrdu(String poetDescInUrdu) {
        this.poetDescInUrdu = poetDescInUrdu;
    }

    public String getPoetBirthPlaceInEng() {

        if(poetBirthPlaceInEng.equalsIgnoreCase("null")){
            return " ";
        }
        return poetBirthPlaceInEng;
    }

    public void setPoetBirthPlaceInEng(String poetBirthPlaceInEng) {
        this.poetBirthPlaceInEng = poetBirthPlaceInEng;
    }

    public String getPoetBirthPlaceInHin() {
        if(poetBirthPlaceInHin.equalsIgnoreCase("null")){
            return " ";
        }
        return poetBirthPlaceInHin;
    }

    public void setPoetBirthPlaceInHin(String poetBirthPlaceInHin) {
        this.poetBirthPlaceInHin = poetBirthPlaceInHin;
    }

    public String getPoetBirthPlaceInUrdu() {
        if(poetBirthPlaceInUrdu.equalsIgnoreCase("null")){
            return " ";
        }
        return poetBirthPlaceInUrdu;
    }

    public void setPoetBirthPlaceInUrdu(String poetBirthPlaceInUrdu) {
        this.poetBirthPlaceInUrdu = poetBirthPlaceInUrdu;
    }

    public String getPoetDeathPlaceInEng() {
        return poetDeathPlaceInEng;
    }

    public void setPoetDeathPlaceInEng(String poetDeathPlaceInEng) {
        this.poetDeathPlaceInEng = poetDeathPlaceInEng;
    }

    public String getPoetDeathPlaceInHin() {
        return poetDeathPlaceInHin;
    }

    public void setPoetDeathPlaceInHin(String poetDeathPlaceInHin) {
        this.poetDeathPlaceInHin = poetDeathPlaceInHin;
    }

    public String getPoetDeathPlaceInUrdu() {
        return poetDeathPlaceInUrdu;
    }

    public void setPoetDeathPlaceInUrdu(String poetDeathPlaceInUrdu) {
        this.poetDeathPlaceInUrdu = poetDeathPlaceInUrdu;
    }

    public String getPoetsRealNameInEng() {
        return poetsRealNameInEng;
    }

    public void setPoetsRealNameInEng(String poetsRealNameInEng) {
        this.poetsRealNameInEng = poetsRealNameInEng;
    }

    public String getPoetsRealNameInHin() {
        return poetsRealNameInHin;
    }

    public void setPoetsRealNameInHin(String poetsRealNameInHin) {
        this.poetsRealNameInHin = poetsRealNameInHin;
    }

    public String getPoetsRealNameInUrdu() {
        return poetsRealNameInUrdu;
    }

    public void setPoetsRealNameInUrdu(String poetsRealNameInUrdu) {
        this.poetsRealNameInUrdu = poetsRealNameInUrdu;
    }

    public String getPoetsDateOfBirth() {
        if(poetsDateOfBirth.equalsIgnoreCase("null")){
            return " ";
        }
        return poetsDateOfBirth;
    }

    public void setPoetsDateOfBirth(String poetsDateOfBirth) {
        this.poetsDateOfBirth = poetsDateOfBirth;
    }

    public String getPoetsDateOfDeath() {
        if(poetsDateOfDeath.equalsIgnoreCase("null")){
            return " ";
        }
        return poetsDateOfDeath;
    }

    public void setPoetsDateOfDeath(String poetsDateOfDeath) {
        this.poetsDateOfDeath = poetsDateOfDeath;
    }

    public int getTotalAudioByPoet() {
        return totalAudioByPoet;
    }

    public void setTotalAudioByPoet(int totalAudioByPoet) {
        this.totalAudioByPoet = totalAudioByPoet;
    }

    public int getTotalVideoByPoet() {
        return totalVideoByPoet;
    }

    public void setTotalVideoByPoet(int totalVideoByPoet) {
        this.totalVideoByPoet = totalVideoByPoet;
    }

    public int getTotalEditorChoiceByPoet() {
        return totalEditorChoiceByPoet;
    }

    public void setTotalEditorChoiceByPoet(int totalEditorChoiceByPoet) {
        this.totalEditorChoiceByPoet = totalEditorChoiceByPoet;
    }

    public JSONArray getTotalSGNData() {
        return totalSGNData;
    }

    public void setTotalSGNData(JSONArray totalSGNData) {
        this.totalSGNData = totalSGNData;
    }


}
