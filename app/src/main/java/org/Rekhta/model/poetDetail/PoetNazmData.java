package org.Rekhta.model.poetDetail;

/**
 * Created by SANCHIT on 11/9/2017.
 */

public class PoetNazmData {

    private String nazmId;
    private String nazmPoetNameInEng;
    private String nazmPoetNameInHin;
    private String nazmPoetNameInUrdu;
    private String nazmSeriesInEng;
    private String nazmSeriesInHin;
    private String nazmSeriesInUrdu;
    private String nazmTitleInEng;
    private String nazmTitleInHin;
    private String nazmTitleInUrdu;
    private boolean isEditorChoice;
    private boolean isPopularChoice;
    private boolean isAudioAvailable;
    private boolean isVideoAvailable;
    private String nazmShareLinkInEng;
    private String nazmShareLinkInHin;
    private String nazmShareLinkInUrdu;

    public PoetNazmData(String nazmId,
                        String nazmPoetNameInEng, String nazmPoetNameInHin, String nazmPoetNameInUrdu,
                        String nazmSeriesInEng, String nazmSeriesInHin, String nazmSeriesInUrdu,
                        String nazmTitleInEng, String nazmTitleInHin, String nazmTitleInUrdu,
                        boolean isEditorChoice, boolean isPopularChoice, boolean isAudioAvailable, boolean isVideoAvailable,
                        String nazmShareLinkInEng, String nazmShareLinkInHin, String nazmShareLinkInUrdu) {
        this.nazmId = nazmId;
        this.nazmPoetNameInEng = nazmPoetNameInEng;
        this.nazmPoetNameInHin = nazmPoetNameInHin;
        this.nazmPoetNameInUrdu = nazmPoetNameInUrdu;
        this.nazmSeriesInEng = nazmSeriesInEng;
        this.nazmSeriesInHin = nazmSeriesInHin;
        this.nazmSeriesInUrdu = nazmSeriesInUrdu;
        this.nazmTitleInEng = nazmTitleInEng;
        this.nazmTitleInHin = nazmTitleInHin;
        this.nazmTitleInUrdu = nazmTitleInUrdu;
        this.isEditorChoice = isEditorChoice;
        this.isPopularChoice = isPopularChoice;
        this.isAudioAvailable = isAudioAvailable;
        this.isVideoAvailable = isVideoAvailable;
        this.nazmShareLinkInEng = nazmShareLinkInEng;
        this.nazmShareLinkInHin = nazmShareLinkInHin;
        this.nazmShareLinkInUrdu = nazmShareLinkInUrdu;
    }

    public String getNazmId() {
        return nazmId;
    }

    public void setNazmId(String nazmId) {
        this.nazmId = nazmId;
    }

    public String getNazmPoetNameInEng() {
        return nazmPoetNameInEng;
    }

    public void setNazmPoetNameInEng(String nazmPoetNameInEng) {
        this.nazmPoetNameInEng = nazmPoetNameInEng;
    }

    public String getNazmPoetNameInHin() {
        return nazmPoetNameInHin;
    }

    public void setNazmPoetNameInHin(String nazmPoetNameInHin) {
        this.nazmPoetNameInHin = nazmPoetNameInHin;
    }

    public String getNazmPoetNameInUrdu() {
        return nazmPoetNameInUrdu;
    }

    public void setNazmPoetNameInUrdu(String nazmPoetNameInUrdu) {
        this.nazmPoetNameInUrdu = nazmPoetNameInUrdu;
    }

    public String getNazmSeriesInEng() {
        return nazmSeriesInEng;
    }

    public void setNazmSeriesInEng(String nazmSeriesInEng) {
        this.nazmSeriesInEng = nazmSeriesInEng;
    }

    public String getNazmSeriesInHin() {
        return nazmSeriesInHin;
    }

    public void setNazmSeriesInHin(String nazmSeriesInHin) {
        this.nazmSeriesInHin = nazmSeriesInHin;
    }

    public String getNazmSeriesInUrdu() {
        return nazmSeriesInUrdu;
    }

    public void setNazmSeriesInUrdu(String nazmSeriesInUrdu) {
        this.nazmSeriesInUrdu = nazmSeriesInUrdu;
    }

    public String getNazmTitleInEng() {
        return nazmTitleInEng;
    }

    public void setNazmTitleInEng(String nazmTitleInEng) {
        this.nazmTitleInEng = nazmTitleInEng;
    }

    public String getNazmTitleInHin() {
        return nazmTitleInHin;
    }

    public void setNazmTitleInHin(String nazmTitleInHin) {
        this.nazmTitleInHin = nazmTitleInHin;
    }

    public String getNazmTitleInUrdu() {
        return nazmTitleInUrdu;
    }

    public void setNazmTitleInUrdu(String nazmTitleInUrdu) {
        this.nazmTitleInUrdu = nazmTitleInUrdu;
    }

    public boolean isEditorChoice() {
        return isEditorChoice;
    }

    public void setEditorChoice(boolean editorChoice) {
        isEditorChoice = editorChoice;
    }

    public boolean isPopularChoice() {
        return isPopularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        isPopularChoice = popularChoice;
    }

    public boolean isAudioAvailable() {
        return isAudioAvailable;
    }

    public void setAudioAvailable(boolean audioAvailable) {
        isAudioAvailable = audioAvailable;
    }

    public boolean isVideoAvailable() {
        return isVideoAvailable;
    }

    public void setVideoAvailable(boolean videoAvailable) {
        isVideoAvailable = videoAvailable;
    }

    public String getNazmShareLinkInEng() {
        return nazmShareLinkInEng;
    }

    public void setNazmShareLinkInEng(String nazmShareLinkInEng) {
        this.nazmShareLinkInEng = nazmShareLinkInEng;
    }

    public String getNazmShareLinkInHin() {
        return nazmShareLinkInHin;
    }

    public void setNazmShareLinkInHin(String nazmShareLinkInHin) {
        this.nazmShareLinkInHin = nazmShareLinkInHin;
    }

    public String getNazmShareLinkInUrdu() {
        return nazmShareLinkInUrdu;
    }

    public void setNazmShareLinkInUrdu(String nazmShareLinkInUrdu) {
        this.nazmShareLinkInUrdu = nazmShareLinkInUrdu;
    }
}
