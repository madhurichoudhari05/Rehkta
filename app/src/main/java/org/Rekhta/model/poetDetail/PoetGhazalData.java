package org.Rekhta.model.poetDetail;


public class PoetGhazalData {

    private String poetsGhazalId;
    private String poetsGhazalTiltleInEng;
    private String poetsGhazalTiltleInHin;
    private String poetsGhazalTiltleInUrdu;
    private boolean isEditorChoice;
    private boolean isPopularChoice;
    private boolean isAudioAvailable;
    private boolean isVideoAvailable;
    private String ghazalLinkInEng;
    private String ghazalLinkInHin;
    private String ghazalLinkInUrdu;

    public PoetGhazalData(String poetsGhazalId,
                          String poetsGhazalTiltleInEng, String poetsGhazalTiltleInHin, String poetsGhazalTiltleInUrdu,
                          boolean isEditorChoice, boolean isPopularChoice, boolean isAudioAvailable, boolean isVideoAvailable,
                          String ghazalLinkInEng, String ghazalLinkInHin, String ghazalLinkInUrdu) {
        this.poetsGhazalId = poetsGhazalId;
        this.poetsGhazalTiltleInEng = poetsGhazalTiltleInEng;
        this.poetsGhazalTiltleInHin = poetsGhazalTiltleInHin;
        this.poetsGhazalTiltleInUrdu = poetsGhazalTiltleInUrdu;
        this.isEditorChoice = isEditorChoice;
        this.isPopularChoice = isPopularChoice;
        this.isAudioAvailable = isAudioAvailable;
        this.isVideoAvailable = isVideoAvailable;
        this.ghazalLinkInEng = ghazalLinkInEng;
        this.ghazalLinkInHin = ghazalLinkInHin;
        this.ghazalLinkInUrdu = ghazalLinkInUrdu;
    }

    public String getPoetsGhazalId() {
        return poetsGhazalId;
    }

    public void setPoetsGhazalId(String poetsGhazalId) {
        this.poetsGhazalId = poetsGhazalId;
    }

    public String getPoetsGhazalTiltleInEng() {
        return poetsGhazalTiltleInEng;
    }

    public void setPoetsGhazalTiltleInEng(String poetsGhazalTiltleInEng) {
        this.poetsGhazalTiltleInEng = poetsGhazalTiltleInEng;
    }

    public String getPoetsGhazalTiltleInHin() {
        return poetsGhazalTiltleInHin;
    }

    public void setPoetsGhazalTiltleInHin(String poetsGhazalTiltleInHin) {
        this.poetsGhazalTiltleInHin = poetsGhazalTiltleInHin;
    }

    public String getPoetsGhazalTiltleInUrdu() {
        return poetsGhazalTiltleInUrdu;
    }

    public void setPoetsGhazalTiltleInUrdu(String poetsGhazalTiltleInUrdu) {
        this.poetsGhazalTiltleInUrdu = poetsGhazalTiltleInUrdu;
    }

    public boolean isEditorChoice() {
        return isEditorChoice;
    }

    public void setEditorChoice(boolean editorChoice) {
        isEditorChoice = editorChoice;
    }

    public boolean isPopularChoice() {
        return isPopularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        isPopularChoice = popularChoice;
    }

    public boolean isAudioAvailable() {
        return isAudioAvailable;
    }

    public void setAudioAvailable(boolean audioAvailable) {
        isAudioAvailable = audioAvailable;
    }

    public boolean isVideoAvailable() {
        return isVideoAvailable;
    }

    public void setVideoAvailable(boolean videoAvailable) {
        isVideoAvailable = videoAvailable;
    }


    public String getGhazalLinkInEng() {
        return ghazalLinkInEng;
    }

    public void setGhazalLinkInEng(String ghazalLinkInEng) {
        this.ghazalLinkInEng = ghazalLinkInEng;
    }

    public String getGhazalLinkInHin() {
        return ghazalLinkInHin;
    }

    public void setGhazalLinkInHin(String ghazalLinkInHin) {
        this.ghazalLinkInHin = ghazalLinkInHin;
    }

    public String getGhazalLinkInUrdu() {
        return ghazalLinkInUrdu;
    }

    public void setGhazalLinkInUrdu(String ghazalLinkInUrdu) {
        this.ghazalLinkInUrdu = ghazalLinkInUrdu;
    }

}
