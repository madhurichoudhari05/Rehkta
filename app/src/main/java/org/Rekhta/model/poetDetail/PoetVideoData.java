package org.Rekhta.model.poetDetail;

/**
 * Created by SANCHIT on 11/10/2017.
 */

public class PoetVideoData {

    /* {
                "I": "9c581ea7-e7bd-41ff-beca-40ec83740f68",
                "YI": "uCPnSifHGjo",
                "CI": "0cae4b66-d503-495d-9317-d50895212dee",
                "TI": "43d60a15-0b49-4caf-8b74-0fcdddeb9f83",
                "TE": "baat karni mujhe mushkil kabhi aisi to na thi",
                "TH": "    बात करनी मुझे मुश्किल कभी ऐसी तो न थी    ",
                "TU": "  بات کرنی مجھے مشکل کبھی ایسی تو نہ تھی  ",
                "DE": "",
                "DH": "",
                "DU": "",
                "CS": "baat-karnii-mujhe-mushkil-kabhii-aisii-to-na-thii-bahadur-shah-zafar-ghazals",
                "PI": "eb9cb3d1-1dbd-4582-a1ca-e48fdafe5bc8",
                "NE": "Bahadur Shah Zafar",
                "NH": "प्रसून जोशी",
                "NU": "پراسون جوشی",
                "PS": "bahadur-shah-zafar",
                "AI": null,
                "AE": null,
                "AH": null,
                "AU": null,
                "AS": null,
                "PSN": "bahadur shah zafar",
                "ASN": null,
                "HA": null,
                "HP": true
            }*/
    private String id;
    private String videoYoutubeId;
    private String videoContentId;
    private String videoThumbId;
    private String videoTitleInEng;
    private String videoTitleInHin;
    private String videoTitleInUrdu;
    private String videoDescriptionInEng;
    private String videoDescriptionInHin;
    private String videoDescriptionInUrdu;
    private String videoContentScript;
    private String videoPoetId;
    private String videoPoetNameInEng;
    private String videoPoetNameInHin;
    private String videoPoetNameInUrdu;
    private String videoPoetScatch;
    private String videoAudioId;
    private String videoAE;
    private String videoAH;
    private String videoAU;
    private String videoAS;
    private String videoPSN;
    private String videoASN;
    private String videoHA;
    private boolean videoHP;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    private int totalCount;


    public PoetVideoData(String id, String videoYoutubeId, String videoContentId, String videoThumbId, String videoTitleInEng,
                         String videoTitleInHin, String videoTitleInUrdu, String videoDescriptionInEng, String videoDescriptionInHin,
                         String videoDescriptionInUrdu, String videoContentScript, String videoPoetId, String videoPoetNameInEng,
                         String videoPoetNameInHin, String videoPoetNameInUrdu, String videoPoetScatch, String videoAudioId, String videoAE,
                         String videoAH, String videoAU, String videoAS, String videoPSN, String videoASN, String videoHA, boolean videoHP, int totalCount) {
        this.id = id;
        this.videoYoutubeId = videoYoutubeId;
        this.videoContentId = videoContentId;
        this.videoThumbId = videoThumbId;
        this.videoTitleInEng = videoTitleInEng;
        this.videoTitleInHin = videoTitleInHin;
        this.videoTitleInUrdu = videoTitleInUrdu;
        this.videoDescriptionInEng = videoDescriptionInEng;
        this.videoDescriptionInHin = videoDescriptionInHin;
        this.videoDescriptionInUrdu = videoDescriptionInUrdu;
        this.videoContentScript = videoContentScript;
        this.videoPoetId = videoPoetId;
        this.videoPoetNameInEng = videoPoetNameInEng;
        this.videoPoetNameInHin = videoPoetNameInHin;
        this.videoPoetNameInUrdu = videoPoetNameInUrdu;
        this.videoPoetScatch = videoPoetScatch;
        this.videoAudioId = videoAudioId;
        this.videoAE = videoAE;
        this.videoAH = videoAH;
        this.videoAU = videoAU;
        this.videoAS = videoAS;
        this.videoPSN = videoPSN;
        this.videoASN = videoASN;
        this.videoHA = videoHA;
        this.videoHP = videoHP;
        this.totalCount = totalCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideoYoutubeId() {
        return videoYoutubeId;
    }

    public void setVideoYoutubeId(String videoYoutubeId) {
        this.videoYoutubeId = videoYoutubeId;
    }

    public String getVideoContentId() {
        return videoContentId;
    }

    public void setVideoContentId(String videoContentId) {
        this.videoContentId = videoContentId;
    }

    public String getVideoThumbId() {
        return videoThumbId;
    }

    public void setVideoThumbId(String videoThumbId) {
        this.videoThumbId = videoThumbId;
    }

    public String getVideoTitleInEng() {
        return videoTitleInEng;
    }

    public void setVideoTitleInEng(String videoTitleInEng) {
        this.videoTitleInEng = videoTitleInEng;
    }

    public String getVideoTitleInHin() {
        return videoTitleInHin;
    }

    public void setVideoTitleInHin(String videoTitleInHin) {
        this.videoTitleInHin = videoTitleInHin;
    }

    public String getVideoTitleInUrdu() {
        return videoTitleInUrdu;
    }

    public void setVideoTitleInUrdu(String videoTitleInUrdu) {
        this.videoTitleInUrdu = videoTitleInUrdu;
    }

    public String getVideoDescriptionInEng() {
        return videoDescriptionInEng;
    }

    public void setVideoDescriptionInEng(String videoDescriptionInEng) {
        this.videoDescriptionInEng = videoDescriptionInEng;
    }

    public String getVideoDescriptionInHin() {
        return videoDescriptionInHin;
    }

    public void setVideoDescriptionInHin(String videoDescriptionInHin) {
        this.videoDescriptionInHin = videoDescriptionInHin;
    }

    public String getVideoDescriptionInUrdu() {
        return videoDescriptionInUrdu;
    }

    public void setVideoDescriptionInUrdu(String videoDescriptionInUrdu) {
        this.videoDescriptionInUrdu = videoDescriptionInUrdu;
    }

    public String getVideoContentScript() {
        return videoContentScript;
    }

    public void setVideoContentScript(String videoContentScript) {
        this.videoContentScript = videoContentScript;
    }

    public String getVideoPoetId() {
        return videoPoetId;
    }

    public void setVideoPoetId(String videoPoetId) {
        this.videoPoetId = videoPoetId;
    }

    public String getVideoPoetNameInEng() {
        return videoPoetNameInEng;
    }

    public void setVideoPoetNameInEng(String videoPoetNameInEng) {
        this.videoPoetNameInEng = videoPoetNameInEng;
    }

    public String getVideoPoetNameInHin() {
        return videoPoetNameInHin;
    }

    public void setVideoPoetNameInHin(String videoPoetNameInHin) {
        this.videoPoetNameInHin = videoPoetNameInHin;
    }

    public String getVideoPoetNameInUrdu() {
        return videoPoetNameInUrdu;
    }

    public void setVideoPoetNameInUrdu(String videoPoetNameInUrdu) {
        this.videoPoetNameInUrdu = videoPoetNameInUrdu;
    }

    public String getVideoPoetScatch() {
        return videoPoetScatch;
    }

    public void setVideoPoetScatch(String videoPoetScatch) {
        this.videoPoetScatch = videoPoetScatch;
    }

    public String getVideoAudioId() {
        return videoAudioId;
    }

    public void setVideoAudioId(String videoAudioId) {
        this.videoAudioId = videoAudioId;
    }

    public String getVideoAE() {
        return videoAE;
    }

    public void setVideoAE(String videoAE) {
        this.videoAE = videoAE;
    }

    public String getVideoAH() {
        return videoAH;
    }

    public void setVideoAH(String videoAH) {
        this.videoAH = videoAH;
    }

    public String getVideoAU() {
        return videoAU;
    }

    public void setVideoAU(String videoAU) {
        this.videoAU = videoAU;
    }

    public String getVideoAS() {
        return videoAS;
    }

    public void setVideoAS(String videoAS) {
        this.videoAS = videoAS;
    }

    public String getVideoPSN() {
        return videoPSN;
    }

    public void setVideoPSN(String videoPSN) {
        this.videoPSN = videoPSN;
    }

    public String getVideoASN() {
        return videoASN;
    }

    public void setVideoASN(String videoASN) {
        this.videoASN = videoASN;
    }

    public String isVideoHA() {
        return videoHA;
    }

    public void setVideoHA(String videoHA) {
        this.videoHA = videoHA;
    }

    public boolean isVideoHP() {
        return videoHP;
    }

    public void setVideoHP(boolean videoHP) {
        this.videoHP = videoHP;
    }
}
