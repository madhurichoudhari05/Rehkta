package org.Rekhta.model.poetDetail;

import org.json.JSONArray;

/**
 * Created by SANCHIT on 11/9/2017.
 */

public class PoetSherData {

    private String sherId;
    private String sherPoetNameInEng;
    private String sherPoetNameInHin;
    private String sherPoetNameInUrdu;
    private String sherTitleInEng;
    private String sherTitleInHin;
    private String sherTitleInUrdu;
    private boolean isEditorChoice;
    private boolean isPopularChoice;
    private boolean isAudioAvailable;
    private boolean isVideoAvailable;
    private String sherShareLinkInEng;
    private String sherShareLinkInHin;
    private String sherShareLinkInUrdu;


    public void setTagArrayLength(int tagArrayLength) {
        this.tagArrayLength = tagArrayLength;
    }

    private int tagArrayLength;

    public int getTagArrayLength() {
        return tagArrayLength;
    }

    public JSONArray getSherTagArray() {
        return sherTagArray;
    }

    public void setSherTagArray(JSONArray sherTagArray) {
        this.sherTagArray = sherTagArray;
    }

    private JSONArray sherTagArray;

    public PoetSherData(String sherId,
                        String sherPoetNameInEng, String sherPoetNameInHin, String sherPoetNameInUrdu,
                        String sherTitleInEng, String sherTitleInHin, String sherTitleInUrdu,
                        boolean isEditorChoice, boolean isPopularChoice, boolean isAudioAvailable,boolean isVideoAvailable,
                        String sherShareLinkInEng, String sherShareLinkInHin, String sherShareLinkInUrdu) {
        this.sherId = sherId;
        this.sherPoetNameInEng = sherPoetNameInEng;
        this.sherPoetNameInHin = sherPoetNameInHin;
        this.sherPoetNameInUrdu = sherPoetNameInUrdu;
        this.sherTitleInEng = sherTitleInEng;
        this.sherTitleInHin = sherTitleInHin;
        this.sherTitleInUrdu = sherTitleInUrdu;
        this.isEditorChoice = isEditorChoice;
        this.isPopularChoice = isPopularChoice;
        this.isAudioAvailable = isAudioAvailable;
        this.isVideoAvailable = isVideoAvailable;
        this.sherShareLinkInEng = sherShareLinkInEng;
        this.sherShareLinkInHin = sherShareLinkInHin;
        this.sherShareLinkInUrdu = sherShareLinkInUrdu;
    }

    public String getSherId() {
        return sherId;
    }

    public void setSherId(String sherId) {
        this.sherId = sherId;
    }

    public String getSherPoetNameInEng() {
        return sherPoetNameInEng;
    }

    public void setSherPoetNameInEng(String sherPoetNameInEng) {
        this.sherPoetNameInEng = sherPoetNameInEng;
    }

    public String getSherPoetNameInHin() {
        return sherPoetNameInHin;
    }

    public void setSherPoetNameInHin(String sherPoetNameInHin) {
        this.sherPoetNameInHin = sherPoetNameInHin;
    }

    public String getSherPoetNameInUrdu() {
        return sherPoetNameInUrdu;
    }

    public void setSherPoetNameInUrdu(String sherPoetNameInUrdu) {
        this.sherPoetNameInUrdu = sherPoetNameInUrdu;
    }

    public String getSherTitleInEng() {
        return sherTitleInEng;
    }

    public void setSherTitleInEng(String sherTitleInEng) {
        this.sherTitleInEng = sherTitleInEng;
    }

    public String getSherTitleInHin() {
        return sherTitleInHin;
    }

    public void setSherTitleInHin(String sherTitleInHin) {
        this.sherTitleInHin = sherTitleInHin;
    }

    public String getSherTitleInUrdu() {
        return sherTitleInUrdu;
    }

    public void setSherTitleInUrdu(String sherTitleInUrdu) {
        this.sherTitleInUrdu = sherTitleInUrdu;
    }

    public boolean isEditorChoice() {
        return isEditorChoice;
    }

    public void setEditorChoice(boolean editorChoice) {
        isEditorChoice = editorChoice;
    }

    public boolean isPopularChoice() {
        return isPopularChoice;
    }

    public void setPopularChoice(boolean popularChoice) {
        isPopularChoice = popularChoice;
    }

    public boolean isAudioAvailable() {
        return isAudioAvailable;
    }

    public void setAudioAvailable(boolean audioAvailable) {
        isAudioAvailable = audioAvailable;
    }

    public boolean isVideoAvailable() {
        return isVideoAvailable;
    }

    public void setVideoAvailable(boolean videoAvailable) {
        isVideoAvailable = videoAvailable;
    }

    public String getSherShareLinkInHin() {
        return sherShareLinkInHin;
    }

    public void setSherShareLinkInHin(String sherShareLinkInHin) {
        this.sherShareLinkInHin = sherShareLinkInHin;
    }


    public String getSherShareLinkInEng() {
        return sherShareLinkInEng;
    }

    public void setSherShareLinkInEng(String sherShareLinkInEng) {
        this.sherShareLinkInEng = sherShareLinkInEng;
    }

    public String getSherShareLinkInUrdu() {
        return sherShareLinkInUrdu;
    }

    public void setSherShareLinkInUrdu(String sherShareLinkInUrdu) {
        this.sherShareLinkInUrdu = sherShareLinkInUrdu;
    }

}
