package org.Rekhta.model.nazm;

/**
 * Created by SANCHIT on 10/3/2017.
 */

public class nazmTop50Model {

    private String nazmId;               // for I
    private String NpoetEnglish;         //  for PE
    private String NpoetHindi;          // for PH
    private String NpoetUrdu;           // for PU
    private String NTitleEng;            //SE
    private String NTitleHin;
    private String NTitleUrdu;
    private String NeditorChoice;
    private String NaudioAvaialbe;
    private String NvideoAvailable;

    public nazmTop50Model(String nazmId,String NpoetEnglish,String NpoetHindi,String NpoetUrdu,String NTitleEng,
                     String NTitleHin, String NTitleUrdu ,String NeditorChoice){

   this.nazmId = nazmId;
   this.NpoetEnglish = NpoetEnglish;
   this.NpoetHindi = NpoetHindi;
   this.NpoetUrdu = NpoetUrdu;
   this.NTitleEng = NTitleEng;
   this.NTitleHin = NTitleHin;
   this.NTitleUrdu = NTitleUrdu;
   this.NeditorChoice = NeditorChoice;
/*   this.NaudioAvaialbe = NaudioAvaialbe;
   this.NvideoAvailable = NvideoAvailable;*/

    }

    public String getNazmId() {
        return nazmId;
    }

    public void setNazmId(String nazmId) {
        this.nazmId = nazmId;
    }

    public String getNpoetEnglish() {
        return NpoetEnglish;
    }

    public void setNpoetEnglish(String npoetEnglish) {
        NpoetEnglish = npoetEnglish;
    }

    public String getNpoetHindi() {
        return NpoetHindi;
    }

    public void setNpoetHindi(String npoetHindi) {
        NpoetHindi = npoetHindi;
    }

    public String getNpoetUrdu() {
        return NpoetUrdu;
    }

    public void setNpoetUrdu(String npoetUrdu) {
        NpoetUrdu = npoetUrdu;
    }

    public String getNTitleEng() {
        return NTitleEng;
    }

    public void setNTitleEng(String NTitleEng) {
        this.NTitleEng = NTitleEng;
    }

    public String getNTitleHin() {
        return NTitleHin;
    }

    public void setNTitleHin(String NTitleHin) {
        this.NTitleHin = NTitleHin;
    }

    public String getNTitleUrdu() {
        return NTitleUrdu;
    }

    public void setNTitleUrdu(String NTitleUrdu) {
        this.NTitleUrdu = NTitleUrdu;
    }

    public String getNeditorChoice() {
        return NeditorChoice;
    }

    public void setNeditorChoice(String neditorChoice) {
        NeditorChoice = neditorChoice;
    }

    public String isNaudioAvaialbe() {
        return NaudioAvaialbe;
    }

    public void setNaudioAvaialbe(String naudioAvaialbe) {
        NaudioAvaialbe = naudioAvaialbe;
    }

    public String isNvideoAvailable() {
        return NvideoAvailable;
    }

    public void setNvideoAvailable(String nvideoAvailable) {
        NvideoAvailable = nvideoAvailable;
    }
}
