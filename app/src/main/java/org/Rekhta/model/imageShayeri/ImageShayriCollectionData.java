package org.Rekhta.model.imageShayeri;

/**
 * Created by SANCHIT on 11/3/2017.
 */

public class ImageShayriCollectionData  {

//            "I":"07de04fb-5d3c-45b3-a158-560bef1cbd25",
//            "PI":"1f858403-a6e0-4ec7-ade7-b739f4b9d516",
//            "NE":"Seemab Akbarabadi",
//            "NH":"प्रसून जोशी",
//            "NU":"پراسون جوشی",
//            "CI":"f535e3d4-ffda-49af-986b-c7a06202b25c",

    private String imageId;
    private String imagePoetId;
    private String imagePoetNameInEng;

    private String imagePoetNameInHin;
    private String imagePoetNameInUrdu;

    private String imageContentId;

  /*  public ImageShayriCollectionData1(String imageId, String imagePoetId,
                                     String imagePoetNameInEng, String imagePoetNameInHin, String imagePoetNameInUrdu,
                                     String imageContentId) {
        this.imageId = imageId;
        this.imagePoetId = imagePoetId;
        this.imagePoetNameInEng = imagePoetNameInEng;
        this.imagePoetNameInHin = imagePoetNameInHin;
        this.imagePoetNameInUrdu = imagePoetNameInUrdu;
        this.imageContentId = imageContentId;
    }*/



    public ImageShayriCollectionData(String imageId, String imagePoetId,
                                     String imagePoetNameInEng, String imagePoetNameInHin, String imagePoetNameInUrdu,
                                     String imageContentId) {
        this.imageId = imageId;
        this.imagePoetId = imagePoetId;
        this.imagePoetNameInEng = imagePoetNameInEng;
        this.imagePoetNameInHin = imagePoetNameInHin;
        this.imagePoetNameInUrdu = imagePoetNameInUrdu;
        this.imageContentId = imageContentId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImagePoetId() {
        return imagePoetId;
    }

    public void setImagePoetId(String imagePoetId) {
        this.imagePoetId = imagePoetId;
    }

    public String getImagePoetNameInEng() {
        return imagePoetNameInEng;
    }

    public void setImagePoetNameInEng(String imagePoetNameInEng) {
        this.imagePoetNameInEng = imagePoetNameInEng;
    }

    public String getImagePoetNameInHin() {
        return imagePoetNameInHin;
    }

    public void setImagePoetNameInHin(String imagePoetNameInHin) {
        this.imagePoetNameInHin = imagePoetNameInHin;
    }

    public String getImagePoetNameInUrdu() {
        return imagePoetNameInUrdu;
    }

    public void setImagePoetNameInUrdu(String imagePoetNameInUrdu) {
        this.imagePoetNameInUrdu = imagePoetNameInUrdu;
    }

    public String getImageContentId() {
        return imageContentId;
    }

    public void setImageContentId(String imageContentId) {
        this.imageContentId = imageContentId;
    }


}
