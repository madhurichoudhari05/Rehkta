package org.Rekhta.model.imageShayeri;

import java.util.List;

import org.Rekhta.adapters.ImageShayari.ImageListModel;

/**
 * Created by SANCHIT on 11/3/2017.
 */

public class ImageShayriCollectionDataGallery  {

//            "I":"07de04fb-5d3c-45b3-a158-560bef1cbd25",
//            "PI":"1f858403-a6e0-4ec7-ade7-b739f4b9d516",
//            "NE":"Seemab Akbarabadi",
//            "NH":"प्रसून जोशी",
//            "NU":"پراسون جوشی",
//            "CI":"f535e3d4-ffda-49af-986b-c7a06202b25c",










    public List<ImageListModel> getList() {
        return list;
    }

    public void setList(List<ImageListModel> list) {
        this.list = list;
    }

    private List<ImageListModel> list;


    private String imageId;
    private String imagePoetId;
    private String imagePoetNameInEng;

    private String imagePoetNameInHin;
    private String imagePoetNameInUrdu;

    private String imageContentId;

    public ImageShayriCollectionDataGallery() {
    }

    public String getImageDate() {
        return imageDate;
    }

    public void setImageDate(String imageDate) {
        this.imageDate = imageDate;
    }

    private String imageDate;


    public ImageShayriCollectionDataGallery(String imageId, String imagePoetId,
                                     String imagePoetNameInEng, String imagePoetNameInHin, String imagePoetNameInUrdu,
                                     String imageContentId,String imageDate) {
        this.imageId = imageId;
        this.imagePoetId = imagePoetId;
        this.imagePoetNameInEng = imagePoetNameInEng;
        this.imagePoetNameInHin = imagePoetNameInHin;
        this.imagePoetNameInUrdu = imagePoetNameInUrdu;
        this.imageContentId = imageContentId;
        this.imageDate = imageDate;
    }



    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImagePoetId() {
        return imagePoetId;
    }

    public void setImagePoetId(String imagePoetId) {
        this.imagePoetId = imagePoetId;
    }

    public String getImagePoetNameInEng() {
        return imagePoetNameInEng;
    }

    public void setImagePoetNameInEng(String imagePoetNameInEng) {
        this.imagePoetNameInEng = imagePoetNameInEng;
    }

    public String getImagePoetNameInHin() {
        return imagePoetNameInHin;
    }

    public void setImagePoetNameInHin(String imagePoetNameInHin) {
        this.imagePoetNameInHin = imagePoetNameInHin;
    }

    public String getImagePoetNameInUrdu() {
        return imagePoetNameInUrdu;
    }

    public void setImagePoetNameInUrdu(String imagePoetNameInUrdu) {
        this.imagePoetNameInUrdu = imagePoetNameInUrdu;
    }

    public String getImageContentId() {
        return imageContentId;
    }

    public void setImageContentId(String imageContentId) {
        this.imageContentId = imageContentId;
    }


}
