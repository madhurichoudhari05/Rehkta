package org.Rekhta.model.homeScreenModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/6/2017.
 */

public class ImageTags implements Parcelable {

    @Expose
    @SerializedName("CoupletId")
    private String CoupletId;

    @Expose
    @SerializedName("TagSlug")
    private String TagSlug;

    @Expose
    @SerializedName("TagId")
    private String TagId;

    @Expose
    @SerializedName("TagName")
    private String TagName;

    protected ImageTags(Parcel in) {
        CoupletId = in.readString();
        TagSlug = in.readString();
        TagId = in.readString();
        TagName = in.readString();
    }

    public static final Creator<ImageTags> CREATOR = new Creator<ImageTags>() {
        @Override
        public ImageTags createFromParcel(Parcel in) {
            return new ImageTags(in);
        }

        @Override
        public ImageTags[] newArray(int size) {
            return new ImageTags[size];
        }
    };

    public String getCoupletId ()
    {
        return CoupletId;
    }

    public void setCoupletId (String CoupletId)
    {
        this.CoupletId = CoupletId;
    }

    public String getTagSlug ()
    {
        return TagSlug;
    }

    public void setTagSlug (String TagSlug)
    {
        this.TagSlug = TagSlug;
    }

    public String getTagId ()
    {
        return TagId;
    }

    public void setTagId (String TagId)
    {
        this.TagId = TagId;
    }

    public String getTagName ()
    {
        return TagName;
    }

    public void setTagName (String TagName)
    {
        this.TagName = TagName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CoupletId = "+CoupletId+", TagSlug = "+TagSlug+", TagId = "+TagId+", TagName = "+TagName+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(CoupletId);
        parcel.writeString(TagSlug);
        parcel.writeString(TagId);
        parcel.writeString(TagName);
    }
}
