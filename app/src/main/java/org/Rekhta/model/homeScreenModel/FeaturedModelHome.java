package org.Rekhta.model.homeScreenModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/6/2017.
 */

public class FeaturedModelHome implements Parcelable{

    @Expose
    @SerializedName("EntityUrl")
    private String EntityUrl;

    @Expose
    @SerializedName("ToDate")
    private String ToDate;

    @Expose
    @SerializedName("RenderingFormat")
    private String RenderingFormat;

    @Expose
    @SerializedName("AnchorText")
    private String AnchorText;

    @Expose
    @SerializedName("Remarks")
    private String Remarks;

    @Expose
    @SerializedName("FromDate")
    private String FromDate;

    @Expose
    @SerializedName("ImageUrl")
    private String ImageUrl;

    @Expose
    @SerializedName("EntityName")
    private String EntityName;

    @Expose
    @SerializedName("DestinationUrl")
    private String DestinationUrl;

    @Expose
    @SerializedName("DayType")
    private String DayType;

    @Expose
    @SerializedName("Title")
    private String Title;

    protected FeaturedModelHome(Parcel in) {
        EntityUrl = in.readString();
        ToDate = in.readString();
        RenderingFormat = in.readString();
        AnchorText = in.readString();
        Remarks = in.readString();
        FromDate = in.readString();
        ImageUrl = in.readString();
        EntityName = in.readString();
        DestinationUrl = in.readString();
        DayType = in.readString();
        Title = in.readString();
    }

    public static final Creator<FeaturedModelHome> CREATOR = new Creator<FeaturedModelHome>() {
        @Override
        public FeaturedModelHome createFromParcel(Parcel in) {
            return new FeaturedModelHome(in);
        }

        @Override
        public FeaturedModelHome[] newArray(int size) {
            return new FeaturedModelHome[size];
        }
    };

    public String getEntityUrl ()
    {
        return EntityUrl;
    }

    public void setEntityUrl (String EntityUrl)
    {
        this.EntityUrl = EntityUrl;
    }

    public String getToDate ()
    {
        return ToDate;
    }

    public void setToDate (String ToDate)
    {
        this.ToDate = ToDate;
    }

    public String getRenderingFormat ()
    {
        return RenderingFormat;
    }

    public void setRenderingFormat (String RenderingFormat)
    {
        this.RenderingFormat = RenderingFormat;
    }

    public String getAnchorText ()
    {
        return AnchorText;
    }

    public void setAnchorText (String AnchorText)
    {
        this.AnchorText = AnchorText;
    }

    public String getRemarks ()
    {
        return Remarks;
    }

    public void setRemarks (String Remarks)
    {
        this.Remarks = Remarks;
    }

    public String getFromDate ()
    {
        return FromDate;
    }

    public void setFromDate (String FromDate)
    {
        this.FromDate = FromDate;
    }

    public String getImageUrl ()
    {
        return ImageUrl;
    }

    public void setImageUrl (String ImageUrl)
    {
        this.ImageUrl = ImageUrl;
    }

    public String getEntityName ()
    {
        return EntityName;
    }

    public void setEntityName (String EntityName)
    {
        this.EntityName = EntityName;
    }

    public String getDestinationUrl ()
    {
        return DestinationUrl;
    }

    public void setDestinationUrl (String DestinationUrl)
    {
        this.DestinationUrl = DestinationUrl;
    }

    public String getDayType ()
    {
        return DayType;
    }

    public void setDayType (String DayType)
    {
        this.DayType = DayType;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [EntityUrl = "+EntityUrl+", ToDate = "+ToDate+", RenderingFormat = "+RenderingFormat+", AnchorText = "+AnchorText+", Remarks = "+Remarks+", FromDate = "+FromDate+", ImageUrl = "+ImageUrl+", EntityName = "+EntityName+", DestinationUrl = "+DestinationUrl+", DayType = "+DayType+", Title = "+Title+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(EntityUrl);
        dest.writeString(ToDate);
        dest.writeString(RenderingFormat);
        dest.writeString(AnchorText);
        dest.writeString(Remarks);
        dest.writeString(FromDate);
        dest.writeString(ImageUrl);
        dest.writeString(EntityName);
        dest.writeString(DestinationUrl);
        dest.writeString(DayType);
        dest.writeString(Title);
    }
}
