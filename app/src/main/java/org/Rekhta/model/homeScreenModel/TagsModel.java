package org.Rekhta.model.homeScreenModel;

/**
 * Created by Admin on 12/6/2017.
 */

public class TagsModel {

    private String TagSlug;

    private String ContentId;

    private String TagId;

    private String TagName;

    public String getTagSlug ()
    {
        return TagSlug;
    }

    public void setTagSlug (String TagSlug)
    {
        this.TagSlug = TagSlug;
    }

    public String getContentId ()
    {
        return ContentId;
    }

    public void setContentId (String ContentId)
    {
        this.ContentId = ContentId;
    }

    public String getTagId ()
    {
        return TagId;
    }

    public void setTagId (String TagId)
    {
        this.TagId = TagId;
    }

    public String getTagName ()
    {
        return TagName;
    }

    public void setTagName (String TagName)
    {
        this.TagName = TagName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [TagSlug = "+TagSlug+", ContentId = "+ContentId+", TagId = "+TagId+", TagName = "+TagName+"]";
    }
}
