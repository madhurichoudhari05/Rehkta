package org.Rekhta.model.homeScreenModel;

/**
 * Created by Admin on 12/6/2017.
 */

public class TopPoetsModel {

    private String Name;

    private String SEO_Slug;

    private String HaveImage;

    private String EntityId;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getSEO_Slug ()
    {
        return SEO_Slug;
    }

    public void setSEO_Slug (String SEO_Slug)
    {
        this.SEO_Slug = SEO_Slug;
    }

    public String getHaveImage ()
    {
        return HaveImage;
    }

    public void setHaveImage (String HaveImage)
    {
        this.HaveImage = HaveImage;
    }

    public String getEntityId ()
    {
        return EntityId;
    }

    public void setEntityId (String EntityId)
    {
        this.EntityId = EntityId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", SEO_Slug = "+SEO_Slug+", HaveImage = "+HaveImage+", EntityId = "+EntityId+"]";
    }
}
