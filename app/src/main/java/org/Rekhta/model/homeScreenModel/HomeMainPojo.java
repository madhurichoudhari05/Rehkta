package org.Rekhta.model.homeScreenModel;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeMainPojo  implements Parcelable{

    @Expose
    @SerializedName("Me")
    private String Me;

    @Expose
    @SerializedName("T")
    private String T;

    @Expose
    @SerializedName("Mh")
    private String Mh;

    @Expose
    @SerializedName("S")
    private String S;

    @Expose
    @SerializedName("R")
    private RHomePojo rHomePojo;

    private String Mu;

    protected HomeMainPojo(Parcel in) {
        Me = in.readString();
        T = in.readString();
        Mh = in.readString();
        S = in.readString();
        Mu = in.readString();


    }

    public static final Creator<HomeMainPojo> CREATOR = new Creator<HomeMainPojo>() {
        @Override
        public HomeMainPojo createFromParcel(Parcel in) {
            return new HomeMainPojo(in);
        }

        @Override
        public HomeMainPojo[] newArray(int size) {
            return new HomeMainPojo[size];
        }
    };

    public String getMe() {
        return Me;
    }

    public void setMe(String Me) {
        this.Me = Me;
    }

    public String getT() {
        return T;
    }

    public void setT(String T) {
        this.T = T;
    }

    public String getMh() {
        return Mh;
    }

    public void setMh(String Mh) {
        this.Mh = Mh;
    }

    public String getS() {
        return S;
    }

    public void setS(String S) {
        this.S = S;
    }

    public RHomePojo getR() {
        return rHomePojo;
    }

    public void setR(RHomePojo rHomePojo) {
        this.rHomePojo = rHomePojo;
    }

    public String getMu() {
        return Mu;
    }

    public void setMu(String Mu) {
        this.Mu = Mu;
    }

    @Override
    public String toString() {
        return "ClassPojo [Me = " + Me + ", T = " + T + ", Mh = " + Mh + ", S = " + S + ", R = " + rHomePojo + ", Mu = " + Mu + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Me);
        dest.writeString(T);
        dest.writeString(Mh);
        dest.writeString(S);
        dest.writeString(Mu);
    }
}
