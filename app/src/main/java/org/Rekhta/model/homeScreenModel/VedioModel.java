package org.Rekhta.model.homeScreenModel;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VedioModel {

    @Expose
    @SerializedName("EntityUrl")
    private String EntityUrl;

    @Expose
    @SerializedName("VideoTitle")
    private String VideoTitle;

    @Expose
    @SerializedName("ImageUrl")
    private String ImageUrl;

    @Expose
    @SerializedName("EntityName")
    private String EntityName;

    @Expose
    @SerializedName("Youtube_Id")
    private String Youtube_Id;

    @Expose
    @SerializedName("ShortUrl_En")
    private String ShortUrl_En;

    @Expose
    @SerializedName("ShortUrl_Hi")
    private String ShortUrl_Hi;

    @Expose
    @SerializedName("ShortUrl_Ur")
    private String ShortUrl_Ur;

    public String getShortUrl_En() {
        return ShortUrl_En;
    }

    public void setShortUrl_En(String shortUrl_En) {
        ShortUrl_En = shortUrl_En;
    }

    public String getShortUrl_Hi() {
        return ShortUrl_Hi;
    }

    public void setShortUrl_Hi(String shortUrl_Hi) {
        ShortUrl_Hi = shortUrl_Hi;
    }

    public String getShortUrl_Ur() {
        return ShortUrl_Ur;
    }

    public void setShortUrl_Ur(String shortUrl_Ur) {
        ShortUrl_Ur = shortUrl_Ur;
    }

    public String getEntityUrl ()
    {
        return EntityUrl;
    }

    public void setEntityUrl (String EntityUrl)
    {
        this.EntityUrl = EntityUrl;
    }

    public String getVideoTitle ()
    {
        return VideoTitle;
    }

    public void setVideoTitle (String VideoTitle)
    {
        this.VideoTitle = VideoTitle;
    }

    public String getImageUrl ()
    {
        return ImageUrl;
    }

    public void setImageUrl (String ImageUrl)
    {
        this.ImageUrl = ImageUrl;
    }

    public String getEntityName ()
    {
        return EntityName;
    }

    public void setEntityName (String EntityName)
    {
        this.EntityName = EntityName;
    }

    public String getYoutube_Id ()
    {
        return Youtube_Id;
    }

    public void setYoutube_Id (String Youtube_Id)
    {
        this.Youtube_Id = Youtube_Id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [EntityUrl = "+EntityUrl+", VideoTitle = "+VideoTitle+", ImageUrl = "+ImageUrl+", EntityName = "+EntityName+", Youtube_Id = "+Youtube_Id+"]";
    }
}
