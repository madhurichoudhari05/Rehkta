package org.Rekhta.model.homeScreenModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RHomePojo implements Parcelable{

    @Expose
    @SerializedName("Carousels")
    private List<CarouselsModel> Carousels;

    @Expose
    @SerializedName("ImgShayari")
    private List<ImageShaeyriModel> ImgShayari;

    @Expose
    @SerializedName("WordOfTheDay")
    private WordOfDayModel WordOfTheDay;

    @Expose
    @SerializedName("TodaysTop")
    private List<org.Rekhta.model.homeScreenModel.TodaysTop> TodaysTop;

    @Expose
    @SerializedName("Featured")
    private List<FeaturedModelHome> featuredModelHomes;

    @Expose
    @SerializedName("T20Series")
    private List<T20Series> T20Series;

    @Expose
    @SerializedName("Cards")
    private List<HomeCards> Cards;

    @Expose
    @SerializedName("Video")
    private VedioModel Video;

    @Expose
    @SerializedName("TopPoets")
    private List<TopPoetsModel> TopPoets;


    protected RHomePojo(Parcel in) {
        Carousels = in.createTypedArrayList(CarouselsModel.CREATOR);
        featuredModelHomes = in.createTypedArrayList(FeaturedModelHome.CREATOR);
        T20Series = in.createTypedArrayList(org.Rekhta.model.homeScreenModel.T20Series.CREATOR);
    }

    public static final Creator<RHomePojo> CREATOR = new Creator<RHomePojo>() {
        @Override
        public RHomePojo createFromParcel(Parcel in) {
            return new RHomePojo(in);
        }

        @Override
        public RHomePojo[] newArray(int size) {
            return new RHomePojo[size];
        }
    };

    public List<CarouselsModel> getCarousels ()
    {
        return Carousels;
    }

    public void setCarousels (List<CarouselsModel> Carousels)
    {
        this.Carousels = Carousels;
    }

    public List<ImageShaeyriModel>  getImgShayari ()
    {
        return ImgShayari;
    }

    public void setImgShayari (List<ImageShaeyriModel>  ImgShayari)
    {
        this.ImgShayari = ImgShayari;
    }

    public WordOfDayModel getWordOfTheDay ()
    {
        return WordOfTheDay;
    }

    public void setWordOfTheDay (WordOfDayModel WordOfTheDay)
    {
        this.WordOfTheDay = WordOfTheDay;
    }

    public List<org.Rekhta.model.homeScreenModel.TodaysTop> getTodaysTop ()
    {
        return TodaysTop;
    }

    public void setTodaysTop (List<org.Rekhta.model.homeScreenModel.TodaysTop> TodaysTop)
    {
        this.TodaysTop = TodaysTop;
    }

    public List<FeaturedModelHome> getFeatured ()
    {
        return featuredModelHomes;
    }

    public void setFeatured (List<FeaturedModelHome> Featured)
    {
        this.featuredModelHomes = Featured;
    }

    public List<T20Series> getT20Series ()
    {
        return T20Series;
    }

    public void setT20Series (List<T20Series> T20Series)
    {
        this.T20Series = T20Series;
    }

    public List<HomeCards> getCards ()
    {
        return Cards;
    }

    public void setCards (List<HomeCards> Cards)
    {
        this.Cards = Cards;
    }

    public VedioModel getVideo ()
    {
        return Video;
    }

    public void setVideo (VedioModel Video)
    {
        this.Video = Video;
    }

    public List<TopPoetsModel> getTopPoets ()
    {
        return TopPoets;
    }

    public void setTopPoets (List<TopPoetsModel> TopPoets)
    {
        this.TopPoets = TopPoets;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Carousels = "+Carousels+", ImgShayari = "+ImgShayari+", WordOfTheDay = "+WordOfTheDay+", TodaysTop = "+TodaysTop+", Featured = "+featuredModelHomes+", T20Series = "+T20Series+", Cards = "+Cards+", Video = "+Video+", TopPoets = "+TopPoets+"]";
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(Carousels);
        dest.writeTypedList(featuredModelHomes);
        dest.writeTypedList(T20Series);
    }
}
