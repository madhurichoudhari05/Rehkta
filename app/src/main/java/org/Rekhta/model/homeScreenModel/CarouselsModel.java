package org.Rekhta.model.homeScreenModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/6/2017.
 */

public class CarouselsModel implements Parcelable{

    @Expose
    @SerializedName("LU")
    private String LU;

    @Expose
    @SerializedName("TIN")
    private String TIN;

    @Expose
    @SerializedName("TD")
    private String TD;

    @Expose
    @SerializedName("FD")
    private String FD;

    @Expose
    @SerializedName("TI")
    private String TI;

    @Expose
    @SerializedName("BU")
    private String BU;

    @Expose
    @SerializedName("BT")
    private String BT;


    @Expose
    @SerializedName("PNU")
    private String PNU;

    @Expose
    @SerializedName("MP")
    private String MP;

    @Expose
    @SerializedName("IT")
    private String IT;

    @Expose
    @SerializedName("BLU")
    private String BLU;

    @Expose
    @SerializedName("I")
    private String I;

    @Expose
    @SerializedName("CI")
    private String CI;

    @Expose
    @SerializedName("LC")
    private String LC;

    @Expose
    @SerializedName("BH")
    private String BH;

    @Expose
    @SerializedName("LB")
    private String LB;

    @Expose
    @SerializedName("BI")
    private String BI;

    @Expose
    @SerializedName("PNH")
    private String PNH;

    @Expose
    @SerializedName("S")
    private String S;

    @Expose
    @SerializedName("BE")
    private String BE;

    @Expose
    @SerializedName("PNE")
    private String PNE;

    @Expose
    @SerializedName("LH")
    private String LH;

    @Expose
    @SerializedName("BLE")
    private String BLE;

    @Expose
    @SerializedName("TU")
    private String TU;

    @Expose
    @SerializedName("LE")
    private String LE;

    @Expose
    @SerializedName("BLH")
    private String BLH;

    protected CarouselsModel(Parcel in) {
        LU = in.readString();
        TIN = in.readString();
        TD = in.readString();
        FD = in.readString();
        TI = in.readString();
        BU = in.readString();
        BT = in.readString();
        PNU = in.readString();
        MP = in.readString();
        IT = in.readString();
        BLU = in.readString();
        I = in.readString();
        CI = in.readString();
        LC = in.readString();
        BH = in.readString();
        LB = in.readString();
        BI = in.readString();
        PNH = in.readString();
        S = in.readString();
        BE = in.readString();
        PNE = in.readString();
        LH = in.readString();
        BLE = in.readString();
        TU = in.readString();
        LE = in.readString();
        BLH = in.readString();
    }

    public static final Creator<CarouselsModel> CREATOR = new Creator<CarouselsModel>() {
        @Override
        public CarouselsModel createFromParcel(Parcel in) {
            return new CarouselsModel(in);
        }

        @Override
        public CarouselsModel[] newArray(int size) {
            return new CarouselsModel[size];
        }
    };

    public String getLU ()
    {
        return LU;
    }

    public void setLU (String LU)
    {
        this.LU = LU;
    }

    public String getTIN ()
    {
        return TIN;
    }

    public void setTIN (String TIN)
    {
        this.TIN = TIN;
    }

    public String getTD ()
    {
        return TD;
    }

    public void setTD (String TD)
    {
        this.TD = TD;
    }

    public String getFD ()
    {
        return FD;
    }

    public void setFD (String FD)
    {
        this.FD = FD;
    }

    public String getTI ()
    {
        return TI;
    }

    public void setTI (String TI)
    {
        this.TI = TI;
    }

    public String getBU ()
    {
        return BU;
    }

    public void setBU (String BU)
    {
        this.BU = BU;
    }

    public String getBT ()
    {
        return BT;
    }

    public void setBT (String BT)
    {
        this.BT = BT;
    }

    public String getPNU ()
    {
        return PNU;
    }

    public void setPNU (String PNU)
    {
        this.PNU = PNU;
    }

    public String getMP ()
    {
        return MP;
    }

    public void setMP (String MP)
    {
        this.MP = MP;
    }

    public String getIT ()
    {
        return IT;
    }

    public void setIT (String IT)
    {
        this.IT = IT;
    }

    public String getBLU ()
    {
        return BLU;
    }

    public void setBLU (String BLU)
    {
        this.BLU = BLU;
    }

    public String getI ()
    {
        return I;
    }

    public void setI (String I)
    {
        this.I = I;
    }

    public String getCI ()
    {
        return CI;
    }

    public void setCI (String CI)
    {
        this.CI = CI;
    }

    public String getLC ()
    {
        return LC;
    }

    public void setLC (String LC)
    {
        this.LC = LC;
    }

    public String getBH ()
    {
        return BH;
    }

    public void setBH (String BH)
    {
        this.BH = BH;
    }

    public String getLB ()
    {
        return LB;
    }

    public void setLB (String LB)
    {
        this.LB = LB;
    }

    public String getBI ()
    {
        return BI;
    }

    public void setBI (String BI)
    {
        this.BI = BI;
    }

    public String getPNH ()
    {
        return PNH;
    }

    public void setPNH (String PNH)
    {
        this.PNH = PNH;
    }

    public String getS ()
    {
        return S;
    }

    public void setS (String S)
    {
        this.S = S;
    }

    public String getBE ()
    {
        return BE;
    }

    public void setBE (String BE)
    {
        this.BE = BE;
    }

    public String getPNE ()
    {
        return PNE;
    }

    public void setPNE (String PNE)
    {
        this.PNE = PNE;
    }

    public String getLH ()
    {
        return LH;
    }

    public void setLH (String LH)
    {
        this.LH = LH;
    }

    public String getBLE ()
    {
        return BLE;
    }

    public void setBLE (String BLE)
    {
        this.BLE = BLE;
    }

    public String getTU ()
    {
        return TU;
    }

    public void setTU (String TU)
    {
        this.TU = TU;
    }

    public String getLE ()
    {
        return LE;
    }

    public void setLE (String LE)
    {
        this.LE = LE;
    }

    public String getBLH ()
    {
        return BLH;
    }

    public void setBLH (String BLH)
    {
        this.BLH = BLH;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [LU = "+LU+", TIN = "+TIN+", TD = "+TD+", FD = "+FD+", TI = "+TI+", BU = "+BU+", BT = "+BT+", PNU = "+PNU+", MP = "+MP+", IT = "+IT+", BLU = "+BLU+", I = "+I+", CI = "+CI+", LC = "+LC+", BH = "+BH+", LB = "+LB+", BI = "+BI+", PNH = "+PNH+", S = "+S+", BE = "+BE+", PNE = "+PNE+", LH = "+LH+", BLE = "+BLE+", TU = "+TU+", LE = "+LE+", BLH = "+BLH+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(LU);
        dest.writeString(TIN);
        dest.writeString(TD);
        dest.writeString(FD);
        dest.writeString(TI);
        dest.writeString(BU);
        dest.writeString(BT);
        dest.writeString(PNU);
        dest.writeString(MP);
        dest.writeString(IT);
        dest.writeString(BLU);
        dest.writeString(I);
        dest.writeString(CI);
        dest.writeString(LC);
        dest.writeString(BH);
        dest.writeString(LB);
        dest.writeString(BI);
        dest.writeString(PNH);
        dest.writeString(S);
        dest.writeString(BE);
        dest.writeString(PNE);
        dest.writeString(LH);
        dest.writeString(BLE);
        dest.writeString(TU);
        dest.writeString(LE);
        dest.writeString(BLH);
    }
}
