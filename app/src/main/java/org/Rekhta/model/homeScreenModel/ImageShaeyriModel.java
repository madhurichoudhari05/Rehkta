package org.Rekhta.model.homeScreenModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 12/6/2017.
 */

public class ImageShaeyriModel {

    @Expose
    @SerializedName("Name")
    private String Name;

    @Expose
    @SerializedName("CoupletId")
    private String CoupletId;

    @Expose
    @SerializedName("ShortUrlIndex")
    private String ShortUrlIndex;

    @Expose
    @SerializedName("SEO_Slug")
    private String SEO_Slug;

    @Expose
    @SerializedName("ShortUrl_En")
    private String ShortUrl_En;

    @Expose
    @SerializedName("ShortUrl_Hi")
    private String ShortUrl_Hi;

    @Expose
    @SerializedName("PoetId")
    private String PoetId;

    @Expose
    @SerializedName("Id")
    private String Id;

    @Expose
    @SerializedName("imgTags")
    private List<ImageTags> imgTags;

    private String Tag1;

    private String ShortUrl_Ur;

    private String IsSher;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getCoupletId ()
    {
        return CoupletId;
    }

    public void setCoupletId (String CoupletId)
    {
        this.CoupletId = CoupletId;
    }

    public String getShortUrlIndex ()
    {
        return ShortUrlIndex;
    }

    public void setShortUrlIndex (String ShortUrlIndex)
    {
        this.ShortUrlIndex = ShortUrlIndex;
    }

    public String getSEO_Slug ()
    {
        return SEO_Slug;
    }

    public void setSEO_Slug (String SEO_Slug)
    {
        this.SEO_Slug = SEO_Slug;
    }

    public String getShortUrl_En ()
    {
        return ShortUrl_En;
    }

    public void setShortUrl_En (String ShortUrl_En)
    {
        this.ShortUrl_En = ShortUrl_En;
    }

    public String getShortUrl_Hi ()
    {
        return ShortUrl_Hi;
    }

    public void setShortUrl_Hi (String ShortUrl_Hi)
    {
        this.ShortUrl_Hi = ShortUrl_Hi;
    }

    public String getPoetId ()
    {
        return PoetId;
    }

    public void setPoetId (String PoetId)
    {
        this.PoetId = PoetId;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public List<ImageTags> getImgTags ()
    {
        return imgTags;
    }

    public void setImgTags (List<ImageTags> imgTags)
    {
        this.imgTags = imgTags;
    }

    public String getTag1 ()
    {
        return Tag1;
    }

    public void setTag1 (String Tag1)
    {
        this.Tag1 = Tag1;
    }

    public String getShortUrl_Ur ()
    {
        return ShortUrl_Ur;
    }

    public void setShortUrl_Ur (String ShortUrl_Ur)
    {
        this.ShortUrl_Ur = ShortUrl_Ur;
    }

    public String getIsSher ()
    {
        return IsSher;
    }

    public void setIsSher (String IsSher)
    {
        this.IsSher = IsSher;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", CoupletId = "+CoupletId+", ShortUrlIndex = "+ShortUrlIndex+", SEO_Slug = "+SEO_Slug+", ShortUrl_En = "+ShortUrl_En+", ShortUrl_Hi = "+ShortUrl_Hi+", PoetId = "+PoetId+", Id = "+Id+", imgTags = "+imgTags+", Tag1 = "+Tag1+", ShortUrl_Ur = "+ShortUrl_Ur+", IsSher = "+IsSher+"]";
    }
}
