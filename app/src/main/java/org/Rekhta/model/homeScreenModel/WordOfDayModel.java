package org.Rekhta.model.homeScreenModel;

/**
 * Created by Admin on 12/6/2017.
 */

public class WordOfDayModel {

    private String PoetName;

    private String Meaning;

    private String Word_En;

    private String ParentTitle;

    private String RenderingFormat;

    private String ParentSlug;

    private String Word_Hi;

    private String Word_Ur;

    private String PoetSlug;

    private String Title;

    public String getPoetName ()
    {
        return PoetName;
    }

    public void setPoetName (String PoetName)
    {
        this.PoetName = PoetName;
    }

    public String getMeaning ()
    {
        return Meaning;
    }

    public void setMeaning (String Meaning)
    {
        this.Meaning = Meaning;
    }

    public String getWord_En ()
    {
        return Word_En;
    }

    public void setWord_En (String Word_En)
    {
        this.Word_En = Word_En;
    }

    public String getParentTitle ()
    {
        return ParentTitle;
    }

    public void setParentTitle (String ParentTitle)
    {
        this.ParentTitle = ParentTitle;
    }

    public String getRenderingFormat ()
    {
        return RenderingFormat;
    }

    public void setRenderingFormat (String RenderingFormat)
    {
        this.RenderingFormat = RenderingFormat;
    }

    public String getParentSlug ()
    {
        return ParentSlug;
    }

    public void setParentSlug (String ParentSlug)
    {
        this.ParentSlug = ParentSlug;
    }

    public String getWord_Hi ()
    {
        return Word_Hi;
    }

    public void setWord_Hi (String Word_Hi)
    {
        this.Word_Hi = Word_Hi;
    }

    public String getWord_Ur ()
    {
        return Word_Ur;
    }

    public void setWord_Ur (String Word_Ur)
    {
        this.Word_Ur = Word_Ur;
    }

    public String getPoetSlug ()
    {
        return PoetSlug;
    }

    public void setPoetSlug (String PoetSlug)
    {
        this.PoetSlug = PoetSlug;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PoetName = "+PoetName+", Meaning = "+Meaning+", Word_En = "+Word_En+", ParentTitle = "+ParentTitle+", RenderingFormat = "+RenderingFormat+", ParentSlug = "+ParentSlug+", Word_Hi = "+Word_Hi+", Word_Ur = "+Word_Ur+", PoetSlug = "+PoetSlug+", Title = "+Title+"]";
    }
}
