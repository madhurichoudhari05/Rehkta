package org.Rekhta.model.homeScreenModel;

import com.google.gson.JsonArray;

import java.util.List;

/**
 * Created by Admin on 12/6/2017.
 */

public class TodaysTop {

    private String PoetName;

    private String Translation;

    private String ParentSlug;

    private String ContentId;

    private String PoetDPSlug;

    private String Slug;

    private String Title;

    private List<TagsModel> Tags;

    private String TranslatorName;

    private String RenderingFormat;

    private String ShortUrlIndex;

    private String Alignment;

    private String TypeSlug;

    private String ParentTypeSlug;

    private String PoetSlug;

    public String getPoetName ()
    {
        return PoetName;
    }

    public void setPoetName (String PoetName)
    {
        this.PoetName = PoetName;
    }

    public String getTranslation ()
    {
        return Translation;
    }

    public void setTranslation (String Translation)
    {
        this.Translation = Translation;
    }

    public String getParentSlug ()
    {
        return ParentSlug;
    }

    public void setParentSlug (String ParentSlug)
    {
        this.ParentSlug = ParentSlug;
    }

    public String getContentId ()
    {
        return ContentId;
    }

    public void setContentId (String ContentId)
    {
        this.ContentId = ContentId;
    }

    public String getPoetDPSlug ()
    {
        return PoetDPSlug;
    }

    public void setPoetDPSlug (String PoetDPSlug)
    {
        this.PoetDPSlug = PoetDPSlug;
    }

    public String getSlug ()
    {
        return Slug;
    }

    public void setSlug (String Slug)
    {
        this.Slug = Slug;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }

    public List<TagsModel>  getTags ()
    {
        return Tags;
    }

    public void setTags (List<TagsModel> Tags)
    {
        this.Tags = Tags;
    }

    public String getTranslatorName ()
    {
        return TranslatorName;
    }

    public void setTranslatorName (String TranslatorName)
    {
        this.TranslatorName = TranslatorName;
    }

    public String getRenderingFormat ()
    {
        return RenderingFormat;
    }

    public void setRenderingFormat (String RenderingFormat)
    {
        this.RenderingFormat = RenderingFormat;
    }

    public String getShortUrlIndex ()
    {
        return ShortUrlIndex;
    }

    public void setShortUrlIndex (String ShortUrlIndex)
    {
        this.ShortUrlIndex = ShortUrlIndex;
    }

    public String getAlignment ()
    {
        return Alignment;
    }

    public void setAlignment (String Alignment)
    {
        this.Alignment = Alignment;
    }

    public String getTypeSlug ()
    {
        return TypeSlug;
    }

    public void setTypeSlug (String TypeSlug)
    {
        this.TypeSlug = TypeSlug;
    }

    public String getParentTypeSlug ()
    {
        return ParentTypeSlug;
    }

    public void setParentTypeSlug (String ParentTypeSlug)
    {
        this.ParentTypeSlug = ParentTypeSlug;
    }

    public String getPoetSlug ()
    {
        return PoetSlug;
    }

    public void setPoetSlug (String PoetSlug)
    {
        this.PoetSlug = PoetSlug;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PoetName = "+PoetName+", Translation = "+Translation+", ParentSlug = "+ParentSlug+", ContentId = "+ContentId+", PoetDPSlug = "+PoetDPSlug+", Slug = "+Slug+", Title = "+Title+", Tags = "+Tags+", TranslatorName = "+TranslatorName+", RenderingFormat = "+RenderingFormat+", ShortUrlIndex = "+ShortUrlIndex+", Alignment = "+Alignment+", TypeSlug = "+TypeSlug+", ParentTypeSlug = "+ParentTypeSlug+", PoetSlug = "+PoetSlug+"]";
    }
}
