package org.Rekhta.model.homeScreenModel;

/**
 * Created by Admin on 12/6/2017.
 */

public class HomeCards {

    private String Name;

    private String CardId;

    private String Link;

    private String TextContent;

    private String TextHeading;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getCardId ()
    {
        return CardId;
    }

    public void setCardId (String CardId)
    {
        this.CardId = CardId;
    }

    public String getLink ()
    {
        return Link;
    }

    public void setLink (String Link)
    {
        this.Link = Link;
    }

    public String getTextContent ()
    {
        return TextContent;
    }

    public void setTextContent (String TextContent)
    {
        this.TextContent = TextContent;
    }

    public String getTextHeading ()
    {
        return TextHeading;
    }

    public void setTextHeading (String TextHeading)
    {
        this.TextHeading = TextHeading;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", CardId = "+CardId+", Link = "+Link+", TextContent = "+TextContent+", TextHeading = "+TextHeading+"]";
    }
}
