package org.Rekhta.model.homeScreenModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12/6/2017.
 */

public class T20Series implements Parcelable{

    @Expose
    @SerializedName("Description_En")
    private String Description_En;

    @Expose
    @SerializedName("LaunchDate")
    private String LaunchDate;

    @Expose
    @SerializedName("SEO_Slug")
    private String SEO_Slug;

    @Expose
    @SerializedName("MediumImageId")
    private String MediumImageId;

    @Expose
    @SerializedName("PoetId")
    private String PoetId;

    @Expose
    @SerializedName("Name_Hi")
    private String Name_Hi;

    @Expose
    @SerializedName("Description_Hi")
    private String Description_Hi;

    @Expose
    @SerializedName("Description_Ur")
    private String Description_Ur;

    @Expose
    @SerializedName("Title_Ur")
    private String Title_Ur;

    @Expose
    @SerializedName("Title_Hi")
    private String Title_Hi;

    @Expose
    @SerializedName("IsPoetBased")
    private String IsPoetBased;



    @Expose
    @SerializedName("ListId")
    private String ListId;

    @Expose
    @SerializedName("Id")
    private String Id;

    @Expose
    @SerializedName("Title_En")
    private String Title_En;

    @Expose
    @SerializedName("Name_En")
    private String Name_En;

    @Expose
    @SerializedName("Name_Ur")
    private String Name_Ur;

    protected T20Series(Parcel in) {
        Description_En = in.readString();
        LaunchDate = in.readString();
        SEO_Slug = in.readString();
        MediumImageId = in.readString();
        PoetId = in.readString();
        Name_Hi = in.readString();
        Description_Hi = in.readString();
        Description_Ur = in.readString();
        Title_Ur = in.readString();
        Title_Hi = in.readString();
        IsPoetBased = in.readString();
        ListId = in.readString();
        Id = in.readString();
        Title_En = in.readString();
        Name_En = in.readString();
        Name_Ur = in.readString();
    }

    public static final Creator<T20Series> CREATOR = new Creator<T20Series>() {
        @Override
        public T20Series createFromParcel(Parcel in) {
            return new T20Series(in);
        }

        @Override
        public T20Series[] newArray(int size) {
            return new T20Series[size];
        }
    };

    public String getDescription_En ()
    {
        return Description_En;
    }

    public void setDescription_En (String Description_En)
    {
        this.Description_En = Description_En;
    }

    public String getLaunchDate ()
    {
        return LaunchDate;
    }

    public void setLaunchDate (String LaunchDate)
    {
        this.LaunchDate = LaunchDate;
    }

    public String getSEO_Slug ()
    {
        return SEO_Slug;
    }

    public void setSEO_Slug (String SEO_Slug)
    {
        this.SEO_Slug = SEO_Slug;
    }

    public String getMediumImageId ()
    {
        return MediumImageId;
    }

    public void setMediumImageId (String MediumImageId)
    {
        this.MediumImageId = MediumImageId;
    }

    public String getPoetId ()
    {
        return PoetId;
    }

    public void setPoetId (String PoetId)
    {
        this.PoetId = PoetId;
    }

    public String getName_Hi ()
    {
        return Name_Hi;
    }

    public void setName_Hi (String Name_Hi)
    {
        this.Name_Hi = Name_Hi;
    }

    public String getDescription_Hi ()
    {
        return Description_Hi;
    }

    public void setDescription_Hi (String Description_Hi)
    {
        this.Description_Hi = Description_Hi;
    }

    public String getDescription_Ur ()
    {
        return Description_Ur;
    }

    public void setDescription_Ur (String Description_Ur)
    {
        this.Description_Ur = Description_Ur;
    }

    public String getTitle_Ur ()
    {
        return Title_Ur;
    }

    public void setTitle_Ur (String Title_Ur)
    {
        this.Title_Ur = Title_Ur;
    }

    public String getTitle_Hi ()
    {
        return Title_Hi;
    }

    public void setTitle_Hi (String Title_Hi)
    {
        this.Title_Hi = Title_Hi;
    }

    public String getIsPoetBased ()
    {
        return IsPoetBased;
    }

    public void setIsPoetBased (String IsPoetBased)
    {
        this.IsPoetBased = IsPoetBased;
    }

    public String getListId ()
    {
        return ListId;
    }

    public void setListId (String ListId)
    {
        this.ListId = ListId;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }

    public String getTitle_En ()
    {
        return Title_En;
    }

    public void setTitle_En (String Title_En)
    {
        this.Title_En = Title_En;
    }

    public String getName_En ()
    {
        return Name_En;
    }

    public void setName_En (String Name_En)
    {
        this.Name_En = Name_En;
    }

    public String getName_Ur ()
    {
        return Name_Ur;
    }

    public void setName_Ur (String Name_Ur)
    {
        this.Name_Ur = Name_Ur;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Description_En = "+Description_En+", LaunchDate = "+LaunchDate+", SEO_Slug = "+SEO_Slug+", MediumImageId = "+MediumImageId+", PoetId = "+PoetId+", Name_Hi = "+Name_Hi+", Description_Hi = "+Description_Hi+", Description_Ur = "+Description_Ur+", Title_Ur = "+Title_Ur+", Title_Hi = "+Title_Hi+", IsPoetBased = "+IsPoetBased+", ListId = "+ListId+", Id = "+Id+", Title_En = "+Title_En+", Name_En = "+Name_En+", Name_Ur = "+Name_Ur+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Description_En);
        dest.writeString(LaunchDate);
        dest.writeString(SEO_Slug);
        dest.writeString(MediumImageId);
        dest.writeString(PoetId);
        dest.writeString(Name_Hi);
        dest.writeString(Description_Hi);
        dest.writeString(Description_Ur);
        dest.writeString(Title_Ur);
        dest.writeString(Title_Hi);
        dest.writeString(IsPoetBased);
        dest.writeString(ListId);
        dest.writeString(Id);
        dest.writeString(Title_En);
        dest.writeString(Name_En);
        dest.writeString(Name_Ur);
    }
}
