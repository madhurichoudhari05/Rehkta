package org.Rekhta.model.poets;


public class PoetsDataModel {


    //new
    private String poetID; //I
    private String poetNameInEng;//NE
    private String poetNameInHin; //NH
    private String poetNameInUrdu;//NU
    private int totalGhazalCount;//GC
    private int totalNazmCount;//NC
    private int totalSherCount;//SC
    private String poetDescInEng; //DE
    private String poetDescInHin; //DH
    private String poetDescInUrdu; //DU
    private boolean isPoetNew; //N
    private String poetImgUrl; //N


    public PoetsDataModel(String poetID, String poetNameInEng, String poetNameInHin, String poetNameInUrdu,
                          int totalGhazalCount, int totalNazmCount, int totalSherCount, String poetDescInEng,
                          String poetDescInHin, String poetDescInUrdu, boolean isPoetNew, String poetImgUrl) {

        this.poetID = poetID;
        this.poetNameInEng = poetNameInEng;
        this.poetNameInHin = poetNameInHin;
        this.poetNameInUrdu = poetNameInUrdu;
        this.totalGhazalCount = totalGhazalCount;
        this.totalNazmCount = totalNazmCount;
        this.totalSherCount = totalSherCount;
        this.poetDescInEng = poetDescInEng;
        this.poetDescInHin = poetDescInHin;
        this.poetDescInUrdu = poetDescInUrdu;
        this.isPoetNew = isPoetNew;
        this.poetImgUrl = poetImgUrl;
    }

    public String getPoetID() {
        return poetID;
    }

    public void setPoetID(String poetID) {
        this.poetID = poetID;
    }

    public String getPoetNameInEng() {
        return poetNameInEng;
    }

    public void setPoetNameInEng(String poetNameInEng) {
        this.poetNameInEng = poetNameInEng;
    }

    public String getPoetNameInHin() {
        return poetNameInHin;
    }

    public void setPoetNameInHin(String poetNameInHin) {
        this.poetNameInHin = poetNameInHin;
    }

    public String getPoetNameInUrdu() {
        return poetNameInUrdu;
    }

    public void setPoetNameInUrdu(String poetNameInUrdu) {
        this.poetNameInUrdu = poetNameInUrdu;
    }

    public int getTotalGhazalCount() {
        return totalGhazalCount;
    }

    public void setTotalGhazalCount(int totalGhazalCount) {
        this.totalGhazalCount = totalGhazalCount;
    }

    public int getTotalNazmCount() {
        return totalNazmCount;
    }

    public void setTotalNazmCount(int totalNazmCount) {
        this.totalNazmCount = totalNazmCount;
    }

    public int getTotalSherCount() {
        return totalSherCount;
    }

    public void setTotalSherCount(int totalSherCount) {
        this.totalSherCount = totalSherCount;
    }

    public String getPoetDescInEng() {
        return poetDescInEng;
    }

    public void setPoetDescInEng(String poetDescInEng) {
        this.poetDescInEng = poetDescInEng;
    }

    public String getPoetDescInHin() {
        return poetDescInHin;
    }

    public void setPoetDescInHin(String poetDescInHin) {
        this.poetDescInHin = poetDescInHin;
    }

    public String getPoetDescInUrdu() {
        return poetDescInUrdu;
    }

    public void setPoetDescInUrdu(String poetDescInUrdu) {
        this.poetDescInUrdu = poetDescInUrdu;
    }

    public boolean isPoetNew() {
        return isPoetNew;
    }

    public void setPoetNew(boolean poetNew) {
        isPoetNew = poetNew;
    }

    public String getPoetImgUrl() {
        return poetImgUrl;
    }

    public void setPoetImgUrl(String poetImgUrl) {
        this.poetImgUrl = poetImgUrl;
    }


}
