package org.Rekhta.model;

/**
 * Created by SANCHIT on 9/12/2017.
 */

public class GhazalListModel {

    private String ghazalTitle;
    private String ghazalAuthorName;

    GhazalListModel(String text1, String text2){
        ghazalTitle = text1;
        ghazalAuthorName = text2;
    }

    public String getmText1() {
        return ghazalTitle;
    }

    public void setmText1(String mText1) {
        this.ghazalTitle = mText1;
    }

    public String getmText2() {
        return ghazalAuthorName;
    }

    public void setmText2(String mText2) {
        this.ghazalAuthorName = mText2;
    }
}
