package org.Rekhta.model.historyModels;

/**
 * Created by Admin on 12/20/2017.
 */

public class HistoryRPojo {

    private String SS;

    private String NH;

    private String TE;

    private String SI;

    private String TH;

    private String NU;

    private String TS;

    private String TU;

    private String I;

    private String NE;


    public String getSS() {
        return SS;
    }

    public void setSS(String SS) {
        this.SS = SS;
    }

    public String getNH() {
        return NH;
    }

    public void setNH(String NH) {
        this.NH = NH;
    }

    public String getTE() {
        return TE;
    }

    public void setTE(String TE) {
        this.TE = TE;
    }

    public String getSI() {
        return SI;
    }

    public void setSI(String SI) {
        this.SI = SI;
    }

    public String getTH() {
        return TH;
    }

    public void setTH(String TH) {
        this.TH = TH;
    }

    public String getNU() {
        return NU;
    }

    public void setNU(String NU) {
        this.NU = NU;
    }

    public String getTS() {
        return TS;
    }

    public void setTS(String TS) {
        this.TS = TS;
    }

    public String getTU() {
        return TU;
    }

    public void setTU(String TU) {
        this.TU = TU;
    }

    public String getI() {
        return I;
    }

    public void setI(String I) {
        this.I = I;
    }

    public String getNE() {
        return NE;
    }

    public void setNE(String NE) {
        this.NE = NE;
    }

    @Override
    public String toString() {
        return "ClassPojo [SS = " + SS + ", NH = " + NH + ", TE = " + TE + ", SI = " + SI + ", TH = " + TH + ", NU = " + NU + ", TS = " + TS + ", TU = " + TU + ", I = " + I + ", NE = " + NE + "]";
    }
}
