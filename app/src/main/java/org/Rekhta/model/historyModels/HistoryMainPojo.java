package org.Rekhta.model.historyModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class HistoryMainPojo {

    @Expose
    @SerializedName("Me")
    private String Me;

    @Expose
    @SerializedName("T")
    private String T;

    @Expose
    @SerializedName("Mh")
    private String Mh;

    @Expose
    @SerializedName("S")
    private String S;

    @Expose
    @SerializedName("R")
    private List<HistoryRPojo> historyRPojos;

    private String Mu;

    public String getMe() {
        return Me;
    }

    public void setMe(String Me) {
        this.Me = Me;
    }

    public String getT() {
        return T;
    }

    public void setT(String T) {
        this.T = T;
    }

    public String getMh() {
        return Mh;
    }

    public void setMh(String Mh) {
        this.Mh = Mh;
    }

    public String getS() {
        return S;
    }

    public void setS(String S) {
        this.S = S;
    }

    public List<HistoryRPojo> getR() {
        return historyRPojos;
    }

    public void setR(List<HistoryRPojo> HistoryRPojo) {
        this.historyRPojos = HistoryRPojo;
    }

    public String getMu() {
        return Mu;
    }

    public void setMu(String Mu) {
        this.Mu = Mu;
    }

    @Override
    public String toString() {
        return "ClassPojo [Me = " + Me + ", T = " + T + ", Mh = " + Mh + ", S = " + S + ", R = " + historyRPojos + ", Mu = " + Mu + "]";
    }
}
