package org.Rekhta.model.myFavorite;

import java.util.List;

/**
 * Created by Admin on 12/11/2017.
 */

public class FavRPojo {

    private List<FavFCModel> FC;

    private String TC;

    public List<FavFCModel> getFC ()
    {
        return FC;
    }

    public void setFC (List<FavFCModel> FC)
    {
        this.FC = FC;
    }

    public String getTC ()
    {
        return TC;
    }

    public void setTC (String TC)
    {
        this.TC = TC;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [FC = "+FC+", TC = "+TC+"]";
    }
}
