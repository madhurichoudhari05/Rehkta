package org.Rekhta.model.myFavorite;

/**
 * Created by Admin on 12/11/2017.
 */

public class OnlineFavModelMain {

    private String Me;

    private String T;

    private String Mh;

    private String S;

    private FavRPojo R;

    private String Mu;

    public String getMe ()
    {
        return Me;
    }

    public void setMe (String Me)
    {
        this.Me = Me;
    }

    public String getT ()
    {
        return T;
    }

    public void setT (String T)
    {
        this.T = T;
    }

    public String getMh ()
    {
        return Mh;
    }

    public void setMh (String Mh)
    {
        this.Mh = Mh;
    }

    public String getS ()
    {
        return S;
    }

    public void setS (String S)
    {
        this.S = S;
    }

    public FavRPojo getR ()
    {
        return R;
    }

    public void setR (FavRPojo R)
    {
        this.R = R;
    }

    public String getMu ()
    {
        return Mu;
    }

    public void setMu (String Mu)
    {
        this.Mu = Mu;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Me = "+Me+", T = "+T+", Mh = "+Mh+", S = "+S+", R = "+R+", Mu = "+Mu+"]";
    }
}
