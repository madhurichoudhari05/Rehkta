package org.Rekhta.model;

import org.json.JSONArray;

/**
 * Created by Admin on 9/28/2017.
 */

public class SherCollection {

    private String shertextInENG;
    private String shertextInHIN;
    private String shertextInURDU;
    private String poetNameInEng;
    private String poetNameInHIN;
    private String poetNameInURDU;
    private String shercontentId;
    private String shareUrlEng;
    private String shareUrlHIN;
    private String shareUrlURDU;
    private JSONArray sherTagArray;
    private int tagArrayLength;
    private String ghazalId;
    String poetId;

    public String getPoetId() {
        return poetId;
    }

    public void setPoetId(String poetId) {
        this.poetId = poetId;
    }

    public SherCollection(String shertextInENG, String shertextInHIN, String shertextInURDU, String poetNameInEng,
                          String poetNameInHIN, String poetNameInURDU, String shercontentId, String shareUrlEng,
                          String shareUrlHIN, String shareUrlURDU, JSONArray sherTagArray, String ghazalId, String poetId) {
        this.shertextInENG = shertextInENG;
        this.shertextInHIN = shertextInHIN;
        this.shertextInURDU = shertextInURDU;
        this.poetNameInEng = poetNameInEng;
        this.poetNameInHIN = poetNameInHIN;
        this.poetNameInURDU = poetNameInURDU;
        this.shercontentId = shercontentId;
        this.shareUrlEng = shareUrlEng;
        this.shareUrlHIN = shareUrlHIN;
        this.shareUrlURDU = shareUrlURDU;
        this.sherTagArray = sherTagArray;
        this.tagArrayLength = sherTagArray.length();
        this.ghazalId = ghazalId;
        this.poetId = poetId;

    }

    public String getShertextInENG() {
        return shertextInENG;
    }

    public void setShertextInENG(String shertextInENG) {
        this.shertextInENG = shertextInENG;
    }

    public String getShertextInHIN() {
        return shertextInHIN;
    }

    public void setShertextInHIN(String shertextInHIN) {
        this.shertextInHIN = shertextInHIN;
    }

    public String getShertextInURDU() {
        return shertextInURDU;
    }

    public void setShertextInURDU(String shertextInURDU) {
        this.shertextInURDU = shertextInURDU;
    }

    public String getPoetNameInEng() {
        return poetNameInEng;
    }

    public void setPoetNameInEng(String poetNameInEng) {
        this.poetNameInEng = poetNameInEng;
    }

    public String getPoetNameInHIN() {
        return poetNameInHIN;
    }

    public void setPoetNameInHIN(String poetNameInHIN) {
        this.poetNameInHIN = poetNameInHIN;
    }

    public String getPoetNameInURDU() {
        return poetNameInURDU;
    }

    public void setPoetNameInURDU(String poetNameInURDU) {
        this.poetNameInURDU = poetNameInURDU;
    }

    public String getShercontentId() {
        return shercontentId;
    }

    public void setShercontentId(String shercontentId) {
        this.shercontentId = shercontentId;
    }

    public String getShareUrlEng() {
        return shareUrlEng;
    }

    public void setShareUrlEng(String shareUrlEng) {
        this.shareUrlEng = shareUrlEng;
    }

    public String getShareUrlHIN() {
        return shareUrlHIN;
    }

    public void setShareUrlHIN(String shareUrlHIN) {
        this.shareUrlHIN = shareUrlHIN;
    }

    public String getShareUrlURDU() {
        return shareUrlURDU;
    }

    public void setShareUrlURDU(String shareUrlURDU) {
        this.shareUrlURDU = shareUrlURDU;
    }

    public JSONArray getSherTagArray() {
        return sherTagArray;
    }

    public void setSherTagArray(JSONArray sherTagArray) {
        this.sherTagArray = sherTagArray;
    }

    public int getTagArrayLength() {
        return tagArrayLength;
    }

    public void setTagArrayLength(int tagArrayLength) {
        this.tagArrayLength = tagArrayLength;
    }

    public String getGhazalId() {
        return ghazalId;
    }

    public void setGhazalId(String ghazalId) {
        this.ghazalId = ghazalId;
    }


}
