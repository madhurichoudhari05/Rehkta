package org.Rekhta.model;

/**
 * Created by Admin on 9/27/2017.
 */

public class Tags {

    private String gazaltextInEnglish;
    private String gazaltextInHindi;
    private String gazaltextInUrdu;
    private String tagId;

    private String count;

    public Tags( String gazaltextInEnglish, String gazaltextInHindi, String gazaltextInUrdu,String count,String tagId) {
        this.gazaltextInEnglish = gazaltextInEnglish;
        this.gazaltextInHindi = gazaltextInHindi;
        this.gazaltextInUrdu = gazaltextInUrdu;
        this.tagId = tagId;
        this.count = count;
    }

    public String getGazaltextInEnglish() {
        return gazaltextInEnglish;
    }

    public void setGazaltextInEnglish(String gazaltextInEnglish) {
        this.gazaltextInEnglish = gazaltextInEnglish;
    }

    public String getGazaltextInHindi() {
        return gazaltextInHindi;
    }

    public void setGazaltextInHindi(String gazaltextInHindi) {
        this.gazaltextInHindi = gazaltextInHindi;
    }

    public String getGazaltextInUrdu() {
        return gazaltextInUrdu;
    }
    public void setGazaltextInUrdu(String gazaltextInUrdu) {
        this.gazaltextInUrdu = gazaltextInUrdu;
    }



    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

}
