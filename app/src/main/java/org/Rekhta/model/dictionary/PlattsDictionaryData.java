package org.Rekhta.model.dictionary;


public class PlattsDictionaryData {

    public String wordId;
    public String wordInUrdu;
    public String wordInHindi;
    public String  wordInEnglish;
    public String translatedEnTr;
    public String meaningHtml;
    public String wordOrigin;

    public PlattsDictionaryData(String wordId, String  wordInEnglish, String wordInHindi, String wordInUrdu, String translatedEnTr,
                                String meaningHtml, String wordOrigin ) {
        this.wordId = wordId;
        this.wordInEnglish = wordInEnglish;
        this.wordInHindi = wordInHindi;
        this.wordInUrdu = wordInUrdu;
        this.wordInUrdu = wordInUrdu;
        this.translatedEnTr = translatedEnTr;
        this.meaningHtml = meaningHtml;
        this.wordOrigin = wordOrigin;

    }

    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public String getWordInUrdu() {
        return wordInUrdu;
    }

    public void setWordInUrdu(String wordInUrdu) {
        this.wordInUrdu = wordInUrdu;
    }

    public String getWordInHindi() {
        return wordInHindi;
    }

    public void setWordInHindi(String wordInHindi) {
        this.wordInHindi = wordInHindi;
    }

    public String getWordInEnglish() {
        return wordInEnglish;
    }

    public void setWordInEnglish(String wordInEnglish) {
        this.wordInEnglish = wordInEnglish;
    }

    public String getTranslatedEnTr() {
        return translatedEnTr;
    }

    public void setTranslatedEnTr(String translatedEnTr) {
        this.translatedEnTr = translatedEnTr;
    }

    public String getMeaningHtml() {
        return meaningHtml;
    }

    public void setMeaningHtml(String meaningHtml) {
        this.meaningHtml = meaningHtml;
    }

    public String getWordOrigin() {
        return wordOrigin;
    }

    public void setWordOrigin(String wordOrigin) {
        this.wordOrigin = wordOrigin;
    }
}
