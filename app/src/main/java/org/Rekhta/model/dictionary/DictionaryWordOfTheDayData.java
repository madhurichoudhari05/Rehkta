package org.Rekhta.model.dictionary;

/**
 * Created by SANCHIT on 11/7/2017.
 */

public class DictionaryWordOfTheDayData {


    private String wordId;
    private String wordInEng;
    private String wordInHin;
    private String wordInUrdu;
    private String MeaningInEng;
    private String MeaningInHin;
    private String MeaningInUrdu;

    private String wordsPoetNameInEng;
    private String wordsPoetNameInHin;
    private String wordsPoetNameInUrdu;

    private String WordUsedinContentEng;
    private String WordUsedinContentHin;
    private String WordUsedinContentUrdu;

    public DictionaryWordOfTheDayData(String wordId,
                     String wordInEng, String wordInHin, String wordInUrdu,
                     String meaningInEng, String meaningInHin, String meaningInUrdu,
                     String wordsPoetNameInEng, String wordsPoetNameInHin, String wordsPoetNameInUrdu,
                     String wordUsedinContentEng, String wordUsedinContentHin, String wordUsedinContentUrdu) {
        this.wordId = wordId;
        this.wordInEng = wordInEng;
        this.wordInHin = wordInHin;
        this.wordInUrdu = wordInUrdu;
        MeaningInEng = meaningInEng;
        MeaningInHin = meaningInHin;
        MeaningInUrdu = meaningInUrdu;
        this.wordsPoetNameInEng = wordsPoetNameInEng;
        this.wordsPoetNameInHin = wordsPoetNameInHin;
        this.wordsPoetNameInUrdu = wordsPoetNameInUrdu;
        WordUsedinContentEng = wordUsedinContentEng;
        WordUsedinContentHin = wordUsedinContentHin;
        WordUsedinContentUrdu = wordUsedinContentUrdu;
    }


    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public String getWordInEng() {
        return wordInEng;
    }

    public void setWordInEng(String wordInEng) {
        this.wordInEng = wordInEng;
    }

    public String getWordInHin() {
        return wordInHin;
    }

    public void setWordInHin(String wordInHin) {
        this.wordInHin = wordInHin;
    }

    public String getWordInUrdu() {
        return wordInUrdu;
    }

    public void setWordInUrdu(String wordInUrdu) {
        this.wordInUrdu = wordInUrdu;
    }

    public String getMeaningInEng() {
        return MeaningInEng;
    }

    public void setMeaningInEng(String meaningInEng) {
        MeaningInEng = meaningInEng;
    }

    public String getMeaningInHin() {
        return MeaningInHin;
    }

    public void setMeaningInHin(String meaningInHin) {
        MeaningInHin = meaningInHin;
    }

    public String getMeaningInUrdu() {
        return MeaningInUrdu;
    }

    public void setMeaningInUrdu(String meaningInUrdu) {
        MeaningInUrdu = meaningInUrdu;
    }

    public String getWordsPoetNameInEng() {

        String poetNameUnderLinedInEng =  "<u>"+wordsPoetNameInEng+"</u>";
        return poetNameUnderLinedInEng;
    }

    public void setWordsPoetNameInEng(String wordsPoetNameInEng) {
        this.wordsPoetNameInEng = wordsPoetNameInEng;
    }

    public String getWordsPoetNameInHin() {
        String poetNameUnderLinedHin =  "<u>"+wordsPoetNameInHin+"</u>";
        return poetNameUnderLinedHin;
    }

    public void setWordsPoetNameInHin(String wordsPoetNameInHin) {
        this.wordsPoetNameInHin = wordsPoetNameInHin;
    }

    public String getWordsPoetNameInUrdu() {
        String poetNameUnderLinedUrdu =  "<u>"+wordsPoetNameInUrdu+"</u>";
        return poetNameUnderLinedUrdu;
    }

    public void setWordsPoetNameInUrdu(String wordsPoetNameInUrdu) {
        this.wordsPoetNameInUrdu = wordsPoetNameInUrdu;
    }

    public String getWordUsedinContentEng() {
        return WordUsedinContentEng;
    }

    public void setWordUsedinContentEng(String wordUsedinContentEng) {
        WordUsedinContentEng = wordUsedinContentEng;
    }

    public String getWordUsedinContentHin() {
        return WordUsedinContentHin;
    }

    public void setWordUsedinContentHin(String wordUsedinContentHin) {
        WordUsedinContentHin = wordUsedinContentHin;
    }

    public String getWordUsedinContentUrdu() {
        return WordUsedinContentUrdu;
    }

    public void setWordUsedinContentUrdu(String wordUsedinContentUrdu) {
        WordUsedinContentUrdu = wordUsedinContentUrdu;
    }
}
