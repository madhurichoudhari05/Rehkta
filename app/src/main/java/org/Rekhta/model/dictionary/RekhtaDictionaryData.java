package org.Rekhta.model.dictionary;


public class RekhtaDictionaryData {

    private String wordId;
    private String wordInEng;
    private String wordInHin;
    private String wordInUrdu;
    private String Meaning1inEng;
    private String Meaning2inEng;
    private String Meaning3inEng;
    private String Meaning1inHin;
    private String Meaning2inHin;
    private String Meaning3inHin;
    private String Meaning1inUrdu;
    private String Meaning2inUrdu;
    private String Meaning3inUrdu;


    public RekhtaDictionaryData(String wordId, String wordInEng, String wordInHin, String wordInUrdu, String meaning1inEng,
                                String meaning2inEng, String meaning3inEng, String meaning1inHin, String meaning2inHin,
                                String meaning3inHin, String meaning1inUrdu, String meaning2inUrdu, String meaning3inUrdu) {
        this.wordId = wordId;
        this.wordInEng = wordInEng;
        this.wordInHin = wordInHin;
        this.wordInUrdu = wordInUrdu;
        this.Meaning1inEng = meaning1inEng;
        this.Meaning2inEng = meaning2inEng;
        this.Meaning3inEng = meaning3inEng;
        this.Meaning1inHin = meaning1inHin;
        this.Meaning2inHin = meaning2inHin;
        this.Meaning3inHin = meaning3inHin;
        this.Meaning1inUrdu = meaning1inUrdu;
        this.Meaning2inUrdu = meaning2inUrdu;
        this.Meaning3inUrdu = meaning3inUrdu;
    }

    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public String getWordInEng() {
        return wordInEng;
    }

    public void setWordInEng(String wordInEng) {
        this.wordInEng = wordInEng;
    }

    public String getWordInHin() {
        return wordInHin;
    }

    public void setWordInHin(String wordInHin) {
        this.wordInHin = wordInHin;
    }

    public String getWordInUrdu() {
        return wordInUrdu;
    }

    public void setWordInUrdu(String wordInUrdu) {
        this.wordInUrdu = wordInUrdu;
    }

    public String getMeaning1inEng() {
        return Meaning1inEng;
    }

    public void setMeaning1inEng(String meaning1inEng) {
        Meaning1inEng = meaning1inEng;
    }

    public String getMeaning2inEng() {
        return Meaning2inEng;
    }

    public void setMeaning2inEng(String meaning2inEng) {
        Meaning2inEng = meaning2inEng;
    }

    public String getMeaning3inEng() {
        return Meaning3inEng;
    }

    public void setMeaning3inEng(String meaning3inEng) {
        Meaning3inEng = meaning3inEng;
    }

    public String getMeaning1inHin() {
        return Meaning1inHin;
    }

    public void setMeaning1inHin(String meaning1inHin) {
        Meaning1inHin = meaning1inHin;
    }

    public String getMeaning2inHin() {
        return Meaning2inHin;
    }

    public void setMeaning2inHin(String meaning2inHin) {
        Meaning2inHin = meaning2inHin;
    }

    public String getMeaning3inHin() {
        return Meaning3inHin;
    }

    public void setMeaning3inHin(String meaning3inHin) {
        Meaning3inHin = meaning3inHin;
    }

    public String getMeaning1inUrdu() {
        return Meaning1inUrdu;
    }

    public void setMeaning1inUrdu(String meaning1inUrdu) {
        Meaning1inUrdu = meaning1inUrdu;
    }

    public String getMeaning2inUrdu() {
        return Meaning2inUrdu;
    }

    public void setMeaning2inUrdu(String meaning2inUrdu) {
        Meaning2inUrdu = meaning2inUrdu;
    }

    public String getMeaning3inUrdu() {
        return Meaning3inUrdu;
    }

    public void setMeaning3inUrdu(String meaning3inUrdu) {
        Meaning3inUrdu = meaning3inUrdu;
    }

}
