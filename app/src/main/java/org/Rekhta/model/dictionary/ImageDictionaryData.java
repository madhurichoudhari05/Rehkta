package org.Rekhta.model.dictionary;


import org.json.JSONArray;

public class ImageDictionaryData {

    private String EnglishWord;
    private String HindiWord;
    private String UrduWord;
    private JSONArray ImgDictionaryEntries;

    public ImageDictionaryData(String englishWord, String hindiWord, String urduWord, JSONArray imgDictionaryEntries) {
        EnglishWord = englishWord;
        HindiWord = hindiWord;
        UrduWord = urduWord;
        ImgDictionaryEntries = imgDictionaryEntries;
    }

    public String getEnglishWord() {
        return EnglishWord;
    }

    public void setEnglishWord(String englishWord) {
        EnglishWord = englishWord;
    }

    public String getHindiWord() {
        return HindiWord;
    }

    public void setHindiWord(String hindiWord) {
        HindiWord = hindiWord;
    }

    public String getUrduWord() {
        return UrduWord;
    }

    public void setUrduWord(String urduWord) {
        UrduWord = urduWord;
    }

    public JSONArray getImgDictionaryEntries() {
        return ImgDictionaryEntries;
    }

    public void setImgDictionaryEntries(JSONArray imgDictionaryEntries) {
        ImgDictionaryEntries = imgDictionaryEntries;
    }
}
