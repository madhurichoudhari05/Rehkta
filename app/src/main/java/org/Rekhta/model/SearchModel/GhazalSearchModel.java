package org.Rekhta.model.SearchModel;

/**
 * Created by SANCHIT on 11/16/2017.
 */

public class GhazalSearchModel {
    /*
      "Body": "hauz me.n gir pa.Daa gulaab kaa phuul<br/>paas laane se duur jaataa hai",
              "Id": "b0b4af7f-e3ca-408e-9d09-92df06de2750",
              "TypeId": "a13c352e-03d3-43fe-ace0-f64918f6d28a",
              "ContentSlug": "hauz-men-gir-padaa-gulaab-kaa-phuul-ehsan-danish-qita",
              "PoetId": "9dd7decd-df6d-414d-a43b-1fabefed70d5",
              "PoetSlug": "ehsan-danish",
              "PoetName": "Ehsan Danish",
              "ImageUrl": "https://www.rekhta.org/Images/Shayar/ehsan-danish.png",
              "ContentUrl": "https://www.rekhta.org/qita/hauz-men-gir-padaa-gulaab-kaa-phuul-ehsan-danish-qita",
              "AudioCount": 0,
              "VideoCount": 0,
              "EditorChoice": false,
              "PopularChoice": false,
              "ShortUrlIndex": 0,
              "Title": "hauz mein gir paDa gulab ka phul"*/

    private String ghazalBody;
    private String ghazalId;
    private String ghazalTypeId;
    private String ghazalContentSlug;
    private String ghazalPoetId;
    private String ghazalPoetSlug;
    private String ghazalPoetName;
    private String ghazalImageUrl;
    private String ghazalContentUrl;
    private int    ghazalAudioCount;
    private int    ghazalVideoCount;
    private boolean ghazalEditorChoice;
    private boolean ghazalPopularChoice;
    private int     ghazalShortUrlIndex;
    private String  ghazalTitle;

    public GhazalSearchModel(String ghazalBody, String ghazalId, String ghazalTypeId, String ghazalContentSlug,
                             String ghazalPoetId, String ghazalPoetSlug, String ghazalPoetName, String ghazalImageUrl,
                             String ghazalContentUrl, int ghazalAudioCount, int ghazalVideoCount, boolean ghazalEditorChoice,
                             boolean ghazalPopularChoice, int ghazalShortUrlIndex, String ghazalTitle) {
        this.ghazalBody = ghazalBody;
        this.ghazalId = ghazalId;
        this.ghazalTypeId = ghazalTypeId;
        this.ghazalContentSlug = ghazalContentSlug;
        this.ghazalPoetId = ghazalPoetId;
        this.ghazalPoetSlug = ghazalPoetSlug;
        this.ghazalPoetName = ghazalPoetName;
        this.ghazalImageUrl = ghazalImageUrl;
        this.ghazalContentUrl = ghazalContentUrl;
        this.ghazalAudioCount = ghazalAudioCount;
        this.ghazalVideoCount = ghazalVideoCount;
        this.ghazalEditorChoice = ghazalEditorChoice;
        this.ghazalPopularChoice = ghazalPopularChoice;
        this.ghazalShortUrlIndex = ghazalShortUrlIndex;
        this.ghazalTitle = ghazalTitle;
    }

    public String getGhazalBody() {
        return ghazalBody;
    }

    public void setGhazalBody(String ghazalBody) {
        this.ghazalBody = ghazalBody;
    }

    public String getGhazalId() {
        return ghazalId;
    }

    public void setGhazalId(String ghazalId) {
        this.ghazalId = ghazalId;
    }

    public String getGhazalTypeId() {
        return ghazalTypeId;
    }

    public void setGhazalTypeId(String ghazalTypeId) {
        this.ghazalTypeId = ghazalTypeId;
    }

    public String getGhazalContentSlug() {
        return ghazalContentSlug;
    }

    public void setGhazalContentSlug(String ghazalContentSlug) {
        this.ghazalContentSlug = ghazalContentSlug;
    }

    public String getGhazalPoetId() {
        return ghazalPoetId;
    }

    public void setGhazalPoetId(String ghazalPoetId) {
        this.ghazalPoetId = ghazalPoetId;
    }

    public String getGhazalPoetSlug() {
        return ghazalPoetSlug;
    }

    public void setGhazalPoetSlug(String ghazalPoetSlug) {
        this.ghazalPoetSlug = ghazalPoetSlug;
    }

    public String getGhazalPoetName() {
        return ghazalPoetName;
    }

    public void setGhazalPoetName(String ghazalPoetName) {
        this.ghazalPoetName = ghazalPoetName;
    }

    public String getGhazalImageUrl() {
        return ghazalImageUrl;
    }

    public void setGhazalImageUrl(String ghazalImageUrl) {
        this.ghazalImageUrl = ghazalImageUrl;
    }

    public String getGhazalContentUrl() {
        return ghazalContentUrl;
    }

    public void setGhazalContentUrl(String ghazalContentUrl) {
        this.ghazalContentUrl = ghazalContentUrl;
    }

    public int getGhazalAudioCount() {
        return ghazalAudioCount;
    }

    public void setGhazalAudioCount(int ghazalAudioCount) {
        this.ghazalAudioCount = ghazalAudioCount;
    }

    public int getGhazalVideoCount() {
        return ghazalVideoCount;
    }

    public void setGhazalVideoCount(int ghazalVideoCount) {
        this.ghazalVideoCount = ghazalVideoCount;
    }

    public boolean isGhazalEditorChoice() {
        return ghazalEditorChoice;
    }

    public void setGhazalEditorChoice(boolean ghazalEditorChoice) {
        this.ghazalEditorChoice = ghazalEditorChoice;
    }

    public boolean isGhazalPopularChoice() {
        return ghazalPopularChoice;
    }

    public void setGhazalPopularChoice(boolean ghazalPopularChoice) {
        this.ghazalPopularChoice = ghazalPopularChoice;
    }

    public int getGhazalShortUrlIndex() {
        return ghazalShortUrlIndex;
    }

    public void setGhazalShortUrlIndex(int ghazalShortUrlIndex) {
        this.ghazalShortUrlIndex = ghazalShortUrlIndex;
    }

    public String getGhazalTitle() {
        return ghazalTitle;
    }

    public void setGhazalTitle(String ghazalTitle) {
        this.ghazalTitle = ghazalTitle;
    }
}
