package org.Rekhta.model.SearchModel;

/**
 * Created by Admin on 11/14/2017.
 */

public class NazmSearchModel {
/*   "Body": "sajii thii bazm-e-paziiraa.ii ek hotel me.n<br/>jahaa.n gulaab se chehre to the gulaab nahii.n",
           "Id": "2f317f3c-5a59-4570-b384-489713480ffb",
           "TypeId": "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b",
           "ContentSlug": "taqriib-e-ruu-numaaii-khalid-irfan-nazms",
           "PoetId": "a390fdd4-efe7-4a7a-8ac8-2e0b84a3ba67",
           "PoetSlug": "khalid-irfan",
           "PoetName": "Khalid Irfan",
           "ImageUrl": "https://www.rekhta.org/Images/Shayar/khalid-irfan.png",
           "ContentUrl": "https://www.rekhta.org/nazms/taqriib-e-ruu-numaaii-khalid-irfan-nazms",
           "AudioCount": 0,
           "VideoCount": 0,
           "EditorChoice": false,
           "PopularChoice": false,
           "ShortUrlIndex": 0,
           "Title": "taqrib-e-ru-numai"*/

    private String nazmBody;
    private String nazmId;
    private String nazmTypeId;
    private String nazmContentSlug;
    private String nazmPoetId;
    private String nazmPoetSlug;
    private String nazmPoetName;
    private String nazmImageUrl;
    private String nazmContentUrl;
    private int    nazmAudioCount;
    private int    nazmVideoCount;
    private boolean nazmEditorChoice;
    private boolean nazmPopularChoice;
    private int     nazmShortUrlIndex;
    private String  nazmTitle;

    public NazmSearchModel(String nazmBody, String nazmId, String nazmTypeId, String nazmContentSlug, String nazmPoetId, String nazmPoetSlug, String nazmPoetName, String nazmImageUrl, String nazmContentUrl, int nazmAudioCount, int nazmVideoCount, boolean nazmEditorChoice, boolean nazmPopularChoice, int nazmShortUrlIndex, String nazmTitle) {
        this.nazmBody = nazmBody;
        this.nazmId = nazmId;
        this.nazmTypeId = nazmTypeId;
        this.nazmContentSlug = nazmContentSlug;
        this.nazmPoetId = nazmPoetId;
        this.nazmPoetSlug = nazmPoetSlug;
        this.nazmPoetName = nazmPoetName;
        this.nazmImageUrl = nazmImageUrl;
        this.nazmContentUrl = nazmContentUrl;
        this.nazmAudioCount = nazmAudioCount;
        this.nazmVideoCount = nazmVideoCount;
        this.nazmEditorChoice = nazmEditorChoice;
        this.nazmPopularChoice = nazmPopularChoice;
        this.nazmShortUrlIndex = nazmShortUrlIndex;
        this.nazmTitle = nazmTitle;
    }

    public String getNazmBody() {
        return nazmBody;
    }

    public void setNazmBody(String nazmBody) {
        this.nazmBody = nazmBody;
    }

    public String getNazmId() {
        return nazmId;
    }

    public void setNazmId(String nazmId) {
        this.nazmId = nazmId;
    }

    public String getNazmTypeId() {
        return nazmTypeId;
    }

    public void setNazmTypeId(String nazmTypeId) {
        this.nazmTypeId = nazmTypeId;
    }

    public String getNazmContentSlug() {
        return nazmContentSlug;
    }

    public void setNazmContentSlug(String nazmContentSlug) {
        this.nazmContentSlug = nazmContentSlug;
    }

    public String getNazmPoetId() {
        return nazmPoetId;
    }

    public void setNazmPoetId(String nazmPoetId) {
        this.nazmPoetId = nazmPoetId;
    }

    public String getNazmPoetSlug() {
        return nazmPoetSlug;
    }

    public void setNazmPoetSlug(String nazmPoetSlug) {
        this.nazmPoetSlug = nazmPoetSlug;
    }

    public String getNazmPoetName() {
        return nazmPoetName;
    }

    public void setNazmPoetName(String nazmPoetName) {
        this.nazmPoetName = nazmPoetName;
    }

    public String getNazmImageUrl() {
        return nazmImageUrl;
    }

    public void setNazmImageUrl(String nazmImageUrl) {
        this.nazmImageUrl = nazmImageUrl;
    }

    public String getNazmContentUrl() {
        return nazmContentUrl;
    }

    public void setNazmContentUrl(String nazmContentUrl) {
        this.nazmContentUrl = nazmContentUrl;
    }

    public int getNazmAudioCount() {
        return nazmAudioCount;
    }

    public void setNazmAudioCount(int nazmAudioCount) {
        this.nazmAudioCount = nazmAudioCount;
    }

    public int getNazmVideoCount() {
        return nazmVideoCount;
    }

    public void setNazmVideoCount(int nazmVideoCount) {
        this.nazmVideoCount = nazmVideoCount;
    }

    public boolean isNazmEditorChoice() {
        return nazmEditorChoice;
    }

    public void setNazmEditorChoice(boolean nazmEditorChoice) {
        this.nazmEditorChoice = nazmEditorChoice;
    }

    public boolean isNazmPopularChoice() {
        return nazmPopularChoice;
    }

    public void setNazmPopularChoice(boolean nazmPopularChoice) {
        this.nazmPopularChoice = nazmPopularChoice;
    }

    public int getNazmShortUrlIndex() {
        return nazmShortUrlIndex;
    }

    public void setNazmShortUrlIndex(int nazmShortUrlIndex) {
        this.nazmShortUrlIndex = nazmShortUrlIndex;
    }

    public String getNazmTitle() {
        return nazmTitle;
    }

    public void setNazmTitle(String nazmTitle) {
        this.nazmTitle = nazmTitle;
    }
}
