package org.Rekhta.model.SearchModel;

/**
 * Created by Admin on 11/14/2017.
 */

public class DictionaryModel {
   /*
{
"Id":"47748b8e-e6e2-4021-a017-b7678ea86f88",
"Urdu":" گلوب ",
"Hindi":" ग्लोब ",
"English":" golob ",
"Meaning1_En":"Globe",
"Meaning2_En":"",
"Meaning3_En":"",
"Meaning1_Hi":"",
"Meaning2_Hi":"",
"Meaning3_Hi":"",
"Meaning1_Ur":"",
"Meaning2_Ur":"",
"Meaning3_Ur":""
}*/

   private String id;
   private String dictionaryWordInUrdu;
   private String dictionaryWordInHindi;
   private String dictionaryWordInEnglish;
   private String dictionaryMeaning1InEng;
   private String dictionaryMeaning2InEng;
   private String dictionaryMeaning3InEng;
   private String dictionaryMeaning1InHin;
   private String dictionaryMeaning2InHin;
   private String dictionaryMeaning3InHin;
   private String dictionaryMeaning1InUrdu;
   private String dictionaryMeaning2InUrdu;
   private String dictionaryMeaning3InUrdu;

    public DictionaryModel(String id, String dictionaryWordInUrdu, String dictionaryWordInHindi, String dictionaryWordInEnglish, String dictionaryMeaning1InEng, String dictionaryMeaning2InEng, String dictionaryMeaning3InEng, String dictionaryMeaning1InHin, String dictionaryMeaning2InHin, String dictionaryMeaning3InHin, String dictionaryMeaning1InUrdu, String dictionaryMeaning2InUrdu, String dictionaryMeaning3InUrdu) {
        this.id = id;
        this.dictionaryWordInUrdu = dictionaryWordInUrdu;
        this.dictionaryWordInHindi = dictionaryWordInHindi;
        this.dictionaryWordInEnglish = dictionaryWordInEnglish;
        this.dictionaryMeaning1InEng = dictionaryMeaning1InEng;
        this.dictionaryMeaning2InEng = dictionaryMeaning2InEng;
        this.dictionaryMeaning3InEng = dictionaryMeaning3InEng;
        this.dictionaryMeaning1InHin = dictionaryMeaning1InHin;
        this.dictionaryMeaning2InHin = dictionaryMeaning2InHin;
        this.dictionaryMeaning3InHin = dictionaryMeaning3InHin;
        this.dictionaryMeaning1InUrdu = dictionaryMeaning1InUrdu;
        this.dictionaryMeaning2InUrdu = dictionaryMeaning2InUrdu;
        this.dictionaryMeaning3InUrdu = dictionaryMeaning3InUrdu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDictionaryWordInUrdu() {
        return dictionaryWordInUrdu;
    }

    public void setDictionaryWordInUrdu(String dictionaryWordInUrdu) {
        this.dictionaryWordInUrdu = dictionaryWordInUrdu;
    }

    public String getDictionaryWordInHindi() {
        return dictionaryWordInHindi;
    }

    public void setDictionaryWordInHindi(String dictionaryWordInHindi) {
        this.dictionaryWordInHindi = dictionaryWordInHindi;
    }

    public String getDictionaryWordInEnglish() {
        return dictionaryWordInEnglish;
    }

    public void setDictionaryWordInEnglish(String dictionaryWordInEnglish) {
        this.dictionaryWordInEnglish = dictionaryWordInEnglish;
    }

    public String getDictionaryMeaning1InEng() {
        return dictionaryMeaning1InEng;
    }

    public void setDictionaryMeaning1InEng(String dictionaryMeaning1InEng) {
        this.dictionaryMeaning1InEng = dictionaryMeaning1InEng;
    }

    public String getDictionaryMeaning2InEng() {
        return dictionaryMeaning2InEng;
    }

    public void setDictionaryMeaning2InEng(String dictionaryMeaning2InEng) {
        this.dictionaryMeaning2InEng = dictionaryMeaning2InEng;
    }

    public String getDictionaryMeaning3InEng() {
        return dictionaryMeaning3InEng;
    }

    public void setDictionaryMeaning3InEng(String dictionaryMeaning3InEng) {
        this.dictionaryMeaning3InEng = dictionaryMeaning3InEng;
    }

    public String getDictionaryMeaning1InHin() {
        return dictionaryMeaning1InHin;
    }

    public void setDictionaryMeaning1InHin(String dictionaryMeaning1InHin) {
        this.dictionaryMeaning1InHin = dictionaryMeaning1InHin;
    }

    public String getDictionaryMeaning2InHin() {
        return dictionaryMeaning2InHin;
    }

    public void setDictionaryMeaning2InHin(String dictionaryMeaning2InHin) {
        this.dictionaryMeaning2InHin = dictionaryMeaning2InHin;
    }

    public String getDictionaryMeaning3InHin() {
        return dictionaryMeaning3InHin;
    }

    public void setDictionaryMeaning3InHin(String dictionaryMeaning3InHin) {
        this.dictionaryMeaning3InHin = dictionaryMeaning3InHin;
    }

    public String getDictionaryMeaning1InUrdu() {
        return dictionaryMeaning1InUrdu;
    }

    public void setDictionaryMeaning1InUrdu(String dictionaryMeaning1InUrdu) {
        this.dictionaryMeaning1InUrdu = dictionaryMeaning1InUrdu;
    }

    public String getDictionaryMeaning2InUrdu() {
        return dictionaryMeaning2InUrdu;
    }

    public void setDictionaryMeaning2InUrdu(String dictionaryMeaning2InUrdu) {
        this.dictionaryMeaning2InUrdu = dictionaryMeaning2InUrdu;
    }

    public String getDictionaryMeaning3InUrdu() {
        return dictionaryMeaning3InUrdu;
    }

    public void setDictionaryMeaning3InUrdu(String dictionaryMeaning3InUrdu) {
        this.dictionaryMeaning3InUrdu = dictionaryMeaning3InUrdu;
    }
}
