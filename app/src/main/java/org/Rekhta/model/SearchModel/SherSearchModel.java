package org.Rekhta.model.SearchModel;

/**
 * Created by Admin on 11/14/2017.
 */

public class SherSearchModel {
/*       "Body": "ye buzurgo.n kii ravaa-daarii ke pazh-murda gulaab<br/>aabiyaarii chaahte hai.n in me.n chi.ngaarii na rakh",
            "Id": "47c507ef-9056-49ef-8689-3b840e85d640",
            "TypeId": "f722d5dc-45da-41ec-a439-900df702a3d6",
            "ContentSlug": "ye-buzurgon-kii-ravaa-daarii-ke-pazh-murda-gulaab-hamid-mukhtar-hamid-couplets",
            "PoetId": "42ac2390-9e70-4cbc-aad0-1cc9fc78dfac",
            "PoetSlug": "hamid-mukhtar-hamid",
            "PoetName": "Hamid Mukhtar Hamid",
            "ImageUrl": "https://www.rekhta.org/Images/Shayar/hamid-mukhtar-hamid.png",
            "ContentUrl": "https://www.rekhta.org/couplets/ye-buzurgon-kii-ravaa-daarii-ke-pazh-murda-gulaab-hamid-mukhtar-hamid-couplets",
            "AudioCount": 0,
            "VideoCount": 0,
            "EditorChoice": false,
            "PopularChoice": false,
            "ShortUrlIndex": 0,
            "Title": null*/

    private String sherBody;
    private String sherId;
    private String sherTypeId;
    private String sherContentSlug;
    private String sherPoetId;
    private String sherPoetSlug;
    private String sherPoetName;
    private String sherImageUrl;
    private String sherContentUrl;
    private int    sherAudioCount;
    private int    sherVideoCount;
    private boolean sherEditorChoice;
    private boolean sherPopularChoice;
    private int     sherShortUrlIndex;
    private String  sherTitle;

    public SherSearchModel(String sherBody, String sherId, String sherTypeId, String sherContentSlug, String sherPoetId, String sherPoetSlug, String sherPoetName, String sherImageUrl, String sherContentUrl, int sherAudioCount, int sherVideoCount, boolean sherEditorChoice, boolean sherPopularChoice, int sherShortUrlIndex, String sherTitle) {
        this.sherBody = sherBody;
        this.sherId = sherId;
        this.sherTypeId = sherTypeId;
        this.sherContentSlug = sherContentSlug;
        this.sherPoetId = sherPoetId;
        this.sherPoetSlug = sherPoetSlug;
        this.sherPoetName = sherPoetName;
        this.sherImageUrl = sherImageUrl;
        this.sherContentUrl = sherContentUrl;
        this.sherAudioCount = sherAudioCount;
        this.sherVideoCount = sherVideoCount;
        this.sherEditorChoice = sherEditorChoice;
        this.sherPopularChoice = sherPopularChoice;
        this.sherShortUrlIndex = sherShortUrlIndex;
        this.sherTitle = sherTitle;
    }

    public String getSherBody() {
        return sherBody;
    }

    public void setSherBody(String sherBody) {
        this.sherBody = sherBody;
    }

    public String getSherId() {
        return sherId;
    }

    public void setSherId(String sherId) {
        this.sherId = sherId;
    }

    public String getSherTypeId() {
        return sherTypeId;
    }

    public void setSherTypeId(String sherTypeId) {
        this.sherTypeId = sherTypeId;
    }

    public String getSherContentSlug() {
        return sherContentSlug;
    }

    public void setSherContentSlug(String sherContentSlug) {
        this.sherContentSlug = sherContentSlug;
    }

    public String getSherPoetId() {
        return sherPoetId;
    }

    public void setSherPoetId(String sherPoetId) {
        this.sherPoetId = sherPoetId;
    }

    public String getSherPoetSlug() {
        return sherPoetSlug;
    }

    public void setSherPoetSlug(String sherPoetSlug) {
        this.sherPoetSlug = sherPoetSlug;
    }

    public String getSherPoetName() {
        return sherPoetName;
    }

    public void setSherPoetName(String sherPoetName) {
        this.sherPoetName = sherPoetName;
    }

    public String getSherImageUrl() {
        return sherImageUrl;
    }

    public void setSherImageUrl(String sherImageUrl) {
        this.sherImageUrl = sherImageUrl;
    }

    public String getSherContentUrl() {
        return sherContentUrl;
    }

    public void setSherContentUrl(String sherContentUrl) {
        this.sherContentUrl = sherContentUrl;
    }

    public int getSherAudioCount() {
        return sherAudioCount;
    }

    public void setSherAudioCount(int sherAudioCount) {
        this.sherAudioCount = sherAudioCount;
    }

    public int getSherVideoCount() {
        return sherVideoCount;
    }

    public void setSherVideoCount(int sherVideoCount) {
        this.sherVideoCount = sherVideoCount;
    }

    public boolean isSherEditorChoice() {
        return sherEditorChoice;
    }

    public void setSherEditorChoice(boolean sherEditorChoice) {
        this.sherEditorChoice = sherEditorChoice;
    }

    public boolean isSherPopularChoice() {
        return sherPopularChoice;
    }

    public void setSherPopularChoice(boolean sherPopularChoice) {
        this.sherPopularChoice = sherPopularChoice;
    }

    public int getSherShortUrlIndex() {
        return sherShortUrlIndex;
    }

    public void setSherShortUrlIndex(int sherShortUrlIndex) {
        this.sherShortUrlIndex = sherShortUrlIndex;
    }

    public String getSherTitle() {
        return sherTitle;
    }

    public void setSherTitle(String sherTitle) {
        this.sherTitle = sherTitle;
    }
}
