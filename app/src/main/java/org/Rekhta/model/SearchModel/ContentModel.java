package org.Rekhta.model.SearchModel;

/**
 * Created by Admin on 11/14/2017.
 */

public class ContentModel {
/*     "Body": "fitrat kii ye guunaaguunii<br/>gulshan bin vaadii viiraane",
             "Id": "31795864-416c-4054-a8d0-3f3e16fea3b9",
             "TypeId": "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b",
             "ContentSlug": "saathii-majeed-amjad-nazms",
             "PoetId": "15dcf431-ee51-4539-aa73-efbe1cf7385a",
             "PoetSlug": "majeed-amjad",
             "PoetName": "Majeed Amjad",
             "ImageUrl": "https://www.rekhta.org/Images/Shayar/majeed-amjad.png",
             "ContentUrl": "https://www.rekhta.org/nazms/saathii-majeed-amjad-nazms",
             "AudioCount": 0,
             "VideoCount": 0,
             "EditorChoice": false,
             "PopularChoice": false,
             "ShortUrlIndex": 0,
             "Title": "sathi"*/

    private String contentBody;
    private String contentId;
    private String contentTypeId;
    private String contentContentSlug;
    private String contentPoetId;
    private String contentPoetSlug;
    private String contentPoetName;
    private String contentImageUrl;
    private String contentContentUrl;
    private int contentAudioCount;
    private int contentVideoCount;
    private boolean contentEditorChoice;
    private boolean contentPopularChoice;
    private int contentShortUrlIndex;
    private String contentTitle;

    public ContentModel(String contentBody, String contentId, String contentTypeId, String contentContentSlug, String contentPoetId, String contentPoetSlug, String contentPoetName, String contentImageUrl, String contentContentUrl, int contentAudioCount, int contentVideoCount, boolean contentEditorChoice, boolean contentPopularChoice, int contentShortUrlIndex, String contentTitle) {
        this.contentBody = contentBody;
        this.contentId = contentId;
        this.contentTypeId = contentTypeId;
        this.contentContentSlug = contentContentSlug;
        this.contentPoetId = contentPoetId;
        this.contentPoetSlug = contentPoetSlug;
        this.contentPoetName = contentPoetName;
        this.contentImageUrl = contentImageUrl;
        this.contentContentUrl = contentContentUrl;
        this.contentAudioCount = contentAudioCount;
        this.contentVideoCount = contentVideoCount;
        this.contentEditorChoice = contentEditorChoice;
        this.contentPopularChoice = contentPopularChoice;
        this.contentShortUrlIndex = contentShortUrlIndex;
        this.contentTitle = contentTitle;
    }

    public String getContentBody() {
        return contentBody;
    }

    public void setContentBody(String contentBody) {
        this.contentBody = contentBody;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getContentTypeId() {
        return contentTypeId;
    }

    public void setContentTypeId(String contentTypeId) {
        this.contentTypeId = contentTypeId;
    }

    public String getContentContentSlug() {
        return contentContentSlug;
    }

    public void setContentContentSlug(String contentContentSlug) {
        this.contentContentSlug = contentContentSlug;
    }

    public String getContentPoetId() {
        return contentPoetId;
    }

    public void setContentPoetId(String contentPoetId) {
        this.contentPoetId = contentPoetId;
    }

    public String getContentPoetSlug() {
        return contentPoetSlug;
    }

    public void setContentPoetSlug(String contentPoetSlug) {
        this.contentPoetSlug = contentPoetSlug;
    }

    public String getContentPoetName() {
        return contentPoetName;
    }

    public void setContentPoetName(String contentPoetName) {
        this.contentPoetName = contentPoetName;
    }

    public String getContentImageUrl() {
        return contentImageUrl;
    }

    public void setContentImageUrl(String contentImageUrl) {
        this.contentImageUrl = contentImageUrl;
    }

    public String getContentContentUrl() {
        return contentContentUrl;
    }

    public void setContentContentUrl(String contentContentUrl) {
        this.contentContentUrl = contentContentUrl;
    }

    public int getContentAudioCount() {
        return contentAudioCount;
    }

    public void setContentAudioCount(int contentAudioCount) {
        this.contentAudioCount = contentAudioCount;
    }

    public int getContentVideoCount() {
        return contentVideoCount;
    }

    public void setContentVideoCount(int contentVideoCount) {
        this.contentVideoCount = contentVideoCount;
    }

    public boolean isContentEditorChoice() {
        return contentEditorChoice;
    }

    public void setContentEditorChoice(boolean contentEditorChoice) {
        this.contentEditorChoice = contentEditorChoice;
    }

    public boolean isContentPopularChoice() {
        return contentPopularChoice;
    }

    public void setContentPopularChoice(boolean contentPopularChoice) {
        this.contentPopularChoice = contentPopularChoice;
    }

    public int getContentShortUrlIndex() {
        return contentShortUrlIndex;
    }

    public void setContentShortUrlIndex(int contentShortUrlIndex) {
        this.contentShortUrlIndex = contentShortUrlIndex;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }
}
