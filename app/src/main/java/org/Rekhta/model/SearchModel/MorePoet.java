package org.Rekhta.model.SearchModel;

/**
 * Created by Admin on 11/13/2017.
 */

public class MorePoet {
   private String poetEntityId;
   private String poetName;
   private int poetGazalCount;
   private boolean poetIsNew;
   private int poetNazmCount;
   private String poetSEO_Slug;
   private String poetImageUrl;

    public MorePoet(String poetEntityId, String poetName, int poetGazalCount, boolean poetIsNew, int poetNazmCount, String poetSEO_Slug, String poetImageUrl) {
        this.poetEntityId = poetEntityId;
        this.poetName = poetName;
        this.poetGazalCount = poetGazalCount;
        this.poetIsNew = poetIsNew;
        this.poetNazmCount = poetNazmCount;
        this.poetSEO_Slug = poetSEO_Slug;
        this.poetImageUrl = poetImageUrl;
    }

    public String getPoetEntityId() {
        return poetEntityId;
    }

    public void setPoetEntityId(String poetEntityId) {
        this.poetEntityId = poetEntityId;
    }

    public String getPoetName() {
        return poetName;
    }

    public void setPoetName(String poetName) {
        this.poetName = poetName;
    }

    public int getPoetGazalCount() {
        return poetGazalCount;
    }

    public void setPoetGazalCount(int poetGazalCount) {
        this.poetGazalCount = poetGazalCount;
    }

    public boolean isPoetIsNew() {
        return poetIsNew;
    }

    public void setPoetIsNew(boolean poetIsNew) {
        this.poetIsNew = poetIsNew;
    }

    public int getPoetNazmCount() {
        return poetNazmCount;
    }

    public void setPoetNazmCount(int poetNazmCount) {
        this.poetNazmCount = poetNazmCount;
    }

    public String getPoetSEO_Slug() {
        return poetSEO_Slug;
    }

    public void setPoetSEO_Slug(String poetSEO_Slug) {
        this.poetSEO_Slug = poetSEO_Slug;
    }

    public String getPoetImageUrl() {
        return poetImageUrl;
    }

    public void setPoetImageUrl(String poetImageUrl) {
        this.poetImageUrl = poetImageUrl;
    }
}
