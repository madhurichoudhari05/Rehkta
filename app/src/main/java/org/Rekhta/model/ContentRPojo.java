package org.Rekhta.model;


import com.google.gson.annotations.SerializedName;

public class ContentRPojo {

    @SerializedName("DM")
    private String DM;

    @SerializedName("SS")
    private String SS;

    @SerializedName("NH")
    private String NH;

    @SerializedName("NU")
    private String NU;

    @SerializedName("I")
    private String I;

    @SerializedName("NE")
    private String NE;

    public String getDM() {
        return DM;
    }

    public void setDM(String DM) {
        this.DM = DM;
    }

    public String getSS() {
        return SS;
    }

    public void setSS(String SS) {
        this.SS = SS;
    }

    public String getNH() {
        return NH;
    }

    public void setNH(String NH) {
        this.NH = NH;
    }

    public String getNU() {
        return NU;
    }

    public void setNU(String NU) {
        this.NU = NU;
    }

    public String getI() {
        return I;
    }

    public void setI(String I) {
        this.I = I;
    }

    public String getNE() {
        return NE;
    }

    public void setNE(String NE) {
        this.NE = NE;
    }

    @Override
    public String toString() {
        return "ClassPojo [DM = " + DM + ", SS = " + SS + ", NH = " + NH + ", NU = " + NU + ", I = " + I + ", NE = " + NE + "]";
    }
}
