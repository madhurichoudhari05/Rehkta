package org.Rekhta.model.onlineFavPoetryModel;

/**
 * Created by Admin on 12/14/2017.
 */

public class OnlineFavNazam {

    private String TE;

    private String EC;

    private String TH;

    private String N;

    private String I;

    private String VI;

    private String SU;

    private String T;

    private String VC;

    private String P;

    private String S;

    private String R;

    private String FL;

    private String UU;

    private String SC;

    private String SE;

    private String FD;

    private String SH;

    private String FC;

    private String SI;

    private String AU;

    private String UE;

    private String UH;

    private String AC;

    private String PU;

    private String PH;

    private String PI;

    private String PE;

    private String TU;

    private String PC;

    public String getTE ()
    {
        return TE;
    }

    public void setTE (String TE)
    {
        this.TE = TE;
    }

    public String getEC ()
    {
        return EC;
    }

    public void setEC (String EC)
    {
        this.EC = EC;
    }

    public String getTH ()
    {
        return TH;
    }

    public void setTH (String TH)
    {
        this.TH = TH;
    }

    public String getN ()
    {
        return N;
    }

    public void setN (String N)
    {
        this.N = N;
    }

    public String getI ()
    {
        return I;
    }

    public void setI (String I)
    {
        this.I = I;
    }

    public String getVI ()
    {
        return VI;
    }

    public void setVI (String VI)
    {
        this.VI = VI;
    }

    public String getSU ()
    {
        return SU;
    }

    public void setSU (String SU)
    {
        this.SU = SU;
    }

    public String getT ()
    {
        return T;
    }

    public void setT (String T)
    {
        this.T = T;
    }

    public String getVC ()
    {
        return VC;
    }

    public void setVC (String VC)
    {
        this.VC = VC;
    }

    public String getP ()
    {
        return P;
    }

    public void setP (String P)
    {
        this.P = P;
    }

    public String getS ()
    {
        return S;
    }

    public void setS (String S)
    {
        this.S = S;
    }

    public String getR ()
    {
        return R;
    }

    public void setR (String R)
    {
        this.R = R;
    }

    public String getFL ()
    {
        return FL;
    }

    public void setFL (String FL)
    {
        this.FL = FL;
    }

    public String getUU ()
    {
        return UU;
    }

    public void setUU (String UU)
    {
        this.UU = UU;
    }

    public String getSC ()
    {
        return SC;
    }

    public void setSC (String SC)
    {
        this.SC = SC;
    }

    public String getSE ()
    {
        return SE;
    }

    public void setSE (String SE)
    {
        this.SE = SE;
    }

    public String getFD ()
    {
        return FD;
    }

    public void setFD (String FD)
    {
        this.FD = FD;
    }

    public String getSH ()
    {
        return SH;
    }

    public void setSH (String SH)
    {
        this.SH = SH;
    }

    public String getFC ()
    {
        return FC;
    }

    public void setFC (String FC)
    {
        this.FC = FC;
    }

    public String getSI ()
    {
        return SI;
    }

    public void setSI (String SI)
    {
        this.SI = SI;
    }

    public String getAU ()
    {
        return AU;
    }

    public void setAU (String AU)
    {
        this.AU = AU;
    }

    public String getUE ()
    {
        return UE;
    }

    public void setUE (String UE)
    {
        this.UE = UE;
    }

    public String getUH ()
    {
        return UH;
    }

    public void setUH (String UH)
    {
        this.UH = UH;
    }

    public String getAC ()
    {
        return AC;
    }

    public void setAC (String AC)
    {
        this.AC = AC;
    }

    public String getPU ()
    {
        return PU;
    }

    public void setPU (String PU)
    {
        this.PU = PU;
    }

    public String getPH ()
    {
        return PH;
    }

    public void setPH (String PH)
    {
        this.PH = PH;
    }

    public String getPI ()
    {
        return PI;
    }

    public void setPI (String PI)
    {
        this.PI = PI;
    }

    public String getPE ()
    {
        return PE;
    }

    public void setPE (String PE)
    {
        this.PE = PE;
    }

    public String getTU ()
    {
        return TU;
    }

    public void setTU (String TU)
    {
        this.TU = TU;
    }

    public String getPC ()
    {
        return PC;
    }

    public void setPC (String PC)
    {
        this.PC = PC;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [TE = "+TE+", EC = "+EC+", TH = "+TH+", N = "+N+", I = "+I+", VI = "+VI+", SU = "+SU+", T = "+T+", VC = "+VC+", P = "+P+", S = "+S+", R = "+R+", FL = "+FL+", UU = "+UU+", SC = "+SC+", SE = "+SE+", FD = "+FD+", SH = "+SH+", FC = "+FC+", SI = "+SI+", AU = "+AU+", UE = "+UE+", UH = "+UH+", AC = "+AC+", PU = "+PU+", PH = "+PH+", PI = "+PI+", PE = "+PE+", TU = "+TU+", PC = "+PC+"]";
    }
}
