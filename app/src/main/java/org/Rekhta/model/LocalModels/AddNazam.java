package org.Rekhta.model.LocalModels;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


import java.io.Serializable;

@Entity(tableName = "user_table")

public class AddNazam implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int userid;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "content")
    private String content;

    @ColumnInfo(name = "auther")
    private String auther;

    @ColumnInfo(name = "data_array")
    private String dataArray;

    @ColumnInfo(name = "lang_type")
    private int lang_type;

    public AddNazam() {

    }

    public int getLang_type() {
        return lang_type;
    }

    public void setLang_type(int lang_type) {
        this.lang_type = lang_type;
    }

    public String getDataArray() {
        return dataArray;
    }

    public void setDataArray(String dataArray) {
        this.dataArray = dataArray;
    }

    @NonNull
    public int getUserid() {
        return userid;
    }

    public void setUserid(@NonNull int userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuther() {
        return auther;
    }

    public void setAuther(String auther) {
        this.auther = auther;
    }

}
