package org.Rekhta.model.LocalModels;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Admin on 12/9/2017.
 */

@Entity(tableName = "sher_table")
public class AddSher {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int userid;

    @ColumnInfo(name = "sher_content")
    private String sher_content;

    @ColumnInfo(name = "sher_auther")
    private String sher_auther;

    @NonNull
    public int getUserid() {
        return userid;
    }

    public void setUserid(@NonNull int userid) {
        this.userid = userid;
    }

    public String getSher_content() {
        return sher_content;
    }

    public void setSher_content(String sher_content) {
        this.sher_content = sher_content;
    }

    public String getSher_auther() {
        return sher_auther;
    }

    public void setSher_auther(String sher_auther) {
        this.sher_auther = sher_auther;
    }
}
