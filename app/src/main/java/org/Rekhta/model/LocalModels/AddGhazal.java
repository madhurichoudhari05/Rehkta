package org.Rekhta.model.LocalModels;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;


@Entity(tableName = "ghazal_table")
public class AddGhazal implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int userid;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "auther")
    private String auther;
    @ColumnInfo(name = "content_data")
    private String content_data;
    @ColumnInfo(name = "lang_type")
    private int lang_type;


    public AddGhazal() {

    }

    protected AddGhazal(Parcel in) {

        userid = in.readInt();
        title = in.readString();
        auther = in.readString();
        content_data = in.readString();
    }

    public String getContent_data() {
        return content_data;
    }

    public void setContent_data(String content_data) {
        this.content_data = content_data;
    }

    public int getLang_type() {
        return lang_type;
    }

    public void setLang_type(int lang_type) {
        this.lang_type = lang_type;
    }

    @NonNull
    public int getUserid() {
        return userid;
    }

    public void setUserid(@NonNull int userid) {
        this.userid = userid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuther() {
        return auther;
    }

    public void setAuther(String auther) {
        this.auther = auther;
    }


}
