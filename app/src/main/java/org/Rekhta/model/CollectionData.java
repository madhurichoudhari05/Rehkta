package org.Rekhta.model;

/**
 * Created by Admin on 9/27/2017.
 */

public class CollectionData {


    private String cTitleEng;
    private String cTitleHin;
    private String cTitleUrdu;
    private String top20TextEng;
    private String top20TextHindi;
    private String top20TextUrdu;
    private String imageUrlTxt;
    private String sherId;

    public CollectionData(String sherId,String imageUrlTxt, String cTitleEng, String cTitleHin, String cTitleUrdu,String top20TextEng,String top20TextHindi,String top20TextUrdu) {
        this.imageUrlTxt = imageUrlTxt;
        this.cTitleEng = cTitleEng;
        this.cTitleHin = cTitleHin;
        this.cTitleUrdu = cTitleUrdu;
        this.top20TextEng = top20TextEng;
        this.top20TextHindi = top20TextHindi;
        this.top20TextUrdu= top20TextUrdu;
        this.sherId= sherId;
    }



    public String getImageUrlTxt() {
        return imageUrlTxt;
    }

    public void setImageUrlTxt(String imageUrlTxt) {
        this.imageUrlTxt = imageUrlTxt;
    }

    public String getcTitleEng() {
        return cTitleEng;
    }

    public void setcTitleEng(String cTitleEng) {
        this.cTitleEng = cTitleEng;
    }

    public String getcTitleHin() {
        return cTitleHin;
    }

    public void setcTitleHin(String cTitleHin) {
        this.cTitleHin = cTitleHin;
    }

    public String getcTitleUrdu() {
        return cTitleUrdu;
    }

    public void setcTitleUrdu(String cTitleUrdu) {
        this.cTitleUrdu = cTitleUrdu;
    }

    public String getTop20TextEng() {
        return top20TextEng;
    }

    public void setTop20TextEng(String top20TextEng) {
        this.top20TextEng = top20TextEng;
    }

    public String getTop20TextHindi() {
        return top20TextHindi;
    }

    public void setTop20TextHindi(String top20TextHindi) {
        this.top20TextHindi = top20TextHindi;
    }

    public String getTop20TextUrdu() {
        return top20TextUrdu;
    }

    public void setTop20TextUrdu(String top20TextUrdu) {
        this.top20TextUrdu = top20TextUrdu;
    }

    public String getSherId() {
        return sherId;
    }

    public void setSherId(String sherId) {
        this.sherId = sherId;
    }


}
