package org.Rekhta.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContentTypePojo {

    @SerializedName("Me")
    private String Me;

    @SerializedName("T")
    private String T;

    @SerializedName("Mh")
    private String Mh;

    @SerializedName("S")
    private String S;

    @SerializedName("R")
    private List<ContentRPojo> contentRPojos;

    private String Mu;

    public String getMe() {
        return Me;
    }

    public void setMe(String Me) {
        this.Me = Me;
    }

    public String getT() {
        return T;
    }

    public void setT(String T) {
        this.T = T;
    }

    public String getMh() {
        return Mh;
    }

    public void setMh(String Mh) {
        this.Mh = Mh;
    }

    public String getS() {
        return S;
    }

    public void setS(String S) {
        this.S = S;
    }

    public List<ContentRPojo> getR() {
        return contentRPojos;
    }

    public void setR(List<ContentRPojo> contentRPojos) {
        this.contentRPojos = contentRPojos;
    }

    public String getMu() {
        return Mu;
    }

    public void setMu(String Mu) {
        this.Mu = Mu;
    }

    @Override
    public String toString() {
        return "ClassPojo [Me = " + Me + ", T = " + T + ", Mh = " + Mh + ", S = " + S + ", R = " + contentRPojos + ", Mu = " + Mu + "]";
    }
}
