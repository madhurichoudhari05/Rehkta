package org.Rekhta.model.viewProfile;

/**
 * Created by Admin on 11/7/2017.
 */

public class ProfileModel {
    String poet;
    String poetName;

    public ProfileModel(String poet, String poetName) {
        this.poet = poet;
        this.poetName = poetName;
    }

    public String getPoet() {

        return poet;
    }

    public void setPoet(String poet) {
        this.poet = poet;
    }

    public String getPoetName() {
        return poetName;
    }

    public void setPoetName(String poetName) {
        this.poetName = poetName;
    }
}
