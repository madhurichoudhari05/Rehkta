package org.Rekhta;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ForgetPassword extends AppCompatActivity {
    Button submit_forget;
    ImageView left_btn;
    EditText usersEmailId;
    TextView serverResponeText;
    String emailId = "";
    LinearLayout userEmailResetLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        Typeface btntf = Typeface.createFromAsset(this.getAssets(), "fonts/LatoBold.ttf");
        Typeface latoRegular = Typeface.createFromAsset(this.getAssets(), "fonts/Lato-Regular.ttf");
        submit_forget = (Button) findViewById(R.id.submit_forget);
        submit_forget.setTypeface(btntf);

        submit_forget.setTypeface(btntf);
        userEmailResetLinear = (LinearLayout) findViewById(R.id.userEmailResetLinear);


        usersEmailId = (EditText) findViewById(R.id.usersEmailId);
        usersEmailId.setTypeface(latoRegular);
        usersEmailId.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        serverResponeText = (TextView) findViewById(R.id.errorMessage);
        left_btn = (ImageView) findViewById(R.id.left_btn);
        left_btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            onBackPressed();
                                        }
                                    }
        );
        usersEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                userEmailResetLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));
            }

            @Override
            public void afterTextChanged(Editable s) {
                userEmailResetLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));
            }
        });

        submit_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                automaticHideErrorMsg();
                hideKeyBoard();

                if (submit_forget.getText().toString().equalsIgnoreCase("Login Again")) {

                    Intent intent = new Intent(ForgetPassword.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    automaticHideErrorMsg();
                    checkAndSendPasswordToMail();
                }
            }
        });


        usersEmailId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    automaticHideErrorMsg();
                    hideKeyBoard();

                    if (submit_forget.getText().toString().equalsIgnoreCase("Login Again")) {
                        Intent intent = new Intent(ForgetPassword.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        automaticHideErrorMsg();
                        checkAndSendPasswordToMail();
                    }
                }
                return false;
            }
        });
    }

    public void checkAndSendPasswordToMail() {
        emailId = usersEmailId.getText().toString();
        if (emailId.equalsIgnoreCase("") || emailId.equalsIgnoreCase(" ")) {
//            serverResponeText.setVisibility(View.VISIBLE);
//            serverResponeText.setText("Please enter your email id");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userEmailResetLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }

        } else {
            if (CommonUtil.isValidEmail(emailId)) {
                sendPasswordToMailId(emailId);
            } else {
//                serverResponeText.setVisibility(View.VISIBLE);
//                serverResponeText.setText("Please enter a valid email id");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    userEmailResetLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
                }

            }

        }
    }

    public void sendPasswordToMailId(String email) {
        final ProgressDialog progressLoader = ProgressDialog.show(this, null, "Please wait...", false, false);
        String uniqueID = UUID.randomUUID().toString();

        HashMap obj = new HashMap();
        obj.put("Email", email);
        serverResponeText.setVisibility(View.GONE);


        RestClient.get().forgotPassword(CommonUtil.SharedPrefKeys.contentType, uniqueID, uniqueID, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing())
                    progressLoader.dismiss();
                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        serverResponeText.setVisibility(View.VISIBLE);
                        usersEmailId.setVisibility(View.GONE);
                        serverResponeText.setTextColor(getResources().getColor(R.color.black));
                        serverResponeText.setText(R.string.success_email_sent);
                        userEmailResetLinear.setVisibility(View.GONE);
                        submit_forget.setText("Login Again");

                    } else {
//                        serverResponeText.setVisibility(View.VISIBLE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            userEmailResetLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
                        }
//                        serverResponeText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
//                        serverResponeText.setText(obj.getString("Me"));
                        Toast.makeText(ForgetPassword.this, obj.getString("Me"), Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                serverResponeText.setVisibility(View.VISIBLE);
                serverResponeText.setText("An Error Ocurred ! Please Try Again");
            }
        });
    }

    public void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void automaticHideErrorMsg() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideErrorMsgs();
            }
        }, 3000);
    }

    public void hideErrorMsgs() {
        serverResponeText.setVisibility(View.GONE);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyBoard();
        }
        return super.dispatchTouchEvent(ev);
    }

}
