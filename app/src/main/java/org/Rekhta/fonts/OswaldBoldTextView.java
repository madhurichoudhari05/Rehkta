package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class OswaldBoldTextView extends android.support.v7.widget.AppCompatTextView {

    public OswaldBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public OswaldBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OswaldBoldTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Oswald-Regular.ttf");
        setTypeface(tf);
    }
}
