package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class MerriweatherRegularTextView extends android.support.v7.widget.AppCompatTextView {

    public MerriweatherRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MerriweatherRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MerriweatherRegularTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Merriweather-Regular.ttf");
        setTypeface(tf);

    }
}
