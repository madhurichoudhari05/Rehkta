package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class LatoRegularTextView extends android.support.v7.widget.AppCompatTextView {

    public LatoRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LatoRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LatoRegularTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        setTypeface(tf);

    }

}
