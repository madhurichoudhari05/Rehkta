package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class MerriweatherBoldTextView extends android.support.v7.widget.AppCompatTextView {

    public MerriweatherBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MerriweatherBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MerriweatherBoldTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/merriweather_bold.ttf");
        setTypeface(tf);

    }
}
