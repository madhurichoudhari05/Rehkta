package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class DevanagriHindiTextView extends android.support.v7.widget.AppCompatTextView {

    public DevanagriHindiTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public DevanagriHindiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DevanagriHindiTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        setTypeface(tf);
    }
}
