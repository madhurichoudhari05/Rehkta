package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class MerriweatherLightItalicTextView extends android.support.v7.widget.AppCompatTextView {


    public MerriweatherLightItalicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MerriweatherLightItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MerriweatherLightItalicTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/merriweather_light_italic.ttf");
        setTypeface(tf);

    }
}
