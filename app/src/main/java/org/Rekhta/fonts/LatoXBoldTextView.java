package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class LatoXBoldTextView extends android.support.v7.widget.AppCompatTextView {

    public LatoXBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LatoXBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LatoXBoldTextView(Context context) {
        super(context);
        init();
    }

    private void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Black.ttf");
        setTypeface(tf);
    }
}
