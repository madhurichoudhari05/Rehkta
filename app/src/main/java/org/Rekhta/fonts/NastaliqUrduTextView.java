package org.Rekhta.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class NastaliqUrduTextView extends android.support.v7.widget.AppCompatTextView {
    public NastaliqUrduTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public NastaliqUrduTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NastaliqUrduTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        //if (!isInEditMode()) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
        setTypeface(tf);
        //}
    }
}
