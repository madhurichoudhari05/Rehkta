package org.Rekhta.utils;

import android.media.MediaPlayer;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.Rekhta.R;
import org.Rekhta.activites.GhazalActivity;

/**
 * Created by SANCHIT on 9/7/2016.
 */
public class MediaPlayerService {

    static private MediaPlayer mediaPlayer;
    static private Boolean isMusicRunning = false;

    static public void playAudio(String audioUrl, final ImageView currentPlayButtonImage, final ProgressBar currentProgressbar) {
        try {

            if (isMusicRunning) {
                //footor_audioIcon.setImageResource(R.drawable.ic_volume_medium);
                currentPlayButtonImage.setImageResource(R.drawable.ic_audio_pausebtn);
                currentProgressbar.setVisibility(View.GONE);
                MediaPlayerService.stopAudio();

            } else {
                currentPlayButtonImage.setVisibility(View.GONE);
                currentProgressbar.setVisibility(View.VISIBLE);
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(audioUrl);
                mediaPlayer.prepareAsync();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                        currentPlayButtonImage.setVisibility(View.VISIBLE);
                        currentProgressbar.setVisibility(View.GONE);
                        currentPlayButtonImage.setImageResource(R.drawable.ic_audio_pausebtn);
                    }
                });
                //mediaPlayer.start();
                isMusicRunning = true;

                // REALEASING INSTANCE WHEN ON COMPLETE IS INVOKED
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        currentPlayButtonImage.setImageResource(android.R.drawable.ic_media_play);

                    }
                });

            }

        } catch (Exception e) {
            System.out.println("*******************************" + e.getMessage());
            mediaPlayer = null;
        }
    }

    ;
    static public void stopAudio() {

        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            isMusicRunning = false;
        }
    }

    ;

}
