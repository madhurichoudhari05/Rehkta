package org.Rekhta.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import org.Rekhta.fragments.ghazalFragments.BeginnersNewTab;
import org.Rekhta.fragments.ghazalFragments.ghazal_Top100Tab;
import org.Rekhta.fragments.ghazalFragments.ghazal_BeginnersTab;
import org.Rekhta.fragments.ghazalFragments.ghazal_EditorChoiceTab;

public class GhazalPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public GhazalPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ghazal_Top100Tab tab1 = new ghazal_Top100Tab();
                return tab1;
            case 1:
                BeginnersNewTab beginnersNewTab = new BeginnersNewTab();
                return beginnersNewTab;
            case 2:
                ghazal_EditorChoiceTab tab3 = new ghazal_EditorChoiceTab();
                return tab3;
            case 3:
                ghazal_BeginnersTab tab2 = new ghazal_BeginnersTab();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        getItem(position).onDetach();
        //super.destroyItem(container, position, object);
    }
}