package org.Rekhta.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;

import org.Rekhta.constants.AppConstants;


/**
 * @author Amardeep Rawat
 */
public class CheckConnectionAndPermissions {
    /**
     * @param ctx
     * @return
     */
    public static boolean isConnection(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
        if (ni != null && ni.isAvailable() && ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void checkPermissions(Activity mContext ,int permission) {

        // only for gingerbread and newer versions
        try {
            // Request location updates using static settings
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                // Should we show an explanation?
                // No explanation needed, we can request the permission.
                if(permission==0) {
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION},
                            AppConstants.REQUEST_CODE_PERMISSION);
                }
                else if(permission==1)
                {
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.CAMERA,},
                            AppConstants.REQUEST_CODE_PERMISSION);
                }
                else if(permission==2)
                {
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.CAMERA,},
                            AppConstants.REQUEST_CODE_PERMISSION);
                }

                // LocationConstants.REQUEST_CODE_COARSE_LOCATION_PERMISSION is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
