package org.Rekhta.utils;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.Broadcast.ConnectivityRecevier;
import org.Rekhta.MainActivity;
import org.Rekhta.activites.Dashboard;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;


import org.Rekhta.R;
import org.Rekhta.SingUp;
import org.Rekhta.activites.ServerErrorActivity;
import org.Rekhta.model.ContentTypePojo;
import org.Rekhta.model.poetDetail.PoetAudioData;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class CommonUtil {

    public static final Pattern passwordPatternUpperCase = Pattern.compile("(?=.*?[A-Z])");
    public static final Pattern passwordPatternNumber = Pattern.compile("(?=.*?[0-9])");
    public static final Pattern passwordPatternSpecialChar = Pattern.compile("(?=.*?[#?!@$%^&*-])");
    public static final Pattern passwordPatternForSpecialCharMoreThanTwo = Pattern.compile("(?=.*[!@#.$&*].*[!@#$&.*])");
    public static SharedPreferences favouritesPrefs;
    public static boolean isContent = false;
    public static boolean isSkipped = false;
    public static int firstRun = 0;
    public static int languageCode = 1;
    public static String userName = "Guest";
    public static String userEmail = "org.Rekhta@gmail.org";
    public static ContentTypePojo contentTypePojo = null;
    public static String cSherTypeNameEnglish = "";
    public static String cSherTypeNameHindi = "";
    public static String cSherTypeNameUrdu = "";
    public static String cSherTotalCount = "";
    public static boolean hasCameFromTag = false;
    public static ScrollView mainContentScrollView;
    public static String serachedText = "";
    public static double currentScreenSize;
    public static ArrayList<PoetAudioData> audioArrayList = new ArrayList<>();
    public static LanguageChangeListener languageChangeListener = new LanguageChangeListener();
    public static String gType;
    public static boolean dontClose = false;
    public static boolean isOffline = false;
    public static File[] myFiles = null;
    public static final String ghazalTypeId = "43D60A15-0B49-4CAF-8B74-0FCDDDEB9F83";
    public static final String NazmtypeId = "C54C4D8B-7E18-4F70-8312-E1C2CC028B0B";
    public static final String sherTypeId = "F722D5DC-45DA-41EC-A439-900DF702A3D6";
    public static final String rekhtiTypeId = "6C5D3444-6534-4FBD-B859-95EB68A20A86";
    public static final String dohaTypeId = "A12D7CF4-60C2-4D2B-85E4-3F342D008472";
    private static ConnectivityRecevier connectivityRecevier = new ConnectivityRecevier();
    public static boolean isFromAudioTab;
    public static boolean fromMeaning;


    public static void showToast(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.skip_sher_dialog_layout, null);
        builder.setView(view);
        builder.show();

        TextView submiteditLogin = (TextView) view.findViewById(R.id.submiteditLogin);
        TextView submitEditRegister = (TextView) view.findViewById(R.id.submitEditRegister);


        final AlertDialog alertDialog = builder.create();

        submiteditLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.cancel();
                context.startActivity(new Intent(context, MainActivity.class));
            }
        });

        submitEditRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.cancel();
                context.startActivity(new Intent(context, SingUp.class));
            }
        });


    }

    public static void showErrorActivity(Context context) {

        context.startActivity(new Intent(context, ServerErrorActivity.class));
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()) {
                return isValidfirstPartOfMail(target.toString());
            } else {
                return false;
            }
        }
    }

    public final static boolean isValidPassword(CharSequence password) {
        if (password == null) {
            return false;
        } else {
            if (passwordPatternNumber.matcher(password).find() && passwordPatternUpperCase.matcher(password).find() && password.length() > 5) {
                return true;
            } else {
                return false;
            }
        }
    }

    public final static boolean isValidfirstPartOfMail(String input) {
        int index = input.indexOf("@");
        String fText = input.substring(0, index);
        String lText = input.substring(index, input.length());
        int count = lText.length() - lText.replaceAll("[;\\\\/:*.?\\\"<>|&']", "").length();
        //int dotCount = lText.length() - fText.replaceAll(".","").length();
        if (passwordPatternSpecialChar.matcher(fText).find()) { // before @ check Special Charc
            return false;
        } else {
            if (count > 1) { // check for single
                return false;
            } else {
                return true;
            }
        }
    }

    public static boolean isValidName(String name) {
        if (passwordPatternNumber.matcher(name).find() || passwordPatternSpecialChar.matcher(name).find() || name.equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    public static void setEnglishMerriwetherItalicFont(Context context, TextView txt) {
        Typeface engfont = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_light_italic.ttf");
        txt.setTypeface(engfont);
    }

    public static void setEnglishMerriweatherBoldFont(Context context, TextView txt) {
        Typeface engfont = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_bold.ttf");
        txt.setTypeface(engfont);
    }

    public static void setEnglishMerriwetherFont(Context context, TextView txt) {
        Typeface engMRfont = Typeface.createFromAsset(context.getAssets(), "fonts/Merriweather-Regular.ttf");
        txt.setTypeface(engMRfont);
    }

    public static void setEnglishLatoBoldFont(Context context, TextView txt) {
        Typeface engLBfont = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        txt.setTypeface(engLBfont);
    }

    public static void setEnglishLatoXRegualr(Context context, TextView txt) {
        Typeface engLBfont = Typeface.createFromAsset(context.getAssets(), "fonts/LatoX-Regular.ttf");
        txt.setTypeface(engLBfont);
    }

    public static void setEnglishLatoXBold(Context context, TextView txt) {
        Typeface engLBfont = Typeface.createFromAsset(context.getAssets(), "fonts/LatoX-Bold.ttf");
        txt.setTypeface(engLBfont);
    }


    public static void setEnglishLatoRegularFont(Context context, TextView txt) {
        Typeface engLBfont = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
        txt.setTypeface(engLBfont);
    }


    public static void setEnglishLatoSemiBold(Context context, TextView txt) {
        Typeface engLBfont = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-SemiBold.ttf");
        txt.setTypeface(engLBfont);
    }

    public static void setEnglishMeriWeatherItalic(Context context, TextView txt) {
        Typeface engLBfont = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_light_italic.ttf");
        txt.setTypeface(engLBfont);
    }

    public static void setLatoMedium(Context context, TextView textView) {

        Typeface hindiTf = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Medium.ttf");
        textView.setTypeface(hindiTf);
    }

    public static void setOswaldMedium(Context context, TextView textView) {

        Typeface hindiTf = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Medium.ttf");
        textView.setTypeface(hindiTf);
    }

    public static void setEnglishMerriweatherBlack(Context context, TextView textView) {

        Typeface hindiTf = Typeface.createFromAsset(context.getAssets(), "fonts/Merriweather-Black.ttf");
        textView.setTypeface(hindiTf);
    }

    public static void setOswaldRegular(Context context, TextView textView) {

        Typeface hindiTf = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Regular.ttf");
        textView.setTypeface(hindiTf);
    }


    public static void setHindiFont(Context context, TextView txt) {
        Typeface hindiTf = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        txt.setTypeface(hindiTf);
    }

    public static void setHindiLailaRegular(Context context, TextView txt) {
        Typeface hindiTf = Typeface.createFromAsset(context.getAssets(), "fonts/Laila-Regular.ttf");
        txt.setTypeface(hindiTf);
    }

    public static void setUrduFont(Context context, TextView txt) {

        Typeface urdufont = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
        txt.setTypeface(urdufont);
    }

    public static void setUrduNotoNataliq(Context context, TextView txt) {

        Typeface urdufont = Typeface.createFromAsset(context.getAssets(), "fonts/NotoNastaliqUrdu.ttf");
        txt.setTypeface(urdufont);
    }

    public static void setUrduRuqa(Context context, TextView txt) {

        Typeface urdufont = Typeface.createFromAsset(context.getAssets(), "fonts/aref-ruqaa.ttf");
        txt.setTypeface(urdufont);
    }

    public static void setRozaOne(Context context, TextView headerTxt) {

        Typeface urdufont = Typeface.createFromAsset(context.getApplicationContext().getAssets(), "fonts/RozhaOne-Regular.ttf");
        headerTxt.setTypeface(urdufont);
    }


    public static int calulateTotalApiCall(int totalCount) {
        int count = totalCount / 20;
        if (totalCount % 20 != 0) {
            int rcount = count + 1;
            return rcount;
        } else {
            return count;
        }
    }

    public static JSONArray concatArray(JSONArray arr1, JSONArray arr2)
            throws JSONException {
        JSONArray result = new JSONArray();
        for (int i = 0; i < arr1.length(); i++) {
            result.put(arr1.get(i));
        }
        for (int i = 0; i < arr2.length(); i++) {
            result.put(arr2.get(i));
        }
        return result;
    }

    public static void setSpecificLanguage(TextView view, String keyId, int id) {
        if (view != null) {
            String langKey = "";
            if (id == 1) {
                langKey = "eng";
            } else if (id == 2) {
                langKey = "hin";
            } else {
                langKey = "urdu";
            }
            String langtext = "";
            try {
                langtext = LanguageSelection.getSpecificLanguageWord(keyId, langKey);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            view.setText(langtext);
        }
    }

    public static void saveToFavPref(String contentId) {
        CommonUtil.favouritesPrefs.edit().putBoolean(contentId, true).commit();
    }

    public static void removeFromFavPref(String contentId) {
        CommonUtil.favouritesPrefs.edit().putBoolean(contentId, false).commit();
    }

    public static boolean isContentUsersFav(String contentId) {
        return CommonUtil.favouritesPrefs.getBoolean(contentId, false);
    }

    public static void addToFavTroughApi(final Context context, final String contentId) {
        RestClient.get().markAsFavourite(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, contentId, CommonUtil.languageCode, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        CommonUtil.favouritesPrefs.edit().putBoolean(contentId, true).commit();
                        //  Toast.makeText(context, "Added to favorites", Toast.LENGTH_SHORT).show();
                        showFavMessage(context);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    public static void removeFromFavTroughApi(final Context context, final String contentId) {
        RestClient.get().removeFromFavourite(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, contentId, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        CommonUtil.favouritesPrefs.edit().putBoolean(contentId, false).commit();
                        Toast.makeText(context, "Removed From favorites", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


    }

    public static void showErrorDialog(Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        View view = LayoutInflater.from(context).inflate(R.layout.activity_server_error, null, false);
        builder.setView(view);
        builder.show();
    }

    public static void registerReciver(Context context) {

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

        context.registerReceiver(connectivityRecevier, intentFilter);
    }

    public static void unregisterReciebr(Context context) {

        context.unregisterReceiver(connectivityRecevier);
    }

    public static void showCommonDialog(Context context) {

        AlertDialog.Builder builder=new AlertDialog.Builder(context);

    }

    public static void showFavMessage(Context context) {

        Toast.makeText(context, LanguageSelection.getSpecificLanguageWordById("Added to Favorite", languageCode), Toast.LENGTH_SHORT).show();

    }

    public static void setBackButton(Context context, ImageView backButton) {

        if (CommonUtil.languageCode == 3) {
            backButton.setImageResource(R.drawable.pre_lollipop_right_arrow);
        } else {
           backButton.setImageResource(R.drawable.ic_back_pre_lollipop);
        }

    }


    // stattic Variables accesabile to all
    public static class SharedPrefKeys {

        public static final String MY_SHAREDPREF_NAME = "biglife.org.org.Rekhta";
        public static final String FAV_FILE = "rekhta_Favpref";
        public static final String contentType = "application/json";
        public static final String contentTypeMultipart = "multipart/form-data";
        public static String UniqueId = "";
        public static String DeviceParams = "";
        public static String rekhtasAccessToken = "Bearer jYcqjxKUiu4prCL2ZLjyAwz1Y4e4bYnLgOSxFbiB7FcZrCglhFYSuqJvpMyXV7ubM1zrcB_5wpZ6Qd5SmpPQM_ApwnL61_mLt1J0D3mQGAjJPPGmqkK5JlDli-v-2Z3cjVi4VKj9BLCFK-LNgMpimTD_ZWyFYwksq_KCzBbkMoNCCU7yBNtJozUw_sYHLCkXv3zHHSyGMeNTTyXs0gQdvVzs9CPcT8K3UgBlbCLVQD4KF5s_DZKJ5Mji0Qd092UqZB2n2TbJS-49ARukrGLlGAvCgGvCdN0XdSkjNHsRKWoTXCKC_GLkKBV9ab5kOK-BDLvF1rVvJBdnSvg_luAInM6aGgaCmM7vSw8DPlCFLoqFiqSQwWNlwTD3v_DBSgHJT5VF1zQGBpN_SA7yZVqLBaPtJeBd2exmfHZYpmc5UH6sWgFGvrIaXwStq7u5Lg1ECdiHNogUg4GqZaEkVpzAiy2DFq1w4Ux8se8xSujLIp0";

    }


}
