package org.Rekhta.utils;

import android.content.Context;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.Rekhta.R;

public class LanguageSelection {

    static private JSONObject dataObj;
    private String data;
    public LanguageSelection(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.rekhta);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int ctr;

        try {
            ctr = inputStream.read();
            while (ctr != -1) {
                byteArrayOutputStream.write(ctr);
                ctr = inputStream.read();
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i("Text Data", byteArrayOutputStream.toString());
        try {
            dataObj = new JSONObject(byteArrayOutputStream.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getSpecificLanguageWord( String id, String lang) throws JSONException {
        String data = "";

        try {
            JSONObject jObjectResult = null;
            jObjectResult = dataObj.getJSONObject(id);
            if(lang.startsWith("e")){
                data = jObjectResult.getString("eng");
            }else{
                data = jObjectResult.getString(lang);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }

    public static String getSpecificLanguageWordById( String id, int langCode) {

            String data = "";
        try {
            String langKey = "";
            if (langCode == 1) {
                langKey = "eng";
            } else if (langCode == 2) {
                langKey = "hin";
            } else {
                langKey = "urdu";
            }


            try {
                JSONObject jObjectResult = null;
                jObjectResult = dataObj.getJSONObject(id);
                if (langKey.startsWith("e")) {
                    data = jObjectResult.getString("eng");
                } else {
                    data = jObjectResult.getString(langKey);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }


}
