package org.Rekhta.utils;

import android.app.Activity;
import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;

import org.Rekhta.MainActivity;

import org.Rekhta.Broadcast.ConnectivityRecevier;
import org.Rekhta.MainActivity;
import org.Rekhta.db.database.AppDatabase;

/**
 * Created by Admin on 12/7/2017.
 */

public class MyApplicationClass extends Application implements Application.ActivityLifecycleCallbacks{


    private static MyApplicationClass myApplicationClass;
    public static Context mAppContext;
    private static AppDatabase db;

    ConnectivityRecevier connectivityRecevier;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        myApplicationClass = this;
        mAppContext = getApplicationContext();

        db = Room.databaseBuilder(this,
                AppDatabase.class, "database_name").build();

    }

    public static AppDatabase getDb(){
        return db;
    }

    public static synchronized Context getmAppContext() {
        return mAppContext;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        if (activity instanceof MainActivity)
        {

        }
    }
}
