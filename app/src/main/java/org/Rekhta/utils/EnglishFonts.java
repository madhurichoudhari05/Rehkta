package org.Rekhta.utils;


import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import org.Rekhta.activites.Dashboard;


public class EnglishFonts {


    static Typeface engLatoRegular, engLatoSemiBold, engMeriWheatherItalic,
            EngMeriWheatherBold, latoXBoldItalic, latoXregular;


    public static void setEngLatoRegular(TextView textView, Context context) {

        engLatoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/engLatoRegular.ttf");
        textView.setText((CharSequence) textView);

    }

    public static void setEngLatoSemiBold(TextView textView, Context context) {

        engLatoSemiBold = Typeface.createFromAsset(context.getAssets(), "fonts/LatoBold.ttf");
        textView.setText((CharSequence) textView);

    }

    public static void setEngMeriWheatherItalic(TextView textView, Context context) {

        engMeriWheatherItalic = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_light_italic.ttf");
        textView.setText((CharSequence) textView);

    }

    public static void setEngMeriWheatherBold(TextView textView, Context context) {

        EngMeriWheatherBold = Typeface.createFromAsset(context.getAssets(), "fonts/merriweather_bold.ttf");
        textView.setText((CharSequence) textView);

    }

    public static void setLatoXBoldItalic(TextView textView, Context context) {

        latoXBoldItalic = Typeface.createFromAsset(context.getAssets(), "fonts/LatoX-Bold.ttf");
        textView.setText((CharSequence) textView);

    }

    public static void setLatoXregular(TextView textView, Context context) {

        latoXregular = Typeface.createFromAsset(context.getAssets(), "fonts/LatoX-Regular.ttf");
        textView.setText((CharSequence) textView);

    }


}
