package org.Rekhta.fragments.SearchFragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.adapters.TotalContentAdapter;
import org.Rekhta.utils.LanguageSelection;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.adapters.SearchAdapter.AllAdapter;
import org.Rekhta.adapters.SearchAdapter.ContentAdapter;
import org.Rekhta.adapters.SearchAdapter.DictionaryAdapter;
import org.Rekhta.model.SearchModel.ContentModel;
import org.Rekhta.model.SearchModel.DictionaryModel;
import org.Rekhta.model.SearchModel.MorePoet;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class All extends Fragment {

    RecyclerView recyclerView, contentRecyclerView, dictionaryRecyclerView, totalContent;
    ArrayList<MorePoet> poetArrayList = new ArrayList<>();
    ArrayList<ContentModel> contentModelArrayList = new ArrayList<>();
    ArrayList<DictionaryModel> dictionaryModelArrayList = new ArrayList<>();

    AllAdapter allAdapter;
    ContentAdapter contentAdapter;
    DictionaryAdapter dictionaryAdapter;
    TotalContentAdapter totalContentAdapter;
    TextView showingText;
    String searchedText = "";

    BroadcastReceiver broadcastReceiver;

    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading = false;
    JSONArray contentData;
    JSONArray dictionaryData;
    NestedScrollView mainScrollview;
    private IntentFilter intentFilter;
    private TextView moreMatches;
    private TextView topFiveMatches;
    private TextView dicMatches, totalContentTxt;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.all_search, container, false);
        searchedText = ((SearchActivity) getActivity()).searchedText;
        if (!searchedText.equalsIgnoreCase("")) {
            // init();
            // contentlist();
            getAllData(searchedText);

        }
        mainScrollview = (NestedScrollView) view.findViewById(R.id.allSearchscrollview);
        mainScrollview.setVisibility(View.INVISIBLE);

        //TopFive section
        contentRecyclerView = (RecyclerView) view.findViewById(R.id.top_five_recyclerview);
        contentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contentAdapter = new ContentAdapter(contentModelArrayList, getActivity());
        totalContentAdapter = new TotalContentAdapter(contentModelArrayList, getActivity());
        contentRecyclerView.setAdapter(contentAdapter);

        //poet list
        recyclerView = (RecyclerView) view.findViewById(R.id.related_poet);
        allAdapter = new AllAdapter(poetArrayList, getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(allAdapter);


        //dictionary list
        dictionaryRecyclerView = (RecyclerView) view.findViewById(R.id.dictionary);
        dictionaryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dictionaryAdapter = new DictionaryAdapter(dictionaryModelArrayList, getActivity());
        dictionaryRecyclerView.setAdapter(dictionaryAdapter);


        //totalContent
        totalContent = (RecyclerView) view.findViewById(R.id.total_content);
        totalContent.setLayoutManager(new LinearLayoutManager(getActivity()));
        totalContent.setAdapter(totalContentAdapter);

        showingText = (TextView) view.findViewById(R.id.search_showingResultTv);
        moreMatches = (TextView) view.findViewById(R.id.moreMatches);
        topFiveMatches = (TextView) view.findViewById(R.id.topFiveMatches);
        dicMatches = (TextView) view.findViewById(R.id.dicMatches);
        totalContentTxt = (TextView) view.findViewById(R.id.totalContentTxt);
        showingText.setText(LanguageSelection.getSpecificLanguageWordById("Showing Result For", CommonUtil.languageCode) + " : " + searchedText);
        moreMatches.setText(LanguageSelection.getSpecificLanguageWordById("MORE MATCHES", CommonUtil.languageCode));
        topFiveMatches.setText(LanguageSelection.getSpecificLanguageWordById("Top 5 Matches", CommonUtil.languageCode));
        dicMatches.setText(LanguageSelection.getSpecificLanguageWordById("Dict Matches", CommonUtil.languageCode));
        totalContentTxt.setText(LanguageSelection.getSpecificLanguageWordById("Total Matches", CommonUtil.languageCode));


        recyclerView.setFocusable(false);
        recyclerView.setNestedScrollingEnabled(false);
        dictionaryRecyclerView.setFocusable(false);
        dictionaryRecyclerView.setNestedScrollingEnabled(false);
        contentRecyclerView.setFocusable(false);
        contentRecyclerView.setNestedScrollingEnabled(false);
        totalContent.setFocusable(false);
        totalContent.setNestedScrollingEnabled(false);


/*        totalContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Toast.makeText(getActivity(), "in last", Toast.LENGTH_SHORT).show();
                if (!recyclerView.canScrollVertically(1)) {
                    if(totalNumberOfContent <= 20){

                    }else{
                        if(pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getPagingDataOnDemand(searchedText);
                        }
                    }
                }
            }
        });*/
        mainScrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (mainScrollview != null) {
                    if (mainScrollview.getChildAt(0).getBottom() <= (mainScrollview.getHeight() + mainScrollview.getScrollY())) {
                        //scroll view is at bottom

//                        Toast.makeText(getActivity(), "in last", Toast.LENGTH_SHORT).show();

                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getPagingDataOnDemand(searchedText);
                        }


                    } else {
                        //scroll view is not at bottom
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


        intentFilter = new IntentFilter("searchBroadcast");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                pageIndex = 1;

                if (!searchedText.equalsIgnoreCase("")) {
                    // init();
                    // contentlist();
                    getAllData(searchedText);

                }

            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("searchBroadcast"));

    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    /*  public void init() {
        for (int i = 0; i < 10; i++) {
           // MorePoet morePoet = new MorePoet("Mirza Ghalib", R.drawable.blank_img);
           // poetArrayList.add(morePoet);

        }
    }

    public void contentlist() {
        for (int i = 0; i < 5; i++) {
            //ContentModel contentModel = new ContentModel("Sher", "Kuch to pa Dhiye ki log khate hain", "Mirza Ghalib");
            //contentModelArrayList.add(contentModel);

        }
    }*/

   /* public void dictionaryList() {
        for (int i = 0; i < 5; i++) {
            DictionaryModel dictionaryModel = new DictionaryModel("Ghalib", "ग़ालिब", "غاؔلب", "Name Of Great Poet");
            dictionaryModelArrayList.add(dictionaryModel);

        }
    }*/

    public void getAllData(String keyWord) {

        poetArrayList.clear();
        contentModelArrayList.clear();
        dictionaryModelArrayList.clear();

        //poetArrayList = new ArrayList<>();
        //contentModelArrayList = new ArrayList<>();
        //dictionaryModelArrayList = new ArrayList<>();

        contentData = new JSONArray();
        dictionaryData = new JSONArray();

        RestClient.get().getAllSearchedData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, keyWord, CommonUtil.languageCode, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getString("Message").equalsIgnoreCase("success")) {

                        for (int i = 0; i < obj.getJSONArray("Poets").length(); i++) {
                            poetArrayList.add(new MorePoet(obj.getJSONArray("Poets").getJSONObject(i).getString("EntityId"), obj.getJSONArray("Poets").getJSONObject(i).getString("Name"), obj.getJSONArray("Poets").getJSONObject(i).getInt("GazalCount"),
                                    obj.getJSONArray("Poets").getJSONObject(i).getBoolean("IsNew"), obj.getJSONArray("Poets").getJSONObject(i).getInt("NazmCount"), obj.getJSONArray("Poets").getJSONObject(i).getString("SEO_Slug"), obj.getJSONArray("Poets").getJSONObject(i).getString("ImageUrl")));

                        }

                        contentData = obj.getJSONArray("Content");
                        totalNumberOfContent = obj.getInt("ContentTotal");
                        for (int i = 0; i < contentData.length(); i++) {
                            contentModelArrayList.add(new ContentModel(contentData.getJSONObject(i).getString("Body")
                                    , contentData.getJSONObject(i).getString("Id")
                                    , contentData.getJSONObject(i).getString("TypeId")
                                    , contentData.getJSONObject(i).getString("ContentSlug")
                                    , contentData.getJSONObject(i).getString("PoetId")
                                    , contentData.getJSONObject(i).getString("PoetSlug")
                                    , contentData.getJSONObject(i).getString("PoetName")
                                    , contentData.getJSONObject(i).getString("ImageUrl")
                                    , contentData.getJSONObject(i).getString("ContentUrl")
                                    , contentData.getJSONObject(i).getInt("AudioCount")
                                    , contentData.getJSONObject(i).getInt("VideoCount")
                                    , contentData.getJSONObject(i).getBoolean("EditorChoice")
                                    , contentData.getJSONObject(i).getBoolean("PopularChoice")
                                    , contentData.getJSONObject(i).getInt("ShortUrlIndex")
                                    , contentData.getJSONObject(i).getString("Title")

                            ));
                        }
                        dictionaryData = obj.getJSONArray("Dictionary");
                        for (int i = 0; i < dictionaryData.length(); i++) {

                            dictionaryModelArrayList.add(new DictionaryModel(
                                    dictionaryData.getJSONObject(i).getString("Id"),
                                    dictionaryData.getJSONObject(i).getString("Urdu"),
                                    dictionaryData.getJSONObject(i).getString("Hindi"),
                                    dictionaryData.getJSONObject(i).getString("English"),
                                    dictionaryData.getJSONObject(i).getString("Meaning1_En"),
                                    dictionaryData.getJSONObject(i).getString("Meaning2_En"),
                                    dictionaryData.getJSONObject(i).getString("Meaning3_En"),
                                    dictionaryData.getJSONObject(i).getString("Meaning1_Hi"),
                                    dictionaryData.getJSONObject(i).getString("Meaning2_Hi"),
                                    dictionaryData.getJSONObject(i).getString("Meaning3_Hi"),
                                    dictionaryData.getJSONObject(i).getString("Meaning1_Ur"),
                                    dictionaryData.getJSONObject(i).getString("Meaning2_Ur"),
                                    dictionaryData.getJSONObject(i).getString("Meaning3_Ur")
                            ));

                        }

                        allAdapter.notifyDataSetChanged();
                        dictionaryAdapter.notifyDataSetChanged();
                        contentAdapter.notifyDataSetChanged();
                        totalContentAdapter.notifyDataSetChanged();

                        mainScrollview.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


    }

    public void getPagingDataOnDemand(String keyWord) {
        contentModelArrayList.clear();
        isDataLoading = true;
        final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);

        RestClient.get().getSearchAllLoadOnDemandData(CommonUtil.SharedPrefKeys.contentType,
                CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, keyWord,
                CommonUtil.languageCode, pageIndex, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {
                        if (progressLoader != null && progressLoader.isShowing())
                            progressLoader.dismiss();
                        JSONObject obj = new JSONObject(res);
                        System.out.print(res.toString());
                        try {
                            if (obj.getString("Message").equalsIgnoreCase("success")) {

                                contentData = CommonUtil.concatArray(contentData, obj.getJSONArray("Content"));
                                totalNumberOfContent = obj.getInt("ContentTotal");
                                Log.e("------", contentData.length() + "");
                                for (int i = 0; i < contentData.length(); i++) {
                                    contentModelArrayList.add(new ContentModel(contentData.getJSONObject(i).getString("Body")
                                            , contentData.getJSONObject(i).getString("Id")
                                            , contentData.getJSONObject(i).getString("TypeId")
                                            , contentData.getJSONObject(i).getString("ContentSlug")
                                            , contentData.getJSONObject(i).getString("PoetId")
                                            , contentData.getJSONObject(i).getString("PoetSlug")
                                            , contentData.getJSONObject(i).getString("PoetName")
                                            , contentData.getJSONObject(i).getString("ImageUrl")
                                            , contentData.getJSONObject(i).getString("ContentUrl")
                                            , contentData.getJSONObject(i).getInt("AudioCount")
                                            , contentData.getJSONObject(i).getInt("VideoCount")
                                            , contentData.getJSONObject(i).getBoolean("EditorChoice")
                                            , contentData.getJSONObject(i).getBoolean("PopularChoice")
                                            , contentData.getJSONObject(i).getInt("ShortUrlIndex")
                                            , contentData.getJSONObject(i).getString("Title")

                                    ));
                                }
                                contentAdapter.notifyDataSetChanged();
                                isDataLoading = false;
                                mainScrollview.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            isDataLoading = false;
                            if (progressLoader != null && progressLoader.isShowing())
                                progressLoader.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        isDataLoading = false;
                        Log.e("Error", error.toString());
                    }
                });


    }
}
