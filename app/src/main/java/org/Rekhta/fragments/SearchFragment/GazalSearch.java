package org.Rekhta.fragments.SearchFragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.adapters.SearchAdapter.ContentAdapter;
import org.Rekhta.adapters.SearchAdapter.GhazalSearchAdapter;
import org.Rekhta.model.SearchModel.ContentModel;
import org.Rekhta.model.SearchModel.GhazalSearchModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class GazalSearch extends Fragment {

    private RecyclerView recyclerView;
    ArrayList<GhazalSearchModel> ghazalSearchModels = new ArrayList<>();
    ContentModel contentModel;
    GhazalSearchAdapter ghazalSearchAdapter;


    int pageIndex = 1;
    int typeOfContent = 3; // for Ghazal
    int totalNumberOfContent;
    JSONArray contentArray;
    boolean isDataLoading = false;
    String searchedText = "";
    private BroadcastReceiver broadcastReceiver;
    private ProgressDialog progressLoader;

    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.gazal_search, container, false);
        searchedText = ((SearchActivity) getActivity()).searchedText;

        recyclerView = (RecyclerView) view.findViewById(R.id.gazal_search);
        ghazalSearchAdapter = new GhazalSearchAdapter(ghazalSearchModels, getActivity());
        recyclerView.setAdapter(ghazalSearchAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (!searchedText.equalsIgnoreCase("")) {
            getSearchedGhazal(searchedText);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    if (totalNumberOfContent <= 20) {

                    } else {
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getSearchedGhazal(searchedText);
                        }
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter("searchBroadcast");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                pageIndex = 1;
                ghazalSearchModels.clear();
                if (!searchedText.equalsIgnoreCase("")) {
                    getSearchedGhazal(searchedText);
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    public void getSearchedGhazal(String keyWord) {
        isDataLoading = true;
        //ghazalSearchModels.clear();
        contentArray = new JSONArray();
        progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);
        RestClient.get().getSearchContentByTypePageWiseData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                CommonUtil.SharedPrefKeys.rekhtasAccessToken,
                keyWord, CommonUtil.languageCode, pageIndex, typeOfContent, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {
                        JSONObject obj = new JSONObject(res);
                        System.out.print(res.toString());
                        try {
                            if (obj.getString("Message").equalsIgnoreCase("success")) {
                                totalNumberOfContent = obj.getInt("ResultsTotal");
                                contentArray = obj.getJSONArray("Results");
                                //contentArray = CommonUtil.concatArray(contentArray, obj.getJSONArray("Results"));

                                for (int i = 0; i < contentArray.length(); i++) {

                                    ghazalSearchModels.add(new GhazalSearchModel(
                                            contentArray.getJSONObject(i).getString("Body"), contentArray.getJSONObject(i).getString("Id"),
                                            contentArray.getJSONObject(i).getString("TypeId"), contentArray.getJSONObject(i).getString("ContentSlug"),
                                            contentArray.getJSONObject(i).getString("PoetId"), contentArray.getJSONObject(i).getString("PoetSlug"),
                                            contentArray.getJSONObject(i).getString("PoetName"), contentArray.getJSONObject(i).getString("ImageUrl"),
                                            contentArray.getJSONObject(i).getString("ContentUrl"), contentArray.getJSONObject(i).getInt("AudioCount"),
                                            contentArray.getJSONObject(i).getInt("VideoCount"), contentArray.getJSONObject(i).getBoolean("EditorChoice"),
                                            contentArray.getJSONObject(i).getBoolean("PopularChoice"), contentArray.getJSONObject(i).getInt("ShortUrlIndex"),
                                            contentArray.getJSONObject(i).getString("Title")));
                                }
                                ghazalSearchAdapter.notifyDataSetChanged();
                                isDataLoading = false;
                            }
                            isDataLoading = false;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (progressLoader != null && progressLoader.isShowing())
                            progressLoader.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        isDataLoading = false;
                        if (progressLoader != null && progressLoader.isShowing())
                            progressLoader.dismiss();
                        Log.e("error", error.toString());
                    }
                });

    }
}
