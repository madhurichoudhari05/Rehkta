package org.Rekhta.fragments.SearchFragment;


import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.Rekhta.Services.GhazalContentService;
import org.Rekhta.activites.AudioActivity;
import org.Rekhta.activites.MyFavoritesActivity;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.activites.SettingActivity;
import org.Rekhta.utils.LanguageSelection;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.Rekhta.R;
import org.Rekhta.activites.ExScrollView;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.activites.ViewActivity;
import org.Rekhta.activites.View_ProfileActivity;
import org.Rekhta.activites.ZoomView;
import org.Rekhta.fonts.LatoBoldTextView;
import org.Rekhta.fonts.MerriweatherBoldTextView;
import org.Rekhta.fragments.AudioFragment;
import org.Rekhta.fragments.YoutubeFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import org.Rekhta.views.MeaningFragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchGhazalContentRendering extends Fragment {

    View view;
    TextView t[];
    LinearLayout linearL;
    ExScrollView vScrollWrapper;
    LinearLayout linearWrapper;
    ZoomView zoomView;
    Toolbar toolbar;
    TextView toolbarTV;
    ImageView backButton;
    ImageView editorChoiceImg, popularchoiceImg, authorImage;
    LatoBoldTextView view_gazals, view_profile;
    Dialog dialog;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    SharedPreferences DataPrefs = null;
    JSONObject jsonData;
    JSONObject prevData;
    JSONObject nextData;
    ImageView footor_dotsIcon, footor_audioIcon, footor_videoIcon, footor_shareIcon, footor_infoIcon, footor_favIcon, footer_default;
    boolean isFav = false;
    int positionClicked = 0;
    Typeface engtf;
    Typeface hinditf;
    Typeface urdutf;
    JSONObject data;
    TextView nextTiltle, nextAuthorName, prevTitle, prevAuthorName;
    LinearLayout nextLinaerlayoutview, prevLinaerlayoutview;
    View didvider_nextandprev;
    String audioUrl = "";
    JSONArray videoArrayData;
    JSONArray audioArrayData;
    BottomSheetDialog bottomSheetDialog;
    MerriweatherBoldTextView detailedAuthorName;
    WindowManager.LayoutParams wlp;
    boolean showCritiqueDialog = true;
    boolean isCritiqueModeOn = false;
    FrameLayout ghazal_framelayout;
    TextView viewProfile, viewGazal;
    float sizeoFtextShouldBe = 100;
    String ghazalId = "";
    public float textSixeToRender;
    boolean renderComplete;
    private String ghaltitle;
    private String poetName;
    private JSONObject mainContent;
    private String poetId;
    private ImageView renderNextArrow, renderPrevArrow;
    private TextView nextItemTxt, prevItemTxt;

    public SearchGhazalContentRendering() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ghazalId = ((SearchActivity) getActivity()).idGhazalToRender;
        setRetainInstance(true);
      /*  view = inflater.inflate(R.layout.fragment_sher_ghal_content, container, false);
        return view;*/
        FrameLayout frameLayout = new FrameLayout(getActivity());
        populateViewForOrientation(inflater, frameLayout);
        return frameLayout;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        populateViewForOrientation(inflater, (ViewGroup) getView());
    }

    private void populateViewForOrientation(LayoutInflater inflater, ViewGroup viewGroup) {
        viewGroup.removeAllViewsInLayout();
        view = inflater.inflate(R.layout.fragment_sher_ghal_content, viewGroup);
        initViews();
        // Find your buttons in subview, set up onclicks, set up callbacks to your parent fragment or activity here.
    }

    public void initViews() {
        ghazal_framelayout = (FrameLayout) view.findViewById(R.id.ghazal_frameLayout);
        toolbar = (Toolbar) view.findViewById(R.id.toolbarfrag);
        toolbar.inflateMenu(R.menu.home_screen);
        zoomView = (ZoomView) view.findViewById(R.id.zoomlayout);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_settings:
                        showRightMenuLangSelectionOption();
                        return true;
                    case R.id.search:
                        getActivity().startActivity(new Intent(getActivity(), SearchActivity.class));
                        return true;
                    default:

                }
                return false;
            }
        });

        initRightMenuPopUpDialog();
        CommonUtil.mainContentScrollView = (ScrollView) view.findViewById(R.id.mainScrollContentView);
        backButton = (ImageView) view.findViewById(R.id.backButton);
        CommonUtil.setBackButton(getActivity(), backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        authorImage = (ImageView) view.findViewById(R.id.authorImg);
        editorChoiceImg = (ImageView) view.findViewById(R.id.editorChoiceImg);
        popularchoiceImg = (ImageView) view.findViewById(R.id.popularChoiceIcon);
        toolbarTV = (TextView) toolbar.findViewById(R.id.toobartextview);
        linearL = (LinearLayout) view.findViewById(R.id.mainContent);


        linearL = zoomView.Wrapper;

        view_gazals = (LatoBoldTextView) view.findViewById(R.id.view_gazals);
        view_profile = (LatoBoldTextView) view.findViewById(R.id.view_profile);


        view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PoetDetailedActivity.class);
                intent.putExtra("poetId", poetId);
                intent.putExtra("ghazalCount", "");
                intent.putExtra("nazmCount", "");
                intent.putExtra("sherCount", "");
                intent.putExtra("shortDescInEng", " ");
                intent.putExtra("shortDescInHin", " ");
                intent.putExtra("shortDescInUrdu", " ");
                intent.putExtra("shouldLandOnProfile", true);
                startActivity(intent);
            }
        });
        view_gazals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PoetDetailedActivity.class);
                try {
                    intent.putExtra("poetId", poetId);
                    intent.putExtra("ghazalCount", "");
                    intent.putExtra("nazmCount", "");
                    intent.putExtra("sherCount", "");
                    intent.putExtra("shortDescInEng", " ");
                    intent.putExtra("shortDescInHin", " ");
                    intent.putExtra("shortDescInUrdu", " ");
                    intent.putExtra("shouldLandonprofile", false);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getGhazal();

        initFooterView(view);
    }


    private void setLangData() {


        nextItemTxt.setText(LanguageSelection.getSpecificLanguageWordById("Next", CommonUtil.languageCode));
        prevItemTxt.setText(LanguageSelection.getSpecificLanguageWordById("Previous", CommonUtil.languageCode));

        view_gazals.setText(LanguageSelection.getSpecificLanguageWordById("see All Ghazals", CommonUtil.languageCode));
        view_profile.setText(LanguageSelection.getSpecificLanguageWordById("View profile", CommonUtil.languageCode));

        if (CommonUtil.languageCode == 3) {
            renderNextArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_left));
            renderPrevArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_right));
        } else {
            renderNextArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_right));
            renderPrevArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_left));
        }


    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            // restartActivity();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //restartActivity();
        }
    }

    private void initFooterView(View v) {
        try {

            didvider_nextandprev = (View) v.findViewById(R.id.didvider_nextandprev);
            nextLinaerlayoutview = (LinearLayout) v.findViewById(R.id.nextLinearLayout);
            nextTiltle = (TextView) v.findViewById(R.id.next_gazal_title);
            nextAuthorName = (TextView) v.findViewById(R.id.next_author_name);

            prevLinaerlayoutview = (LinearLayout) v.findViewById(R.id.prevLinearLayout);
            prevTitle = (TextView) v.findViewById(R.id.prev_title);
            prevAuthorName = (TextView) v.findViewById(R.id.prev_authorName);

            renderNextArrow = (ImageView) v.findViewById(R.id.renderNextarrow);
            renderPrevArrow = (ImageView) v.findViewById(R.id.renderPrevArrow);
            nextItemTxt = (TextView) v.findViewById(R.id.nextItemTxt);
            prevItemTxt = (TextView) v.findViewById(R.id.prevItemTxt);

            setLangData();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getGhazal() {
        final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);
        //Toast.makeText(getActivity(), "calling", Toast.LENGTH_SHORT).show();
        try {

            ghazal_framelayout.setVisibility(View.VISIBLE);
            RestClient.get().getContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.languageCode, ghazalId, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    if (progressLoader != null && progressLoader.isShowing())
                        progressLoader.dismiss();
                    try {
                        JSONObject obj = new JSONObject(res);
                        if (obj.getInt("S") == 1) {
                            JSONObject obj1 = new JSONObject(res);
                            poetId = obj1.getJSONObject("R").getJSONObject("Poet").getString("PI");
                            ghazal_framelayout.setVisibility(View.GONE);
                            textSixeToRender = 14.5f;
                            pumpData(obj1, CommonUtil.languageCode);
                            renderComplete = true;
                            //calculateTextSize(obj1);
                        } else {
                            Toast.makeText(getActivity(), "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


                @Override
                public void failure(RetrofitError error) {
                    if (progressLoader != null && progressLoader.isShowing())
                        progressLoader.dismiss();
                    Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pumpData(JSONObject obj, int langcode) throws JSONException {

        Typeface engtf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/MerriweatherExtended-LightItalic.ttf");
        Typeface hinditf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Laila-Regular.ttf");
        Typeface urdutf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

        TextView ghazalTitle = (TextView) view.findViewById(R.id.gazal_title);
        TextView author_name = (TextView) view.findViewById(R.id.author_name);

        JSONObject mainObj = obj.getJSONObject("R");
        getBottomOfGhazal(mainObj.getString("I"));

        ghaltitle = mainObj.getString("CT");
        poetName = ((SearchActivity) getActivity()).currentGhazalPoetName;
        ghazalTitle.setText(ghaltitle);
        //author_name.setText(mainObj.getJSONObject("Poet").getString("CS").toUpperCase().replace("-", " "));
        detailedAuthorName = (MerriweatherBoldTextView) view.findViewById(R.id.detaied_authorName);
        if (CommonUtil.languageCode == 1) {
            detailedAuthorName.setText(((SearchActivity) getActivity()).currentGhazalPoetName);
            author_name.setText(((SearchActivity) getActivity()).currentGhazalPoetName);
            forceLTRIfSupported();
        } else if (CommonUtil.languageCode == 2) {
            detailedAuthorName.setText(mainObj.getString("TN"));
            author_name.setText(mainObj.getString("TN"));
            forceLTRIfSupported();
        } else if (CommonUtil.languageCode == 3) {
            detailedAuthorName.setText(mainObj.getString("TN"));
            author_name.setText(mainObj.getString("TN"));
            forceRTLIfSupported();
        } else {
            detailedAuthorName.setText(mainObj.getString("TN"));
            author_name.setText(mainObj.getString("TN"));
            forceLTRIfSupported();
        }
        // detailedAuthorName.setText(mainObj.getJSONObject("Poet").getString("CS").toUpperCase().replace("-", " "));
        toolbarTV.setText(mainObj.getString("CT"));

        audioArrayData = new JSONArray(mainObj.getJSONArray("Audios").toString());
        videoArrayData = new JSONArray(mainObj.getJSONArray("Videos").toString());
        initOptionAtFooter();


        Glide.with(getActivity())
                .load("https://rekhta.org" + mainObj.getJSONObject("Poet").getString("IU"))
                .into(authorImage);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearL.removeAllViews();


        //Set layout parameter for vScrollWrapper
        vScrollWrapper = new ExScrollView(getActivity());
        vScrollWrapper.setLayoutParams(new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.WRAP_CONTENT));
        vScrollWrapper.setHorizontalScrollBarEnabled(false);
        vScrollWrapper.setVerticalScrollBarEnabled(false);

        linearWrapper = new LinearLayout(getActivity());
        //  linearWrapper.setBackgroundColor(Color.parseColor("#BFE8BB"));
        linearWrapper.setOrientation(LinearLayout.VERTICAL);
        linearWrapper.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearWrapper.setClipChildren(false);
        linearWrapper.setPadding(4, 0, 4, 0);


        //linearWrapper.setGravity(Gravity.CENTER);
        //Declare minimum gap
        int minGap = 4;
        int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        mainContent = new JSONObject(mainObj.getString("CR"));

        JSONArray Parray = mainContent.getJSONArray("P");

        String line = "";
        for (int i = 0; i < Parray.length(); i++) {

            JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
            for (int j = 0; j < Larray.length(); j++) {

                LinearLayout linear = new LinearLayout(getActivity());
                linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                linear.setOrientation(LinearLayout.HORIZONTAL);
                //   linear.setBackgroundColor(Color.parseColor("#B2E8FF"));
                // linear.setPadding(0, 8, 0, 8);

                if (CommonUtil.languageCode != 3) {
                    if (j % 2 == 0) {
                        linear.setPadding(0, 12, 0, 0);
                    } else {
                        linear.setPadding(0, 0, 0, 20);
                    }
                } else {
                    if (j % 2 == 0) {
                        linear.setPadding(0, 12, 0, 0);
                    } else {
                        linear.setPadding(0, 4, 0, 12);
                    }
                }


                JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                t = new TextView[Warray.length()];

                if (langcode != 3) {
                    for (int k = 0; k < Warray.length(); k++) {

                        t[j] = new TextView(getActivity());
                        t[j].setLayoutParams(dim);
                        t[j].setSingleLine(true);
                        t[j].setIncludeFontPadding(false);

                        t[j].setText(" " + Warray.getJSONObject(k).getString("W"));
//

                        t[j].setTextColor(Color.parseColor("#000000"));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            t[j].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        }

                        if (CommonUtil.languageCode == 1) {
                            CommonUtil.setEnglishMerriwetherFont(getActivity(), t[j]);
                            t[j].setTypeface(engtf);
                            t[j].setTextSize(14);
                        } else {
                            t[j].setTextSize(16);
                            t[j].setTypeface(hinditf);

                        }

                        assignClickToText(t[j], Warray.getJSONObject(k), i, Parray.getJSONObject(i).getJSONArray("L"), j);
                        linear.addView(t[j]);
                    }
                } else {
                    forceRTLIfSupported();
                    for (int k = 0; k < Warray.length(); k++) {
                        t[j] = new TextView(getActivity());
                        t[j].setLayoutParams(dim);
                        t[j].setText(" " + Warray.getJSONObject(k).getString("W"));
                        t[j].setGravity(Gravity.CENTER);
                        t[j].setTextSize(16);
                        t[j].setIncludeFontPadding(true);
                        t[j].setTypeface(urdutf);

                        t[j].setTextColor(Color.parseColor("#000000"));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            t[j].setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        }
                        t[j].setTypeface(urdutf);

                        assignClickToText(t[j], Warray.getJSONObject(k), i, Parray.getJSONObject(i).getJSONArray("L"), j);
                        linear.addView(t[j]);
                    }
                }

                linearWrapper.addView(linear);

                linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                // int widthSpec = View.MeasureSpec.makeMeasureSpec(linearL.getWidth(), View.MeasureSpec.EXACTLY);
                // int heightSpec = View.MeasureSpec.makeMeasureSpec(linearL.getHeight(), View.MeasureSpec.EXACTLY);
                //linear.measure(widthSpec, heightSpec);
            }
        }

        vScrollWrapper.addView(linearWrapper);
        zoomView.addView(vScrollWrapper);
    }

    public void widhtOfView() {

        int curWidthDiff = 0;
        int childCount = linearWrapper.getChildCount();
        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = linearWrapper.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                curWidthDiff = linearWrapper.getWidth() - lineView.getWidth();
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    Log.v("NewPadding", "textView = " + ((TextView) textView).getText().toString() + " and padding =" + String.valueOf(extraPadding));
                    if (textView instanceof TextView) {
                        if (CommonUtil.languageCode == 3) {
                            textView.setPadding(extraPadding, 0, 0, 16);
                        } else {
                            textView.setPadding(4, 0, extraPadding, 0);
                        }
                    }
                }

                //TextView
            }
        }
//        linearWrapper.setPadding(0,0,linearWrapper.getWidth()*4,0);
        renderComplete = false;
        float scaleF = (float) zoomView.getWidth() / (float) linearWrapper.getWidth();
        //1.85624
//        linearWrapper.setMinimumWidth(linearWrapper.getWidth()*3);
        int requiredHeight = (int) (scaleF * linearWrapper.getMeasuredHeight());
        zoomView.setWrapperHeight(requiredHeight, (int) (linearWrapper.getMeasuredHeight()));
//        zoomView.setWrapperHeight(linearWrapper.getMeasuredHeight(),linearWrapper.getMeasuredHeight());
        zoomView.setWrapperChildWidth(linearWrapper.getWidth());

        if (CommonUtil.languageCode != 3) {
            linearWrapper.setPivotY(0);
            linearWrapper.setPivotX(0);

            linearWrapper.setScaleX((float) (scaleF - 0.15));
            linearWrapper.setScaleY((float) (scaleF - 0.15));
        } else {

            int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
            int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
            int g = screenWidth / 2;
            int h = screenHeight / 2;
            linearWrapper.setPivotY(0);
            linearWrapper.setPivotX(linearWrapper.getWidth());

            linearWrapper.setScaleX((float) (scaleF - 0.08));
            linearWrapper.setScaleY((float) (scaleF - 0.08));

            // float myScale = scaleF / 10;
            //linearWrapper.setScaleX(1);
            //linearWrapper.setScaleY(1);
        }
    }

    public void assignClickToText(final TextView txt, final JSONObject obj, final int i, final JSONArray Larray, final int lineNumber) {

        txt.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                try {
                    shareParaGraphText(Larray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;    // set to true
            }
        });

        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int paraNumber = i;
                int clickedLineNo;
                try {
                    if (isCritiqueModeOn) {
                        String ghazalline = getLineFromTheParagraph(Larray, lineNumber);
                        if (lineNumber == 0) {
                            clickedLineNo = paraNumber + lineNumber + 1 + paraNumber;
                        } else {
                            clickedLineNo = paraNumber + lineNumber + 2 + (paraNumber - 1);
                        }

                        // Toast.makeText(getActivity(),clickedLineNo +"--"+ghazalline, Toast.LENGTH_SHORT).show();
                        ShowCritiqueSubmitForm(clickedLineNo, ghazalline);
                    } else {
                        ShowAlert(txt, obj);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void shareParaGraphText(JSONArray Larray) throws JSONException {
        String paratoShare = Paragraph(Larray);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = paratoShare;
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "https://Rekhta.org");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }

    private String Paragraph(JSONArray Larray) throws JSONException {
        String lines = "";
        for (int j = 0; j < Larray.length(); j++) {
            JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
            t = new TextView[Warray.length()];
            lines = lines + "\n";
            if (CommonUtil.languageCode != 3) {
                for (int k = 0; k < Warray.length(); k++) {
                    lines = lines + " " + Warray.getJSONObject(k).getString("W");
                }
            } else {
                for (int k = Warray.length() - 1; k >= 0; k--) {
                    lines = lines + " " + Warray.getJSONObject(k).getString("W");
                }
            }
        }
        return lines;
    }

    public void giveColorToSelectedText(TextView txt) {
        txt.setBackgroundColor(Color.parseColor("#eb0045"));
    }

    public void removeColorToSelectedText(TextView txt) {
        txt.setBackgroundColor(Color.parseColor("#FFFFFF"));
    }

    public void showMeaning(String text, JSONObject obj) {


        try {
            getMeaningOfTheWord(text, obj.getString("M"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void ShowAlert(final TextView mainText, JSONObject obj) throws JSONException {
        //giveColorToSelectedText(mainText);
        MeaningFragment meaningFragment = (MeaningFragment) getActivity().getSupportFragmentManager().findFragmentByTag("meaningFrag");
        if (meaningFragment != null && meaningFragment.isVisible()) {
            getActivity().onBackPressed();
            showMeaning(mainText.getText().toString(), obj);

        } else {
            showMeaning(mainText.getText().toString(), obj);
        }
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        DataPrefs = getActivity().getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);
        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                setLangData();
                dialog.dismiss();
                CommonUtil.setBackButton(getActivity(), backButton);
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                setLangData();
                dialog.dismiss();
                CommonUtil.setBackButton(getActivity(), backButton);
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                setLangData();
                dialog.dismiss();
                CommonUtil.setBackButton(getActivity(), backButton);

            }
        });
    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {


        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);
        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }

        if (CommonUtil.languageCode != langCode) {
            CommonUtil.languageCode = langCode;
            saveTheLangCodeLocally();
            getGhazal();
        }

        dialog.dismiss();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void updateNextAndPrevoiusText() {


        try {
            if (nextData.length() == 0) {
                nextLinaerlayoutview.setVisibility(View.GONE);
                didvider_nextandprev.setVisibility(View.GONE);
            } else {
                nextLinaerlayoutview.setVisibility(View.VISIBLE);
                didvider_nextandprev.setVisibility(View.VISIBLE);
                String authorName = "";
                String ghazalTitle = "";
                if (CommonUtil.languageCode == 1) {
                    ghazalTitle = nextData.getString("TE");
                    authorName = nextData.getString("PE");
                } else if (CommonUtil.languageCode == 2) {
                    ghazalTitle = nextData.getString("TH");
                    authorName = nextData.getString("PH");
                } else {
                    ghazalTitle = nextData.getString("TU");
                    authorName = nextData.getString("PU");
                }
                nextTiltle.setText(ghazalTitle);
                nextAuthorName.setText(authorName);
            }


            if (prevData.length() == 0) {
                prevLinaerlayoutview.setVisibility(View.GONE);
                didvider_nextandprev.setVisibility(View.GONE);
            } else {
                prevLinaerlayoutview.setVisibility(View.VISIBLE);
                String authorName = "";
                String ghazalTitle = "";
                if (CommonUtil.languageCode == 1) {
                    ghazalTitle = prevData.getString("TE");
                    authorName = prevData.getString("PE");
                } else if (CommonUtil.languageCode == 2) {
                    ghazalTitle = prevData.getString("TH");
                    authorName = prevData.getString("PH");
                } else {
                    ghazalTitle = prevData.getString("TU");
                    authorName = prevData.getString("PU");
                }
                prevTitle.setText(ghazalTitle);
                prevAuthorName.setText(authorName);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        final ViewTreeObserver observer = linearL.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (renderComplete) {
                    widhtOfView();
                }
            }
        });

    }

    public void initOptionAtFooter() {
        footor_dotsIcon = (ImageView) view.findViewById(R.id.footor_threedots);
        footor_audioIcon = (ImageView) view.findViewById(R.id.footor_audio);
        footor_videoIcon = (ImageView) view.findViewById(R.id.footor_video);
        footer_default = (ImageView) view.findViewById(R.id.footer_default);
        footer_default.setVisibility(View.GONE);
        footor_infoIcon = (ImageView) view.findViewById(R.id.footer_info);
        footor_dotsIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCriticOptionFooter();
            }
        });
        footor_shareIcon = (ImageView) view.findViewById(R.id.footer_share);
        footor_favIcon = (ImageView) view.findViewById(R.id.fooor_heart);

        if (CommonUtil.isContentUsersFav(ghazalId)) {
            footor_favIcon.setImageResource(R.drawable.ic_favorited);
            footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        }

        footor_favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(getActivity());
                } else {
                    if (CommonUtil.isContentUsersFav(ghazalId)) {
                        CommonUtil.removeFromFavTroughApi(getActivity(), ghazalId);
                        footor_favIcon.setImageResource(R.drawable.ic_favorite);
                        footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                    } else {

                        String title;
                        String auther;


                        Intent intent = new Intent(getActivity(), GhazalContentService.class);

                        intent.putExtra("ghazalTitle", ghaltitle);
                        intent.putExtra("ghazalAuther", poetName);
                        intent.putExtra("jsonObject", mainContent.toString());

                        getActivity().startService(intent);

                        CommonUtil.addToFavTroughApi(getActivity(), ghazalId);
                        footor_favIcon.setImageResource(R.drawable.ic_favorited);
                        footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }
                }


            }
        });
        final boolean[] isaudioPlaying = {false};
        try {
            if (videoArrayData.length() == 0) {
                footor_videoIcon.setImageResource(0);
                footor_audioIcon.setOnClickListener(null);
            } else {
                footor_videoIcon.setImageResource(R.drawable.ic_video);
                footor_videoIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Bundle bundle = new Bundle();
                        bundle.putString("videoObj", videoArrayData.toString());
                        bundle.putString("cActivity", "SearchActivity");
                        YoutubeFragment youtubeFragment = new YoutubeFragment();
                        youtubeFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.mainView, youtubeFragment, "videoFrag")
                                .addToBackStack(null)
                                .commit();

                    }
                });
            }


            if (audioArrayData.length() == 0) {
                footor_audioIcon.setImageResource(0);
                footor_audioIcon.setOnClickListener(null);
                footor_audioIcon.setVisibility(View.GONE);
                footer_default.setVisibility(View.VISIBLE);
            } else {
                footor_audioIcon.setImageResource(R.drawable.ic_audio);
                footor_audioIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            /*Bundle bundle = new Bundle();
                            bundle.putString("audioArray", audioArrayData.toString());
                            AudioFragment audioFragment = new AudioFragment();
                            audioFragment.setArguments(bundle);
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.mainView, audioFragment, "audioFrag")
                                    .addToBackStack(null)
                                    .commit();*/

                            Intent intent = new Intent(getActivity(), AudioActivity.class);
                            intent.putExtra("audioArray", audioArrayData.toString());
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        footor_shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareThroughUrl();
                // ShowThankYouForCritique();
            }
        });
        //player = new MediaPlayer();

    }

    public void getMeaningOfTheWord(final String mainWord, String word) {
        //getMeaningOfWord
        final ProgressDialog loading = ProgressDialog.show(getActivity(), null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        MeaningFragment meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.mainView, meaningFragment, "meaningFrag")
                                .addToBackStack(null)
                                .commit();

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });
    }

    View mainFooter;
    View critiqueFooter;
    View critiqueFooterWhenOn;

    public void showCriticOptionFooter() {
        mainFooter = view.findViewById(R.id.mainFooterContent);
        mainFooter.setVisibility(View.GONE);
        critiqueFooter = view.findViewById(R.id.critiqueFooter);
        critiqueFooter.setVisibility(View.VISIBLE);
        critiqueFooterWhenOn = view.findViewById(R.id.critiqueModeONTemp);
        critiqueFooterWhenOn.setVisibility(View.GONE);
        final TextView turnOffCriticText = (TextView) view.findViewById(R.id.turnOffCriticText);
        final ImageView criticInfo = (ImageView) view.findViewById(R.id.criticInfo);
        turnOffCriticText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCritiqueModeOn = false;
                criticInfo.setImageResource(R.drawable.ic_critique);
                criticInfo.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                critiqueFooterWhenOn.setVisibility(View.GONE);
            }
        });

        ImageView criticClose = (ImageView) view.findViewById(R.id.critique_closeImg);
        criticClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideCriticOption();
            }
        });
        criticInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                criticInfo.setImageResource(R.drawable.ic_critiquefilled);
                criticInfo.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                showCriticScreen();
            }
        });
        final ImageView criticque_footor_favIcon = (ImageView) view.findViewById(R.id.cfooter_heart);

        if (CommonUtil.isContentUsersFav(ghazalId)) {
            criticque_footor_favIcon.setImageResource(R.drawable.ic_favorited);
            criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

        } else {
            criticque_footor_favIcon.setImageResource(R.drawable.ic_favorite);
            criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);

        }

        criticque_footor_favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isContentUsersFav(ghazalId)) {
                    CommonUtil.removeFromFavTroughApi(getActivity(), ghazalId);
                    criticque_footor_favIcon.setImageResource(R.drawable.ic_favorite);
                    criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);

                    footor_favIcon.setImageResource(R.drawable.ic_favorite);
                    footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                } else {
                    CommonUtil.addToFavTroughApi(getActivity(), ghazalId);
                    criticque_footor_favIcon.setImageResource(R.drawable.ic_favorited);
                    criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    footor_favIcon.setImageResource(R.drawable.ic_favorited);
                    footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                }


            }
        });
        ImageView shareFooterIcon = (ImageView) view.findViewById(R.id.cfooter_share);
        shareFooterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareThroughUrl();
            }
        });
    }

    public void hideCriticOption() {
        mainFooter.setVisibility(View.VISIBLE);
        critiqueFooter.setVisibility(View.GONE);
    }

    public void showCriticScreen() {

        final Dialog critiqueDialog = new Dialog(getActivity());

        critiqueDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueDialog.setContentView(R.layout.critique_custom_screen);
        critiqueDialog.setCanceledOnTouchOutside(false);
        TextView okayText = (TextView) critiqueDialog.findViewById(R.id.critiqueOkText);
        final CheckBox doNotShowAgain = (CheckBox) critiqueDialog.findViewById(R.id.critiqueCheckBox);
        //  dfgdfgfg
        okayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (doNotShowAgain.isChecked()) {
                    showCritiqueDialog = false;
                }
                isCritiqueModeOn = true;
                critiqueDialog.dismiss();
                critiqueFooterWhenOn.setVisibility(View.VISIBLE);


            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueDialog.show();
        critiqueDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    public String getLineFromTheParagraph(JSONArray Larray, int lineClicked) throws JSONException {
        //   String line = "";
        String lineToReturn = "";
        String firstLine = "";
        String secondLine = "";
        for (int j = 0; j < Larray.length(); j++) {
            JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
            // line = line + "\n";

            if (CommonUtil.languageCode != 3) {
                for (int k = 0; k < Warray.length(); k++) {
                    // line = line + " " + Warray.getJSONObject(k).getString("W");
                    if (j == 1) {
                        secondLine = secondLine + " " + Warray.getJSONObject(k).getString("W");
                        ;
                    }
                    if (j == 0) {
                        firstLine = firstLine + " " + Warray.getJSONObject(k).getString("W");
                    }
                }
            } else {
                for (int k = Warray.length() - 1; k >= 0; k--) {
                    // line = line + " " + Warray.getJSONObject(k).getString("W");
                    if (j == 1) {
                        secondLine = secondLine + " " + Warray.getJSONObject(k).getString("W");
                    }
                    if (j == 0) {
                        firstLine = firstLine + " " + Warray.getJSONObject(k).getString("W");
                    }
                }
            }

        }
        if (lineClicked == 0) {
            lineToReturn = firstLine;
        } else {
            lineToReturn = secondLine;
        }
        return lineToReturn;
    }

    Dialog critiqueSubmitForm;

    public void ShowCritiqueSubmitForm(final int lineNumber, final String ghazalLine) {
        critiqueSubmitForm = new Dialog(getActivity(), R.style.Dialog);
        critiqueSubmitForm.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueSubmitForm.setContentView(R.layout.submit_critique_template);
        critiqueSubmitForm.setCanceledOnTouchOutside(false);

        TextView critiqueLine = (TextView) critiqueSubmitForm.findViewById(R.id.critiqueLine);
        TextView critiqueLineNumber = (TextView) critiqueSubmitForm.findViewById(R.id.critiqueLineNumber);
        critiqueLineNumber.setText(" LINE #" + lineNumber);
        critiqueLine.setText(ghazalLine);
        TextView critiqueSubmitText = (TextView) critiqueSubmitForm.findViewById(R.id.critiqueSubmit);
        TextView critiqueCancelText = (TextView) critiqueSubmitForm.findViewById(R.id.critiqe_cancel);
        ImageView closeModal = (ImageView) critiqueSubmitForm.findViewById(R.id.closeModal);
        closeModal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueSubmitForm.dismiss();
            }
        });
        final EditText critiqueUserName = (EditText) critiqueSubmitForm.findViewById(R.id.critique_UserName);
        final EditText critiqueUserEmail = (EditText) critiqueSubmitForm.findViewById(R.id.critique_UserEmail);
        final EditText critiqueUserComment = (EditText) critiqueSubmitForm.findViewById(R.id.critique_UserComment);
        critiqueSubmitText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // critiqueSubmitForm.dismiss();

                if (checkValidation(critiqueUserName.getText().toString(), critiqueUserEmail.getText().toString(), critiqueUserComment.getText().toString())) {
                    submitCritiqueData(critiqueUserName.getText().toString(), critiqueUserEmail.getText().toString(), critiqueUserComment.getText().toString(), ghazalLine, lineNumber);
                }
            }
        });
        critiqueCancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueSubmitForm.dismiss();
            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueSubmitForm.show();
        critiqueSubmitForm.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);


    }

    private boolean checkValidation(String name, String email, String comment) {
   /*     TextInputLayout citiqueUserName = (TextInputLayout) critiqueSubmitForm.findViewById(R.id.citiqueUserName);
        TextInputLayout citiqueUserEmail = (TextInputLayout) critiqueSubmitForm.findViewById(R.id.citiqueEMAIL);
        TextInputLayout citiqueUserComment = (TextInputLayout) critiqueSubmitForm.findViewById(R.id.citiqueUserCOMMENT);*/
        int allcorect = 0;

        if (name.equalsIgnoreCase("") || name.equalsIgnoreCase(" ")) {
//            citiqueUserName.setError("Not a valid name");
//            allcorect = allcorect+1;
            Toast.makeText(getActivity(), "Please enter your name", Toast.LENGTH_SHORT).show();
            return false;
        } else {
//            citiqueUserName.setError(null);
        }

        if (!CommonUtil.isValidEmail(email)) {
          /*  citiqueUserEmail.setError("Not a valid email-Id");
            allcorect = allcorect+1;*/
            Toast.makeText(getActivity(), "Not a valid Email", Toast.LENGTH_SHORT).show();
            return false;
        } else {
//            citiqueUserEmail.setError(null);
        }

        if (comment.equalsIgnoreCase("") || comment.equalsIgnoreCase(" ")) {
            Toast.makeText(getActivity(), "Please enter some text", Toast.LENGTH_SHORT).show();
            return false;
        } else {
//            citiqueUserComment.setError(null);
        }

        return true;


    }


    public void submitCritiqueData(String name, String email, String comment, String ghazalLine, int ghazalLineNumber) {
        HashMap obj = new HashMap();
        try {
            String pageUrl = "";
            if (CommonUtil.languageCode == 1) {
                pageUrl = data.getString("UE");
            } else if (CommonUtil.languageCode == 2) {
                pageUrl = data.getString("UH");
            } else if (CommonUtil.languageCode == 3) {
                pageUrl = data.getString("UU");
            } else {
                pageUrl = data.getString("UE");
            }

            obj.put("name", name);
            obj.put("email", email);
            obj.put("contentId", data.getString("I"));
            obj.put("contentTitle", toolbarTV.getText().toString());
            obj.put("pageType", "App Poem");
            obj.put("subject", "LINE #" + ghazalLineNumber + " " + ghazalLine);
            obj.put("message", comment);
            obj.put("typeOfQuery", 2);
            obj.put("pageUrl", pageUrl);
            //  Toast.makeText(getActivity(), "" + obj.toString(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final ProgressDialog loading = ProgressDialog.show(getActivity(), null, "Loading..", false, false);
        RestClient.get().submitCritique(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        ShowThankYouForCritique();
                    } else {
                        Toast.makeText(getActivity(), "oops ! Error occured while submitting your request", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });
    }

    public void shareThroughUrl() {
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "";
            if (CommonUtil.languageCode == 1) {
                shareBody = data.getString("UE");
            } else if (CommonUtil.languageCode == 2) {
                shareBody = data.getString("UH");
            } else if (CommonUtil.languageCode == 3) {
                shareBody = data.getString("UU");
            } else {
                shareBody = data.getString("UE");
            }

            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "https://Rekhta.org");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // for showing ThankYou Dialog after Submitting Crtic
    public void ShowThankYouForCritique() {

        final Dialog critiqueThankYouDialog = new Dialog(getActivity());
        critiqueThankYouDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueThankYouDialog.setContentView(R.layout.critique_thanks_dialog_template);
        critiqueThankYouDialog.setCanceledOnTouchOutside(false);
        TextView thankYouText = (TextView) critiqueThankYouDialog.findViewById(R.id.thanksCritiqueText);
        thankYouText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueThankYouDialog.dismiss();
            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueThankYouDialog.show();
        critiqueThankYouDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                critiqueThankYouDialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 2000);
    }

    public void getBottomOfGhazal(String id) {
        try {


            RestClient.get().getBottomContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.languageCode, id, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {

                    try {
                        JSONObject obj = new JSONObject(res);
                        if (obj.getInt("S") == 1) {
                            JSONArray dataresponse = obj.getJSONObject("R").getJSONArray("nextPrevContent");
                            if (dataresponse.length() == 0) {
                                nextLinaerlayoutview.setVisibility(View.GONE);
                                prevLinaerlayoutview.setVisibility(View.GONE);
                            } else {
                                for (int i = 0; i < dataresponse.length(); i++) {
                                    if (dataresponse.getJSONObject(i).getBoolean("IN") == true) {
                                        nextLinaerlayoutview.setVisibility(View.VISIBLE);
                                        setNextSection(dataresponse.getJSONObject(i));
                                    }

                                    if (dataresponse.getJSONObject(i).getBoolean("IN") == false) {
                                        prevLinaerlayoutview.setVisibility(View.VISIBLE);
                                        setPrevioustSection(dataresponse.getJSONObject(i));


                                    }
                                }
                            }

                        } else {
                            Toast.makeText(getActivity(), "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


                @Override
                public void failure(RetrofitError error) {
                    // Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setNextSection(JSONObject obj) {
        try {
            final String id = obj.getString("I");
            final String authorName = obj.getString("PN");


            nextTiltle.setText(obj.getString("CT"));
            nextAuthorName.setText(obj.getString("PN"));


            if (CommonUtil.languageCode == 1) {
                nextTiltle.setTypeface(engtf);
                nextAuthorName.setTypeface(engtf);
            } else if (CommonUtil.languageCode == 2) {
                nextTiltle.setTypeface(hinditf);
                nextAuthorName.setTypeface(hinditf);
            } else if (CommonUtil.languageCode == 3) {
                nextTiltle.setTypeface(urdutf);
                nextAuthorName.setTypeface(urdutf);
            }

            nextLinaerlayoutview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((SearchActivity) getActivity()).idGhazalToRender = id;
                    ((SearchActivity) getActivity()).currentGhazalPoetName = authorName;
                    ((SearchActivity) getActivity()).removeFragment();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setPrevioustSection(final JSONObject obj) {
        try {
            final String id = obj.getString("I");
            final String authorName = obj.getString("PN");
            prevTitle.setText(obj.getString("CT"));
            prevAuthorName.setText(obj.getString("PN"));

            if (CommonUtil.languageCode == 1) {
                prevTitle.setTypeface(engtf);
                prevAuthorName.setTypeface(engtf);
            } else if (CommonUtil.languageCode == 2) {
                prevTitle.setTypeface(hinditf);
                prevAuthorName.setTypeface(hinditf);
            } else if (CommonUtil.languageCode == 3) {
                prevTitle.setTypeface(urdutf);
                prevAuthorName.setTypeface(urdutf);
            }

            prevLinaerlayoutview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((SearchActivity) getActivity()).idGhazalToRender = id;
                    ((SearchActivity) getActivity()).currentGhazalPoetName = authorName;
                    ((SearchActivity) getActivity()).removeFragment();

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}


