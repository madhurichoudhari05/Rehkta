package org.Rekhta.fragments.SearchFragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.adapters.SearchAdapter.PoetsSearchAdapter;
import org.Rekhta.model.SearchModel.ContentModel;
import org.Rekhta.model.SearchModel.MorePoet;
import org.Rekhta.model.SearchModel.PoetsModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Admin on 11/14/2017.
 */

public class PoetsSearch extends Fragment {

    RecyclerView recyclerView;
    ArrayList<PoetsModel> poetsModelArrayList = new ArrayList<>();
    PoetsSearchAdapter poetsSearchAdapter;

    String searchedText = "";
    private BroadcastReceiver broadcastReceiver;

    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.poets_fragment, container, false);
        setRetainInstance(true);
        recyclerView = (RecyclerView) view.findViewById(R.id.poets_recyclerview);
        poetsSearchAdapter = new PoetsSearchAdapter(poetsModelArrayList, getActivity());
        recyclerView.setAdapter(poetsSearchAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        searchedText = ((SearchActivity) getActivity()).searchedText;


        if (!searchedText.equalsIgnoreCase("")) {
            getAllData(searchedText);
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter("searchBroadcast");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                getAllData(searchedText);
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    public void getAllData(String keyWord) {
        poetsModelArrayList.clear();
        final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);

        RestClient.get().getAllSearchedData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, keyWord, CommonUtil.languageCode, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }

                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getString("Message").equalsIgnoreCase("success")) {

                        for (int i = 0; i < obj.getJSONArray("Poets").length(); i++) {
                            poetsModelArrayList.add(new PoetsModel(obj.getJSONArray("Poets").getJSONObject(i).getString("EntityId"), obj.getJSONArray("Poets").getJSONObject(i).getString("Name"), obj.getJSONArray("Poets").getJSONObject(i).getInt("GazalCount"),
                                    obj.getJSONArray("Poets").getJSONObject(i).getBoolean("IsNew"), obj.getJSONArray("Poets").getJSONObject(i).getInt("NazmCount"), obj.getJSONArray("Poets").getJSONObject(i).getString("SEO_Slug"), obj.getJSONArray("Poets").getJSONObject(i).getString("ImageUrl")));

                        }
                        poetsSearchAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing())
                    progressLoader.dismiss();

            }
        });


    }


}
