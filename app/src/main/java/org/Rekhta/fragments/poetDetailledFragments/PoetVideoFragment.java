package org.Rekhta.fragments.poetDetailledFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.adapters.poetDetail.PoetVideoAdapter;
import org.Rekhta.model.poetDetail.PoetVideoData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PoetVideoFragment extends Fragment {

    View view;
    RecyclerView poetVideoRecyclerView;
    PoetVideoAdapter poetVideoAdapter;
    ArrayList<PoetVideoData> arrayList;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading = false;
    JSONArray dataArray = new JSONArray();


    String poetId = "";
    private ProgressDialog progressLoader;
    private Context context;

    public PoetVideoFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_poet_video, container, false);
        poetId = ((PoetDetailedActivity) getActivity()).currentPoetId;
        // poetId = "EB9CB3D1-1DBD-4582-A1CA-E48FDAFE5BC8";
        arrayList = new ArrayList<>();
        context = getActivity();
        //getAudioListByPoet(((PoetDetailedActivity)getActivity()).currentPoetId);

        poetVideoRecyclerView = (RecyclerView) view.findViewById(R.id.poetVideoRecyclerView);
        poetVideoRecyclerView.setHasFixedSize(true);
        poetVideoRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        poetVideoAdapter = new PoetVideoAdapter(arrayList, getActivity(), totalNumberOfContent, PoetVideoFragment.this);
        poetVideoRecyclerView.setAdapter(poetVideoAdapter);

        getVideoListByPoet(poetId);
        //initList();
        setRetainInstance(true);

        poetVideoRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {

                    } else {
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getVideoListByPoet(poetId);
                        }
                    }
                }
            }
        });
        return view;
    }

/*
    void initList() {
        for (int i = 0; i <3 ; i++) {
            //arrayList.add(new PoetVideoData(i + "", "PE", "PH", "PU", "Video "+i, "TH", "TU", "https://rekhta.org/Images/SiteImages/Audio/027FF6E1-4A2C-47D1-BAD2-5C1E24FCBE2C.mp3"));
            arrayList.add(new PoetVideoData(i + "", "PE", "PH", "PU", "Video "+i, "TH", "TU","PE", "PH", "PU", "Video "+i, "TH", "TU","PE", "PH", "PU", "Video "+i, "TH", "TU","PE", "PH", "PU", "Video "+i, ""+ true,true ));
        }
        //poetAudioAdapter.notifyDataSetChanged();
    }
*/

    public void getVideoListByPoet(String poetId) {
        isDataLoading = true;
       /// arrayList.clear();

        if (!((Activity) context).isFinishing()) {
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }
        RestClient.get().getVideoListByPoetIdWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, poetId, "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
                isDataLoading = false;

                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        totalNumberOfContent = obj.getJSONObject("R").getInt("TC");
                        if (pageIndex == 1) {
                            poetVideoAdapter = new PoetVideoAdapter(arrayList, getActivity(), totalNumberOfContent, PoetVideoFragment.this);
                            poetVideoRecyclerView.setAdapter(poetVideoAdapter);
                        }
                        if (obj.getJSONObject("R").getJSONArray("V").length() == 0) {
                            // do nothng
                        } else {
                           // dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("V"));
                            dataArray = obj.getJSONObject("R").getJSONArray("V");
                            for (int i = 0; i < dataArray.length(); i++) {

                                arrayList.add(new PoetVideoData(
                                        dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("YI"),
                                        dataArray.getJSONObject(i).getString("CI"), dataArray.getJSONObject(i).getString("TI"),
                                        dataArray.getJSONObject(i).getString("TE"), dataArray.getJSONObject(i).getString("TH"),
                                        dataArray.getJSONObject(i).getString("TU"), dataArray.getJSONObject(i).getString("DE"),
                                        dataArray.getJSONObject(i).getString("DH"), dataArray.getJSONObject(i).getString("DU"),
                                        dataArray.getJSONObject(i).getString("CS"), dataArray.getJSONObject(i).getString("PI"),
                                        dataArray.getJSONObject(i).getString("NE"), dataArray.getJSONObject(i).getString("NH"),
                                        dataArray.getJSONObject(i).getString("NU"), dataArray.getJSONObject(i).getString("PS"),
                                        dataArray.getJSONObject(i).getString("AI"), dataArray.getJSONObject(i).getString("AE"),
                                        dataArray.getJSONObject(i).getString("AH"), dataArray.getJSONObject(i).getString("AU"),
                                        dataArray.getJSONObject(i).getString("AS"), dataArray.getJSONObject(i).getString("PSN"),
                                        dataArray.getJSONObject(i).getString("ASN"), dataArray.getJSONObject(i).get("HA") + "",
                                        dataArray.getJSONObject(i).getBoolean("HP"), dataArray.getJSONObject(i).getInt("VC")));
                            }
                            poetVideoAdapter.notifyDataSetChanged();

                        }

                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                isDataLoading = false;
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }

    public void checkOrientaiotn(Intent intent) {

        getActivity().startActivityForResult(intent, 100);
    }
}
