package org.Rekhta.fragments.poetDetailledFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.activites.SherActivity;
import org.Rekhta.adapters.SherCollectionsAdapter;
import org.Rekhta.adapters.poetDetail.PoetShersNewAdapter;
import org.Rekhta.model.SherCollection;
import org.Rekhta.views.MeaningFragment;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.adapters.poetDetail.PoetGhazalAdapter;
import org.Rekhta.adapters.poetDetail.PoetSherAdapter;
import org.Rekhta.model.CollectionData;
import org.Rekhta.model.poetDetail.PoetGhazalData;
import org.Rekhta.model.poetDetail.PoetSherData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PoetSherFragment extends Fragment {

    View view;

    PoetSherAdapter poetSherAdapter;
    // ArrayList<PoetSherData> arrayList;
    RecyclerView poetSherRecylerView;
    EditText poetAllserachEdittext;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    JSONArray dataArray = new JSONArray();

    boolean isLoading = false;
    private ProgressDialog progressLoader;
    private PoetShersNewAdapter poetShersNewAdapter;
    ArrayList<SherCollection> arrayList;

    Context context;
    private ProgressDialog loading;


    public PoetSherFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_poet_sher, container, false);

        arrayList = new ArrayList<>();

        context = getActivity();
        //getPoetSherData(((PoetDetailedActivity) getActivity()).currentPoetId);
        getPoetSherData(((PoetDetailedActivity) getActivity()).currentPoetId);
        poetSherRecylerView = (RecyclerView) view.findViewById(R.id.poetSherrecylerview);
        poetSherRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        poetSherRecylerView.setItemViewCacheSize(20);
        poetSherRecylerView.setDrawingCacheEnabled(true);
        poetSherRecylerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        poetShersNewAdapter = new PoetShersNewAdapter(arrayList, getActivity(), PoetSherFragment.this);
        poetSherRecylerView.setAdapter(poetShersNewAdapter);
        ((PoetDetailedActivity) getActivity()).poetShersNewAdapter = poetShersNewAdapter;

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        poetSherRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {
                        // Toast.makeText(context, "lessthan20", Toast.LENGTH_SHORT).show();

                    } else {
                        //    Toast.makeText(context, "greatwet", Toast.LENGTH_SHORT).show();
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getPoetSherData(((PoetDetailedActivity) getActivity()).currentPoetId);
                        }
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void getPoetSherData(String contentId) {
        // for disabling the Navigation Bar to Open Here
        isDataLoading = true;
        if (!((Activity) context).isFinishing()) {
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }
        RestClient.get().getSlectedSherCollectionData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, contentId, "F722D5DC-45DA-41EC-A439-900DF702A3D6", "", "", pageIndex, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {
                        if (progressLoader != null && progressLoader.isShowing())
                            progressLoader.dismiss();
                        try {

                            JSONObject content = new JSONObject(res);
                            if (content.getInt("S") == 1) {
                                totalNumberOfContent = content.getJSONObject("R").getInt("TC");
                                /*dataArray = content.getJSONObject("R").getJSONArray("CD");*/
                                CommonUtil.cSherTotalCount = totalNumberOfContent + "";
                                arrayList.clear();
                                dataArray = CommonUtil.concatArray(dataArray, content.getJSONObject("R").getJSONArray("CD"));

                                for (int i = 0; i < dataArray.length(); i++) {

                                    arrayList.add(new SherCollection(dataArray.getJSONObject(i).getString("RE"), dataArray.getJSONObject(i).getString("RH"),
                                            dataArray.getJSONObject(i).getString("RU"), dataArray.getJSONObject(i).getString("PE"), dataArray.getJSONObject(i).getString("PH"),
                                            dataArray.getJSONObject(i).getString("PU"), dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("UE")
                                            , dataArray.getJSONObject(i).getString("UH"), dataArray.getJSONObject(i).getString("UU"), dataArray.getJSONObject(i).getJSONArray("TS"),
                                            dataArray.getJSONObject(i).getString("P"), dataArray.getJSONObject(i).getString("PI")));
                                }

                                poetShersNewAdapter.notifyDataSetChanged();
                                isDataLoading = false;
                            } else {

                                Toast.makeText(context, "Oops an error occured", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (progressLoader != null && progressLoader.isShowing()) {
                            progressLoader.dismiss();
                        }
                        Toast.makeText(context, "failed Getting Data", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }

    public void showWordMeaning(final String mainWord, String word) {

        if (!((Activity) context).isFinishing()) {
            loading = ProgressDialog.show(context, null, "Please wait...", false, false);
        }

        //  final ProgressDialog loading = ProgressDialog.show(context, null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {

                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putBoolean("fromPoetSher", true);
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        MeaningFragment meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        ((PoetDetailedActivity) context).
                                getSupportFragmentManager().beginTransaction()
                                .replace(R.id.sherLinearLayout, meaningFragment, "meaningFragTag")
                                .addToBackStack(null)
                                .commit();

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (loading != null)
                    loading.dismiss();
            }
        });

    }

    public void showmeaningFragment(TextView textView, JSONObject obj) {

        MeaningFragment meaningFragment = (MeaningFragment) getActivity().getSupportFragmentManager().findFragmentByTag("meaningFrag");
        if (meaningFragment != null && meaningFragment.isVisible()) {
            // showMeaning(mainText.getText().toString(), obj);

        } else {

            showMeaning(textView.getText().toString(), obj);


        }
    }

    private void showMeaning(String s, JSONObject obj) {

        try {
            getMeaningOfTheWord(s, obj.getString("M"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMeaningOfTheWord(final String mainWord, String word) {

        final ProgressDialog loading = ProgressDialog.show(context, null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (loading != null && loading.isShowing())
                    loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putString("home", "sher");
                        bundle.putBoolean("fromPoetSher", true);
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        MeaningFragment meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .add(R.id.drawer_layout, meaningFragment, "meaningFrag")
                                .addToBackStack(null)
                                .commit();

                    } else {
                        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (loading != null && loading.isShowing())
                    loading.dismiss();
            }
        });
    }

    public void closeActvity() {

        getActivity().finish();

    }
}
