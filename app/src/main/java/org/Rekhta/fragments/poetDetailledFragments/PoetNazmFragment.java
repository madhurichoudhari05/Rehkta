package org.Rekhta.fragments.poetDetailledFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.adapters.poetDetail.PoetNazmAdapter;
import org.Rekhta.model.poetDetail.PoetNazmData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PoetNazmFragment extends Fragment {

    View view;
    PoetNazmAdapter poetNazmAdapter;
    ArrayList<PoetNazmData> arrayList;
    RecyclerView poetNazmRecylerView;
    EditText poetAllserachEdittext;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    boolean isLoading = false;
    JSONArray dataArray = new JSONArray();
    private ProgressDialog progressLoader;
    private Context context;

    public PoetNazmFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_poet_nazm, container, false);

        context = getActivity();
        arrayList = new ArrayList<>();
        getPoetsNazm(((PoetDetailedActivity) getActivity()).currentPoetId);
        poetNazmRecylerView = (RecyclerView) view.findViewById(R.id.poetNazmrecylerview);
        poetNazmRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        poetNazmRecylerView.setItemViewCacheSize(20);
        poetNazmRecylerView.setDrawingCacheEnabled(true);
        poetNazmRecylerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        poetNazmAdapter = new PoetNazmAdapter(arrayList, getActivity(), totalNumberOfContent);
        poetNazmRecylerView.setAdapter(poetNazmAdapter);
        ((PoetDetailedActivity) getActivity()).poetNazmAdapter = poetNazmAdapter;
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        poetNazmRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!poetNazmRecylerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {
                        // Toast.makeText(context, "lessthan20", Toast.LENGTH_SHORT).show();

                    } else {
                        //    Toast.makeText(context, "greatwet", Toast.LENGTH_SHORT).show();
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getPoetsNazm(((PoetDetailedActivity) getActivity()).currentPoetId);
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void getPoetsNazm(String poetId) {

        isDataLoading = true;
        if (!((Activity) context).isFinishing()) {
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }

        RestClient.get().getContentListWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, poetId, "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b", "", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                isLoading = false;

                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        if (obj.getJSONObject("R").getJSONArray("CS").length() == 0) {
                            // do nothng
                        } else {
                            totalNumberOfContent = obj.getJSONObject("R").getInt("TC");
                            if (pageIndex == 1) {
                                poetNazmAdapter = new PoetNazmAdapter(arrayList, getActivity(), totalNumberOfContent);
                                poetNazmRecylerView.setAdapter(poetNazmAdapter);
                            }
                            //  dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("CS"));
                            dataArray = obj.getJSONObject("R").getJSONArray("CS");
                            for (int i = 0; i < dataArray.length(); i++) {
                                arrayList.add(new PoetNazmData(dataArray.getJSONObject(i).getString("I"),
                                        dataArray.getJSONObject(i).getString("PE"), dataArray.getJSONObject(i).getString("PH"), dataArray.getJSONObject(i).getString("PU"),
                                        dataArray.getJSONObject(i).getString("SE"), dataArray.getJSONObject(i).getString("SH"), dataArray.getJSONObject(i).getString("SU"),
                                        dataArray.getJSONObject(i).getString("TE"), dataArray.getJSONObject(i).getString("TH"), dataArray.getJSONObject(i).getString("TU"),
                                        dataArray.getJSONObject(i).getBoolean("EC"), dataArray.getJSONObject(i).getBoolean("PC"), dataArray.getJSONObject(i).getBoolean("AU"), dataArray.getJSONObject(i).getBoolean("VI"),
                                        dataArray.getJSONObject(i).getString("UE"), dataArray.getJSONObject(i).getString("UH"), dataArray.getJSONObject(i).getString("UU")));
                            }
                            poetNazmAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                    isDataLoading = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }
}
