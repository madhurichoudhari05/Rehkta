package org.Rekhta.fragments.poetDetailledFragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.adapters.poetDetail.PoetAudioAdapter;
import org.Rekhta.model.poetDetail.PoetAudioData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class PoetAudioFragment extends Fragment implements Runnable,
        SeekBar.OnSeekBarChangeListener {

    View view;

    PoetAudioAdapter poetAudioAdapter;
    ArrayList<PoetAudioData> arrayList;
    RecyclerView poetAudioRecylerView;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    ProgressBar audioProBar;
    JSONArray dataArray = new JSONArray();

    Context context;

    boolean isLoading = false;
    int clickedPosition;
    TextView poetAudioTitle, remiainTime, totalTime;
    ImageView closePlayerIcon;
    LinearLayout musicPlayerView;
    String poetId = "";
    //For music Player
    private SeekBar seekBar;
    private ImageView playBtn;
    private ImageView backwardBtn;
    private ImageView fwdBtn;
    private MediaPlayer mp;
    private ProgressDialog progressLoader;
    ProgressDialog audioDialog;
    private int position;


    public PoetAudioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_poet_audio, container, false);
        arrayList = new ArrayList<>();
        poetId = ((PoetDetailedActivity) getActivity()).currentPoetId;
        //getAudioListByPoet(poetId);
        //poetId = "EB9CB3D1-1DBD-4582-A1CA-E48FDAFE5BC8";

        context = getActivity();

        setRetainInstance(false);

        poetAudioRecylerView = (RecyclerView) view.findViewById(R.id.poetProfileAudio);
        poetAudioRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        poetAudioAdapter = new PoetAudioAdapter(arrayList, getActivity(), PoetAudioFragment.this, totalNumberOfContent);
        poetAudioRecylerView.setAdapter(poetAudioAdapter);


        ((PoetDetailedActivity) getActivity()).poetAudioAdapter = poetAudioAdapter;
        initViews();


       /* poetAudioRecylerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), poetAudioRecylerView.getChildLayoutPosition(view) + "--" + poetAudioRecylerView.indexOfChild(view), Toast.LENGTH_SHORT).show();
//                clickedPosition =
                //playAudio();
               // poetAudioTitle.setText(arrayList.get(clickedPosition).getAudioPoetNameinEng());
            }
        });*/

        getAudioListByPoet(poetId);
        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (mp != null && mp.isPlaying()) {
                mp.stop();
                mp.reset();
                mp = null;
                // mp.release();
                musicPlayerView.setVisibility(View.GONE);
            }

        }
    }

    public void ItemSelectedInRecyclerView(int pos) {

    }

    public void initBottomView() {

 /*   BottomSheetDialogFragment bottomSheetDialogFragment = new PoetFragmentMusicPlayer();
    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
*/
    }

    private void initViews() {
        //  musicPlayerView = (LinearLayout) view.findViewById(R.id.musicPlayer);
        closePlayerIcon = (ImageView) view.findViewById(R.id.closePlayer);
        poetAudioTitle = (TextView) view.findViewById(R.id.audioTitle);
        seekBar = (SeekBar) view.findViewById(R.id.progessSeekBar);
        seekBar.setMax(99);

        musicPlayerView = (LinearLayout) view.findViewById(R.id.musicPlayer);
        playBtn = (ImageView) view.findViewById(R.id.poetDetailPlayBtn);
        backwardBtn = (ImageView) view.findViewById(R.id.poetDetailBkdBtn);
        fwdBtn = (ImageView) view.findViewById(R.id.poetDetailFwdBtn);

        remiainTime = (TextView) view.findViewById(R.id.remiainTime);
        totalTime = (TextView) view.findViewById(R.id.totalTime);

        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setEnabled(false);
        // mp = new MediaPlayer();

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp != null && mp.isPlaying()) {
                    mp.pause();
                    playBtn.setImageResource(R.drawable.ic_play);
                } else {
                    playBtn.setImageResource(R.drawable.ic_pause);
                    //playAudio();
                    mp.start();
                }

            }
        });

        backwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position > 0) {
                    position = position - 1;
                    // audioDialog.show();
                    playAudio();
                } else {
                    Toast.makeText(context, "No Previous audio available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position < arrayList.size() - 1) {
                    position = position + 1;
                    //audioDialog.show();
                    playAudio();
                } else {
                    Toast.makeText(context, "No Next Audio Available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        closePlayerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp != null && mp.isPlaying() || mp.getDuration() > 0) {
                    mp.stop();
                    mp = null;
                    playBtn.setImageResource(R.drawable.ic_play);
                    seekBar.setProgress(0);

                }
                musicPlayerView.setVisibility(View.GONE);
            }
        });
    }

  /*  void initList() {
        for (int i = 0; i < 10; i++) {
            arrayList.add(new PoetAudioData(i + "", "PE", "PH", "PU", "Audio "+i, "TH", "TU", "https://rekhta.org/Images/SiteImages/Audio/027FF6E1-4A2C-47D1-BAD2-5C1E24FCBE2C.mp3"));
        }
        //poetAudioAdapter.notifyDataSetChanged();
    }*/

    public void getAudioListByPoet(String poetId) {


        if (!((Activity) context).isFinishing()) {
            // progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }
        RestClient.get().getAudioListByPoetIdWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, poetId, "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {


                isLoading = false;

                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        if (obj.getJSONObject("R").getJSONArray("A").length() == 0) {
                            // do nothng
                        } else {
                            totalNumberOfContent = obj.getJSONObject("R").getInt("TC");

                            if (pageIndex == 1) {
                                poetAudioAdapter = new PoetAudioAdapter(arrayList, getActivity(), PoetAudioFragment.this, totalNumberOfContent);
                                poetAudioRecylerView.setAdapter(poetAudioAdapter);
                            }

                            dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("A"));
                            for (int i = 0; i < dataArray.length(); i++) {

                                arrayList.add(new PoetAudioData(dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("PI"),
                                        dataArray.getJSONObject(i).getString("NE"), dataArray.getJSONObject(i).getString("NH"),
                                        dataArray.getJSONObject(i).getString("NU"), dataArray.getJSONObject(i).getString("PS"),
                                        dataArray.getJSONObject(i).getString("CI"), dataArray.getJSONObject(i).getString("TE"),
                                        dataArray.getJSONObject(i).getString("TH"), dataArray.getJSONObject(i).getString("TU"),
                                        dataArray.getJSONObject(i).getString("CS"), dataArray.getJSONObject(i).getString("AI"),
                                        dataArray.getJSONObject(i).getString("AE"), dataArray.getJSONObject(i).getString("AH"),
                                        dataArray.getJSONObject(i).getString("AU"), dataArray.getJSONObject(i).getString("AS"),
                                        dataArray.getJSONObject(i).getString("TI"), dataArray.getJSONObject(i).getString("PSN"),

                                        //    Toast.makeText(getApplicationContext(), "arrayListsize"+arrayList.get(i)., Toast.LENGTH_SHORT).show();
                                        dataArray.getJSONObject(i).getString("ASN"), dataArray.getJSONObject(i).getBoolean("HA"),
                                        dataArray.getJSONObject(i).getBoolean("HP"), dataArray.getJSONObject(i).getString("AD")));
                            }
                            poetAudioAdapter.notifyDataSetChanged();
                            CommonUtil.audioArrayList = arrayList;
                        }
                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        /*if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }*/
    }

    public void playAudio() {

        String title = null;
        String audioUrl = null;
        String length = null;
        Uri myUri = null;

        //playBtn.setImageResource(R.drawable.ic_pause);
        if (position < arrayList.size()) {
            if (CommonUtil.languageCode == 1) {
                title = arrayList.get(position).getAudioTitleInEng();
            } else if (CommonUtil.languageCode == 2) {
                title = arrayList.get(position).getAudioTitleInHin();
            } else if (CommonUtil.languageCode == 3) {
                title = arrayList.get(position).getAudioTitleInUrdu();
            }

            audioUrl = "https://rekhta.org/Images/SiteImages/Audio/" + arrayList.get(position).getId() + ".mp3";
            length = arrayList.get(position).getLength();
        }

        poetAudioTitle.setText(title);
        totalTime.setText(length);
        if (audioUrl != null) {
            myUri = Uri.parse(audioUrl);
        }


        if (mp == null) {
            try {
                //Uri myUri = Uri.parse("https://rekhta.org/Images/SiteImages/Audio/027FF6E1-4A2C-47D1-BAD2-5C1E24FCBE2C.mp3");

                mp = new MediaPlayer();
                mp.setDataSource(context, myUri);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepare(); //don't use prepareAsync for mp3 playback
                mp.start();
                playBtn.setImageResource(R.drawable.ic_pause);

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            try {
                mp.stop();
                mp = null;
                mp = new MediaPlayer();
                mp.setDataSource(context, myUri);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);


                mp.prepare(); //don't use prepareAsync for mp3 playback
                mp.start();
                playBtn.setImageResource(R.drawable.ic_pause);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        //  audioBar.setVisibility(View.GONE);
        seekBar.setEnabled(true);
        seekBar.setMax(mp.getDuration());
        new Thread(this).start();

    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        try {
            if (mp.isPlaying() || mp != null) {
                if (fromUser)
                    mp.seekTo(progress);
            } else if (mp == null) {
                Toast.makeText(getApplicationContext(), "Media is not running",
                        Toast.LENGTH_SHORT).show();
                seekBar.setProgress(0);
            }
        } catch (Exception e) {
            Log.e("seek bar", "" + e);
            seekBar.setEnabled(false);

        }
        updatePlayer(progress, remiainTime);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void run() {

        if (mp != null) {
            int currentPosition = mp.getCurrentPosition();
            int total = mp.getDuration();

            while (mp != null && currentPosition < total) {
                try {
                    Thread.sleep(1000);
                    currentPosition = mp.getCurrentPosition();
                } catch (InterruptedException e) {
                    return;
                } catch (Exception e) {
                    return;
                }
                seekBar.setProgress(currentPosition);
            }
        }

        // updatePlayer(total, remiainTime);

    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mp != null && mp.isPlaying()) {
            mp.stop();
            mp.release();
            musicPlayerView.setVisibility(View.GONE);
        }

    }


    @Override
    public void onStop() {
        super.onStop();

    }

    public void playClickedAudio(int position, Context context1) {

        musicPlayerView.setVisibility(View.VISIBLE);

        this.position = position;
        if (mp != null) {
            if (mp.isPlaying()) {
                mp.stop();
                playAudio();
            } else {
                // playAudio();
            }

        } else {
            playAudio();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void updatePlayer(int currentDuration, TextView remiainTime) {
        remiainTime.setText("" + milliSecondsToTimer((long) currentDuration));
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }


}
