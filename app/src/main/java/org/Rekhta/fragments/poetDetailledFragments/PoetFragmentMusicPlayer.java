package org.Rekhta.fragments.poetDetailledFragments;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.util.ArrayList;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.adapters.poetDetail.PoetAudioListViewAdapter;
import org.Rekhta.model.poetDetail.PoetAudioData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MediaPlayerService;


public class PoetFragmentMusicPlayer extends BottomSheetDialogFragment {
    View view;
    TextView musicTitle, musicArtistName;
    ImageView playBtnImg, musicPreviousBtn, musicNextBtn, musicAuthorImg;
    SeekBar seekBar;
    int position = 0;
    ListView audioListView;
    PoetMediaplayerService poetMediaplayerService;
    PoetAudioListViewAdapter poetAudioListViewAdapter;

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        position = getArguments().getInt("positionClicked");
        view = View.inflate(getContext(), R.layout.demo2, null);
        dialog.setContentView(view);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                poetMediaplayerService.stopAudio();
            }
        });
        initViews();
    }

    private void initViews() {

        audioListView = (ListView) view.findViewById(R.id.audioMPlistView);
        poetAudioListViewAdapter = new PoetAudioListViewAdapter(getActivity(), CommonUtil.audioArrayList);

        audioListView.setAdapter(poetAudioListViewAdapter);

        audioListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                setViewItems();
                poetMediaplayerService.stopAudio();
                poetMediaplayerService.playAudio(CommonUtil.audioArrayList.get(i).getId());
            }
        });

        poetMediaplayerService = new PoetMediaplayerService();
        musicTitle = (TextView) view.findViewById(R.id.musicTitle);
        musicArtistName = (TextView) view.findViewById(R.id.musicArtistName);
        playBtnImg = (ImageView) view.findViewById(R.id.playBtnImg);
        musicPreviousBtn = (ImageView) view.findViewById(R.id.musicPreviousBtn);
        musicNextBtn = (ImageView) view.findViewById(R.id.musicNextBtn);
        musicAuthorImg = (ImageView) view.findViewById(R.id.musicAuthorImg);

        seekBar = (SeekBar) view.findViewById(R.id.progessSeekBar);

        poetMediaplayerService.playAudio(CommonUtil.audioArrayList.get(position).getId());

        playBtnImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Playing", Toast.LENGTH_SHORT).show();
                poetMediaplayerService.pauseAndPlayAudio();

            }
        });
        setViewItems();
        musicNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Next", Toast.LENGTH_SHORT).show();

                if (CommonUtil.audioArrayList.size() > position) {

                    position += 1;
                    setViewItems();
                    poetMediaplayerService.stopAudio();
                    poetMediaplayerService.playAudio(CommonUtil.audioArrayList.get(position).getId());
                } else {
                    Toast.makeText(getActivity(), "This the Last Audio", Toast.LENGTH_SHORT).show();
                }
            }
        });

        musicPreviousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Previous", Toast.LENGTH_SHORT).show();
                if (position > 0) {
                    position -= 1;
                    setViewItems();
                    poetMediaplayerService.stopAudio();
                    poetMediaplayerService.playAudio(CommonUtil.audioArrayList.get(position).getId());

                } else {
                    Toast.makeText(getActivity(), "This the First Audio", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void setViewItems() {
        Glide.with(getActivity()).load("https://rekhtacdn.azureedge.net/Images/Shayar/" + CommonUtil.audioArrayList.get(position).getId() + ".png").into(musicAuthorImg);


        if (CommonUtil.languageCode == 1) {
            musicTitle.setText(CommonUtil.audioArrayList.get(position).getAudioTitleInEng());
            musicArtistName.setText(CommonUtil.audioArrayList.get(position).getAudioPoetNameinEng());
        }

        if (CommonUtil.languageCode == 2) {
            musicTitle.setText(CommonUtil.audioArrayList.get(position).getAudioTitleInHin());
            musicArtistName.setText(CommonUtil.audioArrayList.get(position).getAudioPoetNameInHin());
        }

        if (CommonUtil.languageCode == 3) {
            musicTitle.setText(CommonUtil.audioArrayList.get(position).getAudioTitleInUrdu());
            musicArtistName.setText(CommonUtil.audioArrayList.get(position).getAudioPoetNameInUrdu());
        }
    }

/*    public void playAudio(String id, boolean isNextOrPrev) {

        // Glide.with(getActivity()).load("").into(musicAuthorImg);

        if (isNextOrPrev) {
            mp.reset();
        }

        if (mp == null) {
            try {

                seekBar.setProgress(0);
                Uri myUri = Uri.parse("https://rekhta.org/Images/SiteImages/Audio/" + "027FF6E1-4A2C-47D1-BAD2-5C1E24FCBE2C" + ".mp3");
                Log.e("---------", myUri.toString());
                mp = new MediaPlayer();
                mp.setDataSource(getContext(), myUri);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepare(); //don't use prepareAsync for mp3 playback
                mp.start();
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        if (CommonUtil.audioArrayList.size() > position) {
                            position += position;
                            playAudio(CommonUtil.audioArrayList.get(position).getId(), true);
                        } else {
                            mp.stop();
                        }
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        seekBar.setEnabled(true);


        if (mp.isPlaying()) {
            mp.pause();
            playBtnImg.setImageResource(R.drawable.ic_pause);
        } else {
            mp.start();
            playBtnImg.setImageResource(R.drawable.ic_play);
           // seekBar.setMax(mp.getDuration());
           // new Thread(this).start();
        }

    }

    public void playNextAudio() {
        try {
            Toast.makeText(getActivity(), "next", Toast.LENGTH_SHORT).show();
        *//*    mp.release();
            mp = null;*//*

            if (CommonUtil.audioArrayList.size() > position) {
                playAudio(CommonUtil.audioArrayList.get(position + 1).getId(), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void playPreviousVideo() {
        try {
            Toast.makeText(getActivity(), "previous", Toast.LENGTH_SHORT).show();
            *//*mp.release();
            mp.stop();*//*
            if (position > 0) {
                playAudio(CommonUtil.audioArrayList.get(position - 1).getId(), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    */
/*    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }


    public void onStartTrackingTouch(SeekBar seekBar) {

    }


    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    public void run() {
        int currentPosition = poetMediaplayerService.mediaPlayer.getCurrentPosition();
        int total = poetMediaplayerService.mediaPlayer.getDuration();

        while (poetMediaplayerService.mediaPlayer != null && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = poetMediaplayerService.mediaPlayer.getCurrentPosition();
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                return;
            }
            seekBar.setProgress(currentPosition);
        }
    }*/


    public class PoetMediaplayerService implements Runnable, SeekBar.OnSeekBarChangeListener {

        private MediaPlayer mediaPlayer;
        private Boolean isMusicRunning = false;


        public void playAudio(String poetId) {
            seekBar.setMax(99);
            seekBar.setOnSeekBarChangeListener(this);
            try {
                if (isMusicRunning) {
                    stopAudio();
                    playBtnImg.setImageResource(R.drawable.ic_play);
                } else {
                    seekBar.setProgress(0);
                    seekBar.setEnabled(true);
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setDataSource("https://rekhta.org/Images/SiteImages/Audio/" + poetId + ".mp3");
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.prepare();
                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                        @Override
                        public void onPrepared(MediaPlayer mp) {

                            mp.start();
                            playBtnImg.setImageResource(R.drawable.ic_pause);
                            isMusicRunning = true;
                        }

                    });
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mediaPlayer) {
                            if (CommonUtil.audioArrayList.size() > position) {
                                position += position;
                                playAudio(CommonUtil.audioArrayList.get(position).getId());
                            } else {
                                stopAudio();
                            }
                        }
                    });
                    seekBar.setMax(mediaPlayer.getDuration());
                    new Thread(this).start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void stopAudio() {

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
                isMusicRunning = false;
            }
        }

        public void pauseAndPlayAudio() {

            if (mediaPlayer != null) {
                if (isMusicRunning) {
                    isMusicRunning = false;
                    mediaPlayer.pause();
                    playBtnImg.setImageResource(R.drawable.ic_play);
                    Toast.makeText(getActivity(), "Pause", Toast.LENGTH_SHORT).show();
                } else {
                    mediaPlayer.start();
                    playBtnImg.setImageResource(R.drawable.ic_pause);
                    Toast.makeText(getActivity(), "Play", Toast.LENGTH_SHORT).show();
                }
            }
        }


        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void run() {
            int currentPosition = mediaPlayer.getCurrentPosition();
            int total = mediaPlayer.getDuration();

            while (mediaPlayer != null && currentPosition < total) {
                try {
                    Thread.sleep(1000);
                    currentPosition = mediaPlayer.getCurrentPosition();
                } catch (InterruptedException e) {
                    return;
                } catch (Exception e) {
                    return;
                }
                seekBar.setProgress(currentPosition);
            }
        }
    }
}

