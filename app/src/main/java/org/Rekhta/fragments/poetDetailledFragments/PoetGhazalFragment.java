package org.Rekhta.fragments.poetDetailledFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.adapters.poetDetail.PoetGhazalAdapter;
import org.Rekhta.model.poetDetail.PoetGhazalData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PoetGhazalFragment extends Fragment {

    View view;

    PoetGhazalAdapter poetGhazalAdapter;
    ArrayList<PoetGhazalData> arrayList;
    RecyclerView poetghazalRecylerView;
    EditText poetAllserachEdittext;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    JSONArray dataArray = new JSONArray();

    boolean isLoading = false;
    private ProgressDialog progressLoader;

    private Context context;


    public PoetGhazalFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_poet_ghazal, container, false);
        arrayList = new ArrayList<>();
        context = getActivity();
        getPoetsGhazals(((PoetDetailedActivity) getActivity()).currentPoetId);
        poetghazalRecylerView = (RecyclerView) view.findViewById(R.id.poetghalrecylerview);
        poetghazalRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        poetghazalRecylerView.setItemViewCacheSize(20);
        poetghazalRecylerView.setDrawingCacheEnabled(true);
        poetghazalRecylerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        poetGhazalAdapter = new PoetGhazalAdapter(arrayList, getActivity(), totalNumberOfContent);
        poetghazalRecylerView.setAdapter(poetGhazalAdapter);
        ((PoetDetailedActivity) getActivity()).poetGhazalAdapter = poetGhazalAdapter;
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        poetghazalRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);


                if (!poetghazalRecylerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {
                        // Toast.makeText(context, "lessthan20", Toast.LENGTH_SHORT).show();

                    } else {
                        //    Toast.makeText(context, "greatwet", Toast.LENGTH_SHORT).show();
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getPoetsGhazals(((PoetDetailedActivity) getActivity()).currentPoetId);
                        }
                    }
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    public void getPoetsGhazals(String poetId) {

        isDataLoading = true;

        if (!((Activity) context).isFinishing()) {
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }
        RestClient.get().getContentListWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, poetId, "43d60a15-0b49-4caf-8b74-0fcdddeb9f83", "", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }

                isLoading = false;

                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        if (obj.getJSONObject("R").getJSONArray("CS").length() == 0) {
                            // do nothng
                        } else {
                            //arrayList.clear();
                            // TODO: 1/19/2018 Check if below line does not impact auto load if being performed earlier;
//                            dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("CS"));
                            dataArray = obj.getJSONObject("R").getJSONArray("CS");
                            totalNumberOfContent = obj.getJSONObject("R").getInt("TC");

                            if (pageIndex == 1) {
                                poetGhazalAdapter = new PoetGhazalAdapter(arrayList, getActivity(), totalNumberOfContent);
                                poetghazalRecylerView.setAdapter(poetGhazalAdapter);
                            }

                            for (int i = 0; i < dataArray.length(); i++) {

                                arrayList.add(new PoetGhazalData(dataArray.getJSONObject(i).getString("I"),
                                        dataArray.getJSONObject(i).getString("TE"), dataArray.getJSONObject(i).getString("TH"), dataArray.getJSONObject(i).getString("TU"),
                                        dataArray.getJSONObject(i).getBoolean("EC"), dataArray.getJSONObject(i).getBoolean("PC"), dataArray.getJSONObject(i).getBoolean("AU"), dataArray.getJSONObject(i).getBoolean("VI"),
                                        dataArray.getJSONObject(i).getString("UE"), dataArray.getJSONObject(i).getString("UH"), dataArray.getJSONObject(i).getString("UU")));
                            }
                            poetGhazalAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                    isDataLoading = false;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (progressLoader != null && progressLoader.isShowing()) {
            if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
        }

    }
}
