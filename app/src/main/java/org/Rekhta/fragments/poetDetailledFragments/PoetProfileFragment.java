package org.Rekhta.fragments.poetDetailledFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.Rekhta.R;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.model.poetDetail.PoetDetailData;
import org.Rekhta.utils.CommonUtil;


public class PoetProfileFragment extends Fragment {

    View view;

    public PoetProfileFragment() {
        // Required empty public constructor
    }

    TextView poetName, poetShortName, poetTenure, poetBirthPlace, poetDescription, poetTotalGhazal, poetTotalNazm, poetTotalSher, poetBioTxt;
    ImageView poetProfileImage;
    //Button poetProfileFollowButton;

    PoetDetailData poetDetailData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        view = inflater.inflate(R.layout.fragment_poet_profile, container, false);
        poetDetailData = ((PoetDetailedActivity) getActivity()).poetDetailData;
        initViews();
        return view;
    }

    public void initViews() {
        poetName = (TextView) view.findViewById(R.id.poetProfilePoetName);
        poetShortName = (TextView) view.findViewById(R.id.poetShortName);
        poetTenure = (TextView) view.findViewById(R.id.poetProfilePoetTenure);
        poetBirthPlace = (TextView) view.findViewById(R.id.poetProfileBirthPlace);
        poetDescription = (TextView) view.findViewById(R.id.poetProfilePoetDescription);
        poetTotalGhazal = (TextView) view.findViewById(R.id.poetProfileTotalGhazal);
        poetTotalNazm = (TextView) view.findViewById(R.id.poetProfileTotalNazm);
        poetTotalSher = (TextView) view.findViewById(R.id.poetProfileTotalSher);
        poetBioTxt = (TextView) view.findViewById(R.id.poetProfileBioText);

        poetTotalGhazal.setText(((PoetDetailedActivity) getActivity()).totalGhazalCount);
        poetTotalNazm.setText(((PoetDetailedActivity) getActivity()).totalNazmCount);
        poetTotalSher.setText(((PoetDetailedActivity) getActivity()).totalSherCount);

        poetProfileImage = (ImageView) view.findViewById(R.id.poetProfilePoetImg);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.default_profile);
        requestOptions.error(R.drawable.default_profile);
        Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load("https://rekhtacdn.azureedge.net/Images/Shayar/" + ((PoetDetailedActivity) getActivity()).currentPoetId + ".png").into(poetProfileImage);

        //poetProfileFollowButton = (Button) view.findViewById(R.id.poetProfileFollowBtn);
        setDataTofiels();

    }

    public void setDataTofiels() {

        String birthyear = poetDetailData.getPoetsDateOfBirth().toString();
        String deathyear = poetDetailData.getPoetsDateOfDeath().toString();

        if (poetDetailData.getPoetDescInEng().toLowerCase().toString().equalsIgnoreCase("null")) {
            poetDescription.setVisibility(View.INVISIBLE);
        }

        if (CommonUtil.languageCode == 1) {
            poetName.setText(poetDetailData.getPoetNameInEng());
            poetBirthPlace.setText(poetDetailData.getPoetBirthPlaceInEng());
            poetDescription.setText(poetDetailData.getPoetDescInEng());
            poetShortName.setText(poetDetailData.getPoetsRealNameInEng());
            poetTenure.setText(birthyear + " - " + deathyear + " | " + poetDetailData.getPoetBirthPlaceInEng());
            if (poetDetailData.getPoetBirthPlaceInEng().toLowerCase().toString().equalsIgnoreCase("null") || poetDetailData.getPoetBirthPlaceInEng().isEmpty()) {
//                poetBirthPlace.setVisibility(View.INVISIBLE);
                poetBirthPlace.setText("Not Available");
            }
            if (poetDetailData.getPoetDescInEng().toLowerCase().toString().equalsIgnoreCase("null") || poetDetailData.getPoetDescInEng().isEmpty()) {
//                poetDescription.setVisibility(View.INVISIBLE);
                poetDescription.setText("Not Available");
            }
        } else if (CommonUtil.languageCode == 2) {
            poetName.setText(poetDetailData.getPoetNameInHin());
            poetTenure.setText(birthyear + " - " + deathyear + " | " + poetDetailData.getPoetBirthPlaceInHin());

//            poetBirthPlace.setText(poetDetailData.getPoetBirthPlaceInHin());
//            poetDescription.setText(poetDetailData.getPoetDescInHin());
            poetShortName.setText(poetDetailData.getPoetsRealNameInHin());
            if (poetDetailData.getPoetBirthPlaceInHin().toLowerCase().toString().equalsIgnoreCase("null")) {
//                poetBirthPlace.setVisibility(View.INVISIBLE);

                poetBirthPlace.setText("Not Available");

            }
            if (poetDetailData.getPoetDescInHin().toLowerCase().toString().equalsIgnoreCase("null")) {
//                poetDescription.setVisibility(View.INVISIBLE);
            }
        } else {
            poetName.setText(poetDetailData.getPoetNameInUrdu());
            poetShortName.setText(poetDetailData.getPoetsRealNameInUrdu());
            poetTenure.setText(birthyear + " - " + deathyear + " | " + poetDetailData.getPoetBirthPlaceInUrdu());

//            poetBirthPlace.setText(poetDetailData.getPoetBirthPlaceInUrdu());
//            poetDescription.setText(poetDetailData.getPoetDescInUrdu());
            if (poetDetailData.getPoetBirthPlaceInUrdu().toLowerCase().toString().equalsIgnoreCase("null")) {
//                poetBirthPlace.setVisibility(View.INVISIBLE);
                poetBirthPlace.setText("Not Available");

            }
            if (poetDetailData.getPoetDescInUrdu().toLowerCase().toString().equalsIgnoreCase("null")) {
//                poetDescription.setVisibility(View.INVISIBLE);
                poetDescription.setText("Not Available");

            }
        }


        if (birthyear.equalsIgnoreCase("null") || (deathyear.equalsIgnoreCase("null") || birthyear.isEmpty() || (deathyear.isEmpty()))) {
            poetTenure.setText("Not Available");

        }
        // poetBioTxt.setText("Not Available");

    }

}
