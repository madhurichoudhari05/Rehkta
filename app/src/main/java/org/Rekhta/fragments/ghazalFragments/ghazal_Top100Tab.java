package org.Rekhta.fragments.ghazalFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.GhazalActivity;
import org.Rekhta.adapters.GhazalListAdapter;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageChangeListener;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ghazal_Top100Tab extends Fragment {

    View view;
    ListView listview;
    boolean isLoading = false;
    GhazalListAdapter ghazalTop100ListAdapter;
    JSONObject obj = new JSONObject();
    JSONArray dataArray = new JSONArray();
    int pageIndex = 1;
    int totalNumberOfContent;
    TextView top100descpTV, engheader;
    BroadcastReceiver mMessagRecevier;
    private TextView top100Txt;
    private ProgressDialog progressLoader;
    private Context context;

    public ghazal_Top100Tab() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        view = inflater.inflate(R.layout.fragment_ghazal_top100_tab, container, false);
        context = getActivity();
        ((GhazalActivity) getActivity()).setDrawerEnabled(true); // for enabling the Navigation Bar to Open Here
        listview = (ListView) view.findViewById(R.id.listviewTab1);

        View header = getActivity().getLayoutInflater().inflate(R.layout.ghazal_haeder, null);
        top100descpTV = (TextView) header.findViewById(R.id.top100Description);
        top100Txt = (TextView) header.findViewById(R.id.textView3);
        engheader = (TextView) header.findViewById(R.id.textView4);

        CommonUtil.setSpecificLanguage(top100descpTV, "100famousDescriotion", CommonUtil.languageCode);

        setHeaderFont();

        listview.addHeaderView(header);

        getTop100Ghazals();

        return view;
    }

    private void setHeaderFont() {


        if (CommonUtil.languageCode == 1) {
            engheader.setVisibility(View.VISIBLE);
            top100Txt.setText("Most Famous\nGhazals");
            CommonUtil.setOswaldRegular(context, top100descpTV);
            Typeface engttf = Typeface.createFromAsset(context.getAssets(), "fonts/Oswald-Regular.ttf");
            top100Txt.setTypeface(engttf);
            engheader.setTypeface(engttf);
        } else if (CommonUtil.languageCode == 2) {
            engheader.setVisibility(View.VISIBLE);
            top100Txt.setText(LanguageSelection.getSpecificLanguageWordById("100 Most famous ghazals", CommonUtil.languageCode));
            Typeface hindttf = Typeface.createFromAsset(context.getAssets(), "fonts/RozhaOne-Regular.ttf");
            top100Txt.setTypeface(hindttf);
            engheader.setTypeface(hindttf);
            CommonUtil.setRozaOne(context, top100descpTV);
        } else if (CommonUtil.languageCode == 3) {
            engheader.setVisibility(View.VISIBLE);
            top100Txt.setText(LanguageSelection.getSpecificLanguageWordById("100 Most famous ghazals", CommonUtil.languageCode));
            Typeface urttf = Typeface.createFromAsset(context.getAssets(), "fonts/aref-ruqaa.ttf");
            top100Txt.setTypeface(urttf);
            engheader.setTypeface(urttf);
            CommonUtil.setUrduRuqa(context, top100descpTV);
        }
    }


    public void getTop100Ghazals() {
        if (!((Activity) context).isFinishing()) {
            //show dialog
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }

        RestClient.get().getContentListWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "43d60a15-0b49-4caf-8b74-0fcdddeb9f83", "60e03626-6a76-481c-b4d6-cdd6173da417", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }

                isLoading = false;

                try {
                    obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        totalNumberOfContent = obj.getJSONObject("R").getInt("TC");
                        if (obj.getJSONObject("R").getJSONArray("CS").length() == 0) {
                            // do nothng
                        } else {

                            dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("CS"));
                            loadAdapter();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        mMessagRecevier = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                pageIndex = 1;
                setHeaderFont();
                CommonUtil.setSpecificLanguage(top100descpTV, "100famousDescriotion", CommonUtil.languageCode);

                if (ghazalTop100ListAdapter != null) {
                    ghazalTop100ListAdapter.notifyDataSetChanged();
                }
                //getTop100Ghazals();


            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessagRecevier, new IntentFilter("Truoble"));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessagRecevier);
    }

    public void updateListOnLangChange() {
        ghazalTop100ListAdapter.updateData(dataArray);
    }

    public void loadAdapter() {
        if (pageIndex == 1) {
            ghazalTop100ListAdapter = new GhazalListAdapter(getActivity(), dataArray);
            listview.setAdapter(ghazalTop100ListAdapter);
            ((GhazalActivity) getActivity()).currentActiveGhazalTop100Adapter = ghazalTop100ListAdapter;
        } else {
            ghazalTop100ListAdapter.updateData(dataArray);
            listview.setSelection(pageIndex * 20 - 25);
        }
        CommonUtil.languageChangeListener.setListener(new LanguageChangeListener.ChangeListener() {
            @Override
            public void onChange() {
                updateListOnLangChange();
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {

                    if (position != 0) {

                       /* Bundle bundle = new Bundle();
                        bundle.putString("data", dataArray.get(position - 1).toString());
                        bundle.putInt("position",position-1);
                        if(position == 1){
                            bundle.putString("previousData", "false");
                        }else{
                            bundle.putString("previousData", dataArray.get(position - 2).toString());
                        }
                        if(position == dataArray.length()){
                            bundle.putString("nextData", "false");
                        }else{
                            bundle.putString("nextData", dataArray.get(position ).toString());
                        }*/


                        ((GhazalActivity) getActivity()).currentData = dataArray;
                        ((GhazalActivity) getActivity()).currentIndexOfContent = position - 1;
                        GhazalContent ghazalDetails = new GhazalContent();
//                        ghazalDetails.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.drawer_layout, ghazalDetails, "findThisFragment")
                                .addToBackStack(null)
                                .commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        listview.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listview.getAdapter() == null)
                    return;

                if (listview.getAdapter().getCount() == 0)
                    return;

                int l = visibleItemCount + firstVisibleItem;
                if (l >= totalItemCount && !isLoading) {
                    // It is time to add new data. We call the listener
                    if (pageIndex < 6) {
                        pageIndex = pageIndex + 1;
                        isLoading = true;
                        loadData();
                    } else {
                        // Toast.makeText(getActivity(), "Complete" + pageIndex, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public void loadData() {
        if (pageIndex <= CommonUtil.calulateTotalApiCall(totalNumberOfContent)) {
            getTop100Ghazals();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressLoader != null && progressLoader.isShowing()) {
            if (progressLoader != null && progressLoader.isShowing()) {
                progressLoader.dismiss();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressLoader != null && progressLoader.isShowing()) {
            if (progressLoader != null && progressLoader.isShowing()) {
                progressLoader.dismiss();
            }
        }
    }
}
