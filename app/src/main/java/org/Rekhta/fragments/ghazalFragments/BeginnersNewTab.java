package org.Rekhta.fragments.ghazalFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import org.Rekhta.R;;
import org.Rekhta.activites.GhazalActivity;
import org.Rekhta.adapters.GhazalListAdapter;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageChangeListener;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeginnersNewTab extends Fragment {


    int pageIndex = 1;
    int totalNumberOfContent = 0;
    private ListView beginnersNewList;
    private TextView headerTxt;
    private BroadcastReceiver mMessagRecevier;
    private boolean isLoading;
    private JSONObject jsonObject;
    private JSONArray dataArray;
    private GhazalListAdapter ghazalBeginnerListAdapter;

    public BeginnersNewTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_beginners_new_tab, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        beginnersNewList = (ListView) getView().findViewById(R.id.beggineresNewList);

        View header = getActivity().getLayoutInflater().inflate(R.layout.ghazal_beginner_header, null);
        headerTxt = (TextView) header.findViewById(R.id.textView4Satire);
        headerTxt.setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode));
        beginnersNewList.addHeaderView(header);
        setHeaderFont();

        dataArray = new JSONArray();

        getBegginersData();
    }

    private void setHeaderFont() {

        if (CommonUtil.languageCode == 1) {
            CommonUtil.setOswaldRegular(getActivity(), headerTxt);
        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setRozaOne(getActivity(), headerTxt);
        } else if (CommonUtil.languageCode == 3) {

            CommonUtil.setUrduRuqa(getActivity(), headerTxt);
        }

    }

    private void getBegginersData() {

        RestClient.get().getContentListWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                "", "43d60a15-0b49-4caf-8b74-0fcdddeb9f83", "BFCE2895-551E-4C07-B9D7-F7FF0DA9F9F5", "", pageIndex, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap hashMap, Response response) {

                        isLoading = false;
                        try {
                            jsonObject = new JSONObject(hashMap);
                            if (jsonObject.getInt("S") == 1) {
                                totalNumberOfContent = jsonObject.getJSONObject("R").getInt("TC");
                                dataArray = CommonUtil.concatArray(dataArray, jsonObject.getJSONObject("R").getJSONArray("CS"));
                                loadAdapter();


                            } else {
                                Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                            }

                            System.out.print("-----------------------------------------------------------");
                            System.out.print(response);
                            System.out.print(hashMap);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
    }

    private void loadAdapter() {


        if (pageIndex == 1) {
            ghazalBeginnerListAdapter = new GhazalListAdapter(getActivity(), dataArray);
            beginnersNewList.setAdapter(ghazalBeginnerListAdapter);
            ((GhazalActivity) getActivity()).currentActiveNewBegginerAdapter = ghazalBeginnerListAdapter;
        } else {
            ghazalBeginnerListAdapter.updateData(dataArray);
            beginnersNewList.setSelection(pageIndex * 20 - 25);
        }
        CommonUtil.languageChangeListener.setListener(new LanguageChangeListener.ChangeListener() {
            @Override
            public void onChange() {
                updateListOnLangChange();

            }
        });

        beginnersNewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                try {
                    if (position != 0) {

                        ((GhazalActivity) getActivity()).currentData = dataArray;
                        ((GhazalActivity) getActivity()).currentIndexOfContent = position - 1;
                        GhazalContent ghazalDetails = new GhazalContent();
//                    ghazalDetails.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.drawer_layout, ghazalDetails, "findThisFragment")
                                .addToBackStack(null)
                                .commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        beginnersNewList.setOnScrollListener(new AbsListView.OnScrollListener() {

            int mLastFirstVisibleItem = 0;

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

                if (absListView.getId() == beginnersNewList.getId()) {
                    final int currentFirstVisibleItem = beginnersNewList.getFirstVisiblePosition();
                    if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                        pageIndex = pageIndex + 1;
                        isLoading = true;
                        loadData();
                        Log.i("a", "scrolling down...");
                    } else {
                        Log.i("a", "scrolling up...");
                    }

                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }
        });

    }

    private void loadData() {

        if (pageIndex <= CommonUtil.calulateTotalApiCall(totalNumberOfContent)) {
            getBegginersData();
        } else {
            // Toast.makeText(getActivity(), "No Data to load", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateListOnLangChange() {

        ghazalBeginnerListAdapter.updateData(dataArray);
    }

    @Override
    public void onResume() {
        super.onResume();

        mMessagRecevier = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                pageIndex = 1;
                //Toast.makeText(context, "Tourble Arrived", Toast.LENGTH_SHORT).show();
                headerTxt.setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode));
                if (ghazalBeginnerListAdapter != null) {
                    ghazalBeginnerListAdapter.notifyDataSetChanged();
                }
                //getBegginersData();
                setHeaderFont();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessagRecevier, new IntentFilter("Truoble"));
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessagRecevier);
    }
}
