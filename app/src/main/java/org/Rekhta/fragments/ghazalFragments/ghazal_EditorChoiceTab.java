package org.Rekhta.fragments.ghazalFragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.GhazalActivity;
import org.Rekhta.adapters.GhazalListAdapter;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageChangeListener;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ghazal_EditorChoiceTab extends Fragment {

    View view;
    ListView listview;
    boolean isLoading = false;
    GhazalListAdapter ghazalEditorChoiceListAdapter = null;
    JSONObject obj = new JSONObject();
    JSONArray dataArray = new JSONArray();
    int pageIndex = 1;
    int totalNumberOfContent = 0;
    ProgressBar progressLoder;
    private TextView headerTxt;
    private BroadcastReceiver mMessagRecevier;

    public ghazal_EditorChoiceTab() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setRetainInstance(true);
        view = inflater.inflate(R.layout.fragment_ghazal_editorchoice_tab, container, false);
        ((GhazalActivity) getActivity()).setDrawerEnabled(true); // for enabling the Navigation Bar to Open Here
        listview = (ListView) view.findViewById(R.id.listviewTab3);
        View header = getActivity().getLayoutInflater().inflate(R.layout.ghazal_editorchoice_header, null);
        headerTxt = (TextView) header.findViewById(R.id.textView4Choice);
        headerTxt.setText(LanguageSelection.getSpecificLanguageWordById("Editor's Choice", CommonUtil.languageCode));
        listview.addHeaderView(header);
        setHeaderFont();
        progressLoder = (ProgressBar) view.findViewById(R.id.progressBar2);
        getEditorsChoiceGhazals();
        return view;
    }

    private void setHeaderFont() {

        if (CommonUtil.languageCode == 1) {
            CommonUtil.setOswaldRegular(getActivity(), headerTxt);
        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setRozaOne(getActivity(), headerTxt);
        } else if (CommonUtil.languageCode == 3) {

            CommonUtil.setUrduRuqa(getActivity(), headerTxt);
        }
    }

    public void getEditorsChoiceGhazals() {

        // final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);

        RestClient.get().getContentListWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "43d60a15-0b49-4caf-8b74-0fcdddeb9f83", "13ywdpLJ9iEA93BtCwez2w8zXFPWxoDota", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                //if (progressLoader != null && progressLoader.isShowing())                    progressLoader.dismiss();
                isLoading = false;
                progressLoder.setVisibility(View.GONE);
                listview.setVisibility(View.VISIBLE);
                try {
                    obj = new JSONObject(res);
                    isLoading = false;
                    if (obj.getInt("S") == 1) {
                        totalNumberOfContent = obj.getJSONObject("R").getInt("TC");
                        if (obj.getJSONObject("R").getJSONArray("CS").length() == 0) {
                            // Do Nothing
                        } else {
                            dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("CS"));
                            loadAdapter();
                        }


                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                //progressLoader.dismiss();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        mMessagRecevier = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                pageIndex = 1;
                if (headerTxt != null)
                    headerTxt.setText(LanguageSelection.getSpecificLanguageWordById("Editor's Choice", CommonUtil.languageCode));
                setHeaderFont();
                //getEditorsChoiceGhazals();
                if (ghazalEditorChoiceListAdapter != null) {
                    ghazalEditorChoiceListAdapter.notifyDataSetChanged();
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessagRecevier, new IntentFilter("Truoble"));
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessagRecevier);
    }

    public void updateListOnLangChange() {
        ghazalEditorChoiceListAdapter.updateData(dataArray);
    }

    public void loadAdapter() {
        if (pageIndex == 1) {
            ghazalEditorChoiceListAdapter = new GhazalListAdapter(getActivity(), dataArray);
            listview.setAdapter(ghazalEditorChoiceListAdapter);
            ((GhazalActivity) getActivity()).currentActiveGhazalEditorsAdapter = ghazalEditorChoiceListAdapter;
        } else {
            ghazalEditorChoiceListAdapter.updateData(dataArray);
            listview.setSelection(pageIndex * 20 - 25);
        }

        CommonUtil.languageChangeListener.setListener(new LanguageChangeListener.ChangeListener() {
            @Override
            public void onChange() {
                ghazalEditorChoiceListAdapter.updateData(dataArray);
            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (position != 0) {

                  /*      Bundle bundle = new Bundle();
                        bundle.putString("data", dataArray.get(position - 1).toString());
                        if (position == 1) {
                            bundle.putString("previousData", "false");
                        } else {
                            bundle.putString("previousData", dataArray.get(position-2).toString());
                        }
                        if (position == dataArray.length()) {
                            bundle.putString("nextData", "false");
                        } else {
                            bundle.putString("nextData", dataArray.get(position ).toString());
                        }*/
                        ((GhazalActivity) getActivity()).currentData = dataArray;
                        ((GhazalActivity) getActivity()).currentIndexOfContent = position - 1;

                        GhazalContent ghazalDetails = new GhazalContent();
//                        ghazalDetails.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.drawer_layout, ghazalDetails, "ghazalDetails")
                                .addToBackStack(null)
                                .commit();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            int mLastFirstVisibleItem = 0;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (view.getId() == listview.getId()) {
                    final int currentFirstVisibleItem = listview.getFirstVisiblePosition();
                    if (currentFirstVisibleItem > mLastFirstVisibleItem && !isLoading) {

                        pageIndex = pageIndex + 1;
                        isLoading = true;
                        loadData();
                        Log.i("a", "scrolling down...");
                    } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                        Log.i("a", "scrolling up...");
                    }

                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listview.getAdapter() == null)
                    return;

                if (listview.getAdapter().getCount() == 0)
                    return;

                int l = visibleItemCount + firstVisibleItem;

                if (l >= totalItemCount && !isLoading) {
                    // It is time to add new data. We call the listener
                   /* if (pageIndex < 6) {
                        pageIndex = pageIndex + 1;
                        isLoading = true;
                        loadData();
                    } else {
                        Toast.makeText(getActivity(), "Complete" + pageIndex, Toast.LENGTH_SHORT).show();
                    }*/
                }
            }
        });

    }

    public void loadData() {
        if (pageIndex <= CommonUtil.calulateTotalApiCall(totalNumberOfContent)) {
            getEditorsChoiceGhazals();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }


}
