package org.Rekhta.fragments.myFavorite;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.activites.MyFavoritesActivity;
import org.Rekhta.adapters.myFavorite.HistryAdapter;
import org.Rekhta.model.historyModels.HistoryMainPojo;
import org.Rekhta.model.historyModels.HistoryRPojo;
import org.Rekhta.model.myFavorite.SongAndAuther;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class FragmentHistry extends Fragment {

    HistryAdapter mHistryAdapter;
    Context context;
    String accessToken = "Bearer jYcqjxKUiu4prCL2ZLjyAwz1Y4e4bYnLgOSxFbiB7FcZrCglhFYSuqJvpMyXV7ubM1zrcB_5wpZ6Qd5SmpPQM_ApwnL61_mLt1J0D3mQGAjJPPGmqkK5JlDli-v-2Z3cjVi4VKj9BLCFK-LNgMpimTD_ZWyFYwksq_KCzBbkMoNCCU7yBNtJozUw_sYHLCkXv3zHHSyGMeNTTyXs0gQdvVzs9CPcT8K3UgBlbCLVQD4KF5s_DZKJ5Mji0Qd092UqZB2n2TbJS-49ARukrGLlGAvCgGvCdN0XdSkjNHsRKWoTXCKC_GLkKBV9ab5kOK-BDLvF1rVvJBdnSvg_luAInM6aGgaCmM7vSw8DPlCFLoqFiqSQwWNlwTD3v_DBSgHJT5VF1zQGBpN_SA7yZVqLBaPtJeBd2exmfHZYpmc5UH6sWgFGvrIaXwStq7u5Lg1ECdiHNogUg4GqZaEkVpzAiy2DFq1w4Ux8se8xSujLIp0";
    RecyclerView recyclerView;
    private ArrayList<SongAndAuther> arrayList = new ArrayList<>();

    public FragmentHistry() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_histry, container, false);
        context = getActivity();
        recyclerView = (RecyclerView) view.findViewById(R.id.f_histry_recyclerView);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //addItemDecoration(new DividerItemDecoration(context,LinearLayoutManager.VERTICAL));

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        prepareData();
    }

    private void prepareData() {


        RestClient.get().getHistoryData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, accessToken, new Callback<HistoryMainPojo>() {
            @Override
            public void success(HistoryMainPojo historyMainPojo, Response response) {

                if (historyMainPojo != null) {

                    List<HistoryRPojo> historyRPojos = historyMainPojo.getR();
                    mHistryAdapter = new HistryAdapter(context, historyRPojos);
                    ((MyFavoritesActivity) getActivity()).histryAdapter = mHistryAdapter;
                    recyclerView.setAdapter(mHistryAdapter);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });


        SongAndAuther data = new SongAndAuther();
        data.setSongName("tere bina raha na jaye  ");
        data.setAutherName("A r rehman");
        arrayList.add(data);

        data = new SongAndAuther();
        data.setSongName("Aye mere dil ");
        data.setAutherName("kishore kumar");
        arrayList.add(data);


    }


}
