package org.Rekhta.fragments.myFavorite;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.Rekhta.R;
import org.Rekhta.adapters.FavPagerAdapter;
import org.Rekhta.adapters.MyGhazalAdapter;
import org.Rekhta.adapters.myFavorite.MyFavNazamAdapter;
import org.Rekhta.adapters.myFavorite.MyFavSherAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.GhazalPagerAdapter;
import org.Rekhta.utils.LanguageSelection;


public class FragmentMyConnections extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;


    boolean isOffline;
    private FavPagerAdapter adapter;
    private BroadcastReceiver broadcastReceiver;


    public FragmentMyConnections() {

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_myconnectiins_tab, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        tabLayout = (TabLayout) getView().findViewById(R.id.innerTabLayout);
        viewPager = (ViewPager) getView().findViewById(R.id.favPager);


        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode)));
        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode)));
        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        //tabLayout.setupWithViewPager(viewPager);


        adapter = new FavPagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyPref", 0);
        isOffline = sharedPreferences.getBoolean("isOffline", false);

    }

    @Override
    public void onResume() {
        super.onResume();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                setTabNames();
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("ReloadFav"));
    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    private void setTabNames() {

        tabLayout.getTabAt(0).setText(LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode));
        tabLayout.getTabAt(1).setText(LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode));
        tabLayout.getTabAt(2).setText(LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode));
    }

}
