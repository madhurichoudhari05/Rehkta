package org.Rekhta.fragments.myFavorite;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.Rekhta.adapters.myFavorite.HistryAdapter;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.adapters.myFavorite.MyGhazalAdapter;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.model.myFavorite.FavRPojo;
import org.Rekhta.model.myFavorite.OnlineFavModelMain;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyFavoriteGhzals extends Fragment implements NavigationFragment.NavigationDrawerCallbacks {


    boolean loadingWithServer = false;
    int firstCall = 0;
    private RecyclerView myGhazalRecycler;
    private AppDatabase appDatabase;
    private Context context;
    private boolean isOffline;
    private MyGhazalAdapter myGhazalAdapter;
    private ArrayList<FavFCModel> favGhazalsList, updatedGhazalList;
    private ArrayList<FavFCModel> favNazamsList;
    private ArrayList<FavFCModel> favSherList;
    private String ghazalId = "43d60a15-0b49-4caf-8b74-0fcdddeb9f83";
    private String nazamId = "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b";
    private String sherId = "f722d5dc-45da-41ec-a439-900df702a3d6";
    private ProgressDialog progressDialog, onlineDilaog;
    private int lastCompletelyVisibleItemPosition;
    private int pageNumber = 0, totalPages;
    private int totalItems;
    private boolean isLoading;
    View view;
    BroadcastReceiver broadcastReceiver;
    HistryAdapter histryAdapter;


    public MyFavoriteGhzals() {
        // Required empty public constructor
    }

    public static MyFavoriteGhzals newInstance() {
        MyFavoriteGhzals fragment = new MyFavoriteGhzals();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_my_favorite_ghzals, container, false);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        favGhazalsList = new ArrayList<FavFCModel>();
        updatedGhazalList = new ArrayList<FavFCModel>();
        favNazamsList = new ArrayList<FavFCModel>();
        favSherList = new ArrayList<FavFCModel>();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");


        // isOffline = getArguments().getBoolean("connectStatus");

        onlineDilaog = new ProgressDialog(getActivity());
        onlineDilaog.setMessage("Please wait...");


        appDatabase = MyApplicationClass.getDb();
        myGhazalRecycler = (RecyclerView) view.findViewById(R.id.myGhazalRecycler);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        myGhazalRecycler.setLayoutManager(linearLayoutManager);

        myGhazalRecycler.setNestedScrollingEnabled(true);


       // getFavFromServer();

        myGhazalRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                firstCall = 1;

                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        if (pageNumber < totalPages) {
                            pageNumber = pageNumber + 1;

                            if (onlineDilaog != null)
                                onlineDilaog.show();
                            getFavFromServer();
                        }
                    }
                }




                /*if (recyclerView.canScrollVertically(View.SCROLL_AXIS_VERTICAL)) {
                    lastCompletelyVisibleItemPosition = ((LinearLayoutManager) myGhazalRecycler.getLayoutManager()).findLastVisibleItemPosition();
                    totalItems = myGhazalRecycler.getAdapter().getItemCount();

                    if (lastCompletelyVisibleItemPosition == totalItems - 1) {
                        if (!loadingWithServer) {
                            loadingWithServer = true;
                            

                        }
                    }
                }*/

            }
        });

        if (CommonUtil.isOffline) {
            progressDialog.show();
            new GetLocalGhazals().execute();
        } else if (!isOffline) {

            onlineDilaog.show();
            getFavFromServer();

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;

    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter("ReloadFav");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (!CommonUtil.isOffline) {
                    // getFavFromServer();
                    if (myGhazalAdapter != null)
                        myGhazalAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "This is featured is not supported in offline mode", Toast.LENGTH_SHORT).show();
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, intentFilter);
    }

    private void getFavFromServer() {

        onlineDilaog.show();
        String langCode = String.valueOf(CommonUtil.languageCode);
        RestClient.get().getFavData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                CommonUtil.SharedPrefKeys.rekhtasAccessToken, String.valueOf(pageNumber), new Callback<OnlineFavModelMain>() {
                    @Override
                    public void success(OnlineFavModelMain onlineFavModelMain, Response response) {

                        if (onlineFavModelMain != null) {
                            FavRPojo favRPojo = onlineFavModelMain.getR();
                            String totalCount = favRPojo.getTC();
                            totalPages = (Integer.parseInt(totalCount) / 20);
                            List<FavFCModel> favCollection = favRPojo.getFC();

                            for (int i = 0; i < favCollection.size(); i++) {

                                String typeId = favCollection.get(i).getT();
                                if (typeId.equals(ghazalId)) {
                                    favGhazalsList.add(favCollection.get(i));
                                }

                            }
                            updatedGhazalList.addAll(favGhazalsList);

                            if (updatedGhazalList.size() <= 5) {
                                pageNumber = pageNumber + 1;
                                favGhazalsList.clear();
                                getFavFromServer();
                            } else {
                                onlineDilaog.dismiss();
                                if (firstCall == 0) {
                                    myGhazalAdapter = new MyGhazalAdapter(context, null, updatedGhazalList, MyFavoriteGhzals.this);
                                    myGhazalRecycler.setAdapter(myGhazalAdapter);
                                } else if (firstCall == 1) {
                                    myGhazalAdapter.notifyDataSetChanged();
                                }
                            }


                            Log.v("ListSizes", "ghzalaList = " + favGhazalsList.size() + " NazamList = " + favNazamsList.size() + " sherList = " + favSherList.size());

                        } else {
                            if (onlineDilaog != null)
                                onlineDilaog.dismiss();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }

    public void removeItem(final List<AddGhazal> addGhazals, final int position, final AppDatabase appDatabase) {

        if (isOffline) {
            class RemoveGhazal extends AsyncTask<Void, Void, String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("removing item....");
                    progressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {

                    appDatabase.ghazalDao().delete(addGhazals.get(position));
                    addGhazals.remove(position);

                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    progressDialog.dismiss();
                    myGhazalAdapter.notifyItemRemoved(position);
                    Toast.makeText(context, "Removed from favorite", Toast.LENGTH_SHORT).show();
                }
            }

            new RemoveGhazal().execute();
        } else if (!isOffline) {
            if (onlineDilaog != null)
                onlineDilaog.show();
            CommonUtil.removeFromFavTroughApi(getActivity(), updatedGhazalList.get(position).getI());
            CommonUtil.removeFromFavPref(updatedGhazalList.get(position).getI());
            updatedGhazalList.remove(position);
            myGhazalAdapter.notifyItemRemoved(position);

            onlineDilaog.dismiss();

        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void langChanged() {

        int lng = CommonUtil.languageCode;

    }

    public class GetLocalGhazals extends AsyncTask<Void, Void, List<AddGhazal>> {

        @Override
        protected List<AddGhazal> doInBackground(Void... voids) {

            List<AddGhazal> addGhazals = appDatabase.ghazalDao().getAll();
            Log.v("GhazalSize", String.valueOf(addGhazals.size()));

            return addGhazals;
        }

        @Override
        protected void onPostExecute(List<AddGhazal> addGhazals) {
            super.onPostExecute(addGhazals);
            progressDialog.dismiss();
            if (addGhazals != null && addGhazals.size() != 0) {

                myGhazalAdapter = new MyGhazalAdapter(context, addGhazals, favGhazalsList, MyFavoriteGhzals.this);
                myGhazalRecycler.setAdapter(myGhazalAdapter);
            } else {
                Toast.makeText(getActivity(), "Not found", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
