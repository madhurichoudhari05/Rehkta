package org.Rekhta.fragments.myFavorite;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.adapters.myFavorite.MyFavNazamAdapter;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.model.myFavorite.FavRPojo;
import org.Rekhta.model.myFavorite.OnlineFavModelMain;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MyNazamFagment extends Fragment {

    Context context;
    RecyclerView myFavNazamRecycler;
    MyFavNazamAdapter myFavNazamAdapter;
    ProgressDialog progressDialog;
    int count;
    List<AddNazam> nazamList;
    LinearLayoutManager linearLayoutManager;
    ProgressDialog nazamDialog;
    boolean isOffline;
    private AppDatabase db;
    private ArrayList<FavFCModel> favGhazalsList;
    private ArrayList<FavFCModel> favNazamsList, updatedNazamList;
    private ArrayList<FavFCModel> favSherList;
    private HandlerThread handlerThread;
    private int pageNumber = 0;
    private String ghazalId = "43d60a15-0b49-4caf-8b74-0fcdddeb9f83";
    private String nazamId = "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b";
    private String sherId = "f722d5dc-45da-41ec-a439-900df702a3d6";
    private boolean isLoading = false;
    private int totalPages;
    private int firstCall = 0;
    BroadcastReceiver broadcastReceiver;


    public MyNazamFagment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MyNazamFagment newInstance() {
        MyNazamFagment fragment = new MyNazamFagment();

        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!CommonUtil.isOffline) {
                    // getFavFromServer();
                    myFavNazamAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "This is featured is not supported in offline mode", Toast.LENGTH_SHORT).show();
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("ReloadFav"));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_nazam_fagment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        isOffline = getArguments().getBoolean("connectStatus");

        nazamDialog = new ProgressDialog(getActivity());
        nazamDialog.setMessage("Please wait...");

        myFavNazamRecycler = (RecyclerView) getView().findViewById(R.id.myFavNazamRecycler);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        myFavNazamRecycler.setLayoutManager(linearLayoutManager);
        myFavNazamRecycler.setNestedScrollingEnabled(true);

        favGhazalsList = new ArrayList<FavFCModel>();
        updatedNazamList = new ArrayList<FavFCModel>();
        favNazamsList = new ArrayList<FavFCModel>();
        favSherList = new ArrayList<FavFCModel>();

        myFavNazamRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();


                if (!CommonUtil.isOffline) {
                    firstCall = 1;

                    if (!isLoading) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                && firstVisibleItemPosition >= 0) {
                            if (pageNumber < totalPages) {
                                pageNumber = pageNumber + 1;

                                if (nazamDialog != null)
                                    nazamDialog.show();
                                getFavFromServer();
                            }
                        }
                    }
                }


            }
        });


        handlerThread = new HandlerThread("");
        // getFavFromServer();

        if (CommonUtil.isOffline) {
            new NazamAsync().execute();
        } else if (!CommonUtil.isOffline) {

            nazamDialog.show();
            getFavFromServer();
        }


    }

    private void getFavFromServer() {

        String langCode = String.valueOf(CommonUtil.languageCode);
        RestClient.get().getFavData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                CommonUtil.SharedPrefKeys.rekhtasAccessToken, String.valueOf(pageNumber), new Callback<OnlineFavModelMain>() {
                    @Override
                    public void success(OnlineFavModelMain onlineFavModelMain, Response response) {

                        if (onlineFavModelMain != null) {
                            FavRPojo favRPojo = onlineFavModelMain.getR();
                            String totalCount = favRPojo.getTC();
                            totalPages = (Integer.parseInt(totalCount) / 20);
                            List<FavFCModel> favCollection = favRPojo.getFC();

                            for (int i = 0; i < favCollection.size(); i++) {

                                String typeId = favCollection.get(i).getT();
                                if (typeId.equals(ghazalId)) {

                                    // favGhazalsList.add(favCollection.get(i));
                                } else if (typeId.equals(nazamId)) {
                                    favNazamsList.add(favCollection.get(i));
                                } else if (typeId.equals(sherId)) {
                                    // favSherList.add(favCollection.get(i));
                                }


                            }
                            updatedNazamList.addAll(favNazamsList);

                            count = favNazamsList.size() + favSherList.size() + favGhazalsList.size();
                            if (updatedNazamList.size() <= 5) {
                                favNazamsList.clear();
                                pageNumber = pageNumber + 1;
                                getFavFromServer();
                            } else {
                                nazamDialog.dismiss();
                                if (firstCall == 0) {
                                    if (updatedNazamList.size() != 0) {
                                        myFavNazamAdapter = new MyFavNazamAdapter(context, nazamList, updatedNazamList, MyNazamFagment.this);
                                        myFavNazamRecycler.setAdapter(myFavNazamAdapter);
                                    } else {
                                        myFavNazamRecycler.setAdapter(null);
                                    }
                                } else if (firstCall == 1) {
                                    myFavNazamAdapter.notifyDataSetChanged();
                                }

                            }


                            // Log.v("ListSizes", "ghzalaList = " + favGhazalsList.size() + " NazamList = " + favNazamsList.size() + " sherList = " + favSherList.size());
                            if (nazamDialog != null && nazamDialog.isShowing())
                                nazamDialog.dismiss();
                        } else {
                            if (nazamDialog != null)
                                nazamDialog.dismiss();

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (progressDialog != null)
                            progressDialog.dismiss();
                    }
                });
    }

    ;

    public void rempveItem(final List<AddNazam> nazamList, final int position, final AppDatabase appDatabase) {

       /* if (isOffline) {
            class RemoveData extends AsyncTask<Void, Void, String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("removing item....");
                    progressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {

                    appDatabase.userDao().delete(nazamList.get(position));
                    nazamList.remove(nazamList.get(position));


                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    progressDialog.dismiss();
                    myFavNazamAdapter.notifyItemRemoved(position);
                }
            }
            new RemoveData().execute();
        } else if (!isOffline) {

            if (nazamDialog != null)
                nazamDialog.show();
            CommonUtil.removeFromFavTroughApi(getActivity(), updatedNazamList.get(position).getI());
            CommonUtil.removeFromFavPref(updatedNazamList.get(position).getI());
            updatedNazamList.remove(position);
            myFavNazamAdapter.notifyItemRemoved(position);

            nazamDialog.dismiss();
        }*/

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public class NazamAsync extends AsyncTask<Void, Void, List<AddNazam>> {

        @Override
        protected List<AddNazam> doInBackground(Void... voids) {

            db = MyApplicationClass.getDb();
            nazamList = db.userDao().getAll();

            return nazamList;
        }

        @Override
        protected void onPostExecute(List<AddNazam> nazamList) {
            super.onPostExecute(nazamList);
            if (nazamList != null && nazamList.size() != 0) {

                myFavNazamAdapter = new MyFavNazamAdapter(context, nazamList, updatedNazamList, MyNazamFagment.this);
                myFavNazamRecycler.setAdapter(myFavNazamAdapter);
            } else {
                Toast.makeText(getActivity(), "Not  found", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
