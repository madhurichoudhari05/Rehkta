package org.Rekhta.fragments.myFavorite;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.adapters.ImageShayari.SavedImageShayriGridAdapter;
import org.Rekhta.adapters.myFavorite.EqualSpacingItemDecoration;
import org.Rekhta.adapters.myFavorite.FavoriteSavedImageAdapter;

/**
 * Created by RAJESH on 11/27/2017.
 */

public class FragmentSavedImages extends Fragment {
    GridView grid;
    File imageDir;
    File[] mediaFiles;
    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    ArrayList<Bitmap> bmpArray = new ArrayList<Bitmap>();
    ArrayList<String> fileName = new ArrayList<String>();
    int[] images = {R.drawable.ic_close, R.drawable.ic_close, R.drawable.ic_close, R.drawable.ic_close, R.drawable.ic_close, R.drawable.ic_close};
    private FavoriteSavedImageAdapter favoriteSavedImageAdapter;
    private Context mcontext;
    private static final int REQUEST_WRITE_PERMISSION = 323;


    public FragmentSavedImages() {
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_myfavorite_saved_images, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        int numberOfColumns = 3;
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getActivity(), numberOfColumns);
        recyclerView.setLayoutManager(layoutManager);
       // recyclerView.addItemDecoration(new EqualSpacingItemDecoration(2));

        List<String> input = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            input.add("asd" + i);
        }
        requestPermission();


        return view;
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            getImagesFromStorage();
        }
    }

    private void getImagesFromStorage() {
        imageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "Rekhta");
        if ((imageDir.exists())) {
            mediaFiles = imageDir.listFiles();

            if ( mediaFiles==null || mediaFiles.length==0) {
                Toast.makeText(getActivity(), "No Saved Images Found", Toast.LENGTH_LONG).show();
            } else {
                for (File file : mediaFiles) {

                    bmpArray.add(convertToBitmap(file));
                    fileName.add(readFileName(file));
                    //Log.d(TAG + "bmpArray Size", ""+bmpArray.size());
                    //Log.d(TAG, "call to convertToBitmap");
                }//for

                favoriteSavedImageAdapter = new FavoriteSavedImageAdapter(bmpArray, fileName, getActivity());
                recyclerView.setAdapter(favoriteSavedImageAdapter);
            }


            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.fromFile(imageDir);
                scanIntent.setData(contentUri);
                getActivity().sendBroadcast(scanIntent);
            } else {
                final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()));
                getActivity().sendBroadcast(intent);
            }*/

        } else {

        }

       /* if (swipeLayout != null) {
            swipeLayout.setRefreshing(false);
        }*/
    }

    private String readFileName(File file) {

        String name = file.getName();
        return name;
    }

    private Bitmap convertToBitmap(File file) {


        URL url = null;
        try {
            url = file.toURL();
        } catch (MalformedURLException e1) {
            //Log.d(TAG, e1.toString());
        }//catch

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openStream());
            //bmp.recycle();
        } catch (Exception e) {
            //Log.d(TAG, "Exception: "+e.toString());
        }//catch
        return bmp;
    }

    // set LayoutManager to RecyclerView

}
