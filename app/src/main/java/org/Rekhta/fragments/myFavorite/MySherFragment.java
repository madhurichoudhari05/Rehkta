package org.Rekhta.fragments.myFavorite;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.adapters.myFavorite.MyFavSherAdapter;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.model.LocalModels.AddSher;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.model.myFavorite.FavRPojo;
import org.Rekhta.model.myFavorite.OnlineFavModelMain;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MySherFragment extends Fragment {

    RecyclerView mySherRecycler;

    AppDatabase appDatabase;
    MyFavSherAdapter myFavSherAdapter;
    List<AddSher> addShers;
    Context context;
    boolean isOffline;
    ProgressDialog progressDialog, sherDialog;
    private int pageNumber = 0;
    private int firstCall = 0;
    private ArrayList<FavFCModel> favGhazalsList;
    private ArrayList<FavFCModel> favNazamsList;
    private ArrayList<FavFCModel> favSherList, updatedNazamList;
    private String ghazalId = "43d60a15-0b49-4caf-8b74-0fcdddeb9f83";
    private String nazamId = "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b";
    private String sherId = "f722d5dc-45da-41ec-a439-900df702a3d6";
    private boolean isLoading = false;
    private int totalPages;
    private LinearLayoutManager linearLayoutManager;
    private BroadcastReceiver broadcastReceiver;


    public MySherFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MySherFragment newInstance(String param1, String param2) {
        MySherFragment fragment = new MySherFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_sher, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        appDatabase = MyApplicationClass.getDb();
        mySherRecycler = (RecyclerView) getView().findViewById(R.id.mySherRecycler);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mySherRecycler.setNestedScrollingEnabled(true);
        mySherRecycler.setLayoutManager(linearLayoutManager);


        //isOffline = getArguments().getBoolean("connectStatus");

        favGhazalsList = new ArrayList<FavFCModel>();
        updatedNazamList = new ArrayList<FavFCModel>();
        favNazamsList = new ArrayList<FavFCModel>();

        sherDialog = new ProgressDialog(getActivity());
        sherDialog.setMessage("Loading...");
        //  sherDialog.show();


        mySherRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!CommonUtil.isOffline) {
                    if (!isLoading) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                                && firstVisibleItemPosition >= 0) {
                            if (pageNumber <= totalPages) {
                                firstCall = 1;
                                pageNumber = pageNumber + 1;
                                sherDialog.show();
                                if (!CommonUtil.isOffline) {
                                    GetFavDataFromServer();
                                }

                            }
                        }
                    }
                }


                /*if (isLastVisible()) {

                    if (pageNumber <= totalPages) {
                        firstCall = 1;
                        sherDialog.show();
                        GetFavDataFromServer();
                    }
                }*/
            }
        });


        if (CommonUtil.isOffline) {
            new SherAsync().execute();
        } else if (!CommonUtil.isOffline) {

            GetFavDataFromServer();
        }

    }

    boolean isLastVisible() {
        //linearLayoutManager = ((LinearLayoutManager) mySherRecycler.getLayoutManager());
        int pos = linearLayoutManager.findLastCompletelyVisibleItemPosition();
        int numItems = mySherRecycler.getAdapter().getItemCount();
        return (pos == numItems - 1);
    }

    @Override
    public void onResume() {
        super.onResume();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!CommonUtil.isOffline) {
                    //GetFavDataFromServer();
                    myFavSherAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "This is featured is not supported in offline mode", Toast.LENGTH_SHORT).show();
                }

            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter("ReloadFav"));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    private void GetFavDataFromServer() {


        String langCode = String.valueOf(CommonUtil.languageCode);
        RestClient.get().getFavData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                CommonUtil.SharedPrefKeys.rekhtasAccessToken, String.valueOf(pageNumber), new Callback<OnlineFavModelMain>() {
                    @Override
                    public void success(OnlineFavModelMain onlineFavModelMain, Response response) {

                        if (onlineFavModelMain != null) {
                            FavRPojo favRPojo = onlineFavModelMain.getR();
                            String totalCount = favRPojo.getTC();
                            totalPages = (Integer.parseInt(totalCount)) / 20;
                            List<FavFCModel> favCollection = favRPojo.getFC();
                            favSherList = new ArrayList<FavFCModel>();

                            for (int i = 0; i < favCollection.size(); i++) {

                                String typeId = favCollection.get(i).getT();
                                if (typeId.equals(ghazalId)) {

                                    favGhazalsList.add(favCollection.get(i));
                                } else if (typeId.equals(nazamId)) {
                                    favNazamsList.add(favCollection.get(i));
                                } else if (typeId.equals(sherId)) {

                                    favSherList.add(favCollection.get(i));
                                }

                            }
                            updatedNazamList.addAll(favSherList);

                            if (updatedNazamList.size() <= 5 && pageNumber < totalPages) {
                                pageNumber = pageNumber + 1;
                                favSherList.clear();
                                GetFavDataFromServer();
                            } else {
                                if (firstCall == 0) {
                                    sherDialog.dismiss();
                                    myFavSherAdapter = new MyFavSherAdapter(context, null, updatedNazamList, MySherFragment.this);
                                    mySherRecycler.setAdapter(myFavSherAdapter);
                                } else if (firstCall == 1) {
                                    sherDialog.dismiss();
                                    myFavSherAdapter.notifyDataSetChanged();
                                }
                            }

                            // Log.v("ListSizes", "ghzalaList = " + favGhazalsList.size() + " NazamList = " + favNazamsList.size() + " sherList = " + favSherList.size());

                        } else {

                            if (sherDialog != null && sherDialog.isShowing())
                                sherDialog.dismiss();

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        if (sherDialog != null && sherDialog.isShowing())
                            sherDialog.dismiss();
                    }
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void removeItem(final List<AddSher> addShersList, final int position, final AppDatabase appDatabase) {


        if (CommonUtil.isOffline) {
            class RemoveData extends AsyncTask<Void, Void, String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("removing item....");
                    progressDialog.show();
                }

                @Override
                protected String doInBackground(Void... voids) {

                    appDatabase.sherDao().delete(addShersList.get(position));
                    addShersList.remove(addShersList.get(position));


                    return null;
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    progressDialog.dismiss();
                    myFavSherAdapter.notifyDataSetChanged();
                }
            }
            new RemoveData().execute();
        } else if (!CommonUtil.isOffline) {

            if (progressDialog != null)
                progressDialog.show();
            CommonUtil.removeFromFavTroughApi(getActivity(), updatedNazamList.get(position).getI());
            CommonUtil.removeFromFavPref(updatedNazamList.get(position).getI());
            updatedNazamList.remove(position);
            myFavSherAdapter.notifyItemRemoved(position);

            progressDialog.dismiss();
        }


    }

    public class SherAsync extends AsyncTask<Void, Void, List<AddSher>> {

        @Override
        protected List<AddSher> doInBackground(Void... voids) {
            addShers = appDatabase.sherDao().getAll();

            return addShers;
        }

        @Override
        protected void onPostExecute(List<AddSher> addShers) {
            super.onPostExecute(addShers);

            if (addShers != null && addShers.size() != 0) {
                myFavSherAdapter = new MyFavSherAdapter(getActivity(), addShers, updatedNazamList, MySherFragment.this);
                mySherRecycler.setAdapter(myFavSherAdapter);
            } else {
                Toast.makeText(getActivity(), "Not found", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
