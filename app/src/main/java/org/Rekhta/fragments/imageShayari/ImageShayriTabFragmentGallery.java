package org.Rekhta.fragments.imageShayari;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.adapters.ImageShayari.DateImageSyariModel;
import org.Rekhta.adapters.ImageShayari.ImageShayriAdapterGallery;
import org.Rekhta.madhu.dto.ImageResponse;
import org.Rekhta.madhu.dto.MasterShayriImageDto;
import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionData;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionDataGallery;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageShayriTabFragmentGallery extends Fragment {

    View view;
    ImageShayriAdapterGallery imageShayriRecyclerViewAdapter;
    ArrayList<ShayariDetailDto> arrayList = new ArrayList<>();
    List<MasterShayriImageDto> masterList = new ArrayList<>();
    RecyclerView imageShayeriRecylerView;

    int pageIndex = 1;
    double totalNumberOfContent;
    boolean isDataLoading;
    private Context context;
    private String startDate;
    //JSONArray dataArray = new JSONArray();

    public List<ShayariDetailDto> getShayari1 = new ArrayList<>();


    public ImageShayriTabFragmentGallery() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_image_shayri_galllery, container, false);
        context = getActivity();


        getImagesFromApi();
        imageShayeriRecylerView = (RecyclerView) view.findViewById(R.id.imageShayeriTabrecyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        //  RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
        imageShayeriRecylerView.setLayoutManager(mLayoutManager);
        imageShayriRecyclerViewAdapter = new ImageShayriAdapterGallery(masterList, context,getShayari1);
        imageShayeriRecylerView.setAdapter(imageShayriRecyclerViewAdapter);

        imageShayeriRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= arrayList.size()) {

                        //do nothing.................................

                    } else {

                        if (pageIndex < CommonUtil.calulateTotalApiCall((int) totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getImagesFromApi();
                        }
                    }
                }
            }
        });
        return view;
    }

    private void init() {
/*
             arrayList.add(new ImageShayriCollectionData("","bg"));
             arrayList.add(new ImageShayriCollectionData("","ic_cross_delete"));
             arrayList.add(new ImageShayriCollectionData("","ic_heat_filled"));
             arrayList.add(new ImageShayriCollectionData("","bg"));
             arrayList.add(new ImageShayriCollectionData("","ic_cross_delete"));
             arrayList.add(new ImageShayriCollectionData("","ic_heat_filled"));*/

    }

    public void getImagesFromApi() {
        final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);
        RestClient.get().getImageShayriData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
                String s = new Gson().toJson(res);
                // TODO: 1/19/2018 check
                ImageResponse shariResponse = new Gson().fromJson(s, ImageResponse.class);


                if (shariResponse.getS() == 1) {
                    // totalNumberOfContent = obj.getJSONObject("R").getInt("TC");
                    totalNumberOfContent = shariResponse.getR().getTC();

                    if (shariResponse.getR().getShayari().size() == 0) {
                        // do nothng
                    } else {
                        // ((ImageShyariActivity)getActivity()).dataArray = CommonUtil.concatArray(((ImageShyariActivity)getActivity()).getShayari, obj.getJSONObject("R").getJSONArray("P"));
                        ((ImageShyariActivity) getActivity()).getShayari = shariResponse.getR().getShayari();
                        getShayari1 = shariResponse.getR().getShayari();


                        arrayList.addAll(shariResponse.getR().getShayari());
                        Collections.sort(arrayList, Collections.reverseOrder());

                        //  Collections.reverse(arrayList);
                        // Collections.reverseOrder((Comparator<Object>) arrayList);
                        masterList.clear();
                        String lastDate = "";
                        for (int i = 0; i < arrayList.size(); i++) {
                            String currentDate = arrayList.get(i).getImageDate();
                /*first time lastdate not present*/
                            if (i == 0) {
                                List<ShayariDetailDto> list = new ArrayList<>();
                                list.add(arrayList.get(i));
                                masterList.add(new MasterShayriImageDto(currentDate, list));
                                lastDate = currentDate.split("T")[0].trim();
                            }
                /*When last date is present*/
                            else {
                    /*check if current date is equal to last date*/
                                if (lastDate.equals(currentDate.split("T")[0].trim())) {
                                    masterList.get(masterList.size() - 1).getShyri().add(arrayList.get(i));
                                }
                    /*if not same then make a new object*/
                                else {
                                    List<ShayariDetailDto> list = new ArrayList<>();
                                    list.add(arrayList.get(i));
                                    masterList.add(new MasterShayriImageDto(currentDate, list));
                                    lastDate = currentDate.split("T")[0].trim();
                                }
                            }

                        }
                        //   Toast.makeText(getActivity(), arrayList.get(arrayList.size() - 1).getImageDate().trim().split("T")[0] + "  " + arrayList.size() + "  " + masterList.size(), Toast.LENGTH_LONG).show();
                        imageShayriRecyclerViewAdapter.notifyDataSetChanged();

                    }
                }
            }


            @Override
            public void failure(RetrofitError error) {
                isDataLoading = false;
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }

            }


        });
    }
}