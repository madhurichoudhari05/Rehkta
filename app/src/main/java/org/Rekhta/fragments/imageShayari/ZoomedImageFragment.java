package org.Rekhta.fragments.imageShayari;


import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.content.res.AppCompatResources;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.activites.SavedImagesGallery;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.utils.CommonUtil;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.adapters.ImageShayari.FullScreenImageAdapter;
import org.Rekhta.madhu.dto.ShayariDetailDto;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZoomedImageFragment extends Fragment implements NavigationFragment.NavigationDrawerCallbacks {

    View view;
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    ArrayList<String> filePaths = new ArrayList<String>();
    TextView backImg;

    ImageView leftArrowImg, rightArrowImg;
    public List<ShayariDetailDto> data = new ArrayList<>();
    private int position;

    private String imageId = "";

    Context context;

    ImageView backButton;
    private ImageView search, rignMenu;

    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;

    public ZoomedImageFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        int position = getArguments().getInt("pos");
        setRetainInstance(true);
        view = inflater.inflate(R.layout.fragment_zoomed_image, container, false);
        context = getActivity();
        // backImg = (TextView) view.findViewById(R.id.backButton);
        viewPager = (ViewPager) view.findViewById(R.id.zoomedPager);


        DataPrefs = getActivity().getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", getActivity().MODE_PRIVATE);

        backButton = (ImageView) view.findViewById(R.id.backButton);
        CommonUtil.setBackButton(context, backButton);

        search = (ImageView) view.findViewById(R.id.search);
        rignMenu = (ImageView) view.findViewById(R.id.trippleDot);
        initRightMenuPopUpDialog();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });

        rignMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRightMenuLangSelectionOption();
            }
        });


        if (getArguments() != null) {
            imageId = getArguments().getString("key");
        }

        intFiles();

        adapter = new FullScreenImageAdapter(getActivity(), filePaths, ZoomedImageFragment.this);
        viewPager.setAdapter(adapter);
        ((ImageShyariActivity) getActivity()).fullScreenImageAdapter = adapter;

        // close button click event
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ImageShyariActivity) getActivity()).onBackPressed();
            }
        });

        // viewPager.setCurrentItem(((ImageShyariActivity)getActivity()).currentImgClicked);
        viewPager.setCurrentItem(position);
//https://www.androidhive.info/2013/09/android-fullscreen-image-slider-with-swipe-and-pinch-zoom-gestures/

        leftArrowImg = (ImageView) view.findViewById(R.id.left_nav);
        rightArrowImg = (ImageView) view.findViewById(R.id.right_nav);
        leftArrowImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {

                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);

                }
            }
        });

        // Images right navigatin
        rightArrowImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
            }
        });

        return view;
    }


    public void intFiles() {
        try {
            data = ((ImageShyariActivity) getActivity()).getShayari;
            for (int i = 0; i < data.size(); i++) {

                if (/*((ImageShyariActivity)getActivity()).currentImgClickedS*/imageId.trim().equalsIgnoreCase(data.get(i).getImageId().trim())) {
                    position = i;
                    filePaths.add(data.get(i).getImageId());
                } else {
                    filePaths.add(data.get(i).getImageId());
                    //  Toast.makeText(context, "not Matched", Toast.LENGTH_SHORT).show();
                }
                //  filePaths.add(data.getJSONObject(i).getString("I"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void langChanged() {

        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }

        adapter = new FullScreenImageAdapter(getActivity(), filePaths, ZoomedImageFragment.this);
        viewPager.setAdapter(adapter);
    }


}
