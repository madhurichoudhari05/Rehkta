package org.Rekhta.fragments.imageShayari;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.Rekhta.R;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.activites.PoetsActivity;
import org.Rekhta.adapters.ImageShayari.ImageShayriRecyclerViewAdapter;
import org.Rekhta.adapters.poets.PoetsListAdapter;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageShayriTabFragment extends Fragment {

    View view;
    ImageShayriRecyclerViewAdapter imageShayriRecyclerViewAdapter;
    ArrayList<ImageShayriCollectionData> arrayList;
    RecyclerView imageShayeriRecylerView;

    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    private Context context;
    private ProgressDialog progressLoader;
    //JSONArray dataArray = new JSONArray();


    public ImageShayriTabFragment() {
    }

    public List<ShayariDetailDto> getShayari = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_image_shayri_tab, container, false);
        arrayList = new ArrayList<>();
        context = getActivity();
        getImagesFromApi();

        imageShayeriRecylerView = (RecyclerView) view.findViewById(R.id.imageShayeriTabrecyclerview);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(context, 3);
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,true);
        imageShayeriRecylerView.setLayoutManager(mLayoutManager);
        imageShayriRecyclerViewAdapter = new ImageShayriRecyclerViewAdapter(arrayList, getActivity(), getShayari);
        imageShayeriRecylerView.setAdapter(imageShayriRecyclerViewAdapter);
        imageShayeriRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {

                    } else {
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getImagesFromApi();
                        }
                    }
                }
            }
        });
        return view;
    }

    private void init() {
        arrayList = new ArrayList<>();
/*
             arrayList.add(new ImageShayriCollectionData("","bg"));
             arrayList.add(new ImageShayriCollectionData("","ic_cross_delete"));
             arrayList.add(new ImageShayriCollectionData("","ic_heat_filled"));
             arrayList.add(new ImageShayriCollectionData("","bg"));
             arrayList.add(new ImageShayriCollectionData("","ic_cross_delete"));
             arrayList.add(new ImageShayriCollectionData("","ic_heat_filled"));*/

    }

    public void getImagesFromApi() {
        arrayList = new ArrayList<>();
        progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);
        RestClient.get().getImageShayriData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                Log.e("app", res + "");
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
                isDataLoading = false;

                try {
                    JSONObject obj = new JSONObject(res);

                    if (obj.getInt("S") == 1) {
                        totalNumberOfContent = obj.getJSONObject("R").getInt("TC");

                        if (obj.getJSONObject("R").getJSONArray("P").length() == 0) {
                            // do nothng
                        } else {
                        /*((ImageShyariActivity)getActivity()).dataArray = CommonUtil.concatArray(((ImageShyariActivity)getActivity()).dataArray, obj.getJSONObject("R").getJSONArray("P"));
                        JSONArray data = ((ImageShyariActivity)getActivity()).dataArray;
                        for (int i = 0; i < data.length(); i++) {
                            arrayList.add(new ImageShayriCollectionData(
                                    data.getJSONObject(i).getString("I"), data.getJSONObject(i).getString("PI"),
                                    data.getJSONObject(i).getString("NE"), data.getJSONObject(i).getString("NH"), data.getJSONObject(i).getString("NU"),
                                    data.getJSONObject(i).getString("CI")));
                        }*/
                            imageShayriRecyclerViewAdapter.notifyDataSetChanged();
                        }

                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                isDataLoading = false;
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();

    }
}
