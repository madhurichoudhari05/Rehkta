package org.Rekhta.fragments.imageShayari;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.Rekhta.R;


public class DailyFeedsTabFragment extends Fragment {

    View view;

    public DailyFeedsTabFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_daily_feeds_tab, container, false);

        return view;
    }

}
