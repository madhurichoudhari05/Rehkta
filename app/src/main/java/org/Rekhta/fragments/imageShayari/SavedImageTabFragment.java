package org.Rekhta.fragments.imageShayari;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.Rekhta.R;
import org.Rekhta.activites.SavedImagesGallery;
import org.Rekhta.adapters.ImageShayari.SavedImageShayriGridAdapter;
import org.Rekhta.utils.CommonUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class SavedImageTabFragment extends Fragment {

    public static final String TAG = "Album3Activity";
    static GridView gridView;
    View view;
    File[] mediaFiles;
    File imageDir;
    SavedImageShayriGridAdapter adapter;
    Intent in;
    ImageButton btncam;
    String name = null;
    ArrayList<Bitmap> bmpArray = new ArrayList<Bitmap>();
    ArrayList<String> fileName = new ArrayList<String>();
    SwipeRefreshLayout swipeLayout;
    private Context context;
    private BroadcastReceiver mMessageReceiver;

    public SavedImageTabFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_saved_image_tab, container, false);
        context = getActivity();


        initSwipeView();

        CheckFilesINRekhtaFolder();

        mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                bmpArray.clear();
                fileName.clear();
                CheckFilesINRekhtaFolder();
                initSwipeView();

                //    Toast.makeText(context, "ImagesFull", Toast.LENGTH_SHORT).show();

            }
        };
        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver, new IntentFilter("FullImage"));


        // CheckFilesINRekhtaFolder();
        // initSwipeView();
        return view;
    }

    void initSwipeView() {
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                bmpArray.clear();
                fileName.clear();
                CheckFilesINRekhtaFolder();
            }
        });

    }

    public void CheckFilesINRekhtaFolder() {
        imageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "Rekhta");
//        imageDir = new File(Environment.getExternalStorageDirectory().toString()+"/Rekhta");
        if ((imageDir.exists())) {
            mediaFiles = imageDir.listFiles();

            for (File file : mediaFiles) {
                bmpArray.add(convertToBitmap(file));
                fileName.add(readFileName(file));
                //Log.d(TAG + "bmpArray Size", ""+bmpArray.size());
                //Log.d(TAG, "call to convertToBitmap");
            }//for
            adapter = new SavedImageShayriGridAdapter(bmpArray, fileName, getActivity());
            gridView = (GridView) view.findViewById(R.id.gridview);
            gridView.setAdapter(adapter);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Bundle extras = new Bundle();

                    extras.putSerializable("value", (Serializable) mediaFiles);


                    Intent intent = new Intent(getActivity(), SavedImagesGallery.class);
                    intent.putExtra("imagesList", mediaFiles);
                    intent.putExtra("pos", i);
                    CommonUtil.myFiles = mediaFiles;
                    //intent.putExtra("imagesList", extras);
                    getActivity().startActivity(intent);

                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                final Uri contentUri = Uri.fromFile(imageDir);
                scanIntent.setData(contentUri);
                getActivity().sendBroadcast(scanIntent);
            } else {
                final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()));
                getActivity().sendBroadcast(intent);
            }

        } else {

        }
        if (swipeLayout != null) {
            swipeLayout.setRefreshing(false);
        }
    }

    public Bitmap convertToBitmap(File file) {
        URL url = null;
        try {
            url = file.toURL();
        } catch (MalformedURLException e1) {
            //Log.d(TAG, e1.toString());
        }//catch

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openStream());
            //bmp.recycle();
        } catch (Exception e) {
            //Log.d(TAG, "Exception: "+e.toString());
        }//catch
        return bmp;
    }//convertToBitmap

    public String readFileName(File file) {
        String name = file.getName();
        return name;
    }//readFileName

}
