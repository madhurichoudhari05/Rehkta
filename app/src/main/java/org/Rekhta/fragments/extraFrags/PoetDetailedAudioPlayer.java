package org.Rekhta.fragments.extraFrags;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.Rekhta.R;;

/**
 * A simple {@link Fragment} subclass.
 */
public class PoetDetailedAudioPlayer extends Fragment {


    public PoetDetailedAudioPlayer() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_poet_detailed_audio_player, container, false);
    }

}
