package org.Rekhta.fragments;


import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONObject;

import org.Rekhta.R;
import org.Rekhta.activites.GhazalActivity;
import org.Rekhta.activites.NazmActivity;
import org.Rekhta.adapters.videoListAdapter;

/**
 * A simple {@link Fragment} subclass.
 */

public class YoutubeFragment extends BottomSheetDialogFragment {
    // API キー
    private static final String API_KEY = "AIzaSyAucCMhVmsY5JZRRl2JPsvRdnWeThfOHx8";

    // YouTubeのビデオID
    private static String VIDEO_ID = "M7g3tyqZADY";
   // YouTubePlayerSupportFragment youTubePlayerFragment;
    boolean isVideoPlaying = false;

    JSONArray videoObj;
    TextView currentAuthorName, currentTitle;
    ImageView closeYouTubedownArrow;
    String cActivity ="";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        try {
            videoObj = new JSONArray(getArguments().getString("videoObj"));
            cActivity = getArguments().getString("cActivity");
            System.out.print(videoObj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        View rootView = inflater.inflate(R.layout.fragment_video, container, false);
     /*   RelativeLayout closeHeader = (RelativeLayout) rootView.findViewById(R.id.closeImg);
        closeHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cActivity.equalsIgnoreCase("GhazalActivity")){
                    ((GhazalActivity) getActivity()).isVideoPlaying = true;
                }else if(cActivity.equalsIgnoreCase("NazmActivity")){
                    ((NazmActivity) getActivity()).isVideoPlaying = true;
                }
                getActivity().onBackPressed();

            }
        });*/
        currentAuthorName = (TextView) rootView.findViewById(R.id.currentVideoAuthorName);
        currentTitle = (TextView) rootView.findViewById(R.id.currentVideoTitle);

        try {
            openVideo(videoObj.getJSONObject(0).getString("YI"));
            currentAuthorName.setText(videoObj.getJSONObject(0).getString("AN"));
            currentTitle.setText(videoObj.getJSONObject(0).getString("VT"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ListView listView = (ListView) rootView.findViewById(R.id.videoList);
        videoListAdapter videoAdapter = new videoListAdapter(getActivity(), videoObj);
        listView.setAdapter(videoAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    openVideo(videoObj.getJSONObject(i).getString("YI"));
                    currentAuthorName.setText(videoObj.getJSONObject(i).getString("AN"));
                    currentTitle.setText(videoObj.getJSONObject(i).getString("VT"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        closeYouTubedownArrow = (ImageView) rootView.findViewById(R.id.spinnerToCloseImg);
        closeYouTubedownArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cActivity.equalsIgnoreCase("GhazalActivity")){
                    ((GhazalActivity) getActivity()).isVideoPlaying = true;
                }else if(cActivity.equalsIgnoreCase("NazmActivity")){
                    ((NazmActivity) getActivity()).isVideoPlaying = true;
                }
                getActivity().onBackPressed();

            }
        });
        return rootView;
    }

    public void openVideo(final String videoID) {
        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.youtube_layout, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                if(cActivity.equalsIgnoreCase("GhazalActivity")) {
                    ((GhazalActivity) getActivity()).currentPlayer = player;
                    ((GhazalActivity) getActivity()).isVideoPlaying = true;
                }else if(cActivity.equalsIgnoreCase("NazmActivity")){
                    ((NazmActivity) getActivity()).currentPlayer = player;
                    ((NazmActivity) getActivity()).isVideoPlaying = true;
                }
                if (!wasRestored) {
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                    player.loadVideo(videoID);
                    player.play();

                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                String errorMessage = error.toString();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                Log.d("errorMessage:", errorMessage);
            }

        });
    }
}