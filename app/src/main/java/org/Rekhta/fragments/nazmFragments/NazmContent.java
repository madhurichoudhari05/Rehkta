package org.Rekhta.fragments.nazmFragments;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.Rekhta.activites.AudioActivity;
import org.Rekhta.activites.ExVScrollView;
import org.Rekhta.activites.GhazalActivity;
import org.Rekhta.activites.ImageShyariActivity;
import org.Rekhta.activites.MyFavoritesActivity;
import org.Rekhta.activites.PoetDetailedActivity;
import org.Rekhta.activites.SettingActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.Rekhta.R;
import org.Rekhta.Services.GhazalContentService;
import org.Rekhta.activites.ExScrollView;
import org.Rekhta.activites.NazmActivity;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.activites.ViewActivity;
import org.Rekhta.activites.View_ProfileActivity;
import org.Rekhta.activites.ZoomView;
import org.Rekhta.fonts.LatoBoldTextView;
import org.Rekhta.fonts.MerriweatherBoldTextView;
import org.Rekhta.fragments.AudioFragment;
import org.Rekhta.fragments.YoutubeFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;
import org.Rekhta.views.MeaningFragment;
import org.Rekhta.views.ZoomLayout;

import me.grantland.widget.AutofitHelper;
import me.grantland.widget.AutofitTextView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.MODE_PRIVATE;


public class NazmContent extends Fragment {

    public float textSixeToRender;
    View view = null;
    TextView t[];
    LinearLayout linearL;
    ExScrollView vScrollWrapper;
    LinearLayout linearWrapper;
    RelativeLayout outerLinear;
    Double alignmenht;
    ZoomView zoomView;
    Toolbar toolbar;
    TextView toolbarTV;
    LatoBoldTextView view_gazals, view_profile;
    ImageView backButton;
    ImageView editorChoiceImg;
    ImageView popularchoiceImg;
    ImageView authorImage;
    Dialog dialog;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    SharedPreferences DataPrefs = null;
    JSONObject jsonData;
    JSONObject prevData;
    JSONObject nextData;
    ImageView footor_dotsIcon, footor_audioIcon, footor_videoIcon, footor_shareIcon, footor_infoIcon, footor_favIcon, footer_default;
    MediaPlayer player;
    boolean renderComplete;
    int positionClicked = 0;
    JSONObject data;
    TextView nextTiltle, nextAuthorName, prevTitle, prevAuthorName, nextItemTxt, prevItemTxt;
    LinearLayout nextLinaerlayoutview, prevLinaerlayoutview;
    View didvider_nextandprev;
    String audioUrl = "";
    JSONArray videoArrayData;
    JSONArray audioArrayData;
    BottomSheetDialog bottomSheetDialog;
    MerriweatherBoldTextView detailedAuthorName;
    WindowManager.LayoutParams wlp;
    boolean showCritiqueDialog = true;
    boolean isCritiqueModeOn = false;
    FrameLayout nazm_framelayout;
    float sizeoFtextShouldBe = 100;
    View mainFooter;
    View critiqueFooter;
    View critiqueFooterWhenOn;
    Dialog critiqueSubmitForm;
    ImageView renderNextArrow, renderPrevArrow;
    private TextView turnOffCriticText, footerCrcitcOnText;
    private boolean isLoggedIn;
    private ProgressDialog progressLoader;
    private Typeface engtf, hinditf, urdutf;

    public NazmContent() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {

            positionClicked = ((NazmActivity) getActivity()).currentIndexOfContent;

            data = new JSONObject(((NazmActivity) getActivity()).currentData.getJSONObject(positionClicked).toString());
            if (positionClicked != 0) {
                prevData = new JSONObject(((NazmActivity) getActivity()).currentData.getJSONObject(positionClicked - 1).toString());
            } else {
                prevData = new JSONObject();
            }
            if (positionClicked == ((NazmActivity) getActivity()).currentData.length() - 1) {
                nextData = new JSONObject();

            } else {
                nextData = new JSONObject(((NazmActivity) getActivity()).currentData.getJSONObject(positionClicked + 1).toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        setHasOptionsMenu(true);
        setRetainInstance(true);
      /*  view = inflater.inflate(R.layout.fragment_nazm_content, container, false);
        return view;*/
        FrameLayout frameLayout = new FrameLayout(getActivity());
        populateViewForOrientation(inflater, frameLayout);
        return frameLayout;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        populateViewForOrientation(inflater, (ViewGroup) getView());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        engtf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/MerriweatherExtended-LightItalic.ttf");
        hinditf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Laila-Regular.ttf");
        urdutf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");
    }

    private void populateViewForOrientation(LayoutInflater inflater, ViewGroup viewGroup) {
        viewGroup.removeAllViewsInLayout();
        view = inflater.inflate(R.layout.fragment_nazm_content, viewGroup);
        initViews();
        // Find your buttons in subview, set up onclicks, set up callbacks to your parent fragment or activity here.
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        final ViewTreeObserver observer = linearL.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (renderComplete) {
                    //  widhtOfView();
                    widhtOfViewNew();
                }
            }
        });

    }

    public void initViews() {

        nazm_framelayout = (FrameLayout) view.findViewById(R.id.nazm_frameLayout);
        toolbar = (Toolbar) view.findViewById(R.id.toolbarfrag);
        toolbar.inflateMenu(R.menu.home_screen);
        zoomView = (ZoomView) view.findViewById(R.id.zoomlayout);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_settings:
                        showRightMenuLangSelectionOption();
                        return true;
                    case R.id.search:
                        getActivity().startActivity(new Intent(getActivity(), SearchActivity.class));
                        return true;
                    default:

                }
                return false;
            }
        });
        initRightMenuPopUpDialog();
//        CommonUtil.mainContentScrollView = (ScrollView) view.findViewById(R.id.NazmScrollContentView);
        backButton = (ImageView) view.findViewById(R.id.backButton);
        CommonUtil.setBackButton(getActivity(), backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
                //  getActivity().onBackPressed();

            }
        });
        authorImage = (ImageView) view.findViewById(R.id.authorImg);
        editorChoiceImg = (ImageView) view.findViewById(R.id.editorChoiceImg);
        popularchoiceImg = (ImageView) view.findViewById(R.id.popularChoiceIcon);
        toolbarTV = (TextView) toolbar.findViewById(R.id.toobartextview);
//        linearL = (LinearLayout) view.findViewById(R.id.nazm_mainContent);
        linearL = zoomView.Wrapper;

        //getNazmData();
        view_gazals = (LatoBoldTextView) view.findViewById(R.id.view_gazals);
        view_gazals.setText("View All Nazm");
        view_profile = (LatoBoldTextView) view.findViewById(R.id.view_profile);
        view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), PoetDetailedActivity.class);
                try {
                    intent.putExtra("poetId", data.getString("PI"));
                    intent.putExtra("ghazalCount", "");
                    intent.putExtra("nazmCount", "");
                    intent.putExtra("sherCount", "");
                    intent.putExtra("shortDescInEng", " ");
                    intent.putExtra("shortDescInHin", " ");
                    intent.putExtra("shortDescInUrdu", " ");
                    intent.putExtra("shouldLandOnProfile", true);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        view_gazals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PoetDetailedActivity.class);
                try {
                    intent.putExtra("poetId", data.getString("PI"));
                    intent.putExtra("ghazalCount", "");
                    intent.putExtra("nazmCount", "");
                    intent.putExtra("sherCount", "");
                    intent.putExtra("shortDescInEng", " ");
                    intent.putExtra("shortDescInHin", " ");
                    intent.putExtra("shortDescInUrdu", " ");
                    intent.putExtra("shouldLandonprofile", false);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        initFooterView(view);

        getNazmData();
    }

    private void initFooterView(View v) {
        try {

            didvider_nextandprev = (View) v.findViewById(R.id.didvider_nextandprev);
            nextLinaerlayoutview = (LinearLayout) v.findViewById(R.id.nextLinearLayout);
            nextTiltle = (TextView) v.findViewById(R.id.next_gazal_title);
            nextAuthorName = (TextView) v.findViewById(R.id.author_name);

            renderNextArrow = (ImageView) v.findViewById(R.id.renderNextarrow);
            renderPrevArrow = (ImageView) v.findViewById(R.id.renderPrevArrow);

            prevLinaerlayoutview = (LinearLayout) v.findViewById(R.id.prevLinearLayout);
            prevTitle = (TextView) v.findViewById(R.id.prev_title);
            prevAuthorName = (TextView) v.findViewById(R.id.prev_authorName);

            nextItemTxt = (TextView) v.findViewById(R.id.nextItemTxt);
            prevItemTxt = (TextView) v.findViewById(R.id.prevItemTxt);

            setLangData();

            /*if (((GhazalActivity) getActivity()).currentIndexOfContent == 0) {
                prevLinaerlayoutview.setVisibility(View.GONE);
            } else {
                prevLinaerlayoutview.setVisibility(View.VISIBLE);
            }*/
            prevLinaerlayoutview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((NazmActivity) getActivity()).currentIndexOfContent = ((NazmActivity) getActivity()).currentIndexOfContent - 1;
                    ((NazmActivity) getActivity()).removeFragment();

                }
            });

            nextLinaerlayoutview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((NazmActivity) getActivity()).currentIndexOfContent = ((NazmActivity) getActivity()).currentIndexOfContent + 1;
                    ((NazmActivity) getActivity()).removeFragment();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getNazmData() {
        progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);
        try {

            nazm_framelayout.setVisibility(View.VISIBLE);
            RestClient.get().getContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.languageCode, data.getString("I"), new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {

                    try {
                        JSONObject obj = new JSONObject(res);
                        if (obj.getInt("S") == 1) {
                            JSONObject obj1 = new JSONObject(res);
                            Log.e("res", " " + res);
                            System.out.print("=====================================================" + res);
                            nazm_framelayout.setVisibility(View.GONE);
                            textSixeToRender = 14.5f;
                            //pumpData(obj1, CommonUtil.languageCode);
                            handleServerData(obj1, CommonUtil.languageCode);
                            //calculateTextSize(obj1);
                            renderComplete = true;

                            if (progressLoader != null && progressLoader.isShowing()) {
                                progressLoader.dismiss();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                }

                @Override
                public void failure(RetrofitError error) {
                    if (progressLoader != null && progressLoader.isShowing()) {
                        progressLoader.dismiss();
                    }

                    Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pumpData(JSONObject obj, int langcode) throws JSONException {


        TextView ghazalTitle = (TextView) view.findViewById(R.id.gazal_title);
        TextView author_name = (TextView) view.findViewById(R.id.author_name);
        toolbarTV.setText(LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode));

        JSONObject mainObj = obj.getJSONObject("R");
        alignmenht = mainObj.getDouble("RA");
        if (!mainObj.getBoolean("EC")) {
            editorChoiceImg.setVisibility(View.GONE);
        } else {
            editorChoiceImg.setVisibility(View.VISIBLE);
        }
        if (mainObj.getBoolean("PC")) {
            popularchoiceImg.setVisibility(View.VISIBLE);
        } else {
            popularchoiceImg.setVisibility(View.GONE);
        }
        String title = mainObj.getString("CT");
        Log.d("Title", title);
        ghazalTitle.setText(title);
        author_name.setText(mainObj.getJSONObject("Poet").getString("CS").toUpperCase().replace("-", " "));
        detailedAuthorName = (MerriweatherBoldTextView) view.findViewById(R.id.detaied_authorName);
        detailedAuthorName.setText(mainObj.getJSONObject("Poet").getString("CS").toUpperCase().replace("-", " "));
        if (CommonUtil.languageCode == 1) {
            detailedAuthorName.setText(data.getString("PE"));
            CommonUtil.setEnglishMerriweatherBlack(getActivity(), ghazalTitle);
            CommonUtil.setEnglishMerriweatherBoldFont(getActivity(), detailedAuthorName);
            author_name.setText(data.getString("PE"));
        } else if (CommonUtil.languageCode == 2) {
            detailedAuthorName.setText(data.getString("PH"));
            CommonUtil.setRozaOne(getActivity(), ghazalTitle);
            CommonUtil.setHindiFont(getActivity(), detailedAuthorName);
            author_name.setText(data.getString("PH"));
        } else if (CommonUtil.languageCode == 3) {
            detailedAuthorName.setText(data.getString("PU"));
            CommonUtil.setUrduNotoNataliq(getActivity(), detailedAuthorName);
            CommonUtil.setUrduRuqa(getActivity(), ghazalTitle);
            author_name.setText(data.getString("PU"));
        } else {
            detailedAuthorName.setText(data.getString("PE"));
            CommonUtil.setEnglishMerriweatherBlack(getActivity(), ghazalTitle);
            CommonUtil.setEnglishMerriweatherBoldFont(getActivity(), detailedAuthorName);
            author_name.setText(data.getString("PE"));
        }
        //toolbarTV.setText(mainObj.getString("CT"));

        UpdateNextAndPreviousData();
        updateNextAndPrevoiusText();
        audioArrayData = new JSONArray(mainObj.getJSONArray("Audios").toString());
        videoArrayData = new JSONArray(mainObj.getJSONArray("Videos").toString());
        initOptionAtFooter();

        Glide.with(getActivity())
                .load("https://rekhta.org" + mainObj.getJSONObject("Poet").getString("IU"))
                .into(authorImage);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearL.removeAllViews();
        vScrollWrapper = new ExScrollView(getActivity());
        vScrollWrapper.setVerticalScrollBarEnabled(false);
        vScrollWrapper.setHorizontalScrollBarEnabled(false);

        vScrollWrapper.setFillViewport(true);
        //    vScrollWrapper.setScrollBarStyle(HorizontalScrollView.SCROLL_AXIS_NONE);
        vScrollWrapper.setLayoutParams(new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.WRAP_CONTENT));


        // vScrollWrapper.setBackgroundColor(Color.RED);
        linearWrapper = new LinearLayout(getActivity());
        outerLinear = new RelativeLayout(getActivity());
        outerLinear.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

        outerLinear.setPadding(8, 0, 8, 0);
       /* outerLinear.setBackgroundColor(Color.BLUE);*/
        // outerLinear.setBackgroundColor(Color.YELLOW);
        linearWrapper.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams55 = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        linearWrapper.setLayoutParams(layoutParams55);
        linearWrapper.setClipChildren(false);
        //linearWrapper.setHorizontalGravity(Gravity.CENTER);
        //  linearWrapper.setBackgroundColor(Color.GRAY);
        //linearWrapper.setPadding(0, 12, 0, 12);
        //  linearWrapper.setGravity(Gravity.FILL_HORIZONTAL);
        // linearWrapper.setBackgroundColor(Color.BLUE);


        RelativeLayout.LayoutParams RelativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (alignmenht == 1.0) {
            RelativeLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            linearWrapper.setLayoutParams(RelativeLayoutParams);
            // outerLinear.addView(linearWrapper);
        } else if (alignmenht == 0.0) {
            RelativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
            linearWrapper.setLayoutParams(RelativeLayoutParams);
            //outerLinear.addView(linearWrapper);
        }

        int minGap = 3;
        JSONObject mainContent = new JSONObject(mainObj.getString("CR"));


        JSONArray Parray = mainContent.getJSONArray("P");
        renderNazamNew(Parray, dim);


    }

    private void renderNazamNew(JSONArray Parray, LinearLayout.LayoutParams dim) {

        for (int i = 0; i < Parray.length(); i++) {

            JSONArray Larray = null;
            try {
                Larray = Parray.getJSONObject(i).getJSONArray("L");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < Larray.length(); j++) {

                LinearLayout linear = new LinearLayout(getActivity());
                LinearLayout.LayoutParams layoutParamsh = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linear.setLayoutParams(layoutParamsh);
                linear.setOrientation(LinearLayout.HORIZONTAL);

                JSONArray Warray = null;
                try {
                    Warray = Larray.getJSONObject(j).getJSONArray("W");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                t = new TextView[Warray.length()];

                if (CommonUtil.languageCode != 3) {
                    forceLTRIfSupported();
                    for (int k = 0; k < Warray.length(); k++) {

                        t[k] = new TextView(getActivity());
                        t[k].setLayoutParams(dim);
                        t[k].setIncludeFontPadding(false);
                        // t[k].setGravity(Gravity.CENTER_HORIZONTAL);

                        if (k < Warray.length() - 1) {
                            // t[k].setPadding(0, 0, 0, 0);
                        }
                        try {
                            t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        t[k].setTextColor(Color.parseColor("#000000"));
                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            t[k].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        }*/
                        if (CommonUtil.languageCode == 1) {
                            t[k].setTypeface(engtf);
                            //t[k].setTextSize(14);
                            // CommonUtil.setEnglishMerriwetherFont(getActivity(), t[k]);

                        } else {
                            // t[k].setTextSize(20);
                            t[k].setTypeface(hinditf);
                            CommonUtil.setHindiLailaRegular(getActivity(), t[k]);
                        }

                        //   t[k].setGravity(Gravity.CENTER);
                        try {
                            assignClickToText(t[k], Warray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        linear.addView(t[k]);

                    }
                    //linearWrapper.setGravity(Gravity.CENTER_HORIZONTAL);
                } else {
                    forceRTLIfSupported();

                    for (int k = 0; k < Warray.length(); k++) {
                        t[k] = new TextView(getActivity());
                        t[k].setLayoutParams(dim);
                        t[k].setIncludeFontPadding(false);
                        // t[k].setTextSize(14);
                        //  CommonUtil.setUrduNotoNataliq(getActivity(), t[k]);
                        try {
                            t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (k < Warray.length() - 1) {
                            // t[k].setPadding(0, 0, minGap, 0);
                        }

                        t[k].setTextColor(Color.parseColor("#000000"));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            t[k].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                        }

                        t[k].setTypeface(urdutf);
                        try {
                            assignClickToText(t[k], Warray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        linear.addView(t[k]);
                    }
                }
                if (j == Larray.length() - 1) {
                    linear.setPadding(0, 0, 0, 35);
                } else {
                    linear.setPadding(0, 3, 0, 0);
                }


                linearWrapper.addView(linear);

                /*linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                int widthSpec = View.MeasureSpec.makeMeasureSpec(linearWrapper.getWidth(), View.MeasureSpec.EXACTLY);
                int heightSpec = View.MeasureSpec.makeMeasureSpec(linearWrapper.getHeight(), View.MeasureSpec.EXACTLY);
                linear.measure(widthSpec, heightSpec);

                //linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);*/

                linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            }
        }

        outerLinear.addView(linearWrapper);
        outerLinear.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        vScrollWrapper.addView(outerLinear);
        vScrollWrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        zoomView.addView(vScrollWrapper);
        zoomView.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    public void widhtOfView() {

        int childCount = linearWrapper.getChildCount();

        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = linearWrapper.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                int curWidthDiff = linearWrapper.getWidth() - lineView.getWidth();
                // LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                //linearWrapper.setPadding(curWidthDiff, 0, 0, curWidthDiff);
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        // textView.setPadding(0, 0, extraPadding, 0);
                    }
                }
                //TextView
            }
        }

        renderComplete = false;

        //int x = zoomView.getWidth();
        int y = linearWrapper.getWidth();
        float scaleF1 = (float) zoomView.getWidth() / (float) linearWrapper.getWidth();
        float scaleF = 1.0f;
//        linearWrapper.setMinimumWidth(linearWrapper.getWidth()*3);

        int requiredHeight2 = (int) (scaleF + linearWrapper.getMeasuredHeight());
        //zoomView.setWrapperHeight(requiredHeight, outerLinear.getHeight());
        int requiredHeight = (int) (scaleF * linearWrapper.getMeasuredHeight());
        //zoomView.setWrapperChildWidth(outerLinear.getWidth());

        if (CommonUtil.languageCode == 3) {
            linearWrapper.setPivotX(linearWrapper.getWidth());
            linearWrapper.setPivotY(0);

            linearWrapper.setScaleX((float) (scaleF));
            linearWrapper.setScaleY((float) (scaleF));
        } else {
            linearWrapper.setPivotX(0);
            linearWrapper.setPivotY(0);
            // zoomView.setWrapperHeight(requiredHeight, outerLinear.getHeight());

            linearWrapper.setScaleX(1.0f);
            linearWrapper.setScaleY(1.0f);
        }


    }

    public void assignClickToText(final TextView txt, final JSONObject obj, final int i, final JSONArray Larray, final int lineNumber) {

        txt.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                try {
                    shareParaGraphText(Larray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;    // set to true
            }
        });

        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int paraNumber = i + 1;
                try {
                    if (isCritiqueModeOn) {
                        String nazmline = getLineFromTheParagraph(Larray, lineNumber);
                        // Toast.makeText(getActivity(), paraNumber+"   -    "+nazmline, Toast.LENGTH_SHORT).show();
                        ShowCritiqueSubmitForm(paraNumber, nazmline);
                    } else {
                        ShowAlert(txt, obj);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void shareParaGraphText(JSONArray Larray) throws JSONException {

        String paratoShare = Paragraph(Larray);
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = paratoShare;
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "https://org.Rekhta.org");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }

    private String Paragraph(JSONArray Larray) throws JSONException {
        String lines = "";
        for (int j = 0; j < Larray.length(); j++) {
            JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
            t = new TextView[Warray.length()];
            lines = lines + "\n";
            if (CommonUtil.languageCode != 3) {
                for (int k = 0; k < Warray.length(); k++) {
                    lines = lines + " " + Warray.getJSONObject(k).getString("W");
                }
            } else {
                for (int k = Warray.length() - 1; k >= 0; k--) {
                    lines = lines + " " + Warray.getJSONObject(k).getString("W");
                }
            }
        }
        return lines;
    }

    public void giveColorToSelectedText(TextView txt) {
        txt.setBackgroundColor(Color.parseColor("#eb0045"));
    }

    public void removeColorToSelectedText(TextView txt) {
        txt.setBackgroundColor(Color.parseColor("#FFFFFF"));
    }

    public void showMeaning(String text, JSONObject obj) {


        try {
            getMeaningOfTheWord(text, obj.getString("M"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ShowAlert(final TextView mainText, JSONObject obj) throws JSONException {
        //giveColorToSelectedText(mainText);
        MeaningFragment meaningFragment = (MeaningFragment) getActivity().getSupportFragmentManager().findFragmentByTag("meaningFrag");
        if (meaningFragment != null && meaningFragment.isVisible()) {
            getActivity().onBackPressed();
            showMeaning(mainText.getText().toString(), obj);
        } else {
            showMeaning(mainText.getText().toString(), obj);
        }
    }

    @SuppressLint("RestrictedApi")
    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        DataPrefs = getActivity().getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                setLangData();
                CommonUtil.setBackButton(getActivity(), backButton);
                dialog.dismiss();

            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                setLangData();
                CommonUtil.setBackButton(getActivity(), backButton);
                dialog.dismiss();
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                CommonUtil.setBackButton(getActivity(), backButton);
                setLangData();
                dialog.dismiss();

            }
        });

    }


    private void setLangData() {


        nextItemTxt.setText(LanguageSelection.getSpecificLanguageWordById("Next", CommonUtil.languageCode));
        prevItemTxt.setText(LanguageSelection.getSpecificLanguageWordById("Previous", CommonUtil.languageCode));

        view_gazals.setText(LanguageSelection.getSpecificLanguageWordById("see All Ghazals", CommonUtil.languageCode));
        view_profile.setText(LanguageSelection.getSpecificLanguageWordById("View profile", CommonUtil.languageCode));

        if (CommonUtil.languageCode == 3) {
            renderNextArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_left));
            renderPrevArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_right));
        } else {
            renderNextArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_right));
            renderPrevArrow.setImageDrawable(getResources().getDrawable(R.drawable.vector_arrow_left));
        }


    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {


        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);
        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }

        if (CommonUtil.languageCode != langCode) {
            CommonUtil.languageCode = langCode;
            saveTheLangCodeLocally();
            getNazmData();
        }
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        // setGhazalTextDynamic();
        if (isCritiqueModeOn) {
            setCritciFooterText();
        }

        dialog.dismiss();
    }

    private void setCritciFooterText() {

        if (footerCrcitcOnText != null) {
            footerCrcitcOnText.setText(LanguageSelection.getSpecificLanguageWordById("Critique Mode on", CommonUtil.languageCode));
            turnOffCriticText.setText(LanguageSelection.getSpecificLanguageWordById("Turn Off", CommonUtil.languageCode));
        }

    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void updateNextAndPrevoiusText() {


        try {
            if (nextData.length() == 0) {
                nextLinaerlayoutview.setVisibility(View.GONE);
                didvider_nextandprev.setVisibility(View.GONE);
            } else {
                nextLinaerlayoutview.setVisibility(View.VISIBLE);
                didvider_nextandprev.setVisibility(View.VISIBLE);
                String authorName = "";
                String ghazalTitle = "";
                if (CommonUtil.languageCode == 1) {
                    ghazalTitle = nextData.getString("TE");
                    authorName = nextData.getString("PE");
                } else if (CommonUtil.languageCode == 2) {
                    ghazalTitle = nextData.getString("TH");
                    authorName = nextData.getString("PH");
                } else {
                    ghazalTitle = nextData.getString("TU");
                    authorName = nextData.getString("PU");
                }
                nextTiltle.setText(ghazalTitle);
                nextAuthorName.setText(authorName);
            }


            if (prevData.length() == 0) {
                prevLinaerlayoutview.setVisibility(View.GONE);
                didvider_nextandprev.setVisibility(View.GONE);
            } else {
                prevLinaerlayoutview.setVisibility(View.VISIBLE);
                String authorName = "";
                String ghazalTitle = "";
                if (CommonUtil.languageCode == 1) {
                    ghazalTitle = prevData.getString("TE");
                    authorName = prevData.getString("PE");
                    forceLTRIfSupported();
                } else if (CommonUtil.languageCode == 2) {
                    ghazalTitle = prevData.getString("TH");
                    authorName = prevData.getString("PH");
                    forceLTRIfSupported();
                } else {
                    ghazalTitle = prevData.getString("TU");
                    authorName = prevData.getString("PU");
                    forceRTLIfSupported();
                }
                prevTitle.setText(ghazalTitle);
                prevAuthorName.setText(authorName);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateNextAndPreviousData() {
        try {
            positionClicked = ((NazmActivity) getActivity()).currentIndexOfContent;
            data = new JSONObject(((NazmActivity) getActivity()).currentData.getJSONObject(positionClicked).toString());
            if (positionClicked != 0) {
                prevData = new JSONObject(((NazmActivity) getActivity()).currentData.getJSONObject(positionClicked - 1).toString());
            } else {
                prevData = new JSONObject();
            }
            if (positionClicked == ((NazmActivity) getActivity()).currentData.length() - 1) {
                nextData = new JSONObject();

            } else {
                nextData = new JSONObject(((NazmActivity) getActivity()).currentData.getJSONObject(positionClicked + 1).toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initOptionAtFooter() {

        footor_dotsIcon = (ImageView) view.findViewById(R.id.footor_threedots);
        footor_audioIcon = (ImageView) view.findViewById(R.id.footor_audio);
        footor_videoIcon = (ImageView) view.findViewById(R.id.footor_video);
        footor_infoIcon = (ImageView) view.findViewById(R.id.footer_info);
        footer_default = (ImageView) view.findViewById(R.id.footer_default);
        footer_default.setVisibility(View.GONE);
        footor_dotsIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCriticOptionFooter();
            }
        });
        footor_shareIcon = (ImageView) view.findViewById(R.id.footer_share);
        footor_favIcon = (ImageView) view.findViewById(R.id.fooor_heart);
        try {
            if (CommonUtil.isContentUsersFav(data.getString("I"))) {
                footor_favIcon.setImageResource(R.drawable.ic_favorited);
                footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        footor_favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(getActivity());
                } else {
                    try {
                        if (CommonUtil.isContentUsersFav(data.getString("I"))) {
                            CommonUtil.removeFromFavTroughApi(getActivity(), data.getString("I"));
                            footor_favIcon.setImageResource(R.drawable.ic_favorite);
                            footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                        } else {

                            String title;
                            String auther;

                            if (CommonUtil.languageCode == 1) {
                                title = data.getString("TE");
                                auther = data.getString("PE");

                            } else if (CommonUtil.languageCode == 2) {
                                title = data.getString("TH");
                                auther = data.getString("PH");

                            } else if (CommonUtil.languageCode == 3) {
                                title = data.getString("TU");
                                auther = data.getString("PU");

                            } else {
                                title = data.getString("TE");
                                auther = data.getString("PE");
                            }

                            Intent intent = new Intent(getActivity(), GhazalContentService.class);

                            intent.putExtra("ghazalTitle", title);
                            intent.putExtra("ghazalAuther", auther);
                            intent.putExtra("jsonObject", data.toString());

                            getActivity().startService(intent);

                            CommonUtil.addToFavTroughApi(getActivity(), data.getString("I"));
                            CommonUtil.removeFromFavPref(data.getString("I"));
                            footor_favIcon.setImageResource(R.drawable.ic_favorited);
                            footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        });
        final boolean[] isaudioPlaying = {false};
        try {
            if (videoArrayData.length() == 0) {
                footor_videoIcon.setImageResource(0);
                footor_videoIcon.setOnClickListener(null);
            } else {
                footor_videoIcon.setImageResource(R.drawable.ic_video);
                footor_videoIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Bundle bundle = new Bundle();
                        bundle.putString("videoObj", videoArrayData.toString());
                        bundle.putString("cActivity", "NazmActivity");
                        YoutubeFragment youtubeFragment = new YoutubeFragment();
                        youtubeFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.NazmmainView, youtubeFragment, "videoFrag")
                                .addToBackStack(null)
                                .commit();

                    }
                });
            }


            if (audioArrayData.length() == 0) {
                footor_audioIcon.setImageResource(0);
                footor_audioIcon.setOnClickListener(null);
                footor_audioIcon.setVisibility(View.GONE);
                footer_default.setVisibility(View.VISIBLE);
            } else {
                footor_audioIcon.setImageResource(R.drawable.ic_audio);
                footor_audioIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {

                            Intent intent = new Intent(getActivity(), AudioActivity.class);
                            intent.putExtra("audioArray", audioArrayData.toString());
                            startActivity(intent);

                           /* Bundle bundle = new Bundle();
                            bundle.putString("audioArray", audioArrayData.toString());
                            AudioFragment audioFragment = new AudioFragment();
                            audioFragment.setArguments(bundle);
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.NazmmainView, audioFragment, "audioFrag")
                                    .addToBackStack(null)
                                    .commit();*/

                            //  MediaPlayerService.playAudio(audioUrl, footor_audioIcon);

                  /* if(!isaudioPlaying[0]) {
                       footor_audioIcon.setImageResource(android.R.drawable.ic_media_pause);
                        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        player.setDataSource(audioUrl);
                        player.prepare();
                        player.start();
                        isaudioPlaying[0] = true;
                    }else{
                        player.stop();
                       footor_audioIcon.setImageResource(R.drawable.ic_volume_medium);
                        isaudioPlaying[0] = false;
                    }*/
                /*    if(!isaudioPlaying[0]) {
                        player.setDataSource(audioUrl);
                        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mediaPlayer) {
                                player.start();
                                isaudioPlaying[0] = true;
                            }
                        });
                        player.prepareAsync();
                    }else{
                        isaudioPlaying[0] = false;
                        player.stop();
                    }*/
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        footor_shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(getActivity());
                } else {
                    shareThroughUrl();
                }

            }
        });
        //player = new MediaPlayer();

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            // restartActivity();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //restartActivity();
        }
    }

    public void getMeaningOfTheWord(final String mainWord, String word) {
        //getMeaningOfWord
        final ProgressDialog loading = ProgressDialog.show(getActivity(), null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        MeaningFragment meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.NazmmainView, meaningFragment, "meaningFrag")
                                .addToBackStack(null)
                                .commit();

                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });
    }

    public void showCriticOptionFooter() {
        mainFooter = view.findViewById(R.id.mainFooterContent);
        mainFooter.setVisibility(View.GONE);
        critiqueFooter = view.findViewById(R.id.critiqueFooter);
        critiqueFooter.setVisibility(View.VISIBLE);
        critiqueFooterWhenOn = view.findViewById(R.id.critiqueModeONTemp);
        critiqueFooterWhenOn.setVisibility(View.GONE);
        turnOffCriticText = (TextView) view.findViewById(R.id.turnOffCriticText);
        footerCrcitcOnText = (TextView) view.findViewById(R.id.footerCrcitcOnText);
        final ImageView criticInfo = (ImageView) view.findViewById(R.id.criticInfo);

        setCritciFooterText();
        turnOffCriticText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCritiqueModeOn = false;
                criticInfo.setImageResource(R.drawable.ic_critique);
                criticInfo.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                critiqueFooterWhenOn.setVisibility(View.GONE);
            }
        });

        ImageView criticClose = (ImageView) view.findViewById(R.id.critique_closeImg);
        criticClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideCriticOption();
            }
        });
        criticInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                criticInfo.setImageResource(R.drawable.ic_critiquefilled);
                criticInfo.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                showCriticScreen();
            }
        });
        final ImageView criticque_footor_favIcon = (ImageView) view.findViewById(R.id.cfooter_heart);
        try {
            if (CommonUtil.isContentUsersFav(data.getString("I"))) {
                criticque_footor_favIcon.setImageResource(R.drawable.ic_favorited);
                criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

            } else {
                criticque_footor_favIcon.setImageResource(R.drawable.ic_favorite);
                criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        criticque_footor_favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (CommonUtil.isContentUsersFav(data.getString("I"))) {
                        CommonUtil.removeFromFavTroughApi(getActivity(), data.getString("I"));
                        criticque_footor_favIcon.setImageResource(R.drawable.ic_favorite);
                        footor_favIcon.setImageResource(R.drawable.ic_favorite);
                        criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                        footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.lightGray), PorterDuff.Mode.SRC_IN);
                    } else {
                        CommonUtil.addToFavTroughApi(getActivity(), data.getString("I"));
                        criticque_footor_favIcon.setImageResource(R.drawable.ic_favorited);
                        footor_favIcon.setImageResource(R.drawable.ic_favorited);
                        criticque_footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                        footor_favIcon.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        ImageView shareFooterIcon = (ImageView) view.findViewById(R.id.cfooter_share);
        shareFooterIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareThroughUrl();
            }
        });

    }

    public void hideCriticOption() {
        mainFooter.setVisibility(View.VISIBLE);
        critiqueFooter.setVisibility(View.GONE);
    }

    public void showCriticScreen() {


        final Dialog critiqueDialog = new Dialog(getActivity());
        critiqueDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueDialog.setContentView(R.layout.critique_custom_screen);
        critiqueDialog.setCanceledOnTouchOutside(false);
        TextView okayText = (TextView) critiqueDialog.findViewById(R.id.critiqueOkText);
        final CheckBox doNotShowAgain = (CheckBox) critiqueDialog.findViewById(R.id.critiqueCheckBox);
        TextView CriticOntext = (TextView) critiqueDialog.findViewById(R.id.CriticOntext);
        TextView crciticLongtext = (TextView) critiqueDialog.findViewById(R.id.crciticLongtext);

        okayText.setText(LanguageSelection.getSpecificLanguageWordById("Okay", CommonUtil.languageCode));
        CriticOntext.setText(LanguageSelection.getSpecificLanguageWordById("Critique Mode on", CommonUtil.languageCode));
        crciticLongtext.setText(LanguageSelection.getSpecificLanguageWordById("Tap on any word", CommonUtil.languageCode));
        doNotShowAgain.setText(LanguageSelection.getSpecificLanguageWordById("Don't remind me again", CommonUtil.languageCode));


        okayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (doNotShowAgain.isChecked()) {
                    showCritiqueDialog = false;
                }
                isCritiqueModeOn = true;
                critiqueDialog.dismiss();
                setCritciFooterText();
                critiqueFooterWhenOn.setVisibility(View.VISIBLE);


            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueDialog.show();
        critiqueDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    public String getLineFromTheParagraph(JSONArray Larray, int lineClicked) throws JSONException {
        //   String line = "";
        String lineToReturn = "";
        String firstLine = "";
        String secondLine = "";
//        for (int j = 0; j < Larray.length(); j++) {
        JSONArray Warray = Larray.getJSONObject(lineClicked).getJSONArray("W");
        // line = line + "\n";

        if (CommonUtil.languageCode != 3) {
            for (int k = 0; k < Warray.length(); k++) {
                // line = line + " " + Warray.getJSONObject(k).getString("W");
//                    if (j == 1) {
//                        secondLine = secondLine + " " + Warray.getJSONObject(k).getString("W");
//                        ;
//                    }
//                    if (j == 0) {
                firstLine = firstLine + " " + Warray.getJSONObject(k).getString("W");
//                    }
            }
        } else {
            for (int k = Warray.length() - 1; k >= 0; k--) {
                // line = line + " " + Warray.getJSONObject(k).getString("W");
//                    if (j == 1) {
//                        secondLine = secondLine + " " + Warray.getJSONObject(k).getString("W");
//                    }
//                    if (j == 0) {
                firstLine = firstLine + " " + Warray.getJSONObject(k).getString("W");
//                    }
            }
        }

//        }
      /*  if (lineClicked == 0) {
            lineToReturn = firstLine;
        } else {
            lineToReturn = secondLine;
        }*/
        return firstLine;
    }

    public void ShowCritiqueSubmitForm(final int nazmLineNumber, final String nazmLine) {

        critiqueSubmitForm = new Dialog(getActivity(), R.style.Dialog);
        critiqueSubmitForm.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueSubmitForm.setContentView(R.layout.submit_critique_template);
        critiqueSubmitForm.setCanceledOnTouchOutside(false);

        TextView critiqueLine = (TextView) critiqueSubmitForm.findViewById(R.id.critiqueLine);
        TextView critiqueLineNumber = (TextView) critiqueSubmitForm.findViewById(R.id.critiqueLineNumber);
        critiqueLineNumber.setText(" LINE #" + nazmLineNumber);
        critiqueLine.setText(nazmLine);
        TextView critiqueSubmitText = (TextView) critiqueSubmitForm.findViewById(R.id.critiqueSubmit);
        TextView critiqueCancelText = (TextView) critiqueSubmitForm.findViewById(R.id.critiqe_cancel);
        ImageView closeModal = (ImageView) critiqueSubmitForm.findViewById(R.id.closeModal);
        closeModal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueSubmitForm.dismiss();
            }
        });
        final EditText critiqueUserName = (EditText) critiqueSubmitForm.findViewById(R.id.critique_UserName);
        final EditText critiqueUserEmail = (EditText) critiqueSubmitForm.findViewById(R.id.critique_UserEmail);
        final EditText critiqueUserComment = (EditText) critiqueSubmitForm.findViewById(R.id.critique_UserComment);
        isLoggedIn = DataPrefs.getBoolean("loggedIn", false);

        if (isLoggedIn) {
            critiqueUserName.setText(CommonUtil.userName);
            critiqueUserEmail.setText(CommonUtil.userEmail);

            critiqueUserName.setEnabled(false);
            critiqueUserEmail.setEnabled(false);
        } else {
            critiqueUserName.setText("");
            critiqueUserEmail.setText("");
        }
        critiqueSubmitText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // critiqueSubmitForm.dismiss();

                if (checkValidation(critiqueUserName.getText().toString(), critiqueUserEmail.getText().toString(), critiqueUserComment.getText().toString())) {
                    submitCritiqueData(critiqueUserName.getText().toString(), critiqueUserEmail.getText().toString(), critiqueUserComment.getText().toString(), nazmLine, nazmLineNumber);
                }
            }
        });
        critiqueCancelText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueSubmitForm.dismiss();
            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueSubmitForm.show();
        critiqueSubmitForm.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);


    }

    private boolean checkValidation(String name, String email, String comment) {
   /*     TextInputLayout citiqueUserName = (TextInputLayout) critiqueSubmitForm.findViewById(R.id.citiqueUserName);
        TextInputLayout citiqueUserEmail = (TextInputLayout) critiqueSubmitForm.findViewById(R.id.citiqueEMAIL);
        TextInputLayout citiqueUserComment = (TextInputLayout) critiqueSubmitForm.findViewById(R.id.citiqueUserCOMMENT);*/
        int allcorect = 0;

        if (name.equalsIgnoreCase("") || name.equalsIgnoreCase(" ")) {
//            citiqueUserName.setError("Not a valid name");
//            allcorect = allcorect+1;
            Toast.makeText(getActivity(), "Please enter your name", Toast.LENGTH_SHORT).show();
            return false;
        } else {
//            citiqueUserName.setError(null);
        }

        if (!CommonUtil.isValidEmail(email)) {
          /*  citiqueUserEmail.setError("Not a valid email-Id");
            allcorect = allcorect+1;*/
            Toast.makeText(getActivity(), "Not a valid Email", Toast.LENGTH_SHORT).show();
            return false;
        } else {
//            citiqueUserEmail.setError(null);
        }

        if (comment.equalsIgnoreCase("") || comment.equalsIgnoreCase(" ")) {
            /*citiqueUserComment.setError("Please enter a comment");
            allcorect = allcorect+1;*/
            Toast.makeText(getActivity(), "Please enter some text", Toast.LENGTH_SHORT).show();
            return false;
        } else {
//            citiqueUserComment.setError(null);
        }

        return true;


    }


    public void submitCritiqueData(String name, String email, String comment, String nazmLine, int nazmLineNumber) {
        HashMap obj = new HashMap();
        try {
            String pageUrl = "";
            if (CommonUtil.languageCode == 1) {
                pageUrl = data.getString("UE");
            } else if (CommonUtil.languageCode == 2) {
                pageUrl = data.getString("UH");
            } else if (CommonUtil.languageCode == 3) {
                pageUrl = data.getString("UU");
            } else {
                pageUrl = data.getString("UE");
            }

            obj.put("name", name);
            obj.put("email", email);
            obj.put("contentId", data.getString("I"));
            obj.put("contentTitle", toolbarTV.getText().toString());
            obj.put("pageType", "App Poem");
            obj.put("subject", "LINE #" + nazmLineNumber + " " + nazmLine);
            obj.put("message", comment);
            obj.put("typeOfQuery", 2);
            obj.put("pageUrl", pageUrl);
            //  Toast.makeText(getActivity(), "" + obj.toString(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        critiqueSubmitForm.dismiss();
        final ProgressDialog loading = ProgressDialog.show(getActivity(), null, "Loading..", false, false);
        RestClient.get().submitCritique(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        ShowThankYouForCritique();
                    } else {
                        Toast.makeText(getActivity(), "oops ! Error occured while submitting your request", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });
    }

    public void shareThroughUrl() {
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "";
            if (CommonUtil.languageCode == 1) {
                shareBody = data.getString("UE");
            } else if (CommonUtil.languageCode == 2) {
                shareBody = data.getString("UH");
            } else if (CommonUtil.languageCode == 3) {
                shareBody = data.getString("UU");
            } else {
                shareBody = data.getString("UE");
            }

            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "https://Rekhta.org");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // for showing ThankYou Dialog after Submitting Crtic
    public void ShowThankYouForCritique() {

        final Dialog critiqueThankYouDialog = new Dialog(getActivity());
        critiqueThankYouDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueThankYouDialog.setContentView(R.layout.critique_thanks_dialog_template);
        critiqueThankYouDialog.setCanceledOnTouchOutside(false);
        TextView thankYouText = (TextView) critiqueThankYouDialog.findViewById(R.id.thanksCritiqueText);
        thankYouText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueThankYouDialog.dismiss();
            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueThankYouDialog.show();
        critiqueThankYouDialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                critiqueThankYouDialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 2000);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((NazmActivity) getActivity()).setDrawerEnabled(true); // for enabling the Navigation Bar to Open Here
    }


    private void handleServerData(JSONObject obj, int langcode) throws JSONException {
        TextView ghazalTitle = (TextView) view.findViewById(R.id.gazal_title);
        TextView author_name = (TextView) view.findViewById(R.id.author_name);
        Integer _languageCode = CommonUtil.languageCode;
        String toolbarText = LanguageSelection.getSpecificLanguageWordById("nazm", _languageCode);
        toolbarTV.setText(toolbarText);

        JSONObject mainObj = obj.getJSONObject("R");
        alignmenht = mainObj.getDouble("RA");
        //Double _alignmenht = mainObj.getDouble("RA");
        if (!mainObj.getBoolean("EC")) {
            editorChoiceImg.setVisibility(View.GONE);
        } else {
            editorChoiceImg.setVisibility(View.VISIBLE);
        }
        if (mainObj.getBoolean("PC")) {
            popularchoiceImg.setVisibility(View.VISIBLE);
        } else {
            popularchoiceImg.setVisibility(View.GONE);
        }
        String title = mainObj.getString("CT");
        Log.d("Title", title);
        ghazalTitle.setText(title);
        author_name.setText(mainObj.getJSONObject("Poet").getString("CS").toUpperCase().replace("-", " "));
        detailedAuthorName = (MerriweatherBoldTextView) view.findViewById(R.id.detaied_authorName);
        detailedAuthorName.setText(mainObj.getJSONObject("Poet").getString("CS").toUpperCase().replace("-", " "));
        if (CommonUtil.languageCode == 1) {
            detailedAuthorName.setText(data.getString("PE"));
            CommonUtil.setEnglishMerriweatherBlack(getActivity(), ghazalTitle);
            CommonUtil.setEnglishMerriweatherBoldFont(getActivity(), detailedAuthorName);
            author_name.setText(data.getString("PE"));
        } else if (CommonUtil.languageCode == 2) {
            detailedAuthorName.setText(data.getString("PH"));
            CommonUtil.setRozaOne(getActivity(), ghazalTitle);
            CommonUtil.setHindiFont(getActivity(), detailedAuthorName);
            author_name.setText(data.getString("PH"));
        } else if (CommonUtil.languageCode == 3) {
            detailedAuthorName.setText(data.getString("PU"));
            CommonUtil.setUrduNotoNataliq(getActivity(), detailedAuthorName);
            CommonUtil.setUrduRuqa(getActivity(), ghazalTitle);
            author_name.setText(data.getString("PU"));
        } else {
            detailedAuthorName.setText(data.getString("PE"));
            CommonUtil.setEnglishMerriweatherBlack(getActivity(), ghazalTitle);
            CommonUtil.setEnglishMerriweatherBoldFont(getActivity(), detailedAuthorName);
            author_name.setText(data.getString("PE"));
        }
        //toolbarTV.setText(mainObj.getString("CT"));

        UpdateNextAndPreviousData();
        updateNextAndPrevoiusText();
        audioArrayData = new JSONArray(mainObj.getJSONArray("Audios").toString());
        videoArrayData = new JSONArray(mainObj.getJSONArray("Videos").toString());
        initOptionAtFooter();

        Glide.with(getActivity())
                .load("https://rekhta.org" + mainObj.getJSONObject("Poet").getString("IU"))
                .into(authorImage);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearL.removeAllViews();
        vScrollWrapper = new ExScrollView(getActivity());
        vScrollWrapper.setVerticalScrollBarEnabled(false);
        vScrollWrapper.setHorizontalScrollBarEnabled(false);

        vScrollWrapper.setFillViewport(true);
        //    vScrollWrapper.setScrollBarStyle(HorizontalScrollView.SCROLL_AXIS_NONE);
        vScrollWrapper.setLayoutParams(new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.WRAP_CONTENT));


        int minGap = 3;
        JSONObject mainContent = new JSONObject(mainObj.getString("CR"));
        JSONArray jsonArray = mainContent.getJSONArray("P");

        if (alignmenht == 1.0) {

            linearWrapper = new LinearLayout(getActivity());
            linearWrapper.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams layoutParams55 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            linearWrapper.setLayoutParams(layoutParams55);
            linearWrapper.setGravity(Gravity.CENTER_HORIZONTAL);
            renderCenterNazam(jsonArray, dim, linearWrapper);

        } else {

            linearWrapper = new LinearLayout(getActivity());
            linearWrapper.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams layoutParams55 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            linearWrapper.setLayoutParams(layoutParams55);


            renderNazamNewAgain(jsonArray, dim, linearWrapper);
        }

    }

    private void renderNazamNewAgain(JSONArray Parray, LinearLayout.LayoutParams dim, LinearLayout parentLayout) {

        //parentLayout.setPadding(8, 0, 0, 0);

        /*RelativeLayout.LayoutParams RelativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (alignmenht == 1.0) {
            RelativeLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            parentLayout.setLayoutParams(RelativeLayoutParams);
            // outerLinear.addView(linearWrapper);
        } else if (alignmenht == 0.0) {
            RelativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
            parentLayout.setLayoutParams(RelativeLayoutParams);
            //outerLinear.addView(linearWrapper);
        }*/

        for (int i = 0; i < Parray.length(); i++) {
            JSONArray Larray = null;
            try {
                Larray = Parray.getJSONObject(i).getJSONArray("L");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < Larray.length(); j++) {
                LinearLayout lineOfNazam = new LinearLayout(getActivity());
                LinearLayout.LayoutParams layoutParamsh = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lineOfNazam.setLayoutParams(layoutParamsh);
                lineOfNazam.setOrientation(LinearLayout.HORIZONTAL);

                JSONArray wordsArray = null;
                try {
                    wordsArray = Larray.getJSONObject(j).getJSONArray("W");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int noOfWords = wordsArray.length();
                lineOfNazam.setWeightSum(noOfWords);
                TextView[] textViewsInTheLine = new TextView[noOfWords];
                if (CommonUtil.languageCode != 3) {
                    forceLTRIfSupported();
                    for (int k = 0; k < noOfWords; k++) {
                        textViewsInTheLine[k] = new TextView(getActivity());
                        textViewsInTheLine[k].setIncludeFontPadding(false);
                        if (k == 0) {
                            textViewsInTheLine[k].setGravity(Gravity.START);
                        } else if (k == noOfWords - 1) {
                            textViewsInTheLine[k].setGravity(Gravity.END);
                        } else {
                            textViewsInTheLine[k].setGravity(Gravity.CENTER_HORIZONTAL);
                        }
                        //setWeight(textViewsInTheLine[k]);
                        try {
                            textViewsInTheLine[k].setText(" " + wordsArray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        textViewsInTheLine[k].setTextColor(Color.parseColor("#000000"));
                        if (CommonUtil.languageCode == 1) {
                            textViewsInTheLine[k].setTypeface(engtf);
                        } else {
                            textViewsInTheLine[k].setTypeface(hinditf);
                            CommonUtil.setHindiLailaRegular(getActivity(), textViewsInTheLine[k]);
                        }
                        try {
                            assignClickToText(textViewsInTheLine[k], wordsArray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        lineOfNazam.addView(textViewsInTheLine[k]);
                    }
                } else {
                    forceRTLIfSupported();
                    for (int k = 0; k < noOfWords; k++) {
                        textViewsInTheLine[k] = new TextView(getActivity());
                        textViewsInTheLine[k].setIncludeFontPadding(false);
                        //setWeight(textViewsInTheLine[k]);
                        try {
                            textViewsInTheLine[k].setText(" " + wordsArray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        textViewsInTheLine[k].setTextColor(Color.parseColor("#000000"));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            textViewsInTheLine[k].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                        }
                        textViewsInTheLine[k].setTypeface(urdutf);
                        try {
                            assignClickToText(textViewsInTheLine[k], wordsArray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        lineOfNazam.addView(textViewsInTheLine[k]);
                    }
                }
                if (j == Larray.length() - 1) {
                    lineOfNazam.setPadding(0, 0, 0, 60);
                } else if (Larray.length() == 1) {

                    lineOfNazam.setPadding(0, 4, 0, 0);
                } else if (i == Larray.length() - 1) {
                    lineOfNazam.setPadding(0, 4, 0, 4);
                } else {
                    lineOfNazam.setPadding(0, 4, 0, 4);
                }
                parentLayout.addView(lineOfNazam);
                lineOfNazam.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                parentLayout.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }
        vScrollWrapper.addView(parentLayout);
        vScrollWrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        zoomView.addView(vScrollWrapper);
        zoomView.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    private void renderCenterNazam(JSONArray Parray, LinearLayout.LayoutParams dim, LinearLayout parentLayout) {


        for (int i = 0; i < Parray.length(); i++) {
            JSONArray Larray = null;
            try {
                Larray = Parray.getJSONObject(i).getJSONArray("L");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < Larray.length(); j++) {
                LinearLayout lineOfNazam = new LinearLayout(getActivity());
                LinearLayout.LayoutParams layoutParamsh = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lineOfNazam.setLayoutParams(layoutParamsh);
                lineOfNazam.setGravity(Gravity.CENTER_HORIZONTAL);
                lineOfNazam.setOrientation(LinearLayout.HORIZONTAL);

                JSONArray wordsArray = null;
                try {
                    wordsArray = Larray.getJSONObject(j).getJSONArray("W");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                int noOfWords = wordsArray.length();
                lineOfNazam.setWeightSum(noOfWords);
                TextView[] textViewsInTheLine = new TextView[noOfWords];
                if (CommonUtil.languageCode != 3) {
                    forceLTRIfSupported();
                    for (int k = 0; k < noOfWords; k++) {
                        textViewsInTheLine[k] = new TextView(getActivity());
                        textViewsInTheLine[k].setIncludeFontPadding(false);
                        if (k == 0) {
                            textViewsInTheLine[k].setGravity(Gravity.START);
                        } else if (k == noOfWords - 1) {
                            textViewsInTheLine[k].setGravity(Gravity.END);
                        } else {
                            textViewsInTheLine[k].setGravity(Gravity.CENTER_HORIZONTAL);
                        }
                       /* if (noOfWords > 10) {
                            setWeight(textViewsInTheLine[k]);
                        } else {

                            // lineOfNazam.setGravity(Gravity.CENTER);
                            setWeight(textViewsInTheLine[k]);
                        }*/


                        textViewsInTheLine[k].setTextSize(getResources().getDimensionPixelSize(R.dimen._16ssp));
                        setWeightCenter(textViewsInTheLine[k]);

                        try {
                            textViewsInTheLine[k].setText(" " + wordsArray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        textViewsInTheLine[k].setTextColor(Color.parseColor("#000000"));
                        if (CommonUtil.languageCode == 1) {
                            textViewsInTheLine[k].setTypeface(engtf);
                        } else {
                            textViewsInTheLine[k].setTypeface(hinditf);
                            CommonUtil.setHindiLailaRegular(getActivity(), textViewsInTheLine[k]);
                        }
                        try {
                            assignClickToText(textViewsInTheLine[k], wordsArray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        lineOfNazam.addView(textViewsInTheLine[k]);
                    }
                } else {
                    forceRTLIfSupported();
                    for (int k = 0; k < noOfWords; k++) {
                        textViewsInTheLine[k] = new TextView(getActivity());
                        textViewsInTheLine[k].setIncludeFontPadding(false);
                        textViewsInTheLine[k].setTextSize(getResources().getDimensionPixelSize(R.dimen._14ssp));


                        if (k == 0) {
                            textViewsInTheLine[k].setGravity(Gravity.END);
                        } else if (k == noOfWords - 1) {
                            textViewsInTheLine[k].setGravity(Gravity.START);
                        } else {
                            textViewsInTheLine[k].setGravity(Gravity.CENTER_HORIZONTAL);
                        }

                        setWeightCenter(textViewsInTheLine[k]);
                        try {
                            textViewsInTheLine[k].setText(" " + wordsArray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        textViewsInTheLine[k].setTextColor(Color.parseColor("#000000"));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            textViewsInTheLine[k].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                        }
                        textViewsInTheLine[k].setTypeface(urdutf);
                        try {
                            assignClickToText(textViewsInTheLine[k], wordsArray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        lineOfNazam.addView(textViewsInTheLine[k]);
                    }
                }
                if (j == Larray.length() - 1 && Larray.length() > 1) {
                    lineOfNazam.setPadding(0, 0, 0, 60);
                } else if (Larray.length() == 1) {

                    lineOfNazam.setPadding(0, 4, 0, 0);
                } else {
                    lineOfNazam.setPadding(0, 4, 0, 4);
                }

                /*if (i == Larray.length() - 1) {
                    lineOfNazam.setPadding(0, 0, 0, 0);
                }*/

                parentLayout.addView(lineOfNazam);
                lineOfNazam.measure(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                parentLayout.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }
        vScrollWrapper.addView(parentLayout);
        vScrollWrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        zoomView.addView(vScrollWrapper);
        zoomView.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void setWeightCenter(TextView textView) {

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        // param.gravity = Gravity.CENTER;
        textView.setLayoutParams(param);

    }


    private void setWeight(TextView textView) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        param.gravity = Gravity.CENTER;
        textView.setLayoutParams(param);
    }

    private void widhtOfViewNew() {
        renderComplete = false;

        int childCount = linearWrapper.getChildCount();
        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = linearWrapper.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                int curWidthDiff = linearWrapper.getWidth() - lineView.getWidth();
                // LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                //linearWrapper.setPadding(curWidthDiff, 0, 0, curWidthDiff);
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        textView.setPadding(0, 0, extraPadding, 0);
                    }
                }
                //TextView
            }
        }


        float scaleF = (float) zoomView.getWidth() / (float) linearWrapper.getWidth();
        if (scaleF > (-1.0) && scaleF < (10.0)) {
            // scaleF is valid
            int requiredHeight = (int) (scaleF + 1 * linearWrapper.getMeasuredHeight());
            int requiredHeight2 = (int) (scaleF + linearWrapper.getMeasuredHeight());
            //zoomView.setWrapperHeight(requiredHeight, linearWrapper.getHeight());
            //  zoomView.setWrapperChildWidth(linearWrapper.getWidth());
            if (CommonUtil.languageCode == 3) {
                linearWrapper.setPivotX(linearWrapper.getWidth());
                linearWrapper.setPivotY(0);

                linearWrapper.setScaleX((float) (scaleF));
                linearWrapper.setScaleY((float) (scaleF));
            } else {
                linearWrapper.setPivotX(0);
                linearWrapper.setPivotY(0);

                linearWrapper.setScaleX((float) (scaleF));
                linearWrapper.setScaleY((float) (scaleF));
            }
        }
    }


}
