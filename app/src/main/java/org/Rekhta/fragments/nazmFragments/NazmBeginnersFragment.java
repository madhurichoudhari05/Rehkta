package org.Rekhta.fragments.nazmFragments;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.activites.NazmContentActivity;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.NazmActivity;
import org.Rekhta.adapters.nazm.NazmCommonListAdapter;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageChangeListener;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NazmBeginnersFragment extends Fragment {

    View view;
    ListView listview;
    ProgressDialog progressLoader;
    boolean isLoading = false;
    NazmCommonListAdapter nazmCommonListAdapter;
    JSONObject obj = new JSONObject();
    JSONArray dataArray = new JSONArray();
    int pageIndex = 1;
    int totalNumberOfContent;
    TextView listheaderText;
    private BroadcastReceiver mMessagRecevier;
    private Context context;

    public NazmBeginnersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_nazm_beginners, container, false);
        listview = (ListView) view.findViewById(R.id.nazm_begineersListview);
        context = getActivity();
        View header = getActivity().getLayoutInflater().inflate(R.layout.nazm_list_header, null);
        listheaderText = (TextView) header.findViewById(R.id.nazm_listHeaderTxt);
        listheaderText.setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode));
        listview.addHeaderView(header);
        setHeaderFont();
        getNazmBeginnerSData();
        return view;
    }

    private void setHeaderFont() {
        if (CommonUtil.languageCode == 1) {
            CommonUtil.setOswaldRegular(getActivity(), listheaderText);
        } else if (CommonUtil.languageCode == 2) {
            CommonUtil.setRozaOne(getActivity(), listheaderText);
        } else if (CommonUtil.languageCode == 3) {
            CommonUtil.setUrduRuqa(getActivity(), listheaderText);
        }
    }

    public void getNazmBeginnerSData() {

        // progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        RestClient.get().getContentListWithPaging(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "C54C4D8B-7E18-4F70-8312-E1C2CC028B0B", "BFCE2895-551E-4C07-B9D7-F7FF0DA9F9F5", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }

                isLoading = false;

                try {
                    obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        totalNumberOfContent = obj.getJSONObject("R").getInt("TC");
                        if (obj.getJSONObject("R").getJSONArray("CS").length() == 0) {
                            // do nothng
                        } else {
                            dataArray = CommonUtil.concatArray(dataArray, obj.getJSONObject("R").getJSONArray("CS"));
                            loadAdapter();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    //progressLoader.dismiss();
                }

            }
        });

    }

    public void updateListOnLangChange() {

        nazmCommonListAdapter.updateData(dataArray);
    }

    public void loadAdapter() {
        if (pageIndex == 1) {
            nazmCommonListAdapter = new NazmCommonListAdapter(getActivity(), dataArray);
            listview.setAdapter(nazmCommonListAdapter);
            ((NazmActivity) getActivity()).nazmCurrentBeginnerCommonListAdapterActive = nazmCommonListAdapter;
        } else {
            nazmCommonListAdapter.updateData(dataArray);
            listview.setSelection(pageIndex * 20 - 25);
        }
        CommonUtil.languageChangeListener.setListener(new LanguageChangeListener.ChangeListener() {
            @Override
            public void onChange() {
                updateListOnLangChange();
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {

                    if (position != 0) {

                        /*((NazmActivity) getActivity()).currentData = dataArray;
                        ((NazmActivity) getActivity()).currentIndexOfContent = position - 1;
                        NazmContent nazmContent = new NazmContent();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.drawer_layout, nazmContent, "nazmContentFragment")
                                .addToBackStack(null)
                                .commit();*/

                        Intent k = new Intent(getActivity(), NazmContentActivity.class);
                        k.putExtra("dataArray",dataArray.toString());
                        k.putExtra("postion",position-1);
                        context.startActivity(k);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        listview.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listview.getAdapter() == null)
                    return;

                if (listview.getAdapter().getCount() == 0)
                    return;

                int l = visibleItemCount + firstVisibleItem;
                if (l >= totalItemCount && !isLoading) {
                    // It is time to add new data. We call the listener
                    if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent)) {
                        pageIndex = pageIndex + 1;
                        isLoading = true;
                        loadData();
                    } else {
                        // Toast.makeText(getActivity(), "Complete" + pageIndex, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public void loadData() {
        if (pageIndex <= CommonUtil.calulateTotalApiCall(totalNumberOfContent)) {
            getNazmBeginnerSData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mMessagRecevier = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (listheaderText != null)
                    listheaderText.setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode));
                setHeaderFont();
                // getNazmBeginnerSData();

                if (nazmCommonListAdapter != null) {
                    nazmCommonListAdapter.notifyDataSetChanged();
                }
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessagRecevier, new IntentFilter("NazamTourble"));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessagRecevier);
    }

}
