package org.Rekhta.fragments;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.GoogleApiClient;


import org.Rekhta.MainActivity;
import org.Rekhta.R;
import org.Rekhta.activites.DictionaryActivity;
import org.Rekhta.activites.FeedBackActivity;
import org.Rekhta.activites.GhazalActivity;
import org.Rekhta.activites.ImageShyariActivity;;

import org.Rekhta.activites.MyFavoritesActivity;
import org.Rekhta.activites.NazmActivity;
import org.Rekhta.activites.PoetsActivity;
import org.Rekhta.activites.SettingActivity;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class NavigationFragment extends android.support.v4.app.Fragment implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks {

    public static ActionBarDrawerToggle mDrawerToggle;
    Context context;
    SharedPreferences DataPrefs = null;
    Button nav_eng_btn, nav_hin_btn, nav_urdu_btn;
    TextView navUsername, navUserEmail, nav_homeText, nav_myfavoritesText, nav_poetsText, nav_sherText, nav_ghazalsText,
            nav_nazmsText, nav_imagesayriText, nav_dictonaryText, nav_settingText, nav_aboutusText,
            nav_feedbackText, nav_logoutText, nav_poetryTV, nav_moreTV, langSelecText;
    LinearLayout nav_home, nav_myfavorites, nav_poets, nav_sher, nav_ghazals, nav_nazms,
            nav_imagesayri, nav_dictonary, nav_setting, nav_aboutus, nav_about, nav_feedback, nav_logout;
    private NavigationDrawerCallbacks mCallbacks;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    GoogleApiClient googleApiClient;

    public NavigationFragment() {
    }

    public static void changeDrawableButtonToBack() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
    }

    public static void changeDrawableButtonToHamburger() {
        mDrawerToggle.setDrawerIndicatorEnabled(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        DataPrefs = getActivity().getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);

        setTouchEffects();

    }

    private void setTouchEffects() {

        nav_home.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_myfavorites.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_poets.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_sher.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_ghazals.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_nazms.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_imagesayri.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_dictonary.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_setting.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_aboutus.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_feedback.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));
        nav_logout.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ScrollView scrollView = (ScrollView) inflater.inflate(R.layout.fragment_navigation, container, false);

        nav_eng_btn = (Button) scrollView.findViewById(R.id.nav_english_lang);
        nav_hin_btn = (Button) scrollView.findViewById(R.id.nav_hindi_lang);
        nav_urdu_btn = (Button) scrollView.findViewById(R.id.nav_urdu_lang);
        navUsername = (TextView) scrollView.findViewById(R.id.userNameDashboard);
        navUserEmail = (TextView) scrollView.findViewById(R.id.userEmailDashboard);

        if (CommonUtil.isSkipped) {
            navUsername.setText(LanguageSelection.getSpecificLanguageWordById("Guest", CommonUtil.languageCode));
            navUserEmail.setText(CommonUtil.userEmail);
        } else {
            navUsername.setText(CommonUtil.userName);
            navUserEmail.setText(CommonUtil.userEmail);
        }


        //initiate the tabs in navigation Menu
        nav_home = (LinearLayout) scrollView.findViewById(R.id.nav_home);
        nav_myfavorites = (LinearLayout) scrollView.findViewById(R.id.nav_myfavariotes);
        nav_poets = (LinearLayout) scrollView.findViewById(R.id.nav_Poets);
        nav_sher = (LinearLayout) scrollView.findViewById(R.id.nav_sher);
        nav_ghazals = (LinearLayout) scrollView.findViewById(R.id.nav_ghazals);
        nav_nazms = (LinearLayout) scrollView.findViewById(R.id.nav_nazms);
        nav_imagesayri = (LinearLayout) scrollView.findViewById(R.id.nav_image_shyari);
        nav_dictonary = (LinearLayout) scrollView.findViewById(R.id.nav_dictionary);
        nav_setting = (LinearLayout) scrollView.findViewById(R.id.nav_settings);
        nav_feedback = (LinearLayout) scrollView.findViewById(R.id.nav_feedback);
        nav_aboutus = (LinearLayout) scrollView.findViewById(R.id.nav_about);
        nav_logout = (LinearLayout) scrollView.findViewById(R.id.nav_logout);

        nav_homeText = (TextView) scrollView.findViewById(R.id.homeText);
        nav_myfavoritesText = (TextView) scrollView.findViewById(R.id.favText);
        nav_poetsText = (TextView) scrollView.findViewById(R.id.poetText);
        nav_sherText = (TextView) scrollView.findViewById(R.id.sherText);
        nav_ghazalsText = (TextView) scrollView.findViewById(R.id.ghazalText);
        nav_nazmsText = (TextView) scrollView.findViewById(R.id.nazmText);
        nav_imagesayriText = (TextView) scrollView.findViewById(R.id.imageshyariText);
        nav_dictonaryText = (TextView) scrollView.findViewById(R.id.dictonaryText);
        nav_settingText = (TextView) scrollView.findViewById(R.id.settingText);
        nav_aboutusText = (TextView) scrollView.findViewById(R.id.aboutText);
        nav_feedbackText = (TextView) scrollView.findViewById(R.id.feedbackText);
        nav_logoutText = (TextView) scrollView.findViewById(R.id.logoutText);
        nav_poetryTV = (TextView) scrollView.findViewById(R.id.nav_poetryTV);
        nav_moreTV = (TextView) scrollView.findViewById(R.id.nav_moreTV);
        langSelecText = (TextView) scrollView.findViewById(R.id.langSelecText);

        initNavWidgets();
        return scrollView;


    }


    public void setUp(int fragmentId, final DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                int langIntCode = DataPrefs.getInt("langCode", 0);
                if (langIntCode == 0) {
                    resetButtonColor(1);
                } else {
                    resetButtonColor(langIntCode);
                }
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (toolbar != null)
                    toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });


    }

    public void disableDrawerIcon() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);
    }


    @Override
    public void onResume() {
        super.onResume();
        nav_home.setOnClickListener(this);
        nav_myfavorites.setOnClickListener(this);
        nav_poets.setOnClickListener(this);
        nav_sher.setOnClickListener(this);
        nav_ghazals.setOnClickListener(this);
        nav_nazms.setOnClickListener(this);
        nav_imagesayri.setOnClickListener(this);
        nav_dictonary.setOnClickListener(this);
        nav_setting.setOnClickListener(this);
        nav_feedback.setOnClickListener(this);
        nav_aboutus.setOnClickListener(this);
        nav_logout.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onClick(View view) {
        if (mDrawerLayout != null)
            mDrawerLayout.closeDrawer(containerView);
        int id = view.getId();
        switch (id) {
            case R.id.nav_home:
                if (mCallbacks != null)
                    mCallbacks.goToHomeScreen();
                break;
            case R.id.nav_myfavariotes:
                if (mCallbacks != null)

                    if (CommonUtil.isSkipped) {
                        CommonUtil.showToast(getActivity());
                    } else {
                        startActivity(new Intent(getActivity(), MyFavoritesActivity.class));
                    }

                break;

            case R.id.nav_Poets:
                if (mCallbacks != null)
                    //   mCallbacks.goToSettingPage();
                    startActivity(new Intent(getActivity(), PoetsActivity.class));
                break;

            case R.id.nav_sher:
                if (mCallbacks != null)
                    startActivity(new Intent(getActivity(), SherActivity.class));
                break;
            case R.id.nav_ghazals:
                goToGhazal();
                break;

            case R.id.nav_nazms:
                if (mCallbacks != null)
                    //mCallbacks.goToSharefunction();
                    startActivity(new Intent(getActivity(), NazmActivity.class));
                break;
            case R.id.nav_image_shyari:
                if (mCallbacks != null)
                    //mCallbacks.goToVideoLessonsFunction();
                    startActivity(new Intent(getActivity(), ImageShyariActivity.class));
                break;

            case R.id.nav_dictionary:
                if (mCallbacks != null)
                    startActivity(new Intent(getActivity(), DictionaryActivity.class));
                break;
            case R.id.nav_settings:
                if (mCallbacks != null)
                    // mCallbacks.goToPracticeScreen();
                    startActivity(new Intent(getActivity(), SettingActivity.class));
                break;

            case R.id.nav_about:
                if (mCallbacks != null)
                    //mCallbacks.goToSettingPage();
                    OpenAboutInBrowser();
                break;

            case R.id.nav_feedback:
                if (mCallbacks != null)
                    // mCallbacks.goTohelpFunction();
                    startActivity(new Intent(getActivity(), FeedBackActivity.class));
                break;
            case R.id.nav_logout:
                if (CommonUtil.isSkipped) {
                    goToLogin();
                } else {
                    logoutUser();
                }

                break;
            default:
                throw new UnsupportedOperationException("Id not found " + id);
        }
    }

    private void goToLogin() {

        startActivity(new Intent(getActivity(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }

    private void logoutUser() {

        //FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();


        Intent i = new Intent(getActivity(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        DataPrefs.edit().putBoolean("loggedIn", false).commit();
        getActivity().overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);
        getActivity().startActivity(i);
        getActivity().finish();
    }

    private void initNavWidgets() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetButtonColor(1);
        } else {
            resetButtonColor(langIntCode);
        }

        //navUsername.setText(loggedInUserName);


        nav_eng_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetButtonColor(1);
                if (mCallbacks != null) {
                    mCallbacks.langChanged();
                }
//                closeDrawer();
            }
        });

        nav_hin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetButtonColor(2);
                if (mCallbacks != null) {
                    mCallbacks.langChanged();
                }
//                closeDrawer();
            }
        });

        nav_urdu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetButtonColor(3);
                if (mCallbacks != null) {
                    mCallbacks.langChanged();
                }
//                   closeDrawer();
            }
        });

    }

    //change to About Site Rekhta Url
    public void OpenAboutInBrowser() {
        String aboutUsUrl = "https://rekhta.org/cms/about-site";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(aboutUsUrl));
        startActivity(i);
    }

    // to reset the Button on Click
    public void resetButtonColor(int langCode) {

        CommonUtil.languageCode = langCode;
        CommonUtil.languageChangeListener.setBoo(true);

        nav_eng_btn.setBackgroundResource(R.drawable.languagebtn_white_border);
        nav_eng_btn.setTextColor(Color.WHITE);
        nav_hin_btn.setBackgroundResource(R.drawable.languagebtn_white_border);
        nav_hin_btn.setTextColor(Color.WHITE);
        nav_urdu_btn.setBackgroundResource(R.drawable.languagebtn_white_border);
        nav_urdu_btn.setTextColor(Color.WHITE);

        if (langCode == 1) {
            forceLTRIfSupported();

            nav_eng_btn.setBackgroundResource(R.drawable.languagebtn_white_selected);
            nav_eng_btn.setTextColor(Color.BLACK);
            setSpecificTextAccToLang();
        }
        if (langCode == 2) {
            forceLTRIfSupported();
            nav_hin_btn.setBackgroundResource(R.drawable.languagebtn_white_selected);
            nav_hin_btn.setTextColor(Color.BLACK);
            setSpecificTextAccToLang();
        }
        if (langCode == 3) {
            forceRTLIfSupported();
            nav_urdu_btn.setBackgroundResource(R.drawable.languagebtn_white_selected);
            nav_urdu_btn.setTextColor(Color.BLACK);
            setSpecificTextAccToLang();

        }

        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();


    }

    public void goToGhazal() {
        Intent intent = new Intent(getActivity(), GhazalActivity.class);
        getActivity().startActivity(intent);
    }

    public void onViewCreated() {

    }

    public void closeDrawer() {
        if (CommonUtil.languageCode != 3)
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        else
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
    }

    private void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.LEFT);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void setSpecificTextAccToLang() {

        langSelecText.setText(LanguageSelection.getSpecificLanguageWordById("Language Quick switch", CommonUtil.languageCode));

        if (CommonUtil.isSkipped) {
            navUsername.setText(LanguageSelection.getSpecificLanguageWordById("Guest", CommonUtil.languageCode));
            navUserEmail.setText(CommonUtil.userEmail);
        } else {
            navUsername.setText(CommonUtil.userName);
            navUserEmail.setText(CommonUtil.userEmail);
        }

        CommonUtil.setSpecificLanguage(nav_homeText, "home", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_myfavoritesText, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_poetsText, "poets", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_sherText, "sher", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_ghazalsText, "ghazal", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_nazmsText, "nazm", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_imagesayriText, "shayari_gallery", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_dictonaryText, "dictionary", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_settingText, "setting", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_aboutusText, "about", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_feedbackText, "feedback", CommonUtil.languageCode);
        if (CommonUtil.isSkipped) {
            nav_logoutText.setText(LanguageSelection.getSpecificLanguageWordById("Login", CommonUtil.languageCode));
        } else {
            CommonUtil.setSpecificLanguage(nav_logoutText, "logout", CommonUtil.languageCode);
        }

        CommonUtil.setSpecificLanguage(nav_poetryTV, "poetry", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(nav_moreTV, "more", CommonUtil.languageCode);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void goToHomeScreen();

        void langChanged();


    }

}
