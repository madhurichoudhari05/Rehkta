package org.Rekhta.fragments.sherFragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.activites.Dashboard;
import org.Rekhta.views.MeaningFragment;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SearchActivity;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.adapters.SherCollectionsAdapter;
import org.Rekhta.model.SherCollection;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.MODE_PRIVATE;


public class CollectionSherView extends Fragment {

    RecyclerView sherRecyclerView;
    SherCollectionsAdapter sherCollectionsAdapter;
    ArrayList<SherCollection> arrayList;
    public boolean anothervalue = false;

    TextView[] textview, newTextview;
    LinearLayout linearlayout, newLinearLayout;
    private LinearLayout linear;

    String contentId = "";
    //----
    Toolbar toolbar;
    TextView toolbarTV;
    ImageView backButton;
    Dialog dialog;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    SharedPreferences DataPrefs = null;
    WindowManager.LayoutParams wlp;
    int pageIndex = 1;
    JSONArray dataArray = new JSONArray();
    int totalNumberOfContent;
    boolean isDataLoading = false;
    private String homeFrom;
    private Context context;
    private ProgressDialog progressLoader;


    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        contentId = getArguments().getString("I");

        // homeFrom = getArguments().getString("fromHome");

        if (getActivity() instanceof SherActivity) {
            contentId = ((SherActivity) getActivity()).currentSherID;
        }


        if (getActivity() instanceof Dashboard)

        {
            contentId = ((Dashboard) getActivity()).currentSherID;
        }

        context = getActivity();

        View vv = inflater.inflate(R.layout.collection_head, null);


        toolbar = (Toolbar) vv.findViewById(R.id.toolbarfrag);
        toolbar.inflateMenu(R.menu.home_screen);
        toolbarTV = (TextView) toolbar.findViewById(R.id.toobartextview);
        CommonUtil.setSpecificLanguage(toolbarTV, "sher", CommonUtil.languageCode);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_settings:
                        showRightMenuLangSelectionOption();
                        return true;
                    case R.id.search:
                        // Toast.makeText(getActivity(), "searching", Toast.LENGTH_SHORT).show();
                        getActivity().startActivity(new Intent(getActivity(), SearchActivity.class));
                        return true;
                    default:

                }
                return false;
            }
        });

        sherRecyclerView = (RecyclerView) vv.findViewById(R.id.sherRecycler);
        arrayList = new ArrayList<>();
        getTop20SelectedSher();
        sherCollectionsAdapter = new SherCollectionsAdapter(arrayList, getActivity(), CollectionSherView.this);
        sherRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        sherRecyclerView.setAdapter(sherCollectionsAdapter);
        initRightMenuPopUpDialog();
        backButton = (ImageView) vv.findViewById(R.id.backButton);
        CommonUtil.setBackButton(getActivity(), backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonUtil.dontClose = true;
                getActivity().onBackPressed();
            }
        });

        sherRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    //Toast.makeText(getActivity(),"LAst",Toast.LENGTH_LONG).show();
                    Log.e("_____", "" + totalNumberOfContent);
                    CommonUtil.cSherTotalCount = totalNumberOfContent + "";
                    Log.e("_____", "" + CommonUtil.calulateTotalApiCall(totalNumberOfContent));
                    if (totalNumberOfContent <= 20) {

                    } else {
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getTop20SelectedSher();
                        }
                    }
                }
            }
        });

        return vv;

    }

    private void colllist() {
        arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            // arrayList.add(new SherCollection("Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry " + i));
        }
    }

    public void getTop20SelectedSher() {

        if (getActivity() instanceof SherActivity) {
            ((SherActivity) getActivity()).setDrawerEnabled(false);

        } else if (getActivity() instanceof Dashboard) {
            ((Dashboard) getActivity()).setDrawerEnabled(false);

        }

        // for disabling the Navigation Bar to Open Here
        isDataLoading = true;

        if (!((Activity) context).isFinishing()) {
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }

        RestClient.get().getSlectedSherCollectionData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "F722D5DC-45DA-41EC-A439-900DF702A3D6", contentId, "", pageIndex, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {
                        if (progressLoader != null && progressLoader.isShowing())
                            progressLoader.dismiss();
                        try {

                            JSONObject content = new JSONObject(res);
                            if (content.getInt("S") == 1) {
                                totalNumberOfContent = content.getJSONObject("R").getInt("TC");
                                /*dataArray = content.getJSONObject("R").getJSONArray("CD");*/
                                CommonUtil.cSherTotalCount = totalNumberOfContent + "";
                                arrayList.clear();
                                dataArray = CommonUtil.concatArray(dataArray, content.getJSONObject("R").getJSONArray("CD"));

                                for (int i = 0; i < dataArray.length(); i++) {

                                    arrayList.add(new SherCollection(dataArray.getJSONObject(i).getString("RE"), dataArray.getJSONObject(i).getString("RH"),
                                            dataArray.getJSONObject(i).getString("RU"), dataArray.getJSONObject(i).getString("PE"), dataArray.getJSONObject(i).getString("PH"),
                                            dataArray.getJSONObject(i).getString("PU"), dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("UE")
                                            , dataArray.getJSONObject(i).getString("UH"), dataArray.getJSONObject(i).getString("UU"), dataArray.getJSONObject(i).getJSONArray("TS"),
                                            dataArray.getJSONObject(i).getString("P"),dataArray.getJSONObject(i).getString("PI")));
                                }

                                sherCollectionsAdapter.notifyDataSetChanged();
                                isDataLoading = false;
                            } else {

                                Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (progressLoader != null && progressLoader.isShowing())
                            progressLoader.dismiss();
                        Toast.makeText(getActivity(), "failed Getting Data", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {

        CommonUtil.setSpecificLanguage(toolbarTV, "sher", langCode);

        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);
        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }

        if (CommonUtil.languageCode != langCode) {
            CommonUtil.languageCode = langCode;
            saveTheLangCodeLocally();
        }

        dialog.dismiss();
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        DataPrefs = getActivity().getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);
        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                sherCollectionsAdapter.notifyDataSetChanged();
                dialog.dismiss();
                CommonUtil.setBackButton(getActivity(), backButton);
                if (getActivity() instanceof SherActivity) {
                    ((SherActivity) getActivity()).forceLTRIfSupported();
                } else if (getActivity() instanceof Dashboard) {
                    ((Dashboard) getActivity()).forceLTRIfSupported();
                }


            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                sherCollectionsAdapter.notifyDataSetChanged();
                dialog.dismiss();
                CommonUtil.setBackButton(getActivity(), backButton);

                if (getActivity() instanceof SherActivity) {
                    ((SherActivity) getActivity()).forceLTRIfSupported();
                } else if (getActivity() instanceof Dashboard) {
                    ((Dashboard) getActivity()).forceLTRIfSupported();
                }

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                sherCollectionsAdapter.notifyDataSetChanged();
                dialog.dismiss();
                CommonUtil.setBackButton(getActivity(), backButton);

                if (getActivity() instanceof SherActivity) {
                    ((SherActivity) getActivity()).forceRTLIfSupported();
                } else if (getActivity() instanceof Dashboard) {
                    ((Dashboard) getActivity()).forceRTLIfSupported();
                }


            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getActivity() instanceof SherActivity) {
            ((SherActivity) getActivity()).setDrawerEnabled(true);
        } else if (getActivity() instanceof Dashboard) {
            ((Dashboard) getActivity()).setDrawerEnabled(true);
        }
        // for disabling the Navigation Bar to Open Here
    }

    public void showmeaningFragment(TextView textView, JSONObject obj) {

        MeaningFragment meaningFragment = (MeaningFragment) getActivity().getSupportFragmentManager().findFragmentByTag("meaningFrag");
        if (meaningFragment != null && meaningFragment.isVisible()) {
            // showMeaning(mainText.getText().toString(), obj);

        } else {

            showMeaning(textView.getText().toString(), obj);


        }
    }

    private void showMeaning(String s, JSONObject obj) {

        try {
            getMeaningOfTheWord(s, obj.getString("M"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMeaningOfTheWord(final String mainWord, String word) {

        final ProgressDialog loading = ProgressDialog.show(context, null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (loading != null && loading.isShowing())
                    loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putString("home", "sher");
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        MeaningFragment meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .add(R.id.drawer_layout, meaningFragment, "meaningFrag")
                                .addToBackStack(null)
                                .commit();

                    } else {
                        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (loading != null && loading.isShowing())
                    loading.dismiss();
            }
        });


    }
}
