package org.Rekhta.fragments.sherFragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.adapters.TagRecyclerViewAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.Tags;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Admin on 9/27/2017.
 */

public class TagsFragment extends Fragment implements NavigationFragment.NavigationDrawerCallbacks {


    RecyclerView tagRecyclerView;
    TagRecyclerViewAdapter tagRecyclerViewAdapter;
    ArrayList<Tags> arrayList;
    EditText serachEdittext;
    FrameLayout tag_progressbarholder;
    private BroadcastReceiver mMessagRecevier;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tags_layout, container, false);
        tagRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_tag);
        tag_progressbarholder = (FrameLayout) view.findViewById(R.id.tag_progressbarholder);
        getSherTagData();
        serachEdittext = (EditText) view.findViewById(R.id.tag_serach_editText);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            serachEdittext.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_search, 0);
        }
//        serachEdittext.clearFocus();
        serachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("Serach with tags", CommonUtil.languageCode));
        initSearchBox();
        tagRecyclerViewAdapter = new TagRecyclerViewAdapter(arrayList, getActivity());
        tagRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        tagRecyclerView.setAdapter(tagRecyclerViewAdapter);
        ((SherActivity) getActivity()).tagRecyclerViewAdapter = tagRecyclerViewAdapter;
        return view;
    }

    void initSearchBox() {
        serachEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                filter(s.toString());
            }
        });

        serachEdittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    void filter(String text) {
        ArrayList<Tags> SearchedTextarrayList = new ArrayList<>();
        SearchedTextarrayList.clear();
        switch (CommonUtil.languageCode) {
            case 1:
//                    SearchedTextarrayList.clear();
                for (Tags d : arrayList) {

                    if (d.getGazaltextInEnglish().toLowerCase().contains(text.toLowerCase())) {
                        SearchedTextarrayList.add(d);
                    }
                }
                break;
            case 2:
                for (Tags d : arrayList) {

                    if (d.getGazaltextInHindi().toLowerCase().contains(text.toLowerCase())) {
                        SearchedTextarrayList.add(d);
                    }
                }
                break;
        }
//            if (d.getGazaltextInEnglish().toLowerCase().contains(text.toLowerCase())) {
//                SearchedTextarrayList.add(d);
//            }

        //update recyclerview
        if (SearchedTextarrayList.size() == 0) {
            getView().findViewById(R.id.noRestultTag).setVisibility(View.VISIBLE);
        } else {
            getView().findViewById(R.id.noRestultTag).setVisibility(View.GONE);

        }

        tagRecyclerViewAdapter.updateList(SearchedTextarrayList);


    }

    public void getSherTagData() {
        arrayList = new ArrayList<>();
        tagRecyclerView.setVisibility(View.GONE);
        tag_progressbarholder.setVisibility(View.VISIBLE);
        RestClient.get().getSherTagData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {
                        tagRecyclerView.setVisibility(View.VISIBLE);
                        tag_progressbarholder.setVisibility(View.GONE);
                        try {
                            JSONObject apiResponse = new JSONObject(res);
                            if (apiResponse.getInt("S") == 1) {
                                JSONArray resp = apiResponse.getJSONArray("R");
                                for (int i = 0; i < resp.length(); i++) {
                                    arrayList.add(new Tags(resp.getJSONObject(i).getString("NE"), resp.getJSONObject(i).getString("NH"), resp.getJSONObject(i).getString("NU"), String.valueOf(resp.getJSONObject(i).getInt("T")), resp.getJSONObject(i).getString("I")));
                                }
                                tagRecyclerViewAdapter.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getActivity(), "Please check your network connection", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void langChanged() {
        if (serachEdittext != null) {
            serachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("Serach with tags", CommonUtil.languageCode));
        }
//        serachEdittext.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();

        mMessagRecevier = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                serachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("Serach with tags", CommonUtil.languageCode));
            }
        };

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessagRecevier, new IntentFilter("TruobleSher"));
    }
}
