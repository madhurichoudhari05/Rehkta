package org.Rekhta.fragments.sherFragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.SherActivity;
import org.Rekhta.adapters.CollectionAdapter;
import org.Rekhta.model.CollectionData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class
CollectonFragment extends Fragment {

    RecyclerView collectionRecyclerView;
    CollectionAdapter collectionAdapter;
    ArrayList<CollectionData> arrayList;
    View view;
    private ProgressDialog progressLoader;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = getActivity();
        view = inflater.inflate(R.layout.collection_layout, container, false);
        getSherCollectionData();
        collectionRecyclerView = (RecyclerView) view.findViewById(R.id.collection_recyclerview);

        collectionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        String top20String = LanguageSelection.getSpecificLanguageWordById("Top 20 collections", CommonUtil.languageCode);
        collectionAdapter = new CollectionAdapter(arrayList, getActivity());
        collectionRecyclerView.setAdapter(collectionAdapter);

        ((SherActivity) getActivity()).collectionAdapter = collectionAdapter;

        return view;
    }

    /*    private void init() {
            arrayList = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                arrayList.add(new CollectionData(R.drawable.back1,"hello","hi"));
            }
        }*/
    public void getSherCollectionData() {
        arrayList = new ArrayList<>();
        if (!((Activity) context).isFinishing()) {
            progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);

        }

        RestClient.get().getTop20Sher(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
                try {

                    JSONObject content = new JSONObject(res);

                    if (content.getInt("S") == 1) {
                        JSONArray dataArray = content.getJSONArray("R");

                        for (int i = 0; i < dataArray.length(); i++) {
                            dataArray.getJSONObject(i).getString("Te");
                            arrayList.add(new CollectionData(dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("Sl"), dataArray.getJSONObject(i).getString("Ne")
                                    , dataArray.getJSONObject(i).getString("Nh"), dataArray.getJSONObject(i).getString("Nu"), dataArray.getJSONObject(i).getString("Te"), dataArray.getJSONObject(i).getString("Th"), dataArray.getJSONObject(i).getString("Tu")));
                        }

                        collectionAdapter.notifyDataSetChanged();

                    } else {

                        Toast.makeText(getActivity(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
                Toast.makeText(getActivity(), "failed-----------------", Toast.LENGTH_SHORT).show();
            }
        });
    }

}