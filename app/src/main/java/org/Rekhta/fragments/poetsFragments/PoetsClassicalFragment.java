package org.Rekhta.fragments.poetsFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetsActivity;
import org.Rekhta.adapters.poets.PoetsListAdapter;
import org.Rekhta.model.poets.PoetsDataModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PoetsClassicalFragment extends Fragment {


    View view;
    PoetsListAdapter poetsListAdapter;
    ArrayList<PoetsDataModel> arrayList = new ArrayList<>();
    ;
    RecyclerView poetClassicalRecylerView;
    EditText poetClassicalSerachEdittext;

    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    JSONArray dataArray = new JSONArray();
    private BroadcastReceiver messageReceiver;
    Context context;
    Activity activity;

    public PoetsClassicalFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (Activity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pots_classical, container, false);

        //init();
        context = getActivity();
        poetClassicalRecylerView = (RecyclerView) view.findViewById(R.id.poet_classical_recyclerview);
        poetsListAdapter = new PoetsListAdapter(arrayList, getActivity());
        poetClassicalRecylerView.setAdapter(poetsListAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        poetClassicalRecylerView.setLayoutManager(linearLayoutManager);
        poetClassicalRecylerView.setHasFixedSize(true);

        ((PoetsActivity) getActivity()).poetsClassicalFragment = poetsListAdapter;
        poetClassicalSerachEdittext = (EditText) view.findViewById(R.id.poetclassical_serach_editText);

        if (CommonUtil.languageCode == 3) {
            poetClassicalSerachEdittext.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null, null, null);
        } else {
            poetClassicalSerachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null);
        }

        poetClassicalSerachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("search_poets_name", CommonUtil.languageCode));

        initSearchBox();
        getClassicalTabDataOfPoets();
        poetClassicalRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {

                    } else {

                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            ((PoetsActivity)getActivity()).showProgressDialog();
                            getClassicalTabDataOfPoets();
                        }
                    }
                }
            }
        });
        return view;
    }


    public void getClassicalTabDataOfPoets() {


        isDataLoading = true;
        //arrayList.clear();
//        final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);

        RestClient.get().getPoetsData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "517093F6-D295-4389-AA02-AEA52E738B97", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
//               if (progressLoader != null && progressLoader.isShowing())                    progressLoader.dismiss();
                ((PoetsActivity)getActivity()).dismissDialog();
                try {
                    JSONObject content = new JSONObject(res);
                    if (content.getInt("S") == 1) {

                        JSONObject obj = content.getJSONObject("R");
                        totalNumberOfContent = obj.getInt("TC");
                        //dataArray = CommonUtil.concatArray(dataArray, obj.getJSONArray("P"));
                        dataArray = obj.getJSONArray("P");

                        for (int i = 0; i < dataArray.length(); i++) {

                            arrayList.add(new PoetsDataModel(dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("NE")
                                    , dataArray.getJSONObject(i).getString("NH"), dataArray.getJSONObject(i).getString("NU"), dataArray.getJSONObject(i).getInt("GC"), dataArray.getJSONObject(i).getInt("NC"),
                                    dataArray.getJSONObject(i).getInt("SC"), dataArray.getJSONObject(i).getString("DE"), dataArray.getJSONObject(i).getString("DH"),
                                    dataArray.getJSONObject(i).getString("DU"), dataArray.getJSONObject(i).getBoolean("N"), dataArray.getJSONObject(i).getString("SL")));
                        }

                        poetsListAdapter.notifyDataSetChanged();



                    } else {

                        Toast.makeText(context, "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }
                    isDataLoading = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
//               if (progressLoader != null && progressLoader.isShowing())                    progressLoader.dismiss();
                Toast.makeText(context, "failed-----------------", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*    private void init() { getClassicalTabDataOfPoets
        arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
           // arrayList.add(new PoetsDataModel( i+"Kumar",true,"hello","hi","hello","hi",""));
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();

        messageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //getClassicalTabDataOfPoets();
                poetsListAdapter.notifyDataSetChanged();
                if (CommonUtil.languageCode == 3) {
                    poetClassicalSerachEdittext.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null, null, null);
                } else {
                    poetClassicalSerachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null);
                }
                poetClassicalSerachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("search_poets_name", CommonUtil.languageCode));
            }
        };

        LocalBroadcastManager.getInstance(context).registerReceiver(messageReceiver, new IntentFilter("PoetsBroadcast"));
    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(context).unregisterReceiver(messageReceiver);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        LocalBroadcastManager.getInstance(context).unregisterReceiver(messageReceiver);
    }

    void initSearchBox() {

        poetClassicalSerachEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    void filter(String text) {

        ArrayList<PoetsDataModel> SearchedTextarrayList = new ArrayList<>();
        for (PoetsDataModel pdm : arrayList) {
            if (pdm.getPoetNameInEng().toLowerCase().contains(text.toLowerCase())) {
                SearchedTextarrayList.add(pdm);
            }
        }
        poetsListAdapter.updateList(SearchedTextarrayList);
        if (SearchedTextarrayList.size() == 0) {
            view.findViewById(R.id.no_result_found).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.no_result_found).setVisibility(View.GONE);

        }
    }
}
