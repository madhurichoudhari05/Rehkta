package org.Rekhta.fragments.poetsFragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetsActivity;
import org.Rekhta.adapters.poets.PoetsListAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.CollectionData;
import org.Rekhta.model.Tags;
import org.Rekhta.model.poets.PoetsDataModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PoetsAllFragment extends Fragment implements NavigationFragment.NavigationDrawerCallbacks {


    View view;
    PoetsListAdapter poetsListAdapter;
    ArrayList<PoetsDataModel> arrayList = new ArrayList<>();
    RecyclerView poetAllRecylerView;
    EditText poetAllserachEdittext;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    JSONArray dataArray = new JSONArray();
    private Context context;
    private BroadcastReceiver messageReceiver;
    private ProgressDialog progressLoader;


    public PoetsAllFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.fragment_poets_all, container, false);

            initViews();
        }
        return view;
    }


    private void initViews() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //init();
        poetAllRecylerView = (RecyclerView) view.findViewById(R.id.poet_all_recyclerview);
        poetAllRecylerView.setNestedScrollingEnabled(false);

        context = getActivity();
        poetAllRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        poetsListAdapter = new PoetsListAdapter(arrayList, getActivity());
        ((PoetsActivity) getActivity()).poetsAllFragmentAdapter = poetsListAdapter;
        poetAllRecylerView.setAdapter(poetsListAdapter);

        poetAllserachEdittext = (EditText) view.findViewById(R.id.poetAll_serach_editText);
        //poetAllserachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(getActivity(), R.drawable.ic_searchtab), null);

        if (CommonUtil.languageCode == 3) {
            poetAllserachEdittext.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null, null, null);
        } else {
            poetAllserachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null);
        }
        poetAllserachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("search_poets_name", CommonUtil.languageCode));
        initSearchBox();
        getAllTabDataOfPoets();
        poetAllRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {
                        // Toast.makeText(context, "lessthan20", Toast.LENGTH_SHORT).show();

                    } else {
                        //    Toast.makeText(context, "greatwet", Toast.LENGTH_SHORT).show();
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getAllTabDataOfPoets();
                        }
                    }
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

        messageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                // getAllTabDataOfPoets();
                if (CommonUtil.languageCode == 3) {
                    poetAllserachEdittext.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null, null, null);
                } else {
                    poetAllserachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null);
                }
                poetsListAdapter.notifyDataSetChanged();
                poetAllserachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("search_poets_name", CommonUtil.languageCode));
            }
        };

        LocalBroadcastManager.getInstance(context).registerReceiver(messageReceiver, new IntentFilter("PoetsBroadcast"));
    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onDetach() {
        super.onDetach();

        LocalBroadcastManager.getInstance(context).unregisterReceiver(messageReceiver);
    }

    public void getAllTabDataOfPoets() {

        //arrayList.clear();
        isDataLoading = true;

        if (!((Activity) context).isFinishing()) {
            if (progressLoader != null && progressLoader.isShowing())
                progressLoader = ProgressDialog.show(context, null, "Please wait...", false, false);
        }

        RestClient.get().getPoetsData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                try {
                    JSONObject content = new JSONObject(res);
                    if (content.getInt("S") == 1) {

                        JSONObject obj = content.getJSONObject("R");
                        totalNumberOfContent = obj.getInt("TC");
                        JSONArray jsonArray = obj.getJSONArray("P");

                        //dataArray = CommonUtil.concatArray(dataArray, obj.getJSONArray("P"));
                        dataArray = obj.getJSONArray("P");
                        for (int i = 0; i < dataArray.length(); i++) {

                            arrayList.add(new PoetsDataModel(dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("NE")
                                    , dataArray.getJSONObject(i).getString("NH"), dataArray.getJSONObject(i).getString("NU"), dataArray.getJSONObject(i).getInt("GC"), dataArray.getJSONObject(i).getInt("NC"),
                                    dataArray.getJSONObject(i).getInt("SC"), dataArray.getJSONObject(i).getString("DE"), dataArray.getJSONObject(i).getString("DH"),
                                    dataArray.getJSONObject(i).getString("DU"), dataArray.getJSONObject(i).getBoolean("N"), dataArray.getJSONObject(i).getString("SL")));
                        }

                        poetsListAdapter.notifyDataSetChanged();

                        if (progressLoader != null && progressLoader.isShowing()) {
                            progressLoader.dismiss();
                        }


                    } else {

                        Toast.makeText(context, "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }
                    isDataLoading = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing())
                    progressLoader.dismiss();
                Toast.makeText(getActivity(), "failed-----------------", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void initSearchBox() {

        poetAllserachEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //poetsListAdapter.updateList(arrayList);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //poetsListAdapter.updateList(arrayList);
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    void filter(String text) {
        ArrayList<PoetsDataModel> SearchedTextarrayList = new ArrayList<>();
        for (PoetsDataModel pdm : arrayList) {
            if (pdm.getPoetNameInEng().toLowerCase().contains(text.toLowerCase())) {
                SearchedTextarrayList.add(pdm);
            }
        }
        //update recyclerview
        poetsListAdapter.updateList(SearchedTextarrayList);
        if (SearchedTextarrayList.size() == 0) {
            view.findViewById(R.id.no_result_found).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.no_result_found).setVisibility(View.GONE);

        }
    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (progressLoader != null && progressLoader.isShowing()) {
            progressLoader.dismiss();
        }
    }

    @Override
    public void langChanged() {

        if (poetAllserachEdittext != null)
            poetAllserachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("Search by name", CommonUtil.languageCode));
    }
}
