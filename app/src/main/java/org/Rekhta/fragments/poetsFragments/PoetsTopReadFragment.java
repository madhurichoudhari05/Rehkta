package org.Rekhta.fragments.poetsFragments;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import org.Rekhta.R;
import org.Rekhta.activites.PoetsActivity;
import org.Rekhta.adapters.poets.PoetsListAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.poets.PoetsDataModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class PoetsTopReadFragment extends Fragment {

    View view;
    PoetsListAdapter poetsListAdapter;
    ArrayList<PoetsDataModel> arrayList = new ArrayList<>();
    ;
    RecyclerView poetTopReadRecylerView;
    EditText poetTopReadserachEdittext;

    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    JSONArray dataArray = new JSONArray();

    boolean isDataLoaded = false;
    private BroadcastReceiver messageReceiver;

    Context context;

    public PoetsTopReadFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_poets_top_read, container, false);
            context = getActivity();
            initViews();
        }

        return view;
    }

    private void initViews() {

        poetTopReadRecylerView = (RecyclerView) view.findViewById(R.id.poet_topRead_recyclerview);
        poetTopReadRecylerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        poetsListAdapter = new PoetsListAdapter(arrayList, getActivity());
        poetTopReadRecylerView.setAdapter(poetsListAdapter);
        poetTopReadRecylerView.setHasFixedSize(false);
        poetTopReadRecylerView.canScrollHorizontally(LinearLayoutManager.VERTICAL);
        ((PoetsActivity) getActivity()).poetsTopReadFragment = poetsListAdapter;
        poetTopReadserachEdittext = (EditText) view.findViewById(R.id.poetTopRead_serach_editText);

        poetTopReadserachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("search_poets_name", CommonUtil.languageCode));

        if (CommonUtil.languageCode == 3) {
            poetTopReadserachEdittext.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null, null, null);
        } else {
            poetTopReadserachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null);
        }

        initSearchBox();
        getTopReadTabDataOfPoets();
        poetTopReadRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {

                    if (totalNumberOfContent <= 20) {

                    } else {
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            ((PoetsActivity)getActivity()).showProgressDialog();
                            getTopReadTabDataOfPoets();
                        }
                    }
                }
            }
        });

    }


    public void getTopReadTabDataOfPoets() {


        isDataLoading = true;
        // arrayList.clear();
//    final ProgressDialog progressLoader = ProgressDialog.show(getActivity(), null, "Please wait...", false, false);

        RestClient.get().getPoetsData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "3AB1ACC4-E080-4B77-BFC1-41E2908E6C2D", "", pageIndex, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
//           if (progressLoader != null && progressLoader.isShowing())
//                   progressLoader.dismiss();
                try {
                    JSONObject content = new JSONObject(res);
                    ((PoetsActivity)getActivity()).dismissDialog();
                    if (content.getInt("S") == 1) {

                        JSONObject obj = content.getJSONObject("R");
                        totalNumberOfContent = obj.getInt("TC");
                        //dataArray = CommonUtil.concatArray(dataArray, obj.getJSONArray("P"));
                        dataArray = obj.getJSONArray("P");

                        for (int i = 0; i < dataArray.length(); i++) {

                            arrayList.add(new PoetsDataModel(dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("NE")
                                    , dataArray.getJSONObject(i).getString("NH"), dataArray.getJSONObject(i).getString("NU"), dataArray.getJSONObject(i).getInt("GC"), dataArray.getJSONObject(i).getInt("NC"),
                                    dataArray.getJSONObject(i).getInt("SC"), dataArray.getJSONObject(i).getString("DE"), dataArray.getJSONObject(i).getString("DH"),
                                    dataArray.getJSONObject(i).getString("DU"), dataArray.getJSONObject(i).getBoolean("N"), dataArray.getJSONObject(i).getString("SL")));
                        }

                        poetsListAdapter.notifyDataSetChanged();

                    } else {

                        Toast.makeText(context, "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }
                    isDataLoading = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                //  if (progressLoader != null && progressLoader.isShowing())                    progressLoader.dismiss();
                Toast.makeText(context, "failed-----------------", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        messageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                //  getTopReadTabDataOfPoets();

                poetsListAdapter.notifyDataSetChanged();
                if (CommonUtil.languageCode == 3) {
                    poetTopReadserachEdittext.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null, null, null);
                } else {
                    poetTopReadserachEdittext.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, R.drawable.ic_searchtab), null);
                }
                poetTopReadserachEdittext.setHint(LanguageSelection.getSpecificLanguageWordById("search_poets_name", CommonUtil.languageCode));
            }
        };

        LocalBroadcastManager.getInstance(context).registerReceiver(messageReceiver, new IntentFilter("PoetsBroadcast"));
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();


    }

    @Override
    public void onDetach() {
        super.onDetach();
        LocalBroadcastManager.getInstance(context).unregisterReceiver(messageReceiver);

    }

    void initSearchBox() {

        poetTopReadserachEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    void filter(String text) {
        ArrayList<PoetsDataModel> SearchedTextarrayList = new ArrayList<>();
        for (PoetsDataModel pdm : arrayList) {
            if (pdm.getPoetNameInEng().toLowerCase().contains(text.toLowerCase())) {
                SearchedTextarrayList.add(pdm);
            }
        }
        //update recyclerview
        poetsListAdapter.updateList(SearchedTextarrayList);
        if (SearchedTextarrayList.size() == 0) {
            view.findViewById(R.id.no_result_found).setVisibility(View.VISIBLE);
        } else {
            view.findViewById(R.id.no_result_found).setVisibility(View.GONE);

        }
    }

}
