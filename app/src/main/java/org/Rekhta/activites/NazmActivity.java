package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayer;

import org.json.JSONArray;


import org.Rekhta.R;;
import org.Rekhta.adapters.nazm.NazmCommonListAdapter;
import org.Rekhta.adapters.nazm.NazmViewPagerAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.fragments.nazmFragments.NazmContent;
import org.Rekhta.interfaces.DrawerLocker;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MediaPlayerService;

public class NazmActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks, DrawerLocker {

    public NavigationFragment drawerFragment;
    public JSONArray currentData;
    public int currentIndexOfContent = 0;
    public YouTubePlayer currentPlayer;
    public boolean isVideoPlaying = false;
    public boolean isAudioPlaying = false;
    public NazmCommonListAdapter nazmCurrentTop50CommonListAdapterActive;
    public NazmCommonListAdapter nazmCurrentBeginnerCommonListAdapterActive;
    public NazmCommonListAdapter nazmCurrentHumorCommonListAdapterActive;
    public NazmCommonListAdapter nazmCurrentEditorCommonListAdapterActive;
    Toolbar toolbar;
    SharedPreferences DataPrefs = null;
    NazmViewPagerAdapter nazmViewPagerAdapter = null;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    WindowManager.LayoutParams wlp;
    DrawerLayout drawerLayout;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nazm);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        drawerFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        initTabs();
    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(NazmActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(NazmActivity.this);

    }


    public void initTabs() {

        tabLayout = (TabLayout) findViewById(R.id.nazm_tablayout);
        //setupTabIcons();
        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("50 Nazms", CommonUtil.languageCode)));
        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode)));
        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("Editor's Choice", CommonUtil.languageCode)));
        tabLayout.addTab(tabLayout.newTab().setText(LanguageSelection.getSpecificLanguageWordById("Humour Satire", CommonUtil.languageCode)));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);


        final ViewPager viewPager = (ViewPager) findViewById(R.id.nazm_viewpager);
        viewPager.setOffscreenPageLimit(1);
        nazmViewPagerAdapter = new NazmViewPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(nazmViewPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        changeTabNameAccToLang();
    }

    public void changeTabNameAccToLang() {

        tabLayout.getTabAt(0).setText(LanguageSelection.getSpecificLanguageWordById("50 Nazms", CommonUtil.languageCode));
        tabLayout.getTabAt(1).setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode));
        tabLayout.getTabAt(2).setText(LanguageSelection.getSpecificLanguageWordById("Editor's Choice", CommonUtil.languageCode));
        tabLayout.getTabAt(3).setText(LanguageSelection.getSpecificLanguageWordById("Humour Satire", CommonUtil.languageCode));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(NazmActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NazmActivity.this, MyFavoritesActivity.class));
            }
        });

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                drawerFragment.resetButtonColor(1);
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                drawerFragment.resetButtonColor(2);
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                drawerFragment.resetButtonColor(3);
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {
        Intent intent = new Intent(this, Dashboard.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            // getFragmentManager().popBackStack();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            FragmentManager fm = getSupportFragmentManager();
            if (CommonUtil.fromMeaning) {
                fm.popBackStackImmediate();
                CommonUtil.fromMeaning = false;
            } else {
                for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                sendLocalbroadCast();

                changeTabNameAccToLang();
            }


        } else {

            getFragmentManager().popBackStack();
        }
        if (isVideoPlaying) {
            if (currentPlayer != null)
                this.currentPlayer.release();
        }
        if (isAudioPlaying) {
            MediaPlayerService.stopAudio();
        }
        //MediaPlayerService.stopAudio(new ImageView(this));
    }


    public void removeFragment() {
        getFragmentManager().popBackStack();
        startFragment();
    }

    public void startFragment() {
        NazmContent nazmContent = new NazmContent();
//                    ghazalDetails.setArguments(bundle);
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.drawer_layout, nazmContent, "nazm_content_frag")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void langChanged() {

        if (nazmCurrentEditorCommonListAdapterActive != null)
            nazmCurrentEditorCommonListAdapterActive.notifyDataSetChanged();
        if (nazmCurrentHumorCommonListAdapterActive != null)
            nazmCurrentHumorCommonListAdapterActive.notifyDataSetChanged();
        if (nazmCurrentBeginnerCommonListAdapterActive != null)
            nazmCurrentBeginnerCommonListAdapterActive.notifyDataSetChanged();
        if (nazmCurrentTop50CommonListAdapterActive != null)
            nazmCurrentTop50CommonListAdapterActive.notifyDataSetChanged();


        if (CommonUtil.languageCode == 3) {
            drawerFragment.resetButtonColor(3);
            forceRTLIfSupported();
            drawerLayout.closeDrawers();
        } else if (CommonUtil.languageCode == 2) {
            drawerFragment.resetButtonColor(2);
            forceLTRIfSupported();
            drawerLayout.closeDrawers();
        } else {
            drawerFragment.resetButtonColor(1);
            forceLTRIfSupported();
            drawerLayout.closeDrawers();
        }
        changeTabNameAccToLang();

        sendLocalbroadCast();


    }

    private void setupTabIcons() {


        TextView tabHome = (TextView) LayoutInflater.from(NazmActivity.this).inflate(R.layout.custom_tab, null);
        tabHome.setText(LanguageSelection.getSpecificLanguageWordById("50 Nazms", CommonUtil.languageCode));
        tabLayout.getTabAt(0).setCustomView(tabHome);

        TextView dashBoard = (TextView) LayoutInflater.from(NazmActivity.this).inflate(R.layout.custom_tab, null);
        dashBoard.setText(LanguageSelection.getSpecificLanguageWordById("Beginners", CommonUtil.languageCode));
        tabLayout.getTabAt(1).setCustomView(dashBoard);


        TextView invite = (TextView) LayoutInflater.from(NazmActivity.this).inflate(R.layout.custom_tab, null);
        invite.setText(LanguageSelection.getSpecificLanguageWordById("Editor's Choice", CommonUtil.languageCode));
        tabLayout.getTabAt(2).setCustomView(invite);
        // animationCreate(invite);


        TextView tabDeal = (TextView) LayoutInflater.from(NazmActivity.this).inflate(R.layout.custom_tab, null);
        tabDeal.setText(LanguageSelection.getSpecificLanguageWordById("Humour Satire", CommonUtil.languageCode));
        tabLayout.getTabAt(3).setCustomView(tabDeal);


    }

    private void sendLocalbroadCast() {

        Intent intent = new Intent("NazamTourble");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawerLayout.setDrawerLockMode(lockMode);
    }


}
