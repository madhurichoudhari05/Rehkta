package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import org.Rekhta.R;;
import org.Rekhta.adapters.poets.PoetsAdapter;
import org.Rekhta.adapters.poets.PoetsListAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

public class PoetsActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {

    public NavigationFragment drawerFragment;
    public PoetsListAdapter poetsAllFragmentAdapter;
    public PoetsListAdapter poetsTopReadFragment;
    public PoetsListAdapter poetsWomenFragment;
    public PoetsListAdapter poetsYoungFragment;
    public PoetsListAdapter poetsClassicalFragment;
    PoetsAdapter poetsAdapter;
    ViewPager poetsViewPager;
    TabLayout poetsTablayout;
    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    private Toolbar toolbar;
    ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_poets);

        poetsViewPager = (ViewPager) findViewById(R.id.poets_viewpager);
        poetsViewPager.setOffscreenPageLimit(2);
        poetsTablayout = (TabLayout) findViewById(R.id.poets_tablayout);
        poetsAdapter = new PoetsAdapter(getSupportFragmentManager());
        poetsTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        poetsViewPager.setAdapter(poetsAdapter);
        poetsTablayout.setupWithViewPager(poetsViewPager);
        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        sendPoetBroadcast();

        changeTabNameAccToLang();

        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        CommonUtil.registerReciver(PoetsActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        CommonUtil.unregisterReciebr(PoetsActivity.this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(PoetsActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PoetsActivity.this, MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                drawerFragment.resetButtonColor(1);
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                drawerFragment.resetButtonColor(2);
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                drawerFragment.resetButtonColor(3);
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {

        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(PoetsActivity.this, Dashboard.class));
        finish();
    }

    @Override
    public void langChanged() {

        /*if (poetsAllFragmentAdapter != null)
            poetsAllFragmentAdapter.notifyDataSetChanged();
        if (poetsTopReadFragment != null)
            poetsTopReadFragment.notifyDataSetChanged();
        if (poetsWomenFragment != null)
            poetsWomenFragment.notifyDataSetChanged();
        if (poetsYoungFragment != null)
            poetsYoungFragment.notifyDataSetChanged();
        if (poetsClassicalFragment != null)
            poetsClassicalFragment.notifyDataSetChanged();*/


        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }

        changeTabNameAccToLang();

        sendPoetBroadcast();
    }


    private void sendPoetBroadcast() {

        Intent intent = new Intent("PoetsBroadcast");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void changeTabNameAccToLang() {

        if (poetsTablayout != null) {

            poetsTablayout.getTabAt(0).setText(LanguageSelection.getSpecificLanguageWordById("all", CommonUtil.languageCode));
            poetsTablayout.getTabAt(1).setText(LanguageSelection.getSpecificLanguageWordById("top", CommonUtil.languageCode));
            poetsTablayout.getTabAt(2).setText(LanguageSelection.getSpecificLanguageWordById("womans_poets", CommonUtil.languageCode));
            poetsTablayout.getTabAt(3).setText(LanguageSelection.getSpecificLanguageWordById("Young Poets", CommonUtil.languageCode));
            poetsTablayout.getTabAt(4).setText(LanguageSelection.getSpecificLanguageWordById("Classical Poets", CommonUtil.languageCode));
        }


    }

    public void showProgressDialog() {
        progressDialog = new ProgressDialog(PoetsActivity.this);
        progressDialog.setMessage("Loading...");

        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
        }

    }

    public void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }
}
