package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


import org.Rekhta.R;;
import org.Rekhta.adapters.poets.PoetsAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.CollectionData;
import org.Rekhta.model.SettingModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;

public class SettingActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks, View.OnClickListener {

    public NavigationFragment drawerFragment;
    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    @BindView(R.id.setNotif)
    TextView setNotif;
    @BindView(R.id.setNewPass)
    TextView setNewPassTxt;
    @BindView(R.id.setNewPass2)
    TextView setNewPass2;
    @BindView(R.id.setChangePassTxt)
    TextView setChangePassTxt;
    @BindView(R.id.favMngmntText)
    TextView favMngmntText;
    @BindView(R.id.saveFavOffline)
    TextView saveFavOffline;
    @BindView(R.id.setSherOFday)
    TextView setSherOFday;
    @BindView(R.id.setRemoveSTOD)
    TextView setRemoveSTOD;
    @BindView(R.id.setHinlang)
    TextView setHinlang;
    @BindView(R.id.setEnglang)
    TextView setEnglang;
    @BindView(R.id.setUrdulang)
    TextView setUrdulang;
    @BindView(R.id.setPrefLang)
    TextView setPrefLang;
    @BindView(R.id.settingWordDay)
    TextView settingWordDay;
    @BindView(R.id.settingReceMeaning)
    TextView settingReceMeaning;
    @BindView(R.id.settingEventNotif)
    TextView settingEventNotif;
    @BindView(R.id.settingUpdateRekhta)
    TextView settingUpdateRekhta;
    @BindView(R.id.settingGenericNotif)
    TextView settingGenericNotif;
    @BindView(R.id.settingAllOther)
    TextView settingAllOther;
    @BindView(R.id.settingNewsLetter)
    TextView settingNewsLetter;
    @BindView(R.id.settingRecveLetter)
    TextView settingRecveLetter;
    @BindView(R.id.settingLatestFromRekhta)
    TextView settingLatestFromRekhta;
    @BindView(R.id.AccountTxt)
    TextView AccountTxt;
    @BindView(R.id.setting_Change_Password_Layout)
    LinearLayout changePasswordLayout;

    private SettingModel settingModel;
    private SwitchCompat IsRequestedNewsLetterSwitch, IsWordOfTheDaySubscriberSwitch, IsSaveFavoriteOfflineSwitch, IsGenericNotificationSubscriberSwitch, IsSherOfTheDaySubscriberSwitch;
    private CheckBox IsContentSpecificNotificationEnCheckBox, IsContentSpecificNotificationHiCheckBox, IsContentSpecificNotificationUrCheckBox;
    private Toolbar toolbar;
    private SwitchCompat IsEventNotificationSubscriberSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        ButterKnife.bind(this);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        initViews();
        getSettingData();
        if (CommonUtil.isSkipped) {
          changePasswordLayout.setVisibility(View.GONE);
        } else {
            changePasswordLayout.setVisibility(View.VISIBLE);
        }

    }

    private void initViews() {


        IsRequestedNewsLetterSwitch =  findViewById(R.id.IsRequestedNewsLetterSwitch);
        IsRequestedNewsLetterSwitch.setOnClickListener(this);
        IsWordOfTheDaySubscriberSwitch =  findViewById(R.id.IsWordOfTheDaySubscriberSwitch);
        IsWordOfTheDaySubscriberSwitch.setOnClickListener(this);
        IsSaveFavoriteOfflineSwitch =  findViewById(R.id.IsSaveFavoriteOfflineSwitch);
        IsSaveFavoriteOfflineSwitch.setOnClickListener(this);
        IsGenericNotificationSubscriberSwitch =  findViewById(R.id.IsGenericNotificationSubscriberSwitch);
        IsGenericNotificationSubscriberSwitch.setOnClickListener(this);
        IsEventNotificationSubscriberSwitch =  findViewById(R.id.IsEventNotificationSubscriberSwitch);
        IsEventNotificationSubscriberSwitch.setOnClickListener(this);
        IsSherOfTheDaySubscriberSwitch =  findViewById(R.id.IsSherOfTheDaySubscriberSwitch);
        IsSherOfTheDaySubscriberSwitch.setOnClickListener(this);

        IsContentSpecificNotificationEnCheckBox = (CheckBox) findViewById(R.id.IsContentSpecificNotificationEnCheckBox);
        IsContentSpecificNotificationEnCheckBox.setOnClickListener(this);
        IsContentSpecificNotificationHiCheckBox = (CheckBox) findViewById(R.id.IsContentSpecificNotificationHiCheckBox);
        IsContentSpecificNotificationHiCheckBox.setOnClickListener(this);
        IsContentSpecificNotificationUrCheckBox = (CheckBox) findViewById(R.id.IsContentSpecificNotificationUrCheckBox);
        IsContentSpecificNotificationUrCheckBox.setOnClickListener(this);

        setDataAccordingToLang();

    }

    private void setDataAccordingToLang() {

        setNotif.setText(LanguageSelection.getSpecificLanguageWordById("Notification", CommonUtil.languageCode));
        setNewPassTxt.setText(LanguageSelection.getSpecificLanguageWordById("New Password", CommonUtil.languageCode));
        setNewPass2.setText(LanguageSelection.getSpecificLanguageWordById("New Password", CommonUtil.languageCode));
        favMngmntText.setText(LanguageSelection.getSpecificLanguageWordById("Favorites Mangement", CommonUtil.languageCode));
        saveFavOffline.setText(LanguageSelection.getSpecificLanguageWordById("save Favorites Offline", CommonUtil.languageCode));
        setSherOFday.setText(LanguageSelection.getSpecificLanguageWordById("Sher of the Day", CommonUtil.languageCode));
        setRemoveSTOD.setText(LanguageSelection.getSpecificLanguageWordById("Remove  images daily", CommonUtil.languageCode));
        setHinlang.setText(LanguageSelection.getSpecificLanguageWordById("Hindi", CommonUtil.languageCode));
        setEnglang.setText(LanguageSelection.getSpecificLanguageWordById("English", CommonUtil.languageCode));
        setUrdulang.setText(LanguageSelection.getSpecificLanguageWordById("Urdu", CommonUtil.languageCode));
        setPrefLang.setText(LanguageSelection.getSpecificLanguageWordById("preferred Lang", CommonUtil.languageCode));

        settingWordDay.setText(LanguageSelection.getSpecificLanguageWordById("Word of the Day", CommonUtil.languageCode));
        settingReceMeaning.setText(LanguageSelection.getSpecificLanguageWordById("Receive meaning daily", CommonUtil.languageCode));
        settingEventNotif.setText(LanguageSelection.getSpecificLanguageWordById("Event Notifications", CommonUtil.languageCode));
        settingUpdateRekhta.setText(LanguageSelection.getSpecificLanguageWordById("Updates about rekhta", CommonUtil.languageCode));
        settingGenericNotif.setText(LanguageSelection.getSpecificLanguageWordById("Genric Notifcations", CommonUtil.languageCode));
        settingAllOther.setText(LanguageSelection.getSpecificLanguageWordById("All other new and happenings", CommonUtil.languageCode));

        settingNewsLetter.setText(LanguageSelection.getSpecificLanguageWordById("Newsletter", CommonUtil.languageCode));
        settingRecveLetter.setText(LanguageSelection.getSpecificLanguageWordById("Receive newsletter", CommonUtil.languageCode));
        settingLatestFromRekhta.setText(LanguageSelection.getSpecificLanguageWordById("latest from rekhta", CommonUtil.languageCode));

        setChangePassTxt.setText(LanguageSelection.getSpecificLanguageWordById("Change Password", CommonUtil.languageCode));

        AccountTxt.setText(LanguageSelection.getSpecificLanguageWordById("Account", CommonUtil.languageCode));

    }



    /* {
                                IsContentSpecificNotificationEn = 1,
                                IsContentSpecificNotificationHi = 2,
                                IsContentSpecificNotificationUr = 3,

                                IsEventNotificationSubscriber = 4,
                                IsSaveFavoriteOffline = 5,
                                IsSherOfTheDaySubscriber = 6,
                                IsWordOfTheDaySubscriber = 7,
                                IsGenericNotificationSubscriber = 8,
                                IsRequestedNewsLetter = 0
                }*/


    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(SettingActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(SettingActivity.this);

    }




    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.IsContentSpecificNotificationEnCheckBox) { //1
            if (settingModel.isContentSpecificNotificationEn()) {
                settingModel.setContentSpecificNotificationEn(false);
            } else {
                settingModel.setContentSpecificNotificationEn(true);
            }
            IsContentSpecificNotificationEnCheckBox.setChecked(settingModel.isContentSpecificNotificationEn());
            updateSettingData(1, settingModel.isContentSpecificNotificationEn());

        } else if (id == R.id.IsContentSpecificNotificationHiCheckBox) { //2
            if (settingModel.isContentSpecificNotificationHi()) {
                settingModel.setContentSpecificNotificationHi(false);
            } else {
                settingModel.setContentSpecificNotificationHi(true);
            }
            IsContentSpecificNotificationHiCheckBox.setChecked(settingModel.isContentSpecificNotificationHi());
            updateSettingData(2, settingModel.isContentSpecificNotificationHi());

        } else if (id == R.id.IsContentSpecificNotificationUrCheckBox) {//3
            if (settingModel.isContentSpecificNotificationUr()) {
                settingModel.setContentSpecificNotificationUr(false);
            } else {
                settingModel.setContentSpecificNotificationUr(true);
            }
            IsContentSpecificNotificationUrCheckBox.setChecked(settingModel.isContentSpecificNotificationUr());
            updateSettingData(3, settingModel.isContentSpecificNotificationUr());

        } else if (id == R.id.IsEventNotificationSubscriberSwitch) {//4
            if (settingModel.isEventNotificationSubscriber()) {
                settingModel.setEventNotificationSubscriber(false);
            } else {
                settingModel.setEventNotificationSubscriber(true);
            }
            IsEventNotificationSubscriberSwitch.setChecked(settingModel.isEventNotificationSubscriber());
            updateSettingData(4, settingModel.isEventNotificationSubscriber());

        } else if (id == R.id.IsSaveFavoriteOfflineSwitch) {//5
            if (settingModel.isSaveFavoriteOffline()) {
                settingModel.setSaveFavoriteOffline(false);
            } else {
                settingModel.setSaveFavoriteOffline(true);
            }
            IsSaveFavoriteOfflineSwitch.setChecked(settingModel.isSaveFavoriteOffline());
            updateSettingData(5, settingModel.isSaveFavoriteOffline());

        } else if (id == R.id.IsSherOfTheDaySubscriberSwitch) {//6
            if (settingModel.isSherOfTheDaySubscriber()) {
                settingModel.setSherOfTheDaySubscriber(false);
            } else {
                settingModel.setSherOfTheDaySubscriber(true);
            }
            IsSherOfTheDaySubscriberSwitch.setChecked(settingModel.isSherOfTheDaySubscriber());
            updateSettingData(6, settingModel.isSherOfTheDaySubscriber());

        } else if (id == R.id.IsWordOfTheDaySubscriberSwitch) { // 7
            if (settingModel.isWordOfTheDaySubscriber()) {
                settingModel.setWordOfTheDaySubscriber(false);
            } else {
                settingModel.setWordOfTheDaySubscriber(true);
            }
            IsWordOfTheDaySubscriberSwitch.setChecked(settingModel.isWordOfTheDaySubscriber());
            updateSettingData(7, settingModel.isWordOfTheDaySubscriber());

        } else if (id == R.id.IsGenericNotificationSubscriberSwitch) { //8
            if (settingModel.isGenericNotificationSubscriber()) {
                settingModel.setGenericNotificationSubscriber(false);
            } else {
                settingModel.setGenericNotificationSubscriber(true);
            }
            IsGenericNotificationSubscriberSwitch.setChecked(settingModel.isGenericNotificationSubscriber());
            updateSettingData(8, settingModel.isGenericNotificationSubscriber());

        } else if (id == R.id.IsRequestedNewsLetterSwitch) { //0
            if (settingModel.isRequestedNewsLetter()) {
                settingModel.setRequestedNewsLetter(false);
            } else {
                settingModel.setRequestedNewsLetter(true);
            }
            IsRequestedNewsLetterSwitch.setChecked(settingModel.isRequestedNewsLetter());
            updateSettingData(0, settingModel.isRequestedNewsLetter());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);


        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SettingActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingActivity.this, MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;

        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(SettingActivity.this, Dashboard.class));
        finish();
    }

    @Override
    public void langChanged() {

        setDataAccordingToLang();
    }

    public void getSettingData() {
        final ProgressDialog progressLoader = ProgressDialog.show(this, null, "Please wait...", false, false);

        RestClient.get().getSettingDetails(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, retrofit.client.Response response) {
               if (progressLoader != null && progressLoader.isShowing())                    progressLoader.dismiss();
                try {

                    JSONObject content = new JSONObject(res);

                    if (content.getInt("S") == 1) {
                        JSONObject dataobj = content.getJSONObject("R");
                        settingModel = new SettingModel(dataobj.getBoolean("IsContentSpecificNotificationEn"), dataobj.getBoolean("IsContentSpecificNotificationHi"), dataobj.getBoolean("IsContentSpecificNotificationUr"),
                                dataobj.getBoolean("IsEventNotificationSubscriber"), dataobj.getBoolean("IsSaveFavoriteOffline"), dataobj.getBoolean("IsSherOfTheDaySubscriber")
                                , dataobj.getBoolean("IsWordOfTheDaySubscriber"), dataobj.getBoolean("IsGenericNotificationSubscriber"), dataobj.getBoolean("IsRequestedNewsLetter"));

                        setValueToTheirFields();
                    } else {

                        Toast.makeText(SettingActivity.this, "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null)
                   if (progressLoader != null && progressLoader.isShowing())                    progressLoader.dismiss();
                Toast.makeText(SettingActivity.this, "failed-----------------", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setValueToTheirFields() {
        IsRequestedNewsLetterSwitch.setChecked(settingModel.isRequestedNewsLetter());
        IsWordOfTheDaySubscriberSwitch.setChecked(settingModel.isWordOfTheDaySubscriber());
        IsSaveFavoriteOfflineSwitch.setChecked(settingModel.isSaveFavoriteOffline());
        IsGenericNotificationSubscriberSwitch.setChecked(settingModel.isGenericNotificationSubscriber());
        IsEventNotificationSubscriberSwitch.setChecked(settingModel.isEventNotificationSubscriber());

        IsContentSpecificNotificationEnCheckBox.setChecked(settingModel.isContentSpecificNotificationEn());
        IsContentSpecificNotificationHiCheckBox.setChecked(settingModel.isContentSpecificNotificationHi());
        IsContentSpecificNotificationUrCheckBox.setChecked(settingModel.isContentSpecificNotificationUr());

    }


    public void updateSettingData(int settingNo, boolean updatedVal) {

        RestClient.get().updateSettingDetails(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.rekhtasAccessToken, settingNo, updatedVal, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, retrofit.client.Response response) {

                try {

                    JSONObject content = new JSONObject(res);

                    if (content.getInt("S") == 1) {
                        JSONObject dataobj = content.getJSONObject("R");
                    } else {

                        Toast.makeText(SettingActivity.this, "Oops an error occured,Please try again", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SettingActivity.this, "failed-----------------", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
