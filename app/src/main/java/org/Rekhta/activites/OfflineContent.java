package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class OfflineContent extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks, View.OnClickListener {


    public NavigationFragment drawerFragment;
    TextView nazamContentText;
    ImageView backOffline;
    View view = null;

    Dialog dialog;
    WindowManager.LayoutParams wlp;
    ImageView search, rignMenu, backButton;

    int storedLanguage;
    boolean isOffline;
    ArrayList<FavFCModel> updatedNazamList;
    ArrayList<FavFCModel> favGhazalsList;


    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    private TextView offlineHeaderTitle, titleBig, AutherName;
    private LinearLayout linearWrapper;
    private ViewGroup linearL;
    private TextView[] t;
    private Typeface engtf, hinditf, urdutf;
    private ZoomView zoomView;
    private ExScrollView vScrollWrapper;
    private int position;
    String contentId;

    private LinearLayout nextLinearLayout, prevLinearLayout;
    private List<AddGhazal> addGhazals;
    private List<AddNazam> addNazams;
    ProgressDialog offlineDilaog;
    private String listType;
    private TextView nextGhazalTitle, nextAutherName, prevGhazalTitle, prev_authorName, ghazalTitle, author_name;
    private boolean renderComplete;
    private RelativeLayout outerLinear;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_content);

        //nazamContentText = (TextView) findViewById(R.id.nazamContentText);
        offlineHeaderTitle = (TextView) findViewById(R.id.offlineHeaderTitle);
        ghazalTitle = (TextView) findViewById(R.id.gazal_title);
        author_name = (TextView) findViewById(R.id.author_name);
        ghazalTitle.setSingleLine(true);
        ghazalTitle.setMaxLines(1);
        ghazalTitle.setEllipsize(TextUtils.TruncateAt.END);
        // titleBig = (TextView) findViewById(R.id.titleBig);
        // AutherName = (TextView) findViewById(R.id.offlineautherName);
        backOffline = (ImageView) findViewById(R.id.offlineBack);
        rignMenu = (ImageView) findViewById(R.id.offlinetrippleDot);

        offlineDilaog = new ProgressDialog(OfflineContent.this);
        offlineDilaog.setMessage("Loading...");

        nextLinearLayout = (LinearLayout) findViewById(R.id.nextLinearLayout);
        prevLinearLayout = (LinearLayout) findViewById(R.id.prevLinearLayout);

        rignMenu.setOnClickListener(this);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);

        engtf = Typeface.createFromAsset(this.getAssets(), "fonts/merriweather_light_italic.ttf");
        hinditf = Typeface.createFromAsset(this.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(this.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

        nextGhazalTitle = (TextView) findViewById(R.id.next_gazal_title);
        nextAutherName = (TextView) findViewById(R.id.next_author_name);

        prevGhazalTitle = (TextView) findViewById(R.id.prev_title);
        prev_authorName = (TextView) findViewById(R.id.prev_authorName);

        nextGhazalTitle.setSingleLine(true);
        nextGhazalTitle.setMaxLines(1);
        nextGhazalTitle.setEllipsize(TextUtils.TruncateAt.END);

        prevGhazalTitle.setSingleLine(true);
        prev_authorName.setMaxLines(1);
        nextGhazalTitle.setEllipsize(TextUtils.TruncateAt.END);

        LinearLayout poetDeatailLinear = (LinearLayout) findViewById(R.id.poetDeatailLinear);
        poetDeatailLinear.setVisibility(View.GONE);

        String jsonObj = getIntent().getStringExtra("DataArray");

        offlineHeaderTitle.setText(formatText(jsonObj));
        position = getIntent().getIntExtra("positionClicked", 0);
        String poetName = getIntent().getStringExtra("poetName");
        listType = getIntent().getStringExtra("listType");


        contentId = getIntent().getStringExtra("contentId");

        if (contentId != null && !contentId.equals("")) {

            isOffline = false;
            if (listType.equals("ghazal")) {

                favGhazalsList = (ArrayList<FavFCModel>) getIntent().getSerializableExtra("addGhazalList");
                if (position < favGhazalsList.size() - 1) {
                    nextGhazalTitle.setText(favGhazalsList.get(position + 1).getTE());
                    nextAutherName.setText(favGhazalsList.get(position + 1).getPE());
                } else {
                    nextGhazalTitle.setText("");
                    nextAutherName.setText("");
                }
                if (position > 0) {
                    prevGhazalTitle.setText(favGhazalsList.get(position - 1).getTE());
                    prev_authorName.setText(favGhazalsList.get(position - 1).getPE());
                } else {
                    prevGhazalTitle.setText(favGhazalsList.get(position).getTE());
                    prev_authorName.setText(favGhazalsList.get(position).getPE());
                }


            } else if (listType.equals("nazam")) {
                updatedNazamList = (ArrayList<FavFCModel>) getIntent().getSerializableExtra("addGhazalList");

                if (position < updatedNazamList.size() - 1) {
                    nextGhazalTitle.setText(updatedNazamList.get(position + 1).getTE());
                    nextAutherName.setText(updatedNazamList.get(position + 1).getPE());
                } else {
                    nextGhazalTitle.setText("");
                    nextAutherName.setText("");
                }
                if (position > 0) {
                    prevGhazalTitle.setText(updatedNazamList.get(position - 1).getTE());
                    prev_authorName.setText(updatedNazamList.get(position - 1).getPE());
                } else {
                    prevGhazalTitle.setText(updatedNazamList.get(position).getTE());
                    prev_authorName.setText(updatedNazamList.get(position).getPE());
                }
            }

            offlineDilaog.show();
            getContentRender(contentId);
        } else {
            isOffline = true;
            ghazalTitle.setText(formatText(jsonObj));
            author_name.setText(poetName);
            setOffline();
        }

        initViews();

        nextLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isOffline) {
                    if (listType.equals("ghazal")) {

                        if (position < addGhazals.size() - 1) {
                            position = position + 1;

                            storedLanguage = addGhazals.get(position).getLang_type();
                            if (storedLanguage == 3) {
                                forceRTLIfSupported();
                            } else {
                                forceLTRIfSupported();
                            }
                            setNextGhazalData();
                            setContetnData(addGhazals.get(position).getContent_data());

                        }
                        if (position == 0) {
                            prevLinearLayout.setVisibility(View.GONE);
                            nextLinearLayout.setVisibility(View.VISIBLE);
                        } else if (position == addGhazals.size() - 1) {
                            nextLinearLayout.setVisibility(View.GONE);
                            prevLinearLayout.setVisibility(View.VISIBLE);
                        } else if (addGhazals.size() == 1) {
                            prevLinearLayout.setVisibility(View.GONE);
                            nextLinearLayout.setVisibility(View.GONE);
                        } else {
                            nextLinearLayout.setVisibility(View.VISIBLE);
                            prevLinearLayout.setVisibility(View.VISIBLE);
                        }

                    } else if (listType.equals("nazam")) {

                        if (position < addNazams.size() - 1) {

                            position = position + 1;
                            storedLanguage = addNazams.get(position).getLang_type();

                            if (storedLanguage == 3) {
                                forceRTLIfSupported();
                            } else {
                                forceLTRIfSupported();
                            }
                            setNextNazmData();
                            setContetnData(addNazams.get(position).getDataArray());
                        }

                        if (position == 0) {
                            prevLinearLayout.setVisibility(View.GONE);
                            nextLinearLayout.setVisibility(View.VISIBLE);
                        } else if (position == addNazams.size() - 1) {
                            nextLinearLayout.setVisibility(View.GONE);
                            prevLinearLayout.setVisibility(View.VISIBLE);
                        } else if (addNazams.size() == 1) {
                            prevLinearLayout.setVisibility(View.GONE);
                            nextLinearLayout.setVisibility(View.GONE);
                        } else {
                            nextLinearLayout.setVisibility(View.VISIBLE);
                            prevLinearLayout.setVisibility(View.VISIBLE);
                        }

                    }


                } else if (!isOffline) {


                    if (listType.equals("ghazal")) {

                        if (position < favGhazalsList.size() - 1) {
                            position = position + 1;
                            contentId = favGhazalsList.get(position).getI();
                            offlineDilaog.show();
                            getContentRender(contentId);
                            setNextGhazalData();
                        }


                    } else if (listType.equals("nazam")) {

                        if (position < updatedNazamList.size() - 1) {
                            position = position + 1;
                            contentId = updatedNazamList.get(position).getI();
                            offlineDilaog.show();
                            getContentRender(contentId);
                        }
                    }
                }
            }
        });

        prevLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOffline) {
                    if (position > 0) {
                        if (listType.equals("ghazal")) {

                            position = position - 1;
                            setPrevGhazalData();

                            setContetnData(addGhazals.get(position).getContent_data());

                        } else if (listType.equals("nazam")) {

                            position = position - 1;
                            setPrevNazmData();
                            setContetnData(addNazams.get(position).getDataArray());
                        }
                    } else {
                        if (position == 0) {
                            prevLinearLayout.setVisibility(View.GONE);
                            nextLinearLayout.setVisibility(View.VISIBLE);
                        } else {
                            prevLinearLayout.setVisibility(View.VISIBLE);
                        }
                    }

                } else if (!isOffline) {

                    if (listType.equals("ghazal")) {

                        if (position > 0) {
                            position = position - 1;
                            contentId = favGhazalsList.get(position).getI();
                            offlineDilaog.show();
                            getContentRender(contentId);
                        }


                    } else if (listType.equals("nazam")) {

                        if (position > 0) {
                            position = position - 1;
                            contentId = updatedNazamList.get(position).getI();
                            offlineDilaog.show();
                            getContentRender(contentId);
                        }

                    }
                }

            }
        });

        if (listType.equals("ghazal")) {
            setContetnData(jsonObj);
        } else {
            setContetnData(jsonObj);
        }

        /*if (CommonUtil.languageCode == 3) {
            forceLTRIfSupported();


        } else if (CommonUtil.languageCode == 2)

        {
            forceLTRIfSupported();
            setContetnData(jsonObj);
        } else if (CommonUtil.languageCode == 3)

        {
            forceRTLIfSupported();
            setContetnData(jsonObj);
        }
*/
        checkNextandprev();

        backOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initRightMenuPopUpDialog();
    }

    private void checkNextandprev() {

        if (position == 0) {
            prevLinearLayout.setVisibility(View.GONE);
        } else {
            prevLinearLayout.setVisibility(View.VISIBLE);
        }
        if (listType.equals("ghazal")) {
            if (position == addGhazals.size() - 1) {
                nextLinearLayout.setVisibility(View.GONE);
            } else {
                nextLinearLayout.setVisibility(View.VISIBLE);
            }

        } else {
            if (position == addNazams.size() - 1) {
                nextLinearLayout.setVisibility(View.GONE);
            } else {
                nextLinearLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //   CommonUtil.registerReciver(OfflineContent.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        //  CommonUtil.unregisterReciebr(OfflineContent.this);

    }

    private void setOffline() {


        if (listType.equals("ghazal")) {
            addGhazals = (List<AddGhazal>) getIntent().getSerializableExtra("addGhazalList");
            storedLanguage = addGhazals.get(position).getLang_type();
            if (storedLanguage == 3) {
                //forceRTLIfSupported();
            } else {
                //forceLTRIfSupported();
            }
            setGhazalData(addGhazals);
        } else if (listType.equals("nazam")) {
            addNazams = (List<AddNazam>) getIntent().getSerializableExtra("addGhazalList");
            storedLanguage = addNazams.get(position).getLang_type();
            if (storedLanguage == 3) {
                //   forceRTLIfSupported();
            } else {
                // forceLTRIfSupported();
            }
            setNazamData(addNazams);
        }
    }

    private void getContentRender(String contentId) {

        if (listType.equals("ghazal")) {
            ghazalTitle.setText(favGhazalsList.get(position).getTE());
            author_name.setText(favGhazalsList.get(position).getPE());
            offlineHeaderTitle.setText(favGhazalsList.get(position).getTE());
        } else if (listType.equals("nazam")) {
            offlineHeaderTitle.setText(updatedNazamList.get(position).getTE());
            ghazalTitle.setText(updatedNazamList.get(position).getTE());
            author_name.setText(updatedNazamList.get(position).getPE());
        }

        RestClient.get().getContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                CommonUtil.languageCode, contentId, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {

                        offlineDilaog.dismiss();

                        try {
                            JSONObject obj = new JSONObject(res);
                            if (obj.getInt("S") == 1) {
                                JSONObject obj1 = new JSONObject(res);
                                Log.e("res", " " + res);

                                pumpData(obj1, CommonUtil.languageCode);
                                widhtOfView();
                                renderComplete = true;
                            } else {
                                //Toast.makeText(this, "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {

                        if (offlineDilaog != null)
                            offlineDilaog.dismiss();
                    }

                });
    }

    private void widhtOfView() {

        int childCount = linearWrapper.getChildCount();
        int x = linearWrapper.getWidth();
        for (int j = 0; j < childCount; j++) {
            LinearLayout linearLayout = (LinearLayout) linearWrapper.getChildAt(j);
            int y = linearLayout.getWidth();
            int z = x - y;
            int count = linearLayout.getChildCount();

            int g = z / count;

            for (int k = 0; k < count; k++) {
                View textView = linearLayout.getChildAt(k);
                int extraPadding = g;
                textView.setPadding(0, 0, extraPadding, 0);
            }
        }
    }

    private void pumpData(JSONObject obj1, int languageCode) {

        try {
            JSONObject mainObj = obj1.getJSONObject("R");
            JSONObject mainContent = new JSONObject(mainObj.getString("CR"));
            String ggg = mainObj.getString("CR");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                setContetnData(ggg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setPrevNazmData() {

        if (isOffline) {
            if (position > 0) {
                ghazalTitle.setText(addNazams.get(position).getTitle());
                author_name.setText(addNazams.get(position).getAuther());
                offlineHeaderTitle.setText(addNazams.get(position).getTitle());
                nextGhazalTitle.setText(addNazams.get(position + 1).getTitle());
                nextAutherName.setText(addNazams.get(position + 1).getAuther());

                prevGhazalTitle.setText(addNazams.get(position - 1).getTitle());
                prev_authorName.setText(addNazams.get(position - 1).getAuther());
            } else {
                nextGhazalTitle.setText(addNazams.get(position + 1).getTitle());
                nextAutherName.setText(addNazams.get(position + 1).getAuther());

                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        } else if (!isOffline) {
            if (position > 0) {

                nextGhazalTitle.setText(updatedNazamList.get(position + 1).getTE());
                nextAutherName.setText(updatedNazamList.get(position + 1).getPE());

                prevGhazalTitle.setText(updatedNazamList.get(position - 1).getTE());
                prev_authorName.setText(updatedNazamList.get(position - 1).getPE());
            } else {
                nextGhazalTitle.setText(updatedNazamList.get(position + 1).getTE());
                nextAutherName.setText(updatedNazamList.get(position + 1).getPE());

                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        }
    }

    private void setPrevGhazalData() {

        if (isOffline) {
            if (position > 0) {
                ghazalTitle.setText(addGhazals.get(position).getTitle());
                author_name.setText(addGhazals.get(position).getAuther());
                offlineHeaderTitle.setText(addGhazals.get(position).getTitle());
                nextGhazalTitle.setText(addGhazals.get(position + 1).getTitle());
                nextAutherName.setText(addGhazals.get(position + 1).getAuther());

                prevGhazalTitle.setText(addGhazals.get(position - 1).getTitle());
                prev_authorName.setText(addGhazals.get(position - 1).getAuther());

                nextLinearLayout.setVisibility(View.VISIBLE);
            } else {
                nextLinearLayout.setVisibility(View.VISIBLE);
                prevLinearLayout.setVisibility(View.GONE);
                nextGhazalTitle.setText(addGhazals.get(position + 1).getTitle());
                nextAutherName.setText(addGhazals.get(position + 1).getAuther());

                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        } else if (!isOffline) {

            if (position > 0) {

                nextGhazalTitle.setText(favGhazalsList.get(position + 1).getTE());
                nextAutherName.setText(favGhazalsList.get(position + 1).getPE());

                prevGhazalTitle.setText(favGhazalsList.get(position - 1).getTE());
                prev_authorName.setText(favGhazalsList.get(position - 1).getPE());
            } else {

                nextLinearLayout.setVisibility(View.VISIBLE);
                prevLinearLayout.setVisibility(View.VISIBLE);
                nextGhazalTitle.setText(favGhazalsList.get(position + 1).getTE());
                nextAutherName.setText(favGhazalsList.get(position + 1).getPE());

                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        }
    }

    private void setNextNazmData() {

        if (isOffline) {
            if (position < addNazams.size() - 1) {
                ghazalTitle.setText(addNazams.get(position).getTitle());
                author_name.setText(addNazams.get(position).getAuther());
                offlineHeaderTitle.setText(addNazams.get(position).getTitle());
                nextGhazalTitle.setText(addNazams.get(position + 1).getTitle());
                nextAutherName.setText(addNazams.get(position + 1).getAuther());

            } else if (position == addNazams.size() - 1) {
                nextGhazalTitle.setText("");
                nextAutherName.setText("");
            }

            if (position > 0) {
                prevGhazalTitle.setText(addNazams.get(position - 1).getTitle());
                prev_authorName.setText(addNazams.get(position - 1).getAuther());
            } else if (position == 0) {
                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        } else if (!isOffline) {
            if (position < updatedNazamList.size() - 1) {

                nextGhazalTitle.setText(updatedNazamList.get(position + 1).getTE());
                nextAutherName.setText(updatedNazamList.get(position + 1).getPE());

            } else if (position == updatedNazamList.size() - 1) {
                nextGhazalTitle.setText("");
                nextAutherName.setText("");
            }

            if (position > 0) {
                prevGhazalTitle.setText(updatedNazamList.get(position - 1).getTE());
                prev_authorName.setText(updatedNazamList.get(position - 1).getPE());
            } else if (position == 0) {
                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        }

    }

    private void setNextGhazalData() {

        if (isOffline) {
            if (position < addGhazals.size() - 1) {
                ghazalTitle.setText(addGhazals.get(position).getTitle());
                author_name.setText(addGhazals.get(position).getAuther());
                offlineHeaderTitle.setText(addGhazals.get(position).getTitle());
                nextGhazalTitle.setText(addGhazals.get(position + 1).getTitle());
                nextAutherName.setText(addGhazals.get(position + 1).getAuther());

            } else if (position == addGhazals.size() - 1) {
                nextGhazalTitle.setText("");
                nextAutherName.setText("");
            }

            if (position > 0) {
                prevGhazalTitle.setText(addGhazals.get(position - 1).getTitle());
                prev_authorName.setText(addGhazals.get(position - 1).getAuther());
            } else if (position == 0) {
                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        } else if (!isOffline) {


            if (position < favGhazalsList.size() - 1) {

                nextGhazalTitle.setText(favGhazalsList.get(position + 1).getTE());
                nextAutherName.setText(favGhazalsList.get(position + 1).getPE());

            } else if (position == favGhazalsList.size() - 1) {
                nextGhazalTitle.setText("");
                nextAutherName.setText("");
            }

            if (position > 0) {
                prevGhazalTitle.setText(favGhazalsList.get(position - 1).getTE());
                prev_authorName.setText(favGhazalsList.get(position - 1).getPE());
            } else if (position == 0) {
                prevGhazalTitle.setText("");
                prev_authorName.setText("");
            }
        }

    }

    private void setNazamData(List<AddNazam> addNazams) {

        if (position < addNazams.size() - 1) {
            nextGhazalTitle.setText(addNazams.get(position + 1).getTitle());
            nextAutherName.setText(addNazams.get(position + 1).getAuther());
        } else if (position == addNazams.size() - 1) {
            nextGhazalTitle.setText("");
            nextAutherName.setText("");
        }

        if (position > 0) {
            prevGhazalTitle.setText(addNazams.get(position - 1).getTitle());
            prev_authorName.setText(addNazams.get(position - 1).getAuther());
        } else if (position == 0) {
            prevGhazalTitle.setText("");
            prevGhazalTitle.setText("");
        }

    }

    private void setGhazalData(List<AddGhazal> addGhazals) {


        if (position < addGhazals.size() - 1) {

            nextGhazalTitle.setText(addGhazals.get(position + 1).getTitle());
            nextAutherName.setText(addGhazals.get(position + 1).getAuther());
        } else if (position == addGhazals.size() - 1) {
            nextGhazalTitle.setText("");
            nextAutherName.setText("");
        }

        if (position > 0) {
            prevGhazalTitle.setText(addGhazals.get(position - 1).getTitle());
            prev_authorName.setText(addGhazals.get(position - 1).getAuther());
        } else if (position == 0) {
            prevGhazalTitle.setText("");
            prevGhazalTitle.setText("");
        }
    }

    private void initViews() {

        zoomView = (ZoomView) findViewById(R.id.offlineZoomlayout);
        linearL = zoomView.Wrapper;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setContetnData(String data) {

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
        linearL.removeAllViews();
        vScrollWrapper = new ExScrollView(OfflineContent.this);
        vScrollWrapper.setLayoutParams(new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.WRAP_CONTENT));
        // vScrollWrapper.setBackgroundColor(Color.parseColor("#FFE800"));
        vScrollWrapper.setScrolling(false);

        linearWrapper = new LinearLayout(OfflineContent.this);
        // linearWrapper.setBackgroundColor(Color.parseColor("#BFE8BB"));
        linearWrapper.setOrientation(LinearLayout.VERTICAL);
        linearWrapper.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        linearWrapper.setClipChildren(false);

        linearWrapper.setGravity(Gravity.START);

        int minGap = 4;

        JSONObject mainContent = null;
        try {
            mainContent = new JSONObject(data);
            JSONArray Parray = mainContent.getJSONArray("P");

            String line = "";

            for (int i = 0; i < Parray.length(); i++) {
               /* if (i > 1) {
                    linearWrapper.setPadding(0, 0, 0, 20);
                }*/

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {


                    LinearLayout linear = new LinearLayout(OfflineContent.this);
                    linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linear.setOrientation(LinearLayout.HORIZONTAL);

                    // linear.setPadding(0, 13, 0, 13);

                    if (j == 0) {
                        linear.setPadding(0, 12, 0, 0);
                    } else {
                        linear.setPadding(0, 6, 0, 16);
                    }

                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    t = new TextView[Warray.length()];

                    if (CommonUtil.languageCode != 3) {
                        for (int k = 0; k < Warray.length(); k++) {

                            t[k] = new TextView(OfflineContent.this);
                            t[k].setLayoutParams(dim);
                            CommonUtil.setEnglishMerriwetherFont(OfflineContent.this, t[k]);

                            t[k].setSingleLine(true);
//
                            if (k < Warray.length() - 1) {
                                t[k].setPadding(0, 0, minGap, 0);
                            }
                            t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                            t[k].setTextColor(Color.parseColor("#000000"));
                            t[k].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                            if (CommonUtil.languageCode == 1) {
                                t[k].setTypeface(engtf);

                            } else {
                                t[k].setTypeface(hinditf);

                            }
                            t[k].setTypeface(t[k].getTypeface(), Typeface.ITALIC);
                            t[k].setGravity(Gravity.CENTER);
                            ///assignClickToText(t[k], Warray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                            linear.addView(t[k]);
                        }
                    } else {
                        forceRTLIfSupported();
                        for (int k = 0; k < Warray.length(); k++) {
                            t[k] = new TextView(OfflineContent.this);
                            t[k].setLayoutParams(dim);
                            t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                            /*if (k < Warray.length() - 1) {
                                t[k].setPadding(0, 0, minGap, 0);
                            }*/

                            CommonUtil.setEnglishMeriWeatherItalic(OfflineContent.this, t[k]);

                            t[k].setTextColor(Color.parseColor("#000000"));
                            t[k].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                            t[k].setTypeface(urdutf);

                            t[k].setGravity(Gravity.CENTER);
                            assignClickToText(t[k], Warray.getJSONObject(k), j, Parray.getJSONObject(i).getJSONArray("L"), j);
                            linear.addView(t[k]);
                        }
                    }

                    linearWrapper.addView(linear);

                    linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            }

            vScrollWrapper.addView(linearWrapper);
            zoomView.addView(vScrollWrapper);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void assignClickToText(TextView textView, JSONObject jsonObject, int j, JSONArray l, int j1) {


    }

    private void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        /*    Intent intent = new Intent(this, PoetDetailedActivity.class);
            intent.putExtra("poetId","EB9CB3D1-1DBD-4582-A1CA-E48FDAFE5BC8");
            intent.putExtra("ghazalCount",4+"");
            intent.putExtra("nazmCount",5+"");
            intent.putExtra("sherCount",7+"");

            this.startActivity(intent);*/
        }

        return super.onOptionsItemSelected(item);
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }

        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        //dialog.show();
    }

    private void resetActionBarPopUpViewLangButtonColor(int langCode) {

        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    private void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();

    }

    public String formatText(String data) {
        String str = "";
        try {
            JSONObject obj = new JSONObject(data);
            JSONArray Parray = obj.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {
                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {
                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    if (CommonUtil.languageCode != 3) {

                        for (int k = 0; k < Warray.length(); k++) {
                            str = str + " " + Warray.getJSONObject(k).getString("W");
                        }

                    } else {
                        for (int k = Warray.length() - 1; k >= 0; k--) {
                            str = str + " " + Warray.getJSONObject(k).getString("W");
                        }
                    }
                    str = str + "\n";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.home_screen, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.offlinetrippleDot:
                //showRightMenuLangSelectionOption();
                break;
        }

    }

    @Override
    public void goToHomeScreen() {


    }

    @Override
    public void langChanged() {

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            // restartActivity();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //restartActivity();
        }
    }


    protected void setNazmContent(String data) {

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearL.removeAllViews();
        vScrollWrapper = new ExScrollView(OfflineContent.this);
        vScrollWrapper.setFillViewport(true);
        //    vScrollWrapper.setScrollBarStyle(HorizontalScrollView.SCROLL_AXIS_NONE);
        vScrollWrapper.setLayoutParams(new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.WRAP_CONTENT));


        // vScrollWrapper.setBackgroundColor(Color.RED);
        linearWrapper = new LinearLayout(OfflineContent.this);
        outerLinear = new RelativeLayout(OfflineContent.this);


        outerLinear.setPadding(8, 0, 8, 0);
        // outerLinear.setBackgroundColor(Color.YELLOW);
        linearWrapper.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams55 = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        linearWrapper.setLayoutParams(layoutParams55);
        linearWrapper.setClipChildren(false);
        linearWrapper.setHorizontalGravity(Gravity.CENTER);
        //  linearWrapper.setGravity(Gravity.FILL_HORIZONTAL);
        // linearWrapper.setBackgroundColor(Color.BLUE);

        outerLinear.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));

        RelativeLayout.LayoutParams RelativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

       /* if (alignmenht == 1.0) {
            RelativeLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            linearWrapper.setLayoutParams(RelativeLayoutParams);
            outerLinear.addView(linearWrapper);
        } else if (alignmenht == 0.0) {
            RelativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
            linearWrapper.setLayoutParams(RelativeLayoutParams);
            outerLinear.addView(linearWrapper);
        }*/


        int minGap = 3;
        JSONObject mainContent = null;
        try {
            mainContent = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONArray Parray = null;
        try {
            Parray = mainContent.getJSONArray("P");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        renderNazamNew(Parray, dim);

        vScrollWrapper.addView(outerLinear);
        zoomView.addView(vScrollWrapper);
    }

    private void renderNazamNew(JSONArray parray, LinearLayout.LayoutParams dim) {


        for (int i = 0; i < parray.length(); i++) {
            if (i > 1) {
                //linearWrapper.setPadding(0, 0, 0, 12);
            }

            JSONArray Larray = null;
            try {
                Larray = parray.getJSONObject(i).getJSONArray("L");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < Larray.length(); j++) {

                LinearLayout linear = new LinearLayout(OfflineContent.this);
                LinearLayout.LayoutParams layoutParamsh = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                linear.setOrientation(LinearLayout.HORIZONTAL);

                JSONArray Warray = null;
                try {
                    Warray = Larray.getJSONObject(j).getJSONArray("W");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                t = new TextView[Warray.length()];


                forceRTLIfSupported();

                for (int k = 0; k < Warray.length(); k++) {
                    t[k] = new TextView(OfflineContent.this);
                    t[k].setLayoutParams(dim);
                    t[k].setIncludeFontPadding(false);
                    t[k].setTextSize(20);
                    CommonUtil.setUrduNotoNataliq(OfflineContent.this, t[k]);
                    try {
                        t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (k < Warray.length() - 1) {
                        // t[k].setPadding(0, 0, minGap, 0);
                    }

                    t[k].setTextColor(Color.parseColor("#000000"));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        t[k].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    }

                    t[k].setTypeface(urdutf);
                    try {
                        assignClickToText(t[k], Warray.getJSONObject(k), j, parray.getJSONObject(i).getJSONArray("L"), j);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    linear.addView(t[k]);
                }

                if (j == Larray.length() - 1) {
                    linear.setPadding(0, 0, 0, 25);
                } else {
                    linear.setPadding(0, 4, 0, 0);
                }

                linearWrapper.addView(linear);

                linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                int widthSpec = View.MeasureSpec.makeMeasureSpec(linearL.getWidth(), View.MeasureSpec.EXACTLY);
                int heightSpec = View.MeasureSpec.makeMeasureSpec(linearL.getHeight(), View.MeasureSpec.EXACTLY);
                linear.measure(widthSpec, heightSpec);

                // linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                //linearWrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }

    }


}
