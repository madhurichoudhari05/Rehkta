package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;


import org.Rekhta.R;;
import org.Rekhta.adapters.CollectionAdapter;
import org.Rekhta.adapters.SherAdapter;
import org.Rekhta.adapters.TagRecyclerViewAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.fragments.sherFragments.SherGhalContentFragment;
import org.Rekhta.interfaces.DrawerLocker;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

public class SherActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks, DrawerLocker {

    public NavigationFragment drawerFragment;
    public CollectionAdapter collectionAdapter;
    public TagRecyclerViewAdapter tagRecyclerViewAdapter;
    public String currentSherID = "";
    public ArrayList<String> idsRendered = new ArrayList<String>();
    public ArrayList<String> lastRenderSherName = new ArrayList<String>();
    public String idGhazalToRender;
    SherAdapter sherAdapter;
    ViewPager sherPager;
    TabLayout sherTablayout;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    SharedPreferences DataPrefs = null;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    DrawerLayout drawerLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sher);
        sherPager = (ViewPager) findViewById(R.id.sher_viewpager);
        sherTablayout = (TabLayout) findViewById(R.id.sher_tablayout);
        sherAdapter = new SherAdapter(getSupportFragmentManager());
        sherPager.setAdapter(sherAdapter);
        sherTablayout.setupWithViewPager(sherPager);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(SherActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(SherActivity.this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SherActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SherActivity.this, MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                drawerFragment.resetButtonColor(1);
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                drawerFragment.resetButtonColor(2);
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                drawerFragment.resetButtonColor(3);
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(SherActivity.this, Dashboard.class));
        finish();
    }

    @Override
    public void langChanged() {

        if (collectionAdapter != null)
            collectionAdapter.notifyDataSetChanged();
        if (tagRecyclerViewAdapter != null)
            tagRecyclerViewAdapter.notifyDataSetChanged();

        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }

        changeTabNameAccToLang();
        sendLocalbroadCast();

    }

    private void changeTabNameAccToLang() {

        sherTablayout.getTabAt(0).setText(LanguageSelection.getSpecificLanguageWordById("Collections", CommonUtil.languageCode));
        sherTablayout.getTabAt(1).setText(LanguageSelection.getSpecificLanguageWordById("Tags", CommonUtil.languageCode));

    }

    @Override
    public void onBackPressed() {

        if (this.lastRenderSherName.size() != 0) {
            CommonUtil.cSherTypeNameEnglish = this.lastRenderSherName.get(this.lastRenderSherName.size() - 1);
            this.lastRenderSherName.remove(lastRenderSherName.size() - 1);

        }

        if (this.idsRendered.size() != 0) {
            this.currentSherID = this.idsRendered.get(this.idsRendered.size() - 1);
            this.idsRendered.remove(idsRendered.size() - 1);
        }
        sendLocalbroadCast();

        super.onBackPressed();
    }

    public void removeFragment() {
        getFragmentManager().popBackStack();
        startFragment();
    }

    public void startFragment() {
        SherGhalContentFragment sherGhalContentFragment = new SherGhalContentFragment();
//                    ghazalDetails.setArguments(bundle);
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.drawer_layout, sherGhalContentFragment, "sherGhalContentFragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawerLayout.setDrawerLockMode(lockMode);
    }

    private void sendLocalbroadCast() {

        Intent intent = new Intent("TruobleSher");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        changeTabNameAccToLang();
        if (collectionAdapter != null)
            collectionAdapter.notifyDataSetChanged();
        if (tagRecyclerViewAdapter != null)
            tagRecyclerViewAdapter.notifyDataSetChanged();


    }
}
