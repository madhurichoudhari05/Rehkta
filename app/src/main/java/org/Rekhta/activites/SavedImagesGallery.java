package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.Rekhta.R;;
import org.Rekhta.adapters.SavedImagePagerAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.utils.CommonUtil;

public class SavedImagesGallery extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {


    ViewPager savedImagesPager;
    SavedImagePagerAdapter savedImagePagerAdapter;
    ArrayList<Bitmap> bitmapArrayList;
    int pos;
    ImageView backImage;


    File[] mediaFiles;
    ImageView prev, next;
    TextView backButton;
    private ImageView search, rignMenu;

    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_images_gallery);

        savedImagesPager = (ViewPager) findViewById(R.id.savedImagesPager);
        next = (ImageView) findViewById(R.id.right_nav);
        prev = (ImageView) findViewById(R.id.left_nav);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);

        backButton = (TextView) findViewById(R.id.backButton);
        search = (ImageView) findViewById(R.id.search);
        rignMenu = (ImageView) findViewById(R.id.trippleDot);
        initRightMenuPopUpDialog();
        backButton.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(SavedImagesGallery.this, R.drawable.ic_back_pre_lollipop), null, null, null);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SavedImagesGallery.this, SearchActivity.class));
            }
        });

        rignMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRightMenuLangSelectionOption();
            }
        });


        try {
            //  mediaFiles = (File[]) getIntent().getExtras().get("imagesList");
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        Bundle b = getIntent().getExtras();

        mediaFiles = CommonUtil.myFiles;
        pos = getIntent().getIntExtra("pos", 0);

        //mediaFiles = (File[]) getIntent().getSerializableExtra("imagesList");

        bitmapArrayList = new ArrayList<>();
        if (mediaFiles != null) {
            for (File file : mediaFiles) {
                bitmapArrayList.add(convertToBitmap(file));
                //Log.d(TAG + "bmpArray Size", ""+bmpArray.size());
                //Log.d(TAG, "call to convertToBitmap");
            }
        }


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int tab = savedImagesPager.getCurrentItem();
                if (tab < bitmapArrayList.size()) {
                    tab++;
                    savedImagesPager.setCurrentItem(tab);
                }
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int tab = savedImagesPager.getCurrentItem();
                if (tab > 0) {

                    tab--;
                    savedImagesPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    savedImagesPager.setCurrentItem(tab);

                }

            }
        });

        savedImagePagerAdapter = new SavedImagePagerAdapter(SavedImagesGallery.this, bitmapArrayList);
        savedImagesPager.setAdapter(savedImagePagerAdapter);
        savedImagesPager.setCurrentItem(pos);
    }

    private void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(SavedImagesGallery.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(SavedImagesGallery.this);

    }


    private Bitmap convertToBitmap(File file) {

        URL url = null;
        try {
            url = file.toURL();
        } catch (MalformedURLException e1) {
            //Log.d(TAG, e1.toString());
        }//catch

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openStream());
            //bmp.recycle();
        } catch (Exception e) {
            //Log.d(TAG, "Exception: "+e.toString());
        }//catch
        return bmp;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

       // CommonUtil.myFiles = null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void langChanged() {

        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();

        } else {
            forceLTRIfSupported();
        }
    }
}
