package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.Rekhta.R;
import org.Rekhta.adapters.ImageShayari.FullScreenImageAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.fragments.imageShayari.ZoomedImageFragment;
import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.Rekhta.utils.CommonUtil;

import java.util.ArrayList;
import java.util.List;

public class ZoomedImageActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {


    View view;
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    ArrayList<String> filePaths = new ArrayList<String>();
    TextView backImg;

    ImageView leftArrowImg, rightArrowImg;
    public List<ShayariDetailDto> data = new ArrayList<>();

    private String imageId = "";

    Context context;

    ImageView backButton;
    private ImageView search, rignMenu;

    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;

    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoomed_image);

        viewPager = (ViewPager) view.findViewById(R.id.zoomedPager);


        DataPrefs = this.getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);

        backButton = (ImageView) view.findViewById(R.id.backButton);
        CommonUtil.setBackButton(context, backButton);

        search = (ImageView) view.findViewById(R.id.search);
        rignMenu = (ImageView) view.findViewById(R.id.trippleDot);
        initRightMenuPopUpDialog();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        position = getIntent().getIntExtra("position", 0);
        imageId = getIntent().getStringExtra("key");
        imageId = getIntent().getStringExtra("imageId");


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ZoomedImageActivity.this, SearchActivity.class));
            }
        });

        rignMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRightMenuLangSelectionOption();
            }
        });


       /* if (getArguments() != null) {
            imageId = getArguments().getString("key");
        }*/

        intFiles();

        //adapter = new FullScreenImageAdapter(this, filePaths, ZoomedImageFragment.this);
        //viewPager.setAdapter(adapter);

        // close button click event
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // viewPager.setCurrentItem(((ImageShyariActivity)getActivity()).currentImgClicked);
        viewPager.setCurrentItem(position);
//https://www.androidhive.info/2013/09/android-fullscreen-image-slider-with-swipe-and-pinch-zoom-gestures/

        leftArrowImg = (ImageView) view.findViewById(R.id.left_nav);
        rightArrowImg = (ImageView) view.findViewById(R.id.right_nav);
        leftArrowImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {

                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);

                }
            }
        });

        // Images right navigatin
        rightArrowImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
            }
        });
    }

    public void intFiles() {
      /*  try {
            data = ((ImageShyariActivity) getActivity()).getShayari;
            for (int i = 0; i < data.size(); i++) {

                if (*//*((ImageShyariActivity)getActivity()).currentImgClickedS*//*imageId.trim().equalsIgnoreCase(data.get(i).getImageId().trim())) {
                    position = i;
                    filePaths.add(data.get(i).getImageId());
                } else {
                    filePaths.add(data.get(i).getImageId());
                    //  Toast.makeText(context, "not Matched", Toast.LENGTH_SHORT).show();
                }
                //  filePaths.add(data.getJSONObject(i).getString("I"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    private void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(ZoomedImageActivity.this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);

                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);

                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);

                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        langChanged();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setRTLToolbar();
        }
    }

    private void setRTLToolbar() {

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) backButton.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        backButton.setLayoutParams(params);
        CommonUtil.setBackButton(ZoomedImageActivity.this, backButton);

        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) rignMenu.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        rignMenu.setLayoutParams(params1);

        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) search.getLayoutParams();
        params.addRule(RelativeLayout.RIGHT_OF, R.id.trippleDot);

        search.setLayoutParams(params2);

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setLTRToolbar();
        }
    }

    private void setLTRToolbar() {

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) backButton.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        backButton.setLayoutParams(params);


        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams) rignMenu.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rignMenu.setLayoutParams(params1);

        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) search.getLayoutParams();
        params.addRule(RelativeLayout.LEFT_OF, R.id.trippleDot);

        search.setLayoutParams(params2);

    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void langChanged() {

        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }

        //adapter = new FullScreenImageAdapter(ZoomedImageActivity.this, filePaths, ZoomedImageFragment.this);
        //viewPager.setAdapter(adapter);
    }
}
