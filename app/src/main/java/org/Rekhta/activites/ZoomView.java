package org.Rekhta.activites;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Scroller;

import org.Rekhta.R;;


/**
 * Zooming view.
 */
public class ZoomView extends FrameLayout {


    public static final String TWO_DSCROLL_VIEW_CAN_HOST_ONLY_ONE_DIRECT_CHILD = "TwoDScrollView can host only one direct child";
    /**
     * New Var.
     */

    static final int ANIMATED_SCROLL_GAP = 250;
    public LinearLayout Wrapper;
    int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
    int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    private boolean flagResetHeight = false;
    private Rect mClipBounds;
    private int wrapperHeight = 0;
    private int wrapperWidth = 0;
    private int innerHeight = 0;
    private long mLastScroll;
    private Scroller mScroller;

    /**
     * Position of the last motion event.
     */
    private float mLastMotionY;
    private float mPrvLastMotionY = 0;
    private float mPrvLastMotionX = 0;
    private float mLastMotionX;
    private int mScrollY;
    /**
     * True if the user is currently dragging this TwoDScrollView around. This is
     * not the same as 'is being flinged', which can be checked by
     * mScroller.isFinished() (flinging begins when the user lifts his finger).
     */
    private boolean mIsBeingDragged = false;

    /**
     * Determines speed during touch scrolling
     */
    private VelocityTracker mVelocityTracker;

    /**
     * Whether arrow scrolling is animated.
     */
    private int mTouchSlop;
    private int mMinimumVelocity;

    private float mScale = 1f;
    private float mPrvSpanY = 0f;
    private ScaleGestureDetector mScaleDetector;
    private GestureDetector gestureDetector;
    private boolean mEnableScaling = true;     // scaling is buggy when you click on child views
    private boolean mScaleBegin = false;
    private boolean mMoveFreeze = false;
    private ExScrollView HScrollView;

    public ZoomView(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub

        LayoutInflater.from(context).inflate(R.layout.compound_view, this);
        Wrapper = (LinearLayout) findViewById(R.id.mainContent);
        //Get a reference to the layout where you want children to be placed
        // Sets up interactions
        initTwoDScrollView();
//


    }

    public ZoomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

//        Wrapper=new LinearLayout(context);
//        addView(Wrapper);
        //Wrapper=(LinearLayout)getChildAt(0);


        // TODO Auto-generated constructor stub
    }

    public ZoomView(final Context context) {
        this(context, null);
    }

    private void initTwoDScrollView() {
        mScroller = new Scroller(getContext());
        setFocusable(true);
        setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
        setWillNotDraw(false);
        final ViewConfiguration configuration = ViewConfiguration.get(getContext());
        mTouchSlop = configuration.getScaledTouchSlop();
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();

        if (mEnableScaling) {
            gestureDetector = new GestureDetector(getContext(), new ZoomView.GestureListener());
        }
    }

    public void setWrapperHeight(int height, int innerHeight) {
        wrapperHeight = height;
        this.innerHeight = innerHeight;
        flagResetHeight = true;

    }

    public void setWrapperChildWidth(int width) {
        wrapperWidth = width;


    }

//    @Override
//    public void addView(View child) {
//        if(Wrapper == null){
//        if (getChildCount() > 0) {
//            throw new IllegalStateException(TWO_DSCROLL_VIEW_CAN_HOST_ONLY_ONE_DIRECT_CHILD);
//        }
//
//        super.addView(child);
//
//        if (mEnableScaling) {
//            createScaleGestureDetector(child);
//        }
//    } else {
//        //Forward these calls to the content view
//
//        Wrapper.addView(child);
//    }
//    }
//
//    @Override
//    public void addView(View child, int index) {
//        if(Wrapper == null){
//        if (getChildCount() > 0) {
//            throw new IllegalStateException(TWO_DSCROLL_VIEW_CAN_HOST_ONLY_ONE_DIRECT_CHILD);
//        }
//
//        super.addView(child, index);
//
//        if (mEnableScaling) {
//            createScaleGestureDetector(child);
//        }
//    } else {
//        //Forward these calls to the content view
//
//        Wrapper.addView(child, index);
//    }
//    }
//
//    @Override
//    public void addView(View child, ViewGroup.LayoutParams params) {
//        if(Wrapper == null){
//        if (getChildCount() > 0) {
//            throw new IllegalStateException(TWO_DSCROLL_VIEW_CAN_HOST_ONLY_ONE_DIRECT_CHILD);
//        }
//
//        super.addView(child, params);
//
//        if (mEnableScaling) {
//            createScaleGestureDetector(child);
//        }
//    } else {
//        //Forward these calls to the content view
//
//        Wrapper.addView(child, params);
//    }
//    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if (Wrapper == null) {
            super.addView(child, index, params);

        } else {
            //Forward these calls to the content view
            if (mEnableScaling) {
                createScaleGestureDetector(child);
            }
            Wrapper.addView(child, index, params);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int mode = MeasureSpec.getMode(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);

        if (flagResetHeight) {


            if (wrapperHeight <= innerHeight) {

                ViewGroup.LayoutParams params = Wrapper.getChildAt(0).getLayoutParams();
                params.height = innerHeight;
                Wrapper.getChildAt(0).setLayoutParams(params);
            }

            ViewGroup.LayoutParams params1 = Wrapper.getLayoutParams();
            params1.height = wrapperHeight;
            Wrapper.setLayoutParams(params1);
            flagResetHeight = false;

        }
//        Log.e("Zoom     height----->", parentWidth + "--" +widthMeasureSpec + "--<<<<<<<<<<<<<<<"+mode );
    }
//    @Override
//    protected void measureChild(View child, int parentWidthMeasureSpec, int parentHeightMeasureSpec) {
//        ViewGroup.LayoutParams lp = child.getLayoutParams();
//
//        int childWidthMeasureSpec;
//        int childHeightMeasureSpec;
//
//        childWidthMeasureSpec = getChildMeasureSpec(parentWidthMeasureSpec, getPaddingLeft() + getPaddingRight(), lp.width);
//        childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
//
//        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
//    }

    //    @Override
//    protected void measureChildWithMargins(View child, int parentWidthMeasureSpec, int widthUsed, int parentHeightMeasureSpec, int heightUsed) {
//        final MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
//
//        final int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(lp.leftMargin + lp.rightMargin, MeasureSpec.UNSPECIFIED);
//        final int childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(lp.topMargin + lp.bottomMargin, MeasureSpec.UNSPECIFIED);
//
//        child.measure(childWidthMeasureSpec, childHeightMeasureSpec);
//    }
    private void createScaleGestureDetector(final View childLayout) {
        HScrollView = (ExScrollView) childLayout;
        mScaleDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {

                float scale = 1 - detector.getScaleFactor();
                float dify = detector.getPreviousSpanY() - detector.getCurrentSpanY();
                mPrvSpanY += dify;
                float prevScale = mScale;
                mScale += scale;

                if (mScale < 0.4f) // Minimum scale condition:
                    mScale = 0.4f;

                if (mScale > 1f) // Maximum scale condition:
                    mScale = 1f;
//                Log.e("Zoo----->", "m----->POint dify "+ (int)(wrapperHeight / (mScale)) +" mPrvSpanY " +mPrvSpanY+" focY"+detector.getFocusY()  );
                ScaleAnimation scaleAnimation = new ScaleAnimation(1f / mScale, 1f / mScale,
                        1f / mScale, 1f / mScale);
                scaleAnimation.setDuration(0);
                scaleAnimation.setFillAfter(true);
                childLayout.startAnimation(scaleAnimation);


                ((ExScrollView) Wrapper.getChildAt(0)).setChildWidth((int) (screenWidth / (mScale)));

                ((ExScrollView) Wrapper.getChildAt(0)).getChildAt(0).setPadding(0, 0, ((int) ((((screenWidth / (mScale))) - (screenWidth + ((wrapperWidth - screenWidth) / mScale))) * mScale)), 0);
                ViewGroup.LayoutParams params1 = Wrapper.getLayoutParams();

//                scrollTo(0,(int)detector.getCurrentSpanY());
//                Log.e("Zoo----->", "m----->"+dify + "--line " + (int)(wrapperHeight / mScale) +" focY"+detector.getFocusY() +" span dif " +detector.getPreviousSpan()+" Curspan "+detector.getCurrentSpan());
//                params1.height = (wrapperHeight+ (int)(wrapperHeight / (mScale)))/2;
                params1.height = (int) (wrapperHeight / (mScale));
                Wrapper.setLayoutParams(params1);
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {

//                Log.e("cScaleGeDeor--> Begin", "m----->createScaleGestureDetector");
                mScaleBegin = true;
                mMoveFreeze = true;
//                mScrollY=getScrollY();

                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
//                super.onScaleEnd(detector);
                mScaleBegin = false;
            }
        });
    }


    @Override
    protected float getTopFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }

        final int length = getVerticalFadingEdgeLength();
        if (getScrollY() < length) {
            return getScrollY() / (float) length;
        }

        return 1.0f;
    }

    @Override
    protected float getBottomFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }

        final int length = getVerticalFadingEdgeLength();
        final int bottomEdge = screenHeight - getPaddingBottom();
        final int span = getChildAt(0).getBottom() - getScrollY() - bottomEdge;
        if (span < length) {
            return span / (float) length;
        }

        return 1.0f;
    }

    @Override
    protected float getLeftFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }

        final int length = getHorizontalFadingEdgeLength();
        if (getScrollX() < length) {
            return getScrollX() / (float) length;
        }

        return 1.0f;
    }

    @Override
    protected float getRightFadingEdgeStrength() {
        if (getChildCount() == 0) {
            return 0.0f;
        }

        final int length = getHorizontalFadingEdgeLength();
        final int rightEdge = screenWidth - getPaddingRight();
        final int span = getChildAt(0).getRight() - getScrollX() - rightEdge;
        if (span < length) {
            return span / (float) length;
        }

        return 1.0f;
    }


    /**
     * @return Returns true this TwoDScrollView can be scrolled
     */
    private boolean canScroll() {
        View child = getChildAt(0);
        if (child != null) {
            int childHeight = child.getHeight();
            int childWidth = wrapperWidth;


            return (screenHeight < childHeight + getPaddingTop() + getPaddingBottom()) ||
                    (screenWidth < childWidth + getPaddingLeft() + getPaddingRight());
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        /*
         * This method JUST determines whether we want to intercept the motion.
         * If we return true, onMotionEvent will be called and we do the actual
         * scrolling there.
         */

        /*
        * Shortcut the most recurring case: the user is in the dragging
        * state and he is moving his finger.  We want to intercept this
        * motion.
        */
        final int action = ev.getAction();
        if ((action == MotionEvent.ACTION_MOVE) && (mIsBeingDragged)) {
            return true;
        }

        if (!canScroll()) {
            mIsBeingDragged = false;
            return false;
        }

        final float y = ev.getY();
        final float x = ev.getX();

        switch (action) {
            case MotionEvent.ACTION_MOVE:
                /*
                 * mIsBeingDragged == false, otherwise the shortcut would have caught it. Check
                 * whether the user has moved far enough from his original down touch.
                 */

                /*
                * Locally do absolute value. mLastMotionY is set to the y value
                * of the down event.
                */
                final int yDiff = (int) Math.abs(y - mLastMotionY);
                final int xDiff = (int) Math.abs(x - mLastMotionX);
                if (yDiff > mTouchSlop || xDiff > mTouchSlop) {
                    mIsBeingDragged = true;
                }
                break;

            case MotionEvent.ACTION_DOWN:
                /* Remember location of down touch */
                mLastMotionY = y;
                mLastMotionX = x;
                Log.e("onIntercept----->", "ACTION_MOVE--------------------------------ACTION_DOWN");
                Log.e("onIntercept----->", "mPrvLastMotionY(): " + y);
                Log.e("onIntercept----->", "mPrvLastMotionY(): " + getScrollY());
                mPrvLastMotionY = getScrollY();
                mPrvLastMotionX = x;
                /*
                * If being flinged and user touches the screen, initiate drag...
                * otherwise don't.  mScroller.isFinished should be false when
                * being flinged.
                */
                mIsBeingDragged = !mScroller.isFinished();
                break;

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                /* Release the drag */
                mIsBeingDragged = false;
                break;
        }

        /*
        * The only time we want to intercept motion events is if we are in the
        * drag mode.
        */
        return mIsBeingDragged;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

//        Log.e("Zoo----->", "Y   -------TUchY->"+mLastMotionY);
        if (ev.getAction() == MotionEvent.ACTION_DOWN && ev.getEdgeFlags() != 0) {
            // Don't handle edge touches immediately -- they may actually belong to one of our
            // descendants.
            return false;
        }

        if (!canScroll()) {
            return false;
        }

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(ev);

        final int action = ev.getAction();
        final float y = ev.getY();
        final float x = ev.getX();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                /*
                * If being flinged and user touches, stop the fling. isFinished
                * will be false if being flinged.
                */
                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }

                // Remember where the motion event started
                mLastMotionY = y;
                mLastMotionX = x;
                Log.e("onTouchEvent----->", "ACTION_MOVE--------------------------------ACTION_DOWN");
                Log.e("onTouchEvent----->", "mPrvLastMotionY(): " + y);
                Log.e("onTouchEvent----->", "mPrvLastMotionY(): " + getScrollY());
                mPrvLastMotionY = y + getScrollY();
                mPrvLastMotionX = x;
                break;
            case MotionEvent.ACTION_MOVE:

                if (!mMoveFreeze) {
                    // Scroll to follow the motion event

                    Log.e("onTouchEvent----->", "ACTION_MOVE--------------------------------ACTION_MOVE");
//                    Log.e("onTouchEvent----->", "mLastMotionX(): " +mLastMotionX);
//                    Log.e("onTouchEvent----->", "            x : " +x);
                    int deltaX = (int) (mLastMotionX - x);
                    int deltaY = (int) (mLastMotionY - y);
                    mLastMotionX = x;
                    mLastMotionY = y;
//                    Log.e("onTouchEvent----->", "        deltaX : " +deltaX);
//                    Log.e("onTouchEvent----->", "        Range x : " +(int)(((wrapperWidth-screenWidth)) ));
                    if (deltaX < 0) {
//                        Log.e("onTouchEvent----->", "        ScrollX : " +HScrollView.getScrollX());

//                         - (int)(((wrapperWidth-screenWidth)) * mScale)
                        if (HScrollView != null)
                            if (HScrollView.getScrollX() < 0) {
                                deltaX = 0;
                            }
                    } else if (deltaX > 0) {

                        final int rightEdge = (screenWidth);
//                        Log.e("onTouchEvent----->", "H.geCdWidth(): " +HScrollView.getChildWidth() );
//                        Log.e("onTouchEvent----->", "H.rightEdge(): " +rightEdge);
//                        Log.e("onTouchEvent----->", "H.rightX(): " +HScrollView.getScrollX());
                        int availableToScroll = 0;
                        if (HScrollView != null)
                            availableToScroll = (HScrollView.getChildWidth() - ((wrapperWidth > screenWidth) ? screenWidth : wrapperWidth)) - ((int) (HScrollView.getScrollX()));

//                        Log.e("onTouchEvent----->", "availToScroll: " +availableToScroll);
                        if (availableToScroll > 0) {
                            deltaX = Math.min((availableToScroll), deltaX);
                        } else {
                            deltaX = 0;
                        }
                    }
                    if (deltaY < 0) {
                        if (getScrollY() < 0) {
                            deltaY = 0;
                        }
                    } else if (deltaY > 0) {
                        final int bottomEdge = screenHeight - getPaddingBottom();
//                        Log.e("onTouchEvent----->", "H.bottomEdge(): " +bottomEdge);
                        final int availableToScroll = getChildAt(0).getBottom() - getScrollY() - bottomEdge;
                        if (availableToScroll > 0) {
                            deltaY = Math.min(availableToScroll, deltaY);
                        } else {
                            deltaY = 0;
                        }
                    }
//                    Log.e("onTouchEvent----->", "H.getRight(): " +HScrollView.getChildAt(0).getRight());
//                    Log.e("onTouchEvent----->", "H.getScrollX(): " +HScrollView.getScrollX() );
//                    Log.e("onTouchEvent----->", "getRight(): " +getChildAt(0).getBottom());
//                    Log.e("onTouchEvent----->", " ev deltaX  :" +deltaX);
                    if (deltaY != 0 || deltaX != 0)
                        if (HScrollView != null)
                            HScrollView.scrollBy(((int) ((deltaX) * mScale)), 0, 0);
                    scrollBy(0, deltaY);
                }

                break;
            case MotionEvent.ACTION_UP:
//                Log.e("onTouchEvent----->", "ACTION_UP--------------------------------ACTION_UP");
//                Log.e("onTouchEvent----->", " ev count" +ev.getPointerCount());
                mMoveFreeze = false;
                final VelocityTracker velocityTracker = mVelocityTracker;
                velocityTracker.computeCurrentVelocity(1000);
                int initialXVelocity = (int) velocityTracker.getXVelocity();
                int initialYVelocity = (int) velocityTracker.getYVelocity();

                if ((Math.abs(initialXVelocity) + Math.abs(initialYVelocity) > mMinimumVelocity) && getChildCount() > 0) {
                    fling(-initialXVelocity, -initialYVelocity);
                }

                if (mVelocityTracker != null) {
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (mEnableScaling) {
            super.dispatchTouchEvent(event);
            if (mScaleDetector != null)
                mScaleDetector.onTouchEvent(event);

            // scale event positions according to scale before passing them to children
//            KayakLog.debug(TwoDScrollView.class.getSimpleName(),
//                    String.format(Locale.getDefault(), "Position (%.2f,%.2f) ScrollOffset (%d,%d) Scale %.2f",
//                            event.getX(), event.getY(), getScrollX(), getScrollY(), mScale));

//            Log.e("ZooDD----->", "mPrvLastMotionY: "+mPrvLastMotionY);
//            Log.e("ZooDD----->", "mLastMotionY   :"+mLastMotionY);
//            Log.e("ZooDD----->", "           Y--------------------------------> :"+(mPrvLastMotionY-(( event.getY()) * mScale)) );
//            Log.e("ZooDD----->", "Dispatch   EY  :" + event.getY());
//            Log.e("ZooDD----->", "mScaleBegin    :" + mScaleBegin);


            event.setLocation((event.getX()) * mScale, (mScrollY + event.getY()) * mScale);

//            mScrollY= (int) ( mLastMotionY - ( event.getY() * mScale));

            if (mScaleBegin && (((int) (mPrvLastMotionY - ((event.getY()) * mScale))) > 0 || ((int) (mPrvLastMotionX - ((event.getX()) * mScale))) > 0)) {
                Log.e("ZooDD----->", "Dispatch--------------------------------       Dispatch   :" + mScale);
                Log.e("ZooDD----->", "mPrvLastMotionX: " + mPrvLastMotionY);
                Log.e("ZooDD----->", " event.getX()   :" + event.getY());
//                mScrollY=(int)(mPrvLastMotionY-(( event.getY()) * mScale));
                scrollTo((int) (mPrvLastMotionX - ((event.getX()) * mScale)), (int) (mPrvLastMotionY - ((event.getY()) * mScale)), 0);

            }


            return gestureDetector.onTouchEvent(event);
        } else {
            return super.dispatchTouchEvent(event);
        }
    }


//    /**
//     * <p>Handles scrolling in response to a "home/end" shortcut press.</p>
//     *
//     * @param directionVert the scroll direction: {@link android.view.View#FOCUS_UP}
//     *                  to go the top of the view or
//     *                  {@link android.view.View#FOCUS_DOWN} to go the bottom
//     * @param directionHorz the scroll direction: {@link android.view.View#FOCUS_RIGHT}
//     *                  to go the right of the view or
//     *                  {@link android.view.View#FOCUS_LEFT} to go the left
//     * @return true if the key event is consumed by this method, false otherwise
//     */
//    public boolean fullScroll(int directionVert, int directionHorz) {
//        int scrollAmountY = 0, scrollAmountX = 0;
//        //  vertical
//        switch (directionVert) {
//            case View.FOCUS_UP:
//                scrollAmountY = -getScrollY();
//                break;
//            case View.FOCUS_DOWN:
//                int count = getChildCount();
//                if (count > 0) {
//                    View view = getChildAt(count - 1);
//                    scrollAmountY = (view.getBottom() - screenHeight) - getScrollY();
//                }
//                break;
//        }
//        // horizontal
//        switch (directionHorz) {
//            case View.FOCUS_LEFT:
//                scrollAmountX = -getScrollX();
//                break;
//            case View.FOCUS_RIGHT:
//                int count = getChildCount();
//                if (count > 0) {
//                    View view = getChildAt(count - 1);
//                    scrollAmountX = (view.getRight() - screenWidth) - getScrollX();
//                }
//                break;
//        }
//        boolean handled = (scrollAmountX != 0) || (scrollAmountY != 0);
//        if (handled)
//            doScroll(scrollAmountX, scrollAmountY);
//        return handled;
//    }
//
//    /**
//     * Smooth scroll by a Y delta
//     *
//     * @param deltaX the number of pixels to scroll by on the X axis
//     * @param deltaY the number of pixels to scroll by on the Y axis
//     */
//    private void doScroll(int deltaX, int deltaY) {
//        if (deltaX != 0 || deltaY != 0) {
//            smoothScrollBy(deltaX, deltaY);
//        }
//    }
//
//    /**
//     * Like {@link View#scrollBy}, but scroll smoothly instead of immediately.
//     *
//     * @param dx the number of pixels to scroll by on the X axis
//     * @param dy the number of pixels to scroll by on the Y axis
//     */
//    public final void smoothScrollBy(int dx, int dy) {
//        long duration = AnimationUtils.currentAnimationTimeMillis() - mLastScroll;
//        if (duration > ANIMATED_SCROLL_GAP) {
//            mScroller.startScroll(getScrollX(), getScrollY(), dx, dy);
//            awakenScrollBars(mScroller.getDuration());
//            invalidate();
//        } else {
//            if (!mScroller.isFinished()) {
//                mScroller.abortAnimation();
//            }
//            scrollBy(dx, dy);
//        }
//        mLastScroll = AnimationUtils.currentAnimationTimeMillis();
//    }
//
//    /**
//     * Like {@link #scrollTo}, but scroll smoothly instead of immediately.
//     *
//     * @param x the position where to scroll on the X axis
//     * @param y the position where to scroll on the Y axis
//     */
//    public final void smoothScrollTo(int x, int y) {
//        smoothScrollBy(x - getScrollX(), y - getScrollY());
//    }

    /**
     * <p>The scroll range of a scroll view is the overall height of all of its
     * children.</p>
     */
    @Override
    protected int computeVerticalScrollRange() {
        int count = getChildCount();
        return count == 0 ? screenHeight : (getChildAt(0)).getBottom();
    }

    @Override
    protected int computeHorizontalScrollRange() {
        int count = getChildCount();
        return count == 0 ? screenWidth : (getChildAt(0)).getRight();
    }


    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            // This is called at drawing time by ViewGroup.  We don't want to
            // re-show the scrollbars at this point, which scrollTo will do,
            // so we replicate most of scrollTo here.
            //
            //         It's a little odd to call onScrollChanged from inside the drawing.
            //
            //         It is, except when you remember that computeScroll() is used to
            //         animate scrolling. So unless we want to defer the onScrollChanged()
            //         until the end of the animated scrolling, we don't really have a
            //         choice here.
            //
            //         I agree.  The alternative, which I think would be worse, is to post
            //         something and tell the subclasses later.  This is bad because there
            //         will be a window where mScrollX/Y is different from what the app
            //         thinks it is.
            //
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = mScroller.getCurrX();
            int y = mScroller.getCurrY();
            if (getChildCount() > 0) {
                View child = getChildAt(0);
                Log.e("computeScroll----->", "Y   -------Y->-------------------------------computeScroll 1");
                scrollTo(clamp(x, screenWidth - getPaddingRight() - getPaddingLeft(), wrapperWidth),
                        clamp(y, screenHeight - getPaddingBottom() - getPaddingTop(), child.getHeight()), 3);

            } else {
                Log.e("computeScroll----->", "Y   -------Y->-------------------------------computeScroll2");
                scrollTo(x, y, 0);
            }
            if (oldX != getScrollX() || oldY != getScrollY()) {
                onScrollChanged(getScrollX(), getScrollY(), oldX, oldY);
            }

            // Keep on drawing until the animation has finished.
            postInvalidate();
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        // Calling this with the present values causes it to re-clam them
//        Log.e("onLayout----->", "Y   -------Y->-------------------------------onLayout");
//        scrollTo(getScrollX(),mScrollY,1);
    }

    /**
     * Fling the scroll view
     *
     * @param velocityY The initial velocity in the Y direction. Positive
     *                  numbers mean that the finger/curor is moving down the screen,
     *                  which means we want to scroll towards the top.
     */
    public void fling(int velocityX, int velocityY) {
        if (getChildCount() > 0) {
            int height = screenHeight - getPaddingBottom() - getPaddingTop();
            int bottom = getChildAt(0).getHeight();
            int width = screenWidth - getPaddingRight() - getPaddingLeft();
            int right = getChildAt(0).getWidth();

            mScroller.fling(getScrollX(), getScrollY(), velocityX, velocityY, 0, right - width, 0, bottom - height);

            awakenScrollBars(mScroller.getDuration());
            invalidate();
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>This version also clamps the scrolling to the bounds of our child.
     */
    public void scrollTo(int x, int y, int t) {
        // we rely on the fact the View.scrollBy calls scrollTo.
        if (getChildCount() > 0) {

            Log.e("ZooDD----->", "scrollTo--------------------------------scrollTo");
            Log.e("ZooDD----->", "      -----Y: " + y);
//            Log.e("ZooDD----->", "getScrollY->: "+( ((getScrollY() ) )));

            View child = getChildAt(0);
//            x = clamp(x, screenWidth - getPaddingRight() - getPaddingLeft(), wrapperWidth);
            y = clamp(y, screenHeight - getPaddingBottom() - getPaddingTop(), child.getHeight());
            Log.e("ZooDD----->", "      Y clap: " + y);
            if (HScrollView != null && t != 3) {
//                Log.e("scrollTo----->", "X   -------X->-------------------------------: "+x);
                HScrollView.scrollTo(x, 0, 0);
            }

//            Log.e("ZooDD----->", "getScrollY->: "+( ((getScrollY() ) )));

//            Log.e("ZooDD----->", "      -----Y: "+y);

            if ((x != getScrollX() || y != getScrollY())) {
//                Log.e("ZooDD----->", "      -----Y: "+y);

//                Log.e("Zoo----->", "Y   -------Y->-------------------------------: "+y);
                super.scrollTo(0, y);
            }
        }
    }

    private int clamp(int n, int my, int child) {
        if (my >= child || n < 0) {
            /* my >= child is this case:
             *                    |--------------- me ---------------|
             *     |------ child ------|
             * or
             *     |--------------- me ---------------|
             *            |------ child ------|
             * or
             *     |--------------- me ---------------|
             *                                  |------ child ------|
             *
             * n < 0 is this case:
             *     |------ me ------|
             *                    |-------- child --------|
             *     |-- mScrollX --|
             */
            return 0;
        }
        if ((my + n) > child) {
            /* this case:
             *                    |------ me ------|
             *     |------ child ------|
             *     |-- mScrollX --|
             */
            return child - my;
        }
        return n;
    }


    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            // double tap fired.
            return true;
        }
    }

//    private View child() {
//        return getChildAt(0);
//    }
}

