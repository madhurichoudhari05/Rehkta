package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubePlayer;

import org.Rekhta.adapters.SherCollectionsAdapter;
import org.Rekhta.adapters.poetDetail.PoetShersNewAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import org.Rekhta.R;;
import org.Rekhta.adapters.poetDetail.PoetAudioAdapter;
import org.Rekhta.adapters.poetDetail.PoetDetailedViewPagerAdapter;
import org.Rekhta.adapters.poetDetail.PoetGhazalAdapter;
import org.Rekhta.adapters.poetDetail.PoetNazmAdapter;
import org.Rekhta.adapters.poetDetail.PoetSherAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.fragments.SearchFragment.SearchGhazalContentRendering;
import org.Rekhta.fragments.poetDetailledFragments.GhazalAndNazmContent;
import org.Rekhta.fragments.poetDetailledFragments.PoetAudioFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetGhazalFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetNazmFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetProfileFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetSherFragment;
import org.Rekhta.fragments.poetDetailledFragments.PoetVideoFragment;
import org.Rekhta.model.imageShayeri.ImageShayriCollectionData;
import org.Rekhta.model.poetDetail.PoetDetailData;
import org.Rekhta.model.poetDetail.PoetGhazalData;
import org.Rekhta.model.poetDetail.PoetNazmData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MediaPlayerService;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PoetDetailedActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {

    public NavigationFragment drawerFragment;
    public YouTubePlayer currentPlayer;
    public boolean isVideoPlaying = false;
    public boolean isAudioPlaying = false;
    public PoetDetailData poetDetailData;
    public String currentPoetId = "";
    public String totalGhazalCount = "0";
    public String totalNazmCount = "0";
    public String totalSherCount = "0";
    public String poetShortDescInEng = "";
    public String poetShortDescInHin = "";
    public String poetShortDescInUrdu = "";
    public boolean showProfileTab = false;
    public boolean cameFromLeftNav = true;
    public String searchedText = "";
    public String idGhazalToRender;
    public String currentGhazalPoetName;
    public String currentNazmPoetName;
    public String idNazmToRender;
    public PoetNazmAdapter poetNazmAdapter;
    public PoetGhazalAdapter poetGhazalAdapter;
    public PoetSherAdapter poetSherAdapter;
    public PoetProfileFragment poetProfileFrag;
    public PoetAudioAdapter poetAudioAdapter;
    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    ViewPager poetDetailedVpager;
    TabLayout poetDetailedTablayout;
    PoetDetailedViewPagerAdapter poetDetailedViewPagerAdapter;
    private Toolbar toolbar;
    private int ghazalCount = 0;
    private int nazmCount = 0;
    private int sherCount = 0;
    private int ac, vc;
    ImageView backButton;
    ArrayList<Fragment> data = new ArrayList<>();
    public ArrayList<String> lastRenderSherName = new ArrayList<String>();
    public ArrayList<String> idsRendered = new ArrayList<String>();
    public String currentSherID = "";


    public PoetShersNewAdapter poetShersNewAdapter;
    private ArrayList<String> tabName;
    private ImageView search, rignMenu;
    private PoetAudioFragment audioFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poet_detailed);
        Bundle extras = getIntent().getExtras();

        currentPoetId = extras.getString("poetId");
        totalGhazalCount = extras.getString("ghazalCount");
        totalNazmCount = extras.getString("nazmCount");
        totalSherCount = extras.getString("sherCount");
        poetShortDescInEng = extras.getString("shortDescInEng");
        poetShortDescInHin = extras.getString("shortDescInHin");
        poetShortDescInUrdu = extras.getString("shortDescInUrdu");
        showProfileTab = extras.getBoolean("shouldLandOnProfile");

        // cameFromLeftNav  = extras.getBoolean("cameFromLeftNav");

        poetDetailedVpager = (ViewPager) findViewById(R.id.poetDetailed_viewpager);
        poetDetailedVpager.setOffscreenPageLimit(0);
        poetDetailedTablayout = (TabLayout) findViewById(R.id.poetDetailed_Tablayout);
        // intTheTabsOfViewPager(0,1,1,1,1);


        getPoetDetails(currentPoetId);

        Log.e("temp", "tempToken" + CommonUtil.SharedPrefKeys.UniqueId);
        Log.e("poetid", "poetid" + currentPoetId);


        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);


        search = (ImageView) findViewById(R.id.search);
        rignMenu = (ImageView) findViewById(R.id.trippleDot);
        initRightMenuPopUpDialog();
        backButton = (ImageView) findViewById(R.id.poetDetailbackButton);
        toolbar = (Toolbar) findViewById(R.id.toolbarfrag);
        setSupportActionBar(toolbar);
        //   toolbar.inflateMenu(R.menu.home_screen);

        initRightMenuPopUpDialog();
        CommonUtil.setBackButton(PoetDetailedActivity.this, backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        drawerFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        drawerFragment.disableDrawerIcon();


    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);

        // toolbar.setNavigationIcon(R.drawable.ic_back);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orentaiont = this.getResources().getConfiguration().orientation;
        if (orentaiont == 2) {
            poetDetailedTablayout.setTabGravity(TabLayout.GRAVITY_FILL);
            poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);

        } else if (orentaiont == 1) {
            if (tabName.size() < 5) {
                poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                poetDetailedTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
            //  poetDetailedTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        //   initRightMenuPopUpDialog();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(PoetDetailedActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PoetDetailedActivity.this, MyFavoritesActivity.class));
            }
        });

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(PoetDetailedActivity.this, Dashboard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }

    @Override
    public void langChanged() {

        // CommonUtil.setBackButton(PoetDetailedActivity.this, backButton);


        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }

        int c = toolbar.getChildCount();

        CommonUtil.setBackButton(PoetDetailedActivity.this, backButton);


        if (this.poetGhazalAdapter != null)
            poetGhazalAdapter.notifyDataSetChanged();
        if (this.poetSherAdapter != null)
            poetSherAdapter.notifyDataSetChanged();
        if (this.poetNazmAdapter != null)
            poetNazmAdapter.notifyDataSetChanged();
        if (this.poetProfileFrag != null)
            poetProfileFrag.setDataTofiels();

        if (poetAudioAdapter != null)
            poetAudioAdapter.notifyDataSetChanged();


        intTheTabsOfViewPager(ghazalCount, nazmCount, sherCount, ac, vc);


    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int orentaiont = this.getResources().getConfiguration().orientation;
        if (orentaiont == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);
        }
    }

    public void getPoetDetails(String poetId) {
        final ProgressDialog progressLoader = ProgressDialog.show(this, null, "Please wait...", false, false);
        RestClient.get().getPoetDetails(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, poetId, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();


                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        JSONObject content = obj.getJSONObject("R");
                        poetDetailData = new PoetDetailData(content.getString("I"),
                                content.getString("NE"), content.getString("NH"), content.getString("NU"),
                                content.getString("PE"), content.getString("PH"), content.getString("PU"),
                                content.getString("Be"), content.getString("Bh"), content.getString("Bu"),
                                content.getString("DOe"), content.getString("DOh"), content.getString("DOu"),
                                content.getString("RNe"), content.getString("RNh"), content.getString("RNu"),
                                content.getString("dob"), content.getString("dod"),
                                content.getInt("AC"), content.getInt("VC"), content.getInt("EC"),
                                content.getJSONArray("C"));


                        if (content.getJSONArray("C").length() > 0) {
                            for (int i = 0; i < content.getJSONArray("C").length(); i++) {

                                if (content.getJSONArray("C").getJSONObject(i).getInt("S") == 1) {
                                    ghazalCount = content.getJSONArray("C").getJSONObject(i).getInt("C");
                                }
                                if (content.getJSONArray("C").getJSONObject(i).getInt("S") == 4) {
                                    sherCount = content.getJSONArray("C").getJSONObject(i).getInt("C");
                                }
                                if (content.getJSONArray("C").getJSONObject(i).getInt("S") == 3) {
                                    nazmCount = content.getJSONArray("C").getJSONObject(i).getInt("C");
                                }

                            }
                        }

                        ac = content.getInt("AC");
                        vc = content.getInt("VC");

                        intTheTabsOfViewPager(ghazalCount, nazmCount, sherCount, ac, vc);
                    } else {
                        Toast.makeText(getApplicationContext(), "Oops an error occured", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(PoetDetailedActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(PoetDetailedActivity.this);

    }

    public void intTheTabsOfViewPager(int ghazalCount, int nazmCount, int sherCount, int audioCount, int videoCount) {

        if (data != null) {
            data.clear();
        }
        tabName = new ArrayList<>();
        Fragment f = null;
        data.add(new PoetProfileFragment());


        tabName.add(LanguageSelection.getSpecificLanguageWordById("profile", CommonUtil.languageCode));
        int tabcount = 1;

        if (ghazalCount > 0) {
            tabcount++;
            f = new PoetGhazalFragment();
            data.add(f);
            tabName.add(LanguageSelection.getSpecificLanguageWordById("ghazal", CommonUtil.languageCode));
        }
        if (nazmCount > 0) {
            tabcount++;
            f = new PoetNazmFragment();
            data.add(f);
            tabName.add(LanguageSelection.getSpecificLanguageWordById("nazm", CommonUtil.languageCode));
        }
        if (sherCount > 0) {
            tabcount++;
            f = new PoetSherFragment();
            data.add(f);
            tabName.add(LanguageSelection.getSpecificLanguageWordById("sher", CommonUtil.languageCode));
        }
        if (audioCount > 0) {
            tabcount++;
            audioFragment = new PoetAudioFragment();
            data.add(audioFragment);
            tabName.add(LanguageSelection.getSpecificLanguageWordById("Audio", CommonUtil.languageCode));
        }
        if (videoCount > 0) {
            tabcount++;
            f = new PoetVideoFragment();
            data.add(f);
            tabName.add(LanguageSelection.getSpecificLanguageWordById("Video", CommonUtil.languageCode));
        }

        int orentaiont = this.getResources().getConfiguration().orientation;
        if (orentaiont == 2) {
            poetDetailedTablayout.setTabGravity(TabLayout.GRAVITY_FILL);
            poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);

        } else if (orentaiont == 1) {
            if (tabName.size() < 5) {
                poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                poetDetailedTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
            // poetDetailedTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

        poetDetailedViewPagerAdapter = new PoetDetailedViewPagerAdapter(getSupportFragmentManager(), this, data, tabName);
        //poetDetailedTablayout.setTabGravity(TabLayout.TEXT_ALIGNMENT_GRAVITY);
        // poetDetailedTablayout.setTabMode(TabLayout.TEXT_ALIGNMENT_CENTER );
        poetDetailedVpager.setAdapter(poetDetailedViewPagerAdapter);
        poetDetailedTablayout.setupWithViewPager(poetDetailedVpager);
        if (showProfileTab) {
            poetDetailedVpager.setCurrentItem(0);
            //poetDetailedTablayout.setTabMode(0);
        } else {
            poetDetailedVpager.setCurrentItem(1);
            // poetDetailedTablayout.setTabMode(1);
        }

        /*if (tabName.size() < 5) {
            poetDetailedTablayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            poetDetailedTablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }*/

        //poetDetailedTablayout.removeTabAt(1);
        //poetDetailedTablayout.getChildAt(0).setVisibility(View.GONE);


    }


    @Override
    public void onBackPressed() {

        if (this.lastRenderSherName.size() != 0) {
            CommonUtil.cSherTypeNameEnglish = this.lastRenderSherName.get(this.lastRenderSherName.size() - 1);
            this.lastRenderSherName.remove(lastRenderSherName.size() - 1);

        }

        if (this.idsRendered.size() != 0) {
            this.currentSherID = this.idsRendered.get(this.idsRendered.size() - 1);
            this.idsRendered.remove(idsRendered.size() - 1);
        }

        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {

            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }

          //  super.onBackPressed();
            intTheTabsOfViewPager(ghazalCount, nazmCount, sherCount, ac, vc);
            sendLocalPoetsBroadcast();

        } else {

            getFragmentManager().popBackStack();
        }
        if (isVideoPlaying) {
            this.currentPlayer.release();
        }
        if (isAudioPlaying) {
            MediaPlayerService.stopAudio();
        }
        //MediaPlayerService.stopAudio(new ImageView(this));

        CommonUtil.setBackButton(PoetDetailedActivity.this, backButton);
    }

    private void sendLocalPoetsBroadcast() {

        Intent intent = new Intent("PoestBroadcast");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void removeFragment() {
        getFragmentManager().popBackStack();
        startFragment();
    }

    public void removeMeaningFragment() {
        getSupportFragmentManager().popBackStack();
    }


    public void startFragment() {
        GhazalAndNazmContent ghazalAndNazmContent = new GhazalAndNazmContent();
//                    ghazalDetails.setArguments(bundle);
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.drawer_layout, ghazalAndNazmContent, "searchGhazalContentRendering")
                //.addToBackStack(null)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }
}
