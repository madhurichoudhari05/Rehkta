package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.adapters.ImageShayari.FullScreenImageAdapter;
import org.Rekhta.utils.LanguageSelection;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


import org.Rekhta.R;;
import org.Rekhta.adapters.ImageShayari.ImageShayariViewPagerAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.madhu.dto.ShayariDetailDto;
import org.Rekhta.utils.CommonUtil;

public class ImageShyariActivity extends AppCompatActivity implements
        NavigationFragment.NavigationDrawerCallbacks,
        ActivityCompat.OnRequestPermissionsResultCallback {


    public NavigationFragment drawerFragment;
    public int currentImgClicked = 0;
    public String currentImgClickedS = "";
    public JSONArray dataArray1 = new JSONArray();
    public List<ShayariDetailDto> getShayari = new ArrayList<>();
    ImageShayariViewPagerAdapter imageShayariViewPagerAdapter;
    ViewPager ImgShayeriViewPager;
    TabLayout ImgShayeriTablayout;
    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    private Toolbar toolbar;
    public FullScreenImageAdapter fullScreenImageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_shyari);
        if (checkPermission()) {
            initViews();
        } else {
            requestPermission();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (imageShayariViewPagerAdapter != null) {
            imageShayariViewPagerAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(ImageShyariActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(ImageShyariActivity.this);

    }

    public void initViews() {
        ImgShayeriViewPager = (ViewPager) findViewById(R.id.ImageShayri_viewpager);
//        ImgShayeriViewPager.setOffscreenPageLimit(2);
        ImgShayeriTablayout = (TabLayout) findViewById(R.id.ImageShayri_layout);
        imageShayariViewPagerAdapter = new ImageShayariViewPagerAdapter(getSupportFragmentManager());
        ImgShayeriTablayout.setTabMode(TabLayout.MODE_FIXED);
        ImgShayeriViewPager.setAdapter(imageShayariViewPagerAdapter);
        ImgShayeriTablayout.setupWithViewPager(ImgShayeriViewPager);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ImageShyariActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ImageShyariActivity.this, MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                drawerFragment.resetButtonColor(1);
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                drawerFragment.resetButtonColor(2);
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                drawerFragment.resetButtonColor(3);
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(ImageShyariActivity.this, Dashboard.class));
        finish();
    }

    @Override
    public void langChanged() {

        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else if (CommonUtil.languageCode == 2) {
            forceLTRIfSupported();

        } else if (CommonUtil.languageCode == 1) {
            forceLTRIfSupported();
        }

       /* imageShayariViewPagerAdapter = new ImageShayariViewPagerAdapter(getSupportFragmentManager());
        ImgShayeriTablayout.setTabMode(TabLayout.MODE_FIXED);
        ImgShayeriViewPager.setAdapter(imageShayariViewPagerAdapter);
        ImgShayeriTablayout.setupWithViewPager(ImgShayeriViewPager);*/

        if (imageShayariViewPagerAdapter != null)
            imageShayariViewPagerAdapter.notifyDataSetChanged();

        if (fullScreenImageAdapter != null) {
            fullScreenImageAdapter.notifyDataSetChanged();
        }
    }


    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(ImageShyariActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(ImageShyariActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(ImageShyariActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(ImageShyariActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            ActivityCompat.requestPermissions(ImageShyariActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initViews();
                } else {
                    Log.e("value", "Permission Denied, You cannot use local drive .");
                }
                break;
        }
    }
}
