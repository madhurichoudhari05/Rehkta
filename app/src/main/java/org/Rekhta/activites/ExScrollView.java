package org.Rekhta.activites;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.HorizontalScrollView;
import android.widget.Scroller;


/**
 * Created by kuldeep.pal on 20-11-2017.
 */

public class ExScrollView extends HorizontalScrollView {

    // true if we can scroll the ScrollView
    // false if we cannot scroll
    private Scroller mScroller;
    private boolean enableScrolling = false;
    private int childWidth=0;
    static final int ANIMATED_SCROLL_GAP = 250;
    private long mLastScroll;
    public ExScrollView(Context context) {
        super(context);
        initTwoDScrollView();
    }

    public ExScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTwoDScrollView();
    }

    public ExScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTwoDScrollView();
    }

    private void initTwoDScrollView() {
        mScroller = new Scroller(getContext());
        setFocusable(true);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
        setWillNotDraw(false);
        final ViewConfiguration configuration = ViewConfiguration.get(getContext());
//        mTouchSlop = configuration.getScaledTouchSlop();
//        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();


    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // if we can scroll pass the event to the superclass
                if (scrollingEnabled()) return super.onTouchEvent(ev);
                // only continue to handle the touch event if scrolling enabled
                return scrollingEnabled(); // scrollable is always false at this point
            default:
                return super.onTouchEvent(ev);
        }
    }

    @Override
    protected int computeHorizontalScrollRange() {
        return 1080;
    }

    @Override
    protected int computeHorizontalScrollOffset() {
        int t=super.computeHorizontalScrollOffset();
        return 1080;
    }

    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            // This is called at drawing time by ViewGroup.  We don't want to
            // re-show the scrollbars at this point, which scrollTo will do,
            // so we replicate most of scrollTo here.
            //
            //         It's a little odd to call onScrollChanged from inside the drawing.
            //
            //         It is, except when you remember that computeScroll() is used to
            //         animate scrolling. So unless we want to defer the onScrollChanged()
            //         until the end of the animated scrolling, we don't really have a
            //         choice here.
            //
            //         I agree.  The alternative, which I think would be worse, is to post
            //         something and tell the subclasses later.  This is bad because there
            //         will be a window where mScrollX/Y is different from what the app
            //         thinks it is.
            //
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = mScroller.getCurrX();
            int y = mScroller.getCurrY();
            if (getChildCount() > 0) {
                View child = getChildAt(0);
                Log.e("computeScroll----->", "Y   -------Y->-------------------------------computeScroll 1 Horizontal");
                scrollTo(clamp(x, getWidth() - getPaddingRight() - getPaddingLeft(), child.getWidth()),
                        0,0);

            } else {
                Log.e("computeScroll----->", "Y   -------Y->-------------------------------computeScroll2 Horizontal");
                scrollTo(x, y,0);
            }
            if (oldX != getScrollX() || oldY != getScrollY()) {
                onScrollChanged(getScrollX(), getScrollY(), oldX, oldY);
            }

            // Keep on drawing until the animation has finished.
            postInvalidate();
        }
    }


    public void scrollBy(int x, int y,int t) {
//        Log.e("scrollBy-----==<", "--------------------------------------------Inner Scroll  :" +x);
//        Log.e("scrollBy-----==<", "--------------------------------------------Inner Scroll  :" +getScrollX());
        super.scrollBy(x, 0);
//        super.scrollTo(120, y);
    }

    public void scrollTo(int x, int y, int t) {
        // we rely on the fact the View.scrollBy calls scrollTo.
        if (getChildCount() > 0) {

            Log.e("ZooDD----->", "scrollTo--------------------------------scrollTo");
            Log.e("ZooDD----->", "      -----Y: "+y);
            Log.e("ZooDD----->", "getScrollY->: "+( ((getScrollY() ) )));

            View child = getChildAt(0);
            x = clamp(x, getWidth() - getPaddingRight() - getPaddingLeft(), child.getWidth());
            y = clamp(y,0, child.getHeight());



            Log.e("ZooDD----->", "getScrollY->: "+( ((getScrollY() ) )));

            Log.e("ZooDD----->", "      -----Y: "+y);

            if ((x != getScrollX() || y != getScrollY() ) ){
                Log.e("ZooDD----->", "      -----Y: "+x);

                Log.e("Zoo----->", "Y   -------Y->-------------------------------: "+x);
                super.scrollTo(x, y);
            }
        }
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if (scrollingEnabled()) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private boolean scrollingEnabled(){
        return enableScrolling;
    }
    public void setScrolling(boolean enableScrolling) {
        this.enableScrolling = enableScrolling;
    }
public void setChildWidth(int width)
{



    childWidth=width;
//    ((LinearLayout)getChildAt(0)).setPadding(0,0,0,0);
//    Log.e("setChildWidth-----==<", "---------------------------------Padding  --------  :" +getChildAt(0).getWidth());
}
    public int  getChildWidth()
    {


        return childWidth;
    }

    private int clamp(int n, int my, int child) {
        if (my >= child || n < 0) {
            /* my >= child is this case:
             *                    |--------------- me ---------------|
             *     |------ child ------|
             * or
             *     |--------------- me ---------------|
             *            |------ child ------|
             * or
             *     |--------------- me ---------------|
             *                                  |------ child ------|
             *
             * n < 0 is this case:
             *     |------ me ------|
             *                    |-------- child --------|
             *     |-- mScrollX --|
             */
            return 0;
        }
        if ((my+n) > child) {
            /* this case:
             *                    |------ me ------|
             *     |------ child ------|
             *     |-- mScrollX --|
             */
            return child-my;
        }
        return n;
    }


}