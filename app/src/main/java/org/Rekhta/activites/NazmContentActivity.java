package org.Rekhta.activites;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.R;
import org.Rekhta.fonts.LatoBoldTextView;
import org.Rekhta.fonts.MerriweatherBoldTextView;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NazmContentActivity extends AppCompatActivity {


    ScrollView scrollView;
    JSONArray dataArray;
    JSONObject data;


    public float textSixeToRender;
    View view = null;
    TextView t[];
    LinearLayout linearL;
    ExScrollView vScrollWrapper;
    LinearLayout linearWrapper;
    RelativeLayout outerLinear;
    Double alignmenht;
    ZoomView zoomView;
    Toolbar toolbar;
    TextView toolbarTV;
    LatoBoldTextView view_gazals, view_profile;
    ImageView backButton;
    ImageView editorChoiceImg;
    ImageView popularchoiceImg;
    ImageView authorImage;
    Dialog dialog;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    SharedPreferences DataPrefs = null;
    JSONObject jsonData;
    JSONObject prevData;
    JSONObject nextData;
    ImageView footor_dotsIcon, footor_audioIcon, footor_videoIcon, footor_shareIcon, footor_infoIcon, footor_favIcon, footer_default;
    MediaPlayer player;
    boolean renderComplete;
    int positionClicked = 0;
    TextView nextTiltle, nextAuthorName, prevTitle, prevAuthorName, nextItemTxt, prevItemTxt;
    LinearLayout nextLinaerlayoutview, prevLinaerlayoutview;
    View didvider_nextandprev;
    String audioUrl = "";
    JSONArray videoArrayData;
    JSONArray audioArrayData;
    BottomSheetDialog bottomSheetDialog;
    MerriweatherBoldTextView detailedAuthorName;
    WindowManager.LayoutParams wlp;
    boolean showCritiqueDialog = true;
    boolean isCritiqueModeOn = false;
    FrameLayout nazm_framelayout;
    float sizeoFtextShouldBe = 100;
    View mainFooter;
    View critiqueFooter;
    View critiqueFooterWhenOn;
    Dialog critiqueSubmitForm;
    ImageView renderNextArrow, renderPrevArrow;
    private TextView turnOffCriticText, footerCrcitcOnText;
    private boolean isLoggedIn;
    private ProgressDialog progressLoader;
    private Typeface engtf, hinditf, urdutf;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nazm_content);
        context = NazmContentActivity.this;

        scrollView = (ScrollView) findViewById(R.id.mScrollView);
        String jsonArray = getIntent().getStringExtra("dataArray");
        int postion = getIntent().getIntExtra("position", 0);

        try {
            dataArray = new JSONArray(jsonArray);
            data = new JSONObject(dataArray.getJSONObject(postion).toString());

            if (positionClicked != 0) {
                prevData = new JSONObject(dataArray.getJSONObject(positionClicked - 1).toString());
            } else {
                prevData = new JSONObject();
            }
            if (positionClicked == (dataArray.length() - 1)) {
                nextData = new JSONObject();

            } else {
                nextData = new JSONObject(dataArray.getJSONObject(positionClicked + 1).toString());
            }

            getNazmData();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void getNazmData() {
        try {

            // nazm_framelayout.setVisibility(View.VISIBLE);
            RestClient.get().getContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.languageCode, data.getString("I"), new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {

                    if (progressLoader != null && progressLoader.isShowing()) {
                        progressLoader.dismiss();
                    }
                    try {
                        JSONObject obj = new JSONObject(res);
                        if (obj.getInt("S") == 1) {
                            JSONObject obj1 = new JSONObject(res);
                            Log.e("res", " " + res);
                            System.out.print("=====================================================" + res);
                            //nazm_framelayout.setVisibility(View.GONE);
                            //textSixeToRender = 14.5f;
                            pumpData(obj1, CommonUtil.languageCode);
                            //calculateTextSize(obj1);
                            renderComplete = true;
                        } else {
                            Toast.makeText(context, "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                }

                @Override
                public void failure(RetrofitError error) {
                    if (progressLoader != null && progressLoader.isShowing()) {
                        progressLoader.dismiss();
                    }

                    Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pumpData(JSONObject obj, int languageCode) {

        JSONObject mainObj = null;
        try {
            mainObj = obj.getJSONObject("R");
            alignmenht = mainObj.getDouble("RA");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        vScrollWrapper = new ExScrollView(context);
        vScrollWrapper.setVerticalScrollBarEnabled(false);
        vScrollWrapper.setHorizontalScrollBarEnabled(false);

        vScrollWrapper.setFillViewport(true);
        //    vScrollWrapper.setScrollBarStyle(HorizontalScrollView.SCROLL_AXIS_NONE);
        vScrollWrapper.setLayoutParams(new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.WRAP_CONTENT));


        // vScrollWrapper.setBackgroundColor(Color.RED);
        linearWrapper = new LinearLayout(context);
        outerLinear = new RelativeLayout(context);
        outerLinear.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT));

        outerLinear.setPadding(8, 0, 8, 0);
       /* outerLinear.setBackgroundColor(Color.BLUE);*/
        // outerLinear.setBackgroundColor(Color.YELLOW);
        linearWrapper.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams55 = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        linearWrapper.setLayoutParams(layoutParams55);
        linearWrapper.setClipChildren(false);
        //linearWrapper.setHorizontalGravity(Gravity.CENTER);
        //  linearWrapper.setBackgroundColor(Color.GRAY);
        //linearWrapper.setPadding(0, 12, 0, 12);
        //  linearWrapper.setGravity(Gravity.FILL_HORIZONTAL);
        // linearWrapper.setBackgroundColor(Color.BLUE);


        RelativeLayout.LayoutParams RelativeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        if (alignmenht == 1.0) {
            RelativeLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            linearWrapper.setLayoutParams(RelativeLayoutParams);
            // outerLinear.addView(linearWrapper);
        } else if (alignmenht == 0.0) {
            RelativeLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
            linearWrapper.setLayoutParams(RelativeLayoutParams);
            //outerLinear.addView(linearWrapper);
        }

        int minGap = 3;
        JSONObject mainContent = null;
        JSONArray Parray = null;
        try {
            mainContent = new JSONObject(mainObj.getString("CR"));
            Parray = mainContent.getJSONArray("P");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        renderNazamNew(Parray, dim);

    }

    private void renderNazamNew(JSONArray Parray, LinearLayout.LayoutParams dim) {


        for (int i = 0; i < Parray.length(); i++) {

            JSONArray Larray = null;
            try {
                Larray = Parray.getJSONObject(i).getJSONArray("L");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int j = 0; j < Larray.length(); j++) {

                LinearLayout linear = new LinearLayout(context);
                LinearLayout.LayoutParams layoutParamsh = new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linear.setLayoutParams(layoutParamsh);
                linear.setOrientation(LinearLayout.HORIZONTAL);


                JSONArray Warray = null;
                try {
                    Warray = Larray.getJSONObject(j).getJSONArray("W");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                t = new TextView[Warray.length()];

                if (CommonUtil.languageCode != 3) {
                    // forceLTRIfSupported();
                    for (int k = 0; k < Warray.length(); k++) {

                        t[k] = new TextView(context);
                        t[k].setLayoutParams(dim);
                        t[k].setIncludeFontPadding(false);
                        // t[k].setGravity(Gravity.CENTER_HORIZONTAL);

                        if (k < Warray.length() - 1) {
                            // t[k].setPadding(0, 0, 0, 0);
                        }
                        try {
                            t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        t[k].setTextColor(Color.parseColor("#000000"));
                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            t[k].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        }*/
                        if (CommonUtil.languageCode == 1) {
                            t[k].setTypeface(engtf);
                            //t[k].setTextSize(14);
                            // CommonUtil.setEnglishMerriwetherFont(getActivity(), t[k]);

                        } else {
                            // t[k].setTextSize(20);
                            t[k].setTypeface(hinditf);
                            CommonUtil.setHindiLailaRegular(context, t[k]);
                        }

                        //   t[k].setGravity(Gravity.CENTER);

                        linear.addView(t[k]);

                    }
                    //linearWrapper.setGravity(Gravity.CENTER_HORIZONTAL);
                } else {
                    // forceRTLIfSupported();

                    for (int k = 0; k < Warray.length(); k++) {
                        t[k] = new TextView(context);
                        t[k].setLayoutParams(dim);
                        t[k].setIncludeFontPadding(false);
                        // t[k].setTextSize(14);
                        //  CommonUtil.setUrduNotoNataliq(getActivity(), t[k]);
                        try {
                            t[k].setText(" " + Warray.getJSONObject(k).getString("W"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (k < Warray.length() - 1) {
                            // t[k].setPadding(0, 0, minGap, 0);
                        }

                        t[k].setTextColor(Color.parseColor("#000000"));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            t[k].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                        }

                        t[k].setTypeface(urdutf);

                        linear.addView(t[k]);
                    }
                }
                if (j == Larray.length() - 1) {
                    linear.setPadding(0, 0, 0, 25);
                } else {
                    linear.setPadding(0, 4, 0, 0);
                }


                linearWrapper.addView(linear);

                /*linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                int widthSpec = View.MeasureSpec.makeMeasureSpec(linearWrapper.getWidth(), View.MeasureSpec.EXACTLY);
                int heightSpec = View.MeasureSpec.makeMeasureSpec(linearWrapper.getHeight(), View.MeasureSpec.EXACTLY);
                linear.measure(widthSpec, heightSpec);

                //linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);*/

                linear.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                linearWrapper.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            }
        }

        outerLinear.addView(linearWrapper);
        outerLinear.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        scrollView.addView(outerLinear);
        scrollView.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

}
