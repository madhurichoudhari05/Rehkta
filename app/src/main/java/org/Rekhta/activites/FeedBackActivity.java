package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import org.Rekhta.R;;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.RestClient;

import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FeedBackActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {

    public NavigationFragment drawerFragment;
    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    ImageView userProfileImg, feedbackCloseImg;
    TextView feedbackMsgTextCounter;
    String subjectFeedBack;
    EditText registeredUserName, registeredUserEmail;
    EditText userName, userEmail, userMsgFeddback;
    LinearLayout userDetailview, guestDetailView;
    Button feedbackSubmitBtn;
    TextView shareIdeaTxt;
    boolean isUserRegisterd = false;
    private Toolbar toolbar;
    HintSpinner<String> hintSpinner;
    private TextView idea, feedBack, suggations;
    String selectedType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);


        initViews();

        boolean isLoggedIn = DataPrefs.getBoolean("loggedIn", false);
        if (isLoggedIn) {
            registeredUserEmail.setVisibility(View.GONE);
        } else {
            registeredUserEmail.setVisibility(View.VISIBLE);
        }
        final AppCompatSpinner niceSpinner = (AppCompatSpinner) findViewById(R.id.niceSpinner);
        // niceSpinner.setItems("Ice Cream Sandwich", "Jelly Bean", "KitKat", "Lollipop", "Marshmallow");
        final List<String> dataset = new LinkedList<>(Arrays.asList("Ideas", "Feedback", "Suggestions"));


        //setupSpinner(niceSpinner,dataset);


        hintSpinner = new HintSpinner<>(
                niceSpinner,
                // Default layout - You don't need to pass in any layout id, just your hint text and
                // your list data
                new HintAdapter<>(this, R.string.spinner_promt, dataset),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {
                        // Here you handle the on item selected event (this skips the hint selected event)
                        if (position == 0) {
                            selectedType = "Ideas";
                        } else if (position == 1) {
                            selectedType = "Feedback";
                        } else if (position == 2) {
                            selectedType = "suggestions";
                        }
                        userMsgFeddback.requestFocus();
                    }
                });
        if (hintSpinner != null) {
            hintSpinner.init();
        }

    }

    void initViews() {

        userName = (EditText) findViewById(R.id.feedbackuserName);
        userEmail = (EditText) findViewById(R.id.feedbackuserEmail);
        userMsgFeddback = (EditText) findViewById(R.id.userFeedBack);
        userDetailview = (LinearLayout) findViewById(R.id.userDetailView);
        guestDetailView = (LinearLayout) findViewById(R.id.feedbackguestView);
        userProfileImg = (ImageView) findViewById(R.id.userProfileImg);
        registeredUserName = (EditText) findViewById(R.id.userNameFeedbackTV);
        registeredUserEmail = (EditText) findViewById(R.id.userEmailTV);
        feedbackMsgTextCounter = (TextView) findViewById(R.id.feedbackTextCounterTV);
        // shareIdeaTxt = (TextView) findViewById(R.id.shareIdeaTxt);
        feedbackCloseImg = (ImageView) findViewById(R.id.feedBackCloseImg);
        feedbackSubmitBtn = (Button) findViewById(R.id.submitFeedBackButton);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            feedbackSubmitBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_paperplane, 0, 0, 0);
        }

        feedbackSubmitBtn.setText(LanguageSelection.getSpecificLanguageWordById("SEND", CommonUtil.languageCode));


        feedbackSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isUserRegisterd) {
                    if (userMsgFeddback.getText().toString().length() != 0) {
                        submitFeedBack(registeredUserName.getText().toString(), CommonUtil.userEmail, userMsgFeddback.getText().toString());
                    } else {
                        Toast.makeText(FeedBackActivity.this, "comment cannot be blank", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (registeredUserName.length() == 0) {
                        Toast.makeText(FeedBackActivity.this, "Please enter username", Toast.LENGTH_SHORT).show();

                    } else if (registeredUserEmail.length() == 0) {
                        Toast.makeText(FeedBackActivity.this, "Please enter your email", Toast.LENGTH_SHORT).show();

                    } else if (!isEmailValid(registeredUserEmail.getText().toString())) {
                        Toast.makeText(FeedBackActivity.this, "Please Enter Valid Email Address", Toast.LENGTH_SHORT).show();

                    } else if (selectedType.equals("")) {
                        Toast.makeText(FeedBackActivity.this, "Please select a Feedback Option", Toast.LENGTH_SHORT).show();

                    } else if (userMsgFeddback.length() == 0) {
                        Toast.makeText(FeedBackActivity.this, "Please enter your feedback", Toast.LENGTH_SHORT).show();

                    } else {
                        submitFeedBack(userName.getText().toString(), userEmail.getText().toString(), userMsgFeddback.getText().toString());
                    }



                    /*if (registeredUserEmail.getText().toString().trim().length() == 0 || registeredUserName.getText().toString().trim().length() == 0
                            || userMsgFeddback.getText().toString().trim().length() == 0) {
                        Toast.makeText(FeedBackActivity.this, "Fields Cannnot be Empty", Toast.LENGTH_SHORT).show();
                    } else {
                        if (CommonUtil.isValidEmail(registeredUserName.getText())) {
                        } else {
                        }
                    }*/
                    //ShowThankYouForCritique();

                }

            }
        });

        feedbackCloseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (CommonUtil.isSkipped) {
            registeredUserName.setText("");
            registeredUserEmail.setText("");
            //findViewById(R.id.email_container).setVisibility(View.VISIBLE);
            isUserRegisterd = false;
        } else {
            isUserRegisterd = true;
            registeredUserName.setText(CommonUtil.userName);
            registeredUserName.setEnabled(false);
            // registeredUserEmail.setText(CommonUtil.userEmail);
            //findViewById(R.id.email_container).setVisibility(View.GONE);
            registeredUserEmail.setEnabled(false);
        }

//        if (isUserRegisterd) {
//            userDetailview.setVisibility(View.VISIBLE);
//            // guestDetailView.setVisibility(View.GONE);
//        } else {
//            //guestDetailView.setVisibility(View.VISIBLE);
//            userDetailview.setVisibility(View.GONE);
//        }

        userMsgFeddback.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //This sets a textview to the current length
                feedbackMsgTextCounter.setText(String.valueOf(s.length()) + "/360");
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void showFeedBackOption() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(FeedBackActivity.this);
        LayoutInflater layoutInflater = LayoutInflater.from(FeedBackActivity.this);
        View view = layoutInflater.inflate(R.layout.feedback_share_option, null, false);
        idea = (TextView) view.findViewById(R.id.ideaText);
        feedBack = (TextView) view.findViewById(R.id.feedbackTxt);
        suggations = (TextView) view.findViewById(R.id.suggationTxt);
        builder.setView(view);

        final AlertDialog alertDialog = builder.create();
        idea.setText(LanguageSelection.getSpecificLanguageWordById("Share your Ideas", CommonUtil.languageCode));
        feedBack.setText(LanguageSelection.getSpecificLanguageWordById("FEEDBACK", CommonUtil.languageCode));
        suggations.setText(LanguageSelection.getSpecificLanguageWordById("Suggestions", CommonUtil.languageCode));
        if (CommonUtil.languageCode == 1) {
            idea.setText("Idea");
        }
        if (CommonUtil.languageCode == 3) {
            idea.setGravity(Gravity.RIGHT);
            suggations.setGravity(Gravity.RIGHT);
            feedBack.setGravity(Gravity.RIGHT);

        }

        alertDialog.show();

        idea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                shareIdeaTxt.setText("Idea");
                shareIdeaTxt.setText(LanguageSelection.getSpecificLanguageWordById("Share your Ideas", CommonUtil.languageCode));
                subjectFeedBack = "Idea";
                if (CommonUtil.languageCode == 1) {
                    shareIdeaTxt.setText("Idea");
                }
                alertDialog.cancel();

            }
        });

        feedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                shareIdeaTxt.setText("Feedback");
                shareIdeaTxt.setText(LanguageSelection.getSpecificLanguageWordById("FEEDBACK", CommonUtil.languageCode));
                subjectFeedBack = "Feedback";
                alertDialog.cancel();
            }
        });

        suggations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareIdeaTxt.setText(LanguageSelection.getSpecificLanguageWordById("Suggestions", CommonUtil.languageCode));
                subjectFeedBack = "Suggestions";
                alertDialog.cancel();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(FeedBackActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(FeedBackActivity.this);

    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    public void submitFeedBack(String name, String email, String userFeedBack) {

   /*     {
            "name": "pradipta", "email": "pradiptasaha@polyplex.com", "contentId": NULL,
                "contentTitle": "Feedback Title", "pageType": "App-Feedback-Android", "subject": "test technical feedback for Android",
                "message": "App is very slow and feeds are not loaded while pull to refresh ", "typeOfQuery": 1, "pageUrl": "" }*/
        HashMap obj = new HashMap();
        try {

            obj.put("name", name);
            obj.put("email", email);
            obj.put("contentId", null);
            obj.put("contentTitle", "Feedback Title");
            obj.put("pageType", "App-Feedback-Android");
            obj.put("subject", selectedType);
            obj.put("message", userFeedBack);
            obj.put("typeOfQuery", 1);
            obj.put("pageUrl", "");
            //  Toast.makeText(getActivity(), "" + obj.toString(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final ProgressDialog loading = ProgressDialog.show(this, null, "Loading..", false, false);
        RestClient.get().submitCritique(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        userMsgFeddback.setText("");
                        ShowThankYouForCritique();
                    } else {
                        Toast.makeText(FeedBackActivity.this, "oops ! Error occured while submitting your request", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });


    }

    public void ShowThankYouForCritique() {

        final Dialog critiqueThankYouDialog = new Dialog(this, R.style.Theme_Dialog);
        critiqueThankYouDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        critiqueThankYouDialog.setContentView(R.layout.critique_thanks_dialog_template);
        critiqueThankYouDialog.setCanceledOnTouchOutside(false);
        TextView thankYouText = (TextView) critiqueThankYouDialog.findViewById(R.id.thanksCritiqueText);
        thankYouText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                critiqueThankYouDialog.dismiss();
            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        critiqueThankYouDialog.show();
        critiqueThankYouDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                critiqueThankYouDialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 2000);
    }


    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(FeedBackActivity.this, Dashboard.class));
        finish();
    }

    @Override
    public void langChanged() {
        feedbackSubmitBtn.setText(LanguageSelection.getSpecificLanguageWordById("SEND", CommonUtil.languageCode));
        shareIdeaTxt.setText(LanguageSelection.getSpecificLanguageWordById("Share your Ideas", CommonUtil.languageCode));
    }

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    }

    public void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyBoard();
        }
        return super.dispatchTouchEvent(ev);
    }

}
