package org.Rekhta.activites;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.Rekhta.R;
import org.Rekhta.adapters.AudioListAdapter;
import org.Rekhta.model.poetDetail.PoetAudioData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MediaPlayerService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AudioActivity extends AppCompatActivity implements Runnable,
        SeekBar.OnSeekBarChangeListener {

    JSONArray audioObj;
    ImageView playIamge;
    ProgressBar audioLoadingPbar;
    ListView audioListview;
    TextView currentAudioAuthorName, currentAudioTitle;
    ImageView spinner;
    ImageView currentAudioAuthor;
    RelativeLayout audioHeaderlayout;
    String currentPlayinAudio;


    ArrayList<PoetAudioData> arrayList;
    int pageIndex = 1;
    int totalNumberOfContent;
    boolean isDataLoading;
    ProgressBar audioProBar;
    JSONArray dataArray = new JSONArray();

    Context context;

    boolean isLoading = false;
    int clickedPosition;
    TextView poetAudioTitle, remiainTime, totalTime;
    ImageView closePlayerIcon;
    LinearLayout musicPlayerView;
    String poetId = "";
    //For music Player
    private SeekBar seekBar;
    private ImageView playBtn;
    private ImageView backwardBtn;
    private ImageView fwdBtn;
    private MediaPlayer mp;
    private ProgressDialog progressLoader;
    ProgressDialog audioDialog;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);


        context = AudioActivity.this;
        try {
            String audioArrray = getIntent().getStringExtra("audioArray");
            audioObj = new JSONArray(audioArrray);
        } catch (Exception e) {
        }
        // CommonUtil.isFromAudioTab = true;
        audioListview = (ListView) findViewById(R.id.audioListView);
        playIamge = (ImageView) findViewById(R.id.playButtonImg);
        audioLoadingPbar = (ProgressBar) findViewById(R.id.audioLoadingImg);
        audioHeaderlayout = (RelativeLayout) findViewById(R.id.audioTitleHeader);
        audioHeaderlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        currentAudioAuthorName = (TextView) findViewById(R.id.currentAudioAuthorName);
        currentAudioTitle = (TextView) findViewById(R.id.currentAudioTitle);

        initViews();
        currentAudioAuthor = (ImageView) findViewById(R.id.currentAudioAuthorImg);
        spinner = (ImageView) findViewById(R.id.spinner);
        spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioActivity.super.onBackPressed();
            }
        });
        AudioListAdapter audioListAdapter = new AudioListAdapter(AudioActivity.this, audioObj);
        audioListview.setAdapter(audioListAdapter);
        try {
            // playSelectedVideo(audioObj.getJSONObject(0).toString(), playIamge, audioLoadingPbar);
            playAudio(audioObj.getJSONObject(0).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        audioListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    position = i;
                    //  playSelectedVideo(audioObj.getJSONObject(i).toString(), playIamge, audioLoadingPbar);
                    playAudio(audioObj.getJSONObject(i).toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void initViews() {
        //  musicPlayerView = (LinearLayout) view.findViewById(R.id.musicPlayer);
        closePlayerIcon = (ImageView) findViewById(R.id.closePlayer);
        poetAudioTitle = (TextView) findViewById(R.id.audioTitle);
        seekBar = (SeekBar) findViewById(R.id.progessSeekBar);
        seekBar.setMax(99);

        musicPlayerView = (LinearLayout) findViewById(R.id.musicPlayer);
        playBtn = (ImageView) findViewById(R.id.poetDetailPlayBtn);
        backwardBtn = (ImageView) findViewById(R.id.poetDetailBkdBtn);
        fwdBtn = (ImageView) findViewById(R.id.poetDetailFwdBtn);

        remiainTime = (TextView) findViewById(R.id.remiainTime);
        totalTime = (TextView) findViewById(R.id.totalTime);

        seekBar.setOnSeekBarChangeListener(this);
        seekBar.setEnabled(false);
        // mp = new MediaPlayer();

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mp != null && mp.isPlaying()) {
                    mp.pause();
                    playBtn.setImageResource(R.drawable.ic_play);
                } else {
                    playBtn.setImageResource(R.drawable.ic_pause);
                    //playAudio();
                    mp.start();
                }

            }
        });

        backwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position > 0) {
                    position = position - 1;
                    // audioDialog.show();
                    try {
                        playAudio(audioObj.getJSONObject(position).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "No Previous audio available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position < audioObj.length() - 1) {
                    position = position + 1;
                    //audioDialog.show();
                    try {
                        playAudio(audioObj.getJSONObject(position).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "No Next Audio Available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        closePlayerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mp != null && mp.isPlaying() || mp.getDuration() > 0) {
                    mp.stop();
                    mp = null;
                    playBtn.setImageResource(R.drawable.ic_play);
                    seekBar.setProgress(0);

                }
                //musicPlayerView.setVisibility(View.GONE);
                AudioActivity.super.onBackPressed();
            }
        });
    }


    public void playSelectedVideo(String audioUrlObj, ImageView cPlayBtn, ProgressBar cProgressBar) {
        try {
            JSONObject audObj = new JSONObject(audioUrlObj);
            String audioUri = audObj.getString("AU");
            String audioTitle = audObj.toString();
            String autherName = audObj.getString("ASS");
            currentPlayinAudio = audObj.toString();
            currentAudioAuthorName.setText(audObj.getString("ASS").toUpperCase().replace("-", " "));
            currentAudioTitle.setText(audObj.getString("AT"));
            Glide.with(AudioActivity.this)
                    .load("https://rekhta.org" + audObj.getString("IU"))
                    .into(currentAudioAuthor);
            // ((GhazalActivity) getActivity()).isAudioPlaying = true;
            // MediaPlayerService.playAudio(audObj.getString("AU"), cPlayBtn, cProgressBar);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void playAudio(String audioUrlObj) {

        String title = null;
        String audioUrl = null;
        String length = null;
        Uri myUri = null;

        try {
            JSONObject audObj = new JSONObject(audioUrlObj);
            audioUrl = audObj.getString("AU");
            title = audObj.getString("ASS");

        } catch (JSONException e) {

        }


        playBtn.setImageResource(R.drawable.ic_pause);
        /*if (position < arrayList.size()) {
            if (CommonUtil.languageCode == 1) {
                title = arrayList.get(position).getAudioTitleInEng();
            } else if (CommonUtil.languageCode == 2) {
                title = arrayList.get(position).getAudioTitleInHin();
            } else if (CommonUtil.languageCode == 3) {
                title = arrayList.get(position).getAudioTitleInUrdu();
            }

            audioUrl = "https://rekhta.org/Images/SiteImages/Audio/" + arrayList.get(position).getId() + ".mp3";
            length = arrayList.get(position).getLength();
        }*/

        poetAudioTitle.setText(title);
        totalTime.setText(length);
        if (audioUrl != null) {
            myUri = Uri.parse(audioUrl);
        }


        if (mp == null) {
            try {
                //Uri myUri = Uri.parse("https://rekhta.org/Images/SiteImages/Audio/027FF6E1-4A2C-47D1-BAD2-5C1E24FCBE2C.mp3");

                mp = new MediaPlayer();
                mp.setDataSource(AudioActivity.this, myUri);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepare(); //don't use prepareAsync for mp3 playback
                mp.start();
                playBtn.setImageResource(R.drawable.ic_pause);

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            try {
                mp.stop();
                mp = null;
                mp = new MediaPlayer();
                mp.setDataSource(context, myUri);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);


                mp.prepare(); //don't use prepareAsync for mp3 playback
                mp.start();
                playBtn.setImageResource(R.drawable.ic_pause);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        //  audioBar.setVisibility(View.GONE);
        seekBar.setEnabled(true);
        seekBar.setMax(mp.getDuration());
        new Thread(this).start();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        try {
            if (mp.isPlaying() || mp != null) {
                if (fromUser)
                    mp.seekTo(progress);
            } else if (mp == null) {
                Toast.makeText(getApplicationContext(), "Media is not running",
                        Toast.LENGTH_SHORT).show();
                seekBar.setProgress(0);
            }
        } catch (Exception e) {
            Log.e("seek bar", "" + e);
            seekBar.setEnabled(false);

        }
        updatePlayer(progress, remiainTime);

    }

    private void updatePlayer(int currentDuration, TextView remiainTime) {

        remiainTime.setText("" + milliSecondsToTimer((long) currentDuration));
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mp != null && mp.isPlaying()) {
            mp.stop();
            mp.release();
            musicPlayerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void run() {

        if (mp != null) {
            int currentPosition = mp.getCurrentPosition();
            int total = mp.getDuration();

            while (mp != null && currentPosition < total) {
                try {
                    Thread.sleep(1000);
                    currentPosition = mp.getCurrentPosition();
                } catch (InterruptedException e) {
                    return;
                } catch (Exception e) {
                    return;
                }
                seekBar.setProgress(currentPosition);
            }
        }

    }
}
