package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;

import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.R;;
import org.Rekhta.adapters.dictionary.DictionaryImagesRecyclerAdapter;
import org.Rekhta.adapters.dictionary.DictionaryPlattsRecyclerAdapter;
import org.Rekhta.adapters.dictionary.DictionaryRecyclerAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.dictionary.DictionaryWordOfTheDayData;
import org.Rekhta.model.dictionary.ImageDictionaryData;
import org.Rekhta.model.dictionary.PlattsDictionaryData;
import org.Rekhta.model.dictionary.RekhtaDictionaryData;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DictionaryActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {

    public NavigationFragment drawerFragment;
    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    WindowManager.LayoutParams wlp;
    DictionaryWordOfTheDayData dictionaryWordOfTheDayData;
    ArrayList<RekhtaDictionaryData> arrayList;
    ArrayList<PlattsDictionaryData> arrayListPlatts;
    ArrayList<ImageDictionaryData> arrayListImage;
    DictionaryRecyclerAdapter dictionaryRecyclerAdapter;
    DictionaryPlattsRecyclerAdapter dictionaryPlattsRecyclerAdapter;
    DictionaryImagesRecyclerAdapter dictionaryImageRecyclerAdapter;
    JSONArray dataArray = new JSONArray();
    JSONArray dataArrayPlatts = new JSONArray();
    JSONArray dataArrayImage = new JSONArray();
    RecyclerView dictionaryRecyclerView;
    RecyclerView dictionaryPlattsRecyclerView;
    RecyclerView dictionaryImageRecyclerView;
    LinearLayout recyclerviewHolder;
    RelativeLayout defaultviewHolder;
    EditText dictionarySearchEdidtext;
    TextView serachKeywordTextview, dictionaryText;
    //word of the Day
    TextView wordOfTheDayMainText, wordOfTheDayHindiText, wordOfTheDayUrduText, wordOfTheContentText, wordOfDayMeaningText, wordOfDayPoetText;
    private Toolbar toolbar;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);


        arrayList = new ArrayList<>();
        arrayListPlatts = new ArrayList<>();
        arrayListImage = new ArrayList<>();
        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        dictionaryText = (TextView) toolbar.findViewById(R.id.dictionaryText);
        dictionaryRecyclerView = (RecyclerView) findViewById(R.id.dictionarRecyclerview);
        dictionaryPlattsRecyclerView = (RecyclerView) findViewById(R.id.dictionarPlattsRecyclerview);
        dictionaryImageRecyclerView = (RecyclerView) findViewById(R.id.dictionarImageRecyclerview);

        dictionaryRecyclerView.setNestedScrollingEnabled(false);
        dictionaryPlattsRecyclerView.setNestedScrollingEnabled(false);
        dictionaryImageRecyclerView.setNestedScrollingEnabled(false);

        recyclerviewHolder = (LinearLayout) findViewById(R.id.recyclerviewHolder);
        defaultviewHolder = (RelativeLayout) findViewById(R.id.defaultview);
        serachKeywordTextview = (TextView) findViewById(R.id.dictonarYShowingText);

        recyclerviewHolder.setVisibility(View.GONE);
        defaultviewHolder.setVisibility(View.VISIBLE);
        getWordOfTheDay();
        //adpterSetting
        dictionaryRecyclerAdapter = new DictionaryRecyclerAdapter(arrayList, this);
        dictionaryPlattsRecyclerAdapter = new DictionaryPlattsRecyclerAdapter(arrayListPlatts, this);
        dictionaryImageRecyclerAdapter = new DictionaryImagesRecyclerAdapter(arrayListImage, this);

        dictionaryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        dictionaryPlattsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        dictionaryImageRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        dictionaryRecyclerView.setAdapter(dictionaryRecyclerAdapter);
        dictionaryPlattsRecyclerView.setAdapter(dictionaryPlattsRecyclerAdapter);
        dictionaryImageRecyclerView.setAdapter(dictionaryImageRecyclerAdapter);
        dictionarySearchEdidtext = (EditText) findViewById(R.id.dictionary_serach_editText);
        dictionarySearchEdidtext.clearFocus();
        dictionarySearchEdidtext.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (dictionarySearchEdidtext.getText().toString().equalsIgnoreCase("") || dictionarySearchEdidtext.getText().toString().equalsIgnoreCase(" ")) {
                        Toast.makeText(DictionaryActivity.this, "Please enter text to search", Toast.LENGTH_SHORT).show();
                    } else {
                        getRekhtaDictionaryData(dictionarySearchEdidtext.getText().toString());
                        String text = "<font color=#000000>Showing results for </font> <font color=#00bfff>" + dictionarySearchEdidtext.getText().toString() + "</font>";
                        serachKeywordTextview.setText(Html.fromHtml(text));
                    }

                    return true;
                }
                return false;
            }
        });
        //wordOf the Day
        wordOfTheDayMainText = (TextView) findViewById(R.id.wordOftheDayMainText);
        wordOfTheDayHindiText = (TextView) findViewById(R.id.wordOftheDayHindiText);
        wordOfTheDayUrduText = (TextView) findViewById(R.id.wordOftheDayUrduText);
        wordOfTheContentText = (TextView) findViewById(R.id.wordOftheDayContentUsingtheWord);
        wordOfDayMeaningText = (TextView) findViewById(R.id.dictonarymeaningText);
        wordOfDayPoetText = (TextView) findViewById(R.id.wordOfDayPoetName);

   /*     dictionarySearchEdidtext.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    if (event.getRawX() >= (dictionarySearchEdidtext.getRight() - dictionarySearchEdidtext.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (dictionarySearchEdidtext.getText().toString().equalsIgnoreCase("") || dictionarySearchEdidtext.getText().toString().equalsIgnoreCase(" ")) {
                            Toast.makeText(DictionaryActivity.this, "Please enter text to search", Toast.LENGTH_SHORT).show();
                        } else {
                            getRekhtaDictionaryData(dictionarySearchEdidtext.getText().toString());
                            String text = "<font color=#000000>Showing results for </font> <font color=#00bfff>"+ dictionarySearchEdidtext.getText().toString()+"</font>";
                            serachKeywordTextview.setText(Html.fromHtml(text));
                        }

                        return true;
                    }
                }
                return false;
            }
        });*/
        changWordsAccdTolang();

    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(DictionaryActivity.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(DictionaryActivity.this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        initRightMenuPopUpDialog();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(DictionaryActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DictionaryActivity.this, MyFavoritesActivity.class));
            }
        });

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                drawerFragment.resetButtonColor(1);
                dialog.dismiss();

            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                drawerFragment.resetButtonColor(2);
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                drawerFragment.resetButtonColor(3);
                dialog.dismiss();

            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);
        initTheViewAccordingToLang();
        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {

        startActivity(new Intent(DictionaryActivity.this, Dashboard.class));
        finish();
    }

    @Override
    public void langChanged() {
        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {
            forceLTRIfSupported();
        }
/*        changWordsAccdTolang();
        getRekhtaDictionaryData();
        getPlattsDictionaryData();
        getImagePhysicalDictionaryData()*/

    }

    public void getRekhtaDictionaryData(final String dataToSearch) {
        recyclerviewHolder.setVisibility(View.VISIBLE);
        defaultviewHolder.setVisibility(View.GONE);
        arrayList.clear();
        final ProgressDialog loading = ProgressDialog.show(this, null, "Loading..", false, false);
        RestClient.get().getRekhtaDictionaryData(CommonUtil.SharedPrefKeys.contentTypeMultipart, CommonUtil.SharedPrefKeys.UniqueId, dataToSearch, "Bearer JeEz16biJ4AN9zAl-rr0I3GEa6ZXqMuwmL8ehPDIeVE_Exlm2HdJ-NNBO1yns2Qe2EGPxEEEbHUjsgtOZ8zjpeAP3tcZ8dCTQLbYJBYfts_rmUL5bQC3zHaDZtudOXIdorLPPQvZ1Z_3PFBELFiz4My16rc8rLrdcLe2gclt-hcCoO3hipIZ4EA0EOqigcMLaAJcG0JT_ixGh1oZ4HL-GZmlQmEiPLSgp5WHMY8NILyHCSPj1LnD1RkKBuSfcg1m", new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                loading.dismiss();
                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {

                        CommonUtil.serachedText = dataToSearch;
                        dataArray = obj.getJSONArray("R");
                        for (int i = 0; i < dataArray.length(); i++) {
                            arrayList.add(new RekhtaDictionaryData(dataArray.getJSONObject(i).getString("Id"),
                                    dataArray.getJSONObject(i).getString("English"), dataArray.getJSONObject(i).getString("Hindi"), dataArray.getJSONObject(i).getString("Urdu"),
                                    dataArray.getJSONObject(i).getString("Meaning1_En"), dataArray.getJSONObject(i).getString("Meaning2_En"), dataArray.getJSONObject(i).getString("Meaning3_En"),
                                    dataArray.getJSONObject(i).getString("Meaning1_Hi"), dataArray.getJSONObject(i).getString("Meaning2_Hi"), dataArray.getJSONObject(i).getString("Meaning3_Hi"),
                                    dataArray.getJSONObject(i).getString("Meaning1_Ur"), dataArray.getJSONObject(i).getString("Meaning2_Ur"), dataArray.getJSONObject(i).getString("Meaning3_Ur")));
                        }
                        dictionaryRecyclerAdapter.notifyDataSetChanged();

                        getPlattsDictionaryData(dataToSearch);
                    } else {
                        recyclerviewHolder.setVisibility(View.GONE);
                        defaultviewHolder.setVisibility(View.VISIBLE);
                        Toast.makeText(DictionaryActivity.this, "Error in getting Meaning", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
            }
        });
    }


    public void getWordOfTheDay() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
        String formattedDate = df.format(c.getTime());

        final ProgressDialog loading = ProgressDialog.show(this, null, "Loading..", false, false);
        RestClient.get().getWordOfTheDay(CommonUtil.SharedPrefKeys.contentTypeMultipart, CommonUtil.SharedPrefKeys.UniqueId, formattedDate, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        JSONObject content = obj.getJSONObject("R");

                        dictionaryWordOfTheDayData = new DictionaryWordOfTheDayData(content.getString("I"),
                                content.getString("WE"), content.getString("WH"), content.getString("WU"),
                                content.getString("ME"), content.getString("MH"), content.getString("MU"),
                                content.getString("NE"), content.getString("NH"), content.getString("NU"),
                                content.getString("CE"), content.getString("CH"), content.getString("CU"));
                        initTheViewAccordingToLang();

                    } else {
                        Toast.makeText(DictionaryActivity.this, "Error in getting Word Of the Day", Toast.LENGTH_SHORT).show();
                    }
                    loading.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                loading.dismiss();
                Toast.makeText(DictionaryActivity.this, "Please check your connection and try again", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void initTheViewAccordingToLang() {
        wordOfTheDayMainText.setText(dictionaryWordOfTheDayData.getWordInEng());
        wordOfTheDayHindiText.setText(dictionaryWordOfTheDayData.getWordInHin());
        wordOfTheDayUrduText.setText(dictionaryWordOfTheDayData.getWordInUrdu());
        if (CommonUtil.languageCode == 1) {
            wordOfTheContentText.setText(dictionaryWordOfTheDayData.getWordUsedinContentEng());
            wordOfDayPoetText.setText(Html.fromHtml(dictionaryWordOfTheDayData.getWordsPoetNameInEng()));
            wordOfDayMeaningText.setText(dictionaryWordOfTheDayData.getMeaningInEng());
            CommonUtil.setEnglishMerriwetherItalicFont(this, wordOfTheContentText);
            CommonUtil.setEnglishMerriwetherFont(this, wordOfDayPoetText);
            CommonUtil.setEnglishLatoBoldFont(this, wordOfDayMeaningText);
        } else if (CommonUtil.languageCode == 2) {
            wordOfTheContentText.setText(dictionaryWordOfTheDayData.getWordUsedinContentHin());
            wordOfDayPoetText.setText(Html.fromHtml(dictionaryWordOfTheDayData.getWordsPoetNameInHin()));
            wordOfDayMeaningText.setText(dictionaryWordOfTheDayData.getMeaningInHin());
            CommonUtil.setHindiFont(this, wordOfTheContentText);
            CommonUtil.setHindiFont(this, wordOfDayPoetText);
            CommonUtil.setHindiFont(this, wordOfDayMeaningText);

        } else {
            wordOfTheContentText.setText(dictionaryWordOfTheDayData.getWordUsedinContentUrdu());
            wordOfDayPoetText.setText(Html.fromHtml(dictionaryWordOfTheDayData.getWordsPoetNameInUrdu()));
            wordOfDayMeaningText.setText(dictionaryWordOfTheDayData.getMeaningInUrdu());
            CommonUtil.setUrduFont(this, wordOfTheContentText);
            CommonUtil.setUrduFont(this, wordOfDayPoetText);
            CommonUtil.setUrduFont(this, wordOfDayMeaningText);
        }

    }

    public void getPlattsDictionaryData(final String dataToSearch) {
        arrayListPlatts.clear();
        RestClient.get().getPlattsDictionaryData(CommonUtil.SharedPrefKeys.contentTypeMultipart, CommonUtil.SharedPrefKeys.UniqueId, dataToSearch, "Bearer JeEz16biJ4AN9zAl-rr0I3GEa6ZXqMuwmL8ehPDIeVE_Exlm2HdJ-NNBO1yns2Qe2EGPxEEEbHUjsgtOZ8zjpeAP3tcZ8dCTQLbYJBYfts_rmUL5bQC3zHaDZtudOXIdorLPPQvZ1Z_3PFBELFiz4My16rc8rLrdcLe2gclt-hcCoO3hipIZ4EA0EOqigcMLaAJcG0JT_ixGh1oZ4HL-GZmlQmEiPLSgp5WHMY8NILyHCSPj1LnD1RkKBuSfcg1m", new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {

                        dataArrayPlatts = obj.getJSONArray("R");
                        for (int i = 0; i < dataArrayPlatts.length(); i++) {
                            arrayListPlatts.add(new PlattsDictionaryData(dataArrayPlatts.getJSONObject(i).getString("Id"),
                                    dataArrayPlatts.getJSONObject(i).getString("English"), dataArrayPlatts.getJSONObject(i).getString("Hindi"), dataArrayPlatts.getJSONObject(i).getString("Urdu"),
                                    dataArrayPlatts.getJSONObject(i).getString("EnTr"), dataArrayPlatts.getJSONObject(i).getString("Html"), dataArrayPlatts.getJSONObject(i).getString("Origin")));
                        }
                        dictionaryPlattsRecyclerAdapter.notifyDataSetChanged();
                        getImagePhysicalDictionaryData(dataToSearch);
                    } else {
                        Toast.makeText(DictionaryActivity.this, "Error in getting Meaning", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getImagePhysicalDictionaryData(final String dataToSearch) {

        arrayListImage.clear();
        RestClient.get().getImageDictionaryData(CommonUtil.SharedPrefKeys.contentTypeMultipart, CommonUtil.SharedPrefKeys.UniqueId, dataToSearch, "Bearer JeEz16biJ4AN9zAl-rr0I3GEa6ZXqMuwmL8ehPDIeVE_Exlm2HdJ-NNBO1yns2Qe2EGPxEEEbHUjsgtOZ8zjpeAP3tcZ8dCTQLbYJBYfts_rmUL5bQC3zHaDZtudOXIdorLPPQvZ1Z_3PFBELFiz4My16rc8rLrdcLe2gclt-hcCoO3hipIZ4EA0EOqigcMLaAJcG0JT_ixGh1oZ4HL-GZmlQmEiPLSgp5WHMY8NILyHCSPj1LnD1RkKBuSfcg1m", new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {

                JSONObject obj = new JSONObject(res);
                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {

                        dataArrayImage = obj.getJSONObject("R").getJSONArray("ImageDictionary");
                        for (int i = 0; i < dataArrayImage.length(); i++) {

                            arrayListImage.add(new ImageDictionaryData(dataArrayImage.getJSONObject(i).getString("EnglishWord"),
                                    dataArrayImage.getJSONObject(i).getString("HindiWord"), dataArrayImage.getJSONObject(i).getString("UrduWord"), dataArrayImage.getJSONObject(i).getJSONArray("ImgDictionaryEntries")));
                        }
                        dictionaryImageRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(DictionaryActivity.this, "Error in getting Meaning", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    public void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyBoard();
        }
        return super.dispatchTouchEvent(ev);
    }

    public void changWordsAccdTolang() {
        CommonUtil.setSpecificLanguage(dictionaryText, "dictionary", CommonUtil.languageCode);
    }
}
