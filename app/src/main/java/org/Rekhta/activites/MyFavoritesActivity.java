package org.Rekhta.activites;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.adapters.myFavorite.HistryAdapter;
import org.Rekhta.utils.LanguageSelection;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.Rekhta.R;;
import org.Rekhta.adapters.MyFavoriteAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.interfaces.MyFavouriteInterface;
import org.Rekhta.model.ContentRPojo;
import org.Rekhta.model.ContentTypePojo;
import org.Rekhta.model.myFavorite.FavFCModel;
import org.Rekhta.model.myFavorite.FavRPojo;
import org.Rekhta.model.myFavorite.OnlineFavModelMain;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyFavoritesActivity extends AppCompatActivity implements
        NavigationFragment.NavigationDrawerCallbacks, View.OnClickListener, MyFavouriteInterface {

    public NavigationFragment drawerFragment;
    ViewPager viewPager;
    TabLayout tabLayout;
    Dialog dialog;
    WindowManager.LayoutParams wlp;
    TextView backButton;
    ImageView search, rignMenu;
    boolean isOffline;
    JSONObject jsonObject;


    SharedPreferences DataPrefs = null;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    ContentTypePojo contentTypePojo;
    private Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private MyFavoriteAdapter myFavoriteAdapter;
    public HistryAdapter histryAdapter;
    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;

    protected void onCreate(Bundle savedInsetanceState) {
        super.onCreate(savedInsetanceState);
        setContentView(R.layout.activity_my_favorites);
        // toolbar=(Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        backButton = findViewById(R.id.backButton);
        search = (ImageView) findViewById(R.id.search);
        rignMenu = (ImageView) findViewById(R.id.trippleDot);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        backButton.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(MyFavoritesActivity.this, R.drawable.ic_back_pre_lollipop), null, null, null);

        requestPermission();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!CommonUtil.isOffline) {
            CommonUtil.registerReciver(MyFavoritesActivity.this);
        }


    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!CommonUtil.isOffline) {
            CommonUtil.unregisterReciebr(MyFavoritesActivity.this);
        }


    }


    private void checkTypeId(String typeId, FavFCModel favFCModel) {

        String typeName = null;

        String S = contentTypePojo.getS();
        String Me = contentTypePojo.getMe();
        String Mh = contentTypePojo.getMh();
        String Mu = contentTypePojo.getMu();
        List<ContentRPojo> contentRPojo = contentTypePojo.getR();

        for (int i = 0; i < contentRPojo.size(); i++) {

            String id = contentRPojo.get(i).getI();
            if (id.equals(typeId)) {
                if (CommonUtil.languageCode == 1) {
                    typeName = contentRPojo.get(i).getNE();
                } else if (CommonUtil.languageCode == 2) {

                    typeName = contentRPojo.get(i).getNH();
                }
                if (CommonUtil.languageCode == 3) {
                    typeName = contentRPojo.get(i).getNU();
                }

                int f = R.string.ghazal;
                if (typeName.equals(String.valueOf(R.string.ghazal))) {
                    //favGhazalsList.add(favFCModel);
                } else if (typeName.equals(String.valueOf(R.string.nazam))) {
                    //favNazamsList.add(favFCModel);
                } else if (typeName.equals(String.valueOf(R.string.sher))) {
                    //favSherList.add(favFCModel);
                }
            }


        }
    }


    @Override
    public void goToHomeScreen() {


    }

    @Override
    public void langChanged() {

        if (CommonUtil.languageCode == 3) {
            histryAdapter.notifyDataSetChanged();

        } else if (CommonUtil.languageCode == 2) {
            forceLTRIfSupported();
            histryAdapter.notifyDataSetChanged();

        } else {
            forceLTRIfSupported();
            histryAdapter.notifyDataSetChanged();


        }
        setTabNames();
        sendReloadBroadCast();
    }

    private void setTabNames() {

        tabLayout.getTabAt(0).setText(LanguageSelection.getSpecificLanguageWordById("My Collections", CommonUtil.languageCode));
        tabLayout.getTabAt(1).setText(LanguageSelection.getSpecificLanguageWordById("History", CommonUtil.languageCode));
        tabLayout.getTabAt(2).setText(LanguageSelection.getSpecificLanguageWordById("images", CommonUtil.languageCode));
    }

    private void sendReloadBroadCast() {

        Intent intentFilter = new Intent("ReloadFav");
        LocalBroadcastManager.getInstance(MyFavoritesActivity.this).sendBroadcast(intentFilter);
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                dialog.dismiss();
            }
        });

    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }

        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search) {
            startActivity(new Intent(this, SearchActivity.class));
        /*    Intent intent = new Intent(this, PoetDetailedActivity.class);
            intent.putExtra("poetId","EB9CB3D1-1DBD-4582-A1CA-E48FDAFE5BC8");
            intent.putExtra("ghazalCount",4+"");
            intent.putExtra("nazmCount",5+"");
            intent.putExtra("sherCount",7+"");

            this.startActivity(intent);*/
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.backButton:
                onBackPressed();
                break;

            case R.id.trippleDot:

                showRightMenuLangSelectionOption();

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            // restartActivity();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //restartActivity();
        }
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    @Override
    public void provideSpecifcFavData() {


    }

    public void requestPermission() {

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MyFavoritesActivity.this,
                Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MyFavoritesActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MyFavoritesActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
           // Toast.makeText(MyFavoritesActivity.this, "Storage permission granted", Toast.LENGTH_LONG).show();
            setActivityFragments();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 100: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    setActivityFragments();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MyFavoritesActivity.this, "Please Allow Rekhta to Access Storage", Toast.LENGTH_LONG).show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void setActivityFragments() {

        sharedPreferences = this.getSharedPreferences("MyPref", 0);

        contentTypePojo = CommonUtil.contentTypePojo;

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);

        myFavoriteAdapter = new MyFavoriteAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myFavoriteAdapter);
        tabLayout.setupWithViewPager(viewPager);
        backButton.setOnClickListener(this);
        search.setOnClickListener(this);
        rignMenu.setOnClickListener(this);

        isOffline = sharedPreferences.getBoolean("isOffline", false);


        initRightMenuPopUpDialog();
    }
}
