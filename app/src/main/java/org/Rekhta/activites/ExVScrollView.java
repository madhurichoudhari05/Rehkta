package org.Rekhta.activites;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;



public class ExVScrollView extends ScrollView {

    // true if we can scroll the ScrollView
    // false if we cannot scroll
    private boolean enableScrolling = false;
    public ExVScrollView(Context context) {
        super(context);
    }

    public ExVScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExVScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // if we can scroll pass the event to the superclass
                if (scrollingEnabled()) return super.onTouchEvent(ev);
                // only continue to handle the touch event if scrolling enabled
                return scrollingEnabled(); // scrollable is always false at this point
            default:
                return super.onTouchEvent(ev);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if (scrollingEnabled()) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private boolean scrollingEnabled(){
        return enableScrolling;
    }
    public void setScrolling(boolean enableScrolling) {
        this.enableScrolling = enableScrolling;
    }

}