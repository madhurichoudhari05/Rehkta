package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.speech.RecognizerIntent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import org.Rekhta.R;
import org.Rekhta.adapters.SearchAdapter.SearchViewPagerAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.fragments.SearchFragment.SearchGhazalContentRendering;
import org.Rekhta.fragments.SearchFragment.SearchNazmContentRendering;
import org.Rekhta.fragments.nazmFragments.NazmContent;
import org.Rekhta.fragments.sherFragments.SherGhalContentFragment;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

public class SearchActivity extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {

    private ImageView micButton;
    private ImageView backButton;
    private ImageView rightMenu;
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
    private EditText searchEdit;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private SearchViewPagerAdapter searchViewPagerAdapter;
    public String searchedText = "";
    public String idGhazalToRender;
    public String currentGhazalPoetName;
    public String currentNazmPoetName;
    public String idNazmToRender;
    public boolean islangChanged = false;
    //right menu declaration

    private Toolbar toolbar;
    public NavigationFragment drawerFragment;
    private Button rightMenu_engBtn;
    private Button rightMenu_hinBtn;
    private Button rightMenu_urduBtn;
    private Dialog dialog;
    private SharedPreferences DataPrefs = null;
    private TextView rightMenuname;
    private TextView rightMenuEmail;
    private TextView rightMenuMyFavorates;
    private TextView rightMenuSettings;
    private WindowManager.LayoutParams wlp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        micButton = findViewById(R.id.mic_btn);
        searchEdit = (EditText) findViewById(R.id.search_editbox);
        searchEdit.clearFocus();
        viewPager = findViewById(R.id.search_viewpager);
        viewPager.setOffscreenPageLimit(1);
        tabLayout = findViewById(R.id.tabbar);
        tabLayout.setVisibility(View.GONE);


        int orentaiont = this.getResources().getConfiguration().orientation;
        if (orentaiont == 2) {
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);

        } else if (orentaiont == 1) {

            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

        searchViewPagerAdapter = new SearchViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(searchViewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceRecognitionActivity();

            }
        });
        backButton = findViewById(R.id.back_btn);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rightMenu = findViewById(R.id.menu_btn);
        rightMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRightMenuLangSelectionOption();
            }
        });

        if (CommonUtil.languageCode == 1) {
            forceLTRIfSupported();
        } else if (CommonUtil.languageCode == 2) {
            forceLTRIfSupported();
        } else if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();

        }
        setEditSearch();

        initRightMenuPopUpDialog();
        searchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // Toast.makeText(SearchActivity.this, "in Serach out", Toast.LENGTH_SHORT).show();
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Toast.makeText(SearchActivity.this, "in Serach", Toast.LENGTH_SHORT).show();
                    if (searchViewPagerAdapter != null) {
                        searchedText = searchEdit.getText().toString();
                        hideKeyBoard();
                        searchViewPagerAdapter.notifyDataSetChanged();
                        tabLayout.setVisibility(View.VISIBLE);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void setEditSearch() {

        if (CommonUtil.languageCode == 3) {
            backButton.setImageResource(R.drawable.urdu_back);

        } else {
            backButton.setImageResource(R.drawable.ic_back_pre_lollipop);
        }
        searchEdit.setHint(LanguageSelection.getSpecificLanguageWordById("Search", CommonUtil.languageCode));

    }

    @Override
    protected void onStart() {
        super.onStart();

        CommonUtil.registerReciver(SearchActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(SearchActivity.this);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int orentaiont = this.getResources().getConfiguration().orientation;
        if (orentaiont == 2) {
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);

        } else if (orentaiont == 1) {

            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }

    public void informationMenu() {
        startActivity(new Intent("android.intent.action.INFOSCREEN"));
    }

    private void startVoiceRecognitionActivity() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Speech recognition");
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            // Fill the list view with the strings the recognizer thought it
            // could have heard
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            searchEdit.setText(matches.get(0));
            searchedText = matches.get(0);
            if (searchViewPagerAdapter != null) {
                searchViewPagerAdapter.notifyDataSetChanged();
                tabLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    public void removeFragment() {
        getFragmentManager().popBackStack();
        startFragment();
    }

    private void startFragment() {
        SearchGhazalContentRendering searchGhazalContentRendering = new SearchGhazalContentRendering();
//                    ghazalDetails.setArguments(bundle);
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.searchActivity_mainView, searchGhazalContentRendering, "searchGhazalContentRendering")
                //.addToBackStack(null)
                .commit();
    }

    public void removeNazmFragment() {
        getFragmentManager().popBackStack();
        startNazmFragment();
    }

    private void startNazmFragment() {
        SearchNazmContentRendering searchNazmContentRendering = new SearchNazmContentRendering();
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.searchActivity_mainView, searchNazmContentRendering, "searchNazmContentRendering")
                //.addToBackStack(null)
                .commit();
    }

    private void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.END;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        // for view selection rightMenuMyFavorates , rightMenuSettings;
        rightMenuname = dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = dialog.findViewById(R.id.right_menu_setting);


        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SearchActivity.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SearchActivity.this, MyFavoritesActivity.class));
            }
        });


        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);

        rightMenu_engBtn = dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                // forceLTRIfSupported();
                langChanged();
                dialog.dismiss();
            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                //forceLTRIfSupported();
                langChanged();
                dialog.dismiss();

            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                //forceRTLIfSupported();
                langChanged();
                dialog.dismiss();

            }
        });

    }

    private void resetActionBarPopUpViewLangButtonColor(int langCode) {
        CommonUtil.languageCode = langCode;
        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();

        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            // restartActivity();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //restartActivity();
        }
    }

    private void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    private void showRightMenuLangSelectionOption() {

        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.END;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.START;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {

    }

    @Override
    public void langChanged() {

        searchViewPagerAdapter.notifyDataSetChanged();
        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {

            forceLTRIfSupported();
        }

        setEditSearch();

        sendLocalSearchBroadCast();

    }

    private void sendLocalSearchBroadCast() {

        Intent intentFilter = new Intent("searchBroadcast");
        LocalBroadcastManager.getInstance(SearchActivity.this).sendBroadcast(intentFilter);
    }

    private void changeTabAccLang() {


    }

    @Override
    public void onBackPressed() {

        searchViewPagerAdapter.notifyDataSetChanged();
        if (CommonUtil.languageCode == 3) {
            forceRTLIfSupported();
        } else {

            forceLTRIfSupported();
        }

        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0) {

            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            sendLocalSearchBroadCast();

          //  super.onBackPressed();

        } else {

            getFragmentManager().popBackStack();
        }

        setEditSearch();


    }

    private void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}


