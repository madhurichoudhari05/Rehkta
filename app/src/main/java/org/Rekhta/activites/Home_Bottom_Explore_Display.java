package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.Rekhta.R;
import org.Rekhta.adapters.SherCollectionsAdapter;
import org.Rekhta.fragments.sherFragments.CollectionSherView;
import org.Rekhta.model.SherCollection;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Home_Bottom_Explore_Display extends AppCompatActivity {
    RecyclerView sherRecyclerView;
    SherCollectionsAdapter sherCollectionsAdapter;
    ArrayList<SherCollection> arrayList;
    String contentId = "";
    //----
    Toolbar toolbar;
    TextView toolbarTV;
    ImageView backButton;
    Dialog dialog;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    TextView rightMenuname, rightMenuEmail, rightMenuMyFavorates, rightMenuSettings;
    SharedPreferences DataPrefs = null;
    WindowManager.LayoutParams wlp;
    int pageIndex = 1;
    JSONArray dataArray = new JSONArray();
    int totalNumberOfContent;
    boolean isDataLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home__bottom__explore__display);
        Intent i = getIntent();

        contentId = i.getExtras().getString("sher_Id");


        toolbar = (Toolbar) findViewById(R.id.toolbarfrag);
        toolbar.inflateMenu(R.menu.home_screen);
        toolbarTV = (TextView) toolbar.findViewById(R.id.toobartextview);
        CommonUtil.setSpecificLanguage(toolbarTV, "sher", CommonUtil.languageCode);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_settings:
                        showRightMenuLangSelectionOption();
                        return true;
                    case R.id.search:
                        // Toast.makeText(Home_Bottom_Explore_Display.this, "searching", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Home_Bottom_Explore_Display.this, SearchActivity.class));
                        return true;
                    default:

                }
                return false;
            }
        });

        sherRecyclerView = (RecyclerView) findViewById(R.id.sherRecycler);
        arrayList = new ArrayList<>();
       // getTop20SelectedSher();
        //sherCollectionsAdapter = new SherCollectionsAdapter(arrayList, Home_Bottom_Explore_Display.this, CollectionSherView.this);
       // sherRecyclerView.setLayoutManager(new LinearLayoutManager(Home_Bottom_Explore_Display.this, LinearLayoutManager.VERTICAL, false));
        sherRecyclerView.setAdapter(sherCollectionsAdapter);
        initRightMenuPopUpDialog();
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        sherRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    //Toast.makeText(Home_Bottom_Explore_Display.this,"LAst",Toast.LENGTH_LONG).show();
                    Log.e("_____", "" + totalNumberOfContent);
                    CommonUtil.cSherTotalCount = totalNumberOfContent + "";
                    Log.e("_____", "" + CommonUtil.calulateTotalApiCall(totalNumberOfContent));
                    if (totalNumberOfContent <= 20) {

                    } else {
                        if (pageIndex < CommonUtil.calulateTotalApiCall(totalNumberOfContent) && !isDataLoading) {
                            pageIndex = pageIndex + 1;
                            getTop20SelectedSher();
                        }
                    }
                }
            }
        });


    }



    private void colllist() {
        arrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            // arrayList.add(new SherCollection("Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry " + i));
        }
    }

    public void getTop20SelectedSher() {
//        Home_Bottom_Explore_Display.this.setDrawerEnabled(false); // for disabling the Navigation Bar to Open Here
        isDataLoading = true;

        final ProgressDialog progressLoader = ProgressDialog.show(Home_Bottom_Explore_Display.this, null, "Please wait...", false, false);

        RestClient.get().getSlectedSherCollectionData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, "", "F722D5DC-45DA-41EC-A439-900DF702A3D6", contentId, "", pageIndex, new Callback<HashMap>() {
                    @Override
                    public void success(HashMap res, Response response) {
                        if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                        try {

                            JSONObject content = new JSONObject(res);
                            if (content.getInt("S") == 1) {
                                totalNumberOfContent = content.getJSONObject("R").getInt("TC");
                                /*dataArray = content.getJSONObject("R").getJSONArray("CD");*/
                                CommonUtil.cSherTotalCount = totalNumberOfContent + "";
                                arrayList.clear();
                                dataArray = CommonUtil.concatArray(dataArray, content.getJSONObject("R").getJSONArray("CD"));

                                for (int i = 0; i < dataArray.length(); i++) {

                                   /* arrayList.add(new SherCollection(dataArray.getJSONObject(i).getString("RE"), dataArray.getJSONObject(i).getString("RH"),
                                            dataArray.getJSONObject(i).getString("RU"), dataArray.getJSONObject(i).getString("PE"), dataArray.getJSONObject(i).getString("PH"),
                                            dataArray.getJSONObject(i).getString("PU"), dataArray.getJSONObject(i).getString("I"), dataArray.getJSONObject(i).getString("UE")
                                            , dataArray.getJSONObject(i).getString("UH"), dataArray.getJSONObject(i).getString("UU"), dataArray.getJSONObject(i).getJSONArray("TS"), dataArray.getJSONObject(i).getString("P")));*/
                                }

                                sherCollectionsAdapter.notifyDataSetChanged();
                                isDataLoading = false;
                            } else {

                                Toast.makeText(Home_Bottom_Explore_Display.this, "Oops an error occured", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                        Toast.makeText(Home_Bottom_Explore_Display.this, "failed Getting Data", Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);
        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }


    public void resetActionBarPopUpViewLangButtonColor(int langCode) {

        CommonUtil.setSpecificLanguage(toolbarTV, "sher", langCode);

        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);
        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }

        if (CommonUtil.languageCode != langCode) {
            CommonUtil.languageCode = langCode;
            saveTheLangCodeLocally();
        }

        dialog.dismiss();
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void initRightMenuPopUpDialog() {
        dialog = new Dialog(new ContextThemeWrapper(Home_Bottom_Explore_Display.this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        rightMenuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenuname.setText(CommonUtil.userName);
        rightMenuEmail.setText(CommonUtil.userEmail);
        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(1);
                sherCollectionsAdapter.notifyDataSetChanged();
                dialog.dismiss();
                forceLTRIfSupported();


            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                dialog.dismiss();
                sherCollectionsAdapter.notifyDataSetChanged();
                forceLTRIfSupported();
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                dialog.dismiss();
                sherCollectionsAdapter.notifyDataSetChanged();
                forceRTLIfSupported();

            }
        });
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }
}