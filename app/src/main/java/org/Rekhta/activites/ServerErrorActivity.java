package org.Rekhta.activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import org.Rekhta.R;
import org.Rekhta.utils.CommonUtil;;

public class ServerErrorActivity extends AppCompatActivity {


    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_error);

        sharedPreferences = this.getSharedPreferences("MyPref", 0);

        final ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);


        TextView textView = (TextView) findViewById(R.id.retryText);
        TextView browseOffTxt = (TextView) findViewById(R.id.browseOffTxt);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                if (networkInfo != null) {
                    CommonUtil.isOffline = true;
                    startActivity(new Intent(ServerErrorActivity.this, Dashboard.class));
                    finish();
                } else {
                    Toast.makeText(ServerErrorActivity.this, "Please connect to internet", Toast.LENGTH_LONG).show();
                }
            }
        });

        browseOffTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("isOffline", true);
                editor.commit();
                startActivity(new Intent(ServerErrorActivity.this, MyFavoritesActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
