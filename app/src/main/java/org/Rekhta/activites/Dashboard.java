package org.Rekhta.activites;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import org.Rekhta.Broadcast.ConnectivityRecevier;
import org.Rekhta.R;
import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.model.LocalModels.AddSher;
import org.Rekhta.utils.AutoViewPager1;
import org.Rekhta.utils.LanguageSelection;
import org.Rekhta.utils.MViewPager;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.PlayGifView;
import org.Rekhta.views.MeaningFragment;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.Rekhta.adapters.BottomCategoriesAdapter;
import org.Rekhta.adapters.ExploreRecyclerAdpater;
import org.Rekhta.adapters.FeaturedPagerAdapter;
import org.Rekhta.adapters.HomepageAdapter;
import org.Rekhta.adapters.MoreDialogItemAdapter;
import org.Rekhta.fragments.NavigationFragment;
import org.Rekhta.model.homeScreenModel.CarouselsModel;
import org.Rekhta.model.homeScreenModel.FeaturedModelHome;
import org.Rekhta.model.homeScreenModel.HomeCards;
import org.Rekhta.model.homeScreenModel.HomeMainPojo;
import org.Rekhta.model.homeScreenModel.ImageShaeyriModel;
import org.Rekhta.model.homeScreenModel.ImageTags;
import org.Rekhta.model.homeScreenModel.RHomePojo;
import org.Rekhta.model.homeScreenModel.T20Series;
import org.Rekhta.model.homeScreenModel.TodaysTop;
import org.Rekhta.model.homeScreenModel.TopPoetsModel;
import org.Rekhta.model.homeScreenModel.VedioModel;
import org.Rekhta.model.homeScreenModel.WordOfDayModel;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Dashboard extends AppCompatActivity implements NavigationFragment.NavigationDrawerCallbacks {

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    int count = 0;
    int position = 0;
    LinearLayout wordDayMainLinear;
    boolean doubleBackToExitPressedOnce = false;
    SharedPreferences DataPrefs = null;
    TextView rightMenuname, right_menuEmail, rightMenuMyFavorates, rightMenuSettings;
    Button rightMenu_engBtn, rightMenu_hinBtn, rightMenu_urduBtn;
    Dialog dialog;
    WindowManager.LayoutParams wlp;
    AutoViewPager1 homeViewPager;
    TextView[] textview, newTextview;
    LinearLayout linearlayout, newLinearLayout, descriptionLinear;
    boolean value = false;
    boolean anothervalue = false;
    String shareUrl = "";
    AppBarLayout appBarLayout;
    TextView shayerName;
    String ImageBaseUrl = "https://rekhtacdn.azureedge.net/images/shayar/";
    LinearLayout celebratingTextSher;
    LinearLayout featuredtText;
    HomepageAdapter homepageAdapter;
    LinearLayout celebratingMainLayout;
    ProgressDialog progressDialog;
    CardView celebratingMainCardLayout;
    RecyclerView exploreRecycler, bottomCategoriesRecycler;
    ExploreRecyclerAdpater exploreRecyclerAdpater;
    Typeface engLatoBold, engLatoRegular, engLatoSemiBold, engMeriWheatherItalic,
            EngLatoXBoldItalic, EngLatoRegularX;
    private boolean readyForLoadingYoutubeThumbnail = true;
    TextView featured_heading;
    TextView nextSherarrow;
    AppCompatImageView testingNextArrow;
    Runnable timeCounter;
    Activity activity;
    DrawerLayout drawerLayout;
    private NavigationFragment drawerFragment;
    private Toolbar toolbar;
    private YouTubeThumbnailView youTubeView;
    private MViewPager featuredViewPager;
    private int s = 0;
    private String titileSher = "";
    private Handler handler;
    private int currentIndex = 0;
    private Context context;
    public ArrayList<String> idsRendered = new ArrayList<String>();
    public ArrayList<String> lastRenderSherName = new ArrayList<String>();
    public String currentSherID = "";
    private ImageView addtofav;

    AppDatabase appDatabase;
    private Typeface hinditf, urdutf;
    private MeaningFragment meaningFragment;
    private ConnectivityRecevier connectivityRecevier;

    LinearLayout homeBlank;
    public String idGhazalToRender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (activity == null)
            activity = Dashboard.this;

        context = this;

        initViews();
        //startService(new Intent(Dashboard.this, GetTypeIdsServices.class));
        //CommonUtil.currentScreenSize = calculateTheScreenSize();

        initRightMenuPopUpDialog();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
    }

    private void getDataFormServer(final String s) {

        progressDialog = new ProgressDialog(Dashboard.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        // progressDialog.show();

        String pattern = "dd-MM-yyyy";
        String date = new SimpleDateFormat(pattern).format(new Date());
        String token = CommonUtil.SharedPrefKeys.rekhtasAccessToken;
        HashMap<String, String> hashMap = new HashMap<>();


        RestClient.get().getHomeData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, token, s, date, hashMap)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<HomeMainPojo>() {
                    @Override
                    public void call(HomeMainPojo homeMainPojo) {

                        homeBlank.setVisibility(View.GONE);
                        if (homeMainPojo != null) {

                            RHomePojo rHomePojo = homeMainPojo.getR();
                            if (rHomePojo != null) {

                                List<CarouselsModel> carouselsModels = rHomePojo.getCarousels();
                                List<ImageShaeyriModel> imageShaeyriModels = rHomePojo.getImgShayari();
                                WordOfDayModel wordOfDayModel = rHomePojo.getWordOfTheDay();
                                List<TodaysTop> todaysTopsModel = rHomePojo.getTodaysTop();
                                List<FeaturedModelHome> featuredModelHomes = rHomePojo.getFeatured();

                                List<T20Series> t20Series = rHomePojo.getT20Series();
                                List<HomeCards> cardsModel = rHomePojo.getCards();


                                TextView explore_home_txt = (TextView) findViewById(R.id.explore_home_txt);
                                explore_home_txt.setText(LanguageSelection.getSpecificLanguageWordById("Explore By Categories", CommonUtil.languageCode));

                                if (CommonUtil.languageCode == 1) {
                                    CommonUtil.setEnglishLatoBoldFont(Dashboard.this, explore_home_txt);
                                    explore_home_txt.setTextSize(14);

                                } else if (CommonUtil.languageCode == 2) {
                                    CommonUtil.setRozaOne(Dashboard.this, explore_home_txt);
                                    explore_home_txt.setTextSize(18);

                                } else if (CommonUtil.languageCode == 3) {
                                    CommonUtil.setUrduRuqa(Dashboard.this, explore_home_txt);
                                    explore_home_txt.setTextSize(18);

                                }
                                exploreRecyclerAdpater = new ExploreRecyclerAdpater(Dashboard.this, cardsModel);
                                exploreRecycler.setAdapter(exploreRecyclerAdpater);

                                VedioModel vedioModel = rHomePojo.getVideo();
                                List<TopPoetsModel> topPoetsModels = rHomePojo.getTopPoets();

                                homepageAdapter = new HomepageAdapter(Dashboard.this, carouselsModels, s, Dashboard.this);
                                homeViewPager.setAdapter(homepageAdapter);

                                homeViewPager.startScroll();

                                TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
                                tabLayout.setupWithViewPager(homeViewPager, true);

                                BottomCategoriesAdapter bottomCategoriesAdapter = new BottomCategoriesAdapter(Dashboard.this, t20Series, s);
                                bottomCategoriesRecycler.setAdapter(bottomCategoriesAdapter);

                                if (topPoetsModels != null && topPoetsModels.size() != 0) {
                                    setPoetsData(topPoetsModels, todaysTopsModel);
                                }
                                //setPoetsData(topPoetsModels, todaysTopsModel);

                                if (imageShaeyriModels != null && imageShaeyriModels.size() != 0) {
                                    setImageShayeri(imageShaeyriModels);
                                }
                                //setImageShayeri(imageShaeyriModels);

                                if (featuredModelHomes != null && featuredModelHomes.size() != 0) {
                                    celebratingMainCardLayout.setVisibility(View.VISIBLE);
                                    celebratingMainLayout.setVisibility(View.VISIBLE);
                                    setCelebrateTodayData(featuredModelHomes);
                                } else {
                                    celebratingMainCardLayout.setVisibility(View.GONE);
                                    celebratingMainLayout.setVisibility(View.GONE);
                                }


                                //setVedioData(vedioModel);
                                if (vedioModel != null) {
                                    setVedioData(vedioModel);
                                }
                                //setVedioData(vedioModel);
                                //  setWordOfDay(rHomePojo.getWordOfTheDay());

                                if (wordOfDayModel != null) {
                                    if (wordOfDayModel.getTitle() == null || wordOfDayModel.getPoetName() == null) {
                                        wordDayMainLinear.setVisibility(View.GONE);
                                    } else {
                                        setWordOfDay(rHomePojo.getWordOfTheDay());
                                    }
                                } else {
                                    wordDayMainLinear.setVisibility(View.GONE);
                                }
                                //  scrollPagerAuto(carouselsModels);



                            }
                        } else {
                            // CommonUtil.showErrorActivity(Dashboard.this);
                            CommonUtil.showErrorDialog(Dashboard.this);
                        }
                    }

                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                        Toast.makeText(Dashboard.this, "Error Occured", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                });




        /*RestClient.get().getHomeData(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, token, s, date, hashMap, new Callback<HomeMainPojo>() {

            @Override
            public void success(HomeMainPojo homeMainPojo, Response response) {

                if (progressDialog != null)
                    progressDialog.dismiss();

                if (homeMainPojo != null) {


                    RHomePojo rHomePojo = homeMainPojo.getR();
                    if (rHomePojo != null) {
                        List<CarouselsModel> carouselsModels = rHomePojo.getCarousels();
                        List<ImageShaeyriModel> imageShaeyriModels = rHomePojo.getImgShayari();
                        WordOfDayModel wordOfDayModel = rHomePojo.getWordOfTheDay();
                        List<TodaysTop> todaysTopsModel = rHomePojo.getTodaysTop();
                        List<FeaturedModelHome> featuredModelHomes = rHomePojo.getFeatured();

                        List<T20Series> t20Series = rHomePojo.getT20Series();
                        List<HomeCards> cardsModel = rHomePojo.getCards();

                        exploreRecyclerAdpater = new ExploreRecyclerAdpater(Dashboard.this, cardsModel);
                        exploreRecycler.setAdapter(exploreRecyclerAdpater);

                        VedioModel vedioModel = rHomePojo.getVideo();
                        List<TopPoetsModel> topPoetsModels = rHomePojo.getTopPoets();

                        homepageAdapter = new HomepageAdapter(Dashboard.this, carouselsModels, s, Dashboard.this);
                        homeViewPager.setAdapter(homepageAdapter);

                        BottomCategoriesAdapter bottomCategoriesAdapter = new BottomCategoriesAdapter(Dashboard.this, t20Series, s);
                        bottomCategoriesRecycler.setAdapter(bottomCategoriesAdapter);

                        if (topPoetsModels != null && topPoetsModels.size() != 0) {
                            setPoetsData(topPoetsModels, todaysTopsModel);
                        }


                        if (imageShaeyriModels != null && imageShaeyriModels.size() != 0) {
                            setImageShayeri(imageShaeyriModels);
                        }


                        if (featuredModelHomes != null && featuredModelHomes.size() != 0) {
                            setFeaturedData(featuredModelHomes);
                        }


                        if (t20Series != null && t20Series.size() != 0) {
                            //  setBottomTags(t20Series);
                        }

                        if (vedioModel != null) {
                            setVedioData(vedioModel);
                        }

                        if (wordOfDayModel != null) {
                            if (wordOfDayModel.getTitle() == null || wordOfDayModel.getPoetName() == null) {
                                wordDayMainLinear.setVisibility(View.GONE);
                            } else {
                                setWordOfDay(rHomePojo.getWordOfTheDay());
                            }

                        }

                        scrollPagerAuto(carouselsModels);
                    }
                } else {
                    CommonUtil.showErrorActivity(Dashboard.this);
                }

            }

            @Override
            public void failure(RetrofitError error) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                CommonUtil.showErrorActivity(Dashboard.this);
                Log.v("Erroe", error.toString());
            }
        });
*/
    }

    private void scrollPagerAuto(final List<CarouselsModel> carouselsModels) {


        timeCounter = new Runnable() {

            @Override
            public void run() {
                if ((currentIndex + 1) > carouselsModels.size()) {
                    currentIndex = 0;

                    //handler.removeCallbacks(timeCounter);
                    //scrollPagerAuto(carouselsModels);

                    homepageAdapter = new HomepageAdapter(Dashboard.this, carouselsModels, String.valueOf(CommonUtil.languageCode), Dashboard.this);
                    homeViewPager.setAdapter(homepageAdapter);
                } else {
                    currentIndex++;

                }
                homeViewPager.setCurrentItem(currentIndex);
                handler.postDelayed(timeCounter, 4 * 1000);
            }
        };
        handler.postAtTime(timeCounter, 4 * 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if ((progressDialog != null) && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

    private void setWordOfDay(WordOfDayModel wordOfTheDay) {


        TextView mainWord = (TextView) findViewById(R.id.mainWordOfday);
        TextView wordOfDayHeading = (TextView) findViewById(R.id.wordOfDayHeading);
        TextView see_archives = (TextView) findViewById(R.id.see_archives);
        TextView meaning1 = (TextView) findViewById(R.id.meaing1);
        TextView meaning2 = (TextView) findViewById(R.id.meaing2);
        TextView mainMeaning = (TextView) findViewById(R.id.mainMeaning);
        TextView wordOfdayPoet = (TextView) findViewById(R.id.wordOfdayShaer);

        descriptionLinear = (LinearLayout) findViewById(R.id.wordOfDayDetail);
        wordOfdayPoet.setText(wordOfTheDay.getPoetName());


        mainMeaning.setText(wordOfTheDay.getMeaning());

        setDescription(wordOfTheDay.getTitle());

        wordOfDayHeading.setText(LanguageSelection.getSpecificLanguageWordById("Word of the Day", CommonUtil.languageCode));
        see_archives.setText(LanguageSelection.getSpecificLanguageWordById("See archives", CommonUtil.languageCode));


        if (CommonUtil.languageCode == 1) {

            mainWord.setText(wordOfTheDay.getWord_En());
            meaning1.setText(wordOfTheDay.getWord_Hi());
            meaning2.setText(wordOfTheDay.getWord_Ur());
            wordOfDayHeading.setTextSize(14);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, wordOfDayHeading);
            CommonUtil.setEnglishLatoSemiBold(Dashboard.this, see_archives);

            // description.setText(formateString());
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, mainWord);
            CommonUtil.setHindiFont(Dashboard.this, meaning1);
            CommonUtil.setUrduRuqa(Dashboard.this, meaning2);


        } else if (CommonUtil.languageCode == 2) {

            wordOfDayHeading.setTextSize(18);
            mainWord.setText(wordOfTheDay.getWord_Hi());
            meaning1.setText(wordOfTheDay.getWord_En());
            meaning2.setText(wordOfTheDay.getWord_Ur());
            CommonUtil.setRozaOne(Dashboard.this, wordOfDayHeading);
            CommonUtil.setHindiLailaRegular(Dashboard.this, mainWord);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, meaning1);
            CommonUtil.setUrduRuqa(Dashboard.this, meaning2);
            CommonUtil.setHindiFont(Dashboard.this, see_archives);

        } else if (CommonUtil.languageCode == 3) {

            wordOfDayHeading.setTextSize(18);
            mainWord.setText(wordOfTheDay.getWord_Ur());
            meaning1.setText(wordOfTheDay.getWord_En());
            meaning2.setText(wordOfTheDay.getWord_Hi());
            CommonUtil.setUrduRuqa(Dashboard.this, wordOfDayHeading);
            CommonUtil.setUrduFont(Dashboard.this, mainWord);

            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, meaning1);
            CommonUtil.setHindiFont(Dashboard.this, meaning2);

            CommonUtil.setUrduNotoNataliq(Dashboard.this, see_archives);

        } else {

            mainWord.setText(wordOfTheDay.getWord_En());
            meaning1.setText(wordOfTheDay.getWord_Hi());
            meaning2.setText(wordOfTheDay.getWord_Ur());

            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, mainWord);
            CommonUtil.setHindiFont(Dashboard.this, meaning1);
            CommonUtil.setUrduFont(Dashboard.this, meaning2);
        }


    }

    private void setDescription(String title) {

        descriptionLinear.removeAllViews();
        String titileSher = title;
        anothervalue = true;
        int minGap = 10;
        newLinearLayout = new LinearLayout(Dashboard.this);
        newLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        newLinearLayout.setOrientation(LinearLayout.VERTICAL);

        newLinearLayout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        try {

            JSONObject jsonObject = new JSONObject(titileSher);
            JSONArray Parray = jsonObject.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {

                    LinearLayout linear = new LinearLayout(Dashboard.this);
                    linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linear.setOrientation(LinearLayout.HORIZONTAL);
                    linear.setPadding(0, 17, 0, 17);


                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    newTextview = new TextView[Warray.length()];


                    for (int k = 0; k < Warray.length(); k++) {
                        newTextview[j] = new TextView(Dashboard.this);
                        newTextview[j].setLayoutParams(dim);
                        newTextview[j].setSingleLine(true);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            newTextview[j].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        }
                        if (k < Warray.length() - 1) {
                            newTextview[j].setPadding(0, 0, minGap, 0);
                        }
                        newTextview[j].setText(" " + Warray.getJSONObject(k).getString("W"));
                        newTextview[j].setGravity(Gravity.CENTER);
                        if (CommonUtil.languageCode == 1) {
                            CommonUtil.setEnglishLatoRegularFont(Dashboard.this, newTextview[j]);
                        } else if (CommonUtil.languageCode == 2) {
                            CommonUtil.setHindiFont(Dashboard.this, newTextview[j]);
                        } else if (CommonUtil.languageCode == 3) {
                            CommonUtil.setUrduFont(Dashboard.this, newTextview[j]);
                        }

                        linear.addView(newTextview[j]);

//                            str = str + " " + Warray.getJSONObject(k).getString("W");
                    }
                    newLinearLayout.addView(linear);
                }
            }
        } catch (Exception e) {

        }

        descriptionLinear.addView(newLinearLayout);
        descriptionLinear.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (anothervalue)
                    widhtOfAnotherView();
                anothervalue = false;
            }
        });
    }


    private String formateString(String title) {

        String str = "";
        try {
            JSONObject obj = new JSONObject(title);
            JSONArray Parray = obj.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {
                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {
                    if (j == 1) {
                        //str = str + "\n";
                    }
                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    if (CommonUtil.languageCode != 3) {


                        for (int k = 0; k < Warray.length(); k++) {

                            str = str + " " + Warray.getJSONObject(k).getString("S");
                        }

                    } else {
                        for (int k = Warray.length() - 1; k >= 0; k--) {
                            str = str + " " + Warray.getJSONObject(k).getString("S");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    public void widhtOfView() {

        int extraPadding = 0;
        int curWidthDiff = 0;
        int childCount = linearlayout.getChildCount();
        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = linearlayout.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                curWidthDiff = linearlayout.getWidth() - lineView.getWidth();
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
//                    textView.setBackgroundColor(Color.parseColor("#FFE800"));
                    extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        textView.setPadding(0, 0, extraPadding, 0);
                    }
                }

                //TextView
            }
        }
    }

    public void widhtOfAnotherView() {
        int childCount = newLinearLayout.getChildCount();
        for (int lineCount = 0; lineCount < childCount; lineCount++) {
            View chVw = newLinearLayout.getChildAt(lineCount);
            if (chVw instanceof LinearLayout) {
                LinearLayout lineView = (LinearLayout) chVw;
                int curWidthDiff = newLinearLayout.getWidth() - lineView.getWidth();
//                Log.e("height----->", curWidthDiff + "--" + linearWrapper.getMeasuredWidth()+ "--" +linearWrapper.getWidth());
                for (int chCount = 0; chCount < lineView.getChildCount() - 1; chCount++) {
                    View textView = lineView.getChildAt(chCount);
//                    textView.setBackgroundColor(Color.parseColor("#FFE800"));
                    int extraPadding = (curWidthDiff / (lineView.getChildCount() - 1)) + textView.getPaddingRight();
                    if (textView instanceof TextView) {
                        textView.setPadding(0, 0, extraPadding, 0);
                    }
                }

                //TextView
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (s == 2) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

    }

    private void setVedioData(final VedioModel vedioModel) {

        TextView vedioPoetName = (TextView) findViewById(R.id.vedioPoetName);
        TextView home_vedio_text = (TextView) findViewById(R.id.home_vedio_text);
        TextView home_vedio_seeMore = (TextView) findViewById(R.id.home_vedio_seeMore);
        home_vedio_text.setText(LanguageSelection.getSpecificLanguageWordById("Feaured Video", CommonUtil.languageCode));
        home_vedio_seeMore.setText(LanguageSelection.getSpecificLanguageWordById("See More", CommonUtil.languageCode));
        TextView vedioPoetTitle = (TextView) findViewById(R.id.vedioTitle);
        TextView vedioShare = (TextView) findViewById(R.id.vedioShare);
        final String shareUrl;

        if (CommonUtil.languageCode == 3) {

            shareUrl = vedioModel.getShortUrl_Ur();

            CommonUtil.setUrduFont(Dashboard.this, home_vedio_seeMore);
            CommonUtil.setUrduRuqa(Dashboard.this, home_vedio_text);
            home_vedio_text.setTextSize(18);
            CommonUtil.setUrduFont(Dashboard.this, vedioPoetName);
            CommonUtil.setUrduFont(Dashboard.this, vedioPoetTitle);
            CommonUtil.setUrduFont(Dashboard.this, home_vedio_seeMore);
            home_vedio_seeMore.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(Dashboard.this, R.drawable.rtl_arrow), null, null, null);

        } else if (CommonUtil.languageCode == 2) {

            CommonUtil.setHindiFont(Dashboard.this, home_vedio_seeMore);
            CommonUtil.setRozaOne(Dashboard.this, home_vedio_text);
            home_vedio_text.setTextSize(18);
            CommonUtil.setHindiLailaRegular(Dashboard.this, vedioPoetName);
            CommonUtil.setHindiFont(Dashboard.this, vedioPoetTitle);

            shareUrl = vedioModel.getShortUrl_Hi();
            home_vedio_seeMore.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);


        } else {
            shareUrl = vedioModel.getShortUrl_En();
            home_vedio_text.setTextSize(14);
            CommonUtil.setEnglishLatoSemiBold(Dashboard.this, home_vedio_seeMore);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, home_vedio_text);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, vedioPoetName);
            CommonUtil.setEnglishLatoRegularFont(Dashboard.this, vedioPoetTitle);
            CommonUtil.setEnglishLatoRegularFont(Dashboard.this, home_vedio_seeMore);
            home_vedio_seeMore.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);

        }

        vedioShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(Dashboard.this);
                } else {
                    if (shareUrl != null) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");

                        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
                        startActivity(Intent.createChooser(intent, "Share using"));
                    } else {
                        Toast.makeText(Dashboard.this, "Share Url Not Available", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        vedioPoetName.setText(vedioModel.getEntityName());
        vedioPoetTitle.setText(vedioModel.getVideoTitle());

        ImageView playButton = (ImageView) findViewById(R.id.youtubePalyButton);
        if (readyForLoadingYoutubeThumbnail) {
            readyForLoadingYoutubeThumbnail = false;
            try {

                youTubeView.initialize("AIzaSyAucCMhVmsY5JZRRl2JPsvRdnWeThfOHx8", new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                        youTubeThumbnailLoader.setVideo(vedioModel.getYoutube_Id());

                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {

                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {

                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                            }
                        });

                        readyForLoadingYoutubeThumbnail = true;

                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                        readyForLoadingYoutubeThumbnail = true;

                    }
                });


                playButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        s = 2;
                        Intent intent = YouTubeStandalonePlayer.createVideoIntent(activity, "AIzaSyAucCMhVmsY5JZRRl2JPsvRdnWeThfOHx8", vedioModel.getYoutube_Id());
                        startActivityForResult(intent, 100);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setCelebrateTodayData(List<FeaturedModelHome> featuredModelHomes) {

        celebratingTextSher.removeAllViews();

        ImageView poetImage;
        TextView celebratingPoetName, celebratingPoetAge, celebratingRemark;
        LinearLayout celebratingSher;

        celebratingPoetName = (TextView) findViewById(R.id.celebratingPoetName);
        celebratingPoetAge = (TextView) findViewById(R.id.celebratingPoetAge);
        TextView home_celebrat_text = (TextView) findViewById(R.id.home_celebrat_text);

        home_celebrat_text.setText(LanguageSelection.getSpecificLanguageWordById("Celebrating Today", CommonUtil.languageCode));
        celebratingSher = (LinearLayout) findViewById(R.id.celebratingTextSher);
        celebratingRemark = (TextView) findViewById(R.id.celebratingTextRemark);
        poetImage = (ImageView) findViewById(R.id.celebratingpoetImage);


        String baseUrl = "https://rekhtacdn.azureedge.net";
        String imageUrl = featuredModelHomes.get(0).getImageUrl();
        if (featuredModelHomes != null && featuredModelHomes.size() > 0) {
            if (imageUrl != null && !imageUrl.equals("")) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.default_profile);
                requestOptions.error(R.drawable.default_profile);
                Glide.with(this).setDefaultRequestOptions(requestOptions).load(imageUrl).into(poetImage);
            }

            celebratingPoetName.setText(featuredModelHomes.get(0).getEntityName());
            celebratingPoetAge.setText(featuredModelHomes.get(0).getFromDate() + "-" + featuredModelHomes.get(0).getToDate());
            celebratingRemark.setText(featuredModelHomes.get(0).getRemarks());
            titileSher = featuredModelHomes.get(0).getTitle();
        }

        anothervalue = true;
        int minGap = 10;
        newLinearLayout = new LinearLayout(Dashboard.this);
        newLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        newLinearLayout.setOrientation(LinearLayout.VERTICAL);
//        newLinearLayout.setBackgroundColor(Color.parseColor("#BFE8BB"));
        newLinearLayout.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams dim = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        try {

            JSONObject jsonObject = new JSONObject(titileSher);
            JSONArray Parray = jsonObject.getJSONArray("P");
            for (int i = 0; i < Parray.length(); i++) {

                JSONArray Larray = Parray.getJSONObject(i).getJSONArray("L");
                for (int j = 0; j < Larray.length(); j++) {

                    LinearLayout linear = new LinearLayout(Dashboard.this);
                    linear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    linear.setOrientation(LinearLayout.HORIZONTAL);
                    linear.setPadding(0, 17, 0, 17);

//                    if (j == 1) {
//                        str = str + "\n";
//                    }
                    JSONArray Warray = Larray.getJSONObject(j).getJSONArray("W");
                    newTextview = new TextView[Warray.length()];


                    for (int k = 0; k < Warray.length(); k++) {
                        newTextview[j] = new TextView(Dashboard.this);
                        newTextview[j].setLayoutParams(dim);
                        newTextview[j].setSingleLine(true);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            newTextview[j].setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        }
                        if (k < Warray.length() - 1) {
                            newTextview[j].setPadding(0, 0, minGap, 0);
                        }
                        newTextview[j].setText(" " + Warray.getJSONObject(k).getString("W"));
                        newTextview[j].setGravity(Gravity.CENTER);

                        if (CommonUtil.languageCode == 1) {

                            CommonUtil.setEnglishMeriWeatherItalic(Dashboard.this, newTextview[j]);


                        } else if (CommonUtil.languageCode == 2) {


                            CommonUtil.setHindiFont(Dashboard.this, newTextview[j]);


                        } else if (CommonUtil.languageCode == 3) {


                            CommonUtil.setUrduFont(Dashboard.this, newTextview[j]);

                        }
                        CommonUtil.setHindiLailaRegular(Dashboard.this, newTextview[j]);
                        linear.addView(newTextview[j]);

//                            str = str + " " + Warray.getJSONObject(k).getString("W");
                    }
                    newLinearLayout.addView(linear);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        celebratingTextSher.addView(newLinearLayout);
        celebratingTextSher.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (anothervalue)
                    widhtOfAnotherView();
                anothervalue = false;
            }
        });


        if (CommonUtil.languageCode == 1) {

            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, home_celebrat_text);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, celebratingPoetName);
            CommonUtil.setEnglishLatoRegularFont(Dashboard.this, celebratingRemark);
            CommonUtil.setEnglishLatoRegularFont(Dashboard.this, celebratingPoetAge);

        } else if (CommonUtil.languageCode == 2) {


            CommonUtil.setRozaOne(Dashboard.this, home_celebrat_text);
            CommonUtil.setHindiFont(Dashboard.this, celebratingPoetName);
            CommonUtil.setHindiFont(Dashboard.this, celebratingRemark);
            CommonUtil.setHindiFont(Dashboard.this, celebratingPoetAge);

        } else if (CommonUtil.languageCode == 3) {


            CommonUtil.setUrduRuqa(Dashboard.this, home_celebrat_text);
            CommonUtil.setUrduFont(Dashboard.this, celebratingPoetName);
            CommonUtil.setUrduFont(Dashboard.this, celebratingRemark);
            CommonUtil.setUrduFont(Dashboard.this, celebratingPoetAge);
        }

    }


    @Override
    protected void onStart() {
        super.onStart();

       /* int lang = CommonUtil.languageCode;


        homeBlank.setVisibility(View.VISIBLE);
        if (lang == 1) {
            forceLTRIfSupported();
            getDataFormServer("1");
        } else if (lang == 2) {
            forceLTRIfSupported();
            getDataFormServer("2");
        } else if (lang == 3) {
            forceRTLIfSupported();
            getDataFormServer("3");
        }*/

        CommonUtil.registerReciver(Dashboard.this);

    }

    @Override
    protected void onStop() {
        super.onStop();

        CommonUtil.unregisterReciebr(Dashboard.this);

    }

    private void setImageShayeri(List<ImageShaeyriModel> imageShaeyriModels) {

        final ImageView shayeriImage = (ImageView) findViewById(R.id.shayeriImage);
        final ImageView fav_Image_shayari = (ImageView) findViewById(R.id.fav_Image_shayari);
        TextView shareImage = (TextView) findViewById(R.id.share_image);
        TextView seeMore = (TextView) findViewById(R.id.seeMoreImage);
        TextView home_tag_txt = (TextView) findViewById(R.id.home_tag_txt);
        home_tag_txt.setText(LanguageSelection.getSpecificLanguageWordById("Tags", CommonUtil.languageCode));

        TextView home_image_Sh_text = (TextView) findViewById(R.id.home_image_Sh_text);
        home_image_Sh_text.setText(LanguageSelection.getSpecificLanguageWordById("IMAGE SHAYERI", CommonUtil.languageCode));
        seeMore.setText(LanguageSelection.getSpecificLanguageWordById("See More", CommonUtil.languageCode));

        TextView firstTag = (TextView) findViewById(R.id.firstTag);
        final TextView moreTags = (TextView) findViewById(R.id.moretxt);
        List<ImageTags> imageTags = null;

        checkImageExist("rekhta0", fav_Image_shayari);

        fav_Image_shayari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveImageToGellary(shayeriImage, fav_Image_shayari);
            }
        });

        if (imageShaeyriModels.size() > 0) {
            imageTags = imageShaeyriModels.get(0).getImgTags();
            if (imageTags.size() > 0) {
                firstTag.setText(" : " + imageTags.get(0).getTagName());
                String ande = LanguageSelection.getSpecificLanguageWordById("and", CommonUtil.languageCode);
                String more = LanguageSelection.getSpecificLanguageWordById("home_more", CommonUtil.languageCode);
                moreTags.setText(" " + ande + " " + Integer.parseInt(String.valueOf(imageTags.size() - 1)) + " " + more);

            }

        }


        final List<ImageTags> finalImageTags = imageTags;
        moreTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                initiatePopupWindow(finalImageTags).showAsDropDown(moreTags);
            }
        });

        if (CommonUtil.languageCode == 3) {
            shareUrl = imageShaeyriModels.get(0).getShortUrl_Ur();
            CommonUtil.setUrduRuqa(Dashboard.this, home_image_Sh_text);
            home_image_Sh_text.setTextSize(18);
            CommonUtil.setUrduFont(Dashboard.this, seeMore);
            seeMore.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(Dashboard.this, R.drawable.rtl_arrow), null, null, null);

            //moreTags.setTypeface(urdutf);
            //firstTag.setTypeface(urdutf);

        } else if (CommonUtil.languageCode == 2) {
            shareUrl = imageShaeyriModels.get(0).getShortUrl_Hi();
            home_image_Sh_text.setTypeface(hinditf);
            CommonUtil.setHindiFont(Dashboard.this, seeMore);
            CommonUtil.setRozaOne(Dashboard.this, home_image_Sh_text);
            home_image_Sh_text.setTextSize(18);
            CommonUtil.setRozaOne(Dashboard.this, firstTag);
            CommonUtil.setRozaOne(Dashboard.this, moreTags);
            CommonUtil.setHindiFont(Dashboard.this, home_tag_txt);
            seeMore.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);


            moreTags.setTypeface(hinditf);
            firstTag.setTypeface(hinditf);

        } else {
            home_image_Sh_text.setTextSize(14);
            shareUrl = imageShaeyriModels.get(0).getShortUrl_En();
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, home_image_Sh_text);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, home_tag_txt);
            CommonUtil.setEnglishLatoSemiBold(Dashboard.this, seeMore);

            moreTags.setTypeface(hinditf);
            firstTag.setTypeface(hinditf);
            seeMore.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);
        }
        String url = "https://rekhta.org/Images/ShayariImages/";
        String imageUrl = ImageBaseUrl + imageShaeyriModels.get(0).getSEO_Slug() + ".png";

        if (imageShaeyriModels.get(0).getId() != null) {
            if (!this.isFinishing()) {
                // Load the image using Glide
                Glide.with(context).
                        load(url + imageShaeyriModels.get(0).getId() + ".png")
                        .into(shayeriImage);
            }

        }


        seeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, ImageShyariActivity.class));
            }
        });
        shareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isSkipped) {
                    CommonUtil.showToast(Dashboard.this);
                } else {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");

                    intent.putExtra(android.content.Intent.EXTRA_TEXT, shareUrl);
//
                    startActivity(Intent.createChooser(intent, "Share via"));
                }

//               sta
            }
        });

    }

    private void checkImageExist(String imgName, ImageView fav_Image_shayari) {


        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "Rekhta"); //Creates app specific folder
        path.mkdirs();
        File imageFile = new File(path, imgName + ".png"); // Imagename.png
        //check if file already exist...............................................................
        if (imageFile.exists()) {
            if (imageFile.isFile()) {
                fav_Image_shayari.setImageResource(R.drawable.ic_favorited);
                fav_Image_shayari.setColorFilter(ContextCompat.getColor(Dashboard.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            }

        } else {
            fav_Image_shayari.setImageResource(R.drawable.ic_favorite);
            //  fav_Image_shayari.setColorFilter(ContextCompat.getColor(Dashboard.this, R.color.black), PorterDuff.Mode.SRC_IN);
        }

    }

    private void saveImageToGellary(ImageView imgDisplay, ImageView favImage) {


        try {
            BitmapDrawable draw = (BitmapDrawable) imgDisplay.getDrawable();
            final Bitmap bitmap = draw.getBitmap();
            if (Build.VERSION.SDK_INT >= 23) {
                if (chckPermission()) {
//                            SaveImageLocally(anImage);

                    saveImageToExternal("rekhta0", bitmap, favImage);
                } else {
                    requestPermission(); // Code for permission
                }
            } else {
//                        SaveImageLocally(anImage);
                saveImageToExternal("rekhta0", bitmap, favImage);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(Dashboard.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(Dashboard.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(Dashboard.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            ActivityCompat.requestPermissions(Dashboard.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

    }

    private boolean chckPermission() {

        int result = ContextCompat.checkSelfPermission(Dashboard.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void saveImageToExternal(String imgName, Bitmap bitmap, ImageView favImage) throws IOException {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "Rekhta"); //Creates app specific folder
        path.mkdirs();
        File imageFile = new File(path, imgName + ".png"); // Imagename.png
        //check if file already exist...............................................................
        if (imageFile.exists()) {
            if (imageFile.isFile()) {
                //............delete file ...
                imageFile.delete();
                favImage.setImageResource(R.drawable.ic_favorite);
                //  favImage.setColorFilter(ContextCompat.getColor(Dashboard.this, R.color.black), PorterDuff.Mode.SRC_IN);
                Toast.makeText(Dashboard.this, "Image Removed from Gallery", Toast.LENGTH_LONG).show();
            }

        } else {
            FileOutputStream out = new FileOutputStream(imageFile);
            try {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
                out.flush();
                out.close();

                favImage.setImageResource(R.drawable.ic_favorited);
                favImage.setColorFilter(ContextCompat.getColor(Dashboard.this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                Toast.makeText(Dashboard.this, "Image Saved Successfully", Toast.LENGTH_LONG).show();


                MediaScannerConnection.scanFile(Dashboard.this, new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
            } catch (Exception e) {
                throw new IOException();
            }
        }


    }

    private void showMoreDialog(List<ImageTags> imageTags) {

        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        LayoutInflater layoutInflater = LayoutInflater.from(Dashboard.this);
        View view = layoutInflater.inflate(R.layout.more_dialog_items, null, false);
        ListView listView = (ListView) view.findViewById(R.id.moreList);

        ArrayList<String> moreTags = new ArrayList<>();

        for (int i = 0; i < imageTags.size(); i++) {
            moreTags.add(imageTags.get(i).getTagName());
        }

        // MoreDialogItemAdapter moreDialogItemAdapter = new MoreDialogItemAdapter(Dashboard.this, moreTags);
        // ArrayAdapter arrayAdapter = new ArrayAdapter(Dashboard.this, R.layout.more_dialog_items, moreTags);
        //listView.setAdapter(moreDialogItemAdapter);

        builder.setView(view);
        builder.show();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    }

    private void setPoetsData(final List<TopPoetsModel> topPoetsModels, final List<TodaysTop> todaysTopsModel) {


        //featuredtText.removeAllViews();
        ImageView poestImage1, poestImage2, poestImage3, poestImage4;
        TextView poestName1, poestName2, poestName3, poestName4, readMoreSher;
        ArrayList<ImageView> poetsImageList = new ArrayList<>();
        ArrayList<TextView> poetsNameList = new ArrayList<>();
        featured_heading = (TextView) findViewById(R.id.featured_heading);
        featured_heading.setText(LanguageSelection.getSpecificLanguageWordById("Featured Sher", CommonUtil.languageCode));

        final String titileSher = todaysTopsModel.get(count).getTitle();
        final int size = todaysTopsModel.size();

        poestImage1 = (ImageView) findViewById(R.id.poetsImage1);
        poestImage2 = (ImageView) findViewById(R.id.poetsImage2);
        poestImage3 = (ImageView) findViewById(R.id.poetsImage3);
        poestImage4 = (ImageView) findViewById(R.id.poetsImage4);


        poestImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Dashboard.this, PoetDetailedActivity.class);
                if (todaysTopsModel.size() > 0) {
                    intent.putExtra("poetId", topPoetsModels.get(0).getEntityId());
                    intent.putExtra("ghazalCount", "" + "");
                    intent.putExtra("nazmCount", "" + "");
                    intent.putExtra("sherCount", "" + "");
                    intent.putExtra("shortDescInEng", "");
                    intent.putExtra("shortDescInHin", "");
                    intent.putExtra("shortDescInUrdu", "");
                    startActivity(intent);
                }
            }
        });

        poestImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (topPoetsModels.size() > 1) {
                    Intent intent = new Intent(Dashboard.this, PoetDetailedActivity.class);
                    intent.putExtra("poetId", topPoetsModels.get(1).getEntityId());
                    intent.putExtra("ghazalCount", "" + "");
                    intent.putExtra("nazmCount", "" + "");
                    intent.putExtra("sherCount", "" + "");
                    intent.putExtra("shortDescInEng", "");
                    intent.putExtra("shortDescInHin", "");
                    intent.putExtra("shortDescInUrdu", "");

                    startActivity(intent);
                }

            }
        });

        poestImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (topPoetsModels.size() > 2) {
                    Intent intent = new Intent(Dashboard.this, PoetDetailedActivity.class);
                    intent.putExtra("poetId", topPoetsModels.get(2).getEntityId());
                    intent.putExtra("ghazalCount", "" + "");
                    intent.putExtra("nazmCount", "" + "");
                    intent.putExtra("sherCount", "" + "");
                    intent.putExtra("shortDescInEng", "");
                    intent.putExtra("shortDescInHin", "");
                    intent.putExtra("shortDescInUrdu", "");

                    startActivity(intent);
                }

            }
        });

        poestImage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (topPoetsModels.size() > 3) {
                    Intent intent = new Intent(Dashboard.this, PoetDetailedActivity.class);
                    intent.putExtra("poetId", topPoetsModels.get(3).getEntityId());
                    intent.putExtra("ghazalCount", "" + "");
                    intent.putExtra("nazmCount", "" + "");
                    intent.putExtra("sherCount", "" + "");
                    intent.putExtra("shortDescInEng", "");
                    intent.putExtra("shortDescInHin", "");
                    intent.putExtra("shortDescInUrdu", "");

                    startActivity(intent);
                }

            }
        });

        TextView nextSher = (TextView) findViewById(R.id.arrowNext);
        TextView prevSher = (TextView) findViewById(R.id.arrowPrev);
        // prevSher.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(Dashboard.this, R.drawable.ic_keyboard_arrow_left_black_24dp), null, null, null);

        addtofav = (ImageView) findViewById(R.id.addFavFeatured);

        shayerName = (TextView) findViewById(R.id.shaerName);
        final TextView finalShayerName = shayerName;
        nextSher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position < todaysTopsModel.size() - 1) {
                    position = position + 1;
                    featuredViewPager.setCurrentItem(position);
                    shayerName.setText(todaysTopsModel.get(position).getPoetName());
                }

            }
        });

       /* prevSher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position > 0) {
                    position = position - 1;
                    featuredViewPager.setCurrentItem(position);
                    shayerName.setText(todaysTopsModel.get(position).getPoetName());
                }
            }
        });*/

        featuredViewPager = (MViewPager) findViewById(R.id.featuredViewPager);
        FeaturedPagerAdapter featuredPagerAdapter = new FeaturedPagerAdapter(Dashboard.this, todaysTopsModel, Dashboard.this);
        featuredViewPager.setAdapter(featuredPagerAdapter);

        poestName1 = (TextView) findViewById(R.id.poetsName1);
        poestName2 = (TextView) findViewById(R.id.poetsName2);
        poestName3 = (TextView) findViewById(R.id.poetsName3);
        poestName4 = (TextView) findViewById(R.id.poetsName4);

        readMoreSher = (TextView) findViewById(R.id.readMoreSher);
        readMoreSher.setText(LanguageSelection.getSpecificLanguageWordById("Read More", CommonUtil.languageCode));

        if (CommonUtil.languageCode == 1) {

            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, poestName1);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, poestName2);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, poestName3);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, poestName4);
            CommonUtil.setEnglishLatoBoldFont(Dashboard.this, featured_heading);
            CommonUtil.setEnglishMerriweatherBoldFont(Dashboard.this, shayerName);
            CommonUtil.setEnglishLatoSemiBold(Dashboard.this, readMoreSher);
            nextSher.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);
            readMoreSher.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);

            shayerName.setTextSize(14);
            featured_heading.setTextSize(14);


        } else if (CommonUtil.languageCode == 2) {

            CommonUtil.setHindiFont(Dashboard.this, poestName1);
            CommonUtil.setHindiFont(Dashboard.this, poestName2);
            CommonUtil.setHindiFont(Dashboard.this, poestName3);
            CommonUtil.setHindiFont(Dashboard.this, poestName4);
            CommonUtil.setHindiFont(Dashboard.this, poestName1);
            CommonUtil.setHindiFont(Dashboard.this, readMoreSher);
            CommonUtil.setRozaOne(Dashboard.this, featured_heading);
            nextSher.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);
            readMoreSher.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(Dashboard.this, R.drawable.ltr_arrow), null);

            shayerName.setTextSize(18);
            featured_heading.setTextSize(18);

            CommonUtil.setRozaOne(Dashboard.this, shayerName);

        } else if (CommonUtil.languageCode == 3) {

            poestName1.setTypeface(urdutf);
            poestName2.setTypeface(urdutf);
            poestName3.setTypeface(urdutf);
            poestName4.setTypeface(urdutf);

            CommonUtil.setUrduRuqa(Dashboard.this, shayerName);
            // shayerName.setTypeface(urdutf);
            CommonUtil.setUrduRuqa(Dashboard.this, featured_heading);
            nextSher.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(Dashboard.this, R.drawable.rtl_arrow), null, null, null);
            readMoreSher.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(Dashboard.this, R.drawable.rtl_arrow), null, null, null);

            // featured_heading.setTypeface(urdutf);
            readMoreSher.setTypeface(urdutf);
            shayerName.setTextSize(12);
            featured_heading.setTextSize(18);
        }


        readMoreSher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, SherActivity.class));
            }
        });

        try {

            poetsImageList.addAll(Arrays.asList(poestImage1, poestImage2, poestImage3, poestImage4));
            poetsNameList.addAll(Arrays.asList(poestName1, poestName2, poestName3, poestName4));

            for (int i = 0; i < 4; i++) {
                poetsNameList.get(i).setText(topPoetsModels.get(i).getName());
                String imageURl = topPoetsModels.get(i).getSEO_Slug();
                if (imageURl != null && !imageURl.equals("")) {
                    Glide.with(getApplicationContext()).load(ImageBaseUrl + imageURl + ".png").into(poetsImageList.get(i));

                } else {
                    poetsImageList.get(i).setImageResource(R.drawable.default_profile);
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        int lang = CommonUtil.languageCode;


        if (lang == 1) {
            getDataFormServer("1");
        } else if (lang == 2) {
            getDataFormServer("2");
        } else if (lang == 3) {
            getDataFormServer("3");
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }


    @Override
    public void langChanged() {

        if (progressDialog != null && !progressDialog.isShowing())
            //progressDialog.show();

            homeBlank.setVisibility(View.VISIBLE);

        if (CommonUtil.languageCode == 3) {

            //Urdu..
            forceRTLIfSupported();
            // homeBlank.setVisibility(View.VISIBLE);
            getDataFormServer("3");

            drawerFragment.closeDrawer();

        } else if (CommonUtil.languageCode == 2) {

            //hindi..................................
            forceLTRIfSupported();
            //homeBlank.setVisibility(View.VISIBLE);
            getDataFormServer("2");

            drawerFragment.closeDrawer();

        } else {
            forceLTRIfSupported();
            //homeBlank.setVisibility(View.VISIBLE);
            getDataFormServer("1");

            drawerFragment.closeDrawer();
        }
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void forceLTRIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public void onBackPressed() {

        if (CommonUtil.dontClose) {
            CommonUtil.dontClose = false;
            getSupportFragmentManager().popBackStack();

        } else {
            if (drawerFragment.isVisible()) {
                drawerFragment.closeDrawer();
                this.invalidateOptionsMenu();
            } else {
                if (doubleBackToExitPressedOnce) {
                    Dashboard.this.finish();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Press back again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_white, menu);

        Drawable drawable = menu.findItem(R.id.action_dots).getIcon();
        Drawable drawable2 = menu.findItem(R.id.search_dash).getIcon();
        if (drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        }

        if (drawable2 != null) {
            drawable2.mutate();
            //drawable2.setTint(Color.WHITE);
            drawable2.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_dots) {
            showRightMenuLangSelectionOption();
        } else if (id == R.id.search_dash) {
            startActivity(new Intent(this, SearchActivity.class));
        /*    Intent intent = new Intent(this, PoetDetailedActivity.class);
            intent.putExtra("poetId","EB9CB3D1-1DBD-4582-A1CA-E48FDAFE5BC8");
            intent.putExtra("ghazalCount",4+"");
            intent.putExtra("nazmCount",5+"");
            intent.putExtra("sherCount",7+"");

            this.startActivity(intent);*/
        }

        return super.onOptionsItemSelected(item);
    }

    public void initRightMenuPopUpDialog() {

        dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.actionbar_menu);
        dialog.setCanceledOnTouchOutside(true);
        Window window = dialog.getWindow();
        wlp = window.getAttributes();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        wlp.width = width / 2 + 150;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // for view selection
        rightMenuname = (TextView) dialog.findViewById(R.id.right_menuName);
        right_menuEmail = (TextView) dialog.findViewById(R.id.right_menuEmail);
        rightMenuMyFavorates = (TextView) dialog.findViewById(R.id.right_menu_myfav);
        rightMenuSettings = (TextView) dialog.findViewById(R.id.right_menu_setting);

        rightMenu_engBtn = (Button) dialog.findViewById(R.id.actionBarmenu_eng_lang);
        rightMenu_hinBtn = (Button) dialog.findViewById(R.id.actionBarmenu_hindi_lang);
        rightMenu_urduBtn = (Button) dialog.findViewById(R.id.actionBarmenu_urdu_lang);

        rightMenuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Dashboard.this, SettingActivity.class));
            }
        });

        rightMenuMyFavorates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, MyFavoritesActivity.class));
            }
        });


        rightMenuname.setText(CommonUtil.userName);
        right_menuEmail.setText(CommonUtil.userEmail);
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);


        rightMenu_engBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetActionBarPopUpViewLangButtonColor(1);
                langChanged();
                drawerFragment.resetButtonColor(1);
                dialog.dismiss();

            }
        });

        rightMenu_hinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(2);
                langChanged();
                drawerFragment.resetButtonColor(2);
                dialog.dismiss();
            }
        });

        rightMenu_urduBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetActionBarPopUpViewLangButtonColor(3);
                langChanged();
                drawerFragment.resetButtonColor(3);
                dialog.dismiss();
            }
        });

    }

    public void resetActionBarPopUpViewLangButtonColor(int langCode) {


        CommonUtil.languageCode = langCode;

        rightMenu_urduBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_urduBtn.setTextColor(Color.BLACK);
        rightMenu_hinBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_hinBtn.setTextColor(Color.BLACK);
        rightMenu_engBtn.setBackgroundResource(R.drawable.languagebtn);
        rightMenu_engBtn.setTextColor(Color.BLACK);

        if (langCode == 1) {
            rightMenu_engBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_engBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 2) {
            rightMenu_hinBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_hinBtn.setTextColor(Color.WHITE);
        }
        if (langCode == 3) {
            rightMenu_urduBtn.setBackgroundResource(R.color.colorPrimary);
            rightMenu_urduBtn.setTextColor(Color.WHITE);
        }
        saveTheLangCodeLocally();
        CommonUtil.setSpecificLanguage(rightMenuMyFavorates, "myFavorites", CommonUtil.languageCode);
        CommonUtil.setSpecificLanguage(rightMenuSettings, "setting", CommonUtil.languageCode);

        dialog.dismiss();

    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    public void showRightMenuLangSelectionOption() {
        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetActionBarPopUpViewLangButtonColor(1);
        } else {
            resetActionBarPopUpViewLangButtonColor(langIntCode);
        }

        if (CommonUtil.languageCode != 3) {
            wlp.gravity = Gravity.TOP | Gravity.RIGHT;
        } else {
            wlp.gravity = Gravity.TOP | Gravity.LEFT;
        }
        dialog.show();
    }

    @Override
    public void goToHomeScreen() {
        // intentionally left Blank..alerady on home
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youTubePlayerView);
    }

    public void changePoetName(int position, List<TodaysTop> todaysTopsModell) {

        shayerName.setText(todaysTopsModell.get(position).getPoetName());
    }
 /*   public double calculateTheScreenSize(){
        DisplayMetrics dm = getResources().getDisplayMetrics();
        double density = dm.density * 160;
        double x = Math.pow(dm.widthPixels / density, 2);
        double y = Math.pow(dm.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);
        return screenInches;
    }*/

    private PopupWindow initiatePopupWindow(List<ImageTags> imageTags) {

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.more_dialog_items, null);
        ListView listView = (ListView) view.findViewById(R.id.moreList);
        listView.setAdapter(new MoreDialogItemAdapter(Dashboard.this, imageTags));
        final PopupWindow popupWindow = new PopupWindow(Dashboard.this);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.showAtLocation(listView, Gravity.CENTER, 0, 0);

        popupWindow.setFocusable(true);
        popupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setContentView(view);

        return popupWindow;
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null)
            progressDialog.dismiss();
        super.onDestroy();
        handler.removeCallbacks(timeCounter);
    }

    public void shareURL(String tu) {

        if (CommonUtil.isSkipped) {
            CommonUtil.showToast(Dashboard.this);
        } else {
            if (tu != null) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");

                intent.putExtra(android.content.Intent.EXTRA_TEXT, tu);
                startActivity(Intent.createChooser(intent, "Share via"));
            } else {
                Toast.makeText(Dashboard.this, "Share Url Not Available", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void changeFavIcon(final int position, final List<TodaysTop> todaysTopsModell) {

        if (CommonUtil.isContentUsersFav(todaysTopsModell.get(position).getContentId())) {
            addtofav.setImageResource(R.drawable.ic_favorited);
            addtofav.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        } else {
            addtofav.setImageResource(R.drawable.ic_favorite);
            addtofav.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
        }

        addtofav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CommonUtil.isContentUsersFav(todaysTopsModell.get(position).getContentId())) {
                    CommonUtil.removeFromFavTroughApi(context, todaysTopsModell.get(position).getContentId());
                    addtofav.setImageResource(R.drawable.ic_favorite);
                    addtofav.setColorFilter(ContextCompat.getColor(context, R.color.lightGray), PorterDuff.Mode.SRC_IN);
                } else {
                    //add to local.......................................................
                    String sherContent = null, poetName = null;

                    sherContent = todaysTopsModell.get(position).getTitle();
                    poetName = todaysTopsModell.get(position).getPoetName();

                    CommonUtil.addToFavTroughApi(context, todaysTopsModell.get(position).getContentId());

                    final AddSher addSher = new AddSher();
                    addSher.setSher_content(sherContent);
                    addSher.setSher_auther(poetName);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            appDatabase.sherDao().insert(addSher);
                        }
                    }).start();

                    addtofav.setImageResource(R.drawable.ic_favorited);
                    addtofav.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                }


            }
        });

    }

    public void getMeaningOfTheWord(final String mainWord, String word) {


        final ProgressDialog loading = ProgressDialog.show(context, null, "Loading..", false, false);
        RestClient.get().getWordMeaning(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, word, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (loading != null && loading.isShowing())
                    loading.dismiss();
                JSONObject obj = new JSONObject(res);

                System.out.print(res.toString());
                try {
                    if (obj.getInt("S") == 1) {
                        Bundle bundle = new Bundle();
                        bundle.putString("data", mainWord);
                        bundle.putString("home", "home");
                        bundle.putString("response", obj.getJSONObject("R").toString());

                        meaningFragment = new MeaningFragment();
                        meaningFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .add(R.id.drawer_layout, meaningFragment, "meaningFrag")
                                .addToBackStack(null)
                                .commit();

                    } else {
                        Toast.makeText(Dashboard.this, "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (loading != null && loading.isShowing())
                    loading.dismiss();
            }
        });
    }

    public void removeFragment() {

        getSupportFragmentManager().popBackStack();
    }

    public void setDrawerEnabled(boolean enabled) {

        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawerLayout.setDrawerLockMode(lockMode);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            bottomCategoriesRecycler.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            //code for landscape mode
            bottomCategoriesRecycler.setLayoutManager(new GridLayoutManager(this, 4));
        }

        getDataFormServer(String.valueOf(CommonUtil.languageCode));
    }

    private void initViews() {

        homeBlank = (LinearLayout) findViewById(R.id.homeBlank);

        handler = new Handler();

        appDatabase = MyApplicationClass.getDb();

        int orientation = this.getResources().getConfiguration().orientation;

        exploreRecycler = (RecyclerView) findViewById(R.id.exploreRecycler);
        celebratingMainLayout = (LinearLayout) findViewById(R.id.celebratingMainLayout);
        bottomCategoriesRecycler = (RecyclerView) findViewById(R.id.bottomCategoriesRecycler);
        exploreRecycler.setNestedScrollingEnabled(false);
        bottomCategoriesRecycler.setNestedScrollingEnabled(false);

        wordDayMainLinear = (LinearLayout) findViewById(R.id.wordDayMainLinear);

        celebratingMainCardLayout = (CardView) findViewById(R.id.celebratingMainCardLayout);

        //testingNextArrow = (AppCompatImageView) findViewById(R.id.nextSherarrow);

        nextSherarrow = (TextView) findViewById(R.id.readMoreSher);


        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            bottomCategoriesRecycler.setLayoutManager(new GridLayoutManager(this, 2));
        } else {
            //code for landscape mode
            bottomCategoriesRecycler.setLayoutManager(new GridLayoutManager(this, 4));
        }

        youTubeView = (YouTubeThumbnailView) findViewById(R.id.youTubePlayerView);

        int numberOfColumns = 2;
        exploreRecycler.setLayoutManager(new GridLayoutManager(this, numberOfColumns));

        engLatoBold = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/LatoBold.ttf");
        engLatoRegular = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/Lato-Regular.ttf");
        engLatoSemiBold = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/Lato-SemiBold.ttf");
        engMeriWheatherItalic = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/merriweather_light_italic.ttf");
        EngLatoXBoldItalic = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/LatoX-Bold.ttf");
        EngLatoRegularX = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/LatoX-Regular.ttf");


        hinditf = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/NotoSansDevanagari-Regular.ttf");
        urdutf = Typeface.createFromAsset(Dashboard.this.getAssets(), "fonts/NotoNastaliqUrdu-Regular.ttf");

        featuredtText = (LinearLayout) findViewById(R.id.featured_sher_text);
        celebratingTextSher = (LinearLayout) findViewById(R.id.celebratingTextSher);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            toolbar.setBackground(getResources().getDrawable(R.drawable.background_toolbar_translucent));
        }

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);

        drawerFragment = (NavigationFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        homeViewPager = (AutoViewPager1) findViewById(R.id.home_pager);

        homeViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                currentIndex = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}
