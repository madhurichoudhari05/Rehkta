package org.Rekhta;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.Rekhta.activites.Dashboard;
import org.Rekhta.utils.CustomTypefaceSpan;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;

import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import org.Rekhta.utils.ValidationHelper;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    TextView sign_up, forget_password, serverResponseMsg, email_err_msg;
    String ace;
    EditText userEmail, userPassword;
    Button login_Button;
    RelativeLayout btnfb;
    RelativeLayout button_google;
    SharedPreferences DataPrefs = null;
    LinearLayout linear2, forget_layout;
    ValidationHelper validation;
    private Typeface latoRegualr, latoBold;
    TextView skiptxt;
    String fbAccessToken = "";
    String userNameFromGoogle = "";
    String userEmailIdfromGoogle = "";

    LinearLayout userEmailLinear, userPassLinear;
    ;
    RelativeLayout fbLoginLayout;
    private TextInputLayout textInputLayoutuserEmail;
    private TextInputLayout textInputLayoutuserPassword;
    private SignInButton signInButton;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 100;
    private LoginButton loginButton;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private CallbackManager callbackManager;
    boolean iskeyboardOpen = false;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            fbAccessToken = loginResult.getAccessToken().getToken();
            Log.e("fbAccessToken", fbAccessToken);
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.v("LoginActivity", response.toString());

                            // Application code
                            AccessToken token = AccessToken.getCurrentAccessToken();
                            if (token != null) {
                                //  Toast.makeText(MainActivity2.this, "token: "+token, Toast.LENGTH_SHORT).show();
                                Log.d("access token is: ", String.valueOf(token));
                                Log.d("access token is: ", String.valueOf(token.getSource()));
                                Log.d("access token is: ", String.valueOf(token.getApplicationId()));
                                Log.d("access token is: ", String.valueOf(token.getUserId()));
                            }
                            try {
                                Log.e("tttttt", object.getString("id"));
                                String birthday = "";
                                if (object.has("birthday")) {
                                    birthday = object.getString("birthday");
                                }

                                String mail;

                                String fnm = object.getString("first_name");
                                String lnm = object.getString("last_name");
                                String gender = object.getString("gender");
                                String fid = object.getString("id");

                                if (object.has("email")) {
                                    mail = object.getString("email");
                                    if (mail != null) {
                                        Log.e("dtealis", "=" + "Name: " + fnm + " " + lnm + " \n" + "Email: " + mail + " \n" + "Gender: " + gender + " \n" + "ID: " + fid + " \n" + "Birth Date: " + birthday);
                                        Log.d("aswwww", "https://graph.facebook.com/" + fid + "/picture?type=large");
                                        //btnfb.setBackgroundResource(R.drawable.fb_logout);
                                        LoginFromFaceBookOrGooglePlus(fnm + " " + lnm, "Facebook", fbAccessToken, mail);
                                        System.out.print("-------------------------------------------" + fid);
                                    } else {
                                        Toast.makeText(MainActivity.this, "Email Id not found", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(MainActivity.this, "Email Id not found", Toast.LENGTH_SHORT).show();
                                }


                                //Toast.makeText(MainActivity2.this, "fid : " + fid, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location");
            request.setParameters(parameters);
            request.executeAsync();

        }

        @Override
        public void onCancel() {
            // Toast.makeText(MainActivity.this, "Login Cancelled", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(MainActivity.this, "Login Error", Toast.LENGTH_SHORT).show();
        }
    };
    private ProgressDialog progressLoader;


    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        setContentView(R.layout.activity_login);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().getDecorView().clearFocus();
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        initViews();
        loginButton = (LoginButton) findViewById(R.id.btnfacebook);
        loginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);


        userEmailLinear = (LinearLayout) findViewById(R.id.userEmailLinear);
        userPassLinear = (LinearLayout) findViewById(R.id.userPassLinear);
        callbackManager = CallbackManager.Factory.create();
        fbLoginLayout = (RelativeLayout) findViewById(R.id.btnfb);
        fbLoginLayout.setOnClickListener(this);
        skiptxt = (TextView) findViewById(R.id.skiptxt);

       // skiptxt.setBackgroundDrawable(getResources().getDrawable(R.drawable.touch_effect));

        skiptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Dashboard.class));
                CommonUtil.isSkipped = true;
                finish();
            }
        });

        skiptxt.setTypeface(latoRegualr);

        accessTokenTracker = new AccessTokenTracker() {


            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

            }
        };

        hideKeyBoard();
        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        loginButton.registerCallback(callbackManager, callback);
        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        //forget_password.setText(Html.fromHtml(getString(R.string.forget_password)));
        //sign_up.setText(Html.fromHtml(getString(R.string.new_user)));
        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, SingUp.class);
                startActivity(intent);
                finish();
            }
        });
        forget_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ForgetPassword.class);
                startActivity(intent);
            }
        });

        userEmailLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    userPassLinear.setBackground(getResources().getDrawable(R.drawable.textlayouthint));
                }
            }
        });

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


    }

    private void initViews() {
        keyHashfind();
        validation = new ValidationHelper(this);
        button_google = (RelativeLayout) findViewById(R.id.button_google_login);
        linear2 = (LinearLayout) findViewById(R.id.linear2);
        forget_layout = (LinearLayout) findViewById(R.id.forget_layout);
        textInputLayoutuserEmail = (TextInputLayout) findViewById(R.id.text_input_layout_name);
        textInputLayoutuserPassword = (TextInputLayout) findViewById(R.id.text_input_layout_name);
        //btnfb = (RelativeLayout) findViewById(R.id.btnfb);
        sign_up = (TextView) findViewById(R.id.sign_up);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.sign_in_request_token))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this)
                .build();

        mGoogleApiClient.connect();


        signInButton = (SignInButton) findViewById(R.id.btn_googleplus);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        //fonts used
        Typeface engtf = Typeface.createFromAsset(this.getAssets(), "fonts/Merriweather-Light.ttf");
        latoBold = Typeface.createFromAsset(this.getAssets(), "fonts/LatoBold.ttf");
        latoRegualr = Typeface.createFromAsset(this.getAssets(), "fonts/LatoRegular.ttf");


        //Initializing google api client

        //Setting onclick listener to signing button
        button_google.setOnClickListener(this);
        //  btnfb.setOnClickListener(this);
        signInButton.setOnClickListener(this);
        forget_password = (TextView) findViewById(R.id.forget_password);
        forget_password.setTypeface(latoRegualr);
        userEmail = (EditText) findViewById(R.id.signInEmail);
        userEmail.setTypeface(latoRegualr);
        userEmail.setImeOptions(EditorInfo.IME_ACTION_NEXT);

        userEmail.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        userPassword = (EditText) findViewById(R.id.signInPassword);

        userPassword.setTypeface(latoRegualr);

        setOnTextListener();
        login_Button = (Button) findViewById(R.id.btnLogin);
        login_Button.setTypeface(latoRegualr);
        userEmail.addTextChangedListener(new MyTextWatcher(userEmail));
        userEmail.setSingleLine();
        userEmail.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        userPassword.addTextChangedListener(new MyTextWatcher(userPassword));
        //serverResponseMsg = (TextView) findViewById(R.id.serverResponse);
        //email_err_msg = (TextView) findViewById(R.id.email_errormsg);

        userPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    hideErrorMsg();
                    hideKeyBoard();
                    automaticHideErrorMsg();
                    checkInputFields();
                }
                return false;
            }
        });
        // font setting
        //userEmail.setTypeface(engtf);
        userPassword.setTypeface(engtf);
        //login_Button.setTypeface(btntf2);

        login_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideErrorMsg();
                hideKeyBoard();
                automaticHideErrorMsg();
                //checkValidation();
                checkInputFields();
            }


        });
    }

    private void setOnTextListener() {

        userEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.textlayouthint));
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void afterTextChanged(Editable s) {
                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.textlayouthint));
            }
        });

        userPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userPassLinear.setBackground(getResources().getDrawable(R.drawable.textlayouthint));
            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void afterTextChanged(Editable s) {
                userPassLinear.setBackground(getResources().getDrawable(R.drawable.textlayouthint));
            }
        });
    }

    private void googlesignIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //For Simple Login
    public void signIn() {
/* refrence Keys to be sent in body
{
    "Email":"ras@r.com",
	"Password":"Testtest123",
	"RememberMe":"true",
    "DeviceParams":"db231a850a2c5d6a,MotoG3,33dfa584-1f03-428c-b0f8-7a2c11089779,Android",
    "Language":"1"
}*/
        progressLoader = ProgressDialog.show(MainActivity.this, null, "Please wait...", false, false);


        HashMap obj = new HashMap();
        obj.put("Email", userEmail.getText().toString());
        obj.put("Password", userPassword.getText().toString());
        obj.put("RememberMe", "true");
        obj.put("DeviceParams", CommonUtil.SharedPrefKeys.DeviceParams);
        obj.put("Language", 1);

        RestClient.get().login(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) {
                    progressLoader.dismiss();
                }
                if (response != null) {
                    try {
                        JSONObject obj = new JSONObject(res);
                        if (obj.getInt("S") == 1) {

                            Intent intent = new Intent(MainActivity.this, Dashboard.class);
                            JSONObject userInfo = new JSONObject(obj.getJSONObject("R").toString());
                            intent.putExtra("username", userInfo.getString("DisplayName"));
                            CommonUtil.userName = userInfo.getString("DisplayName");
                            CommonUtil.userEmail = userEmail.getText().toString();
                            CommonUtil.SharedPrefKeys.rekhtasAccessToken = "Bearer " + userInfo.getJSONObject("TokenDetails").getString("access_token");
                            DataPrefs.edit().putBoolean("loggedIn", true).putString("userName", userInfo.getString("DisplayName")).putString("userEmail", userEmail.getText().toString()).commit();
                            startActivity(intent);
                            CommonUtil.isSkipped = false;
                            finish();
                        } else {
                            // serverResponseMsg.setVisibility(View.VISIBLE);
                            //serverResponseMsg.setText(obj.getString("Me"));
                            // serverResponseMsg.setText("Invalid email or password");
                            Toast.makeText(MainActivity.this, "Invalid email or password", Toast.LENGTH_LONG).show();
                        }

                        System.out.print("-----------------------------------------------------------");
                        System.out.print(response);
                        System.out.print(res);

                    } catch (Exception e) {

                        e.printStackTrace();

                    }
                } else {
                    Toast.makeText(MainActivity.this, "An Error Ocurred", Toast.LENGTH_LONG).show();
                }

            }


            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing())
                    progressLoader.dismiss();
                Toast.makeText(MainActivity.this, "An Error Ocurred", Toast.LENGTH_LONG).show();
                // serverResponseMsg.setVisibility(View.VISIBLE);
                // serverResponseMsg.setText("An Error Ocurred");
            }
        });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnfb:

                LoginManager.getInstance().logOut();
                loginButton.performClick();
                break;
            case R.id.button_google_login:
                googlesignIn();
                break;
        }

    }

    private void keyHashfind() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("org.Rekhta", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                String ff=Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT);
                Log.e("key Hash", Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            Log.e("ProfileName", profile.getName());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    private void handleSignInResult(GoogleSignInResult result) {

        //If the login succeed
        System.out.print("Status of Result: " + result.toString());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            ace = result.getSignInAccount().getIdToken();
            System.out.print(ace + "=========================" + acct.getId() + "========================="
                    + acct.getIdToken() + "=======" + result.getSignInAccount().getServerAuthCode() + result.getSignInAccount().getId());

            Log.e("======== 1 : ", "" + ace);
            Log.e("======== 2 : ", "" + acct.getId());
            Log.e("======== 3 : ", "" + acct.getIdToken());
            Log.e("======== 4 ", "" + acct.getServerAuthCode());

            userEmailIdfromGoogle = acct.getEmail();
            userNameFromGoogle = acct.getDisplayName();
            new RetrieveTokenTask().execute(userEmailIdfromGoogle);

        } else {
            //If login fails
            Toast.makeText(this, "Login Cancelled! Please Try Again", Toast.LENGTH_LONG).show();
        }
    }

    public void LoginFromFaceBookOrGooglePlus(String userName, String provide, String externalToken, final String emailId) {

        final ProgressDialog progressLoader = ProgressDialog.show(this, null, "Please wait...", false, false);

        HashMap obj = new HashMap();
        obj.put("UserName", userName);
        obj.put("Provider", provide);
        obj.put("ExternalAccessToken", externalToken);
        obj.put("DeviceParams", CommonUtil.SharedPrefKeys.DeviceParams);
        obj.put("Email", emailId);
        Log.e("objFB", obj.toString());
        Log.e("objUniqueId", CommonUtil.SharedPrefKeys.UniqueId);

        RestClient.get().externalLoginFbGplus(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing())
                    progressLoader.dismiss();
                try {
                    JSONObject obj1 = new JSONObject(res);
                    Log.e("objFB2", obj1.toString());
                    if (obj1.getInt("S") == 1) {

                        Intent intent = new Intent(MainActivity.this, Dashboard.class);
                        JSONObject userInfo = new JSONObject(obj1.getJSONObject("R").toString());
                        intent.putExtra("username", userInfo.getString("DisplayName"));
                        CommonUtil.SharedPrefKeys.rekhtasAccessToken = "Bearer " + userInfo.getJSONObject("TokenDetails").getString("access_token");
                        CommonUtil.userName = userInfo.getString("DisplayName");
                        CommonUtil.userEmail = emailId;
                        //DataPrefs.edit().putBoolean("loggedIn", true).putString("userName", userInfo.getString("DisplayName")).commit();
                        DataPrefs.edit().putBoolean("loggedIn", true).putString("userName", userInfo.getString("DisplayName")).putString("userEmail", emailId).commit();
                        startActivity(intent);
                        CommonUtil.isSkipped = false;
                        finish();
                    } else {

                        //serverResponseMsg.setVisibility(View.VISIBLE);
                        ///serverResponseMsg.setText(obj.getString("Me"));
                        Toast.makeText(MainActivity.this, "Invalid External Token", Toast.LENGTH_SHORT).show();
                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                Toast.makeText(MainActivity.this, "An Error Ocurred", Toast.LENGTH_SHORT).show();
                // serverResponseMsg.setVisibility(View.VISIBLE);
                // serverResponseMsg.setText("An Error Ocurred");
            }
        });
    }

    public void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public void hideErrorMsg() {
        //serverResponseMsg.setVisibility(View.GONE);
        //email_err_msg.setVisibility(View.GONE);
    }

    public void automaticHideErrorMsg() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideErrorMsg();
            }
        }, 3000);
    }

    private void checkValidation() {
        if (!validation.isEditTextFilled(userEmail, textInputLayoutuserEmail, "Please enter your email Id")) {
            return;
        }

        if (!validation.isEditTextEmail(userPassword, textInputLayoutuserPassword, "Please enter your password")) {
            return;
        }


    }

    private boolean validateEmail() {
        String email = userEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            textInputLayoutuserEmail.setError("Please enter your email Id");
            //requestFocus(userEmail);
            return false;
        } else {
            textInputLayoutuserEmail.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validatePassword() {
        if (userPassword.getText().toString().trim().isEmpty()) {
            textInputLayoutuserPassword.setError("Please enter your password");
            requestFocus(userPassword);
            return false;
        } else {
            textInputLayoutuserPassword.setErrorEnabled(false);
        }

        return true;
    }

    public void checkInputFields() {

        boolean isvalidEmailEntered = CommonUtil.isValidEmail(userEmail.getText().toString());
        boolean isvalidPasswordEntered = CommonUtil.isValidPassword(userPassword.getText().toString());

        if (userEmail.getText().toString().equalsIgnoreCase("")) {
            //email_err_msg.setVisibility(View.VISIBLE);
            // textInputLayoutuserEmail.setError("Please enter your email Id");
            // email_err_msg.setText("Please enter your email Id");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));

            }

          /*  serverResponseMsg.setVisibility(View.VISIBLE);
            serverResponseMsg.setText("invalid email or password. Sign up for an account.");*/

        } else if (userPassword.getText().toString().equalsIgnoreCase("")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userPassLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }
        } else if (!CommonUtil.isValidEmail(userEmail.getText().toString())) {
            //email_err_msg.setVisibility(View.VISIBLE);
            // email_err_msg.setText("Please enter valid email Id");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }
        } else if (!userEmail.getText().toString().equalsIgnoreCase("") && userPassword.getText().toString().equalsIgnoreCase("")) {
            // serverResponseMsg.setVisibility(View.VISIBLE);
            // serverResponseMsg.setText("Please enter your password");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                // userEmailLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
                //userPassLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }

        } else if (userEmail.getText().toString().equalsIgnoreCase("") && isvalidPasswordEntered) {
            // serverResponseMsg.setVisibility(View.VISIBLE);
            //serverResponseMsg.setText("invalid email id. Sign up for an account.");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }
        } else if (!isvalidEmailEntered && !isvalidPasswordEntered) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
                userPassLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }
            // serverResponseMsg.setVisibility(View.VISIBLE);
            // serverResponseMsg.setText("invalid email id. Sign up for an account.");
        } else if (!isvalidEmailEntered && isvalidPasswordEntered) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                userEmailLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
                userPassLinear.setBackground(getResources().getDrawable(R.drawable.text_error_layout));
            }
            //serverResponseMsg.setVisibility(View.VISIBLE);
            // serverResponseMsg.setText("invalid email id. Sign up for an account.");
        } /*else if (isvalidEmailEntered && !isvalidPasswordEntered) {
            serverResponseMsg.setVisibility(View.VISIBLE);
            serverResponseMsg.setText("invalid password. Did you forget your password?");
        }*/ else {
            signIn();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyBoard();
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Log.e("get the hell Out here", status.toString());
            }
        });

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            switch (view.getId()) {
                case R.id.signInEmail:
                    hideErrorMsg();
                    break;
                case R.id.signInPassword:
                    hideErrorMsg();
                    break;
            }
        }

        public void afterTextChanged(Editable editable) {

        }
    }

    private class RetrieveTokenTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String accountName = params[0];
            String scopes = "oauth2:profile email";
            String token = null;
            try {
                token = GoogleAuthUtil.getToken(getApplicationContext(), accountName, scopes);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), RC_SIGN_IN);
            } catch (GoogleAuthException e) {
                e.printStackTrace();
            }
            return token;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LoginFromFaceBookOrGooglePlus(userNameFromGoogle, "Google", s, userEmailIdfromGoogle);
        }
    }


}




