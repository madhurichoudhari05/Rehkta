package org.Rekhta;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.Rekhta.activites.Dashboard;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;

import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SingUp extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    TextView sign_up_login, messageFromServer, name_errorMsg, email_errorMsg;
    EditText userName, userPassword, userEmailId;
    Button signUpButton;
    RelativeLayout button_google;
    RelativeLayout btnfb;
    LinearLayout linear2;
    SharedPreferences DataPrefs = null;
    String fbAccessToken = "";
    String userNameFromGoogle = "";
    String userEmailIdfromGoogle = "";
    private SignInButton signInButton;
    private LoginButton loginButton;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private CallbackManager callbackManager;
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 100;
    LinearLayout nameRegisterLinear, emailRegisterLinear, passRegisterLinear;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            fbAccessToken = loginResult.getAccessToken().getToken();
            Log.e("fbAccessToken", fbAccessToken);
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.v("LoginActivity", response.toString());

                            // Application code
                            try {
                                Log.d("tttttt", object.getString("id"));
                                String birthday = "";
                                if (object.has("birthday")) {
                                    birthday = object.getString("birthday");
                                }

                                String fnm = object.getString("first_name");
                                String lnm = object.getString("last_name");
                                String mail = object.getString("email");
                                String gender = object.getString("gender");
                                String fid = object.getString("id");
                                Log.e("dtealis", "=" + "Name: " + fnm + " " + lnm + " \n" + "Email: " + mail + " \n" + "Gender: " + gender + " \n" + "ID: " + fid + " \n" + "Birth Date: " + birthday);
                                // aQuery.id(ivpic).image("https://graph.facebook.com/" + fid + "/picture?type=large");
                                //https://graph.facebook.com/143990709444026/picture?type=large
                                Log.d("aswwww", "https://graph.facebook.com/" + fid + "/picture?type=large");
                                btnfb.setBackgroundResource(R.drawable.fb_logout);
                                LoginFromFaceBookOrGooglePlus(fnm + " " + lnm, "Facebook", fbAccessToken, mail);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email, gender, birthday, location");
            request.setParameters(parameters);
            request.executeAsync();

        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onError(FacebookException error) {
        }
    };
    private TextView skiptxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_signup);
        linear2 = (LinearLayout) findViewById(R.id.linear2);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
        button_google = (RelativeLayout) findViewById(R.id.button_google_login);
        btnfb = (RelativeLayout) findViewById(R.id.btnfb);
        btnfb.setOnClickListener(this);
        button_google.setOnClickListener(this);
        sign_up_login = (TextView) findViewById(R.id.sign_up_login);
        //sign_up_login.setText(Html.fromHtml(getString(R.string.already_have_aacount)));
        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SingUp.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        initWidegets();

        loginButton = (LoginButton) findViewById(R.id.btnfacebook);
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));
        loginButton.registerCallback(callbackManager, callback);

        hideKeyBoard();

        userPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    //hideErrorMsgs();
                    //automaticHideErrorMsg();
                    validateAndShowMessageBeforeSignUp();
                }
                return false;
            }
        });

    }

    private void initWidegets() {
        keyHashfind();

        //fonts used
        Typeface btntf = Typeface.createFromAsset(this.getAssets(), "fonts/LatoBold.ttf");
        Typeface latoRegular = Typeface.createFromAsset(this.getAssets(), "fonts/Lato-Regular.ttf");

        userName = (EditText) findViewById(R.id.signUpname);
        userEmailId = (EditText) findViewById(R.id.signUpemail);
        userEmailId.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        userPassword = (EditText) findViewById(R.id.signUppassword);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            userPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,R.drawable.ic_popup_icon,0);
        }

        userName.setTypeface(latoRegular);
        userEmailId.setTypeface(latoRegular);
        userPassword.setTypeface(latoRegular);

        nameRegisterLinear = (LinearLayout) findViewById(R.id.NameRegisterLinear);
        emailRegisterLinear = (LinearLayout) findViewById(R.id.emailRegisterLinear);
        passRegisterLinear = (LinearLayout) findViewById(R.id.passwordRegisterLinear);

        addTextListeners();
      //  messageFromServer = (TextView) findViewById(R.id.messageFromServer);
        // name_errorMsg = (TextView) findViewById(R.id.signUpname_errormsg);
        //email_errorMsg = (TextView) findViewById(R.id.signUpemail_errmsg);
        signUpButton = (Button) findViewById(R.id.btnSignUp);

        signUpButton.setTypeface(btntf);

        /*userPassword.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (userPassword.getRight() - userPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        CheckPassWordFormat(userPassword.getText().toString(), 1);
                        return true;
                    }
                }
                return false;
            }
        });*/
        signUpButton = (Button) findViewById(R.id.btnSignUp);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // hideErrorMsgs();
                //automaticHideErrorMsg();
                validateAndShowMessageBeforeSignUp();
            }
        });

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.sign_in_request_token))
                .requestEmail()
                .build();
        signInButton = (SignInButton) findViewById(R.id.btn_googleplus);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(gso.getScopeArray());

        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // .enableAutoManage( SingUp.this/* FgragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        signInButton.setOnClickListener(this);

    }

    private void addTextListeners() {

        userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                nameRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));
            }

            @Override
            public void afterTextChanged(Editable s) {
                nameRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));
            }
        });

        userEmailId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                emailRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));
            }


            @Override
            public void afterTextChanged(Editable s) {

                emailRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));
            }
        });

        userPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                passRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));

            }

            @Override
            public void afterTextChanged(Editable s) {


                passRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.textlayouthint));

            }
        });
    }

    private void googlesignIn() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnfb:
                loginButton.performClick();
                break;
            case R.id.button_google_login:
                googlesignIn();
                break;
        }
    }


    private void keyHashfind() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("org.Rekhta", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.e("key Hash", Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            Log.e("ProfileName", profile.getName());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        System.out.print("Status of Result: " + result.toString());
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            userEmailIdfromGoogle = acct.getEmail();
            userNameFromGoogle = acct.getDisplayName();
            new RetrieveTokenTask().execute(userEmailIdfromGoogle);

        } else {
            //If login fails
           /* userEmailIdfromGoogle = "bindaasbinod@gmail.com";
            userNameFromGoogle = "mr testing";
            new RetrieveTokenTask().execute(userEmailIdfromGoogle); */
            Toast.makeText(this, "Login failed! Please try again", Toast.LENGTH_LONG).show();
        }
    }

    public void signUp() {
        final ProgressDialog progressLoader = ProgressDialog.show(this, null, "Please wait...", false, false);
     //   messageFromServer.setVisibility(View.GONE);

        HashMap obj = new HashMap();
        obj.put("Email", userEmailId.getText().toString());
        obj.put("Password", userPassword.getText().toString());
        obj.put("ConfirmPassword", userPassword.getText().toString());
        obj.put("DisplayName", userName.getText().toString());
        obj.put("IsRequestedNewsLetter", "true");
        obj.put("DeviceParams", CommonUtil.SharedPrefKeys.DeviceParams);
        obj.put("IsSuccess", "true");

        RestClient.get().register(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {
                        // DataPrefs.edit().putBoolean("loggedIn", true).commit();
                        JSONObject userInfo = new JSONObject(obj.getJSONObject("R").toString());
                        CommonUtil.userName = userName.getText().toString();
                        CommonUtil.userEmail = userEmailId.getText().toString();
                        DataPrefs.edit().putBoolean("loggedIn", true).putString("userName", userName.getText().toString()).putString("userEmail", userEmailId.getText().toString()).commit();
                        CommonUtil.SharedPrefKeys.rekhtasAccessToken = "Bearer " + userInfo.getJSONObject("TokenDetails").getString("access_token");
                        Intent intent = new Intent(SingUp.this, Dashboard.class);
                        startActivity(intent);
                        finish();
                    } else {
//                        messageFromServer.setVisibility(View.VISIBLE);
//                        if (obj.getString("Me").indexOf("already registered with") >= -1) {
//                            Toast.makeText(SingUp.this, "you have already registered with this email id", Toast.LENGTH_SHORT).show();
////                            messageFromServer.setText("");
//                        } else {
//                            messageFromServer.setText(obj.getString("Me"));
                            Toast.makeText(SingUp.this, obj.getString("Me").toString(), Toast.LENGTH_SHORT).show();

//                        }

                    }

                    System.out.print("-----------------------------------------------------------");
                    System.out.print(response);
                    System.out.print(res);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(SingUp.this, "ERROR IN API", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void CheckPassWordFormat(CharSequence userPass, int showPasswordClickedFlag) {
        boolean hasPassLength = false;
        boolean hasNumber = false;
        boolean hasUpperCase = false;
        if (userPass.length() > 5) {
            hasPassLength = true;
        }
        if (CommonUtil.passwordPatternNumber.matcher(userPass).find()) {
            hasNumber = true;
        }
        if (CommonUtil.passwordPatternUpperCase.matcher(userPass).find()) {
            hasUpperCase = true;
        }
       /* if (CommonUtil.passwordPatternSpecialChar.matcher(userPass).find()) {
            hasSpecialChar = true;
        }*/
        if (hasPassLength && hasNumber && hasUpperCase) {
            if (showPasswordClickedFlag == 1) {
                showPasswordDialog(hasPassLength, hasUpperCase, hasNumber);
            } else {
                signUp();
            }

        } else {
            showPasswordDialog(hasPassLength, hasUpperCase, hasNumber);
        }

    }

    public void showPasswordDialog(boolean hasPassLen, boolean hasUppercase, boolean hasNumber) {
        // custom dialog
        final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogAnimation));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_validation_alert);
        ImageView pass_lengthIcon = (ImageView) dialog.findViewById(R.id.pass_lengthIcon);
        ImageView upperCaseimage = (ImageView) dialog.findViewById(R.id.uppercaseIcon);
        ImageView numberimage = (ImageView) dialog.findViewById(R.id.numberIcon);
        TextView closeText = (TextView) dialog.findViewById(R.id.textView2);
        if (hasPassLen) {
            pass_lengthIcon.setImageResource(R.drawable.tick);
        }
        if (hasUppercase) {
            upperCaseimage.setImageResource(R.drawable.tick);
        }

        if (hasNumber) {
            numberimage.setImageResource(R.drawable.tick);
        }

       /* if (hasSpecialchar) {
            specialCharimage.setImageResource(R.drawable.tick);
        }*/
        dialog.setCanceledOnTouchOutside(true);
        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void LoginFromFaceBookOrGooglePlus(final String userName, String provide, String externalToken, final String emailId) {

        final ProgressDialog progressLoader = ProgressDialog.show(this, null, "Please wait...", false, false);

        HashMap obj = new HashMap();
        obj.put("UserName", userName);
        obj.put("Provider", provide);
        obj.put("ExternalAccessToken", externalToken);
        obj.put("DeviceParams", CommonUtil.SharedPrefKeys.DeviceParams);
        obj.put("Email", emailId);

        RestClient.get().externalLoginFbGplus(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, CommonUtil.SharedPrefKeys.UniqueId, obj, new Callback<HashMap>() {
            @Override
            public void success(HashMap res, Response response) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
                try {
                    JSONObject obj = new JSONObject(res);
                    if (obj.getInt("S") == 1) {


                        Intent intent = new Intent(SingUp.this, Dashboard.class);
                        JSONObject userInfo = new JSONObject(obj.getJSONObject("R").toString());
                        CommonUtil.userName = userName;
                        CommonUtil.userEmail = emailId;
                        CommonUtil.SharedPrefKeys.rekhtasAccessToken = "Bearer " + userInfo.getJSONObject("TokenDetails").getString("access_token");
                        intent.putExtra("username", userInfo.getString("DisplayName"));
                        DataPrefs.edit().putBoolean("loggedIn", true).putString("userName", userInfo.getString("DisplayName")).putString("userEmail", emailId).commit();
                        startActivity(intent);
                        finish();
                    } else {
//                        messageFromServer.setVisibility(View.VISIBLE);
                        Toast.makeText(SingUp.this, obj.getString("Me"), Toast.LENGTH_SHORT).show();
//                        messageFromServer.setText(obj.getString("Me"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void failure(RetrofitError error) {
                if (progressLoader != null && progressLoader.isShowing()) progressLoader.dismiss();
//                messageFromServer.setVisibility(View.VISIBLE);
//                messageFromServer.setText("An Error Ocurred");
                Toast.makeText(SingUp.this, "An Error Occured", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(SingUp.this, MainActivity.class);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        startActivity(intent);
        finish();

    }

    public void hideErrorMsgs() {
        email_errorMsg.setVisibility(View.GONE);
        messageFromServer.setVisibility(View.GONE);
        name_errorMsg.setVisibility(View.GONE);
    }

    public void automaticHideErrorMsg() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //hideErrorMsgs();
            }
        }, 3000);
    }

    public void validateAndShowMessageBeforeSignUp() {
        boolean isValidName = CommonUtil.isValidName(userName.getText().toString());
        boolean isValidEmail = CommonUtil.isValidEmail(userEmailId.getText().toString());
        ;
        boolean isValidPassword = CommonUtil.isValidPassword(userPassword.getText().toString());

        if (userName.getText().toString().equalsIgnoreCase("")) {
            //name_errorMsg.setVisibility(View.VISIBLE);
            //name_errorMsg.setText("Please enter your name");
            nameRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_error_layout));

        } else if (!userName.getText().toString().equalsIgnoreCase("") && !isValidName) {
            // name_errorMsg.setVisibility(View.VISIBLE);
            //name_errorMsg.setText("The name should contain only alphabets");
            nameRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_error_layout));

        } else if (userEmailId.getText().toString().equalsIgnoreCase("")) {
            // email_errorMsg.setVisibility(View.VISIBLE);
            // email_errorMsg.setText("Please enter your email Id");
            emailRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_error_layout));

        } else if (!userEmailId.getText().toString().equalsIgnoreCase("") && !isValidEmail) {
            // email_errorMsg.setVisibility(View.VISIBLE);
            // email_errorMsg.setText("Please enter valid email Id");
            emailRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_error_layout));

        } else if (userPassword.getText().toString().equalsIgnoreCase("")) {
            // messageFromServer.setVisibility(View.VISIBLE);
            //messageFromServer.setText("Please enter your password");
            passRegisterLinear.setBackgroundDrawable(getResources().getDrawable(R.drawable.text_error_layout));

        } else if (!userPassword.getText().toString().equalsIgnoreCase("") && !isValidPassword) {
            CheckPassWordFormat(userPassword.getText().toString(), 1);
           /* messageFromServer.setVisibility(View.VISIBLE);
            messageFromServer.setText("Please enter valid password");
*/
/*
       *//*

        else if (!isValidName && isValidPassword) {
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("The name should contain only alphabets");

        } else if (isValidName && isValidPassword && userEmailId.getText().toString().equalsIgnoreCase("")) {
            email_errorMsg.setVisibility(View.VISIBLE);
            email_errorMsg.setText("Please enter your email ID");

        } else if (isValidName && isValidEmail && userPassword.getText().toString().equalsIgnoreCase("")) {
            messageFromServer.setVisibility(View.VISIBLE);
            messageFromServer.setText("Please enter your password");

        } else if (isValidName && userEmailId.getText().toString().equalsIgnoreCase("") && userPassword.getText().toString().equalsIgnoreCase("")) {
            email_errorMsg.setVisibility(View.VISIBLE);
            email_errorMsg.setText("Please enter your email ID");
        } else if (isValidEmail && userName.getText().toString().equalsIgnoreCase("") && userPassword.getText().toString().equalsIgnoreCase("")) {
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("Please enter your name");

        } else if (userName.getText().toString().equalsIgnoreCase("") && userEmailId.getText().toString().equalsIgnoreCase("") && isValidPassword) {
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("Please enter your name");

        } else if (!isValidName && userEmailId.getText().toString().equalsIgnoreCase("") && userPassword.getText().toString().equalsIgnoreCase("")) { // secenerio 10
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("The name should contain only alphabets");

        } else if (!isValidName && !isValidEmail && !isValidPassword) { // senerio 9
            messageFromServer.setVisibility(View.VISIBLE);
            messageFromServer.setText(R.string.password_error);

        } else if (!isValidName && !isValidEmail && userPassword.getText().toString().equalsIgnoreCase("")) { // secenerio 11
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("The name should contain only alphabets");
        } else if (!isValidName && !isValidPassword && isValidEmail) { // secenerio 12
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("The name should contain only alphabets");
        } else if (userName.getText().toString().equalsIgnoreCase("") && userPassword.getText().toString().equalsIgnoreCase("") && !isValidEmail) { // secenerio 13
            name_errorMsg.setVisibility(View.VISIBLE);
            name_errorMsg.setText("Please enter your name");
        } else if (isValidName && isValidPassword && !isValidEmail) { // secenrio 14
            email_errorMsg.setVisibility(View.VISIBLE);
            email_errorMsg.setText("Please enter valid email ID");*//*

        } else if (isValidName && isValidEmail && !isValidPassword) { //secenrio 16

            messageFromServer.setVisibility(View.VISIBLE);
            if (userPassword.getText().toString().length() < 6) {

                if (CommonUtil.passwordPatternNumber.matcher(userPassword.getText().toString()).find() && CommonUtil.passwordPatternUpperCase.matcher(userPassword.getText().toString()).find() && CommonUtil.passwordPatternSpecialChar.matcher(userPassword.getText().toString()).find()) {
                    messageFromServer.setText("Password should be at least 6 characters long.");


                } else {

                    if (CommonUtil.passwordPatternUpperCase.matcher(userPassword.getText().toString()).find() &&
                            !CommonUtil.passwordPatternNumber.matcher(userPassword.getText().toString()).find() &&
                            CommonUtil.passwordPatternSpecialChar.matcher(userPassword.getText().toString()).find()) {
                        messageFromServer.setText("The password should have at least one number");

                    } else if (!CommonUtil.passwordPatternUpperCase.matcher(userPassword.getText().toString()).find() &&
                            CommonUtil.passwordPatternNumber.matcher(userPassword.getText().toString()).find() &&
                            CommonUtil.passwordPatternSpecialChar.matcher(userPassword.getText().toString()).find()) {
                        messageFromServer.setText("The password should have at least one uppercase letter");


                    } else if (CommonUtil.passwordPatternUpperCase.matcher(userPassword.getText().toString()).find() &&
                            CommonUtil.passwordPatternNumber.matcher(userPassword.getText().toString()).find() &&
                            !CommonUtil.passwordPatternSpecialChar.matcher(userPassword.getText().toString()).find()) {

                        messageFromServer.setText("The password should have at least one special character.");
                    } else {

                        messageFromServer.setText("Password should be at least 6 characters long ,1 uppercase letter, 1 number and 1 special characters");
                    }
                }
            } else if (!isValidName && isValidEmail && isValidPassword) { //secenerio 17
                name_errorMsg.setVisibility(View.VISIBLE);
                name_errorMsg.setText("The name should contain only alphabets");
            }*/

        } else {
            signUp();
        }
    }

    public void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyBoard();
        }
        return super.dispatchTouchEvent(ev);
    }

    private class RetrieveTokenTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String accountName = params[0];
            String scopes = "oauth2:profile email";
            String token = null;
            try {
                token = GoogleAuthUtil.getToken(getApplicationContext(), accountName, scopes);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (UserRecoverableAuthException e) {
                startActivityForResult(e.getIntent(), RC_SIGN_IN);
            } catch (GoogleAuthException e) {
                e.printStackTrace();
            }
            return token;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            LoginFromFaceBookOrGooglePlus(userNameFromGoogle, "Google", s, userEmailIdfromGoogle);
        }
    }


}




