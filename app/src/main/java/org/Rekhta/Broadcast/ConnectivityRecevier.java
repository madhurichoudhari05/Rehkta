package org.Rekhta.Broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;


import org.Rekhta.activites.ServerErrorActivity;
import org.Rekhta.utils.CommonUtil;

public class ConnectivityRecevier extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();




        if (networkInfo == null || !networkInfo.isConnected()) {
            CommonUtil.isOffline = true;
           // Toast.makeText(context, "No Internet Connections", Toast.LENGTH_SHORT).show();
            Intent intent1 = new Intent(context, ServerErrorActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent1);
        }

    }
}
