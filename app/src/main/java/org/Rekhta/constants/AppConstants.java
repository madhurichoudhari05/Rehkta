package org.Rekhta.constants;

public class AppConstants {


    public static final int REQUEST_CODE_TAKE_PICTURE = 101;
    public static final int REQUEST_CODE_GALLERY = 102;
    public final static int REQUEST_CODE_PERMISSION = 400;
    public static final int PERMISSION_CAMERA = 103;
    public static final int SELECT_IMAGE_REQUEST_GALLERY = 104;

}
