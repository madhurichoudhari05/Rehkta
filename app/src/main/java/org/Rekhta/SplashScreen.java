package org.Rekhta;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.UUID;

import org.Rekhta.activites.Dashboard;
import org.Rekhta.activites.MyFavoritesActivity;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.LanguageSelection;

public class SplashScreen extends Activity implements View.OnClickListener {


    private final int SPLASH_DISPLAY_LENGTH = 1600;
    int i = 0;
    private ImageView pGif;
    private SharedPreferences prefs = null;
    private SharedPreferences DataPrefs = null;
    private Button continuebtn, english_lang, hindi_lang, urdu_lang;
    private ImageView langwelcomeImg,splashRImg, langRImg;
    private RelativeLayout mainLayout;

    private LinearLayout langSectionView;
    private SharedPreferences sharedPreferences;
    boolean isAnimated = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        /* Making this activity, full screen */

        CommonUtil.isSkipped = false;
        CommonUtil.gType = "";

        if (savedInstanceState == null) {
            CommonUtil.favouritesPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.FAV_FILE, MODE_PRIVATE);
            prefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME, MODE_PRIVATE);
            sharedPreferences = this.getSharedPreferences("MyPref", 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("isOffline", false);
            editor.commit();
            DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
            String uniqueId = UUID.randomUUID().toString();
            CommonUtil.SharedPrefKeys.UniqueId = uniqueId;
            CommonUtil.SharedPrefKeys.DeviceParams = Settings.Secure.getString(getApplicationContext().
                    getContentResolver(), Settings.Secure.ANDROID_ID) + "," + Build.MANUFACTURER + "," + uniqueId + ",Android";
            initWidgets();
            LanguageSelection languageSelection = new LanguageSelection(this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    checkWhtherLoggedInOrNot();

                }
            }, SPLASH_DISPLAY_LENGTH);
        } else {
            CommonUtil.favouritesPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.FAV_FILE, MODE_PRIVATE);
            prefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME, MODE_PRIVATE);
            sharedPreferences = this.getSharedPreferences("MyPref", 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("isOffline", false);
            editor.commit();
            DataPrefs = getSharedPreferences(CommonUtil.SharedPrefKeys.MY_SHAREDPREF_NAME + "userData", MODE_PRIVATE);
            String uniqueId = UUID.randomUUID().toString();
            CommonUtil.SharedPrefKeys.UniqueId = uniqueId;
            CommonUtil.SharedPrefKeys.DeviceParams = Settings.Secure.getString(getApplicationContext().
                    getContentResolver(), Settings.Secure.ANDROID_ID) + "," + Build.MANUFACTURER + "," + uniqueId + ",Android";
            initWidgets();
            langSectionView.setVisibility(View.VISIBLE);
            langwelcomeImg.setVisibility(View.VISIBLE);
            if (!isAnimated) {
                MoveRIconUp();
            }


        }


    }


    public void checkWhtherLoggedInOrNot() {

        // If app is first time started after Installation
        if (prefs.getBoolean("firstrun", true)) {
            DataPrefs.edit().putBoolean("loggedIn", false).apply();
            prefs.edit().putBoolean("firstrun", false).apply();

            showLanguageSelectionView();


        } else {
            // after second Time app started after installation

            if (DataPrefs.getBoolean("loggedIn", true)) {
                CommonUtil.userName = DataPrefs.getString("userName", "DEFAULT");
                //CommonUtil.userName = "Surendra kumar Singh";
                CommonUtil.userEmail = DataPrefs.getString("userEmail", "DEFAULT");
                Intent mainIntent = new Intent(SplashScreen.this, Dashboard.class);
                SplashScreen.this.startActivity(mainIntent);
                SplashScreen.this.finish();
            } else {
//                showLanguageSelectionView();

                fadeOutAndHideImage(splashRImg);
            }
        }
    }

    public void initWidgets() {

        splashRImg = (ImageView) findViewById(R.id.splashRlogo);
        langRImg = (ImageView) findViewById(R.id.langRlogo);
        langSectionView = (LinearLayout) findViewById(R.id.selectLangView1);

        langwelcomeImg = (ImageView) findViewById(R.id.welcomtxt1);
        continuebtn = (Button) findViewById(R.id.continuebtn);
        english_lang = (Button) findViewById(R.id.english_lang);
        hindi_lang = (Button) findViewById(R.id.hindi_lang);
        urdu_lang = (Button) findViewById(R.id.urdu_lang);

        english_lang.setOnClickListener(this);
        hindi_lang.setOnClickListener(this);
        urdu_lang.setOnClickListener(this);

        Typeface engtf = Typeface.createFromAsset(this.getAssets(), "fonts/LatoBold.ttf");
        continuebtn.setTypeface(engtf);

        int langIntCode = DataPrefs.getInt("langCode", 0);
        if (langIntCode == 0) {
            resetButtonColorsAndHighLightSelected(1);
        } else {
            resetButtonColorsAndHighLightSelected(langIntCode);
        }
        continuebtn.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               saveTheLangCodeLocally();
                                               Intent intent = new Intent(SplashScreen.this, MainActivity.class);
//                                             Intent intent = new Intent(SplashScreen.this, Dashboard.class);
                                               startActivity(intent);
                                               finish();
                                           }
                                       }
        );
    }

    public void showLanguageSelectionView() {

        //   splashRImg.setVisibility(View.VISIBLE);

//        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, 500, 0);
//        translateAnimation.setDuration(1000);
        langSectionView.setVisibility(View.VISIBLE);
//        translateAnimation.setFillAfter(true);
//        langSectionView.setAnimation(translateAnimation);

//        TranslateAnimation translateAnimation2 = new TranslateAnimation(0, 0, 205, 0);
//        translateAnimation2.setDuration(1000);
        langRImg.setVisibility(View.GONE);
//        translateAnimation2.setFillAfter(true);
//        langRImg.setAnimation(translateAnimation2);
        AlphaAnimation animation1 = new AlphaAnimation(0.0f, 2.0f);
        Animation hide = AnimationUtils.loadAnimation(this, R.anim.bottom_to_center);
        Animation hide2 = AnimationUtils.loadAnimation(this, R.anim.center_top);
        //langRImg.startAnimation(hide2);
        langSectionView.startAnimation(hide);


        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);

        //langwelcomeImg.startAnimation(alphaAnimation);
        //langwelcomeImg.animate().alpha(1.0f);
        langwelcomeImg.setVisibility(View.VISIBLE);
        fadeOutAndSHowImage(langwelcomeImg);
        animation1.setDuration(500);
        animation1.setStartOffset(800);
        animation1.setFillAfter(true);
        //langwelcomeImg.startAnimation(animation1);
    }

    public void resetButtonColorsAndHighLightSelected(int selectedNo) {
        english_lang.setBackgroundResource(R.drawable.languagebtn);
        hindi_lang.setBackgroundResource(R.drawable.languagebtn);
        urdu_lang.setBackgroundResource(R.drawable.languagebtn);
        english_lang.setTextColor(Color.BLACK);
        hindi_lang.setTextColor(Color.BLACK);
        urdu_lang.setTextColor(Color.BLACK);

        if (selectedNo == 1) {
            english_lang.setBackgroundResource(R.drawable.languagebtn_selected);
            english_lang.setTextColor(Color.WHITE);

        } else if (selectedNo == 2) {
            hindi_lang.setBackgroundResource(R.drawable.languagebtn_selected);
            hindi_lang.setTextColor(Color.WHITE);
        } else {
            urdu_lang.setBackgroundResource(R.drawable.languagebtn_selected);
            urdu_lang.setTextColor(Color.WHITE);
        }
        CommonUtil.languageCode = selectedNo;
    }

    public void saveTheLangCodeLocally() {
        DataPrefs.edit().putInt("langCode", CommonUtil.languageCode).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.english_lang:
                resetButtonColorsAndHighLightSelected(1);
                break;
            case R.id.hindi_lang:
                resetButtonColorsAndHighLightSelected(2);
                break;
            case R.id.urdu_lang:
                resetButtonColorsAndHighLightSelected(3);
                break;
        }
    }

    private void fadeOutAndHideImage(final ImageView img) {
        Animation fadeOut = new AlphaAnimation(15, 60);

        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(50);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {

                int orentaiont = getResources().getConfiguration().orientation;
                if (orentaiont == 2) {

                    MoveRIconUp();
                    showLanguageSelectionView();

                } else if (orentaiont == 1) {
                    showLanguageSelectionView();
                }

            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeOut);
    }

    private void MoveRIconUp() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        int percent = height * 70 / 100;
        isAnimated = true;
        splashRImg.animate().translationX(0).translationY(-110).setInterpolator(new AccelerateInterpolator()).setDuration(500);
        langwelcomeImg.animate().translationX(0).translationY(-110).setInterpolator(new AccelerateInterpolator()).setDuration(500);

    }


    private void fadeOutAndSHowImage(final ImageView img) {

        Animation fadeOut = new AlphaAnimation(0, 1);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(800);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {

                // showLanguageSelectionView();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeOut);
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}


