package org.Rekhta.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.model.LocalModels.AddGhazal;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.RestClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class GhazalContentService extends IntentService {

    private AppDatabase db;

    public GhazalContentService() {

        super("GhazalContentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            JSONObject jsonObj = null;
            db = MyApplicationClass.getDb();
            String ghazalId = null;

            String jsonObjectData = intent.getStringExtra("jsonObject");
            if (jsonObjectData.contains("{")) {
                try {
                    jsonObj = new JSONObject(jsonObjectData);
                    ghazalId = jsonObj.getString("I");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                ghazalId = jsonObjectData;
            }

            String ghazalTitle = intent.getStringExtra("ghazalTitle");
            String autherName = intent.getStringExtra("ghazalAuther");


            final AddGhazal addGhazal = new AddGhazal();
            addGhazal.setTitle(ghazalTitle);
            addGhazal.setAuther(autherName);


            RestClient.get().getContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                    CommonUtil.languageCode, ghazalId, new Callback<HashMap>() {
                        @Override
                        public void success(HashMap res, Response response) {

                            try {
                                JSONObject obj = new JSONObject(res);
                                if (obj.getInt("S") == 1) {
                                    JSONObject obj1 = new JSONObject(res);
                                    Log.e("res", " " + res);

                                    pumpData(obj1, CommonUtil.languageCode, addGhazal);
                                    //calculateTextSize(obj1);
                                    //renderComplete=true;
                                } else {
                                    //Toast.makeText(this, "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            System.out.print("-----------------------------------------------------------");
                            System.out.print(response);
                            System.out.print(res);
                        }


                        @Override
                        public void failure(RetrofitError error) {


                        }
                    });
        }
    }

    private void pumpData(JSONObject obj1, int languageCode, final AddGhazal addGhazal) {

        try {
            JSONObject mainObj = obj1.getJSONObject("R");
            JSONObject mainContent = new JSONObject(mainObj.getString("CR"));
            addGhazal.setContent_data(mainContent.toString());
            addGhazal.setLang_type(CommonUtil.languageCode);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    db.ghazalDao().insert(addGhazal);
                }
            }).start();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
