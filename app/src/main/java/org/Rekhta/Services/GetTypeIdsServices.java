package org.Rekhta.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.Rekhta.interfaces.API;
import org.Rekhta.model.ContentRPojo;
import org.Rekhta.model.ContentTypePojo;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.RestClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class GetTypeIdsServices extends IntentService {


    public GetTypeIdsServices() {
        super("GetTypeIdsServices");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final SharedPreferences idsPreferences = this.getSharedPreferences("IdsPreference", 0);
            String token = "Bearer jYcqjxKUiu4prCL2ZLjyAwz1Y4e4bYnLgOSxFbiB7FcZrCglhFYSuqJvpMyXV7ubM1zrcB_5wpZ6Qd5SmpPQM_ApwnL61_mLt1J0D3mQGAjJPPGmqkK5JlDli-v-2Z3cjVi4VKj9BLCFK-LNgMpimTD_ZWyFYwksq_KCzBbkMoNCCU7yBNtJozUw_sYHLCkXv3zHHSyGMeNTTyXs0gQdvVzs9CPcT8K3UgBlbCLVQD4KF5s_DZKJ5Mji0Qd092UqZB2n2TbJS-49ARukrGLlGAvCgGvCdN0XdSkjNHsRKWoTXCKC_GLkKBV9ab5kOK-BDLvF1rVvJBdnSvg_luAInM6aGgaCmM7vSw8DPlCFLoqFiqSQwWNlwTD3v_DBSgHJT5VF1zQGBpN_SA7yZVqLBaPtJeBd2exmfHZYpmc5UH6sWgFGvrIaXwStq7u5Lg1ECdiHNogUg4GqZaEkVpzAiy2DFq1w4Ux8se8xSujLIp0";

            String pattern = "dd-MM-yyyy";
            String date = new SimpleDateFormat(pattern).format(new Date());
            RestClient.get().getContentTypeIds(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId, token, date, new Callback<ContentTypePojo>() {
                @Override
                public void success(ContentTypePojo contentTypePojo, Response response) {

                    if (contentTypePojo != null) {
                        CommonUtil.contentTypePojo = contentTypePojo;
                    }


                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }


}
