package org.Rekhta.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import org.Rekhta.db.database.AppDatabase;
import org.Rekhta.model.LocalModels.AddNazam;
import org.Rekhta.utils.CommonUtil;
import org.Rekhta.utils.MyApplicationClass;
import org.Rekhta.utils.RestClient;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class NazamContentService extends IntentService {


    private AppDatabase db;

    public NazamContentService() {
        super("NazamContentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            JSONObject jsonObj = null;
            db = MyApplicationClass.getDb();
            String nazamId = null;

            String jsonData = intent.getStringExtra("Ivalue");
            if (jsonData.contains("{")) {
                try {
                    jsonObj = new JSONObject(jsonData);
                    nazamId = jsonObj.getString("I");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                nazamId = jsonData;
            }


            String nazamTitle = intent.getStringExtra("title");
            String content = intent.getStringExtra("content");
            String autherName = intent.getStringExtra("autherName");


            final AddNazam addNazam = new AddNazam();
            addNazam.setTitle(nazamTitle);
            addNazam.setContent(content);
            addNazam.setAuther(autherName);

            RestClient.get().getContentById(CommonUtil.SharedPrefKeys.contentType, CommonUtil.SharedPrefKeys.UniqueId,
                    CommonUtil.languageCode, nazamId, new Callback<HashMap>() {
                        @Override
                        public void success(HashMap res, Response response) {

                            try {
                                JSONObject obj = new JSONObject(res);
                                if (obj.getInt("S") == 1) {
                                    JSONObject obj1 = new JSONObject(res);
                                    Log.e("res", " " + res);

                                    pumpData(obj1, CommonUtil.languageCode, addNazam);
                                    //calculateTextSize(obj1);
                                    //renderComplete=true;
                                } else {
                                    //Toast.makeText(this, "Oops! some error occurred,please try again", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            System.out.print("-----------------------------------------------------------");
                            System.out.print(response);
                            System.out.print(res);


                        }


                        @Override
                        public void failure(RetrofitError error) {


                        }
                    });

        }
    }

    private void pumpData(JSONObject obj1, int languageCode, final AddNazam addNazam) {

        try {
            JSONObject mainObj = obj1.getJSONObject("R");
            JSONObject mainContent = new JSONObject(mainObj.getString("CR"));
            addNazam.setDataArray(mainContent.toString());
            addNazam.setLang_type(CommonUtil.languageCode);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    db.userDao().insert(addNazam);
                }
            }).start();


            JSONArray Parray = mainObj.getJSONArray("P");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}





